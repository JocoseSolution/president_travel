﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Serialization;
//using System.Threading.Tasks;

namespace ITZLib
{
    //For User Login Info 
    public class ITZLoginUsers
    {
        AdvanceLoginServiceImpl objAdvLog = new AdvanceLoginServiceImpl();
        ITZLib.ErrorLogs.ErrorLog errLog = new ITZLib.ErrorLogs.ErrorLog();
        //public string ITZLoginUser(_validateLogin LoginUserParam)
        //{
        //    // ITZComman Class _validateLogin//
        //    LoginParams objLPr = new LoginParams();
        //    try
        //    {
        //        objLPr.modeType = LoginUserParam.strModeType;
        //        objLPr.username = LoginUserParam.strUserName;
        //        objLPr.password = LoginUserParam.strPassword;
        //        LoginServiceImpl objLSr = new LoginServiceImpl();
        //        return objLSr.validateLogin(objLPr);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        public string ITZForgetPassword(_ForgetPassword ForgetPwdParam)
        {
            // ITZComman Class _ForgetPassword//
            ForgetPwdParams objForgetPwd = new ForgetPwdParams();
            ForgetPasswordServiceImpl objSerImpl = new ForgetPasswordServiceImpl();
            try
            {
                objForgetPwd.dob = ForgetPwdParam.strDOB;
                objForgetPwd.emailId = ForgetPwdParam.strEmailID;
                objForgetPwd.username = ForgetPwdParam.strUserName;
            }
            catch (Exception ex)
            {
                ////throw ex;
                try
                {
                    errLog.WriteErrorLog(ex.Message.ToString());
                }
                catch (Exception err) { }
            }
            return objSerImpl.ForgetPwd(objForgetPwd);
        }
        public string ITZChangePWD(_ChangePassword ChangePwdParam)
        {
            // ITZComman Class _ChangePassword//
            ChangePwdParams objChgParam = new ChangePwdParams();
            ChangePasswordServiceImpl objChgImpl = new ChangePasswordServiceImpl();
            try
            {
                objChgParam.userId = ChangePwdParam.strUserID;
                objChgParam.dateOfBirth = ChangePwdParam.strDOB;
                objChgParam.oldPwd = ChangePwdParam.strOldPWD;
                objChgParam.newPwd = ChangePwdParam.strNewPWD;
                objChgParam.confirmPwd = ChangePwdParam.strConfrimPWD;
            }
            catch (Exception ex)
            {
                ////throw ex;
                try
                {
                    errLog.WriteErrorLog(ex.Message.ToString());
                }
                catch (Exception err) { }
            }
            return objChgImpl.changePassword(objChgParam);
        }

        public UserInfo Advance_Login(AdvLoginPar objParmVals)
        {
            UserInfo objResp = new UserInfo();
            AdvanceLoginParams objParm = new AdvanceLoginParams();
            try
            {
                objParm.modeType = objParmVals.strModeType != null && objParmVals.strModeType != "" ? objParmVals.strModeType.Trim() : "";
                objParm.password = objParmVals.strPassword != null && objParmVals.strPassword != "" ? objParmVals.strPassword.Trim() : "";
                objParm.username = objParmVals.strUserName != null && objParmVals.strUserName != "" ? objParmVals.strUserName.Trim() : "";
                objResp = objAdvLog.validateLogin(objParm);
                string xmlResp = ToXML(objResp);
                string xmlReq = ToXML(objParm);
                errLog.WriteErrorLogLoginResp(xmlReq, "LoginRequest_");
                errLog.WriteErrorLogLoginResp(xmlResp, "LoginResponse_");
            }
            catch (WebException ex)
            {
                try
                {
                    errLog.WriteErrorLog(ex.Message.ToString());
                }
                catch (Exception err) { }
                ////var resp = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
            }
            return objResp;
        }

        public string ToXML(object objToXml)
        {
            string respxml = "";
            try
            {
                var stringwriter = new System.IO.StringWriter();
                var serializer = new XmlSerializer(objToXml.GetType());
                serializer.Serialize(stringwriter, objToXml);
                respxml = stringwriter.ToString();
            }
            catch (Exception ex)
            {
            }
            return respxml;
        }


    }
}
