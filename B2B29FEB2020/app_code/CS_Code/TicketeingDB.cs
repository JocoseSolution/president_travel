﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections;
using System.Xml;
using System.Xml.Linq;

/// <summary>
/// Summary description for TicketeingDB
/// </summary>
public class TicketeingDB
{
    public TicketeingDB()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public ArrayList GalwsTicketResponseParse(string PNBF2Res)
    {
        //string PNBF2Res = HardCodedTicketingResponse();
        #region Get Ticket number from xml with pax
        ArrayList TktNoArray = new ArrayList();
        try
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(PNBF2Res);
            XmlNode xxd = xd.SelectSingleNode("//PNRBFRetrieve");
            XmlNodeList xt = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSRText");
            XmlNodeList xt1 = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSR");           // Get Ticket No of Adult Child and Infant- Devesh(09-04-2018)
                                                                                          //XmlNodeList xt1 = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSR[starts-with(SSRCode,'TKNE')]");  // Comment line because not getting Infant Ticket No, Get Infant Name in TicketNo-Devesh(09-04-2018)
            #region Add if condition:Getting Meal in Response
            if (xt1.Count != xt.Count && xt1.Count > xt.Count)
            {
                xt1 = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSR[starts-with(SSRCode,'TKNE')]");
            }
            #endregion

            int aa = xt.Count - 1;
            for (int ii = 0; ii <= xt1.Count - 1; ii++)
            {
                XmlNode xt2 = xt1[ii];
                xt1[ii].AppendChild(xt[ii]);
                xxd.ReplaceChild(xt2, xt1[ii]);
            }
            int i = 0;
            XDocument xdo = XDocument.Load(new XmlNodeReader(xxd));

            var tkt = from t in xdo.Descendants("PNRBFRetrieve").Descendants("ProgramaticSSR")
                      from p in xdo.Descendants("PNRBFRetrieve").Descendants("FNameInfo")
                      from l in xdo.Descendants("PNRBFRetrieve").Descendants("LNameInfo")
                      where p.Element("AbsNameNum").Value == t.Element("AppliesToAry").Element("AppliesTo").Element("AbsNameNum").Value &&
                      l.Element("LNameNum").Value == t.Element("AppliesToAry").Element("AppliesTo").Element("AbsNameNum").Value &&
                      t.Element("Status").Value == "HK"
                      select new
                      {
                          indx = i,
                          fn = p.Element("FName").Value,
                          ln = l.Element("LName").Value,
                          tk = t.Element("ProgramaticSSRText").Element("Text").Value,
                          abs = t.Element("AppliesToAry").Element("AppliesTo").Element("AbsNameNum").Value

                      };

            foreach (var a in tkt)
            {
                i++;
                string[] tkt1 = STD.BAL.Utility.Split(a.fn, " ");
                string nm = "", tkno = "";
                for (int n = tkt1.Length - 1; n >= 0; n--)
                {
                    nm = nm + tkt1[n].ToString();
                }
                tkno = STD.BAL.Utility.Left(STD.BAL.Utility.Left(a.tk, 13), 3) + "-" + STD.BAL.Utility.Right(STD.BAL.Utility.Left(a.tk, 13), 10);
                TktNoArray.Add(nm + a.ln + "/" + tkno + "/" + (a.fn + a.ln).Replace(" ", ""));
            }
        }
        catch (Exception ex1)
        {
            ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(GetTicketNumberUsingOLTKT)", "Error_004", ex1, "GetTicketNumberUsingOLTKT");
            TktNoArray.Add("AirLine is not able to make ETicket");
        }
        #endregion
        return TktNoArray;

    }
}