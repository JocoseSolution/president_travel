﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="response.aspx.cs" Inherits="response" %>
    <%@ Import Namespace = "System.IO" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Zaakpay</title>
<link href="../stylesheets/styles-payflow.css" rel="stylesheet" type="text/css" />

 <link href="../javascripts/txnpage/sort.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../javascripts/txnpage/sort_files/jquery.js"></script>
		<script type="text/javascript" src="../javascripts/txnpage/sort_files/interface.js"></script>
<!--this is use for remove captchasecton color or image-->

<!--popup_layer-->  


</head>
<body>


<% 
    String secretKey = "Insert your secret key here";
    String allParamValue = null;
    Boolean isChecksumValid = false;
    allParamValue = ChecksumResponse.ChecksumCalculatorResponse.getAllNotEmptyParamValue(Request).Trim();
    String checksum = ChecksumResponse.ChecksumCalculatorResponse.calculateChecksum(secretKey, allParamValue);
    System.Diagnostics.Debug.WriteLine("allParamValue Response : " + allParamValue);
    System.Diagnostics.Debug.WriteLine("secretKey Response : " + secretKey);
    if(checksum != null){

        isChecksumValid = ChecksumResponse.ChecksumCalculatorResponse.verifyChecksum(secretKey, allParamValue, checksum);

    }
   
	%>
    <center>
    
   <table>
   
   <tr>
        <td align = "center">OrderId</td>
        <td align = "center"><%= Request.Params.Get("orderId")%> </td>
  </tr>
  <tr>
        <td align = "center">Response Code</td>
        <%if (isChecksumValid)
          { %>
        <td align = "center"> <%=Request.Params.Get("responseCode")%></td>
        <%}
          else
          { %>
          <td align ="center"><font color ="red">***</font></td>
          <%} %>
  
  </tr>
   <tr>
        <td align = "center">Response Description</td>
        <%if (isChecksumValid)
          { %>
        <td align = "center"> <%=Request.Params.Get("responseDescription")%></td>
        <%}
          else
          { %>
          <td align ="center"><font color ="red">The Response is Compromised.</font></td>
          <%} %>
  
  </tr>

   <tr>
        <td align = "center">Checksum Valid</td>
        <%if (isChecksumValid)
          { %>
        <td align = "center">Yes</td>
        <%}
          else
          { %>
          <td align ="center"><font color ="red">No </font><br /><br />The Transaction might have been Successfull.</td>
          
          <%} %>
  
  </tr>
   
   
   </table>
   
    </center>
    
</body>
</html>

