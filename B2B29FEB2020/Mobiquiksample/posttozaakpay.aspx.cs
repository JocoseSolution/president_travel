﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Text;
using System.Security.Cryptography;
using System.Collections.Specialized;
using System.IO;

namespace Checksum
{
    public class ParamSanitizer
    {
        public static string sanitizeParam(string param)
        {

            String ret = null;
            if (param == null)
                return null;

            ret = param.Replace("[>><>(){}?&* ~`!#$%^=+|\\:'\";,\\x5D\\x5B]+", " ");

            return ret;
        }

        public static String SanitizeURLParam(String url)
        {

            if (url == null)
                return "";

            Match match = Regex.Match(url, "^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", RegexOptions.IgnoreCase);

            if (match.Success)

                return url;
            else
                return "";

        }

    }


    public class ChecksumCalculator
    {
        public static string toHex(byte[] bytes)
        {
            StringBuilder hex = new StringBuilder(bytes.Length * 2);
            foreach (byte b in bytes)
                hex.AppendFormat("{0:x2}", b);
            return hex.ToString();


        }

        public static string calculateChecksum(string secretkey, string allparamvalues)
        {

            byte[] dataToEncryptByte = Encoding.UTF8.GetBytes(allparamvalues);
            byte[] keyBytes = Encoding.UTF8.GetBytes(secretkey);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyBytes);
            byte[] checksumByte = hmacsha256.ComputeHash(dataToEncryptByte);
            String checksum = toHex(checksumByte);

            return checksum;
        }

        public static Boolean verifyChecksum(String secretKey, String allParamVauleExceptChecksum, String checksumReceived)
        {

            byte[] dataToEncryptByte = Encoding.UTF8.GetBytes(allParamVauleExceptChecksum);
            byte[] keyBytes = Encoding.UTF8.GetBytes(secretKey);
            HMACSHA256 hmacsha256 = new HMACSHA256(keyBytes);
            byte[] checksumCalculatedByte = hmacsha256.ComputeHash(dataToEncryptByte); ;
            String checksumCalculated = toHex(checksumCalculatedByte);

            if (checksumReceived.Equals(checksumCalculated))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public static string getAllNotEmptyParamValue(HttpRequest Request)
        {
            String allNonEmptyParamValue = "";
          

            NameValueCollection postedValues = Request.Form;

            //optional & 
            //String[] paramSeq = { "amount","bankid","buyerAddress",
            //    "buyerCity","buyerCountry","buyerEmail","buyerFirstName","buyerLastName","buyerPhoneNumber","buyerPincode",
            //    "buyerState","currency","merchantIdentifier","merchantIpAddress","mode","orderId",
            //    "product1Description","product2Description","product3Description","product4Description",
            //    "productDescription","productInfo","purpose","returnUrl","shipToAddress","shipToCity","shipToCountry",
            //    "shipToFirstname","shipToLastname","shipToPhoneNumber","shipToPincode","shipToState","showMobile","txnDate",
            //    "txnType","zpPayOption"};

            //mandatory
            String[] paramSeq = { "amount","buyerEmail","currency","merchantIdentifier","orderId"};
           
            foreach(String i in paramSeq)
            {
                
                try
                {
                    String paramInArray = postedValues[i];
                    
                    if (!paramInArray.Equals("")) {
                        //paramName = postedValues.AllKeys[i];


                        String paramValue = ParamSanitizer.sanitizeParam(paramInArray);
                        

                        if (paramValue != null)
                        {
                            allNonEmptyParamValue = allNonEmptyParamValue +i+"="+ paramValue + "&";

                        }
                    }

            }
                catch(Exception e)
                {
                    Console.WriteLine("Exception caught: {0}", e);
                }
        }
           
            return allNonEmptyParamValue;

        }
    }

}
public partial class posttozaakpay : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
}