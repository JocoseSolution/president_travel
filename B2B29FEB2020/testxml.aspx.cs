﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class testxml : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string xmlr = "";



        // Open the file to read from.
        string readText = System.IO.File.ReadAllText(@"C: \Users\dev\OneDrive\Desktop\MVCPROB2B\Saurav Work\iweenbook.txt");
        //Console.WriteLine(readText);
        xmlr = RemoveAllNamespaces(readText);
        xmlr = readText.Replace("<![CDATA[", "").Replace("]]]]>>", "").Replace("]]>", "").Replace("S:", "").Replace("ns2:", "");
        XDocument xd = XDocument.Parse(xmlr);
        //XDocument xd2 = XDocument.Parse(responseXml);
        //int flight = 1;
        string GDSPR = "";
        string AIRLINEPNR = "";
        foreach (var AvlR in xd.Descendants("return").Descendants("FlightInvoice"))
        {
            foreach (var Avlflight in AvlR.Descendants("FlightRecord"))
            {
                GDSPR = Avlflight.Element("GdsPnr").Value.ToString();
                AIRLINEPNR= Avlflight.Element("AirlinePnr").Value.ToString();
            }
        }
    }
    public static string RemoveAllNamespaces(string xmlDocument)
    {
        XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

        return xmlDocumentWithoutNs.ToString();
    }

    //Core recursion function
    private static XElement RemoveAllNamespaces(XElement xmlDocument)
    {
        if (!xmlDocument.HasElements)
        {
            XElement xElement = new XElement(xmlDocument.Name.LocalName);
            xElement.Value = xmlDocument.Value;

            foreach (XAttribute attribute in xmlDocument.Attributes())
                xElement.Add(attribute);

            return xElement;
        }
        return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
    }
}