﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FltSearch.ascx.vb" Inherits="UserControl_FltSearch" %>

<%@ Register Src="~/UserControl/HotelSearch.ascx" TagPrefix="uc1" TagName="HotelSearch" %>
<%@ Register Src="~/UserControl/HotelDashboard.ascx" TagPrefix="uc1" TagName="HotelDashboard" %>


<link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
    rel="stylesheet" />

<style>
    .flt_icon {
        background-image: url(../Images/icons/location-alt-512.png) !important;
        width:20px;
    }
</style>

<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    * {
        box-sizing: border-box;
    }

    .input-container {
        display: -ms-flexbox; /* IE10 */
        display: flex;
        width: 100%;
        margin-bottom: 15px;
        height: 37px;
    }

    .icon {
        padding: 13px;
        background: #f1f1f1;
        color: #272727;
        min-width: 40px;
        text-align: center;
        border-bottom: 0.5px solid #a7a7a7;
        border-top: 0.5px solid #a7a7a7;
        border-left: 0.5px solid #a7a7a7;
    }

    .input-field {
        width: 100%;
        padding: 10px;
        outline: none;
    }

    .input-field:focus {
            border: 0.5px solid dodgerblue;
            box-shadow: 0px 0px 30px rgba(0, 99, 222, 0.8);
        }
</style>

<style>
    .circle {
        border-radius: 50%;
        margin: -32px;
        font-size: 2em;
        z-index: 1011;
        position: relative;
        margin-left: -356px;
        margin-top: 26px;
        cursor: pointer;
        border: 2px solid #1d1d1d00;
        ;
    }

    .fa-exchange {
        background: #0000;
        color: orange;
        padding: 6px;
        font-size: 15px;
    }


    @media only screen and (max-width: 600px) {
        .dis {
            display: none;
        }
    }
</style>


<!--Rotate CSS-->
<style>
    .circle {
        -moz-transition: all .5s linear;
        -webkit-transition: all .5s linear;
        transition: all .5s linear;
    }

    .circle.down {
            -moz-transform: rotate(180deg);
            -webkit-transform: rotate(180deg);
            transform: rotate(180deg);
        }
</style>
<!--Rotate CSS-->






    
        <h1 style="font-size: 24px; font-weight: 400;">Search Cheapest Flight</h1>





        <div class="container topways" style="margin-left: -17px;">

            <%--<input type='radio' class='free atype' value='0' name='dutch' checked='checked' autocomplete="off"/> Free<br/>--%>
            <label class="mail ">
                <input type="radio" name="TripType" value="rdbOneWay" id="rdbOneWay"/>
                One Way</label>
            &nbsp;
            <label class="mail">
                <input type="radio" name="TripType" value="rdbRoundTrip" id="rdbRoundTrip" />
                Round Trips</label>
            &nbsp;
            <label class="mail" style="display:none;">
                <input type="radio" name="TripType" value="rdbMultiCity" id="rdbMultiCity" />
                Multi-City
            </label>

        </div>
        <script>
            $(document).ready(function () {
                var selector = '.topways div label';
                $(selector).bind('click', function () {
                    $(selector).removeClass('active');
                    $(this).addClass('active');
                });

            });


        </script>


        <br />

        <div class="section">


            <div class="row">
                <div class="onewayss col-md-6">

                    <label for="exampleInputEmail1">
                        Leaving From:</label>

                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/flight_dep.png" style="width: 23px; margin-top: -6px;" /></i>
                        <input type="text" name="txtDepCity1" class="input-field" placeholder="Departure City" onclick="this.value = '';" id="txtDepCity1" />
                        <input type="hidden" id="hidtxtDepCity1" class="" name="hidtxtDepCity1" value="" />

                    </div>

                </div>


                <%--    
                 <div class="input-container">
    <i class="fa fa-envelope icon"></i>
    <input class="input-field" type="text" placeholder="Email" name="email">
  </div>--%>



                <a href="#">
                    <div class=" circle dis interchange" id="change" style=""><img src="../Images/icons/swap_1354136.png" style="width:20px;position: absolute;right: 360px;top: 3px;"/></div>
                </a>

                <div class="onewayss col-md-6">

                    <label for="exampleInputEmail1">
                        Going To:</label>

                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/Flight_Arri.png" style="width: 23px; margin-top: -6px;" /></i>
                        <input type="text" name="txtArrCity1" onclick="this.value = '';" id="txtArrCity1" class="input-field" placeholder="Destination City" />
                        <input type="hidden" id="hidtxtArrCity1" name="hidtxtArrCity1" value="" />

                    </div>

                </div>
            </div>


           

            <div class="row">
                <div class="col-md-6" id="one">

                    <label for="exampleInputEmail1">
                        Depart Date:</label>


                    <div class="input-container">
                        <i class="fa fa-calendar icon" aria-hidden="true"></i>
                        <input type="text" class="input-field" placeholder="dd/mm/yyyy" name="txtDepDate" id="txtDepDate" value=""
                            readonly="readonly" />
                        <input type="hidden" name="hidtxtDepDate" id="hidtxtDepDate" value="" />

                    </div>





                </div>
                <div class="col-md-6" id="Return">
                    <div id="trRetDateRow" style="display: none;">
                        <label for="exampleInputEmail1">
                            Return Date:</label>
                        <div class="input-container">
                            <i class="fa fa-calendar icon" aria-hidden="true"></i>
                            <input type="text" placeholder="dd/mm/yyyy" name="txtRetDate" id="txtRetDate" class="input-field" value=""
                                readonly="readonly" />
                            <input type="hidden" name="hidtxtRetDate" id="hidtxtRetDate" value="" />

                        </div>




                    </div>
                </div>
            </div>


            <div class="row">
            <div  style="display: none;" id="two">
                <div class="onewayss col-md-4" id="DivDepCity2">
                    <label for="exampleInputEmail1">
                        Leaving From:</label>
                        
                            <div class="input-container">
                                <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/flight_dep.png" style="width: 23px; margin-top: -6px;" /></i>
                            
                            <input type="text" name="txtDepCity2" class="input-field" placeholder="Departure City" id="txtDepCity2" />
                            <input type="hidden" id="hidtxtDepCity2" name="hidtxtDepCity2" value="" />
                     </div>
                   
                </div>

                <div class="onewayss col-md-4">
                    <label for="exampleInputEmail1">
                        Going To:</label>
                       
                            <div class="input-container">
                                <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/Flight_Arri.png" style="width: 23px; margin-top: -6px;" /></i>
                            
                            <input type="text" name="txtArrCity2" class="input-field" placeholder="Destination City" id="txtArrCity2" />
                            <input type="hidden" id="hidtxtArrCity2" name="hidtxtArrCity2" value="" />
                     </div>
                  
                </div>
                <div class="col-md-4" id="DivArrCity2">
                   
                    <label for="exampleInputEmail1">
                        Depart Date:</label>
                            <div class="input-container">
                                 <i class="fa fa-calendar icon" aria-hidden="true"></i>
                           
                            <input type="text" name="txtDepDate2" id="txtDepDate2" class="input-field" placeholder="dd/mm/yyyy" readonly="readonly" value="" />
                            <input type="hidden" name="hidtxtDepDate2" id="hidtxtDepDate2" value="" />
                  </div>
              
                </div>
            </div>
                </div>
            <div class="row">
            <div style="display: none;" id="three">

                <div class="onewayss col-md-4" id="DivDepCity3">
                   

                    
                            <div class="input-container">
                               <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/flight_dep.png" style="width: 23px; margin-top: -6px;" /></i>
                          
                            <input type="text" name="txtDepCity3" class="input-field" placeholder="Departure City" id="txtDepCity3" />
                            <input type="hidden" id="hidtxtDepCity3" name="hidtxtDepCity3" value="" />
                        </div>
                   
                </div>
                <div class="onewayss col-md-4">
                    

                       
                            <div class="input-container">
                               <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/Flight_Arri.png" style="width: 23px; margin-top: -6px;" /></i>
                            <input type="text" name="txtArrCity3" class="input-field" placeholder="Destination City" id="txtArrCity3" />
                            <input type="hidden" id="hidtxtArrCity3" name="hidtxtArrCity3" value="" />
                   </div>
                  
                </div>
                <div class="col-md-4" id="DivArrCity3">
                

                       

                            <div class="input-container">
                                <i class="fa fa-calendar icon" aria-hidden="true"></i>
                          
                            <input type="text" name="txtDepDate3" id="txtDepDate3" class="input-field" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate3" id="hidtxtDepDate3" value="" />
                    
                     </div>
                </div>
            </div>
                </div>
            <div class="row">
            <div style="display: none;" id="four">
                <div class="onewayss col-md-4" id="DivDepCity4">
                    

                        
                            <div class="input-container">
                                <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/flight_dep.png" style="width: 23px; margin-top: -6px;" /></i>
                           
                            <input type="text" name="txtDepCity4" class="input-field" placeholder="Departure City" id="txtDepCity4" />
                            <input type="hidden" id="hidtxtDepCity4" name="hidtxtDepCity4" value="" />
                        </div>
                   
                </div>
                <div class="onewayss col-md-4">
                    

                        <div class="input-container">
                         
                                <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/Flight_Arri.png" style="width: 23px; margin-top: -6px;" /></i>
                           
                            <input type="text" name="txtArrCity4" class="input-field" placeholder="Destination City" id="txtArrCity4" />
                            <input type="hidden" id="hidtxtArrCity4" name="hidtxtArrCity4" value="" />
                        </div>
                    
                </div>
                <div class="col-md-4" id="DivArrCity4">
                    
                        <div class="input-container">
                           
                                <i class="fa fa-calendar icon" aria-hidden="true"></i>
                           
                            <input type="text" name="txtDepDate4" id="txtDepDate4" class="input-field" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate4" id="hidtxtDepDate4" value="" />
                        </div>
                    
                </div>
            </div>
                </div>
            <div class="row">
            <div style="display: none;" id="five">
                <div class="onewayss col-md-4" id="DivDepCity5">
                    
                        <div class="input-container">
                           <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/flight_dep.png" style="width: 23px; margin-top: -6px;" /></i>
                            <input type="text" name="txtDepCity5" class="input-field" placeholder="Departure City" id="txtDepCity5" />
                            <input type="hidden" id="hidtxtDepCity5" name="hidtxtDepCity5" value="" />
                        </div>
                   
                </div>
                <div class="onewayss col-md-4">
                    <div class="form-group">
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/Flight_Arri.png" style="width: 23px; margin-top: -6px;" /></i>
                            <input type="text" name="txtArrCity5" class="input-field" placeholder="Destination City" onclick="this.value = '';" id="txtArrCity5" />
                            <input type="hidden" id="hidtxtArrCity5" name="hidtxtArrCity5" value="" />
                        </div>
                    </div>
                </div>
                <div class="col-md-4" id="DivArrCity5">
                 
                        <div class="input-container">
                          <i class="fa fa-calendar icon" aria-hidden="true"></i>
                            <input type="text" name="txtDepDate5" id="txtDepDate5" class="input-field" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate5" id="hidtxtDepDate5" value="" />
                        </div>
                 
                </div>
            </div>
                </div>
            <div class="row">
            <div style="display: none;" id="six">
                <div class="onewayss col-md-4" id="DivDepCity6">
                  
                        <div class="input-container">
                            <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/flight_dep.png" style="width: 23px; margin-top: -6px;" /></i>
                            <input type="text" name="txtDepCity6" class="input-field" placeholder="Departure City" id="txtDepCity6" />
                            <input type="hidden" id="hidtxtDepCity6" name="hidtxtDepCity6" value="" />
                        </div>
                   
                </div>

                <div class="onewayss col-md-4">
                 
                        <div class="input-container">
                         <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/Flight_Arri.png" style="width: 23px; margin-top: -6px;" /></i>
                            <input type="text" name="txtArrCity6" class="input-field" placeholder="Destination City" onclick="this.value = '';" id="txtArrCity6" />
                            <input type="hidden" id="hidtxtArrCity6" name="hidtxtArrCity6" value="" />
                        </div>
                  
                </div>
                <div class="col-md-4" id="ArrCity6">
                    
                        <div class="input-container">
                           <i class="fa fa-calendar icon" aria-hidden="true"></i>
                            <input type="text" name="txtDepDate6" id="txtDepDate6" class="input-field" placeholder="dd/mm/yyyy" readonly="readonly" />
                            <input type="hidden" name="hidtxtDepDate6" id="hidtxtDepDate6" value="" />
                        </div>
                 
                </div>
            </div>

                 <div class="row">
                   <div class="col-md-6" id="add">
                <div class="col-md-4">
                    <a id="plus" class="pulse btn btn-danger">Add City</a>
                </div>
                <div class="col-md-2">
                    <a id="minus" class="pulse btn btn-danger">Remove City</a>
                </div>
            </div>
            </div>
         </div>
           
           
         


         
            <div class="row" id="box">
                <div class="col-md-4">
                    <label>Adult(12+) Yrs</label>
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/Adult.png" style="width: 16px;" /></i>
                        <select class="input-field" id="Adult" name="Adult" style="padding: 9px 32px !important;">
                            <option value="1" selected="selected">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>

                    </div>
                </div>






                <div class="col-md-4">
                    <label>Child(2-12) Yrs</label>
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/child-boy.png" style="width: 16px;" /></i>
                        <select class="input-field" name="Child" id="Child" style="padding: 9px 32px !important;">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>

                        </select>

                    </div>
                </div>

                <div class="col-md-4">
                    <label>Infant(0-2) Yrs</label>
                    <div class="input-container">
                        <i class="icon" aria-hidden="true">
                            <img src="../Images/icons/infant.png" style="width: 16px;" /></i>
                        <select class="input-field" name="Infant" id="Infant" style="padding: 9px 32px !important;">
                            <option value="0">0</option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6</option>
                            <option value="7">7</option>
                            <option value="8">8</option>
                            <option value="9">9</option>
                        </select>

                    </div>
                </div>
            </div>


       

            <div class="row">
                <div class="col-md-12" id="advtravel" onclick="moreoptions()">
                    <div class="col-md-7">More options: Class of travel, Airline preference <i class="fa fa-chevron-right" aria-hidden="true"></i></div>&nbsp
                    <div class="col-md-3">
                    <label class="checkbox-inline" for="chp42">
                        <input id="chp42" class="styled" type="checkbox">Direct Flight</label>
                        </div>
                    </div>
                <%-- <div class="checkbox checkbox-info checkbox-circle">
                                    
                                    
                                </div>--%>
                <div class="col-md-12 advopt" id="advtravelss" style="display: none; width: 93%; float: left; position: relative; border: 1px solid #e4e5e5; padding: 14px; margin-top: 15px; margin-bottom: 10px; left: 24px;">
                    <div class="row">
                        <div class="col-md-3">

                            <label style="font-weight: 600;">
                                Airlines</label>
                            <input type="text" placeholder="Search By Airlines" class="form-control" name="txtAirline" value="" id="txtAirline" />
                            <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />


                        </div>
                        <div class="col-md-3">

                            <label style="font-weight: 600;">
                                Class Type</label>
                            <select name="Cabin" class="form-control" id="Cabin">
                                <option value="" selected="selected">--All--</option>
                                <option value="C">Business</option>
                                <option value="Y">Economy</option>
                                <option value="F">First</option>
                                <option value="W">Premium Economy</option>
                            </select>


                        </div>

                        <div class="col-md-3">
                            <label style="font-weight: 600;">Stops</label>
                            <select name="stops" class="form-control">
                                <option value="0-stop" selected="selected">0-Stop</option>
                                <option value="1-stop" selected="selected">1-Stop</option>
                                <option value="2-stop" selected="selected">2-Stop(+)</option>
                            </select>
                        </div>


                        <div class="col-md-3">
                            <label style="font-weight: 600;">Currency</label>
                            
                          <select name="countries" class="form-control" id="countries">
                             <option>India Rupee</option>
                              <option>Euro</option>
                              <option>British Pound</option>
                              <option>Australian Dollar</option>
                              <option>Canadian Dollar</option>
                              <option>Singapore Dollar</option>
                              <option>Malaysian Ringgit</option>
                              <option>Japanese Yen</option>
                              <option>Chinese Yuan Renminbi</option>
                              <option>Bahraini Dinar</option>
                              <option>Brazilian Real</option>
                              <option>British Pound</option>
                              <option>Emirati Dirham</option>
                              <option>Hong Kong Dollar</option>
                              <option>Kuwaiti Dinar</option>
                              <option>Mauritian Rupee</option>
                              <option>New Zealand Dollar</option>
                              <option>Omani Rial</option>
                              <option>Nepalese Rupee</option>
                              <option>Qatari Ruble</option>
                              <option>Russian Ruble</option>
                              <option>Saudi Arabian Riyal</option>
                              <option>South Africa Rand</option>
                              <option>Sri Lankan Rupee</option>
                              <option>Taiwan New Dollar</option>
                              <option>Thai Baht</option>
                             
                         
</select>
                        </div>
                    </div>
                    <br />
                    <div class="">

                        <div class="row">
                            <div class="col-md-12">
                                <label style="color:orange;font-weight:600;">Restrict my Search to :</label>
                                    <input type="checkbox" class="selectall" />
                                    Select All 
                            </div>
                        </div>

                        <div class="row" style="background: #eee;padding: 12px;width: 100%;left: 15px;position: relative;">




                            <div class="col-md-3">


                                <input type="checkbox" class="individual" />
                                GDS
                                <br>
                                <input type="checkbox" class="individual" />
                                AirAsia
                                <br>
                               
                                <input type="checkbox" class="individual" />
                                Indigo
                                <br>
                            </div>

                            <div class="col-md-3">


                                <input type="checkbox" class="individual" />
                                TrueJet
                                <br>
                                <input type="checkbox" class="individual" />
                                SpiceJet
                                <br>
                                <input type="checkbox" class="individual" />
                                Vistara
                                <br>
                                
                            </div>

                            <div class="col-md-3">


                                <input type="checkbox" class="individual" />
                                GoAir
                                <br>
                                
                                <input type="checkbox" class="individual" />
                                TBO
                                <br>
                                <input type="checkbox" class="individual" />
                                Richa
                                <br>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row" style="margin-right: 1px; float: right;">
                <div class="col-md-2">
                    <button type="button" id="btnSearch" name="btnSearch" value="Search" class="btn btn-danger" style="width: 156px;position: relative;right: -16px;">
                        <i class="fa fa-search"></i>  Search</button>
                </div>
            </div>



            <br />


        </div>
<br />
<br />


        <div class="col-md-3 nopad mrgss">

            <div class="col-md-6 nopad text-search" style="cursor: pointer; padding-top: 25px; display: none;" id="Traveller">
                <div class="form-group">
                    <label for="exampleInputEmail1">
                        Traveller</label>
                    <div class="input-group">

                        <div class="input-group-addon">
                            <i class="fa fa-user" aria-hidden="true"></i>
                        </div>
                        <input type="text" class="form-control" id="sapnTotPax" placeholder=" Traveller">
                    </div>
                </div>
            </div>



            <div class="col-md-12 col-xs-12" style="display: none;">
                <button type="button" onclick="plus()" class="btn btn-success btn-lg" id="serachbtn" style="margin-top: 5px;">
                    Done</button>
            </div>


        </div>
        <div class="clear1"></div>


        <div class="col-md-3 col-xs-12 text-search" id="trAdvSearchRow" style="display: none">
            <div class="lft ptop10">
                All Fare Classes
            </div>
            <div class="lft mright10">
                <input type="checkbox" name="chkAdvSearch" id="chkAdvSearch" value="True" />
            </div>
            <div class="large-4 medium-4 small-12 columns">
                Gds Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="GDS_RTF" id="GDS_RTF" value="True" />
                                </span>
            </div>

            <div class="large-4 medium-4 small-12 columns">
                Lcc Round Trip Fares
                                
                                <span class="lft mright10">
                                    <input type="checkbox" name="LCC_RTF" id="LCC_RTF" value="True" />
                                </span>
            </div>

        </div>



        <script>
            $('#basic').flagStrap();

            $('.select-country').flagStrap({
                countries: {
                    "US": "USD",
                    "AU": "AUD",
                    "CA": "CAD",
                    "SG": "SGD",
                    "GB": "GBP",
                },
                buttonSize: "btn-sm",
                buttonType: "btn-info",
                labelMargin: "10px",
                scrollable: false,
                scrollableHeight: "350px"
            });

            $('#advanced').flagStrap({
                buttonSize: "btn-lg",
                buttonType: "btn-primary",
                labelMargin: "20px",
                scrollable: false,
                scrollableHeight: "350px"
            });
        </script>

        <script>
            $(document).ready(function () {
                //$("#advtravel").click(function () {
                //    $("#advtravelss").slideToggle();
                //});


                $("#Traveller").click(function () {
                    $("#box").slideToggle();
                });
                $("#serachbtn").click(function () {
                    $("#box").slideToggle();
                });

            });
        </script>


        <script>
            function moreoptions() {
                var x = document.getElementById("advtravelss");
                if (x.style.display === "none") {
                    x.style.display = "block";
                } else {
                    x.style.display = "none";
                }
            }
        </script>



   

    


<%--<script>
    $(document).ready(function () {
        $("#countries").msDropdown();
    })
</script>--%>

<script>
    $(document).ready(function () {

        // $(".interchange").on('click', function () {
        $(".interchange").unbind().click(function () {
            debugger;
            var pickup = $('#txtDepCity1').val();
            $('#txtDepCity1').val($('#txtArrCity1').val());
            $('#txtArrCity1').val(pickup);


        });



    });
</script>

<script>
    $(".selectall").click(function () {
        $(".individual").prop("checked", $(this).prop("checked"));
    });
</script>


<script type="text/javascript">
    function plus() {
        document.getElementById("sapnTotPax").value = (parseInt(document.getElementById("Adult").value.split(' ')[0]) + parseInt(document.getElementById("Child").value.split(' ')[0]) + parseInt(document.getElementById("Infant").value.split(' ')[0])).toString() + ' Traveller';
    }
    plus();
</script>
<script type="text/javascript">
    var myDate = new Date();
    var currDate = (myDate.getDate()) + '/' + (myDate.getMonth() + 1) + '/' + myDate.getFullYear();
    document.getElementById("txtDepDate").value = currDate;
    document.getElementById("hidtxtDepDate").value = currDate;
    document.getElementById("txtRetDate").value = currDate;
    document.getElementById("hidtxtRetDate").value = currDate;
    var UrlBase = '<%=ResolveUrl("~/") %>';
</script>

<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/change.min.js") %>"></script>
<script type="text/javascript" src="<%=ResolveUrl("~/Scripts/search3.js?v=1")%>"></script>

<script type="text/javascript">

    $(function () {
        $("#CB_GroupSearch").click(function () {
            debugger;
            if ($(this).is(":checked")) {
                // $("#box").hide();
                $("#Traveller").hide();
                $("#rdbRoundTrip").attr("checked", true);
                $("#rdbOneWay").attr("checked", false);

            } else {
                // $("#box").show();
                $("#Traveller").show();
            }
        });
    });
</script>







<!--Rotate-->

<!--Rotate-->
