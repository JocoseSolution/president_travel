﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FltResIntl.aspx.vb" Inherits="FltResIntl"
    MasterPageFile="~/MasterAfterLogin.master" %>

<%@ Register Src="~/UserControl/FltSearchmdf.ascx" TagPrefix="uc1" TagName="FltSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link href="../Styles/flightsearch.css" rel="stylesheet" type="text/css" />
    <%-- <link href="../Styles/main.css" rel="stylesheet" />--%>
    <link href="../Styles/jquery-ui-1.8.8.custom.css" rel="stylesheet" type="text/css" />


    <link href="../Custom_Design/css/my_design.css" rel="stylesheet" />

<style type="text/css">

@media screen and (min-width: 1440px) and (max-width: 1920px){

.fltkun {
        position: relative;
    top: -17px;
    float: left;
    right: 655px;
    width: 501px;
  }

}

    /*@media screen and (min-width: 1440px) and (max-width: 1080px) {*/
        .fltkun {
            position: relative;
            top: -17px;
            float: left;
            right: 571px;
            width: 501px;
        }
    /*}*/

    /*@media only screen and (max-width: 1440px) {
  .fltkun {
        position: relative;
    top: -17px;
    float: left;
    right: 655px;
    width: 501px;
  }
}

        @media only screen and (max-width: 1920px) {
  .fltkun {
        position: relative;
    top: -17px;
    float: left;
    right: 655px;
    width: 501px;
  }
}*/

</style>


    <style>

.animation {
  box-shadow: 0 0 10rem 0 #000000;
}

body {
  /*box-shadow: 0 0 10rem 0 #000000 inset;*/
}

html, body {
  width: 100%;
  height: 100%;
}

body {
  /*box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-bottom: 3rem;
  background-color: #1D1F20;
  color: #FFFFFF;
  font-family: "Josefin Sans", sans-serif;
  font-size: 10px;
  font-weight: 100;
  box-shadow: 0 0 10rem 0 #000000 inset;*/
}

footer {
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100vw;
  height: 3rem;
  background-color: #1D1F20;
  text-align: center;
  font-family: "Indie Flower", cursive;
  font-size: 1.2rem;
  letter-spacing: 0.1rem;
}
footer span {
  font-family: "Josefin Sans", sans-serif;
  font-size: 1rem;
  font-weight: 100;
}

a {
  text-decoration: none;
  color: #FFA500;
}
a:hover {
  text-decoration: underline;
}

.animation {
  position: relative;
  overflow: hidden;
  width: 100%;
  height: 20rem;
  max-width: 55rem;
  background-color: #ffffff;
  border-radius: 4px;
  animation: 24s linear infinite sky;
}
.animation:after {
  position: absolute;
  z-index: 0;
  top: 0;
  left: 0;
  content: '';
  display: block;
  width: 100%;
  height: 100%;
  box-shadow: 0 7rem 30rem -4rem #5aacdc inset;
}
.animation [class^="animation__"], .animation [class*=" animation__"] {
  position: absolute;
}
.animation__plane {
  position: absolute;
  z-index: 1;
  top: calc(40% - (62px/2));
  left: calc(50% - (272px/2));
  width: 17rem;
  height: 6.2rem;
  animation: 2s ease-in-out takeOff, 5s ease-in-out infinite alternate flight 2s;
}
.animation__plane--shadow {
  bottom: 1rem;
  left: calc(54% - (8rem/2));
  width: 8rem;
  height: 1rem;
  border-radius: 50%;
  background-color: rgba(0, 0, 0, 0.15);
  animation: 2s ease-in-out takeOffShadow, 5s ease-in-out infinite alternate flightShadow 2s;
}
.animation__cloud--front {
  z-index: 2;
  top: 50%;
  left: 20%;
  width: 8.5rem;
  height: 3rem;
  fill: #cee4e7;
  animation: 4s linear infinite cloudFront;
}
.animation__cloud--middle {
  top: 22%;
  left: 75%;
  width: 6rem;
  height: 3rem;
  fill: #cee4e7;
  animation: 5s linear infinite cloudMiddle;
}
.animation__cloud--back {
  top: 6%;
  left: 34%;
  fill: #cee4e7;
  animation: 8s linear infinite cloudBack;
}
.animation .animation__loader {
  position: absolute;
  left: 0;
  bottom: 3rem;
  width: 100%;
  height: 4px;
  background-color: rgba(0, 0, 0, 0.15);
}
.animation .animation__loader:after {
  position: absolute;
  display: block;
  content: '';
  width: 100%;
  height: 100%;
  background-color: #cee4e7;
  overflow: hidden;
  animation: 1.5s ease-in-out infinite loader;
}

@keyframes loader {
  0% {
    left: -100%;
  }
  100% {
    left: 100%;
  }
}
@keyframes takeOff {
  0% {
    transform: translate(-220%, 110%);
  }
  100% {
    transform: translate(0, 0);
  }
}
@keyframes takeOffShadow {
  0% {
    transform: translate(-440%, 110%);
    opacity: 1;
  }
  100% {
    transform: translate(0, 0);
    opacity: 0.8;
  }
}
@keyframes flight {
  0% {
    transform: translate(0, 0);
  }
  25% {
    transform: translate(0, 10%);
  }
  75% {
    transform: translate(0, -10%);
  }
  100% {
    transform: translate(0, 0);
  }
}
@keyframes flightShadow {
  0% {
    transform: scale(0.8);
    opacity: 0.8;
  }
  25% {
    transform: scale(0.9);
    opacity: 1;
  }
  75% {
    transform: scale(1.1);
    opacity: 0.6;
  }
  100% {
    transform: scale(0.8);
    opacity: 0.8;
  }
}
@keyframes cloudFront {
  0% {
    transform: translate(520%, 0);
  }
  100% {
    transform: translate(-600%, 0);
  }
}
@keyframes cloudMiddle {
  0% {
    transform: translate(230%, 0);
  }
  100% {
    transform: translate(-900%, 0);
  }
}
@keyframes cloudBack {
  0% {
    transform: translate(910%, 0);
  }
  100% {
    transform: translate(-1000%, 0);
  }
}
@keyframes sky {
  0% {
    background-color: #ffffff;
  }
  20% {
    background-color: #ffffff;
  }
  35% {
    background-color: #ffc8bd;
  }
  50% {
    background-color: transparent;
  }
  70% {
    background-color: transparent;
  }
  85% {
    background-color: #ffc8bd;
  }
  100% {
    background-color: #ffffff;
  }
}

  </style>

    <style>
        .flt-dur {
            width: 92%;
            margin: 7px auto;
            font-size: 16px;
            position: relative;
        }

            .flt-dur:before {
                content: "";
                border-bottom: 1px solid #a5a4a4;
                width: 100%;
                display: inline-block;
                margin-bottom: 3px;
            }

        .dot {
            width: 6px;
            height: 6px;
            border-radius: 50%;
            top: -2px;
            left: -3px;
            position: absolute;
            background: #a5a4a4;
        }

        .dot-plan {
            margin: -10px auto;
            padding: 0px;
            width: 10%;
            background: #fff;
        }

        .dot2 {
            width: 6px;
            height: 6px;
            border-radius: 50%;
            top: -2px;
            right: -1px;
            position: absolute;
            background: #a5a4a4;
        }
    </style>

    <style type="text/css">
        #flterTab {
            padding-left: 0px !important;
        }

        input[type=checkbox] {
            margin-top: 7px;
        }

        body {
            font-family: "Lato", sans-serif;
        }

        ul.tab {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }
            /* Float the list items side by side */
            ul.tab li {
                float: left;
                display: inline;
            }
                /* Style the links inside the list items */
                ul.tab li a {
                    display: inline-block;
                    color: black;
                    text-align: center;
                    padding: 14px 16px;
                    text-decoration: none;
                    transition: 0.3s;
                    font-size: 17px;
                }
                    /* Change background color of links on hover */
                    ul.tab li a:hover {
                        background-color: #ddd;
                    }
                    /* Create an active/current tablink class */
                    ul.tab li a:focus, .active {
                        /*background-color: #ccc;*/
                    }

        /* Style the tab content */
        .tabcontent {
            /*display: none;*/
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
            width: 98%;
        }

        .tablinks {
            font-size: 11px;
            font-weight: 600;
        }

        .des-nav {
            z-index: 999;
            position: fixed;
            top: 135px;
            left: 21.5%;
            font-size: 13px;
            width: 76%;
            border: 1px solid #d1d1d1;
            border-bottom: none;
            border-top: none;
        }
    </style>

    <style>
        @import url(https://fonts.googleapis.com/css?family=Lobster);

        @-webkit-keyframes fly {
            to {
                transform: rotate(180deg);
            }
        }

        @-webkit-keyframes shadow {
            from {
                text-shadow: 0 4px 0 rgba(0, 0, 0, 0.2);
            }

            to {
                text-shadow: 4px 0 0 rgba(0, 0, 0, 0.2);
            }
        }

        #loader {
            position: absolute;
            top: 50%;
            left: 50%;
            width: 400px;
            height: 220px;
            margin: -110px 0 0 -200px;
            /*background: linear-gradient(skyblue, steelblue);*/
            border-radius: 8px;
            text-align: center;
            color: white;
            overflow: hidden;
            box-shadow: 0 4px 0 rgba(0, 0, 0, 0.2);
            background: linear-gradient(to right, rgb(75, 121, 161), rgb(40, 62, 81));
        }

            #loader:before {
                content: '';
                position: absolute;
                top: 20px;
                right: 20px;
                bottom: -180px;
                left: 20px;
                border-radius: 50%;
                border: 1.8px dashed rgba(255, 255, 255, 0.75);
            }

        #plane {
            position: absolute;
            left: 0;
            bottom: 0;
            height: 40px;
            width: 40px;
            line-height: 40px;
            text-align: center;
            font-size: 44px;
            transform-origin: 200px 20px;
            -webkit-animation: fly 5s infinite linear;
        }

            #plane i {
                margin: 4px 0 0 2px;
                transform: rotate(-45deg);
                -webkit-animation: shadow 5s infinite linear;
            }

        #loader p {
            margin-top: 120px;
            line-height: 32px;
            font-family: 'Lobster', sans-serif;
            font-size: 72px;
            text-shadow: 0 4px 0 rgba(0, 0, 0, 0.2);
        }
    </style>


    <%--<style type="text/css">
        input[type=checkbox] {
            margin-top: 7px;
        }
        fltrDiv
        body {
            font-family: "Lato", sans-serif;
        }

        ul.tab {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }
            /* Float the list items side by side */
            ul.tab li {
                float: left;
                display: inline;
            }
                /* Style the links inside the list items */
                ul.tab li a {
                    display: inline-block;
                    color: black;
                    text-align: center;
                    padding: 14px 16px;
                    text-decoration: none;
                    transition: 0.3s;
                    font-size: 17px;
                }
                    /* Change background color of links on hover */
                    ul.tab li a:hover {
                        background-color: #ddd;
                    }
                    /* Create an active/current tablink class */
                    /*ul.tab li a:focus, .active {
                        background-color: #ccc;
                    }*/

        /* Style the tab content */
        .tabcontent {
            /*display: none;*/
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>--%>


    <div class="w100   " id="toptop">
        <div id="MainSFR">
            <div id="MainSF">
                <%-- <div id="lftdv" class="hide" onclick="fltrclick1(this.id)">
                    <div class="collapse"><< Collapse</div>
                    <div class="collapse hide">>> Expand</div>
                </div>--%>

                <div class="w100">

                    <div class="" id="lftdv1">
                        <div id="fltrDiv">
                            <%-- <div class="clear"></div>
                            <div class="row">
                                   <input type="checkbox" class="chkNonstop" style="display:none" name="chkNonstop" id="chkNonstop" value="Y"/>
                                <div id="Modsearch" style="display: none; background: url(../images/blur-images-14.jpg); height: 100%; width: 100%; position: absolute; z-index: 99; left: 0; top: 0;">
                                    <div style="width: 85%; margin: 50px auto;">
                                        <div id="mdclose" style="width: 30px; height: 30px; border-radius: 50%; border: 3px solid #ccc; position: absolute; right: 10px; top: 10px; text-align: Center; font-size: 15px; background-color: black; cursor: pointer; color: #fff;">
                                            X
                                        </div>--%>
                            <div class="container">
                                <uc1:FltSearch runat="server" ID="FltSearch" />
                            </div>
                            <%--</div>
                                </div>--%>
                            <%--  <div class="large-2 medium-3 small-6 columns passenger"><button type="button" class="jplist-reset-btn cursorpointer f16" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="border: none; background: none;">
                                        Reset All Filters
                                        <img src="../images/reset.png" style="position: relative; top: 3px;" />
                                    </button></div> --%>
                            <%--</div>--%>



                            <%--<div class="bld f16">
                                <img src="../images/filter.png" />
                                Filter Search
                            </div>--%>


                            <div id="FilterLoad">
                            </div>
                            <div id="FilterBox">
                                <div class="jplist-panel">


                                    <div id="flterTab" style="display: none">
                                        <div id="flterTabO" class="spn1">
                                            Outbound
                                        </div>
                                        <div class="lft w2">&nbsp;</div>
                                        <div id="flterTabR" class="spn">
                                            Inbound
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="w20 lft wht" style="margin-top: 5px; width: 207px; margin-left: 14px; border: 1px solid #dad8d8; box-shadow: 0 2px 0 0px rgba(0,0,0,.08); -webkit-box-shadow: 0 2px 0 0px rgba(0,0,0,.08); -moz-box-shadow: 0 2px 0 0px rgba(0,0,0,.08); border-radius: 4px;">
                            <div id="dsplm" class="large-12 medium-12 small-12 columns jplist-panel" style="height: 59px; background: #005999; padding-top: 13px; padding-left: 20px!important; width: 100%; margin-top: -6px; border-radius: 4px 4px 0px 0px;">
                                <%--<input type="button" id="ModifySearch" value="Modify Search" onclick="DiplayMsearch1('DivMsearch');" class="pnday daybtn f10" />--%>
                                <%--<a href="#" id="ModifySearch" class="pnday msearch">Modify Search</a>--%>
                                <a href="#" class="" style="font-size: x-large; margin-top: -9px; position: absolute; color: #fff;">Filters</a>
                                <a href="#" class="" data-control-type="reset" data-control-name="reset" data-control-action="reset" style="float: right; margin-right: 9px; color: #fff;">Reset All </a>

                                <%--                                <a href="#" class="jplist-reset-btn cursorpointer resetall" data-control-type="reset" data-control-name="reset" data-control-action="reset">Reset All </a>--%>
                            </div>
                            <div class="clear">
                            </div>
                            <div class="row passenger w95 auto jplist-panel">


                                <div class="clear1"></div>
                                <div class="large-12 medium-12 small-12" id="FltrPrice">
                                    <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBP" style="color: #005999 !important;">
                                        <i>
                                            <img src="../Images/hd_icons/price-tag-icon.png" style="width: 22px;" /></i>  Price
                                    </div>
                                    <div id="FBP1" class="w98 lft">
                                        <div class="clear2"></div>
                                        <div class="fo">
                                            <div class="clsone">
                                                <div class="jplist-range-slider" data-control-type="range-slider" data-control-name="range-slider"
                                                    data-control-action="filter" data-path=".price" style="margin-left: 10px;">
                                                    <div class="clear1"></div>
                                                    <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                    </div>
                                                    <div class="clear1"></div>
                                                    <div class="lft w45">
                                                        <span class="lft">
                                                            <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                        <span class="value lft" data-type="prev-value"></span>
                                                    </div>
                                                    <div class="rgt w45">
                                                        <span class="value rgt" data-type="next-value"></span>
                                                        <span class="rgt">
                                                            <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                    </div>
                                                </div>
                                                <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                    data-path=".price" data-order="asc" data-type="number">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="fr">
                                            <div class="jplist-range-slider" data-control-type="range-sliderR" data-control-name="range-slider"
                                                data-control-action="filter" data-path=".price">
                                                <div class="clear1"></div>
                                                <div class="ui-slider w90 mauto" data-type="ui-slider">
                                                </div>
                                                <div class="clear1"></div>
                                                <div class="lft w45">
                                                    <span class="lft">
                                                        <img src="../images/rs.png" style="height: 13px;" />&nbsp;</span>
                                                    <span class="value lft" data-type="prev-value"></span>
                                                </div>
                                                <div class="rgt w45">
                                                    <span class="value rgt" data-type="next-value"></span>
                                                    <span class="rgt">
                                                        <img src="../images/rs.png" class="rgt" style="height: 13px;" />&nbsp;</span>
                                                </div>
                                            </div>
                                            <div class="hidden" data-control-type="default-sort" data-control-name="sort" data-control-action="sort"
                                                data-path=".price" data-order="desc" data-type="number">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>

                                <div class="clear1">
                                    <hr />
                                </div>

                                <div class="large-12 medium-12 small-12" id="fltrTime">
                                    <div class="clear"></div>
                                    <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBDT" style="color: #005999 !important;">
                                        <i>
                                            <img src="../Images/hd_icons/Airplane_Time-512.png" style="width: 22px;" /></i>  Departure Time
                                    </div>
                                    <div id="FBDT1" class="w98 lft " style="display: none;">
                                        <div class="fo">
                                            <div class="jplist-group"
                                                data-control-type="DTimefilterO"
                                                data-control-action="filter"
                                                data-control-name="DTimefilterO"
                                                data-path=".dtime" data-logic="or">
                                                <div class="clear"></div>

                                                <div class="lft w20 abc1 bdrs">
                                                    <input value="0_6" id="CheckboxT1" type="checkbox" title="Early Morning" />
                                                    <label for="CheckboxT1"></label>
                                                    <span>00 - 06</span>
                                                </div>
                                                <div class="lft w20 abc2t bdrs">
                                                    <input value="6_12" id="CheckboxT2" type="checkbox" title="Morning" />
                                                    <label for="CheckboxT2"></label>
                                                    <span>06 - 12</span>
                                                </div>

                                                <div class="lft w20 abc3t bdrs">
                                                    <input value="12_18" id="CheckboxT3" type="checkbox" title="Mid Day" />
                                                    <label for="CheckboxT3"></label>
                                                    <span>12 - 18</span>
                                                </div>
                                                <div class="lft w20 abc4t bdrs">
                                                    <input value="18_0" id="CheckboxT4" type="checkbox" title="Evening" />
                                                    <label for="CheckboxT4"></label>
                                                    <span>18 - 00</span>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="fr">
                                            <div class="jplist-group"
                                                data-control-type="DTimefilterR"
                                                data-control-action="filter"
                                                data-control-name="DTimefilterR"
                                                data-path=".atime" data-logic="or">

                                                <div class="lft w20 abc1 bdrs">
                                                    <input value="0_6" id="CheckboxT1R" type="checkbox" title="Early Morning" />
                                                    <label for="CheckboxT1"></label>
                                                    <span>00 - 06</span>
                                                </div>
                                                <div class="lft w20 abc2t bdrs">
                                                    <input value="6_12" id="CheckboxT2R" type="checkbox" title="Morning" />
                                                    <label for="CheckboxT2"></label>
                                                    <span>06 - 12</span>
                                                </div>

                                                <div class="lft w20 abc3t bdrs">
                                                    <input value="12_18" id="CheckboxT3R" type="checkbox" title="Mid Day" />
                                                    <label for="CheckboxT3"></label>
                                                    <span>12 - 18</span>
                                                </div>
                                                <div class="lft w20 abc4t bdrs">
                                                    <input value="18_0" id="CheckboxT4R" type="checkbox" title="Evening" />
                                                    <label for="CheckboxT4"></label>
                                                    <span>18 - 00</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>

                                <div class="clear1">
                                    <hr />
                                </div>
                                <div class="large-12 medium-12 small-12">
                                    <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBA" style="color: #005999 !important;">
                                        <i>
                                            <img src="../Images/hd_icons/plane-hiysd.png" style="width: 22px;" /></i>  Airline
                                    </div>
                                    <div class="w98 lft" id="FBA1">
                                        <div class="clear2"></div>
                                        <div id="airlineFilter" class="fo" style="overflow-y: scroll; height: 121px;"></div>
                                        <div id="airlineFilterR" class="fr"></div>
                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>


                                <div class="clear1">
                                    <hr />
                                </div>
                                <div class="large-12 medium-12 small-12">
                                    <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBS" style="color: #005999 !important;">
                                        <i>
                                            <img src="../Images/hd_icons/xxx036-2-512.png" style="width: 22px;" /></i>  Stops
                                    </div>
                                    <div class="w98 lft" id="FBS1">
                                        <div id="stopFlter" class="fo"></div>
                                        <div id="stopFlterR" class="fr"></div>
                                        <div class="clear"></div>
                                    </div>
                                </div>


                                <div class="clear1">
                                    <hr />
                                </div>
                                <div class="large-12 medium-12 small-12">
                                    <div class="bld closeopen" onclick="fltrclick(this.id)" id="FBFT" style="color: #005999 !important;">
                                        <i>
                                            <img src="../Images/hd_icons/unnamed.png" style="width: 22px;" /></i>  Fare Type
                                    </div>
                                    <div class="clsone" id="FBFT1">
                                        <div class="w98 lft">
                                            <div class="fo">
                                                <div class="jplist-group"
                                                    data-control-type="RfndfilterO"
                                                    data-control-action="filter"
                                                    data-control-name="RfndfilteO"
                                                    data-path=".rfnd" data-logic="or">
                                                    <div class="clear"></div>
                                                    <div class="lft w15">
                                                        <input value="r" id="CheckboxR1" type="checkbox" />
                                                    </div>
                                                    <div class="lft w80" style="padding-top: 3px">
                                                        <label for="CheckboxR1">Refundable</label>
                                                    </div>
                                                    <div class="clear"></div>
                                                    <div class="lft w15">
                                                        <input value="n" id="CheckboxR2" type="checkbox" />
                                                    </div>
                                                    <div class="lft w80" style="padding-top: 3px">
                                                        <label for="CheckboxR2">Non Refundable</label>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <div class="clear1">
                                    <hr />
                                </div>
                                <div class="large-12 medium-12 small-12 columns">
                                    <div class="bld closeopen" onclick="fltrclick(this.id)" id="DAFT" style="color: #005999 !important;">
                                        <i>
                                            <img src="../Images/hd_icons/unnamed.png" style="width: 22px;" /></i>  Airline Fare Type
                                    </div>
                                    <div class="w100 lft " id="DAFT1">
                                        <div class="clear"></div>

                                        <div id="AirlineFareType" class="FareTypeO"></div>
                                        <div class="clear1">
                                            <hr />
                                        </div>

                                        <div id="AirlineFareTypeR" class="FareTypeR"></div>

                                        <div class="clear"></div>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="fltbox2 rgt w79">
                            <div id="DivLoadP" class="w100" style="display: none;"></div>
                            <div id="searchtext" class="clear1 passenger" style="display: none;">
                                <div class="lft bld colormn hide">
                                    <div id="divSearchText1">
                                    </div>
                                </div>
                            </div>

                            <%-- <div class="w100 passenger">
                            <div class="lft padding1s cursorpointer clspMatrix" id="clspMatrix"><span style="padding-left:20px;">Matrix</span></div>
                            <div class="lft w80 hide" id="refinetitle"></div>
                        
                      

                        </div>--%>
                            <%--<div class="jplist-panel w5 rgt">
                            <div class="jplist-views"
                                data-control-type="views"
                                data-control-name="views"
                                data-control-action="views"
                                data-default="list-view">
                                <!-- data-default="list-view" -->
                                <button type="button" class="jplist-view list-view lft" style="background: url(../images/list-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in List View" data-type="list-view"></button>
                                <button type="button" class="jplist-view grid-view rgt" style="background: url(../images/grid-btn.png) no-repeat; height: 18px; width: 18px; border: none; cursor: pointer; margin-top: 3px;" title="Click to See Results in Grid View" data-type="grid-view"></button>
                            </div>
                        </div>--%>

                            <div class="jplist-panel">
                                <div id="divMatrixRtfO" class="divMatrix"></div>
                                <div id="divMatrixRtfR" class="hide divMatrix"></div>
                                <div id="divMatrix"></div>

                                <div class="w100 auto passenger">
                                    <div class="w100 ptop10" style="background: #005999; height: 54px; width: 98%; padding: 1px 18px 0px 0px; border-radius: 4px; overflow: hidden; min-height: 60px; border: 1px solid #ccc;">
                                        <div class="lft" style="margin-top: 16px;">
                                            <div id="displaySearchinput" class="lft"></div>
                                            <div class="rgt passenger" id="prexnt" style="margin-top: 20px; color: #000;">
                                                <div class="rgt" style="position: relative; left: 640px; top: -16px;">
                                                    <div class="auto lft">
                                                        <span id="spanShow" onclick="ShowHideDiscount('show');" class="spnBtnShow" style="display: none; cursor: pointer;">Show&nbsp;&nbsp;</span>
                                                        <span id="spanHide" onclick="ShowHideDiscount('hide');" style="display: none; cursor: pointer;" class="spnBtnHide">Hide&nbsp;&nbsp;</span>
                                                    </div>
                                                    <div class="auto lft">
                                                        <a href="#" id="PrevDay" class="pnday" style="color: #fff;">
                                                            <img src="../Styles/images/closeopen2.png" />&nbsp;Prev Day</a>
                                                    </div>
                                                    <div class="dotbdr lft">&nbsp;</div>
                                                    <div class="auto rgt"><a href="#" id="NextDay" class="pnday" style="color: #fff;">Next Day&nbsp;<img src="../Styles/images/closeopen.png" /></a></div>




                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>



                            <div id="RoundTripH" class="flightbox">
                                <div class="nav-container">

                                    <div class="jplist-panel">
                                        <div class="clear"></div>
                                        <div id="fltselct" class="hide">
                                            <div class="f16">Your Selection</div>
                                            <div id="selctcntnt" class="mauto">
                                                <div id="fltgo" class="w40 lft brdrtop">
                                                </div>
                                                <div class="w5 lft">&nbsp;</div>
                                                <div id="fltbk" class="w40 lft brdrtop">
                                                </div>
                                                <div id="fltbtn" class="w15 rgt">
                                                    <span class="f16 bld rgt" id="totalPay"></span>
                                                    <div class="clear"></div>
                                                    <input type="button" value="Book" class="buttonfltbk" id="FinalBook" />
                                                    <div id="Divproc" class="hide bld">
                                                        <img alt="Booking In Progress" width="50" height="50" src="~/Images/loading_bar.gif" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="w100 mauto bld bgmn1 colorwht">
                                            <div class="w50 padding1 lft">
                                                <div id="RTFTextFrom" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfFromPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfFromNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="w45 padding1 lft">
                                                <div id="RTFTextTo" class="lft f16"></div>
                                                <div class="rgt w5">&nbsp;</div>
                                                <div class="rgt">
                                                    <input type="button" id="RtfToPrevDay" value="Prev Day" class="pnday f10 colorwht" />
                                                    |
                                                    <input type="button" id="RtfToNextDay" value="Next Day" class="pnday f10 colorwht" />
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                        <div class="clear">
                                        </div>
                                        <div class="headerow hide">
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number" >
                                                    Depart
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number" >
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number" >
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ1"
                                                    data-control-name="sortCITZ1"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirlineR"
                                                    data-control-name="sortAirlineR"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptimeR"
                                                    data-control-name="sortDeptimeR"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Depart
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtimeR"
                                                    data-control-name="sortArrtimeR"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Arrival
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdurR"
                                                    data-control-name="sortTotdurR"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZR"
                                                    data-control-name="sortCITZR"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number">
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="lft w51">
                                    <div id="divFrom1" class="listO w100">
                                        <%-- <div class="dvsrc">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                    </div>
                                </div>
                                <div class="rgt w50">
                                    <div id="divTo1" class="listR w100">
                                        <%--<div class="dvsrc textaligncenter">
                                        <img src="~/Images/fltloding.gif" />
                                    </div>--%>
                                    </div>

                                    <%--<div class="dvsrc textaligncenter">
                                    <img src="~/Images/fltloding.gif" />
                                </div>--%>
                                </div>
                            </div>
                            <div class="flightbox" style="display: block;" id="onewayH">
                                <div class="nav-container">


                                    <div class="jplist-panel">

                                        <div class="headerow hide">
                                            <div class="w15 colorwht lft srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortAirline"
                                                    data-control-name="sortAirline"
                                                    data-control-action="sort"
                                                    data-path=".airlineImage"
                                                    data-order="asc"
                                                    data-type="text">
                                                    Airline
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortDeptime"
                                                    data-control-name="sortDeptime"
                                                    data-control-action="sort"
                                                    data-path=".deptime"
                                                    data-order="asc"
                                                    data-type="number" >
                                                    Departure
                                                </div>
                                            </div>
                                            <div class="w18 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortArrtime"
                                                    data-control-name="sortArrtime"
                                                    data-control-action="sort"
                                                    data-path=".arrtime"
                                                    data-order="asc"
                                                    data-type="number" >
                                                    Arrival
                                                </div>

                                            </div>
                                            <div class="w10 colorwht lft srtarw" onclick="myfunction(this)">

                                                <div
                                                    class="hidden"
                                                    data-control-type="sortTotdur"
                                                    data-control-name="sortTotdur"
                                                    data-control-action="sort"
                                                    data-path=".totdur"
                                                    data-order="asc"
                                                    data-type="number" >
                                                    Duration
                                                </div>
                                            </div>
                                            <div class="w24 lft">
                                                &nbsp;
                                            </div>
                                            <div class="w15 colorwht rgt srtarw" onclick="myfunction(this)">
                                                <div
                                                    class="hidden"
                                                    data-control-type="sortCITZ"
                                                    data-control-name="sortCITZ"
                                                    data-control-action="sort"
                                                    data-path=".price"
                                                    data-order="asc"
                                                    data-type="number" >
                                                    Fare
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="clear">
                                </div>
                                <div class="nav-container">
                                    <div class="nav">

                                        <div class="jplist-panel">

                                            <div class="headerow">
                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">
                                                    <div class=""
                                                        data-control-type="sortAirline"
                                                        data-control-name="sortAirline"
                                                        data-control-action="sort"
                                                        data-path=".airlineImage"
                                                        data-order="asc"
                                                        data-type="text">
                                                        Airline <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>
                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortDeptime"
                                                        data-control-name="sortDeptime"
                                                        data-control-action="sort"
                                                        data-path=".deptime"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -37px;">
                                                        Departure <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>
                                                <div class="large-2 medium-2 small-3 columns colorwht abdd srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortArrtime"
                                                        data-control-name="sortArrtime"
                                                        data-control-action="sort"
                                                        data-path=".arrtime"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -43px;">
                                                        Arrival <i class="fa fa-caret-up"></i>
                                                    </div>

                                                </div>
                                                <div class="large-4 medium-4 small-4 columns passenger abdd colorwht  srtarw" onclick="myfunction(this)">

                                                    <div
                                                        class=""
                                                        data-control-type="sortTotdur"
                                                        data-control-name="sortTotdur"
                                                        data-control-action="sort"
                                                        data-path=".totdur"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -85px;">
                                                        Duration <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>

                                                <div class="large-2 medium-2 small-3 columns abdd colorwht srtarw" onclick="myfunction(this)">
                                                    <div
                                                        class=""
                                                        data-control-type="sortCITZ"
                                                        data-control-name="sortCITZ"
                                                        data-control-action="sort"
                                                        data-path=".price"
                                                        data-order="asc"
                                                        data-type="number" style="margin-left: -246px;">
                                                        Fare <i class="fa fa-caret-up"></i>
                                                    </div>
                                                </div>
                                                <div class="clear">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                                <div id="mainDiv">
                                    <div id="divResult">
                                        <div id="divFrom" class="list" style="width: 100%;">
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                <%-- <div class="jplist-no-results jplist-hidden">
                                <div class='clear1'></div>
                                <div class='clear1'></div>
                                <div class='w90 mauto padding1 brdr'>
                                    <div class='clear1'></div>
                                    <div class='clear1'></div>
                                    <span class='vald f20'>Sorry, we could not find a match for your query. Please modify your search.</span> &nbsp;<span onclick='DiplayMsearch(this.id);' class='underlineitalic cursorpointer'>Modify Search</span><div class='clear'></div>
                                </div>
                            </div>--%>
                            </div>
                        </div>
                        <div class="clear">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="clear">
        </div>
        <div id="render">
        </div>
        <div class="clear">
        </div>
    </div>
    <%--<div id="fltselct" class="hide">
        <div class="clear1"></div>
        <div id="totalPay" class="f16">
            Your Selection
            <div class="clear1"></div>
        </div>
        <div id="selctcntnt" class="w70 mauto">
            <div class="clear1">
            </div>
            <div id="fltgo" class="w50 lft">
            </div>
            <div id="fltbk" class="w45 rgt">
            </div>
            <div id="fltbtn">
                <input type="button" value="Book" class="button1 hide" id="FinalBook" />
                <div id="Divproc" class="hide bld">
                    <img alt="Booking In Progress" src="~/Images/loading_bar.gif" />
                </div>
            </div>
        </div>
    </div>--%>
    <%--   </div>--%>
    <div id="waitMessage" style="display: none;">
        <div style="text-align: center; z-index: 1001; font-size: 12px; font-weight: bold; padding: 20px; border-radius: 0px;">
            <%-- <h1 class="text-center" style="font-size: 20px;">PLEASE WAIT</h1>
            <span>Please do not close or refresh this window.</span><br />
            <img alt="loading" width="50" height="50" src="../images/loadingAnim.gif" />
            <br />
            <div id="searchquery" style="color: #004b91; padding-top: 15px">
            </div>--%>
            <div id="loader" style="display:none;">

                <div id="plane">
                    <i>
                        <img src="../Images/icons/146552.png" style="width: 60px;" /></i>
                </div>
                <%--<div id="searchquery" style="color: #fff; padding-top: 138px; font-size: 18px;">--%>
                </div>

            <div class="animation" >
                  
  <svg id="master-artboard" class="animation__cloud--back" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px"><path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)"/></svg>
<svg id="Svg1" class="animation__cloud--middle" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px"><path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)"/></svg>
<div class="animation__plane--shadow"></div>
                
<svg class="animation__plane" xmlns="http://www.w3.org/2000/svg" xmlns:cc="http://creativecommons.org/ns#" xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" xmlns:svg="http://www.w3.org/2000/svg" data-name="svgi-plane" viewBox="0 0 135.67 49.55"><path fill="#fff" stroke="#464646" stroke-linejoin="round" d="M74.663 45.572h-9.106z" class="svgi-plane--stripe3"/><path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M.75 26.719h23.309z" class="svgi-plane--stripe1"/><path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M11.23 31.82h22.654z" class="svgi-plane--stripe2"/><path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 53.47597,24.263013 h 68.97869 c 6.17785,0 12.47074,6.758518 12.40872,8.67006 -0.05,1.537903 -5.43763,7.036166 -11.72452,7.056809 l -59.599872,0.201269 c -9.092727,0.03097 -23.597077,-5.992662 -22.178652,-11.794378 1.160348,-4.74789 7.862358,-4.13376 12.115634,-4.13376 z"/><path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="M 45.243501,24.351777 37.946312,0.952937 h 7.185155 c 15.37061,20.184725 28.402518,23.28324 28.402518,23.28324 0,0 -27.106129,-0.178562 -28.290484,0.1156 z"/><path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 42.699738,18.984597 h 10.627187 c 5.753726,0 5.364609,7.799958 0,7.799958 H 42.998828 c -4.96749,0 -5.574672,-7.799958 -0.29909,-7.799958 z m 33.139939,16.164502 h 29.656893 c 6.62199,0 6.49395,6.577892 0,6.577892 H 75.940707 c -8.658596,0 -8.499549,-6.35598 -0.10103,-6.577892 z m 9.693907,6.664592 h 8.86866 c 4.439332,0 4.309293,7.066099 0,7.066099 h -8.756626 z"/><path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 85.55159,42.447431 c 0,0 -5.282585,0.456211 -6.372912,3.263659 1.335401,2.378073 6.397919,2.528767 6.397919,2.528767 z"/><path fill="#fff" stroke="#464646" stroke-linejoin="round" stroke-width="1" d="m 133.68903,31.07417 h -7.01411 c -1.26938,0 -2.89286,-1.005314 -3.21496,-3.216179 h 7.50225 z"/><ellipse cx="113.564" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="107.56219" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="101.56039" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="95.558594" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="89.556793" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="83.554993" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="77.553192" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="71.551392" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/><ellipse cx="65.549591" cy="29.448534" fill="#fff" stroke="#464646" stroke-linecap="square" stroke-linejoin="round" stroke-width="1" rx="1.3354006" ry="1.6400863"/></svg>
<svg id="Svg2" class="animation__cloud--front" viewBox="0 0 45 18" version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="45px" height="18px"><path d="M 58.88372802734375 825.155517578125 C 16.61685562133789 826.1433715820312 57.925209045410156 781.6769409179688 58.883731842041016 781.3507080078125 C 106.25779724121094 731.152099609375 169.17739868164062 692.9862670898438 226.64694213867188 694.6730346679688 C 227.2968292236328 557.091552734375 389.74322509765625 563.0558471679688 425.166748046875 635.9559326171875 C 534.7359619140625 431.2034912109375 793.6226196289062 599.6361694335938 715.956298828125 741.27392578125 C 820.5570068359375 803.94287109375 789.773193359375 826.9736328125 767.21728515625 825.1555786132812 C 394.85125732421875 825.5911865234375 359.5561218261719 823.805908203125 58.88372802734375 825.155517578125 Z" transform="matrix(0.0573558509349823, 0, 0, 0.056244850158691406, -1.3596858978271484, -29.666284561157227)"/></svg>
        <div class="animation__loader"></div>

              
 </div>

<footer>

    <div id="searchquery" style="color: #fff;  font-size: 18px;z-index:10111111;" >
                </div>

 <%-- <a href="https://www.linkedin.com/in/giovanni-mrqs/" target="_blank">linkedin</a>
  -
  <a href="https://github.com/web-duke" target="_blank">github</a>
  -
  <a href="https://dock.io?r=dukeweb:aaaat3HE" target="_blank">dock.io</a>
  <br>
  <span>buy me a coffee with bitcoin&nbsp;: 1CVRSirjLkKQdY2prdBta2udcoEYCKEwms</span>--%>
</footer>


            </div>
        </div>

       <%-- <div id="divMail">
        <a href="#" class="topopup pop_button1" id="btnFullDetails">Mail All Result</a>
        <a href="#" class="topopup pop_button1" id="btnSendHtml">Mail Selected Result</a>
    </div>--%>
    <div id="backgroundPopup">
    </div>
    <div id="toPopup" class="flight_head">
        <div class="close">
        </div>
        <span class="ecs_
            ">Press Esc to close <span class="arrow"></span></span>
        <div id="popup_content">
            <!--your content start-->
            <table cellpadding="3" cellspacing="3">
                <tr>
                    <td colspan="2">
                        <h4 style="text-align: center; color: #FFFFFF; background-color: #20313f; font-weight: bold; padding-top: 5px; padding-bottom: 5px;">Send Mail</h4>
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;"></td>
                    <td class="textsmall">
                        <input type='radio' name='choices' checked="checked" value='A' />
                        All Result
                         <input type='radio' name='choices' value='S' />Selected Result
                    </td>
                </tr>

                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">From:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtFromMail" name="txtFromMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">To:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtToMail" name="txtToMail" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Subject:
                    </td>
                    <td>
                        <input type="text" class="headmail" id="txtSubj" name="txtSubj" />
                    </td>
                </tr>
                <tr>
                    <td class="textsmall" style="width: 120px; padding-left: 10px;">Message:
                    </td>
                    <td>
                        <textarea id="txtMsg" class="headmail" name="txtMsg" rows="4" cols="20"></textarea>
                    </td>
                </tr>
                <tr>
                    <td style="margin-left: 20px;"></td>
                    <td align="right">
                        <%--<input type="button" class="pop_button" id="btnCancel" name="btnCancel" value="Cancel" />--%>
                        <input type="button" class="buttonfltbk" id="btnSendMail" name="btnSendMail" value="Send Details" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <%--<div id="divabc">&nbsp;</div>--%>
                        <label id="lblMailStatus" style="display: none; color: Red;">
                        </label>
                    </td>
                </tr>
            </table>
        </div>
        <!--your content end-->
    </div>
    <div id="FareBreakupHeder" class="modal" tabindex="-1" role="dialog" aria-labelledby="FareBreakupHederLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="FareBreakupHederLabel">Fare Summary</h5>
                    <button type="button" class="close FareBreakupHederClose" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>

                </div>
                <div class="modal-body" id="FareBreakupHederId" style="padding: 2px 10px">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary FareBreakupHederClose">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div id="ConfmingFlight" class="CfltFare" style="text-align: center; z-index: 1001; background-color: #f9f9f9; font-size: 12px; font-weight: bold; padding: 20px; box-shadow: 0px 1px 5px #000; border: 1px solid #d1d1d1; border-radius: 0px; display: none;">
        
        <div id="Div1" style="display: none;">
        <div style="text-align: center; z-index: 1001; font-size: 12px; font-weight: bold; padding: 20px; border-radius: 0px;">
        <div id="divLoadcf" class="CfltFare1">
        
        </div>
            </div>
            </div>

    </div>
    <div class="clear"></div>
    <a href="#toptop"><span class="toptop" style="position: fixed; bottom: 4px; right: 20px; height: 50px; font-size: 20px; width: 50px; border-radius: 50%; cursor: pointer; padding: 13px 15px; background: rgb(0, 75, 145); color: rgb(255, 255, 255); display: block;"><i class="fa fa-chevron-up" aria-hidden="true"></i></span></a>
    <div class="clear1"></div>
    <input type="hidden" id="hdnMailString" name="hdnMailString" />
    <input type="hidden" id="hdnAllOrSelecte" name="hdnAllOrSelecte" />
    <input type="hidden" id="hdnOnewayOrRound" name="hdnOnewayOrRound" />
    <asp:Literal ID="henAgcDetails" runat="server" Visible="false"></asp:Literal>
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/")%>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.7.1.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/json2.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/JSLINQ.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jplist.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/SortAD.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/TextFilterGroup.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/handleQueryString.js?v=4.9")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/jquery.blockUI.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/async.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/LZCompression.js?y=1.8")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightResultsNew.js?v=1.9")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery.tooltip.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/gridview-readonly-script.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/FlightMailing.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Flight/pako.min.js") %>"></script>
    <%--   <script src="../Scripts/script.js" type="text/javascript"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            $(".abc1").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT1") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc2t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT2") {
                    $(this).toggleClass("bdrss");
                }
            });
            $(".abc3t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT3") {
                    $(this).toggleClass("bdrss");
                }

            });
            $(".abc4t").unbind('click').bind('click', function (e) {

                if (e.target.id != "CheckboxT4") {
                    $(this).toggleClass("bdrss");
                }

            });

        });
    </script>
    <script type="text/javascript">
        jQuery("document").ready(function ($) {
            debugger;
            var nav = $('.nav-container');
            $(window).scroll(function () {
                if ($(this).scrollTop() > 175) {
                    $(".toptop").fadeIn();
                    if ($("#lftdv1").is(":visible") == false) {
                        nav.addClass("f-nav1");
                    }
                    else {
                        nav.addClass("f-nav");
                    }
                } else {
                    $(".toptop").fadeOut();
                    nav.removeClass("f-nav");
                    nav.removeClass("f-nav1");
                }
            });



        });
        //$(document).ready(function () {
        //    debugger;
        //    var vars = [], hash;
        //    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        //    for (var i = 0; i < hashes.length; i++) {
        //        hash = hashes[i].split('=');
        //        vars.push(hash[0]);
        //        vars[hash[0]] = hash[1];
        //    }
        //    if (vars.TripType != "rdbOneWay") {
        //        $('.fltkun').hide();
        //    }
        //});
        //document.onreadystatechange = function () {
        //    debugger;
        //    if (document.readyState === "complete") {
        //        myFunction();
        //    }
        //    else {
        //        window.onload = function () {
        //            myFunction();
        //        };
        //    };
        //}


    </script>
</asp:Content>
