﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="true" CodeFile="CheckCommissionAgent.aspx.cs" Inherits="Report_Admin_CheckCommissionAgent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />

    
    <script type="text/javascript">

        var UrlBase = '<%=ResolveUrl("~/") %>';



    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
        <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Common.js") %>"></script>

    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/Search3_off.js")%>"></script>
    <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <div class="form-group">
                                <label>Prefered Airlines</label>
                                <input type="text" name="txtAirline" value="" id="txtAirline" class="form-control" />
                                <input type="hidden" id="hidtxtAirline" name="hidtxtAirline" value="" />
                            </div></div>
                    <div class="col-md-4" style=" margin-top: 25px;">
                <asp:Button ID="btn_result" runat="server" class="btn btn-danger" Text="Search Result" OnClick="btn_result_Click" />
                <asp:Button ID="btn_export" runat="server" class="btn btn-danger" Text="Export" OnClick="btn_export_Click" />
            </div>
        </div>
            <div>

       
                    <asp:GridView ID="CommGrid" runat="server" AllowPaging="True" AllowSorting="True" OnPageIndexChanging="CommGrid_PageIndexChanging"
                        AutoGenerateColumns="False" CssClass="rtable" GridLines="None" Font-Size="12px" PageSize="10">
                        <Columns>

                            <asp:BoundField HeaderText="AirlineName" DataField="AirlineName"></asp:BoundField>
                            <asp:BoundField HeaderText="Trip Type" DataField="TripType"></asp:BoundField>
                            <asp:BoundField HeaderText="On Basic" DataField="CommisionOnBasic"></asp:BoundField>
                            <asp:BoundField HeaderText="On YQ" DataField="CommissionOnYq"></asp:BoundField>
                            <asp:BoundField HeaderText="On Basic+YQ" DataField="CommisionOnBasicYq"></asp:BoundField>
                            <asp:BoundField HeaderText="Cash Back" DataField="CashBackAmt"></asp:BoundField>

                        </Columns>



                    </asp:GridView>
          
     
        </div>

</asp:Content>

