﻿<%@ Page Title="" Language="VB" MasterPageFile="~/MasterPageForDash.master" AutoEventWireup="false"
    CodeFile="StaffTransaction.aspx.vb" Inherits="Reports_Accounts_StaffTransaction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <%--<link href="../../CSS/style.css" rel="stylesheet" type="text/css" />
    <link href="../../css/main2.css" rel="stylesheet" type="text/css" />
    <link href="../../CSS/StyleSheet.css" rel="stylesheet" type="text/css" />--%>

    <link type="text/css" href="<%=ResolveUrl("~/Styles/jquery-ui-1.8.8.custom.css") %>"
        rel="stylesheet" />
    <link type="text/css" href="<%=ResolveUrl("~/CSS/newcss/main.css")%>"
        rel="stylesheet" />
    <%--<style>

        .msi {
            width:130%!important;
            max-width:130%!important;
        }
    </style>--%>
   


    <div class="container">
        <div class="card-header">
            <div class="col-md-9">
                <h3 style="text-align: center;">Staff Transaction Details</h3>
                <hr />
            </div>
        </div>
        <div class="card-body">

        <div class="col-md-9">
            <div class="row">



                <div class="row">

                    <div class="form-groups  col-md-3 col-xs-12">
                        <label>From</label>
                        <div class="inputWithIcon">

                            <input type="text" name="From" id="From" placeholder="Select Date" class="form-control" readonly="readonly" />
                            <i class="fa fa-calendar fa-lg fa-fw" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="form-groups col-md-3 col-xs-12">
                        <label>To</label>
                        <div class="inputWithIcon">
                            <input type="text" name="To" placeholder="Select Date" id="To" class="form-control" readonly="readonly" />
                            <i class="fa fa-calendar fa-lg fa-fw" aria-hidden="true"></i>
                        </div>
                    </div>

                    <div class="form-groups col-md-3 col-xs-12" id="tr_Cat" runat="server" style="display: none;">
                        <asp:DropDownList ID="ddl_Category" class="form-control" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="form-groups col-md-3 col-xs-12" id="tr_BookingType" runat="server" style="display: none;">
                        <asp:DropDownList ID="ddl_BookingType" class="form-control" runat="server">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-4 col-xs-12">
                        <label>&nbsp;</label>
                        <asp:Button ID="btn_search" runat="server" class="btn btn-danger" Text="Search Result" />
                         <asp:Button ID="btn_export" runat="server" class="btn btn-danger" Text="Export" />
                    </div>
                    
                       
                  
                </div>





                <div class="clear"></div>
                <div class=" " style="padding: 10px 10px 10px 10px;">
                    <div class="col-md-10 col-xs-12 col-md-push-1">
                    </div>
                </div>
            </div>

        </div>
            </div>

    </div>
    <div class="clear1"></div>


    <div class="table-responsive">

        <asp:UpdatePanel ID="up" runat="server">
            <ContentTemplate>
                <asp:GridView ID="Grid_Ledger" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CssClass="rtable" GridLines="Both" Font-Size="12px" PageSize="30">
                    <Columns>
                        <asp:BoundField HeaderText="Id" DataField="Id" />
                        <%--<asp:BoundField HeaderText="OrderId" DataField="OrderId" />--%>
                        <asp:TemplateField HeaderText="Order ID">
                            <ItemTemplate>
                                <a href='PnrSummaryIntl.aspx?OrderId=<%#Eval("OrderId")%> &TransID=' target="_blank" style="font-family: Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; color: #004b91">
                                    <asp:Label ID="OrderID" runat="server" Text='<%#Eval("OrderId")%>'></asp:Label></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="ServiceType" DataField="ServiceType" />
                        <asp:BoundField HeaderText="Module" DataField="Module" />
                        <asp:BoundField HeaderText="TransAmount" DataField="TransAmount" />
                        <asp:BoundField HeaderText="BookedBy" DataField="CreatedBy" />
                        <asp:BoundField HeaderText="Mobile" DataField="Mobile" />
                        <asp:BoundField HeaderText="AgencyId" DataField="AgencyId" />
                        <asp:BoundField HeaderText="Remark" DataField="Remark" />
                        <asp:BoundField HeaderText="CreatedDate" DataField="CreatedDate" />
                        <asp:BoundField HeaderText="Debit" DataField="Debit" />
                        <asp:BoundField HeaderText="Credit" DataField="Credit" />
                        <asp:BoundField HeaderText="Balance" DataField="AvalBal" />
                        <asp:BoundField DataField="AgentMobile" HeaderText="AgentMobile" />
                        <asp:BoundField DataField="Email" HeaderText="Email" />
                        <asp:BoundField HeaderText="StaffId" DataField="StaffId" />

                    </Columns>

                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>


    <hr />
    <div style="text-align: center" class="">

        <asp:Label ID="lblTitle" runat="server"></asp:Label>&nbsp; <span>
            <b>
                <asp:Label ID="lblClosingBal" runat="server"></asp:Label></b></span>

    </div>
    <div class="clear"></div>
    <hr />
    <script type="text/javascript">
        var UrlBase = '<%=ResolveUrl("~/") %>';
    </script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-1.4.4.min.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/jquery-ui-1.8.8.custom.min.js")%>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/Scripts/AgencySearch.js") %>"></script>
    <script type="text/javascript" src="<%=ResolveUrl("~/JS/Distributor.js") %>"></script>
    <style type="text/css">
        .bdrbtm1 {
            border-bottom: 2px solid #ddd;
        }
    </style>
</asp:Content>
