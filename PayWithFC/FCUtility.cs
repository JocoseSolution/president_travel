﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Serialization;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace PayWithFC
{
    public class FCUtility
    {
        public static String Encrypt(String accessToken, String secretKey)
        {
            var plainBytes = Encoding.UTF8.GetBytes(accessToken);
            var keyBytes = new byte[16];
            var secretKeyBytes = Encoding.UTF8.GetBytes(secretKey);
            Array.Copy(secretKeyBytes, keyBytes, Math.Min(keyBytes.Length, secretKeyBytes.Length));

            RijndaelManaged rijndaelManaged = new RijndaelManaged
            {
                Mode = CipherMode.ECB,
                Padding = PaddingMode.PKCS7,
                KeySize = 128,
                BlockSize = 128,
                Key = keyBytes
            };

            byte[] crypt = rijndaelManaged.CreateEncryptor().TransformFinalBlock(plainBytes, 0, plainBytes.Length);
            return GetHexString(crypt);

        }
        public static string Checksum(object obj, String secretKey)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver()
            };

            var json = JsonConvert.SerializeObject(obj, Formatting.None, settings);
            string s = json + secretKey;
            Console.WriteLine(s);
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(s), 0, Encoding.UTF8.GetByteCount(s));
            return GetHexString(crypto);

        }
        public static string GetHexString(byte[] bytes)
        {

            System.Text.StringBuilder str = new System.Text.StringBuilder();
            foreach (byte theByte in bytes)
            {
                str.Append(theByte.ToString("x2"));
            }
            return str.ToString();
        }
        public static Boolean verifyChecksum(String secretKey, object obj, String checksumReceived)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new OrderedContractResolver()
            };
            var json = JsonConvert.SerializeObject(obj, Formatting.None, settings);
            string s = json + secretKey;
            Console.WriteLine(s);
            System.Security.Cryptography.SHA256Managed crypt = new System.Security.Cryptography.SHA256Managed();
            byte[] crypto = crypt.ComputeHash(Encoding.UTF8.GetBytes(s), 0, Encoding.UTF8.GetByteCount(s));
            String checksumCalculated = GetHexString(crypto);
            if (checksumReceived.Equals(checksumCalculated))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

    }

    public class FCTrans
    {
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);//TotalAmount
        SqlCommand cmd;
        SqlDataAdapter adap;
        MerchantPaymentRequest objMPR;
        string calChecksum = "";//string.Empty;
        public string PaymentGatewayReqFreecharge(string orderId, string TId, string IBTrackId, string AgentId, string AgencyName, double TotalAmount, double amount, string BillingName, string BillingAddress, string BillingCity, string BillingState, string BillingZip, string BillingTel, string buyerEmail, string currency, string ServiceType, string IP, string Trip, string PaymentOption)
        {
            string merchantIdentifier = "";// merchantIdentifier.Text;
            String secretKey = "";
            //string orderId = "";// orderId.Text;
            string ProviderUrl = "";
            string returnUrl = "";//returnUrl.Text;
            string FailureUrl = "";
            string CancelUrl = "";
            StringBuilder postHTML = new StringBuilder();
            //string postHTML = "";

            string MERC_UNQ_REF = "";
            string CHANNEL_ID = "";
            string INDUSTRY_TYPE_ID = "";
            string WEBSITE = "";
            string SALT = "";
            string HashSequence = "";
            int flag = 0;
            string txnType = "";//txnType.Text;
            string zpPayOption = "";// zpPayOption.Text;
            string mode = "1";// mode.Text;
            //string currency = "";//currency.Text;
            //double Amount = "";//Convert.ToInt32(amount.Text);
            string ccaRequest = "";
            string strEncRequest = "";
            string merchantIpAddress = "";// merchantIpAddress.Text;
            string loginToken = "";
            string purpose = "1";//purpose.Text;
            string productDescription = "";//productDescription.Text;
            //DateTime Datetime = "";//Convert.ToDateTime(txnDate.Text);
            string allParamValue = null;
            double TransCharges = 0;
            string ChargesType = "";
            double TotalPgCharges = 0;
            //string calChecksum = "";
            string Provider = Convert.ToString(ConfigurationManager.AppSettings["Freecharge"]);

            try
            {
                DataTable pgDT = GetTotalAmountWithPgCharge(PaymentOption, amount, AgentId, orderId);
                if (pgDT != null)
                {
                    if (pgDT.Rows.Count > 0)
                    {
                        TotalAmount = Convert.ToDouble(pgDT.Rows[0]["TotalAmount"]);
                        TotalPgCharges = Convert.ToDouble(pgDT.Rows[0]["TotalPgCharges"]);
                        TransCharges = Convert.ToDouble(pgDT.Rows[0]["Charges"]);
                        ChargesType = Convert.ToString(pgDT.Rows[0]["ChargesType"]);
                    }
                }
                DataTable dt = new DataTable();
                dt = GetPgCredential();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        merchantIdentifier = Convert.ToString(dt.Rows[0]["MerchantID"]);
                        secretKey = Convert.ToString(dt.Rows[0]["MERCHANT_KEY"]);
                        
                        //MERC_UNQ_REF = Convert.ToString(dt.Rows[0]["MERC_UNQ_REF"]);
                        
                        CHANNEL_ID = Convert.ToString(dt.Rows[0]["CHANNEL_ID"]);
                        
                        //INDUSTRY_TYPE_ID = Convert.ToString(dt.Rows[0]["INDUSTRY_TYPE_ID"]);
                        //WEBSITE = Convert.ToString(dt.Rows[0]["WEBSITE"]);
                        
                        ProviderUrl = Convert.ToString(dt.Rows[0]["ProviderUrl"]);
                        FailureUrl = Convert.ToString(dt.Rows[0]["FailureUrl"]);
                        returnUrl = Convert.ToString(dt.Rows[0]["SuccessUrl"]);

                        //CancelUrl = Convert.ToString(dt.Rows[0]["CancelUrl"]); 
                        //txnType = Convert.ToString(dt.Rows[0]["Txntype"]);
                        //zpPayOption = Convert.ToString(dt.Rows[0]["Zppayoption"]);
                        //HashSequence = Convert.ToString(dt.Rows[0]["HashSequence"]);

                        //"amount=10000&buyerEmail=admin@RWT.com&currency=INR&merchantIdentifier=a4f55c7c338d41d9aaefc1fc66b8e934&merchantIpAddress=127.1.0.1&mode=1&orderId=ZPLive1547729564413&purpose=1&txnType=1&zpPayOption=1&"

                        //string Parameters = "amount=" + TotalAmount  + "&buyerEmail=" + buyerEmail + "&currency=" + currency + "&merchantIdentifier=" + merchantIdentifier + "&merchantIpAddress=" + IP + "&mode=" + mode + "&orderId=" + orderId + "&purpose=" + purpose + "&returnUrl=" + returnUrl + "&txnType=" + txnType + "&zpPayOption=" + zpPayOption + "&";
                        //loginToken = FCUtility.Encrypt("qwerty", secretKey);                       
                        //ccaRequest = "" + merchantIdentifier + "|" + MERC_UNQ_REF + "|" + secretKey + "|" + CHANNEL_ID + "|" + INDUSTRY_TYPE_ID + "|" + WEBSITE + "|" + orderId + "|" + Convert.ToDecimal(TotalAmount).ToString("g29") + "|" + ServiceType + "|" + BillingName + "|" + buyerEmail + "|||||||||||" + SALT + "";
                        
                        objMPR = new MerchantPaymentRequest();
                        objMPR.amount = Convert.ToString(TotalAmount);
                        objMPR.merchantId = merchantIdentifier;
                        objMPR.merchantTxnId = orderId;
                        objMPR.channel = CHANNEL_ID;
                        objMPR.furl = FailureUrl;
                        objMPR.surl = returnUrl;

                        calChecksum = PayWithFC.FCUtility.Checksum(objMPR, secretKey); //ChecksumCalculator.calculateChecksum(secretKey, Parameters);
                        string s = "";
                        strEncRequest = calChecksum;
                        try
                        {
                            var settings = new JsonSerializerSettings()
                            {
                                ContractResolver = new OrderedContractResolver()
                            };

                            var json = JsonConvert.SerializeObject(objMPR, Formatting.None, settings);
                             s = json + secretKey;
                            ccaRequest = json + secretKey;                            
                        }
                        catch(Exception ex){
                            int c = InsertExceptionLog("FCWallet", "FCTrans", "FCGatewayReq", "RequestString-JsonConvert", ex.Message, ex.StackTrace);
                        }
                        postHTML.Append("<html>");
                        postHTML.Append("<head>");
                        postHTML.Append("<title>Merchant Check Out Page</title>");
                        postHTML.Append("</head>");
                        postHTML.Append("<body>");
                        postHTML.Append("<center><h1>Please do not refresh this page...</h1></center>");
                        postHTML.Append("<form enctype='application/json' method='post' action='" + ProviderUrl + "'" + orderId + " name='f1'>");//" + orderId + " name='f1'
                        postHTML.Append("<table border='1'>");
                        postHTML.Append("<tbody>");

                        postHTML.Append("<div>");
                        postHTML.Append("<label>");
                        postHTML.Append("<input id='merchantId' type='text'  name= 'merchantId' value='" + merchantIdentifier + "'>");
                        postHTML.Append("</label>");
                        postHTML.Append("</div>");

                        postHTML.Append("<div>");
                        postHTML.Append("<label>");
                        postHTML.Append("<input id='merchantTxnId' type='text' name= 'merchantTxnId' value='" + orderId + "'>");
                        postHTML.Append("</label>");
                        postHTML.Append("</div>");
                        postHTML.Append("<div>");
                        postHTML.Append("<label>");
                        postHTML.Append("<input id='amount' type='text' name= 'amount' value='" + TotalAmount + "'>");
                        postHTML.Append("</label>");
                        postHTML.Append("</div>");
                        postHTML.Append("<div>");
                        postHTML.Append("<label>");
                        postHTML.Append("<input id='furl' type='text' name= 'furl' value='" + FailureUrl + "'>");
                        postHTML.Append("</label>");
                        postHTML.Append("</div>");
                        postHTML.Append("<div>");
                        postHTML.Append("<label>");
                        postHTML.Append("<input id='surl' type='text' name= 'surl' value='" + returnUrl + "'>");
                        postHTML.Append("</label>");
                        postHTML.Append("</div>");
                        postHTML.Append("<div>");
                        postHTML.Append("<label>");
                        postHTML.Append("<input id='channel' type='text' name= 'channel' value='" + CHANNEL_ID + "'>");
                        postHTML.Append("</label>");
                        postHTML.Append("</div>");
                        //postHTML.Append("<div>");
                        postHTML.Append("<label>");
                        postHTML.Append("<input id='checksum' type='text' name= 'checksum' value='" + calChecksum + "'>");
                        postHTML.Append("</label>");
                        #region OTHLevel
                        //postHTML += "</div>";
                        //postHTML += "<div>";
                        //postHTML += "<label>";
                        //postHTML += "<input type='hidden' name= 'currency' value='" + currency + "'>";
                        //postHTML += "</label>";
                        //postHTML += "</div>";
                        //postHTML += "<div>";
                        //postHTML += "<label>";
                        //postHTML += "<input type='hidden' name= 'merchantIpAddress' value='" + IP + "'>";
                        //postHTML += "</label>";
                        //postHTML += "</div>";
                        //postHTML += "<div>";
                        //postHTML += "<label>";
                        //postHTML += "<input type='hidden' name= 'purpose' value='" + purpose + "'>";//CancelUrl
                        //postHTML += "</label>";
                        //postHTML += "</div>";
                        //postHTML += "<div>";
                        //postHTML += "<label>";
                        //postHTML += "<input type='hidden' name= 'returnUrl' value='" + returnUrl + "'>";
                        //postHTML += "</label>";
                        //postHTML += "</div>";
                        //postHTML += "<div>";
                        //postHTML += "<label>";
                        //postHTML += "<input type='hidden' name='checksum' value='" + strEncRequest + "'>";
                        //postHTML += "</label>";
                        //postHTML += "</div>";
                        #endregion
                        postHTML.Append("</tbody>");
                        postHTML.Append("</table>");
                        postHTML.Append("<script type='text/javascript'>");
                        postHTML.Append("document.f1.submit();");
                        postHTML.Append("</script>");
                        postHTML.Append("</form>");
                        postHTML.Append("</body>");
                        postHTML.Append("</html>");

                        flag = InsertPaymentRequestDetails(orderId, TId, IBTrackId, BillingName, "Freecharge", buyerEmail, BillingTel, BillingAddress, TotalAmount, amount, AgentId, AgencyName, IP, ccaRequest, ServiceType, strEncRequest, Trip, TotalPgCharges, TransCharges, ChargesType, postHTML.ToString());
                    }
                    else
                    {
                    }
                }
                if (flag > 0)
                {
                    postHTML.Append("~yes");
                }
                else
                {
                    postHTML.Append("~no");
                }
            }
            catch (Exception ex)
            {
                postHTML.Append("~no" + ex.Message);
                int insert = InsertExceptionLog("FCWallet", "FCTrans", "FCGatewayReq", "insert", ex.Message, ex.StackTrace);
                return postHTML.ToString();
            }

            return postHTML.ToString();
        }

        public DataTable GetTotalAmountWithPgCharge(string PaymentMode, double OriginalAmount, string UserId, string TrackId)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            DataTable dt = new DataTable();
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter("SpInsertPaymentDetails", con);
                adp.SelectCommand.CommandType = CommandType.StoredProcedure;
                adp.SelectCommand.Parameters.AddWithValue("@PaymentMode", PaymentMode);
                adp.SelectCommand.Parameters.AddWithValue("@OriginalAmount", OriginalAmount);
                adp.SelectCommand.Parameters.AddWithValue("@AgentId", UserId);
                adp.SelectCommand.Parameters.AddWithValue("@TrackId", TrackId);
                adp.SelectCommand.Parameters.AddWithValue("@Action", "PgTotalAmount");
                adp.Fill(dt);
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("FCWallet", "FCTrans", "GetTotalAmountWithPgCharge", "SELECT", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
            }
            return dt;
        }
        public int InsertExceptionLog(string Module, string ClassName, string MethodName, string ErrorCode, string ExMessage, string ExStackTrace)
        {
            int temp = 0;
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("SpInsertExceptionLog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Module", Module);
                cmd.Parameters.AddWithValue("@ClassName", ClassName);
                cmd.Parameters.AddWithValue("@MethodName", MethodName);
                cmd.Parameters.AddWithValue("@ErrorCode", ErrorCode);
                cmd.Parameters.AddWithValue("@ExMessage", ExMessage);
                cmd.Parameters.AddWithValue("@ExStackTrace ", ExStackTrace);
                temp = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close();

            }
            return temp;
        }
        public DataTable GetPgCredential()
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            DataTable dt = new DataTable();
            string Provider = Convert.ToString(ConfigurationManager.AppSettings["Freecharge"]);

            try
            {
                con.Open();
                SqlCommand cmd = new SqlCommand("Sp_Get_PgCredentials", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Pvd", Provider);
                SqlDataReader reader = cmd.ExecuteReader();
                dt.Load(reader);
                //adap.Fill(dt);
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("FCPayWallet", "FCTrans", "FCGetPgCredential", "select", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();

            }
            return dt;
        }
        public int InsertPaymentRequestDetails(string TrackId, string TId, string IBTrackId, string Name, string PaymentGateway, string Email, string Mobile, string Address, double TotalAmount, double OriginalAmount, string AgentId, string AgencyName, string Ip, string PgRequest, string ServiceType, string EncRequest, string Trip, double TotalPgCharges, double TransCharges, string ChargesType, string PostHtml)
        {
            int temp = 0;
            try
            {
                //Name, TrackId, PaymentGateway, Email, Mobile, Address, Amount, OriginalAmount, AgentId, AgencyName, Status, Ip
                con.Open();
                SqlCommand cmd = new SqlCommand("SpInsertPaymentDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TrackId", TrackId);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@PaymentGateway", PaymentGateway);
                cmd.Parameters.AddWithValue("@Email", Email);
                cmd.Parameters.AddWithValue("@Mobile", Mobile);
                cmd.Parameters.AddWithValue("@Address", Address);
                cmd.Parameters.AddWithValue("@Amount", TotalAmount);
                cmd.Parameters.AddWithValue("@OriginalAmount", OriginalAmount);
                cmd.Parameters.AddWithValue("@AgentId", AgentId);
                cmd.Parameters.AddWithValue("@AgencyName", AgencyName);
                cmd.Parameters.AddWithValue("@Status", "Requested");
                cmd.Parameters.AddWithValue("@Ip", Ip);
                cmd.Parameters.AddWithValue("@Action", "insert");
                cmd.Parameters.AddWithValue("@PgRequest", PgRequest);
                cmd.Parameters.AddWithValue("@EncRequest", EncRequest);
                cmd.Parameters.AddWithValue("@TId", TId);
                cmd.Parameters.AddWithValue("@IBTrackId", IBTrackId);
                cmd.Parameters.AddWithValue("@ServiceType", ServiceType);
                cmd.Parameters.AddWithValue("@Trip", Trip);
                // add new param 07 sept 2016
                cmd.Parameters.AddWithValue("@PgTotalCharges", TotalPgCharges);
                cmd.Parameters.AddWithValue("@PgTransCharges", TransCharges);
                cmd.Parameters.AddWithValue("@PgChargesType", ChargesType);
                cmd.Parameters.AddWithValue("@PostForm", PostHtml);

                temp = cmd.ExecuteNonQuery();
                con.Close();



            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("ZaakPaymWallet", "MobikwikTrans", "InsertPaymentRequestDetails", "insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();

            }
            return temp;
        }
        public int UpdateCreditLimit(string AgentId, string TrackId, string BookingType, string IpAddress)
        {
            int temp = 0;
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            try
            {
                con.Open();
                cmd = new SqlCommand("AddCreditLimitByPaymentGateway", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@AgentID", AgentId);
                cmd.Parameters.AddWithValue("@InvoiceNo", TrackId);
                cmd.Parameters.AddWithValue("@BookingType", BookingType);
                cmd.Parameters.AddWithValue("@IPAddress", IpAddress);
                //cmd.Parameters.AddWithValue("@ActionType", "PGCREDITLIMIT");
                temp = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdateCreditLimit", "insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }
        public DataSet GetPaymentDetails(string TrackId, string AgentID)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            DataSet ds = new DataSet();
            try
            {
                SqlDataAdapter adp = new SqlDataAdapter("SpInsertPaymentDetails", con);
                adp.SelectCommand.CommandType = CommandType.StoredProcedure;
                adp.SelectCommand.Parameters.AddWithValue("@TrackId", TrackId);
                adp.SelectCommand.Parameters.AddWithValue("@AgentId", AgentID);
                adp.SelectCommand.Parameters.AddWithValue("@Action", "GetDetails");
                adp.Fill(ds);
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "GetPaymentDetails", "SELECT", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
            }
            return ds;
        }
        public string UpdatePaymentResponseDetails(string AgentId, string PgResponse)
        {
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            int temp = 0;
            string OrderId = string.Empty;
            string TxnId = string.Empty;
            string PaymentId = string.Empty;
            string Status = string.Empty;
            string BankRefNo = string.Empty;
            string ErrorText = string.Empty;
            string ResponseCode = string.Empty;
            string PaymentMode = string.Empty;
            string CardType = string.Empty;
            string IssuingBank = string.Empty;
            string CardName = string.Empty;
            string DoRedirect = string.Empty;
            string CardNumber = string.Empty;
            string PaymentMethod = string.Empty;
            string UnmappedStatus = string.Empty;
            string PgAmount = "0.0";
            string DiscountValue = "0.0";
            string MerAamount = "0.0";
            string msg = "no~" + OrderId;
            string ApiRequest = string.Empty;
            string ApiResponse = string.Empty;
            string ApiStatus = string.Empty;
            string ApiEncryptRequest = string.Empty;
            string CardHashedId = "";
            string ReturnCheckSum = "";
            string authCode = "";
            Boolean isChecksumValid = false;
            //string OfferType = "";
            //string OfferCode = "";            
            //string pgResponse = string.Empty;
            //string apiStatus = string.Empty;           
            //Status result = new Status();

            try
            {

                //DataTable dt = new DataTable();
                //dt = GetPgCredential();
                //if (dt != null)
                //{
                //    workingKey = Convert.ToString(dt.Rows[0]["MERCHANT_KEY"]);
                //}

                #region parse FCPay Response

                // string Parameters = "amount=" + amount + "&buyerEmail=" + buyerEmail + "&currency=" + currency + "&merchantIdentifier=" + merchantIdentifier + "&merchantIpAddress=" + IP + "&mode=" + mode + "&orderId=" + orderId + "&purpose=" + purpose + "&returnUrl=" + returnUrl + "&txnType=" + txnType + "&zpPayOption=" + zpPayOption + "&";

                ////ccaRequest = "" + merchantIdentifier + "|" + MERC_UNQ_REF + "|" + secretKey + "|" + CHANNEL_ID + "|" + INDUSTRY_TYPE_ID + "|" + WEBSITE + "|" + orderId + "|" + Convert.ToDecimal(TotalAmount).ToString("g29") + "|" + ServiceType + "|" + BillingName + "|" + buyerEmail + "|||||||||||" + SALT + "";
                // strEncRequest = ChecksumCalculator.calculateChecksum(secretKey, Parameters);

                System.Collections.Specialized.NameValueCollection Params = new System.Collections.Specialized.NameValueCollection();
                string[] segments = PgResponse.Split('&');

                foreach (string seg in segments)
                {
                    string[] parts = seg.Split('=');
                    if (parts.Length > 0)
                    {
                        string Key = parts[0].Trim();
                        string Value = parts[1].Trim();
                        if (Key == "merchantTxnId")
                            OrderId = parts[1].Trim();

                        if (Key == "status")
                            Status = parts[1].Trim();
                        if (Key == "txnId")
                            TxnId = parts[1].Trim();
                        if (Key == "authCode")
                            authCode = parts[1].Trim();

                        if (Key == "amount")
                            PgAmount = parts[1].Trim();

                        if (Key == "authCode")
                            CardHashedId = parts[1].Trim();
                        if (Key == "checksum")
                            ReturnCheckSum = parts[1].Trim();

                        Params.Add(Key, Value);
                    }
                }
                #endregion

                #region Cross check of payment status

                try
                {

                    DataTable dt = new DataTable();
                    dt = GetPgCredential();
                    if (dt != null)
                    {
                        if (dt.Rows.Count > 0)
                        {
                            string Url = Convert.ToString(dt.Rows[0]["WebServiceUrl"]);//"https://test.payu.in/merchant/postservice.php?form=2";
                            string MID = Convert.ToString(dt.Rows[0]["MerchantID"]);
                            string salt = Convert.ToString(dt.Rows[0]["MERCHANT_PSWD"]);//"eCwWELxi";
                            string key = Convert.ToString(dt.Rows[0]["MERCHANT_KEY"]);//"gtKFFx";
                            string TxnType = "CUSTOMER_PAYMENT";
                            string method = "'" + MID + "'" + OrderId + "'" + TxnType + "'";
                            MerchantPaymentResponse objMPRes = new MerchantPaymentResponse();
                            objMPRes.merchantId = MID;
                            objMPRes.txnType = "CUSTOMER_PAYMENT";
                            objMPRes.merchantTxnId = OrderId;
                            
                            string CalChecksum = PayWithFC.FCUtility.Checksum(objMPRes, key);
                            Url += MID + "&merchantTxnId=" + OrderId + "&txnType=CUSTOMER_PAYMENT" + "&checksum=" + CalChecksum;
                            if (ReturnCheckSum != null)
                            {
                                isChecksumValid = PayWithFC.FCUtility.verifyChecksum(key, objMPRes, CalChecksum);
                            }
                            #region XMLRequestResponse
                            if (isChecksumValid)
                            {
                                ApiRequest += "<form method='get' action='" + Url + " name='f1'>";//" + orderId + " name='f1'
                                ApiRequest += "<table border='1'>";
                                ApiRequest += "<tbody>";
                                ApiRequest += "<input type='hidden' name= 'merchantId' value='" + MID + "'>";
                                ApiRequest += "<input type='hidden' name= 'amount' value='" + PgAmount + "'>";
                                ApiRequest += "<input type='hidden' name= 'merchantTxnId' value='" + OrderId + "'>";
                                ApiRequest += "<input type='hidden' name= 'txnType' value='CUSTOMER_PAYMENT'>";
                                //ApiRequest += "<input type='hidden' name= 'txnId' value='" + TxnId + "'>";
                                ApiRequest += "<input type='hidden' name='checksum' value='" + CalChecksum + "'>";
                                ApiRequest += "</tbody>";
                                ApiRequest += "</table>";
                                ApiRequest += "<script type='text/javascript'>";
                                ApiRequest += "document.f1.submit();";
                                ApiRequest += "</script>";
                                ApiRequest += "</form>";
                                #endregion
                                string ToHash = MID + "|" + OrderId + "|" + TxnType + "|" + salt + "|" + "|" + key;
                                string Hashed = Generatehash512(ToHash);
                                srvFCG objPg = new srvFCG();
                                string response = objPg.GetPostReqResPayU(ApiRequest, Url, ToHash, Hashed, OrderId, method, "GET");
                                ApiEncryptRequest = Hashed;
                                ApiResponse = response;
                                Newtonsoft.Json.Linq.JObject account = Newtonsoft.Json.Linq.JObject.Parse(ApiResponse);
                                ApiStatus = (string)account.SelectToken("status");

                            }
                        }
                    }
                }
                catch (Exception expg)
                {
                    int insert = InsertExceptionLog("FCPaymentGateway", "FCPaymentGateway", "UpdatePaymentResponseDetails-GetPostReqResPayU", "GetApiRequest", expg.Message, expg.StackTrace);
                }
                #endregion
                if (Status == "COMPLETED")
                {
                    Status = "success";
                }
                #region Update PG Response
                con.Open();

                cmd = new SqlCommand("SpInsertPaymentDetails", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@TrackId", OrderId);
                cmd.Parameters.AddWithValue("@PaymentId", OrderId);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@ResponseMessage", Status);
                cmd.Parameters.AddWithValue("@ResponseCode", authCode);
                cmd.Parameters.AddWithValue("@ErrorText", ErrorText);
                cmd.Parameters.AddWithValue("@PgResponse", PgResponse);
                cmd.Parameters.AddWithValue("@BankRefNo", TxnId);
                cmd.Parameters.AddWithValue("@PgAmount", Convert.ToDouble(PgAmount));
                cmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);
                cmd.Parameters.AddWithValue("@CardName", CardName);
                cmd.Parameters.AddWithValue("@DiscountValue", Convert.ToDouble(string.IsNullOrEmpty(DiscountValue) ? "0.0" : DiscountValue));
                cmd.Parameters.AddWithValue("@MerAamount", Convert.ToDouble(string.IsNullOrEmpty(MerAamount) ? "0.0" : MerAamount));
                cmd.Parameters.AddWithValue("@CardType", CardType);
                cmd.Parameters.AddWithValue("@IssuingBank", IssuingBank);
                cmd.Parameters.AddWithValue("@CardNumber", CardNumber);
                cmd.Parameters.AddWithValue("@UnmappedStatus", Status);
                cmd.Parameters.AddWithValue("@TId", TxnId);
                //cmd.Parameters.AddWithValue("@OfferCode", OfferCode);
                cmd.Parameters.AddWithValue("@ApiRequest", ApiRequest);
                cmd.Parameters.AddWithValue("@ApiResponse", ApiResponse);
                cmd.Parameters.AddWithValue("@ApiStatus", ApiStatus);
                cmd.Parameters.AddWithValue("@ApiEncryptRequest", ApiEncryptRequest);
                cmd.Parameters.AddWithValue("@Action", "update");
                temp = cmd.ExecuteNonQuery();
                #endregion

            }

            catch (Exception ex)
            {
                if (temp > 0)
                {
                    msg = "yes~" + OrderId;
                }
                else
                {
                    msg = "no~" + OrderId;
                }
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdatePaymentResponseDetails- Update PG Response", "insert", ex.Message, ex.StackTrace);
                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightBooking");
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            if (temp > 0)
            {
                msg = "yes~" + OrderId;
            }
            return msg;                     //OrderId,ApiName,ToHash,Hashed,Request,Response,Status,ExMessage,ExStackTrace
        }
        public int InsertWebServiceLog(string OrderId, string ApiName, string ToHash, string Hashed, string Request, string Response, string Status, string ExMessage, string ExStackTrace)
        {
            int temp = 0;
            con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertPgWebserviceLog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@OrderId", OrderId);
                cmd.Parameters.AddWithValue("@ApiName", ApiName);
                cmd.Parameters.AddWithValue("@ToHash", ToHash);
                cmd.Parameters.AddWithValue("@Hashed", Hashed);
                cmd.Parameters.AddWithValue("@Request", Request);
                cmd.Parameters.AddWithValue("@Response", Response);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@ExMessage", ExMessage);
                cmd.Parameters.AddWithValue("@ExStackTrace", ExStackTrace);
                cmd.Parameters.AddWithValue("@Action", "INSERT");
                temp = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "InsertWebServiceLog", "insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }
        public string Generatehash512(string text)
        {

            byte[] message = Encoding.UTF8.GetBytes(text);

            UnicodeEncoding UE = new UnicodeEncoding();
            byte[] hashValue;
            SHA512Managed hashString = new SHA512Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            return hex;

        }
    }
    public class OrderedContractResolver : DefaultContractResolver
    {
        protected override System.Collections.Generic.IList<JsonProperty> CreateProperties(System.Type type, MemberSerialization memberSerialization)
        {
            return base.CreateProperties(type, memberSerialization).OrderBy(p => p.PropertyName).ToList();
        }
    }
    public class ParamSanitizer
    {
        public static string pram = "";
        public static string sanitizeParam(string param)
        {
            String ret = null;
            if (param == null)
                return null;

            ret = param.Replace("[>><>(){}?&* ~`!#$%^=+|\\:'\";,\\x5D\\x5B]+", " ");

            return ret;
        }
        public static String SanitizeURLParam(String url)
        {

            if (url == null)
                return "";

            Match match = Regex.Match(url, "^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", RegexOptions.IgnoreCase);

            if (match.Success)

                return url;
            else
                return "";

        }
    }
    public class MerchantPaymentRequest
    {
        public string merchantTxnId { get; set; }
        public string amount { get; set; }
        public string furl { get; set; }
        public string surl { get; set; }
        public string channel { get; set; }
        public string merchantId { get; set; }
        //public string customerName { get; set; }
        //public string productInfo { get; set; }
        //public string email { get; set; }
        //public string mobile { get; set; }
        //public string checksum { get; set; }
        //public string metadata { get; set; }
        //public string currency { get; set; }

    }
    public class MerchantPaymentResponse
    {
        //public string authCode { get; set; }
        //public string status { get; set; }
        public string merchantTxnId { get; set; }
        //public string amount { get; set; }
        //public string txnId { get; set; }
        public string merchantId { get; set; }
        public string txnType { get; set; }
    }
}
