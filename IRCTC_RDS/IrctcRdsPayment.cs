﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace IRCTC_RDS
{
    public class IrctcRdsPayment
    {       
        SqlCommand cmd;
        SqlDataAdapter adap;
        SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        #region Parse IRCTC RDS Req
        public string ParseRdsRequest(string EncryptedReqRDS, string DecryptedReqRDS, string IPAddress, string CreatedBy)
        {
           
            int flag = 0;
            string msg = string.Empty;
            string merchantCode = string.Empty;
            string reservationId = string.Empty;
            string txnAmount = "0.0";
            string currencyType = string.Empty;
            string appCode = string.Empty;
            string pymtMode = string.Empty;
            string txnDate = string.Empty;
            string securityId = string.Empty;
            string RU = string.Empty;
            string checkSum = string.Empty;

            string RandomNo = DateTime.Now.ToString("yyyyMMddHHmmssffffff");
            //string ReferenceNo = "FRD" + RandomNo.Substring(3, 17);   //20 digit random         
            string ReferenceNo = "FR" + RandomNo.Substring(7, 13); //15 digit random  
            bool CheckSumFlag = false;
            try
            {
                #region Parse Request from string
                System.Collections.Specialized.NameValueCollection Params = new System.Collections.Specialized.NameValueCollection();
                string[] segments = DecryptedReqRDS.Split('|');
                foreach (string seg in segments)
                {
                    string[] parts = seg.Split('=');
                    if (parts.Length > 0)
                    {
                        string Key = parts[0].Trim();
                        string Value = parts[1].Trim();
                        if (Key == "merchantCode")
                            merchantCode = parts[1].Trim();

                        if (Key == "reservationId")
                            reservationId = parts[1].Trim();

                        if (Key == "txnAmount")
                            txnAmount = parts[1].Trim();

                        if (Key == "currencyType")
                            currencyType = parts[1].Trim();

                        if (Key == "appCode")
                            appCode = parts[1].Trim();

                        if (Key == "pymtMode")
                            pymtMode = parts[1].Trim();

                        if (Key == "txnDate")
                            txnDate = parts[1].Trim();

                        if (Key == "securityId")
                            securityId = parts[1].Trim();

                        if (Key == "RU")
                            RU = parts[1].Trim();

                        if (Key == "checkSum")
                            checkSum = parts[1].Trim();


                        Params.Add(Key, Value);

                    }
                }
                #endregion
                string LogType = "";
                string LogRemark = "";
                #region Check Hash
                string PaymentRequest = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|txnAmount=" + txnAmount + "|currencyType=" + currencyType + "|appCode=" + appCode + "|pymtMode=" + pymtMode + "|txnDate=" + txnDate + "|securityId=" + securityId + "|RU=" + RU;
                string EncriptPaymentRequest = sha256(PaymentRequest).ToUpper();

                if (EncriptPaymentRequest == checkSum)
                {
                    CheckSumFlag = true;
                    LogRemark = "checkSum hash match";
                }
                else
                {
                    LogRemark = "checkSum hash not match";
                }

                //CheckSumFlag = true;
                //LogRemark = "checkSum hash match";
                #endregion

                flag = InsertIrctcRdsRequest(ReferenceNo, merchantCode, reservationId, txnAmount, currencyType, appCode, pymtMode, txnDate, securityId, RU, checkSum, IPAddress, EncryptedReqRDS, DecryptedReqRDS, LogType, LogRemark, CreatedBy);
                if (flag > 1)
                {
                    if (CheckSumFlag == true)
                    {
                        msg = "yes~" + ReferenceNo + "~" + merchantCode + "~" + reservationId + "~" + CheckSumFlag + "~" + txnAmount+"~"+RU;
                    }
                    else
                    {
                        msg = "yes~" + ReferenceNo + "~" + merchantCode + "~" + reservationId + "~" + CheckSumFlag + "~" + txnAmount + "~" + RU;
                    }
                }
                else
                {
                    msg = "no~" + ReferenceNo + "~" + merchantCode + "~" + reservationId + "~" + CheckSumFlag + "~" + txnAmount + "~" + RU;
                }
            }
            catch (Exception ex)
            {
                msg = "no~" + ReferenceNo + "~" + merchantCode + "~" + reservationId + "~" + CheckSumFlag + "~" + txnAmount + "~" + RU;
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "ParseRdsRequest", "ParseInsert", ex.Message, ex.StackTrace);
            }
            return msg;
        }



        public int InsertIrctcRdsRequest(string ReferenceNo, string MerchantCode, string ReservationId, string TxnAmount, string CurrencyType, string AppCode, string PymtMode, string TxnDate, string SecurityId, string RU, string CheckSum, string IPAddress, string EncryptedReq, string DecryptedReq, string LogType, string LogRemark, string CreatedBy)
        {
            string ActionType = "insert";
            int temp = 0;
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                cmd.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                cmd.Parameters.AddWithValue("@ReservationId", ReservationId);
                cmd.Parameters.AddWithValue("@TxnAmount", TxnAmount);
                cmd.Parameters.AddWithValue("@CurrencyType", CurrencyType);
                cmd.Parameters.AddWithValue("@AppCode", AppCode);
                cmd.Parameters.AddWithValue("@PymtMode", PymtMode);
                cmd.Parameters.AddWithValue("@TxnDate", TxnDate);
                cmd.Parameters.AddWithValue("@SecurityId", SecurityId);
                cmd.Parameters.AddWithValue("@RU", RU);
                cmd.Parameters.AddWithValue("@CheckSum", CheckSum);               
                #region Use for Log Table
                cmd.Parameters.AddWithValue("@EncryptedReq", EncryptedReq);
                cmd.Parameters.AddWithValue("@DecryptedReq", DecryptedReq);
                cmd.Parameters.AddWithValue("@Type", LogType);
                cmd.Parameters.AddWithValue("@Remark", LogRemark);
                cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                #endregion
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                temp = cmd.ExecuteNonQuery();
                con.Close();
                string msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);
            }
            catch (Exception ex)
            {
                //int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdateCreditLimit", "insert", ex.Message, ex.StackTrace);
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "InsertIrctcRdsRequest", "Insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }

        public int InsertIrctcRdsLog(string ReferenceNo, string MerchantCode, string ReservationId, string EncryptedReq, string DecryptedReq, string Type, string Remark, string IPAddress, string CreatedBy, string ActionType)
        {           
            ActionType = "LOGREQINS";
            int temp = 0;
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                cmd.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                cmd.Parameters.AddWithValue("@ReservationId", ReservationId);
                cmd.Parameters.AddWithValue("@EncryptedReq", EncryptedReq);
                cmd.Parameters.AddWithValue("@DecryptedReq", DecryptedReq);
                cmd.Parameters.AddWithValue("@Type", Type);
                cmd.Parameters.AddWithValue("@Remark", Remark);
                cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                temp = cmd.ExecuteNonQuery();
                con.Close();
                string msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);
            }
            catch (Exception ex)
            {
                //int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdateCreditLimit", "insert", ex.Message, ex.StackTrace);
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "InsertIrctcRdsLog", "Insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }

        public string CheckUserBalanceAndDeduct(double TxnAmount, string UserId, string ReferenceNo, string MerchantCode, string ReservationId, string IPAddress, string CreatedBy)
        {
            
            string BankTxnId = ReferenceNo;
            string ActionType = "DEDUCTAMT";
            int temp = 0;
            string msgout = string.Empty;
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@BankTxnId", BankTxnId);
                cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                cmd.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                cmd.Parameters.AddWithValue("@ReservationId", ReservationId);
                cmd.Parameters.AddWithValue("@TxnAmount", TxnAmount);
                cmd.Parameters.AddWithValue("@Type", "");
                cmd.Parameters.AddWithValue("@Remark", "");
                cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                temp = cmd.ExecuteNonQuery();
                con.Close();
                msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);                
            }
            catch (Exception ex)
            {
                //int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdateCreditLimit", "insert", ex.Message, ex.StackTrace);
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "CheckUserBalanceAndDeduct", "Insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }



            return msgout;
        }
        public string CreatePaymentResponse(double TxnAmountTotal, string UserId, string ReferenceNo, string MerchantCode, string ReservationId, string BankTxnId, string IPAddress, string CreatedBy)
        {
            string reservationId = "";
            string bankTxnId = "";
            string txnAmount = "";
            string status = "";
            string statusDesc = "";
            string ReturnUrl = "";
            string EncryptedRes = "";
            string DecryptedRes = "";
            string ResponseStatus = "";
            string ResponseRemark = "";
            string postHtml = "";
            try { 
            DataTable dt = GetRdsPaymentDetails(UserId, ReferenceNo, MerchantCode, ReservationId, BankTxnId);
            if (dt != null && dt.Rows.Count > 0)
            {
                reservationId = Convert.ToString(dt.Rows[0]["ReservationId"]);
                bankTxnId = Convert.ToString(dt.Rows[0]["BankTxnId"]);
                txnAmount = Convert.ToDouble(dt.Rows[0]["TxnAmount"]).ToString("0.00"); //Convert.ToString( Convert.ToInt32(dt.Rows[0]["TxnAmount"]));
                status = Convert.ToString(dt.Rows[0]["StatusFlag"]);
                statusDesc = Convert.ToString(dt.Rows[0]["Status"]);
                ReturnUrl = Convert.ToString(dt.Rows[0]["ReturnUrl"]);               

                //string strRes = "resrvationId=" + ReservationId + "|bankTxnId= " + BankTxnId + "|txnAmount=" + TxnAmount + "| status=0|statusDesc=Success";
                string strRes = "reservationId=" + reservationId + "|bankTxnId=" + bankTxnId + "|txnAmount=" + txnAmount + "|status=" + status + "|statusDesc=" + statusDesc;
                string strCheckSum = sha256(strRes);
                string strResWithCheckSum = strRes + "|checkSum=" + strCheckSum;
                //EncryptedRes = strResWithCheckSum;
                EncryptDecryptString ENDE = new EncryptDecryptString();
                EncryptedRes = ENDE.EncryptString(strResWithCheckSum);
                DecryptedRes = strResWithCheckSum;
                ResponseStatus = statusDesc + "-" + status;
                    if (Convert.ToString(ConfigurationManager.AppSettings["RdsReturnURLTest"]) == "true")//RdsReturnURLTest
                    {
                        ReturnUrl = Convert.ToString(ConfigurationManager.AppSettings["PostRdsReturnURL"]); //"http://localhost:53943/Eticketing/VerificationResponse.aspx"; //Use for test ,after test Comment line
                    }
                    // ReturnUrl = "http://localhost:53943/Eticketing/BankResponse.aspx";//Use for test
                    postHtml = PreparePOSTForm(ReturnUrl, EncryptedRes);
            }
            else
            {
                ResponseRemark = "Record not found in RDSPayment table";
            }

                }
            catch(Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "CreatePaymentResponse", "CreateResponse", ex.Message, ex.StackTrace);
            }
            string ActionType = "RESPONSE";
            int temp = 0;
            string msgout = string.Empty;
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);               
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@BankTxnId", BankTxnId);
                cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                cmd.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                cmd.Parameters.AddWithValue("@ReservationId", ReservationId);
                cmd.Parameters.AddWithValue("@TxnAmount",Convert.ToDouble(txnAmount));
                cmd.Parameters.AddWithValue("@Status", ResponseStatus);
                cmd.Parameters.AddWithValue("@Remark", ResponseRemark);
                cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@EncryptedRes", EncryptedRes);
                cmd.Parameters.AddWithValue("@DecryptedRes", DecryptedRes);//PostHtml
                cmd.Parameters.AddWithValue("@PostHtml", postHtml);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                temp = cmd.ExecuteNonQuery();
                con.Close();
                msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);

                if (temp > 0 && msgout == "Success")
                {
                    postHtml ="yes~" + postHtml;
                }
                else
                {
                    postHtml = "no~" + postHtml;
                }
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "CreatePaymentResponse", "Insert", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return postHtml;
        }
        public DataTable GetRdsPaymentDetails(string UserId, string ReferenceNo, string MerchantCode, string ReservationId, string BankTxnId)
        {
            DataTable dt = new DataTable();
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                adap = new SqlDataAdapter("SpInsertIrctcRdsRequest", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@UserId", UserId);
                adap.SelectCommand.Parameters.AddWithValue("@BankTxnId", BankTxnId);
                adap.SelectCommand.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                adap.SelectCommand.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                adap.SelectCommand.Parameters.AddWithValue("@ReservationId", ReservationId);
                adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GETPAYMENTDETAILS");
                adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
                adap.Fill(dt);
                string msgout = Convert.ToString(adap.SelectCommand.Parameters["@Msg"].Value);
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "GetRdsPaymentDetails", "DataTable", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                adap.Dispose();
            }
            return dt;
        }


        public static string sha256(string randomString)
        {
            SHA256Managed crypt = new SHA256Managed();
            string hash = String.Empty;
            try
            {
                byte[] crypto = crypt.ComputeHash(Encoding.ASCII.GetBytes(randomString), 0, Encoding.ASCII.GetByteCount(randomString));
                foreach (byte theByte in crypto)
                {
                    hash += theByte.ToString("x2");
                }
            }
            catch(Exception ex)
            {
                //int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "GetRdsPaymentDetails", "DataTable", ex.Message, ex.StackTrace); 
            }
            return hash.ToUpper();
        }



        private string PreparePOSTForm(string url, string Response)
        {
            //Set a name for the form
            string formID = "PostForm";
            //Build the form using the specified data to be posted.
            StringBuilder strForm = new StringBuilder();
            strForm.Append("<form id=\"" + formID + "\" name=\"" +
                           formID + "\" action=\"" + url +
                           "\" method=\"POST\">");

            //foreach (System.Collections.DictionaryEntry key in data)
            //{

            //    strForm.Append("<input type=\"hidden\" name=\"" + key.Key +
            //                   "\" value=\"" + key.Value + "\">");
            //}

            string name = "encdata";
            string strValue = Response;
            strForm.Append("<input type=\"hidden\" name=\"" + name +
                             "\" value=\"" + strValue + "\">");

            strForm.Append("</form>");
            //Build the JavaScript which will do the Posting operation.
            StringBuilder strScript = new StringBuilder();
            strScript.Append("<script language='javascript'>");
            strScript.Append("var v" + formID + " = document." +
                             formID + ";");
            strScript.Append("v" + formID + ".submit();");
            strScript.Append("</script>");
            //Return the form and the script concatenated.
            //(The order is important, Form then JavaScript)
            return strForm.ToString() + strScript.ToString();
        }

        public string MakeDoublleVerificationReq(string EncryptedReqRDS, string DecryptedReqRDS, string IPAddress, string CreatedBy)
        {            
            string msg = string.Empty;
            string merchantCode = string.Empty;
            string reservationId = string.Empty;
            string txnAmount = "0.0";
            string bankTxnId = string.Empty;
            string checkSum = string.Empty;           
            string postHtml = string.Empty;          
            try
            {
                #region Parse Request from string
                System.Collections.Specialized.NameValueCollection Params = new System.Collections.Specialized.NameValueCollection();
                string[] segments = DecryptedReqRDS.Split('|');
                foreach (string seg in segments)
                {
                    string[] parts = seg.Split('=');
                    if (parts.Length > 0)
                    {
                        //resrvationId=1100012345|bankTxnId=FR0181838798413|txnAmount=458.0000|status=0|statusDesc=Success|checkSum=F55711A2B4F8C0AC1E2C91299CF616B501FBB0B6687CC49C896DA5B7B05D4ABA
                        string Key = parts[0].Trim();
                        string Value = parts[1].Trim();
                        if (Key == "merchantCode")
                            merchantCode = parts[1].Trim();

                        if (Key == "resrvationId")
                            reservationId = parts[1].Trim();

                        if (Key == "txnAmount")
                            txnAmount = parts[1].Trim();                     

                        if (Key == "checkSum")
                            checkSum = parts[1].Trim();

                        if (Key == "bankTxnId")
                            bankTxnId = parts[1].Trim();

                        Params.Add(Key, Value);

                    }
                }
                #endregion
                
                #region Check Hash
               // merchantCode = "IR_CRIS";
                //merchantCode=IR_CRIS|reservationId=104433|txnAmount=650.00|bankTxnId=IG01439494
                string Req = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|txnAmount=" + txnAmount + "|bankTxnId=" + bankTxnId + "";
                string checkSumEncript = sha256(Req).ToUpper();
                string DoubleVerificationReq = Req + "|checkSum=" + checkSumEncript;
                string EncriptReqAES256 = "";
                EncryptDecryptString ENDE = new EncryptDecryptString();
                //string EnDecryptKey = Convert.ToString(ConfigurationManager.AppSettings["RDSKEY"]);
                EncriptReqAES256 = ENDE.EncryptString(DoubleVerificationReq);
                string ReturnUrl ="http://localhost:53943/Eticketing/Verification.aspx"; //Use for test

                if (Convert.ToString(ConfigurationManager.AppSettings["RdsReturnURLTest"]) == "true")//RdsReturnURLTest
                {
                    ReturnUrl = "https://RWT.co/preprod/Eticketing/Verification.aspx";
                }
                postHtml = PreparePOSTForm(ReturnUrl, EncriptReqAES256);
                postHtml = "yes~" + postHtml;
                #endregion               
            }
            catch (Exception ex)
            {
                postHtml = "yes~" + postHtml;
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "MakeDoublleVerificationReq", "DataTable", ex.Message, ex.StackTrace); 
            }
            return postHtml;
        }

        public string ParseDoubleVerificationReq(string EncryptedReqRDS, string DecryptedReqRDS, string IPAddress, string RequestPostMethod,string ReturnUrl)
        {
            string msg = string.Empty;
            string merchantCode = string.Empty;
            string reservationId = string.Empty;
            string bankTxnId = string.Empty;
            string txnAmount = "0.0";
            string currencyType = string.Empty;            
            string checkSum = string.Empty;             
            bool CheckSumFlag = false;
            string ReferenceNo = string.Empty;
            string postHtml = string.Empty;
            try
            {
                #region Parse Request from string
                System.Collections.Specialized.NameValueCollection Params = new System.Collections.Specialized.NameValueCollection();
                string[] segments = DecryptedReqRDS.Split('|');
                foreach (string seg in segments)
                {
                    string[] parts = seg.Split('=');
                    if (parts.Length > 0)
                    {
                        string Key = parts[0].Trim();
                        string Value = parts[1].Trim();
                        if (Key == "merchantCode")
                            merchantCode = parts[1].Trim();

                        if (Key == "reservationId")
                            reservationId = parts[1].Trim();

                        if (Key == "bankTxnId")
                            bankTxnId = parts[1].Trim();

                        if (Key == "txnAmount")
                            txnAmount = parts[1].Trim();                       

                        if (Key == "checkSum")
                            checkSum = parts[1].Trim();


                        Params.Add(Key, Value);

                    }
                }
                #endregion
                string LogType = "";
                string LogRemark = "";
                #region Check Hash
                //Live Server                
                //merchantCode=RWT01|reservationId=100001406709103|bankTxnId=FR1163829819626|txnAmount=2170.00
                string PaymentRequest = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|bankTxnId=" + bankTxnId + "|txnAmount=" + txnAmount;
               
                //Test Server
                // string PaymentRequest = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|txnAmount=" + txnAmount + "|bankTxnId=" + bankTxnId;
                
                string EncriptPaymentRequest = sha256(PaymentRequest).ToUpper();

                if (EncriptPaymentRequest == checkSum)
                {
                    CheckSumFlag = true;
                    LogRemark = "checkSum hash match";
                }
                else
                {
                    LogRemark = "checkSum hash not match";
                }
                #endregion

                ReferenceNo = bankTxnId;
                LogType = "DoubleVerificationRequest";
                string appCode="";
                string pymtMode = "";
                string txnDate = "";
                string securityId = "";
                string RU="";
                string PaymentSatus = CheckPaymentStatus(ReferenceNo, bankTxnId, merchantCode, reservationId, txnAmount, currencyType, appCode, pymtMode, txnDate, securityId, RU, checkSum, IPAddress, EncryptedReqRDS, DecryptedReqRDS, LogType, LogRemark, "", CheckSumFlag);
                ReturnUrl = GetReturnUrlForDoubleVerification(ReferenceNo,merchantCode,reservationId,bankTxnId);  //Get Return URL from  RDSRequest table 
                string strRes = "";
                string strCheckSum = "";
                string status = "2";
                string statusDesc = "Error";
                txnAmount = Convert.ToDouble(txnAmount).ToString("0.00");
                //PaymentSatus.Split('~')[0]
                if (PaymentSatus.Split('~').Length > 1)
                {
                if (PaymentSatus.Split('~')[0] == "yes" && PaymentSatus.Split('~')[1] == "Success" && CheckSumFlag == true)
                {
                    statusDesc = "Success";
                    status = "0";
                        // string fff = "reservationId=104433|bankTxnId=IG01439494|txnAmount=1|status=0|statusDesc=Success|checkSum=C8166A2C4626ED03BF4918020D540AC3FCCD75F8BF393BAC61EB6A0D1F3191A8";
                        //  merchantCode = FW03719 | reservationId = 100001410594191 | bankTxnId = OP2408U81OEX | txnAmount = 2485.00 | status = 0 | statusDesc = Success | checkSum = DCB6CFBBEECC31F23762ED56BBA2E625AE4DD8F7BDA00215EF5C58FCC70DBA7D
                        strRes = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|bankTxnId=" + bankTxnId + "|txnAmount=" + txnAmount + "|status=" + status + "|statusDesc=" + statusDesc;
                    }
                else if (PaymentSatus.Split('~')[1] == "Fail")
                {
                    status = "1";
                    statusDesc = "Fail";
                    strRes = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|bankTxnId=" + bankTxnId + "|txnAmount=" + txnAmount + "|status=" + status + "|statusDesc=" + statusDesc;
                }
                else
                {
                    //2 //Error
                    statusDesc = "Error";
                    status = "2";
                    strRes = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|bankTxnId=" + bankTxnId + "|txnAmount=" + txnAmount + "|status=" + status + "|statusDesc=" + statusDesc;
                }
                }
                else
                {
                    //2 //Error
                    statusDesc = "Error";
                    status = "2";
                    strRes = "merchantCode=" + merchantCode + "|reservationId=" + reservationId + "|bankTxnId=" + bankTxnId + "|txnAmount=" + txnAmount + "|status=" + status + "|statusDesc=" + statusDesc;
                }
               

                strCheckSum = sha256(strRes);
                string strResWithCheckSum = strRes + "|checkSum=" + strCheckSum;
                //EncryptedRes = strResWithCheckSum;
                EncryptDecryptString ENDE = new EncryptDecryptString();
                //string EnDecryptKey = Convert.ToString(ConfigurationManager.AppSettings["RDSKEY"]);
               string EncryptedRes = ENDE.EncryptString(strResWithCheckSum);
               string DecryptedRes = strResWithCheckSum;
               string ResponseStatus = PaymentSatus + "-" + status;
                //if(Convert.ToString(ConfigurationManager.AppSettings["RdsReturnURLTest"])== "true")//RdsReturnURLTest
                //{
                //    ReturnUrl = Convert.ToString(ConfigurationManager.AppSettings["RdsReturnURL"]); //"http://localhost:53943/Eticketing/VerificationResponse.aspx"; //Use for test ,after test Comment line
                //}
                if(RequestPostMethod=="false")
                {
                   postHtml = EncryptedRes;//Use for WebSocket
                }
                else
                {
                    postHtml = PreparePOSTForm(ReturnUrl, EncryptedRes);   //Use For Post Method
                }                
                #region Insert Double Verification  Response
                string ActionType = "DoubleVerification";
               int temp = 0;
               string msgout = string.Empty;
               try
               {
                   con.Open();
                   cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);                  
                   cmd.CommandType = CommandType.StoredProcedure;                   
                   cmd.Parameters.AddWithValue("@BankTxnId", bankTxnId);
                   cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                   cmd.Parameters.AddWithValue("@MerchantCode", merchantCode);
                   cmd.Parameters.AddWithValue("@ReservationId", reservationId);
                   cmd.Parameters.AddWithValue("@TxnAmount", Convert.ToDouble(txnAmount));
                   cmd.Parameters.AddWithValue("@Status", ResponseStatus);
                   cmd.Parameters.AddWithValue("@Remark", LogRemark);
                   cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                   cmd.Parameters.AddWithValue("@EncryptedRes", EncryptedRes);
                   cmd.Parameters.AddWithValue("@DecryptedRes", DecryptedRes);//PostHtml
                   cmd.Parameters.AddWithValue("@PostHtml", postHtml);
                   cmd.Parameters.AddWithValue("@CreatedBy", RequestPostMethod);
                   cmd.Parameters.AddWithValue("@ActionType", ActionType);
                   cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                   cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                   temp = cmd.ExecuteNonQuery();
                   con.Close();
                   msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);
                   //Status Flag – 0 - Success,1 - Fail , 2 - Error
                   if (temp > 0 )//&& msgout == "Success")
                   {
                       postHtml = "yes~" + postHtml;
                   }                  
                   else
                   {
                        postHtml = msgout + "~" + postHtml;
                     // postHtml = "yes~" + postHtml;
                    }
               }
               catch (Exception ex)
               {
                   postHtml = "no~" + postHtml;
                   //int insert = InsertExceptionLog("PaymentGateway", "PaymentGateway", "UpdateCreditLimit", "insert", ex.Message, ex.StackTrace);
               }
               finally
               {
                   con.Close();
                   cmd.Dispose();
               }

                #endregion
            }
            catch (Exception ex)
            {
                postHtml = "no~" + postHtml;
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "ParseDoubleVerificationReq", "ParseInsert", ex.Message, ex.StackTrace); 
            }
            return postHtml;
        }

        public string InsertDoubleVerificationReq(string ReferenceNo, string MerchantCode, string ReservationId, string TxnAmount, string CurrencyType, string AppCode, string PymtMode, string TxnDate, string SecurityId, string RU, string CheckSum, string IPAddress, string EncryptedReq, string DecryptedReq, string LogType, string LogRemark, string CreatedBy,bool CheckSumFlag)
        {
            string ActionType = "DoubleVerification";
            int temp = 0;
            string msgout = "no";
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);
                //cmd = new SqlCommand("SP_INSERT_IRCTCRDSREQUEST", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                cmd.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                cmd.Parameters.AddWithValue("@ReservationId", ReservationId);
                cmd.Parameters.AddWithValue("@TxnAmount", TxnAmount);
                cmd.Parameters.AddWithValue("@CurrencyType", CurrencyType);
                cmd.Parameters.AddWithValue("@AppCode", AppCode);
                cmd.Parameters.AddWithValue("@PymtMode", PymtMode);
                cmd.Parameters.AddWithValue("@TxnDate", TxnDate);
                cmd.Parameters.AddWithValue("@SecurityId", SecurityId);
                cmd.Parameters.AddWithValue("@RU", RU);
                cmd.Parameters.AddWithValue("@CheckSum", CheckSum);
                //cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                //cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                #region Use for Log Table
                cmd.Parameters.AddWithValue("@EncryptedReq", EncryptedReq);
                cmd.Parameters.AddWithValue("@DecryptedReq", DecryptedReq);
                cmd.Parameters.AddWithValue("@Type", LogType);
                cmd.Parameters.AddWithValue("@Remark", LogRemark);
                cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                #endregion
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                temp = cmd.ExecuteNonQuery();
                con.Close();
                msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);
                if(temp>1)
                {
                    msgout = "yes~" + msgout;
                }
                else
                {
                    msgout = "no~" + msgout;
                }
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "InsertDoubleVerificationReq", "Insert", ex.Message, ex.StackTrace); 
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return msgout;
        }

        public string CheckPaymentStatus(string ReferenceNo,string BankTxnId, string MerchantCode, string ReservationId, string TxnAmount, string CurrencyType, string AppCode, string PymtMode, string TxnDate, string SecurityId, string RU, string CheckSum, string IPAddress, string EncryptedReq, string DecryptedReq, string LogType, string LogRemark, string CreatedBy, bool CheckSumFlag)
        {
            string ActionType = "CHECKPAYMENT";
            int temp = 0;
            string msgout = "no";            
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);                
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@BankTxnId", BankTxnId);
                cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                cmd.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                cmd.Parameters.AddWithValue("@ReservationId", ReservationId);
                cmd.Parameters.AddWithValue("@TxnAmount", TxnAmount);
                cmd.Parameters.AddWithValue("@CurrencyType", CurrencyType);
                cmd.Parameters.AddWithValue("@AppCode", AppCode);
                cmd.Parameters.AddWithValue("@PymtMode", PymtMode);
                cmd.Parameters.AddWithValue("@TxnDate", TxnDate);
                cmd.Parameters.AddWithValue("@SecurityId", SecurityId);
                cmd.Parameters.AddWithValue("@RU", RU);
                cmd.Parameters.AddWithValue("@CheckSum", CheckSum);               
                #region Use for Log Table
                cmd.Parameters.AddWithValue("@EncryptedReq", EncryptedReq);
                cmd.Parameters.AddWithValue("@DecryptedReq", DecryptedReq);
                cmd.Parameters.AddWithValue("@Type", LogType);
                cmd.Parameters.AddWithValue("@Remark", LogRemark);
                cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@CreatedBy", CreatedBy);
                #endregion
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                temp = cmd.ExecuteNonQuery();
                con.Close();
                msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);
                if (temp > 0)
                {
                    msgout = "yes~" + msgout;
                }
                else
                {
                    msgout = "no~" + msgout;
                }
            }
            catch (Exception ex)
            {
                msgout = "no~" + msgout;
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "CheckPaymentStatus", "Insert", ex.Message, ex.StackTrace); 
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return msgout;
        }        
        #endregion        

        public DataSet user_auth(string uid, string passwd)
        {
            DataSet ds = new DataSet();
            try
            {
                //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                //SqlDataAdapter adap;
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                adap = new SqlDataAdapter("UserLoginNew", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;
                adap.SelectCommand.Parameters.AddWithValue("@uid", uid);
                adap.SelectCommand.Parameters.AddWithValue("@pwd", passwd);
                adap.Fill(ds);
                con.Close();
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "user_auth", "DataSet", ex.Message, ex.StackTrace);
                con.Close();
                adap.Dispose();
            }

            return ds;
        }


        public string MakeFailAndErrorPaymentResponse(string UserId, string MerchantCode, string ReferenceNo, string ResrvationId, string BankTxnId, string TxnAmount, string Status, string ReturnUrl, string Remark, string IPAddress)
        {
            
            //merchantCode=IR_CRIS|reservationId=1100012345|txnAmount=458.00|currencyType=INR|appCode=BOOKING|pymtMode=Internet|txnDate=20111206|securityId=CRIS|RU=https://localhost:8080/eticketing/BankResponse|checkSum=1EF
            //yes~FR1154150849881~IR_CRIS~1100012345~True~458.00~https://localhost:8080/eticketing/BankResponse
            //yes~bankTxnId~merchantCode~reservationId~True~txnAmount~RU
            //resrvationId=104433|bankTxnId= IG01439494|txnAmount=1| status=0|statusDesc=Success|checkSum= 1EF7C9AA4C9CF912C1539AE332E997E15019E40010A59363064C9C308EA13873
            #region Create Respons
            string PostHtml = string.Empty;
            string EncryptedRes = string.Empty;
            string DecryptedRes = string.Empty;
            string ResponseStatus = string.Empty;
            TxnAmount = Convert.ToDouble(TxnAmount).ToString("0.00");
            try
            {
                string StatusDesc = "";
                //Status Flag – 0 - Success,1 - Fail , 2 - Error
                if(Status=="1")
                {
                    StatusDesc = "Fail";
                }
                else
                {
                    Status = "2";
                    StatusDesc = "Error";
                }

                
                //string strRes = "resrvationId=" + ReservationId + "|bankTxnId= " + BankTxnId + "|txnAmount=" + TxnAmount + "| status=0|statusDesc=Success";
                string strRes = "reservationId=" + ResrvationId + "|bankTxnId=" + BankTxnId + "|txnAmount=" + TxnAmount + "|status=" + Status + "|statusDesc=" + StatusDesc;
                string strCheckSum = sha256(strRes);
                string strResWithCheckSum = strRes + "|checkSum=" + strCheckSum;
                //EncryptedRes = strResWithCheckSum;
                EncryptDecryptString ENDE = new EncryptDecryptString();
                EncryptedRes = ENDE.EncryptString(strResWithCheckSum);
                DecryptedRes = strResWithCheckSum;
                ResponseStatus = StatusDesc + "-" + Status;
                //ReturnUrl = "http://localhost:53943/Eticketing/BankResponse.aspx";  //Use for test
                PostHtml = PreparePOSTForm(ReturnUrl, EncryptedRes);
            }
            catch(Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "MakeFailAndErrorPaymentResponse", "CreateResponse", ex.Message, ex.StackTrace);
            }
            #endregion


            #region Insert Respons in Database
            string ActionType = "RESPONSE";
            int temp = 0;
            string msgout = string.Empty;
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertIrctcRdsRequest", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@UserId", UserId);
                cmd.Parameters.AddWithValue("@BankTxnId", BankTxnId);
                cmd.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                cmd.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                cmd.Parameters.AddWithValue("@ReservationId", ResrvationId);
                cmd.Parameters.AddWithValue("@TxnAmount", TxnAmount);
                cmd.Parameters.AddWithValue("@Status", ResponseStatus);
                cmd.Parameters.AddWithValue("@Remark", Remark);
                cmd.Parameters.AddWithValue("@IPAddress", IPAddress);
                cmd.Parameters.AddWithValue("@EncryptedRes", EncryptedRes);
                cmd.Parameters.AddWithValue("@DecryptedRes", DecryptedRes);//PostHtml
                cmd.Parameters.AddWithValue("@PostHtml", PostHtml);
                cmd.Parameters.AddWithValue("@CreatedBy", UserId);
                cmd.Parameters.AddWithValue("@ActionType", ActionType);
                cmd.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                cmd.Parameters["@Msg"].Direction = ParameterDirection.Output;
                temp = cmd.ExecuteNonQuery();
                con.Close();
                msgout = Convert.ToString(cmd.Parameters["@Msg"].Value);

                if (temp > 0 && msgout == "Success")
                {
                    msgout = "Success";
                }
                else
                {
                    PostHtml = "";
                }
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "MakeFailAndErrorPaymentResponse", "InsertFailAndErrorResponse", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            #endregion


            return PostHtml;
        }

        public string GetReturnUrlForDoubleVerification(string ReferenceNo, string MerchantCode, string ReservationId, string BankTxnId)
        {
            string ReturnUrl = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                if (con.State == ConnectionState.Closed)
                {
                    con.Open();
                }
                adap = new SqlDataAdapter("SpInsertIrctcRdsRequest", con);
                adap.SelectCommand.CommandType = CommandType.StoredProcedure;                
                adap.SelectCommand.Parameters.AddWithValue("@BankTxnId", BankTxnId);
                adap.SelectCommand.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
                adap.SelectCommand.Parameters.AddWithValue("@MerchantCode", MerchantCode);
                adap.SelectCommand.Parameters.AddWithValue("@ReservationId", ReservationId);
                adap.SelectCommand.Parameters.AddWithValue("@ActionType", "GET_VERIVIACTION_URL");
                adap.SelectCommand.Parameters.Add("@Msg", SqlDbType.NVarChar, 100);
                adap.SelectCommand.Parameters["@Msg"].Direction = ParameterDirection.Output;
                adap.Fill(dt);
                string msgout = Convert.ToString(adap.SelectCommand.Parameters["@Msg"].Value);                          
                if (dt != null && dt.Rows.Count > 0)
                 {
                        ReturnUrl = Convert.ToString(dt.Rows[0]["RU"]);                        
                 }
                else
                {
                    ReturnUrl = "https://www.agent.irctc.co.in/eticketing/BankResponse";
                }                
            }
            catch (Exception ex)
            {
                int insert = InsertExceptionLog("IRCTC-RDS", "IrctcRdsPayment.cs", "GetReturnUrlForDoubleVerification", "DataTable", ex.Message, ex.StackTrace);
            }
            finally
            {
                con.Close();
                adap.Dispose();
            }            
            return ReturnUrl;
        }



        #region Insert Exception Log
        public int InsertExceptionLog(string Module, string ClassName, string MethodName, string ErrorCode, string ExMessage, string ExStackTrace)
        {
            int temp = 0;           
            try
            {
                con.Open();
                cmd = new SqlCommand("SpInsertExceptionLog", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Module", Module);
                cmd.Parameters.AddWithValue("@ClassName", ClassName);
                cmd.Parameters.AddWithValue("@MethodName", MethodName);
                cmd.Parameters.AddWithValue("@ErrorCode", ErrorCode);
                cmd.Parameters.AddWithValue("@ExMessage", ExMessage);
                cmd.Parameters.AddWithValue("@ExStackTrace ", ExStackTrace);
                temp = cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                con.Close();
                cmd.Dispose();
            }
            return temp;
        }

        #endregion


    }
}
