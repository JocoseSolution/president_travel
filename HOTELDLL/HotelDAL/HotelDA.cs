﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using HotelShared;
namespace HotelDAL
{
    public class HotelDA
    {
        SqlCommand sqlcmd;
        private SqlDatabase DBhelper; private SqlDatabase DBhelperLnb;
        public HotelDA()
        {
            DBhelper = new SqlDatabase(ConfigurationManager.ConnectionStrings["HTLConnStr"].ConnectionString);
            DBhelperLnb = new SqlDatabase(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        }
        //'Hotel City Search
        public List<CitySearch> GetCityList(string City)
        {
            SqlCommand sqlcmd = new SqlCommand();
            List<CitySearch> CityList = new List<CitySearch>();
            try
            {
                sqlcmd.CommandText = "SP_HtlCitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
                sqlcmd.Parameters.AddWithValue("@ReqType", "CITY");
                //sqlcmd.Parameters.AddWithValue("@ReqType", "OYO");
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    CityList.Add(new CitySearch
                    {
                        //change by Anurag 17-05-2019
                        CityName = dbr["CityName"].ToString().Trim(),
                        CityCode = dbr["CityCode"].ToString().Trim(),
                        Country = dbr["Country"].ToString().Trim(),
                        CountryCode = dbr["CountryCode"].ToString().Trim(),
                        RegionID = dbr["RegionID"].ToString().Trim(),
                        ZumataRegionID = dbr["ZumataRegionID"].ToString().Trim(),
                        MTSCity = dbr["MTS_City"].ToString().Trim(),
                        InnstantCityID = dbr["CityID"].ToString().Trim(),
                        NoOfHotel = dbr["NoOfHotel"].ToString().Trim(),
                        SearchType = dbr["SearchType"].ToString().Trim(),
                        ExpediaRegionID = dbr["ExpediaRegionId"].ToString().Trim(),
                        GRNCityCode = dbr["GrnCityCode"].ToString().Trim()
                        // Availability_0Star = Convert.ToBoolean(dbr["Availability_ZeroStar"])

                        //CityName = dbr["HotelName"].ToString().Trim(),
                        //CityCode = dbr["HotelID"].ToString().Trim(),
                        //Country = dbr["CountryName"].ToString().Trim(),
                        //RegionID = dbr["ZipCode"].ToString().Trim(),
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetCityList");
            }
            finally { sqlcmd.Dispose(); }

            return CityList;
        }
        //'Hotel City Search for International and domestic
        public List<CitySearch> GetCityList2(string City, string HLT_NAME)
        {
            SqlCommand sqlcmd = new SqlCommand();
            List<CitySearch> CityList = new List<CitySearch>();
            try
            {
                sqlcmd.CommandText = "SP_HtlCitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", HLT_NAME);
                sqlcmd.Parameters.AddWithValue("@ReqType", "HOTEL");
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    CityList.Add(new CitySearch
                    {
                        CityName = dbr["VendorName"].ToString().Trim(),
                        CityCode = dbr["VendorID"].ToString().Trim(),
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetCityList2");
            }
            finally { sqlcmd.Dispose(); }

            return CityList;
        }
        //get Hotel Credencials
        public HotelSearch GetCredentials(HotelSearch SearchDetail, string Reqtype)
        {
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectCredential");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Type", Reqtype);
                //sqlcmd.Parameters.AddWithValue("@Provider", SearchDetail.Provider);
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                //while (dbr.Read())
                //{
                //    switch (dbr["provider"].ToString().Trim())
                //    {
                //        case "GTA":
                //                SearchDetail.GTAURL = dbr["Url"].ToString().Trim();
                //                SearchDetail.GTAClintID = dbr["UserId"].ToString().Trim();
                //                SearchDetail.GTAPassword = dbr["password"].ToString().Trim();
                //                SearchDetail.GTAEmailAddress = dbr["propertyid"].ToString().Trim();
                //                SearchDetail.GTATrip = dbr["trip"].ToString().Trim();
                //            break;
                //        case "TG":
                //                SearchDetail.TGUrl = dbr["Url"].ToString().Trim();
                //                SearchDetail.TGUsername = dbr["Username"].ToString().Trim();
                //                SearchDetail.TGPassword = dbr["password"].ToString().Trim();
                //                SearchDetail.TGPropertyId = dbr["propertyid"].ToString().Trim();
                //                SearchDetail.TGTrip = dbr["trip"].ToString().Trim();
                //            break;
                //        case "ROOMXML":
                //            SearchDetail.RoomXMLURL = dbr["Url"].ToString().Trim();
                //            SearchDetail.RoomXMLUserID = dbr["Username"].ToString().Trim();
                //            SearchDetail.RoomXMLPassword = dbr["password"].ToString().Trim();
                //            SearchDetail.RoomXMLOrgID = dbr["propertyid"].ToString().Trim();
                //            SearchDetail.RoomXMLTrip = dbr["trip"].ToString().Trim();
                //            break;
                //        case "ZUMATA":
                //            SearchDetail.ZumataURL = dbr["Url"].ToString().Trim();
                //            SearchDetail.ZumataAPIKEY = dbr["Username"].ToString().Trim();
                //            SearchDetail.ZumataTrip = dbr["trip"].ToString().Trim();
                //            break;
                //        case "RZ":
                //            SearchDetail.RezNextUrl = dbr["Url"].ToString().Trim();
                //            SearchDetail.RezNextUserName = dbr["Username"].ToString().Trim();
                //            SearchDetail.RezNextPassword = dbr["password"].ToString().Trim();
                //            SearchDetail.RezNextSystemId = dbr["propertyid"].ToString().Trim();
                //            SearchDetail.RezNextTrip = dbr["trip"].ToString().Trim();
                //            break;
                //    }
                //}  
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetCredentials");
            }
            finally { sqlcmd.Dispose(); }
            //servicetax
            // SearchDetail = HotelServicetaxNew(SearchDetail);
            // SearchDetail.TG_servicetax = 0;
            //SearchDetail.GTA_servicetax = 0;

            return SearchDetail;
        }
        //NEW Hotel Credencials
        public List<HotelCredencials> GETCredencialsList(List<HotelCredencials> CredencialsList, string Reqtype)
        {
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectCredential");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Type", Reqtype);
                //sqlcmd.Parameters.AddWithValue("@Provider", SearchDetail.Provider);
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    CredencialsList.Add(new HotelCredencials
                    {
                        HotelUrl = dbr["Url"].ToString().Trim(),
                        HotelUsername = dbr["UserId"].ToString().Trim(),
                        HotelPassword = dbr["password"].ToString().Trim(),
                        HotelCompanyID = dbr["propertyid"].ToString().Trim(),
                        HotelTrip = dbr["trip"].ToString().Trim(),
                        HotelProvider = dbr["provider"].ToString().Trim(),
                        HotelBookingUrl = dbr["BookingURL"].ToString().Trim(),
                        CancelUrl = dbr["CancelURL"].ToString().Trim(),
                        OtherData = dbr["OtherData"].ToString().Trim(),
                        Status = Convert.ToBoolean(dbr["Enables"]),
                        Counter = Convert.ToInt32(dbr["CredencialID"])
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GETCredencialsList");
            }
            finally { sqlcmd.Dispose(); }
            return CredencialsList;
        }
        //get Credentials Details
        public HotelBooking GetBookingCredentials(HotelBooking BookingDetail, string Type)
        {
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectCredential");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Type", Type);
                sqlcmd.Parameters.AddWithValue("@Provider", BookingDetail.Provider);
                var dbr = DBhelper.ExecuteReader(sqlcmd);

                while (dbr.Read())
                {
                    BookingDetail.HotelUrl = dbr["Url"].ToString().Trim();
                    BookingDetail.HotelUsername = dbr["UserId"].ToString().Trim();
                    BookingDetail.HotelPassword = dbr["password"].ToString().Trim();
                    BookingDetail.HotelCompanyID = dbr["propertyid"].ToString().Trim();
                    BookingDetail.HotelBookingUrl = dbr["BookingURL"].ToString().Trim();
                    BookingDetail.CancelUrl = dbr["CancelURL"].ToString().Trim();
                    BookingDetail.OtherData = dbr["OtherData"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetBookingCredentials");
            }
            finally { sqlcmd.Dispose(); }
            return BookingDetail;
        }
        //get Credentials Details
        public HotelCancellation GetCancleCredentials(HotelCancellation BookingDetail, string Type)
        {
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectCredential");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Type", Type);
                sqlcmd.Parameters.AddWithValue("@Provider", BookingDetail.Provider);
                var dbr = DBhelper.ExecuteReader(sqlcmd);

                while (dbr.Read())
                {
                    BookingDetail.HotelUrl = dbr["Url"].ToString().Trim();
                    BookingDetail.Can_UserID = dbr["UserId"].ToString().Trim();
                    BookingDetail.Can_Password = dbr["password"].ToString().Trim();
                    BookingDetail.Can_PropertyId = dbr["propertyid"].ToString().Trim();
                    BookingDetail.HotelBookingUrl = dbr["BookingURL"].ToString().Trim();
                    BookingDetail.CancelUrl = dbr["CancelURL"].ToString().Trim();
                    BookingDetail.OtherData = dbr["OtherData"].ToString().Trim();
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetCancleCredentials");
            }
            finally { sqlcmd.Dispose(); }
            return BookingDetail;
        }
        //Select Hotel Service Tax for all provider
        public HotelSearch HotelServicetaxNew(HotelSearch SearchDetail)
        {
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@CurrencyValue", 0);
                sqlcmd.Parameters.AddWithValue("@ReqType", "ServiceTax");
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                //while (dbr.Read())
                //{
                //    switch (dbr["HotelType"].ToString().Trim())
                //    {
                //        case "GTA":
                //            SearchDetail.GTA_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
                //            break;
                //        case "TG":
                //            SearchDetail.TG_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
                //            break;
                //        case "ROOMXML":
                //            SearchDetail.ROOMXML_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
                //            break;
                //        case "EX":
                //            SearchDetail.EX_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
                //            break;
                //    }
                //}
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "HotelServicetaxNew");
            }
            finally { sqlcmd.Dispose(); }
            return SearchDetail;
        }
        //Select Hotel Service Tax
        public decimal HotelServicetax(string HotelType)
        {
            decimal i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                // sqlcmd.Parameters.AddWithValue("@HotelType", HotelType);
                sqlcmd.Parameters.AddWithValue("@CurrencyValue", 0);
                sqlcmd.Parameters.AddWithValue("@ReqType", "ServiceTax");
                i = Convert.ToDecimal(DBhelper.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "HotelServicetax");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        // Update Currancy value
        public int UpdateCurrancyValue(decimal CurrancyValue)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                //   sqlcmd.Parameters.AddWithValue("@HotelType", "");
                sqlcmd.Parameters.AddWithValue("@CurrencyValue", CurrancyValue);
                sqlcmd.Parameters.AddWithValue("@ReqType", "UpdateCurrecy");
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateCurrancyValue");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Select Currancy value 
        public decimal SelectCurrancyValue()
        {
            decimal i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                //   sqlcmd.Parameters.AddWithValue("@HotelType", HotelStatus.International.ToString());
                sqlcmd.Parameters.AddWithValue("@CurrencyValue", 0);
                sqlcmd.Parameters.AddWithValue("@ReqType", "SelectCurrecy");
                i = Convert.ToDecimal(DBhelper.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "SelectCurrancyValue");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Get Hotel Markup
        public DataSet GetHtlMarkup(string LoginId, string AgentId, string Country, string City, string Star, string Htltype, string MarkupType, string markupAmt, int Counter, string Supplier, string GroupType, string ReqType)
        {
            DataSet MrkDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsGetHtlMarkup");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
                sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@TripType", Htltype);
                sqlcmd.Parameters.AddWithValue("@MarkupType", MarkupType);
                sqlcmd.Parameters.AddWithValue("@MarkupAmount", markupAmt);
                sqlcmd.Parameters.AddWithValue("@Supplier", Supplier);
                sqlcmd.Parameters.AddWithValue("@GroupType", GroupType);
                sqlcmd.Parameters.AddWithValue("@MarkupId", Counter);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                MrkDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHtlMarkup");
            }
            finally { sqlcmd.Dispose(); }
            return MrkDs;
        }

        //generate Orderid
        public string GetOrderID()
        {
            string OrderID = "";
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_GetOrderID");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                OrderID += DBhelper.ExecuteScalar(sqlcmd).ToString();
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetOrderID");
            }
            finally
            { sqlcmd.Dispose(); }

            return OrderID;
        }
        //get Hotel Overviw
        public DataTable GetHotelOverview(string City, string HotelCode, string SupplierName)
        {
            DataSet Hotelds = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectHotelOverview");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@HotelId", HotelCode);
                sqlcmd.Parameters.AddWithValue("@SupplierName", SupplierName);
                Hotelds = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHotelOverview");
            }
            finally { sqlcmd.Dispose(); }
            return Hotelds.Tables[0];
        }
        //get Room Discription
        public DataTable GetRoomDiscription(string Hotelcode)
        {
            DataSet RoomDT = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectRoomDiscription");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Hotelcode", Hotelcode);
                RoomDT = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetRoomDiscription");
            }
            finally { sqlcmd.Dispose(); }
            return RoomDT.Tables[0];
        }
        //get Hotel Services
        public DataTable GetHotelServices(string HotelCode, string AmenityType)
        {

            DataSet HotelDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_HotelFacilities");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@HotelCode", HotelCode);
                sqlcmd.Parameters.AddWithValue("@AmenityType", AmenityType);
                HotelDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHotelServices");
            }
            finally { sqlcmd.Dispose(); }
            return HotelDS.Tables[0];
        }
        //get Hotel Services and Image and Attraction
        public DataSet GetHotelImageandAround(string HotelCode)
        {
            sqlcmd = new SqlCommand("SP_HTL_SelectHotelService");
            DataSet HotelDS = new DataSet();
            try
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@HotelCode", HotelCode);
                sqlcmd.Parameters.AddWithValue("@AmenityType", "");
                HotelDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "");
            }
            finally { sqlcmd.Dispose(); }
            return HotelDS;
        }
        ////get RoomDiscription
        //public DataSet GetRoomDiscription(string HotelCode)
        //{
        //    sqlcmd = new SqlCommand("SP_SELECT_HOTEL_DETALS");
        //    DataSet HotelDS = new DataSet();
        //    try
        //    {
        //        sqlcmd.CommandType = CommandType.StoredProcedure;
        //        sqlcmd.Parameters.AddWithValue("@venderID", HotelCode);
        //        sqlcmd.Parameters.AddWithValue("@ref", "RoomDescription");
        //        HotelDS = DBhelper.ExecuteDataSet(sqlcmd);
        //    }
        //    catch (Exception ex)
        //    {
        //        InsertHotelErrorLog(ex, "");
        //    }
        //    finally { sqlcmd.Dispose(); }
        //    return HotelDS;
        //}
        //'Insert Data in Hotel Heder table
        public int InsHtlHrdDetails(string OrderID, string Status, string HtlCode, string RoomCode, string StarRating, decimal TotalCost, decimal NetCost, decimal Tax, decimal ExtraGuestTax, decimal BaseRate,
           decimal ExchangeRate, decimal AdminMrk, decimal AgentMarkup, string AdminMrkType, string AgentMrkType, decimal AdminMrkPercent, decimal AgtMrkPercent, String DiscountMsg, decimal DiscountAmt,
            string TripType, string Currency, string Country, string City, string Address, string PostalCode, string CheckIN, string CheckOut, int RoomCount, int NightCount, int AdtCount, int ChildCount,
         string IP, string AgentID, string AgencyName, decimal ServiceTax, decimal ServiceTaxAmt, decimal VServiceTaxAmt, string provider, decimal CommisionPer, decimal CommisionAmt, string CommisionType)
        {
            int i = 0;
            SqlCommand sqlcmd = new SqlCommand("SP_HTL_InsertBookingHeader");
            try
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Orderid", OrderID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@TripType", TripType);
                sqlcmd.Parameters.AddWithValue("@DiscountAmt", DiscountAmt);
                sqlcmd.Parameters.AddWithValue("@DiscounttMsg", DiscountMsg);
                sqlcmd.Parameters.AddWithValue("@TotalCost", TotalCost);
                sqlcmd.Parameters.AddWithValue("@NetCost", NetCost);
                sqlcmd.Parameters.AddWithValue("@BaseRate", BaseRate);
                sqlcmd.Parameters.AddWithValue("@Tax", Tax);
                sqlcmd.Parameters.AddWithValue("@ExtraGuestTax", ExtraGuestTax);
                sqlcmd.Parameters.AddWithValue("@ExchangeRate", ExchangeRate);
                sqlcmd.Parameters.AddWithValue("@AdminMrkPer", AdminMrkPercent);
                sqlcmd.Parameters.AddWithValue("@AdminMrkAmt", AdminMrk);
                sqlcmd.Parameters.AddWithValue("@AdminMrkType", AdminMrkType);
                sqlcmd.Parameters.AddWithValue("@AgentMrkAmt", AgentMarkup);
                sqlcmd.Parameters.AddWithValue("@AgentMrkPer", AgtMrkPercent);
                sqlcmd.Parameters.AddWithValue("@AgentMrkType", AgentMrkType);
                sqlcmd.Parameters.AddWithValue("@CheckIN", CheckIN);
                sqlcmd.Parameters.AddWithValue("@CheckOut", CheckOut);
                sqlcmd.Parameters.AddWithValue("@RoomCount", RoomCount);
                sqlcmd.Parameters.AddWithValue("@NightCount", NightCount);
                sqlcmd.Parameters.AddWithValue("@AdultCount", AdtCount);
                sqlcmd.Parameters.AddWithValue("@ChildCount", ChildCount);
                sqlcmd.Parameters.AddWithValue("@StarRating", StarRating);
                sqlcmd.Parameters.AddWithValue("@provider", provider);
                sqlcmd.Parameters.AddWithValue("@IP", IP);
                sqlcmd.Parameters.AddWithValue("@Currency", Currency);
                sqlcmd.Parameters.AddWithValue("@LoginID", AgentID);
                sqlcmd.Parameters.AddWithValue("@AgencyName", AgencyName);
                sqlcmd.Parameters.AddWithValue("@ServiceTaxPer", ServiceTax);
                sqlcmd.Parameters.AddWithValue("@ServiceTaxAmount", ServiceTaxAmt);
                sqlcmd.Parameters.AddWithValue("@VServiceTaxAmount", VServiceTaxAmt);
                sqlcmd.Parameters.AddWithValue("@CommisionPer", CommisionPer);
                sqlcmd.Parameters.AddWithValue("@CommisionAmt", CommisionAmt);
                sqlcmd.Parameters.AddWithValue("@CommisionType", CommisionType);
                // sqlcmd.Parameters.AddWithValue("@OriginalFare", OriginalFare);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsHtlHrdDetails");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        //Insert Hotel Details
        public int InsHotelDetails(string OrderID, string HtlCode, string HtlName, string RoomName, string RoomTypeCode, string RoomRatePlane, string RoomRPH, string HotelLocation, string Address, string CountryCode, string CityCode,
         string CityName, string CountryName, string ContactNo, string HtlImage, string Inclusion, string EssentialInfo, string SharingBedding, string PerRoomPreNightRate_org, string PerRoomPreNightRate_mrk,
            string PerRoomWise_Gues, string CancellationPoli, string SearchType, string Refundable, string Smoking)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsertHotelDetails");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@HotelCode", HtlCode);
                sqlcmd.Parameters.AddWithValue("@HotelName", HtlName);
                sqlcmd.Parameters.AddWithValue("@RoomTypeCode", RoomTypeCode);
                sqlcmd.Parameters.AddWithValue("@RoomRatePlane", RoomRatePlane);
                sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
                sqlcmd.Parameters.AddWithValue("@RoomRPH", RoomRPH);
                sqlcmd.Parameters.AddWithValue("@Address", Address);
                sqlcmd.Parameters.AddWithValue("@CityName", CityName);
                sqlcmd.Parameters.AddWithValue("@CityCode", CityCode);
                sqlcmd.Parameters.AddWithValue("@CountryCode", CountryCode);
                sqlcmd.Parameters.AddWithValue("@CountryName", CountryName);
                sqlcmd.Parameters.AddWithValue("@HotelLocation", HotelLocation);
                sqlcmd.Parameters.AddWithValue("@ContactNo", ContactNo);
                sqlcmd.Parameters.AddWithValue("@HotelImage", HtlImage);
                sqlcmd.Parameters.AddWithValue("@PerRoomPreNightRate_org", PerRoomPreNightRate_org);
                sqlcmd.Parameters.AddWithValue("@PerRoomPreNightRate_mrk", PerRoomPreNightRate_mrk);
                sqlcmd.Parameters.AddWithValue("@PerRoomWise_Guest", PerRoomWise_Gues);
                sqlcmd.Parameters.AddWithValue("@Inclusion", Inclusion);
                sqlcmd.Parameters.AddWithValue("@EssentialInfo", EssentialInfo);
                sqlcmd.Parameters.AddWithValue("@SharingBedding", SharingBedding);
                sqlcmd.Parameters.AddWithValue("@CancellationPoli", CancellationPoli);
                sqlcmd.Parameters.AddWithValue("@SearchType", SearchType);
                sqlcmd.Parameters.AddWithValue("@Refundable", Refundable);
                sqlcmd.Parameters.AddWithValue("@Smoking", Smoking);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsHotelDetails");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        //Insert Room Details
        public int InsRoomDetails(string OrderID, string RoomTypeCode, string RoomPlanCode, string RoomName, string RoomRPH, int AdultCount, int ChildCount, int RoomNo, string ChildAge)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsertRoomDetail");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@RoomTypeCode", RoomTypeCode);
                sqlcmd.Parameters.AddWithValue("@RoomPlanCode", RoomPlanCode);
                sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
                sqlcmd.Parameters.AddWithValue("@RoomRPH", RoomRPH);
                sqlcmd.Parameters.AddWithValue("@AdultCount", AdultCount);
                sqlcmd.Parameters.AddWithValue("@ChildCount", ChildCount);
                sqlcmd.Parameters.AddWithValue("@ChildAge", ChildAge);
                sqlcmd.Parameters.AddWithValue("@RoomNo", RoomNo);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsRoomDetails");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        //Insert traveler Guest Details
        public int insGuestDetails(string OrderID, string GType, string GTitle, string GFName, string GLName, string GPhoneNo, string GEmail, string Country, string State, string City, string Address, string PostalCode, int RoomNo, int ChildAge, string LoginID, string DOB)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsertGuestDetails");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@GType", GType);
                sqlcmd.Parameters.AddWithValue("@GTitle", GTitle);
                sqlcmd.Parameters.AddWithValue("@GFName", GFName);
                sqlcmd.Parameters.AddWithValue("@GLName", GLName);
                sqlcmd.Parameters.AddWithValue("@GPhoneNo", GPhoneNo);
                sqlcmd.Parameters.AddWithValue("@GEmail", GEmail);
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@State", State);
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@Address", Address);
                sqlcmd.Parameters.AddWithValue("@PinCode", PostalCode);
                sqlcmd.Parameters.AddWithValue("@RoomNo", RoomNo);
                sqlcmd.Parameters.AddWithValue("@ChildAge", ChildAge);
                sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
                sqlcmd.Parameters.AddWithValue("@DOB", DOB);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "insGuestDetails");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Check Ticketing Status and Refreshing page
        public int TicketingStatus_Refresh(string Orderid, string User_Id)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_TicketingStatus_Refresh");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", Orderid);
                sqlcmd.Parameters.AddWithValue("@User_Id", User_Id);
                i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "TicketingStatus_Refresh");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Insert Additional Guest Details
        public int InsAdditionalGuesttbl(string OrderID, int RoomNO, string FName, string LName, string PaxType, int ChildAge)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("InsAdditionalGuesttbl");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@RoomNO", RoomNO);
                sqlcmd.Parameters.AddWithValue("@FName", FName);
                sqlcmd.Parameters.AddWithValue("@LName", LName);
                sqlcmd.Parameters.AddWithValue("@PaxType", PaxType);
                sqlcmd.Parameters.AddWithValue("@ChildAge", ChildAge);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsAdditionalGuesttbl");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        // update Header of guest details
        public int UpdateHtlHeader(string OrderID, string PaymentMode, string status, string CustomerRemarkToHotel, string AnyotherData)
        {
            int i = 0;
            try//
            {
                sqlcmd = new SqlCommand("SP_HTL_UpdateHtlHeader_PreBook");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);
                sqlcmd.Parameters.AddWithValue("@status", status);
                sqlcmd.Parameters.AddWithValue("@CustomerRemarkToHotel", CustomerRemarkToHotel);
                sqlcmd.Parameters.AddWithValue("@AnyOtherData", AnyotherData);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateHtlHeader");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Get Addition guest Details
        public DataSet GetAddiGuest(string OrderID)
        {
            DataSet GuestDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("GetAddiGuest");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                GuestDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetAddiGuest");
            }
            finally { sqlcmd.Dispose(); }
            return GuestDS;
        }
        //Update Data after Hotel Booked
        public int UpdateHtlBooking(string OrderID, string BookingID, string ConfirmID, string Status, string HtlPhoneNo, string HtlFax, string HotelEmailId)
        {
            int i = 0;
            try
            {//@OrderID nvarchar(20),@Status nvarchar(20), @BookingID nvarchar(50), @ConfirmID nvarchar(50)='', @ContactNo nvarchar(50)='',  @HotelFax Nvarchar(50)='', @HotelEmailId Nvarchar(50)=''
                sqlcmd = new SqlCommand("SP_HTL_UpdateBooking");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@BookingID", BookingID);
                sqlcmd.Parameters.AddWithValue("@ConfirmID", ConfirmID);
                sqlcmd.Parameters.AddWithValue("@HotelPhoneNo", HtlPhoneNo);
                sqlcmd.Parameters.AddWithValue("@HotelFax", HtlFax);
                sqlcmd.Parameters.AddWithValue("@HotelEmailId", HotelEmailId);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateHtlBooking");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //After Booked Hotel Subtract Booked Amt from Agent balance 
        public double UpdateBalance(string UserId, double Amount)
        {
            double i = 0;
            try
            {
                sqlcmd = new SqlCommand("SubtractFromLimit");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@UserId", UserId);
                sqlcmd.Parameters.AddWithValue("@Amount", Amount);
                i = Convert.ToDouble(DBhelperLnb.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateBalance");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Get All Aegent List for Bind DropDownList in Report Page
        public DataSet GetAgencyDtl()
        {
            DataSet AgencyDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("GetAgency");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                AgencyDs = DBhelperLnb.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetAgencyDtl");
            }
            finally { sqlcmd.Dispose(); }
            return AgencyDs;
        }
        //Get One Agency Details
        public DataSet GetAgencyDetails(string UserId)
        {
            DataSet AgencyDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("AgencyDetails");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@UserId", UserId);
                AgencyDs = DBhelperLnb.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetAgencyDetails");
            }
            finally { sqlcmd.Dispose(); }
            return AgencyDs;
        }
        //Check Agent balance
        public int CheckBalance(string UserId, string Amt)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("CheckBalance");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@UserId", UserId);
                sqlcmd.Parameters.AddWithValue("@Amt", Amt);
                i = Convert.ToInt32(DBhelperLnb.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "CheckBalance");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Insert Data in Ledger Details
        public int insertLedgerDetails(string AgentID, string AgencyName, string InvoiceNo, string PnrNo, string TicketNo, string TicketingCarrier, string YatraAccountID, string AccountID, string ExecutiveID, string IPAddress,
        double Debit, double Credit, double Aval_Balance, string BookingType, string Remark, int PaxId)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("usp_insert_LedgerDetails");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@AgencyName", AgencyName);
                sqlcmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
                sqlcmd.Parameters.AddWithValue("@PnrNo", PnrNo);
                sqlcmd.Parameters.AddWithValue("@TicketNo", TicketNo);
                sqlcmd.Parameters.AddWithValue("@TicketingCarrier", TicketingCarrier);
                sqlcmd.Parameters.AddWithValue("@YatraAccountID", YatraAccountID);

                sqlcmd.Parameters.AddWithValue("@AccountID", AccountID);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@IPAddress", IPAddress);

                sqlcmd.Parameters.AddWithValue("@Debit", Debit);
                sqlcmd.Parameters.AddWithValue("@Credit", Credit);
                sqlcmd.Parameters.AddWithValue("@Aval_Balance", Aval_Balance);
                sqlcmd.Parameters.AddWithValue("@BookingType", BookingType);
                sqlcmd.Parameters.AddWithValue("@Remark", Remark);
                sqlcmd.Parameters.AddWithValue("@PaxId", PaxId);
                i = Convert.ToInt32(DBhelperLnb.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "insertLedgerDetails");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        //Hotel Ticket Copy
        public DataSet htlintsummary(string orderId, string ReqType)
        {
            DataSet htlsummryDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_GetHotelDetailData");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@orderId", orderId);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                htlsummryDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "htlintsummary");
            }
            finally { sqlcmd.Dispose(); }
            return htlsummryDs;
        }
        //get mailing details fro Look and book
        public DataSet GetMailingDetails(string department, string UserID)
        {
            DataSet mailDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_GETMAILINGCREDENTIAL_ITZ");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Department", department);
                sqlcmd.Parameters.AddWithValue("@UserID", UserID);
                mailDs = DBhelperLnb.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetMailingDetails");
            }
            finally { sqlcmd.Dispose(); }
            return mailDs;
        }
        /// <summary>
        /// Check Hotel Refund Status
        /// </summary>
        /// <param name="BookingID"></param>
        /// <param name="OrderId"></param>
        /// <returns></returns>

        public int CheckHtlRefuStaus(string BookingID, string OrderId)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_CheckHtlStaus");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderId", OrderId);
                sqlcmd.Parameters.AddWithValue("@BookingID", BookingID);
                i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "CheckHtlRefuStaus");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        /// <summary>
        /// Check Hotel Refund Status for  Hold By Agency
        /// </summary>
        /// <param name="BookingID"></param>
        /// <param name="OrderId"></param>
        /// <returns></returns>
        public int CheckHoldByAgencyStaus(string BookingID, string OrderId, string ReqType)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_CheckHoldByAgencyStaus");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderId", OrderId);
                sqlcmd.Parameters.AddWithValue("@BookingID", BookingID);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "CheckHoldByAgencyStaus");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Hotel Booking Report
        public DataSet HotelSearchRpt(string FromDate, string ToDate, string BookingID, string OrderID, string HtlName, string RoomName, string Trip, string AgentID, string CheckIn, string Status,
        string LoginID, string usertype)
        {
            DataSet HotelrptDs = new DataSet();
            try
            {
                if (Status == "Confirm" || Status == "Hold" || Status == "Rejected" || Status == "HoldByAgency" || Status == "ALL")
                    sqlcmd = new SqlCommand("SP_HTL_BookingReport");
                else
                    sqlcmd = new SqlCommand("SP_HTL_CancelReport");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@FromDate", FromDate);
                sqlcmd.Parameters.AddWithValue("@ToDate ", ToDate);
                sqlcmd.Parameters.AddWithValue("@BookingID ", BookingID);
                sqlcmd.Parameters.AddWithValue("@CheckIn ", CheckIn);
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@HotelName", HtlName);
                sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
                sqlcmd.Parameters.AddWithValue("@Trip", Trip);
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@usertype", usertype);
                sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
                HotelrptDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "HotelSearchRpt");
            }
            finally { sqlcmd.Dispose(); }
            return HotelrptDs;
        }
        public DataSet HotelRefundRpt(string FromDate, string ToDate, string BID, string OrderID, string HtlName, string Trip, string AgentID, string Status, string ExecutiveID, string LoginID,
        string usertype)
        {
            DataSet HotelrptRfndDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("HtlRefundRpt");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@FromDate", FromDate);
                sqlcmd.Parameters.AddWithValue("@ToDate ", ToDate);
                sqlcmd.Parameters.AddWithValue("@BID ", BID);
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@HtlName", HtlName);
                sqlcmd.Parameters.AddWithValue("@Trip", Trip);
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID ", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@usertype", usertype);
                sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
                HotelrptRfndDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "HotelRefundRpt");
            }
            finally { sqlcmd.Dispose(); }
            return HotelrptRfndDs;
        }
        //Hold Hotel Booking Report
        public DataSet HoldHotelSearchRpt(string FromDate, string ToDate, string BID, string OrderID, string HtlName, string RoomName, string Star, string AgentID, string PgFName, string Status,
        string LoginID, string usertype)
        {
            DataSet HotelrptHoldDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("GetHotelRpt");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@FromDate", FromDate);
                sqlcmd.Parameters.AddWithValue("@ToDate ", ToDate);
                sqlcmd.Parameters.AddWithValue("@BID ", BID);
                sqlcmd.Parameters.AddWithValue("@PgFName ", PgFName);
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@HtlName", HtlName);
                sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@usertype", usertype);
                sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
                HotelrptHoldDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "HoldHotelSearchRpt");
            }
            finally { sqlcmd.Dispose(); }
            return HotelrptHoldDs;
        }
        //Insert Hotel Markup
        public int InsHtlMarkup(string LoginId, string AgentId, string Country, string City, string Htltype, string MarkupType, string Star, string markupAmt, string Supplier, string GroupType, string ReqType, int Counter)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsGetHtlMarkup");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
                sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@TripType", Htltype);
                sqlcmd.Parameters.AddWithValue("@MarkupType", MarkupType);
                sqlcmd.Parameters.AddWithValue("@markupAmount", markupAmt);
                sqlcmd.Parameters.AddWithValue("@Supplier", Supplier);
                sqlcmd.Parameters.AddWithValue("@MarkupID", Counter);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                sqlcmd.Parameters.AddWithValue("@GroupType", GroupType);
                if (Counter == 0)
                    i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
                else
                    i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsHtlMarkup");
            }
            finally { sqlcmd.Dispose(); }
            return i;

        }
        //Check Ladger report Status and uplode Amount in Agent Account
        public double CheckLadgerStatus(string InvoceID, string AgentID, double Amt, string Status)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("CheckLedgerBookingType");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@orderid", InvoceID);
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@Amt", Amt);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                i = Convert.ToInt32(DBhelperLnb.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "CheckLadgerStatus");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Get markup City List
        public DataSet GetMrkCityList(string city, string country)
        {
            DataSet MrkCityDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HotelMarkup_CitySearch");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Country", country);
                sqlcmd.Parameters.AddWithValue("@City", city);
                MrkCityDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetMrkCityList");
            }
            finally { sqlcmd.Dispose(); }
            return MrkCityDs;
        }
        //Get markup Country List
        public DataSet GetCountry(string HtlType, string Country)
        {
            DataSet mrkCountryDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("GetCountry");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@HtlType", HtlType);
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                mrkCountryDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetCountry");
            }
            finally { sqlcmd.Dispose(); }
            return mrkCountryDs;
        }

        //Insert and Update Hotel Booking XML Logs
        public int SP_Htl_InsUpdBookingLog(string HTLOrderID, string HTLRequest, string HTLResponse, string AgentID, string ReqType)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_Htl_InsUpdBookingLog");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@HTLOrderID", HTLOrderID);
                sqlcmd.Parameters.AddWithValue("@HTLRequest", HTLRequest);
                sqlcmd.Parameters.AddWithValue("@HTLResponse", HTLResponse);
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "SP_Htl_InsUpdBookingLog");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Select Hotel Error Log and XML LOGS
        public DataSet SelectHotelLog(string top, string orderid, string AgentID, string Provider, string ReqType)
        {
            DataSet latilongids = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectHotelLog");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@top", top);
                sqlcmd.Parameters.AddWithValue("@orderid", orderid);
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@Provider", Provider);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                latilongids = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "SelectHotelLog");
            }
            finally { sqlcmd.Dispose(); }
            return latilongids;
        }
        public int UpdatePolicy(string orderid, string Policy)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("UpdateBookedHotelPolicy");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                sqlcmd.Parameters.AddWithValue("@Policy", Policy);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        public DataSet SelectHltRefund(string orderid)
        {
            DataSet RfndDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("GetCancellationDetail");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                RfndDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "SelectHltRefund");
            }
            finally { sqlcmd.Dispose(); }
            return RfndDS;
        }
        //Insert Data in Hoel Cancellation table
        public int InsHtlRefund(string orderid, string RequestBy, string Status, decimal CancelCharge, int ServiceCharge, int CancelNight, string FromCancelDate, string remark, string PgTitle, string PgFname, string PgLname, string PgEmail)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsRefundRequest");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderId", orderid);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@CancelNight", CancelNight);
                sqlcmd.Parameters.AddWithValue("@FromCancelDate", FromCancelDate);
                sqlcmd.Parameters.AddWithValue("@CancelCharge", CancelCharge);
                sqlcmd.Parameters.AddWithValue("@ServiceCharge", ServiceCharge);
                sqlcmd.Parameters.AddWithValue("@PgTitle", PgTitle);
                sqlcmd.Parameters.AddWithValue("@PgFirstName", PgFname);
                sqlcmd.Parameters.AddWithValue("@PgLastName", PgLname);
                sqlcmd.Parameters.AddWithValue("@PgEmail", PgEmail);
                sqlcmd.Parameters.AddWithValue("@RequestBy", RequestBy);
                sqlcmd.Parameters.AddWithValue("@Remark", remark);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsHtlRefund");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Update Hotel Refund
        public int UpdateHltRefund(string Status, string orderid, decimal RefundAmt, decimal CancelCharge, int ServiceCharge, string Remarks)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_UpdateRefund");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                sqlcmd.Parameters.AddWithValue("@RefundAmt", RefundAmt);
                sqlcmd.Parameters.AddWithValue("@CancelCharge", CancelCharge);
                sqlcmd.Parameters.AddWithValue("@ServiceCharge", ServiceCharge);
                sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateHltRefund");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Refund Details
        public DataSet HtlRefundDetail(string Status, string orderid, string ExecutiveID, string ReqType, string Remarks)
        {
            DataSet RfndDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_Htl_RefundDetail");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
                RfndDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "HtlRefundDetail");
            }
            finally { sqlcmd.Dispose(); }
            return RfndDS;
        }
        //Update Hotel Refund 
        public int HtlRefundUpdates(string Status, string orderid, string ExecutiveID, string ReqType, string Remarks)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_Htl_RefundDetail");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "HtlRefundUpdates");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Update Hotel Auto cancellation
        public int SP_HTL_AutoRefund(HotelCancellation HotelDetails, decimal cancelamt)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_AutoRefund");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Status", HotelDetails.CancelStatus);
                sqlcmd.Parameters.AddWithValue("@OrderID", HotelDetails.Orderid);
                sqlcmd.Parameters.AddWithValue("@RefundAmt", HotelDetails.RefundAmt);
                sqlcmd.Parameters.AddWithValue("@CancelCharge", cancelamt);

                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateHltRefund");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Refund Details
        //Update Hold Hotel Booking
        public int UpdateHrdHold(string OrderID, string BookingID, string ConfirmID, string Status, string ExecutiveID, string Remark)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_UpdateHrdHold");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@BookingID", BookingID);
                sqlcmd.Parameters.AddWithValue("@ConfirmID", ConfirmID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@Remark", Remark);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateHrdHold");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Reject Hotel Booking
        public int RejectHoldBooking(string Status, string orderid, decimal RefundAmt, string ExecutiveID, string Remarks)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_RejectHoldBooking");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
                sqlcmd.Parameters.AddWithValue("@RefundAmt", RefundAmt);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "RejectHoldBooking");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        // Checking Hotel is present in table or not for Hol Booking
        public DataSet CheckHotelForHold(string HtlCode, string RoomTypeID)
        {
            DataSet HoldhtlDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("CheckHotelForHold");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@HtlCode", HtlCode);
                sqlcmd.Parameters.AddWithValue("@RoomTypeID", RoomTypeID);
                HoldhtlDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "CheckHotelForHold");
            }
            finally { sqlcmd.Dispose(); }
            return HoldhtlDS;
        }
        //Update Hold booking 
        public int UpdateHoldHotelBooking(string OrderID, string Status)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("UpdateHoldHotelBooking");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateHoldHotelBooking");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Get Hold booking 
        public DataSet GetHoldHotel(string ExecutiveID, string Status, string Trip)
        {
            DataSet HoldhtlDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_GetHoldHotel");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Status", Status);
                sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
                sqlcmd.Parameters.AddWithValue("@Trip", Trip);
                HoldhtlDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHoldHotel");
            }
            finally { sqlcmd.Dispose(); }
            return HoldhtlDS;
        }
        //insert Hotel Error LOG 
        public static void InsertHotelErrorLog(Exception ex, string LoginID)
        {
            SqlCommand cmd = new SqlCommand("SP_HTL_InsertErrorLog");//ex.StackTrace
            SqlDatabase DBhelperErr = new SqlDatabase(ConfigurationManager.ConnectionStrings["HTLConnStr"].ConnectionString);
            try
            {
                System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
                string linenumber = "0", fileNames = "";
                if (trace != null)
                {
                    if (trace.FrameCount > 0 && trace.GetFrame(trace.FrameCount - 1).GetFileName() != null)
                    {
                        linenumber = (trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber()).ToString();
                        fileNames = (trace.GetFrame((trace.FrameCount - 1)).GetFileName()).ToString();
                    }
                    else
                        fileNames = ex.StackTrace;
                }
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PageName", fileNames + " - " + LoginID);
                cmd.Parameters.AddWithValue("@ErrorMessage", ex.Message);
                cmd.Parameters.AddWithValue("@LineNumber", linenumber);
                DBhelperErr.ExecuteNonQuery(cmd);
            }
            catch (Exception ex1)
            { }
            finally
            { cmd.Dispose(); }
        }
        //Get Hotel Commision not in use
        public HotelBooking GetHtlCommision(HotelBooking ObjBooking, string TypeID, decimal Amount)
        {
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SELECT_Commisiom");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Country", ObjBooking.Country);
                sqlcmd.Parameters.AddWithValue("@City", ObjBooking.CityName);
                sqlcmd.Parameters.AddWithValue("@Star", ObjBooking.StarRating);
                sqlcmd.Parameters.AddWithValue("@AgentId", ObjBooking.AgentID);
                sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
                sqlcmd.Parameters.AddWithValue("@Amount", Amount);
                sqlcmd.Parameters.AddWithValue("@Provider", ObjBooking.Provider);
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    ObjBooking.CommisionType = dbr["CommisionType"].ToString();
                    ObjBooking.CommisionPer = Convert.ToDecimal(dbr["CommisionPer"]);
                    ObjBooking.CommisionAmt = Convert.ToDecimal(dbr["CommisionAmt"]);
                    ObjBooking.AgentDebitAmt = Convert.ToDecimal(dbr["TotalAmount"]);
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHtlCommision");
            }
            finally { sqlcmd.Dispose(); }
            return ObjBooking;
        }
        //Get Hotel Commision Detail page breakups]
        public DataSet GetCommisionInRoomDetails(string Provider, string StarRating, string AgentID, decimal OrgCommission, decimal TotalAmount)
        {
            DataSet commisionDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SELECT_Commisiom");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Provider", Provider);
                sqlcmd.Parameters.AddWithValue("@Star", StarRating);
                sqlcmd.Parameters.AddWithValue("@AgentId", AgentID);
                //  sqlcmd.Parameters.AddWithValue("@AgentGroup", TypeID);
                sqlcmd.Parameters.AddWithValue("@OrgCommission", OrgCommission);
                sqlcmd.Parameters.AddWithValue("@TotalAmount", TotalAmount);
                commisionDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetCommisionInRoomDetails");
            }
            finally { sqlcmd.Dispose(); }
            return commisionDS;
        }
        public DataSet GetDetailsPageCommisionOld(string Country, string CityName, string StarRating, string AgentID, string TypeID, decimal Amount)
        {
            DataSet commisionDS = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SELECT_Commisiom");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@City", CityName);
                sqlcmd.Parameters.AddWithValue("@Star", StarRating);
                sqlcmd.Parameters.AddWithValue("@AgentId", AgentID);
                sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
                sqlcmd.Parameters.AddWithValue("@Amount", Amount);
                sqlcmd.Parameters.AddWithValue("@Provider", "");
                commisionDS = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetDetailsPageCommision");
            }
            finally { sqlcmd.Dispose(); }
            return commisionDS;
        }
        //Insert Hotel Commision
        public int InsHtlCommision(string LoginId, string AgentId, string Country, string City, string TypeID, string Star, string CommisionType, string CommisionPer, string ReqType, int Counter)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsGetCommision");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
                sqlcmd.Parameters.AddWithValue("@CommisionType", CommisionType);
                sqlcmd.Parameters.AddWithValue("@CommisionPer", CommisionPer);
                sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
                sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
                sqlcmd.Parameters.AddWithValue("@Counter", Counter);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                if (Counter == 0)
                    i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
                else
                    i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsHtlCommision");
            }
            finally { sqlcmd.Dispose(); }
            return i;

        }

        //Get Hotel Cach Back panal
        public DataSet GetHtlCachbackPanal(string LoginId, string AgentId, string Country, string City, string TypeID, string Star, string CommisionType, string CommisionPer, string ReqType, int Counter)
        {
            DataSet MrkDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsGetCommision");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
                sqlcmd.Parameters.AddWithValue("@CommisionType", CommisionType);
                sqlcmd.Parameters.AddWithValue("@CommisionPer", CommisionPer);
                sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
                sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
                sqlcmd.Parameters.AddWithValue("@Counter", Counter);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                MrkDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHtlCachbackPanal");
            }
            finally { sqlcmd.Dispose(); }
            return MrkDs;
        }

        //'Hotel Markup City country Search
        public List<CitySearch> GetMarkupCityCountryList(string city, string Country, List<CitySearch> CityList)
        {
            try
            {
                if (city == "")
                {
                    sqlcmd = new SqlCommand("SP_HTL_MarkupCountrySearch");
                    sqlcmd.Parameters.AddWithValue("@Country", Country);
                    sqlcmd.CommandType = CommandType.StoredProcedure;
                    var dbr = DBhelper.ExecuteReader(sqlcmd);
                    while (dbr.Read())
                    {
                        CityList.Add(new CitySearch { Country = dbr["Country"].ToString().Trim(), CountryCode = dbr["CountryCode"].ToString().Trim() });
                    }
                }
                else
                {
                    sqlcmd = new SqlCommand("SP_HTL_MarkupCitySearch");
                    sqlcmd.Parameters.AddWithValue("@City", city);
                    sqlcmd.Parameters.AddWithValue("@Country", Country);
                    sqlcmd.CommandType = CommandType.StoredProcedure;

                    var dbr = DBhelper.ExecuteReader(sqlcmd);
                    while (dbr.Read())
                    {
                        CityList.Add(new CitySearch { CityName = dbr["CityName"].ToString().Trim(), CityCode = dbr["CityCode"].ToString().Trim() });
                    }
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetMarkupCityCountryList");
            }
            finally { sqlcmd.Dispose(); }

            return CityList;
        }

        public List<HotelInformation> UAPIHotelInfoList(string CityCode, List<HotelInformation> HotelInfoList)
        {
            SqlCommand sqlcmd = new SqlCommand();
            try
            {
                sqlcmd.CommandText = "SP_HtlCitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", CityCode);
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
                sqlcmd.Parameters.AddWithValue("@ReqType", "UAPI");
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    HotelInfoList.Add(new HotelInformation
                    {
                        HotelName = dbr["ITEM_NM"].ToString(),
                        HotelCode = dbr["PPTY_ID_1G"].ToString(),
                        HotelChanCode = dbr["SUPPLR_CD"].ToString(),
                        Address = dbr["STREET_ADDR_LINE"].ToString() + "," + dbr["CITY_NAME"].ToString() + "-" + dbr["POSTAL_ZIP_CD"].ToString(),
                        Latitude = dbr["LAT_DEG"].ToString(),
                        Longitude = dbr["LONGT_DEG"].ToString()
                        //HotelName = dbr.GetString(dbr.GetOrdinal("ITEM_NM"),
                        //HotelCode = dbr.GetString(dbr.GetOrdinal("PPTY_ID_1G"),
                        //HotelChanCode = dbr.GetString(dbr.GetOrdinal("SUPPLR_CD")),

                        // PPTY_ID_1G,  ITEM_NM, SUPPLR_CD, STREET_ADDR_LINE, LAT_DEG, LONGT_DEG, CNTRY_CD, STPRVNC_CD, CTY_CD, COUNTY_NM, POSTAL_ZIP_CD, PPTY_PHONE_NBR, CITY_NAME

                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UAPIHotelInfoList");
            }
            finally { sqlcmd.Dispose(); }

            return HotelInfoList;
        }
        public List<HotelInformation> InnstantHotelInfoList(string CityCode, string Cityname, List<HotelInformation> HotelInfoList)
        {
            SqlCommand sqlcmd = new SqlCommand();
            try
            {
                sqlcmd.CommandText = "SP_HtlCitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", CityCode.Trim());
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
                sqlcmd.Parameters.AddWithValue("@ReqType", "Insstant");
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    HotelInfoList.Add(new HotelInformation
                    {
                        HotelName = dbr["HotelName"].ToString(),
                        HotelCode = dbr["HotelId"].ToString(),
                        // HotelChanCode = dbr["SUPPLR_CD"].ToString(),
                        Address = dbr["Address"].ToString() + "," + Cityname + "-" + dbr["Zip"].ToString(),
                        Latitude = dbr["Latitude"].ToString(),
                        Longitude = dbr["Longitude"].ToString(),
                        StarRating = dbr["Rating"].ToString(),
                        Location = dbr["Zone"].ToString(),
                        ThumbImg = dbr["ThumbURL"].ToString(),
                        HotelServices = dbr["Facilities"].ToString(),
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InnstantHotelInfoList");
            }
            finally { sqlcmd.Dispose(); }

            return HotelInfoList;
        }
        public List<HotelInformation> AgodaHotelInfoList(string CityCode, string ReqType, List<HotelInformation> HotelInfoList)
        {
            SqlCommand sqlcmd = new SqlCommand();
            try
            {
                sqlcmd.CommandText = "SP_HtlCitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", CityCode.Trim());
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    HotelInfoList.Add(new HotelInformation
                    {
                        HotelName = dbr["hotel_name"].ToString(),
                        HotelCode = dbr["Hotel_id"].ToString(),
                        // HotelChanCode = dbr["SUPPLR_CD"].ToString(),
                        Address = dbr["addressline1"].ToString() + "," + CityCode + "-" + dbr["zipcode"].ToString(),
                        Latitude = dbr["latitude"].ToString(),
                        Longitude = dbr["longitude"].ToString(),
                        StarRating = dbr["star_rating"].ToString(),
                        Location = "",
                        ThumbImg = dbr["photo1"].ToString(),
                        HotelServices = dbr["HotelAmenities"].ToString(),
                        HotelChanCode = ""
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "AgodaHotelInfoList");

            }
            finally { sqlcmd.Dispose(); }

            return HotelInfoList;
        }
        //Get Credit Card Details
        public DataSet CreditCardDetails()
        {
            DataSet HotelCardDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("CardDetail");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                HotelCardDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "");
            }
            finally { sqlcmd.Dispose(); }
            return HotelCardDs;
        }
        //Insert Latitude and Longitude
        public int InsLatLog(string SearchCity, object Hotelname, decimal lat, decimal lng, string ReqType)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_Htl_InsLatLog");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@SearchCity", SearchCity);
                sqlcmd.Parameters.AddWithValue("@Hotelname", Hotelname.ToString());
                sqlcmd.Parameters.AddWithValue("@lat", lat);
                sqlcmd.Parameters.AddWithValue("@lng", lng);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Select Latitude and Longitude
        public DataSet SelectLatLog(string SearchCity, string ReqType)
        {
            DataSet latLongDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_Htl_InsLatLog");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@SearchCity", SearchCity);
                sqlcmd.Parameters.AddWithValue("@Hotelname", "");
                sqlcmd.Parameters.AddWithValue("@lat", 0.0);
                sqlcmd.Parameters.AddWithValue("@lng", 0.0);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                latLongDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "");
            }
            finally { sqlcmd.Dispose(); }
            return latLongDs;
        }
        //Get Hotel Discount
        public int GetHtlDiscount(string Trip, string AgentID, string star, string City)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("HTLDiscount");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Trip", Trip);
                sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
                sqlcmd.Parameters.AddWithValue("@star", star);
                sqlcmd.Parameters.AddWithValue("@City", City);
                i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHtlDiscount");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        public DataSet GETHtlDiscount()
        {
            DataSet disds = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("GETHtlDiscount");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                disds = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GETHtlDiscount");
            }
            finally { sqlcmd.Dispose(); }
            return disds;
        }
        public int InsDiscount(string Trip, string City, string Agentid, string Star, string Percentage, int DisID, string ReqType)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("InsDiscount");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Trip", Trip);
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@Agentid", Agentid);
                sqlcmd.Parameters.AddWithValue("@Percentage", Percentage);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@DisID", DisID);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        //insert City for GTA
        public int insertGTACity(string City, string CityCode, string Country, string CountryCode)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("insertGTACity");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@CityName", City);
                sqlcmd.Parameters.AddWithValue("@CityCode", CityCode);
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@CountryCode", CountryCode);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHotelMarkup");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        //Insert Area Detals in table
        public int InsertGTAArea(string CityName, string CityCode, string Country)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("InsertGTAArea");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@CityName", CityName);
                sqlcmd.Parameters.AddWithValue("@CityCode", CityCode);
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsertGTAArea");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        public List<CitySearch> GetHotelCity(string cityCode, int maxlength)
        {
            SqlCommand sqlcmd = new SqlCommand();
            List<CitySearch> CityList = new List<CitySearch>();
            try
            {
                sqlcmd.CommandText = "SP_HLT_CitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@CITY_NAME", cityCode);
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
                sqlcmd.Parameters.AddWithValue("@ReqType", "CITY");
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    CityList.Add(new CitySearch
                    {
                        CityName = dbr["CityName"].ToString().Trim(),
                        CityCode = dbr["CityCode"].ToString().Trim(),
                        Country = dbr["Country"].ToString().Trim(),
                        CountryCode = dbr["CountryCode"].ToString().Trim(),
                        SearchType = dbr["SearchType"].ToString().Trim()
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHotelCity");
            }
            finally { sqlcmd.Dispose(); }

            return CityList;
        }
        public List<CitySearch> GetHotelnameSearch(string cityCode, string HLT_NAME)
        {
            sqlcmd = new SqlCommand("SP_HLT_CitySearch");
            List<CitySearch> fetchCity = new List<CitySearch>();
            try
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@CITY_NAME", cityCode);
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", HLT_NAME);
                sqlcmd.Parameters.AddWithValue("@ReqType", "HOTEL");
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    fetchCity.Add(new CitySearch
                    {
                        CityName = dbr["City"].ToString().Trim(),
                        Country = dbr["VendorName"].ToString().Trim(),
                        CountryCode = dbr["VendorID"].ToString().Trim()
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHotelnameSearch");
            }
            finally { sqlcmd.Dispose(); }

            return fetchCity.ToList();
        }
        //get Hotel Markup
        public DataTable GetHotelMarkup(string City, string Country, string Htltype)
        {
            DataSet MarkupDT = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_SelectHotelMarkup");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", City);
                sqlcmd.Parameters.AddWithValue("@Country", Country);
                sqlcmd.Parameters.AddWithValue("@TripType", Htltype);
                MarkupDT = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHotelMarkup");
            }
            finally { sqlcmd.Dispose(); }
            return MarkupDT.Tables[0];
        }
        //Insert data in Transacttion 
        public int InsertTransactionRepot(string pnr, string userid, string credit, string avalbal, string debit, string sector, string paxname, string flightno, string agencyname, string rm,
        string Status, string RoomName, string TotalPrice, string AgentMarkup)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("InsertTransReport");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@pnr", pnr);
                sqlcmd.Parameters.AddWithValue("@userid", userid);
                sqlcmd.Parameters.AddWithValue("@credit", credit);
                sqlcmd.Parameters.AddWithValue("@AvalBal", avalbal);
                sqlcmd.Parameters.AddWithValue("@Debit", debit);
                sqlcmd.Parameters.AddWithValue("@sector", sector);
                sqlcmd.Parameters.AddWithValue("@paxname", paxname);
                sqlcmd.Parameters.AddWithValue("@flightno", flightno);
                sqlcmd.Parameters.AddWithValue("@agencyname", agencyname);
                sqlcmd.Parameters.AddWithValue("@Rm", rm);
                sqlcmd.Parameters.AddWithValue("@status", Status);
                sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
                sqlcmd.Parameters.AddWithValue("@TotalPrice", TotalPrice);
                sqlcmd.Parameters.AddWithValue("@AgentMarkup", AgentMarkup);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        //get Recent Booked Hotel
        public DataSet RecentBookedHotel()
        {
            DataSet bookedHotelDT = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_RecentBooked");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                bookedHotelDT = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHotelMarkup");
            }
            finally { sqlcmd.Dispose(); }
            return bookedHotelDT;
        }
        public DataSet getCreditCardDetails(string HotelSupplier, DataSet dtCC)
        {
            try
            {
                sqlcmd = new SqlCommand("usp_Creditcard_Details");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Supplier", HotelSupplier);
                dtCC = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "getCreditCardDetails");
            }
            finally { sqlcmd.Dispose(); }
            return dtCC;
        }

        //Insert Hotel Commision
        public int InsUpdateHotelDiscount(string LoginId, string GroupType, string SupplierName, string SupplierCode, string Star, decimal MinCapvalue, decimal RetaintionPer, string ReqType, int Counter)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsGetUpdateHotelDiscount");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@SupplierName", SupplierName);
                sqlcmd.Parameters.AddWithValue("@SupplierCode", SupplierCode);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@MinCapvalue", MinCapvalue);
                sqlcmd.Parameters.AddWithValue("@RetaintionPer", RetaintionPer);
                sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
                sqlcmd.Parameters.AddWithValue("@AgentGroup", GroupType);
                sqlcmd.Parameters.AddWithValue("@Counter", Counter);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                if (Counter == 0)
                    i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
                else
                    i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "InsHtlCommision");
            }
            finally { sqlcmd.Dispose(); }
            return i;

        }

        //Get Hotel Cach Back panal
        public DataSet GetUpdateHotelDiscount(string LoginId, string GroupType, string SupplierName, string SupplierCode, string Star, decimal MinCapvalue, decimal RetaintionPer, string ReqType, int Counter)
        {
            DataSet MrkDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_InsGetUpdateHotelDiscount");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@SupplierName", SupplierName);
                sqlcmd.Parameters.AddWithValue("@SupplierCode", SupplierCode);
                sqlcmd.Parameters.AddWithValue("@Star", Star);
                sqlcmd.Parameters.AddWithValue("@MinCapvalue", MinCapvalue);
                sqlcmd.Parameters.AddWithValue("@RetaintionPer", RetaintionPer);
                sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
                sqlcmd.Parameters.AddWithValue("@AgentGroup", GroupType);
                sqlcmd.Parameters.AddWithValue("@Counter", Counter);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                MrkDs = DBhelper.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetHtlCachbackPanal");
            }
            finally { sqlcmd.Dispose(); }
            return MrkDs;
        }
        //@OrderId nVarchar(20), @CommissionTaxPercentage decimal(18,2), @CommissionRetaintionPer decimal(18,2),  @CommissionMinCapValue decimal(18,2),
        //	@AgentCommissionAmt decimal(18,2),@CommissionTaxAmt decimal(18,2),  @AdminCommissionAmt decimal(18,2), @SupplierCommissionAmt decimal(18,2), @SupplierCommissionPer decimal(18,2), @SupplierCommisionTaxIncluded nVarchar(20))
        //Insert Hotel Commision
        public int UpdateHotelDiscountInCheckout(string OrderId, decimal MinCapvalue, decimal RetaintionPer, decimal TDSPer, decimal TDSAmt, decimal AgentCommissionAmt, decimal AdminCommissionAmt,
            decimal GSTPer, decimal GSTAmt, decimal SupplierCommissionAmt, decimal SupplierCommissionPer, string SupplierCommisionTaxIncluded)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL_Update_CheckoutCommisiom");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@OrderId", OrderId);
                sqlcmd.Parameters.AddWithValue("@CommissionMinCapValue", MinCapvalue);
                sqlcmd.Parameters.AddWithValue("@CommissionRetaintionPer", RetaintionPer);
                sqlcmd.Parameters.AddWithValue("@TDSPer", TDSPer);
                sqlcmd.Parameters.AddWithValue("@TDSAmt", TDSAmt);
                sqlcmd.Parameters.AddWithValue("@GSTPer", GSTPer);
                sqlcmd.Parameters.AddWithValue("@GSTAmt", GSTAmt);
                sqlcmd.Parameters.AddWithValue("@AgentCommissionAmt", AgentCommissionAmt);
                sqlcmd.Parameters.AddWithValue("@AdminCommissionAmt", AdminCommissionAmt);
                sqlcmd.Parameters.AddWithValue("@SupplierCommissionAmt", SupplierCommissionAmt);
                sqlcmd.Parameters.AddWithValue("@SupplierCommissionPer", SupplierCommissionPer);
                sqlcmd.Parameters.AddWithValue("@SupplierCommisionTaxIncluded", SupplierCommisionTaxIncluded);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "UpdateHotelDiscountInCheckout");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        public DataSet GetCorporateCreditCard(string Trip, string Product, string HotelSupplier)
        {
            DataSet CCDs = new DataSet();
            try
            {
                sqlcmd = new SqlCommand("USP_CREDITCARDDETAILS");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@Pcc", "");
                sqlcmd.Parameters.AddWithValue("@Airline", "");
                sqlcmd.Parameters.AddWithValue("@Trip", Trip);
                sqlcmd.Parameters.AddWithValue("@CardType", "");
                sqlcmd.Parameters.AddWithValue("@HName", "");
                sqlcmd.Parameters.AddWithValue("@CardNumber", "");
                sqlcmd.Parameters.AddWithValue("@ExpiryDate", "");
                sqlcmd.Parameters.AddWithValue("@Status", 0);
                sqlcmd.Parameters.AddWithValue("@ActionType", "GetCardBooking");
                sqlcmd.Parameters.AddWithValue("@Product", Product);
                sqlcmd.Parameters.AddWithValue("@SupplierName", HotelSupplier);
                CCDs = DBhelperLnb.ExecuteDataSet(sqlcmd);
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GetCorporateCreditCard");
            }
            finally
            {
                sqlcmd.Dispose();
            }
            return CCDs;

        }
        public CurrencyRateSetting CurrancyDetails(CurrencyRateSetting objcurrancy)
        {
            List<CurrencyRate> currancyinfo = new List<CurrencyRate>();
            SqlCommand sqlcmd = new SqlCommand("SP_HTL_InsertCurrencyDetails");
            try
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@CountryName", objcurrancy.CountryName);
                sqlcmd.Parameters.AddWithValue("@CountryCode", objcurrancy.CountryCode);
                sqlcmd.Parameters.AddWithValue("@CurrancyCode", objcurrancy.CurrancyCode);
                sqlcmd.Parameters.AddWithValue("@ExchangeRate", objcurrancy.ExchangeRate);
                sqlcmd.Parameters.AddWithValue("@UpdateBy", objcurrancy.UpdatedBy);
                sqlcmd.Parameters.AddWithValue("@Reqtype", objcurrancy.ReqType);
                sqlcmd.Parameters.AddWithValue("@IPAddress", objcurrancy.IPAddress);
                sqlcmd.Parameters.AddWithValue("@ID", objcurrancy.IDS);
                var dbr = DBhelperLnb.ExecuteReader(sqlcmd);

                while (dbr.Read())
                {
                    currancyinfo.Add(new CurrencyRate
                    {
                        CountryName = dbr["CountryName"] != null ? dbr["CountryName"].ToString() : "",
                        CountryCode = dbr["CountryCode"] != null ? dbr["CountryCode"].ToString() : "",
                        CurrancyCode = dbr["CurrancyCode"] != null ? dbr["CurrancyCode"].ToString() : "",
                        ExchangeRate = dbr["ExchangeRate"] != null ? Convert.ToDecimal(dbr["ExchangeRate"]) : 0,
                        IDS = Convert.ToInt32(dbr["id"]),
                        UpdateDate = dbr["UpdateDate"].ToString()
                    });
                }
                objcurrancy.CurrancyList = currancyinfo;
            }
            catch (Exception ex)
            {

            }
            finally { sqlcmd.Dispose(); }
            return objcurrancy;
        }
        public string InsertUpdateCurrancyDetails(CurrencyRateSetting objcurrancy)
        {
            string i = "";
            SqlCommand sqlcmd = new SqlCommand("SP_HTL_InsertCurrencyDetails");
            try
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@CountryName", objcurrancy.CountryName);
                sqlcmd.Parameters.AddWithValue("@CountryCode", objcurrancy.CountryCode);
                sqlcmd.Parameters.AddWithValue("@CurrancyCode", objcurrancy.CurrancyCode);
                sqlcmd.Parameters.AddWithValue("@ExchangeRate", objcurrancy.ExchangeRate);
                sqlcmd.Parameters.AddWithValue("@UpdateBy", objcurrancy.UpdatedBy);
                sqlcmd.Parameters.AddWithValue("@Reqtype", objcurrancy.ReqType);
                sqlcmd.Parameters.AddWithValue("@IPAddress", objcurrancy.IPAddress);
                sqlcmd.Parameters.AddWithValue("@ID", objcurrancy.IDS);
                i = DBhelperLnb.ExecuteScalar(sqlcmd).ToString();
            }
            catch (Exception ex)
            {

            }
            finally { sqlcmd.Dispose(); }
            return i;
        }

        public int INS_GDSCreditcard_Details(HotelBooking ObjBooking)
        {
            int i = 0;
            try
            {
                sqlcmd = new SqlCommand("SP_HTL__INS_SEL_Creditcard_Details");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@HotelSupplier", ObjBooking.Provider);
                sqlcmd.Parameters.AddWithValue("@CardType", ObjBooking.GDSCreditCardInfo.CardType);
                sqlcmd.Parameters.AddWithValue("@CardNo", ObjBooking.GDSCreditCardInfo.CardNo);
                sqlcmd.Parameters.AddWithValue("@ExpMonth", ObjBooking.GDSCreditCardInfo.ExpMonth);
                sqlcmd.Parameters.AddWithValue("@ExpYear", ObjBooking.GDSCreditCardInfo.ExpYear);
                sqlcmd.Parameters.AddWithValue("@CVV", ObjBooking.GDSCreditCardInfo.CardCVV);
                sqlcmd.Parameters.AddWithValue("@holderName", ObjBooking.GDSCreditCardInfo.holderName);
                sqlcmd.Parameters.AddWithValue("@Orderid", ObjBooking.Orderid);
                sqlcmd.Parameters.AddWithValue("@AgentID", ObjBooking.AgentID);
                i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "INS_GDSCreditcard_Details");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        public CreditCardDetails GET_GDSCreditcard_Details(CreditCardDetails objcardDetails)
        {
            SqlCommand sqlcmd = new SqlCommand("SP_HTL__INS_SEL_Creditcard_Details");
            try
            {
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@HotelSupplier", objcardDetails.HotelSupplier);
                sqlcmd.Parameters.AddWithValue("@CardType", "");
                sqlcmd.Parameters.AddWithValue("@CardNo", "");
                sqlcmd.Parameters.AddWithValue("@ExpMonth", "");
                sqlcmd.Parameters.AddWithValue("@ExpYear", "");
                sqlcmd.Parameters.AddWithValue("@CVV", 0);
                sqlcmd.Parameters.AddWithValue("@holderName", "");
                sqlcmd.Parameters.AddWithValue("@Orderid", objcardDetails);
                sqlcmd.Parameters.AddWithValue("@AgentID", "");
                var dbr = DBhelper.ExecuteReader(sqlcmd);

                while (dbr.Read())
                {
                    objcardDetails.CardType = dbr["CardType"] != null ? dbr["CardType"].ToString() : "";
                    objcardDetails.CardNo = dbr["CardNo"] != null ? dbr["CardNo"].ToString() : "";
                    objcardDetails.ExpMonth = dbr["ExpMonth"] != null ? dbr["ExpMonth"].ToString() : "";
                    objcardDetails.ExpYear = dbr["ExpYear"] != null ? dbr["ExpYear"].ToString() : "";
                    objcardDetails.CardCVV = Convert.ToInt32(dbr["CardCVV"]);
                    objcardDetails.holderName = dbr["holderName"] != null ? dbr["holderName"].ToString() : "";
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "GET_GDSCreditcard_Details");
            }
            finally { sqlcmd.Dispose(); }
            return objcardDetails;
        }

        #region Seeingo work 11_9_2018
        public int INS_HotelServiseCredencials(string Provider, string Trip, string UserId, string Password, string propertyid, bool Enables, string ReqType, int CredencialID)
        {
            int i = 0;
            try
            {//UserId, Password, propertyid, provider, URLTYPE, Enables, Trip
                sqlcmd = new SqlCommand("USP_Htl_InsetHotelCredencials");
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@UserId", UserId);
                sqlcmd.Parameters.AddWithValue("@Password", Password);
                sqlcmd.Parameters.AddWithValue("@propertyid", propertyid);
                sqlcmd.Parameters.AddWithValue("@Provider", Provider);
                sqlcmd.Parameters.AddWithValue("@Enables", Enables);
                sqlcmd.Parameters.AddWithValue("@Trip", Trip);
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                sqlcmd.Parameters.AddWithValue("@CredencialID", CredencialID);
                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "INS_GDSCreditcard_Details");
            }
            finally { sqlcmd.Dispose(); }
            return i;
        }
        #endregion

        public List<HotelInformation> OYOHotelInfoList(string Cityname, string ReqType, List<HotelInformation> HotelInfoList)
        {
            SqlCommand sqlcmd = new SqlCommand();
            try
            {
                sqlcmd.CommandText = "SP_HtlCitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", Cityname);
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    HotelInfoList.Add(new HotelInformation
                    {
                        HotelName = dbr["HotelName"].ToString(),
                        HotelCode = dbr["HotelID"].ToString(),
                        Address = dbr["Address"].ToString() + "-" + dbr["ZipCode"].ToString(),
                        Latitude = dbr["Latitude"].ToString(),
                        Longitude = dbr["Longtitude"].ToString(),
                        ThumbImg = dbr["ImagesURL"].ToString(),
                        HotelServices = dbr["Amenities"].ToString(),
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "OYOHotelInfoList");
            }
            finally { sqlcmd.Dispose(); }

            return HotelInfoList;
        }
        public List<HotelInformation> OYOHotelInfoList_Room(string hotelCode, string ReqType, List<HotelInformation> HotelInfoList)
        {
            SqlCommand sqlcmd = new SqlCommand();
            try
            {
                sqlcmd.CommandText = "SP_HtlCitySearch";
                sqlcmd.CommandType = CommandType.StoredProcedure;
                sqlcmd.Parameters.AddWithValue("@City", hotelCode);
                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
                var dbr = DBhelper.ExecuteReader(sqlcmd);
                while (dbr.Read())
                {
                    HotelInfoList.Add(new HotelInformation
                    {
                        HotelName = dbr["HotelName"].ToString(),
                        HotelCode = dbr["HotelID"].ToString(),
                        Address = dbr["Address"].ToString() + "-" + dbr["ZipCode"].ToString(),
                        Latitude = dbr["Latitude"].ToString(),
                        Longitude = dbr["Longtitude"].ToString(),
                        ThumbImg = dbr["ImagesURL"].ToString(),
                        HotelDiscription = dbr["Description"].ToString(),
                        HotelServices = dbr["Amenities"].ToString(),
                        CheckInPolicy = dbr["CheckInPolicies"].ToString(),
                        CheckOutPolicy = dbr["CheckOutPolicies"].ToString(),
                        AllImages = dbr["AllImages"].ToString(),
                    });
                }
            }
            catch (Exception ex)
            {
                InsertHotelErrorLog(ex, "OYOHotelInfoList_Room");
            }
            finally { sqlcmd.Dispose(); }

            return HotelInfoList;
        }
    }
}




//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Data;
//using System.Data.SqlClient;
//using System.Configuration;
//using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
//using Microsoft.Practices.EnterpriseLibrary.Data;
//using HotelShared;
//namespace HotelDAL
//{
//    public class HotelDA
//    {
//        SqlCommand sqlcmd;
//        private SqlDatabase DBhelper; private SqlDatabase DBhelperLnb;
//        public HotelDA()
//        {
//            DBhelper = new SqlDatabase(ConfigurationManager.ConnectionStrings["HTLConnStr"].ConnectionString);
//            DBhelperLnb = new SqlDatabase(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
//        }
//        //'Hotel City Search
//        public List<CitySearch> GetCityList(string City)
//        {
//            SqlCommand sqlcmd = new SqlCommand();
//            List<CitySearch> CityList = new List<CitySearch>();
//            try
//            {
//                sqlcmd.CommandText = "SP_HtlCitySearch";
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@City", City);
//                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
//                sqlcmd.Parameters.AddWithValue("@ReqType", "CITY");
//                var dbr = DBhelper.ExecuteReader(sqlcmd);
//                while (dbr.Read())
//                {
//                    CityList.Add(new CitySearch
//                    {
//                        CityName = dbr["CityName"].ToString().Trim(),
//                        CityCode = dbr["CityCode"].ToString().Trim(),
//                        Country = dbr["Country"].ToString().Trim(),
//                        CountryCode = dbr["CountryCode"].ToString().Trim(),
//                        SearchType = dbr["SearchType"].ToString().Trim(),
//                        RegionID = dbr["RegionID"].ToString().Trim(),
//                        NoOfHotel = dbr["NoOfHotel"].ToString().Trim(),
                        
//                       // Availability_0Star = Convert.ToBoolean(dbr["Availability_ZeroStar"])
//                    });
//                }
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetCityList");
//            }
//            finally { sqlcmd.Dispose(); }

//            return CityList;
//        }
//        //'Hotel City Search for International and domestic
//        public List<CitySearch> GetCityList2(string City, string HLT_NAME)
//        {
//            SqlCommand sqlcmd = new SqlCommand();
//            List<CitySearch> CityList = new List<CitySearch>();
//            try
//            {
//                sqlcmd.CommandText = "SP_HtlCitySearch";
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@City", City);
//                sqlcmd.Parameters.AddWithValue("@HLT_NAME", HLT_NAME);
//                sqlcmd.Parameters.AddWithValue("@ReqType", "HOTEL");
//                var dbr = DBhelper.ExecuteReader(sqlcmd);
//                while (dbr.Read())
//                {
//                    CityList.Add(new CitySearch
//                    {
//                        CityName = dbr["VendorName"].ToString().Trim(),
//                        CityCode = dbr["VendorID"].ToString().Trim(),
//                    });
//                }
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetCityList2");
//            }
//            finally { sqlcmd.Dispose(); }

//            return CityList;
//        }
//        //get Hotel Credencials
//        public HotelSearch GetCredentials(HotelSearch SearchDetail, string Reqtype)
//        {
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectCredential");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@Type", Reqtype);
//                //sqlcmd.Parameters.AddWithValue("@Provider", SearchDetail.Provider);
//                var dbr = DBhelper.ExecuteReader(sqlcmd);
//                while (dbr.Read())
//                {
//                    switch (dbr["provider"].ToString().Trim())
//                    {
//                        case "GTA":
//                                SearchDetail.GTAURL = dbr["Url"].ToString().Trim();
//                                SearchDetail.GTAClintID = dbr["UserId"].ToString().Trim();
//                                SearchDetail.GTAPassword = dbr["password"].ToString().Trim();
//                                SearchDetail.GTAEmailAddress = dbr["propertyid"].ToString().Trim();
//                                SearchDetail.GTATrip = dbr["trip"].ToString().Trim();
//                            break;
//                        case "TG":
//                                SearchDetail.TGUrl = dbr["Url"].ToString().Trim();
//                                SearchDetail.TGUsername = dbr["Username"].ToString().Trim();
//                                SearchDetail.TGPassword = dbr["password"].ToString().Trim();
//                                SearchDetail.TGPropertyId = dbr["propertyid"].ToString().Trim();
//                                SearchDetail.TGTrip = dbr["trip"].ToString().Trim();
//                            break;
//                        case "ROOMXML":
//                            SearchDetail.RoomXMLURL = dbr["Url"].ToString().Trim();
//                            SearchDetail.RoomXMLUserID = dbr["Username"].ToString().Trim();
//                            SearchDetail.RoomXMLPassword = dbr["password"].ToString().Trim();
//                            SearchDetail.RoomXMLOrgID = dbr["propertyid"].ToString().Trim();
//                            SearchDetail.RoomXMLTrip = dbr["trip"].ToString().Trim();
//                            break;
//                        case "EX":
//                            SearchDetail.EXURL = dbr["Url"].ToString().Trim();
//                            SearchDetail.EXAPIKEY = dbr["Username"].ToString().Trim();
//                            SearchDetail.EXCID = dbr["password"].ToString().Trim();
//                            SearchDetail.EXSecretKey = dbr["propertyid"].ToString().Trim();
//                            SearchDetail.EXTrip = dbr["trip"].ToString().Trim();
//                            break;
//                        case "RZ":
//                            SearchDetail.RezNextUrl = dbr["Url"].ToString().Trim();
//                            SearchDetail.RezNextUserName = dbr["Username"].ToString().Trim();
//                            SearchDetail.RezNextPassword = dbr["password"].ToString().Trim();
//                            SearchDetail.RezNextSystemId = dbr["propertyid"].ToString().Trim();
//                            SearchDetail.RezNextTrip = dbr["trip"].ToString().Trim();
//                            break;
//                        case "TB":
//                            SearchDetail.TBOURL = dbr["Url"].ToString().Trim();
//                            SearchDetail.TBOUserID = dbr["UserId"].ToString().Trim();
//                            SearchDetail.TBOPassword  = dbr["password"].ToString().Trim();
//                            SearchDetail.TBOOrgID  = dbr["propertyid"].ToString().Trim();
//                            SearchDetail.TBOTrip  = dbr["trip"].ToString().Trim();
//                            SearchDetail.TBOIP = dbr["IPaddress"].ToString().Trim();
//                            break;
//                    }
//                }  
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetCredentials");
//            }
//            finally { sqlcmd.Dispose(); }
//            //servicetax
//           // SearchDetail = HotelServicetaxNew(SearchDetail);
//            SearchDetail.TG_servicetax = 0;
//            SearchDetail.GTA_servicetax = 0;
            
//            return SearchDetail;
//        }
//        //get Credentials Details
//        public HotelBooking GetBookingCredentials(HotelBooking BookingDetail, string Reqtype)
//        {
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectCredential");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@Type", Reqtype);
//                //sqlcmd.Parameters.AddWithValue("@Provider", BookingDetail.Provider);
//                var dbr = DBhelper.ExecuteReader(sqlcmd);
                
//                while (dbr.Read())
//                {
//                    if (dbr["provider"].ToString() == "TGBooking")
//                    {
//                        BookingDetail.TGUrl = dbr["Url"].ToString().Trim();
//                        BookingDetail.TGUsername = dbr["Username"].ToString().Trim();
//                        BookingDetail.TGPassword = dbr["password"].ToString().Trim();
//                        BookingDetail.TGPropertyId = dbr["propertyid"].ToString().Trim();
//                    }
//                    else if (dbr["provider"].ToString() == "EXBooking")
//                    {
//                        BookingDetail.EXURL = dbr["Url"].ToString().Trim();
//                        BookingDetail.EXAPIKEY = dbr["Username"].ToString().Trim();
//                        BookingDetail.EXCID = dbr["password"].ToString().Trim();
//                        BookingDetail.EXSecretKey = dbr["propertyid"].ToString().Trim();
//                    }
//                    else if (dbr["provider"].ToString() == "EX")
//                    {
//                        BookingDetail.EXURL = dbr["Url"].ToString().Trim();
//                        BookingDetail.EXAPIKEY = dbr["Username"].ToString().Trim();
//                        BookingDetail.EXCID = dbr["password"].ToString().Trim();
//                        BookingDetail.EXSecretKey = dbr["propertyid"].ToString().Trim();
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetBookingCredentials");
//            }
//            finally { sqlcmd.Dispose(); }
//            return BookingDetail;
//        }
//        //get Credentials Details
//        public HotelCancellation GetCancleCredentials(HotelCancellation BookingDetail)
//        {
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectCredential");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@Type", BookingDetail.Provider);
//                var dbr = DBhelper.ExecuteReader(sqlcmd);

//                while (dbr.Read())
//                {  
//                        BookingDetail.HotelUrl = dbr["Url"].ToString().Trim();
//                        BookingDetail.Can_UserID = dbr["Username"].ToString().Trim();
//                        BookingDetail.Can_Password = dbr["password"].ToString().Trim();
//                        BookingDetail.Can_PropertyId = dbr["propertyid"].ToString().Trim();
//                }
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetCancleCredentials");
//            }
//            finally { sqlcmd.Dispose(); }
//            return BookingDetail;
//        }
//        //Select Hotel Service Tax for all provider
//        public HotelSearch HotelServicetaxNew(HotelSearch SearchDetail)
//        {
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@CurrencyValue", 0);
//                sqlcmd.Parameters.AddWithValue("@ReqType", "ServiceTax");
//                var dbr = DBhelper.ExecuteReader(sqlcmd);
//                while (dbr.Read())
//                {
//                    switch (dbr["HotelType"].ToString().Trim())
//                    {
//                        case "GTA":
//                            SearchDetail.GTA_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
//                            break;
//                        case "TG":
//                            SearchDetail.TG_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
//                            break;
//                        case "ROOMXML":
//                            SearchDetail.ROOMXML_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
//                            break;
//                        case "EX":
//                            SearchDetail.EX_servicetax = Convert.ToDecimal(dbr["TaxPercent"].ToString().Trim());
//                            break;
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "HotelServicetaxNew");
//            }
//            finally { sqlcmd.Dispose(); }
//            return SearchDetail;
//        }
//        //Select Hotel Service Tax
//        public decimal HotelServicetax(string HotelType)
//        {
//            decimal i = 0;
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//               // sqlcmd.Parameters.AddWithValue("@HotelType", HotelType);
//                sqlcmd.Parameters.AddWithValue("@CurrencyValue", 0);
//                sqlcmd.Parameters.AddWithValue("@ReqType", "ServiceTax");
//                i = Convert.ToDecimal(DBhelper.ExecuteScalar(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "HotelServicetax");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//        }
//        // Update Currancy value
//        public int UpdateCurrancyValue(decimal CurrancyValue)
//        {
//            int i = 0;
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//             //   sqlcmd.Parameters.AddWithValue("@HotelType", "");
//                sqlcmd.Parameters.AddWithValue("@CurrencyValue", CurrancyValue);
//                sqlcmd.Parameters.AddWithValue("@ReqType", "UpdateCurrecy");
//                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "UpdateCurrancyValue");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//        }
//        //Select Currancy value 
//        public decimal SelectCurrancyValue()
//        {
//            decimal i = 0;
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectServicetax");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//             //   sqlcmd.Parameters.AddWithValue("@HotelType", HotelStatus.International.ToString());
//                sqlcmd.Parameters.AddWithValue("@CurrencyValue", 0);
//                sqlcmd.Parameters.AddWithValue("@ReqType", "SelectCurrecy");
//                i = Convert.ToDecimal(DBhelper.ExecuteScalar(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "SelectCurrancyValue");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//        }
//        //Get Hotel Markup
//        public DataSet GetHtlMarkup(string LoginId, string AgentId, string Country, string City, string Star, string Htltype, string MarkupType, string markupAmt, int Counter, string ReqType)
//        {
//            DataSet MrkDs = new DataSet();
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_InsGetHtlMarkup");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
//                sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
//                sqlcmd.Parameters.AddWithValue("@Country", Country);
//                sqlcmd.Parameters.AddWithValue("@City", City);
//                sqlcmd.Parameters.AddWithValue("@Star", Star);
//                sqlcmd.Parameters.AddWithValue("@TripType", Htltype);
//                sqlcmd.Parameters.AddWithValue("@MarkupType", MarkupType);
//                sqlcmd.Parameters.AddWithValue("@MarkupAmount", markupAmt);
//                sqlcmd.Parameters.AddWithValue("@MarkupId", Counter);
//                sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                MrkDs = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetHtlMarkup");
//            }
//            finally { sqlcmd.Dispose(); }
//            return MrkDs;
//        }

//        //generate Orderid
//        public string GetOrderID()
//        {
//            string OrderID = "";
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_GetOrderID");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                OrderID += DBhelper.ExecuteScalar(sqlcmd).ToString();
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetOrderID");
//            }
//            finally
//            { sqlcmd.Dispose(); }

//            return OrderID;
//        }
//        //get Hotel Overviw
//        public DataTable GetHotelOverview(string City, string HotelCode)
//        {
//            DataSet Hotelds = new DataSet();
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectHotelOverview");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@City", City);
//                sqlcmd.Parameters.AddWithValue("@VendorID", HotelCode);
//                Hotelds = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetHotelOverview");
//            }
//            finally { sqlcmd.Dispose(); }
//            return Hotelds.Tables[0];
//        }
//        //get Room Discription
//        public DataTable GetRoomDiscription(string Hotelcode)
//        {
//            DataSet RoomDT = new DataSet();
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectRoomDiscription");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@Hotelcode", Hotelcode);
//                RoomDT = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetRoomDiscription");
//            }
//            finally { sqlcmd.Dispose(); }
//            return RoomDT.Tables[0];
//        }
//        //get Hotel Services
//        public DataTable GetHotelServices(string HotelCode, string AmenityType)
//        {

//            DataSet HotelDS = new DataSet();
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_HotelFacilities");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@HotelCode", HotelCode);
//                sqlcmd.Parameters.AddWithValue("@AmenityType", AmenityType);
//                HotelDS = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetHotelServices");
//            }
//            finally { sqlcmd.Dispose(); }
//            return HotelDS.Tables[0];
//        }
//        //get Hotel Services and Image and Attraction
//        public DataSet GetHotelImageandAround(string HotelCode)
//        {
//            sqlcmd = new SqlCommand("SP_HTL_SelectHotelService");
//            DataSet HotelDS = new DataSet();
//            try
//            {
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@HotelCode", HotelCode);
//                sqlcmd.Parameters.AddWithValue("@AmenityType", "");
//                HotelDS = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "");
//            }
//            finally { sqlcmd.Dispose(); }
//            return HotelDS;
//        }
//        ////get RoomDiscription
//        //public DataSet GetRoomDiscription(string HotelCode)
//        //{
//        //    sqlcmd = new SqlCommand("SP_SELECT_HOTEL_DETALS");
//        //    DataSet HotelDS = new DataSet();
//        //    try
//        //    {
//        //        sqlcmd.CommandType = CommandType.StoredProcedure;
//        //        sqlcmd.Parameters.AddWithValue("@venderID", HotelCode);
//        //        sqlcmd.Parameters.AddWithValue("@ref", "RoomDescription");
//        //        HotelDS = DBhelper.ExecuteDataSet(sqlcmd);
//        //    }
//        //    catch (Exception ex)
//        //    {
//        //        InsertHotelErrorLog(ex, "");
//        //    }
//        //    finally { sqlcmd.Dispose(); }
//        //    return HotelDS;
//        //}
//        //'Insert Data in Hotel Heder table
//        public int InsHtlHrdDetails(string OrderID, string Status, string HtlCode, string RoomCode, string StarRating, decimal TotalCost, decimal NetCost, decimal Tax, decimal ExtraGuestTax, decimal BaseRate,
//           decimal ExchangeRate, decimal AdminMrk, decimal AgentMarkup, string AdminMrkType, string AgentMrkType, decimal AdminMrkPercent, decimal AgtMrkPercent, String DiscountMsg, decimal DiscountAmt,
//            string TripType, string Currency, string Country, string City, string Address, string PostalCode, string CheckIN, string CheckOut, int RoomCount, int NightCount, int AdtCount, int ChildCount,
//         string IP, string AgentID, string AgencyName, decimal ServiceTax, decimal ServiceTaxAmt, decimal VServiceTaxAmt, string provider, decimal CommisionPer, decimal CommisionAmt, string CommisionType)
//         {
//                int i = 0;
//                SqlCommand sqlcmd = new SqlCommand("SP_HTL_InsertBookingHeader");
//                try
//                {
//                    sqlcmd.CommandType = CommandType.StoredProcedure;
//                     sqlcmd.Parameters.AddWithValue("@Orderid", OrderID);                        
//                    sqlcmd.Parameters.AddWithValue("@Status", Status);
//                    sqlcmd.Parameters.AddWithValue("@TripType", TripType);                 
//                    sqlcmd.Parameters.AddWithValue("@DiscountAmt", DiscountAmt);
//                    sqlcmd.Parameters.AddWithValue("@DiscounttMsg", DiscountMsg);
//                    sqlcmd.Parameters.AddWithValue("@TotalCost", TotalCost);
//                    sqlcmd.Parameters.AddWithValue("@NetCost", NetCost);
//                    sqlcmd.Parameters.AddWithValue("@BaseRate", BaseRate);
//                    sqlcmd.Parameters.AddWithValue("@Tax", Tax);
//                    sqlcmd.Parameters.AddWithValue("@ExtraGuestTax", ExtraGuestTax);
//                    sqlcmd.Parameters.AddWithValue("@ExchangeRate", ExchangeRate);
//                    sqlcmd.Parameters.AddWithValue("@AdminMrkPer", AdminMrkPercent);                   
//                    sqlcmd.Parameters.AddWithValue("@AdminMrkAmt", AdminMrk);
//                    sqlcmd.Parameters.AddWithValue("@AdminMrkType", AdminMrkType);
//                    sqlcmd.Parameters.AddWithValue("@AgentMrkAmt", AgentMarkup);
//                    sqlcmd.Parameters.AddWithValue("@AgentMrkPer", AgtMrkPercent);
//                    sqlcmd.Parameters.AddWithValue("@AgentMrkType", AgentMrkType);
//                    sqlcmd.Parameters.AddWithValue("@CheckIN", CheckIN);
//                    sqlcmd.Parameters.AddWithValue("@CheckOut", CheckOut);
//                    sqlcmd.Parameters.AddWithValue("@RoomCount", RoomCount);
//                    sqlcmd.Parameters.AddWithValue("@NightCount", NightCount);
//                    sqlcmd.Parameters.AddWithValue("@AdultCount", AdtCount);
//                    sqlcmd.Parameters.AddWithValue("@ChildCount", ChildCount);
//                    sqlcmd.Parameters.AddWithValue("@StarRating", StarRating);
//                    sqlcmd.Parameters.AddWithValue("@provider", provider);                  
//                    sqlcmd.Parameters.AddWithValue("@IP", IP);
//                    sqlcmd.Parameters.AddWithValue("@Currency", Currency);
//                    sqlcmd.Parameters.AddWithValue("@LoginID", AgentID);
//                    sqlcmd.Parameters.AddWithValue("@AgencyName", AgencyName);
//                    sqlcmd.Parameters.AddWithValue("@ServiceTaxPer", ServiceTax);
//                    sqlcmd.Parameters.AddWithValue("@ServiceTaxAmount", ServiceTaxAmt);
//                    sqlcmd.Parameters.AddWithValue("@VServiceTaxAmount", VServiceTaxAmt);                  
//                    sqlcmd.Parameters.AddWithValue("@CommisionPer", CommisionPer);
//                    sqlcmd.Parameters.AddWithValue("@CommisionAmt", CommisionAmt);
//                    sqlcmd.Parameters.AddWithValue("@CommisionType", CommisionType);
//                    i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));   
//                }
//                catch (Exception ex)
//                {
//                    InsertHotelErrorLog(ex, "InsHtlHrdDetails");
//                }
//                finally { sqlcmd.Dispose(); }
//                return i;
//            }

//         //Insert Hotel Details
//        public int InsHotelDetails(string OrderID, string HtlCode, string HtlName, string RoomName, string RoomTypeCode, string RoomRatePlane, string RoomRPH, string HotelLocation, string Address, string CountryCode, string CityCode,
//         string CityName, string CountryName, string ContactNo, string HtlImage, string Inclusion, string EssentialInfo, string SharingBedding, string PerRoomPreNightRate_org, string PerRoomPreNightRate_mrk,
//            string PerRoomWise_Gues, string CancellationPoli, string SearchType, string Refundable, string Smoking)
//         {
//            int i = 0;
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_InsertHotelDetails");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                sqlcmd.Parameters.AddWithValue("@HotelCode", HtlCode);
//                sqlcmd.Parameters.AddWithValue("@HotelName", HtlName);
//                sqlcmd.Parameters.AddWithValue("@RoomTypeCode", RoomTypeCode);
//                sqlcmd.Parameters.AddWithValue("@RoomRatePlane", RoomRatePlane);
//                sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
//                sqlcmd.Parameters.AddWithValue("@RoomRPH", RoomRPH);
//                sqlcmd.Parameters.AddWithValue("@Address", Address);
//                sqlcmd.Parameters.AddWithValue("@CityName", CityName);
//                sqlcmd.Parameters.AddWithValue("@CityCode", CityCode);
//                sqlcmd.Parameters.AddWithValue("@CountryCode", CountryCode);
//                sqlcmd.Parameters.AddWithValue("@CountryName", CountryName);
//                sqlcmd.Parameters.AddWithValue("@HotelLocation", HotelLocation);
//                sqlcmd.Parameters.AddWithValue("@ContactNo", ContactNo);               
//                sqlcmd.Parameters.AddWithValue("@HotelImage", HtlImage);
//                sqlcmd.Parameters.AddWithValue("@PerRoomPreNightRate_org", PerRoomPreNightRate_org);
//                sqlcmd.Parameters.AddWithValue("@PerRoomPreNightRate_mrk", PerRoomPreNightRate_mrk);
//                sqlcmd.Parameters.AddWithValue("@PerRoomWise_Guest", PerRoomWise_Gues);
//                sqlcmd.Parameters.AddWithValue("@Inclusion", Inclusion);
//                sqlcmd.Parameters.AddWithValue("@EssentialInfo", EssentialInfo);
//                sqlcmd.Parameters.AddWithValue("@SharingBedding", SharingBedding);
//                sqlcmd.Parameters.AddWithValue("@CancellationPoli", CancellationPoli);
//                sqlcmd.Parameters.AddWithValue("@SearchType", SearchType);
//                sqlcmd.Parameters.AddWithValue("@Refundable", Refundable);
//                 sqlcmd.Parameters.AddWithValue("@Smoking", Smoking);
//                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "InsHotelDetails");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//         }

//         //Insert Room Details
//        public int InsRoomDetails(string OrderID, string RoomTypeCode, string RoomPlanCode, string RoomName,  string RoomRPH, int AdultCount, int ChildCount, int RoomNo, string ChildAge)
//         {
//             int i = 0;
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_InsertRoomDetail");
//                sqlcmd.CommandType = CommandType.StoredProcedure; 
//             sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//             sqlcmd.Parameters.AddWithValue("@RoomTypeCode", RoomTypeCode);
//             sqlcmd.Parameters.AddWithValue("@RoomPlanCode", RoomPlanCode);
//             sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
//             sqlcmd.Parameters.AddWithValue("@RoomRPH", RoomRPH);
//             sqlcmd.Parameters.AddWithValue("@AdultCount", AdultCount);
//             sqlcmd.Parameters.AddWithValue("@ChildCount", ChildCount);
//             sqlcmd.Parameters.AddWithValue("@ChildAge", ChildAge);
//             sqlcmd.Parameters.AddWithValue("@RoomNo", RoomNo);
//             i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "InsRoomDetails");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//         }

//         //Insert traveler Guest Details
//         public int insGuestDetails(string OrderID, string GType, string GTitle, string GFName, string GLName, string GPhoneNo, string GEmail, string Country, string State, string City, string Address, string PostalCode, int RoomNo, int ChildAge, string LoginID, string DOB)
//         {
//            int i = 0;
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_InsertGuestDetails");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                sqlcmd.Parameters.AddWithValue("@GType", GType);
//                sqlcmd.Parameters.AddWithValue("@GTitle", GTitle);
//                sqlcmd.Parameters.AddWithValue("@GFName", GFName);
//                sqlcmd.Parameters.AddWithValue("@GLName", GLName);
//                sqlcmd.Parameters.AddWithValue("@GPhoneNo", GPhoneNo);
//                sqlcmd.Parameters.AddWithValue("@GEmail", GEmail);
//                sqlcmd.Parameters.AddWithValue("@Country", Country);
//                sqlcmd.Parameters.AddWithValue("@State", State);
//                sqlcmd.Parameters.AddWithValue("@City", City);
//                sqlcmd.Parameters.AddWithValue("@Address", Address);
//                sqlcmd.Parameters.AddWithValue("@PinCode", PostalCode);
//                sqlcmd.Parameters.AddWithValue("@RoomNo", RoomNo);
//                sqlcmd.Parameters.AddWithValue("@ChildAge", ChildAge);
//                sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
//                sqlcmd.Parameters.AddWithValue("@DOB", DOB);
//                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "insGuestDetails");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//         }
//         //Check Ticketing Status and Refreshing page
//         public int TicketingStatus_Refresh(string Orderid, string User_Id)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_TicketingStatus_Refresh");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", Orderid);
//                 sqlcmd.Parameters.AddWithValue("@User_Id", User_Id);
//                 i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "TicketingStatus_Refresh");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Insert Additional Guest Details
//         public int InsAdditionalGuesttbl(string OrderID, int RoomNO, string FName, string LName, string PaxType, int ChildAge)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("InsAdditionalGuesttbl");
//                 sqlcmd.CommandType = CommandType.StoredProcedure; 
//             sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//             sqlcmd.Parameters.AddWithValue("@RoomNO", RoomNO);
//             sqlcmd.Parameters.AddWithValue("@FName", FName);
//             sqlcmd.Parameters.AddWithValue("@LName", LName);
//             sqlcmd.Parameters.AddWithValue("@PaxType", PaxType);
//             sqlcmd.Parameters.AddWithValue("@ChildAge", ChildAge);
//              i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "InsAdditionalGuesttbl");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//         }
//        // update Header of guest details
//         public int UpdateHtlHeader(string OrderID, string PaymentMode, string status)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_UpdateHtlHeader_PreBook");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                 sqlcmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);
//                 sqlcmd.Parameters.AddWithValue("@status", status);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "UpdateHtlHeader");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Get Addition guest Details
//         public DataSet GetAddiGuest(string OrderID)
//         {
//             DataSet GuestDS = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("GetAddiGuest");
//                 sqlcmd.CommandType = CommandType.StoredProcedure; 
//                 sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                 GuestDS = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetAddiGuest");
//             }
//             finally { sqlcmd.Dispose(); }
//             return GuestDS; 
//         }
//         //Update Data after Hotel Booked
//         public int UpdateHtlBooking(string OrderID, string BookingID, string ConfirmID, string Status, string HtlPhoneNo, string HtlFax, string HotelEmailId)
//         {
//             int i = 0;
//             try
//             {//@OrderID nvarchar(20),@Status nvarchar(20), @BookingID nvarchar(50), @ConfirmID nvarchar(50)='', @ContactNo nvarchar(50)='',  @HotelFax Nvarchar(50)='', @HotelEmailId Nvarchar(50)=''
//                 sqlcmd = new SqlCommand("SP_HTL_UpdateBooking");
//                 sqlcmd.CommandType = CommandType.StoredProcedure; 
//                sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                sqlcmd.Parameters.AddWithValue("@Status", Status);
//                sqlcmd.Parameters.AddWithValue("@BookingID", BookingID);
//                sqlcmd.Parameters.AddWithValue("@ConfirmID", ConfirmID);
//                sqlcmd.Parameters.AddWithValue("@HotelPhoneNo", HtlPhoneNo);
//                sqlcmd.Parameters.AddWithValue("@HotelFax", HtlFax);
//                sqlcmd.Parameters.AddWithValue("@HotelEmailId", HotelEmailId);
//             i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "UpdateHtlBooking");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //After Booked Hotel Subtract Booked Amt from Agent balance 
//         public double UpdateBalance(string UserId, double Amount) 
//         {
//             double i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SubtractFromLimit");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@UserId", UserId);
//                 sqlcmd.Parameters.AddWithValue("@Amount", Amount);
//                 i = Convert.ToDouble(DBhelperLnb.ExecuteScalar(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "UpdateBalance");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Get All Aegent List for Bind DropDownList in Report Page
//         public DataSet GetAgencyDtl()
//         {
//             DataSet AgencyDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("GetAgency");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 AgencyDs = DBhelperLnb.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetAgencyDtl");
//             }
//             finally { sqlcmd.Dispose(); }
//             return AgencyDs;
//         }
//          //Get One Agency Details
//         public DataSet GetAgencyDetails(string UserId)
//         {
//             DataSet AgencyDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("AgencyDetails");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@UserId", UserId);
//                 AgencyDs = DBhelperLnb.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetAgencyDetails");
//             }
//             finally { sqlcmd.Dispose(); }
//             return AgencyDs;
//         }
//         //Check Agent balance
//         public int CheckBalance(string UserId, string Amt)
//         {
//            int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("CheckBalance");
//                 sqlcmd.CommandType = CommandType.StoredProcedure; 
//             sqlcmd.Parameters.AddWithValue("@UserId", UserId);
//             sqlcmd.Parameters.AddWithValue("@Amt", Amt);
//                 i = Convert.ToInt32(DBhelperLnb.ExecuteScalar(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "CheckBalance");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Insert Data in Ledger Details
//         public int insertLedgerDetails(string AgentID, string AgencyName, string InvoiceNo, string PnrNo, string TicketNo, string TicketingCarrier, string YatraAccountID, string AccountID, string ExecutiveID, string IPAddress,
//         double Debit, double Credit, double Aval_Balance, string BookingType, string Remark, int PaxId)
//         {
//            int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("usp_insert_LedgerDetails");
//                 sqlcmd.CommandType = CommandType.StoredProcedure; 
//                 sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@AgencyName", AgencyName);
//                 sqlcmd.Parameters.AddWithValue("@InvoiceNo", InvoiceNo);
//                 sqlcmd.Parameters.AddWithValue("@PnrNo", PnrNo);
//                 sqlcmd.Parameters.AddWithValue("@TicketNo", TicketNo);
//                 sqlcmd.Parameters.AddWithValue("@TicketingCarrier", TicketingCarrier);
//                 sqlcmd.Parameters.AddWithValue("@YatraAccountID", YatraAccountID);

//                 sqlcmd.Parameters.AddWithValue("@AccountID", AccountID);
//                 sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
//                 sqlcmd.Parameters.AddWithValue("@IPAddress", IPAddress);

//                 sqlcmd.Parameters.AddWithValue("@Debit", Debit);
//                 sqlcmd.Parameters.AddWithValue("@Credit", Credit);
//                 sqlcmd.Parameters.AddWithValue("@Aval_Balance", Aval_Balance);
//                 sqlcmd.Parameters.AddWithValue("@BookingType", BookingType);
//                 sqlcmd.Parameters.AddWithValue("@Remark", Remark);
//                 sqlcmd.Parameters.AddWithValue("@PaxId", PaxId);
//                 i = Convert.ToInt32(DBhelperLnb.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "insertLedgerDetails");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
       
//        //Hotel Ticket Copy
//         public DataSet htlintsummary(string orderId, string ReqType)
//         {
//             DataSet htlsummryDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_GetHotelDetailData");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@orderId", orderId);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 htlsummryDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "htlintsummary");
//             }
//             finally { sqlcmd.Dispose(); }
//             return htlsummryDs;
//         }
//         //get mailing details fro Look and book
//         public DataSet GetMailingDetails(string department, string UserID)
//         {
//             DataSet mailDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_GETMAILINGCREDENTIAL_ITZ");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Department", department);
//                 sqlcmd.Parameters.AddWithValue("@UserID", UserID);
//                 mailDs = DBhelperLnb.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetMailingDetails");
//             }
//             finally { sqlcmd.Dispose(); }
//             return mailDs;
//         }
//         //Check Hotel Refund Status
//         public int CheckHtlRefuStaus(string BookingID, string OrderId)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_CheckHtlStaus");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderId", OrderId);
//                 sqlcmd.Parameters.AddWithValue("@BookingID", BookingID);
//                 i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "CheckHtlRefuStaus");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Hotel Booking Report
//         public DataSet HotelSearchRpt(string FromDate, string ToDate, string BookingID, string OrderID, string HtlName, string RoomName, string Trip, string AgentID, string CheckIn, string Status,
//         string LoginID, string usertype)
//         {
//             DataSet HotelrptDs = new DataSet();
//             try
//             {
//                 if (Status == "Confirm" || Status == "Hold")
//                    sqlcmd = new SqlCommand("SP_HTL_BookingReport");
//                 else
//                     sqlcmd = new SqlCommand("SP_HTL_CancelReport");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@FromDate", FromDate);
//                 sqlcmd.Parameters.AddWithValue("@ToDate ", ToDate);
//                 sqlcmd.Parameters.AddWithValue("@BookingID ", BookingID);
//                 sqlcmd.Parameters.AddWithValue("@CheckIn ", CheckIn);
//                 sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                 sqlcmd.Parameters.AddWithValue("@HotelName", HtlName);
//                 sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
//                 sqlcmd.Parameters.AddWithValue("@Trip", Trip);
//                 sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@usertype", usertype);
//                 sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
//                 HotelrptDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "HotelSearchRpt");
//             }
//             finally { sqlcmd.Dispose(); }
//             return HotelrptDs;
//         }
//         public DataSet HotelRefundRpt(string FromDate, string ToDate, string BID, string OrderID, string HtlName, string Trip, string AgentID, string Status, string ExecutiveID, string LoginID,
//         string usertype)
//         {
//             DataSet HotelrptRfndDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("HtlRefundRpt");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@FromDate", FromDate);
//                 sqlcmd.Parameters.AddWithValue("@ToDate ", ToDate);
//                 sqlcmd.Parameters.AddWithValue("@BID ", BID);
//                 sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                 sqlcmd.Parameters.AddWithValue("@HtlName", HtlName);
//                 sqlcmd.Parameters.AddWithValue("@Trip", Trip);
//                 sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@ExecutiveID ", ExecutiveID);
//                 sqlcmd.Parameters.AddWithValue("@usertype", usertype);
//                 sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
//                 HotelrptRfndDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "HotelRefundRpt");
//             }
//             finally { sqlcmd.Dispose(); }
//             return HotelrptRfndDs;
//         }
//         //Hold Hotel Booking Report
//         public DataSet HoldHotelSearchRpt(string FromDate, string ToDate, string BID, string OrderID, string HtlName, string RoomName, string Star, string AgentID, string PgFName, string Status,
//         string LoginID, string usertype)
//         {
//             DataSet HotelrptHoldDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("GetHotelRpt");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@FromDate", FromDate);
//                 sqlcmd.Parameters.AddWithValue("@ToDate ", ToDate);
//                 sqlcmd.Parameters.AddWithValue("@BID ", BID);
//                 sqlcmd.Parameters.AddWithValue("@PgFName ", PgFName);
//                 sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                 sqlcmd.Parameters.AddWithValue("@HtlName", HtlName);
//                 sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
//                 sqlcmd.Parameters.AddWithValue("@Star", Star);
//                 sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@usertype", usertype);
//                 sqlcmd.Parameters.AddWithValue("@LoginID", LoginID);
//                 HotelrptHoldDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "HoldHotelSearchRpt");
//             }
//             finally { sqlcmd.Dispose(); }
//             return HotelrptHoldDs;
//         }
//         //Insert Hotel Markup
//         public int InsHtlMarkup(string LoginId, string AgentId, string Country, string City, string Htltype, string MarkupType, string Star, string markupAmt, string ReqType, int Counter)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_InsGetHtlMarkup");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
//                 sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
//                 sqlcmd.Parameters.AddWithValue("@Country", Country);
//                 sqlcmd.Parameters.AddWithValue("@City", City);
//                 sqlcmd.Parameters.AddWithValue("@Star", Star);
//                 sqlcmd.Parameters.AddWithValue("@TripType", Htltype);
//                 sqlcmd.Parameters.AddWithValue("@MarkupType", MarkupType);
//                 sqlcmd.Parameters.AddWithValue("@markupAmount", markupAmt);
//                 sqlcmd.Parameters.AddWithValue("@MarkupID", Counter);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 if (Counter == 0)
//                     i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
//                 else
//                     i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "InsHtlMarkup");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;

//         }
//         //Check Ladger report Status and uplode Amount in Agent Account
//         public double CheckLadgerStatus(string InvoceID, string AgentID, double Amt, string Status)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("CheckLedgerBookingType");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@orderid", InvoceID);
//                 sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@Amt", Amt);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 i = Convert.ToInt32(DBhelperLnb.ExecuteScalar(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "CheckLadgerStatus");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Get markup City List
//         public DataSet GetMrkCityList(string city, string country)
//         {
//            DataSet MrkCityDs = new DataSet();
//              try
//             {
//                 sqlcmd = new SqlCommand("SP_HotelMarkup_CitySearch");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//             sqlcmd.Parameters.AddWithValue("@Country", country);
//             sqlcmd.Parameters.AddWithValue("@City", city);
//             MrkCityDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//              catch (Exception ex)
//              {
//                  InsertHotelErrorLog(ex, "GetMrkCityList");
//              }
//              finally { sqlcmd.Dispose(); }
//              return MrkCityDs;
//         }
//         //Get markup Country List
//         public DataSet GetCountry(string HtlType, string Country)
//         {
//             DataSet mrkCountryDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("GetCountry");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@HtlType", HtlType);
//                 sqlcmd.Parameters.AddWithValue("@Country", Country);
//                 mrkCountryDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetCountry");
//             }
//             finally { sqlcmd.Dispose(); }
//             return mrkCountryDs;
//         }

//         //Insert and Update Hotel Booking XML Logs
//         public int SP_Htl_InsUpdBookingLog(string HTLOrderID, string HTLRequest, string HTLResponse, string AgentID, string ReqType)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_Htl_InsUpdBookingLog");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@HTLOrderID", HTLOrderID);
//                 sqlcmd.Parameters.AddWithValue("@HTLRequest", HTLRequest);
//                 sqlcmd.Parameters.AddWithValue("@HTLResponse", HTLResponse);
//                 sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "SP_Htl_InsUpdBookingLog");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Select Hotel Error Log and XML LOGS
//         public DataSet SelectHotelLog(string top, string orderid, string AgentID, string Provider, string ReqType)
//         {
//             DataSet latilongids = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_SelectHotelLog");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@top", top);
//                 sqlcmd.Parameters.AddWithValue("@orderid", orderid);
//                 sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@Provider", Provider);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 latilongids = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "SelectHotelLog");
//             }
//             finally { sqlcmd.Dispose(); }
//             return latilongids;
//         }
//         public int UpdatePolicy(string orderid, string Policy)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("UpdateBookedHotelPolicy");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
//                 sqlcmd.Parameters.AddWithValue("@Policy", Policy);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         public DataSet SelectHltRefund(string orderid)
//         {
//             DataSet RfndDS = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("GetCancellationDetail");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
//                 RfndDS = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "SelectHltRefund");
//             }
//             finally { sqlcmd.Dispose(); }
//             return RfndDS;
//         }
//         //Insert Data in Hoel Cancellation table
//         public int InsHtlRefund(string orderid, string RequestBy, string Status, decimal CancelCharge, int ServiceCharge, int CancelNight, string FromCancelDate, string remark, string PgTitle, string PgFname, string PgLname, string PgEmail)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_InsRefundRequest");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderId", orderid);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@CancelNight", CancelNight);
//                 sqlcmd.Parameters.AddWithValue("@FromCancelDate", FromCancelDate);
//                 sqlcmd.Parameters.AddWithValue("@CancelCharge", CancelCharge);
//                 sqlcmd.Parameters.AddWithValue("@ServiceCharge", ServiceCharge);
//                 sqlcmd.Parameters.AddWithValue("@PgTitle", PgTitle);
//                 sqlcmd.Parameters.AddWithValue("@PgFirstName", PgFname);
//                 sqlcmd.Parameters.AddWithValue("@PgLastName", PgLname);
//                 sqlcmd.Parameters.AddWithValue("@PgEmail", PgEmail);
//                 sqlcmd.Parameters.AddWithValue("@RequestBy", RequestBy);
//                 sqlcmd.Parameters.AddWithValue("@Remark", remark);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "InsHtlRefund");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Update Hotel Refund
//         public int UpdateHltRefund(string Status, string orderid, decimal RefundAmt, decimal CancelCharge, int ServiceCharge, string Remarks)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_UpdateRefund");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
//                 sqlcmd.Parameters.AddWithValue("@RefundAmt", RefundAmt);
//                 sqlcmd.Parameters.AddWithValue("@CancelCharge", CancelCharge);
//                 sqlcmd.Parameters.AddWithValue("@ServiceCharge", ServiceCharge);
//                 sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "UpdateHltRefund");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Refund Details
//         public DataSet HtlRefundDetail(string Status, string orderid, string ExecutiveID, string ReqType, string Remarks)
//         {
//             DataSet RfndDS = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_Htl_RefundDetail");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
//                 sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
//                 RfndDS = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "HtlRefundDetail");
//             }
//             finally { sqlcmd.Dispose(); }
//             return RfndDS;
//         }
//         //Update Hotel Refund 
//         public int HtlRefundUpdates(string Status, string orderid, string ExecutiveID, string ReqType, string Remarks)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_Htl_RefundDetail");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
//                 sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "HtlRefundUpdates");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Update Hotel Auto cancellation
//         public int SP_HTL_AutoRefund(HotelCancellation HotelDetails, decimal cancelamt)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_AutoRefund");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Status", HotelDetails.CancelStatus);
//                 sqlcmd.Parameters.AddWithValue("@OrderID", HotelDetails.Orderid);
//                 sqlcmd.Parameters.AddWithValue("@RefundAmt", HotelDetails.RefundAmt);
//                 sqlcmd.Parameters.AddWithValue("@CancelCharge", cancelamt);

//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "UpdateHltRefund");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Refund Details
//         //Update Hold Hotel Booking
//         public int UpdateHrdHold(string OrderID, string BookingID, string ConfirmID, string Status, string ExecutiveID, string Remark)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_UpdateHrdHold");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                 sqlcmd.Parameters.AddWithValue("@BookingID", BookingID);
//                 sqlcmd.Parameters.AddWithValue("@ConfirmID", ConfirmID);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
//                 sqlcmd.Parameters.AddWithValue("@Remark", Remark);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "UpdateHrdHold");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Reject Hotel Booking
//         public int RejectHoldBooking(string Status, string orderid, decimal RefundAmt, string ExecutiveID, string Remarks)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_RejectHoldBooking");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@OrderID", orderid);
//                 sqlcmd.Parameters.AddWithValue("@RefundAmt", RefundAmt);
//                 sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
//                 sqlcmd.Parameters.AddWithValue("@Remarks", Remarks);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "RejectHoldBooking");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         // Checking Hotel is present in table or not for Hol Booking
//         public DataSet CheckHotelForHold(string HtlCode, string RoomTypeID)
//         {
//             DataSet HoldhtlDS = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("CheckHotelForHold");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@HtlCode", HtlCode);
//                 sqlcmd.Parameters.AddWithValue("@RoomTypeID", RoomTypeID);
//                 HoldhtlDS = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "CheckHotelForHold");
//             }
//             finally { sqlcmd.Dispose(); }
//             return HoldhtlDS;
//         }
//         //Update Hold booking 
//         public int UpdateHoldHotelBooking(string OrderID, string Status)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("UpdateHoldHotelBooking");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@OrderID", OrderID);
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "UpdateHoldHotelBooking");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Get Hold booking 
//         public DataSet GetHoldHotel(string ExecutiveID, string Status, string Trip)
//         {
//             DataSet HoldhtlDS = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_GetHoldHotel");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Status", Status);
//                 sqlcmd.Parameters.AddWithValue("@ExecutiveID", ExecutiveID);
//                 sqlcmd.Parameters.AddWithValue("@Trip", Trip);
//                 HoldhtlDS = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetHoldHotel");
//             }
//             finally { sqlcmd.Dispose(); }
//             return HoldhtlDS;
//         }
//         //insert Hotel Error LOG 
//         public static void InsertHotelErrorLog(Exception ex, string LoginID)
//         {
//             SqlCommand cmd = new SqlCommand("SP_HTL_InsertErrorLog");//ex.StackTrace
//             SqlDatabase DBhelperErr = new SqlDatabase(ConfigurationManager.ConnectionStrings["HTLConnStr"].ConnectionString);
//             try
//             {
//                 System.Diagnostics.StackTrace trace = new System.Diagnostics.StackTrace(ex, true);
//                 string linenumber = "0", fileNames = "";
//                 if (trace != null)
//                 {
//                     if (trace.FrameCount > 0 && trace.GetFrame(trace.FrameCount - 1).GetFileName() != null)
//                     {
//                         linenumber = (trace.GetFrame((trace.FrameCount - 1)).GetFileLineNumber()).ToString();
//                         fileNames = (trace.GetFrame((trace.FrameCount - 1)).GetFileName()).ToString();
//                     }
//                     else
//                         fileNames = ex.StackTrace;
//                 }
//                 cmd.CommandType = CommandType.StoredProcedure;
//                 cmd.Parameters.AddWithValue("@PageName", fileNames + " - " + LoginID);
//                 cmd.Parameters.AddWithValue("@ErrorMessage", ex.Message);
//                 cmd.Parameters.AddWithValue("@LineNumber", linenumber);
//                 DBhelperErr.ExecuteNonQuery(cmd);
//             }
//             catch (Exception ex1)
//             { }
//             finally
//             { cmd.Dispose(); }
//         }
//         //Get Hotel Commision
//         public HotelBooking GetHtlCommision(HotelBooking ObjBooking, string TypeID, decimal Amount)
//         {
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_SELECT_Commisiom");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Country", ObjBooking.Country);
//                 sqlcmd.Parameters.AddWithValue("@City", ObjBooking.CityName);
//                 sqlcmd.Parameters.AddWithValue("@Star", ObjBooking.StarRating);
//                 sqlcmd.Parameters.AddWithValue("@AgentId", ObjBooking.AgentID);
//                 sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
//                 sqlcmd.Parameters.AddWithValue("@Amount", Amount);
//                 sqlcmd.Parameters.AddWithValue("@Provider", ObjBooking.Provider);
//                 var dbr = DBhelper.ExecuteReader(sqlcmd);
//                 while (dbr.Read())
//                 {
//                     ObjBooking.CommisionType = dbr["CommisionType"].ToString();
//                     ObjBooking.CommisionPer = Convert.ToDecimal(dbr["CommisionPer"]);
//                     ObjBooking.CommisionAmt = Convert.ToDecimal(dbr["CommisionAmt"]);
//                     ObjBooking.AgentDebitAmt = Convert.ToDecimal(dbr["TotalAmount"]);
//                 }
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetHtlCommision");
//             }
//             finally { sqlcmd.Dispose(); }
//             return ObjBooking;
//         }
//         //Get Hotel Commision Detail page breakups
//         public DataSet GetDetailsPageCommision(string Country, string CityName, string StarRating, string AgentID, string TypeID, decimal Amount)
//         {
//             DataSet commisionDS = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_SELECT_Commisiom");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Country", Country);
//                 sqlcmd.Parameters.AddWithValue("@City", CityName);
//                 sqlcmd.Parameters.AddWithValue("@Star", StarRating);
//                 sqlcmd.Parameters.AddWithValue("@AgentId", AgentID);
//                 sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
//                 sqlcmd.Parameters.AddWithValue("@Amount", Amount);
//                 sqlcmd.Parameters.AddWithValue("@Provider", "");
//                 commisionDS = DBhelper.ExecuteDataSet(sqlcmd);                
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetDetailsPageCommision");
//             }
//             finally { sqlcmd.Dispose(); }
//             return commisionDS;
//         }
//         //Insert Hotel Commision
//         public int InsHtlCommision(string LoginId, string AgentId, string Country, string City, string TypeID, string Star, string CommisionType, string CommisionPer, string ReqType, int Counter)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_InsGetCommision");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Country", Country);
//                 sqlcmd.Parameters.AddWithValue("@City", City);
//                 sqlcmd.Parameters.AddWithValue("@Star", Star);
//                 sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
//                 sqlcmd.Parameters.AddWithValue("@CommisionType", CommisionType);
//                 sqlcmd.Parameters.AddWithValue("@CommisionPer", CommisionPer);
//                 sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
//                 sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
//                 sqlcmd.Parameters.AddWithValue("@Counter", Counter);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 if (Counter == 0)
//                     i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
//                 else
//                     i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "InsHtlCommision");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;

//         }

//         //Get Hotel Cach Back panal
//         public DataSet GetHtlCachbackPanal(string LoginId, string AgentId, string Country, string City, string TypeID, string Star, string CommisionType, string CommisionPer, string ReqType, int Counter)
//         {
//             DataSet MrkDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_HTL_InsGetCommision");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@Country", Country);
//                 sqlcmd.Parameters.AddWithValue("@City", City);
//                 sqlcmd.Parameters.AddWithValue("@Star", Star);
//                 sqlcmd.Parameters.AddWithValue("@TypeID", TypeID);
//                 sqlcmd.Parameters.AddWithValue("@CommisionType", CommisionType);
//                 sqlcmd.Parameters.AddWithValue("@CommisionPer", CommisionPer);
//                 sqlcmd.Parameters.AddWithValue("@LoginId", LoginId);
//                 sqlcmd.Parameters.AddWithValue("@AgentId", AgentId);
//                 sqlcmd.Parameters.AddWithValue("@Counter", Counter);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 MrkDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetHtlCachbackPanal");
//             }
//             finally { sqlcmd.Dispose(); }
//             return MrkDs;
//         }

//         //'Hotel Markup City country Search
//         public List<CitySearch> GetMarkupCityCountryList(string city, string Country, List<CitySearch> CityList)
//         {
//             try
//             {
//                 if (city == "")
//                 {
//                     sqlcmd = new SqlCommand("SP_HTL_MarkupCountrySearch");
//                     sqlcmd.Parameters.AddWithValue("@Country", Country);
//                     sqlcmd.CommandType = CommandType.StoredProcedure;
//                     var dbr = DBhelper.ExecuteReader(sqlcmd);
//                     while (dbr.Read())
//                     {
//                         CityList.Add(new CitySearch { Country = dbr["Country"].ToString().Trim(), CountryCode = dbr["CountryCode"].ToString().Trim() });
//                     }
//                 }
//                 else
//                 {
//                     sqlcmd = new SqlCommand("SP_HTL_MarkupCitySearch");
//                     sqlcmd.Parameters.AddWithValue("@City", city);
//                     sqlcmd.Parameters.AddWithValue("@Country", Country);
//                     sqlcmd.CommandType = CommandType.StoredProcedure;

//                     var dbr = DBhelper.ExecuteReader(sqlcmd);
//                     while (dbr.Read())
//                     {
//                         CityList.Add(new CitySearch { CityName = dbr["CityName"].ToString().Trim(), CityCode = dbr["CityCode"].ToString().Trim() });
//                     }
//                 }
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetMarkupCityCountryList");
//             }
//             finally { sqlcmd.Dispose(); }

//             return CityList;
//         }












//        //Get Credit Card Details
//         public DataSet CreditCardDetails()
//         {
//             DataSet HotelCardDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("CardDetail");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 HotelCardDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//              catch (Exception ex)
//              {
//                  InsertHotelErrorLog(ex, "");
//              }
//              finally { sqlcmd.Dispose(); }
//             return HotelCardDs;
//         }
//         //Insert Latitude and Longitude
//         public int InsLatLog(string SearchCity, object Hotelname, decimal lat, decimal lng, string ReqType)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_Htl_InsLatLog");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//             sqlcmd.Parameters.AddWithValue("@SearchCity", SearchCity);
//             sqlcmd.Parameters.AddWithValue("@Hotelname", Hotelname.ToString());
//             sqlcmd.Parameters.AddWithValue("@lat", lat);
//             sqlcmd.Parameters.AddWithValue("@lng", lng);
//             sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                  i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Select Latitude and Longitude
//         public DataSet SelectLatLog(string SearchCity, string ReqType)
//         {
//             DataSet latLongDs = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("SP_Htl_InsLatLog");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@SearchCity", SearchCity);
//                 sqlcmd.Parameters.AddWithValue("@Hotelname", "");
//                 sqlcmd.Parameters.AddWithValue("@lat", 0.0);
//                 sqlcmd.Parameters.AddWithValue("@lng", 0.0);
//                 sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                 latLongDs = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "");
//             }
//             finally { sqlcmd.Dispose(); }
//             return latLongDs;
//         }
//         //Get Hotel Discount
//         public int GetHtlDiscount(string Trip, string AgentID, string star, string City)
//         {
//              int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("HTLDiscount");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//             sqlcmd.Parameters.AddWithValue("@Trip", Trip);
//             sqlcmd.Parameters.AddWithValue("@AgentID", AgentID);
//             sqlcmd.Parameters.AddWithValue("@star", star);
//             sqlcmd.Parameters.AddWithValue("@City", City);
//             i = Convert.ToInt32(DBhelper.ExecuteScalar(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetHtlDiscount");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         public DataSet GETHtlDiscount()
//         {
//             DataSet disds = new DataSet();
//             try
//             {
//                 sqlcmd = new SqlCommand("GETHtlDiscount");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 disds = DBhelper.ExecuteDataSet(sqlcmd);
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GETHtlDiscount");
//             }
//             finally { sqlcmd.Dispose(); }
//             return disds;
//         }
//         public int InsDiscount(string Trip, string City, string Agentid, string Star, string Percentage, int DisID, string ReqType)
//         {
//            int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("InsDiscount");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//             sqlcmd.Parameters.AddWithValue("@Trip", Trip);
//             sqlcmd.Parameters.AddWithValue("@City", City);
//             sqlcmd.Parameters.AddWithValue("@Agentid", Agentid);
//             sqlcmd.Parameters.AddWithValue("@Percentage", Percentage);
//             sqlcmd.Parameters.AddWithValue("@Star", Star);
//             sqlcmd.Parameters.AddWithValue("@DisID", DisID);
//             sqlcmd.Parameters.AddWithValue("@ReqType", ReqType);
//                    i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }

//         //insert City for GTA
//         public int insertGTACity(string City, string CityCode, string Country, string CountryCode)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("insertGTACity");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//                 sqlcmd.Parameters.AddWithValue("@CityName", City);
//                 sqlcmd.Parameters.AddWithValue("@CityCode", CityCode);
//                 sqlcmd.Parameters.AddWithValue("@Country", Country);
//                 sqlcmd.Parameters.AddWithValue("@CountryCode", CountryCode);
//                 i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "GetHotelMarkup");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//         //Insert Area Detals in table
//         public int InsertGTAArea(string CityName, string CityCode, string Country)
//         {
//             int i = 0;
//             try
//             {
//                 sqlcmd = new SqlCommand("InsertGTAArea");
//                 sqlcmd.CommandType = CommandType.StoredProcedure;
//             sqlcmd.Parameters.AddWithValue("@CityName", CityName);
//             sqlcmd.Parameters.AddWithValue("@CityCode", CityCode);
//             sqlcmd.Parameters.AddWithValue("@Country", Country);
//                  i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//             }
//             catch (Exception ex)
//             {
//                 InsertHotelErrorLog(ex, "InsertGTAArea");
//             }
//             finally { sqlcmd.Dispose(); }
//             return i;
//         }
//        public List<CitySearch> GetHotelCity(string cityCode, int maxlength)
//        {
//            SqlCommand sqlcmd = new SqlCommand();
//            List<CitySearch> CityList = new List<CitySearch>();
//            try
//            {
//                sqlcmd.CommandText = "SP_HLT_CitySearch";
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@CITY_NAME", cityCode);
//                sqlcmd.Parameters.AddWithValue("@HLT_NAME", "");
//                sqlcmd.Parameters.AddWithValue("@ReqType", "CITY");
//                var dbr = DBhelper.ExecuteReader(sqlcmd);
//                 while (dbr.Read())
//                {
//                    CityList.Add(new CitySearch
//                    {
//                        CityName = dbr["CityName"].ToString().Trim(),
//                        CityCode=dbr["CityCode"].ToString().Trim(),
//                        Country = dbr["Country"].ToString().Trim(),
//                        CountryCode = dbr["CountryCode"].ToString().Trim(),
//                        SearchType = dbr["SearchType"].ToString().Trim()
//                    });
//                }
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetHotelCity");
//            }
//            finally { sqlcmd.Dispose(); }

//            return CityList;
//        }
//        public List<CitySearch> GetHotelnameSearch(string cityCode, string HLT_NAME)
//        {
//            sqlcmd = new SqlCommand("SP_HLT_CitySearch");
//            List<CitySearch> fetchCity = new List<CitySearch>();
//            try
//            {
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@CITY_NAME", cityCode);
//                sqlcmd.Parameters.AddWithValue("@HLT_NAME", HLT_NAME);
//                sqlcmd.Parameters.AddWithValue("@ReqType", "HOTEL");
//                var dbr = DBhelper.ExecuteReader(sqlcmd);
//                while (dbr.Read())
//                {
//                    fetchCity.Add(new CitySearch
//                    {
//                        CityName = dbr["City"].ToString().Trim(),
//                        Country = dbr["VendorName"].ToString().Trim(),
//                        CountryCode = dbr["VendorID"].ToString().Trim()
//                    });
//                }
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetHotelnameSearch");
//            }
//            finally { sqlcmd.Dispose(); }

//            return fetchCity.ToList();
//        }
//        //get Hotel Markup
//        public DataTable GetHotelMarkup(string City, string Country, string Htltype)
//        {
//            DataSet MarkupDT = new DataSet();
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_SelectHotelMarkup");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@City", City);
//                sqlcmd.Parameters.AddWithValue("@Country", Country);
//                sqlcmd.Parameters.AddWithValue("@TripType", Htltype);
//                MarkupDT = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetHotelMarkup");
//            }
//            finally { sqlcmd.Dispose(); }
//            return MarkupDT.Tables[0];
//        }
//        //Insert data in Transacttion 
//        public int InsertTransactionRepot(string pnr, string userid, string credit, string avalbal, string debit, string sector, string paxname, string flightno, string agencyname, string rm,
//        string Status, string RoomName, string TotalPrice, string AgentMarkup)
//        {
//            int i = 0;
//            try
//            {
//                sqlcmd = new SqlCommand("InsertTransReport");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@pnr", pnr);
//                sqlcmd.Parameters.AddWithValue("@userid", userid);
//                sqlcmd.Parameters.AddWithValue("@credit", credit);
//                sqlcmd.Parameters.AddWithValue("@AvalBal", avalbal);
//                sqlcmd.Parameters.AddWithValue("@Debit", debit);
//                sqlcmd.Parameters.AddWithValue("@sector", sector);
//                sqlcmd.Parameters.AddWithValue("@paxname", paxname);
//                sqlcmd.Parameters.AddWithValue("@flightno", flightno);
//                sqlcmd.Parameters.AddWithValue("@agencyname", agencyname);
//                sqlcmd.Parameters.AddWithValue("@Rm", rm);
//                sqlcmd.Parameters.AddWithValue("@status", Status);
//                sqlcmd.Parameters.AddWithValue("@RoomName", RoomName);
//                sqlcmd.Parameters.AddWithValue("@TotalPrice", TotalPrice);
//                sqlcmd.Parameters.AddWithValue("@AgentMarkup", AgentMarkup);
//                i = Convert.ToInt32(DBhelper.ExecuteNonQuery(sqlcmd));
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "");
//            }
//            finally { sqlcmd.Dispose(); }
//            return i;
//        }

//        //get Recent Booked Hotel
//        public DataSet RecentBookedHotel()
//        {
//            DataSet bookedHotelDT = new DataSet();
//            try
//            {
//                sqlcmd = new SqlCommand("SP_HTL_RecentBooked");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                bookedHotelDT = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "GetHotelMarkup");
//            }
//            finally { sqlcmd.Dispose(); }
//            return bookedHotelDT;
//        }
//        public DataTable getCreditCardDetails(string HotelSupplier)
//        {
//            DataSet dtCC = new DataSet();
//            try
//            {
//                sqlcmd = new SqlCommand("usp_Creditcard_Details");
//                sqlcmd.CommandType = CommandType.StoredProcedure;
//                sqlcmd.Parameters.AddWithValue("@Supplier", HotelSupplier);
//                dtCC = DBhelper.ExecuteDataSet(sqlcmd);
//            }
//            catch (Exception ex)
//            {
//                InsertHotelErrorLog(ex, "getCreditCardDetails");
//            }
//            finally { sqlcmd.Dispose(); }
//            return dtCC.Tables[0];
//        }

        
//    }
//}
