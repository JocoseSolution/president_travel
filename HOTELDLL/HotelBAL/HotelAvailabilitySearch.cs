﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;
using System.Text.RegularExpressions;
//using Microsoft.Win32;
//using System.Diagnostics;
using System.Threading;
using System.Net;
using HotelShared;
using HotelDAL;

namespace HotelBAL
{
   public class HotelAvailabilitySearch
    {
        TGHotelResponse objTGHotel = new TGHotelResponse(); TBOHotelResponse objTBOHotel = new TBOHotelResponse();
        HotelCompositeList objnewServiserList = new HotelCompositeList(); AllCombindHotel objcombindHotel = new AllCombindHotel();
        HotelBAL.OYO_V2.OYOHotelSearchResponse objoyo = new OYO_V2.OYOHotelSearchResponse();
      
       
        public HotelComposite HotelAvailability(HotelSearch SearchDetails)
        {
            HotelComposite obgHotelDetails = new HotelComposite();
            ArrayList vlist1 = new ArrayList(); ArrayList vlist = new ArrayList();
            try
            {
                DateTime chekout = Convert.ToDateTime(SearchDetails.CheckOutDate);
                TimeSpan tsTimeSpan = chekout.Subtract(Convert.ToDateTime(SearchDetails.CheckInDate));
                SearchDetails.NoofNight = tsTimeSpan.Days;
                string[] citylists = SearchDetails.HTLCityList.Split(',');
                if (citylists.Length > 4)
                {
                    SearchDetails.SearchCity = citylists[0];
                    SearchDetails.SearchCityCode = citylists[1];
                    SearchDetails.Country = citylists[2];
                    SearchDetails.CountryCode = citylists[3];
                    SearchDetails.SearchType = citylists[4];
                    SearchDetails.RegionId = citylists[5];
                    SearchDetails.cityid = citylists[6];
                    //SearchDetails.Availability_0S =  citylists[6];
                }
                if (SearchDetails.CountryCode == "IN")
                    SearchDetails.HtlType = HotelStatus.Domestic.ToString();
                else
                    SearchDetails.HtlType = HotelStatus.International.ToString();

                SearchDetails = SetAuthentication(SearchDetails, "Search");
                SearchDetails.CurrancyRate = 0;//CurrancyConvert_USD_To_INR();
                objcombindHotel.AllHotelSearch = SearchDetails;
               // SearchQuery = SearchDetails;
                #region Add in Thread
                //Thread TG = new Thread(new ThreadStart(TGAvailability));
                //TG.Start();
                //vlist.Add(TG);
                //vlist1.Add(DateTime.Now);
                
                //Thread TBO = new Thread(new ThreadStart(TBOAvailability));
                //TBO.Start();
                //vlist.Add(TBO);
                //vlist1.Add(DateTime.Now);

                Thread oyo = new Thread(new ThreadStart(OYOAvailability));
                oyo.Start();
                Thread.Sleep(500);
                vlist.Add(oyo);
                vlist1.Add(DateTime.Now);
                #endregion

                #region Wait for Thread
                int counter = 0;
                while ((counter < vlist.Count))
                {
                    Thread TH = (Thread)vlist[counter];
                    if (TH.ThreadState == ThreadState.WaitSleepJoin)
                    {
                        TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
                        if ((DIFF.Seconds > 400))
                        {
                            TH.Abort();
                            counter += 1;
                        }
                    }
                    else if ((TH.ThreadState == ThreadState.Stopped))
                    {
                        counter += 1;
                    }
                }
                #endregion

                #region Mearg all Hotel
                obgHotelDetails.HotelSearchDetail = objcombindHotel.AllHotelSearch;
                obgHotelDetails.HotelOrgList = new List<HotelResult>();

                if (objcombindHotel.TGHotelList != null)
                    obgHotelDetails.HotelOrgList = obgHotelDetails.HotelOrgList.Union(objcombindHotel.TGHotelList).ToList();
                if (objcombindHotel.TBOHotelLst != null)
                    obgHotelDetails.HotelOrgList = obgHotelDetails.HotelOrgList.Union(objcombindHotel.TBOHotelLst).ToList();
                if (objcombindHotel.OYOHotelList != null)
                    obgHotelDetails.HotelOrgList = obgHotelDetails.HotelOrgList.Union(objcombindHotel.OYOHotelList).ToList();
                List<HotelResult> objHtlNewList = new List<HotelResult>();
                objHtlNewList = obgHotelDetails.HotelOrgList;
                //objHtlNewList.ToList().ForEach(u =>
                //{
                //    u.HotelName = u.HotelName.ToUpper().Replace(" HOTEL ", " ").Replace("HOTEL ", " ").Replace(" HOTEL", " ").Replace("  ", " ").Trim();
                //});
                var Hotellist = objHtlNewList.GroupBy(item => item.HotelName.ToUpper()).SelectMany(g => g.Count() > 1 ? g.Where(x => x.hotelPrice == g.Min(c => c.hotelPrice)) : g).ToList();
                // Hotellist = Hotellist.Where(x => x.hotelPrice > 0).ToList();

                obgHotelDetails.Hotelresults = Hotellist;

                if (obgHotelDetails.Hotelresults.Count == 0)
                {
                    List<HotelResult> objHotellist = new List<HotelResult>();
                    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel are not available." });
                    obgHotelDetails.Hotelresults = objHotellist;
                }

            #endregion
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "HotelAvailability " + SearchDetails.SearchCity);
            }
            return obgHotelDetails;
        }

        public HotelSearch SetAuthentication(HotelSearch SearchDetails, string AuthenticationFor)
        {
            try
            {
                HotelDA objhtlDa = new HotelDA();
                List<HotelCredencials> CredencialsList = new List<HotelCredencials>();
                //SearchDetails = objhtlDa.GetCredentials(SearchDetails, "");
                SearchDetails.CredencialsList = objhtlDa.GETCredencialsList(CredencialsList, "");
                if (AuthenticationFor == "Search")
                {
                   // SearchDetails.MarkupDS = objhtlDa.GetHtlMarkup("", SearchDetails.AgentID, SearchDetails.Country, SearchDetails.SearchCity, "", SearchDetails.HtlType, "", "", 0, "SEL");
                    SearchDetails.MarkupDS = objhtlDa.GetHtlMarkup("", SearchDetails.AgentID, SearchDetails.Country, SearchDetails.SearchCity, "", SearchDetails.HtlType, "", "", 0, "", SearchDetails.AgencyGroup, "SEL");
                    SearchDetails.SearchID = objhtlDa.GetOrderID();
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "SetAuthentication");
            }
            return SearchDetails;
        }

        protected void TGAvailability()
        {
            objcombindHotel = objTGHotel.TGHotelSearchResponse(objcombindHotel);
        }
      
        protected void TBOAvailability()
        {
            objcombindHotel = objTBOHotel.TBOHotel(objcombindHotel);
        }
        protected void OYOAvailability()
        {
            objcombindHotel = objoyo.OYOHotelSearchResponses(objcombindHotel);
        }
       // protected void EXAvailability()
       // {
       //     EXHotels = objexHotel.EXHotels(SearchQuery);
       // }
       // protected void RZAvailability()
       //{
       //    RZHotels = objRZHotel.RZHotel(SearchQuery);
       // }
        public RoomComposite SelectedHotelResponse(HotelSearch SearchDetails, HotelComposite HotelCompositResult, RoomComposite obgRoomDetails, string HotelName)
        {
           try
            {
                List<HotelResult> HtlList = new List<HotelResult>();
                HtlList = HotelCompositResult.Hotelresults;
                HtlList = HtlList.Where(x => x.HotelCode == SearchDetails.HtlCode && x.HotelCityCode == SearchDetails.HotelCityCode).ToList();
                if ((HtlList.Count > 0))
                {
                    SearchDetails.StarRating = HtlList[0].StarRating;
                    SearchDetails.HotelName = HtlList[0].HotelName;
                    SearchDetails.ThumbImage = HtlList[0].HotelThumbnailImg;
                    SearchDetails.Provider = HtlList[0].Provider;
                }
                #region Caling first Suplier Servise
                if (SearchDetails.Provider == "TG")
                {
                    SearchDetails.HotelName = SearchDetails.HtlCode;
                    obgRoomDetails = objTGHotel.TGSelectedHotelResponse(SearchDetails);
                }
               
                else if (SearchDetails.Provider == "TBO")
                {
                    obgRoomDetails = objTBOHotel.TBORoomDetails(SearchDetails);

                    if (obgRoomDetails.RoomDetails[0].tboinfosource == "FixedCombination" && SearchDetails.NoofRoom > 1)
                    {
                        obgRoomDetails.tboRoomDetail = obgRoomDetails.RoomDetails.GroupBy(x => x.tbocommonid).Select(g => g.First()).ToList();
                    }
                }
                else if (SearchDetails.Provider == "OYO")
                    obgRoomDetails = objoyo.OYOHotelRoomDetals(SearchDetails);
                #endregion
                #region If matching Hotel name are available in Other suplier then call other servise below
                List<HotelResult> filterOrgHtlList = new List<HotelResult>();
                if (HotelCompositResult.HotelOrgList != null)
                    if ((HotelCompositResult.Hotelresults.Count < (HotelCompositResult.HotelOrgList).Count))
                        filterOrgHtlList = HotelCompositResult.HotelOrgList.Where(x => x.HotelName.ToUpper().Replace(" HOTEL ", " ").Replace("HOTEL ", " ").Replace(" HOTEL", " ").Replace("  ", " ").Trim() == HotelName.ToUpper().Trim() && x.Provider != SearchDetails.Provider).ToList();
               
                if (filterOrgHtlList.Count > 0)
                {
                    RoomComposite RommCompositResultTG = new RoomComposite(); RoomComposite RommCompositResultTBO = new RoomComposite(); 
                   RoomComposite RommCompositResultOYO = new RoomComposite();
                    #region Calling other rooms
                    for (int z = 0; z < filterOrgHtlList.Count; z++)
                    {
                        switch (filterOrgHtlList[z].Provider)
                        {
                            case "TG":
                                SearchDetails.Provider = "TG";
                                SearchDetails.HtlCode = filterOrgHtlList[z].HotelCode;
                                RommCompositResultTG = objTGHotel.TGSelectedHotelResponse(SearchDetails);
                                if (RommCompositResultTG.RoomDetails.Count > 0)
                                    obgRoomDetails.RoomDetails = obgRoomDetails.RoomDetails.Union(RommCompositResultTG.RoomDetails).ToList();
                                
                                if (RommCompositResultTG.SelectedHotelDetail != null)
                                {
                                    obgRoomDetails.SelectedHotelDetail.HotelDescription += RommCompositResultTG.SelectedHotelDetail.HotelDescription;
                                    obgRoomDetails.SelectedHotelDetail.HotelImage += RommCompositResultTG.SelectedHotelDetail.HotelImage;
                                    obgRoomDetails.SelectedHotelDetail.Attraction += RommCompositResultTG.SelectedHotelDetail.Attraction;
                                    obgRoomDetails.SelectedHotelDetail.RoomAmenities += RommCompositResultTG.SelectedHotelDetail.RoomAmenities;
                                    obgRoomDetails.SelectedHotelDetail.HotelAmenities += RommCompositResultTG.SelectedHotelDetail.HotelAmenities;
                                }
                                break;
                            case "TBO":
                                SearchDetails.Provider = "TBO";
                                SearchDetails.HtlCode = filterOrgHtlList[z].HotelCode;
                                SearchDetails.HotelCityCode = filterOrgHtlList[z].HotelCityCode;

                                List<HotelResult> GALHtlList = new List<HotelResult>();
                                GALHtlList = HotelCompositResult.HotelOrgList.Where(x => x.HotelName.ToUpper().Replace(" HOTEL ", " ").Replace("HOTEL ", " ").Replace(" HOTEL", " ").Replace("  ", " ").Trim() == HotelName.ToUpper().Trim() && x.Provider == "GAL").ToList();
                                RommCompositResultTBO = objTBOHotel.TBORoomDetails(SearchDetails);
                                if (RommCompositResultTBO.RoomDetails.Count > 0)
                                    obgRoomDetails.RoomDetails = obgRoomDetails.RoomDetails.Union(RommCompositResultTBO.RoomDetails).ToList();

                                if (RommCompositResultTBO.SelectedHotelDetail != null)
                                {
                                    obgRoomDetails.SelectedHotelDetail.HotelChain = RommCompositResultTBO.SelectedHotelDetail.HotelChain;
                                    obgRoomDetails.SelectedHotelDetail.HotelDescription += RommCompositResultTBO.SelectedHotelDetail.HotelDescription;
                                    obgRoomDetails.SelectedHotelDetail.HotelImage += RommCompositResultTBO.SelectedHotelDetail.HotelImage;
                                    obgRoomDetails.SelectedHotelDetail.Attraction += RommCompositResultTBO.SelectedHotelDetail.Attraction;
                                    obgRoomDetails.SelectedHotelDetail.RoomAmenities += RommCompositResultTBO.SelectedHotelDetail.RoomAmenities;
                                    obgRoomDetails.SelectedHotelDetail.HotelAmenities += RommCompositResultTBO.SelectedHotelDetail.HotelAmenities;
                                    if (RommCompositResultTBO.SelectedHotelDetail.HotelContactNo != "")
                                        SearchDetails.HotelContactNo = RommCompositResultTBO.SelectedHotelDetail.HotelContactNo;
                                }
                                break;
                                                      
                        }
                    }
                    #endregion
                }
                else
                {
                    if (SearchDetails.Provider == "ROOMXML" || SearchDetails.Provider == "INNSTANT")
                    {
                        SearchDetails.HotelContactNo = obgRoomDetails.SelectedHotelDetail.HotelContactNo;
                    }
                }
                #endregion
                obgRoomDetails.RoomDetails = obgRoomDetails.RoomDetails.OrderBy(x => x.TotalRoomrate).ToList();

            }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "SelectedHotelResponse");
           }
           return obgRoomDetails;
        }

        //public RoomComposite SelectedHotelResponseNEWForBothService(HotelSearch SearchDetails)
        //{
        //    RoomComposite obgRoomDetails = new RoomComposite();
        //    try
        //    {
        //        GTAHotelResponse objGTAHotel = new GTAHotelResponse();
        //        RoomComposite GTARoomDetails = new RoomComposite();
        //        GTARoomDetails = objGTAHotel.GTARoomDetals(SearchDetails);
        //        if (SearchDetails.CountryCode == "IN")
        //        {
        //            TGHotelResponse objTGHotel = new TGHotelResponse();
        //            RoomComposite TGRoomDetails = new RoomComposite();
        //            TGRoomDetails = objTGHotel.TGSelectedHotelResponse(SearchDetails);

        //            var Hotellist = GTARoomDetails.RoomDetails.Union(TGRoomDetails.RoomDetails).ToList();
        //            obgRoomDetails.RoomDetails = Hotellist;
        //            obgRoomDetails.SelectedHotelDetail = TGRoomDetails.SelectedHotelDetail;
        //        }
        //        else
        //            obgRoomDetails = GTARoomDetails;
        //    }
        //    catch (Exception ex)
        //    {
        //        HotelDA.InsertHotelErrorLog(ex, "");
        //    }
        //    return obgRoomDetails;
        //}

        public HotelBooking PreBookingHotelRequest(HotelBooking HotelDetails)
        {
             try
            {
                HotelDA objhtlDa = new HotelDA();
                if (HotelDetails.Provider == "TG")
                {
                    HotelDetails.Provider = "TGBooking";
                    HotelDetails = objhtlDa.GetBookingCredentials(HotelDetails, HotelDetails.Provider);
                    HotelDetails = objTGHotel.TGHotelsPreBooking(HotelDetails);
                }
                //else if (HotelDetails.Provider == "EX")
                //{
                //    HotelDetails = objhtlDa.GetBookingCredentials(HotelDetails, HotelDetails.Provider);
                //    HotelDetails = objexHotel.EXHotelPreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "GTA")
                //    HotelDetails = objGTAHotel.GTAHotelsPreBooking(HotelDetails);
                //else if (HotelDetails.Provider == "RZ")
                //    HotelDetails = objRZHotel.RZHotelsPreBooking(HotelDetails);
                //else if (HotelDetails.Provider == "RoomXML")
                //    HotelDetails = objRoomXmlHotel.RoomXMLHotelsPreBooking(HotelDetails);
                else if (HotelDetails.Provider == "TBO")
                    HotelDetails = objTBOHotel.TBOHotelsPreBooking(HotelDetails);
                //Update PreBook Hotels Price Request and Response XML Log
                int m = objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.ProBookingReq, HotelDetails.ProBookingRes, HotelDetails.Provider, "HtlUpdateProBooking");
            }
             catch (Exception ex)
             {
                 HotelDA.InsertHotelErrorLog(ex, "PreBookingHotelRequest");
             }
            return HotelDetails;
        }
      
        public HotelBooking HotelBookingRequest(HotelBooking HotelDetails)
        {
             HotelDA objhtlDa = new HotelDA();
            try
            {
                if (HotelDetails.Provider == "TGBooking")
                    HotelDetails = objTGHotel.TGHotelsBooking(HotelDetails);
                else if (HotelDetails.Provider == "TBO")
                    HotelDetails = objTBOHotel.TBOHotelsBooking(HotelDetails);
                //else if (HotelDetails.Provider == "EX")
                //{
                //    HotelDetails.Provider = "EXBooking";
                //    HotelDetails = objhtlDa.GetBookingCredentials(HotelDetails, HotelDetails.Provider);
                //    HotelDetails = objexHotel.EXHotelsBooking(HotelDetails);
                //}
                // else if (HotelDetails.Provider == "RoomXML")
                //    HotelDetails = objRoomXmlHotel.RoomXMLHotelsBooking(HotelDetails);
                // //Update Booking Confirmation Request and Response XML Log
                objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.BookingConfReq, HotelDetails.BookingConfRes, HotelDetails.Provider, "HtlUpdateBooking");             
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "HotelBookingRequest");
            }
            return HotelDetails;
        }
       
        #region Corency Converter from Google API
        public decimal CurrancyConvert_USD_To_INR()
        {
            HotelDA objhtlDa = new HotelDA();
            try
            {
                WebClient web = new WebClient();
                string url = string.Format("http://www.google.com/finance/converter?a=1&from=USD&to=INR");
                byte[] databuffer = Encoding.ASCII.GetBytes("test=postvar&test2=another");
                HttpWebRequest _webreqquest = (HttpWebRequest)WebRequest.Create(url);
                _webreqquest.Method = "POST";
                _webreqquest.ContentType = "application/x-www-form-urlencoded";
                _webreqquest.Timeout = 11000;
                _webreqquest.ContentLength = databuffer.Length;
                Stream PostData = _webreqquest.GetRequestStream();
                PostData.Write(databuffer, 0, databuffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)_webreqquest.GetResponse();
                Stream finalanswer = WebResp.GetResponseStream();
                StreamReader _answer = new StreamReader(finalanswer);
                string[] value = Regex.Split(_answer.ReadToEnd(), "&nbsp;");
                var a = Regex.Split(value[1], "<div id=currency_converter_result>1 USD = <span class=bld>");
                decimal rate = 0;
                try
                {
                  rate=  Convert.ToDecimal(Regex.Split(a[1], " INR</span>")[0].Trim());
                  int i = objhtlDa.UpdateCurrancyValue(rate);
                }
                catch (Exception ex)
                {
                    ConvertCurrancy_USD_To_INR_from_xe();
                    HotelDA.InsertHotelErrorLog(ex, a.ToString());
                }
                return rate;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "CurrancyConvert_USD_To_INR");
                decimal curval = objhtlDa.SelectCurrancyValue();
                if (curval > 0)
                    return curval;
                else
                    return ConvertCurrancy_USD_To_INR_from_xe();
            }
        }
        #endregion

        #region Corency Converter from http://www.xe.com
        public decimal ConvertCurrancy_USD_To_INR_from_xe()
        {
            HotelDA HTLST = new HotelDA();
            try
            {
                HttpWebRequest HttpWebReq = (HttpWebRequest)WebRequest.Create("http://www.xe.com/ucc/convert/?Amount=1&From=USD&To=INR");
                HttpWebReq.Timeout = 11000;
                HttpWebResponse WebResponse = (HttpWebResponse)HttpWebReq.GetResponse();
                Stream responseStream = WebResponse.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                reader = new StreamReader(HttpWebReq.GetResponse().GetResponseStream());
                string response = reader.ReadToEnd();

                string convert__1 = response.Substring(response.IndexOf("XE Currency Converter"), response.IndexOf("View") - response.IndexOf("XE Currency Converter"));
                convert__1 = convert__1.Substring(convert__1.IndexOf("uccRes"), convert__1.IndexOf("uccResRgn") - convert__1.IndexOf("uccRes"));
                string[] str = Regex.Split(convert__1, "<td");
                string[] str1 = str[3].Split('>');
                string[] inr__2 = str1[1].Split('&');
                decimal rate = 0;
                try 
                {
                    rate = Convert.ToDecimal(inr__2[0].Trim());
                    HTLST.UpdateCurrancyValue(rate);
                }
                catch(Exception ex)
                {
                    HotelDA.InsertHotelErrorLog(ex, convert__1);
                }
                return rate;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "ConvertCurrancy_USD_To_INR_from_xe");
                decimal curval = HTLST.SelectCurrancyValue();
                if (curval > 0)
                    return curval;
                else
                    return 0;
            }
        }
        #endregion
        public string SearchBookingItem(HotelSearch SearchDetails)
        {
            try
            {
                SearchDetails.Provider = "GTA";
                SearchDetails.HtlType = HotelStatus.International.ToString();
                SearchDetails = SetAuthentication(SearchDetails, "");
               // GTAHotelResponse objhtl=new GTAHotelResponse();
               //return objhtl.SearchHotelBookingItem(SearchDetails);
                return "Not found";
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
                return "Not found";
            }
           
        }
        public string ItemDownlode(HotelSearch SearchDetails)
       {
           try
           {
                SearchDetails.Provider = "GTA";
                SearchDetails.HtlType = HotelStatus.International.ToString();
                SearchDetails = SetAuthentication(SearchDetails,"");
               // GTAHotelResponse objhtl=new GTAHotelResponse();
                string filedownloading = "";//=objhtl.HotelItemDownlode(SearchDetails);
               if(filedownloading=="Downloaded")
               {
                   //Hotel
                   string unziped = UnZipFile(System.Configuration.ConfigurationManager.AppSettings["ItemDownloadPath"]);
                   //SightSeeing
                   //string unziped = UnZipFile(System.Configuration.ConfigurationManager.AppSettings["ShiteseeingDATA"]);
                   if (unziped == "true")
                       return "File Download Completed. and folder Unziped";
                   else
                       return "File Download Completed. Unziped not complete";
               }
               else
                   return "Only File Download Completed.";
                //return objhtl.HotelItemDownlode(SearchDetails);

            //    UnZipFile(ConfigurationManager.AppSettings("ExtractFilePath"), ConfigurationManager.AppSettings("ItemDownloadPath") & ".zip")
            //    downloadStatus.InnerText = "File Download Completed."
            //    'UnZipFile("D:\GTADownload", "D:\GTA_DATA_2013_01.rar")
           }
           catch(Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "");
               return ex.Message;
           }
       }

        public string UnZipFile(string filepath)
        {
            try
            {
                System.Diagnostics.Process pross = new System.Diagnostics.Process();
                pross.StartInfo.FileName = @"C:\Program Files (x86)\WinRAR\WinRAR.EXE"; //zip exe path for Windows 7
                //pross.StartInfo.FileName = @"C:\Program Files\WinRAR\WinRAR.EXE "; //zip exe path forWindosXP
                pross.StartInfo.Arguments = " x -y " + filepath.Replace(".zip", "") + " " + filepath;
                pross.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                pross.Start();
                pross.WaitForExit();
                return "true";
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "");
                return ex.Message;
            }
        }
        public List<HotelResult> Promotion_HotelSearchResponse(HotelSearch SearchDetails)
        {
            //HotelComposite obgHotelDetails = new HotelComposite();
            List<HotelResult> objHotellist = new List<HotelResult>();
            try
            {
                DateTime chekout = Convert.ToDateTime(SearchDetails.CheckOutDate);
                TimeSpan tsTimeSpan = chekout.Subtract(Convert.ToDateTime(SearchDetails.CheckInDate));
                SearchDetails.NoofNight = tsTimeSpan.Days;
                string[] citylists = SearchDetails.HTLCityList.Split(',');
                if (citylists.Length > 4)
                {
                    SearchDetails.SearchCity = citylists[0];
                    SearchDetails.SearchCityCode = citylists[1];
                    SearchDetails.Country = citylists[2];
                    SearchDetails.CountryCode = citylists[3];
                    SearchDetails.SearchType = citylists[4];
                }
                if (SearchDetails.CountryCode == "IN")
                    SearchDetails.HtlType = HotelStatus.Domestic.ToString();
                else
                    SearchDetails.HtlType = HotelStatus.International.ToString();

                SearchDetails = SetAuthentication(SearchDetails, "Search");
                SearchDetails.CurrancyRate = CurrancyConvert_USD_To_INR();
                if (SearchDetails.CountryCode == "IN")
                {
                    TGHotelResponse objTGHotel = new TGHotelResponse();
                   // HotelComposite TGHotels = new HotelComposite();
                    objHotellist = objTGHotel.Promotion_Hotels(SearchDetails);

                    //SearchDetails.NoofHotel = TGHotels.Hotelresults.Count;
                    //obgHotelDetails.HotelOrgList = TGHotels.Hotelresults;
                    //obgHotelDetails.Hotelresults = TGHotels.Hotelresults;
                    //obgHotelDetails.HotelSearchDetail = TGHotels.HotelSearchDetail;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "Promotion_HotelSearchResponse");
            }
            return objHotellist;
        }
        //public HotelCancellation CancellationHotelBooking(HotelCancellation HotelDetails)
        //{
        //    HotelDA objhtlDa = new HotelDA();
        //    try
        //    {
        //        HotelDetails = objhtlDa.GetCancleCredentials(HotelDetails);
        //        if (HotelDetails.Provider == "TGBooking")
        //            HotelDetails = objTGHotel.TGHotelsCancelation(HotelDetails);
        //        //else if (HotelDetails.Provider == "GTA")
        //        //    HotelDetails = objGTAHotel.GTAHotelCancalltion(HotelDetails);
        //        //else if (HotelDetails.Provider == "RZ")
        //        //    HotelDetails = objRZHotel.RZHotelsCancelation(HotelDetails);
        //        else if (HotelDetails.Provider == "EX")
        //            HotelDetails = objexHotel.EXHotelCancalltion(HotelDetails);
        //        else if (HotelDetails.Provider == "TBO")
        //            HotelDetails = OBJTBOHotel.TBOHotelCancelation(HotelDetails);
        //        else

        //            HotelDetails = objRoomXmlHotel.RoomXMLCancelBooking(HotelDetails);
        //        //Update Booking Confirmation Request and Response XML Log
        //        objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.BookingCancelReq, HotelDetails.BookingCancelRes, HotelDetails.Provider, "HtlUpdateCancle");
        //    }
        //    catch (Exception ex)
        //    {
        //        HotelDA.InsertHotelErrorLog(ex, "CancellationHotelBooking");
        //    }
        //    return HotelDetails;
        //}
    }
}
