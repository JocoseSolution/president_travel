﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HotelShared;
using System.Collections;
using System.Data;
using HotelDAL;
using System.Web;
using System.Xml.Linq;
using System.Security.Cryptography;
namespace HotelBAL
{
   public class EXHotelRequest
   {
      
       public HotelSearch EXHotelSearchRequest(HotelSearch HtlSearchQuery)
       {
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               int Timestamp = (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
               string v_sig = MD5GenerateHash(HtlSearchQuery.EXAPIKEY + HtlSearchQuery.EXSecretKey + Timestamp);
               ReqXml.Append("&sig=" + v_sig + "&apiKey=" + HtlSearchQuery.EXAPIKEY + "&cid=" + HtlSearchQuery.EXCID + "&xml=");
               ReqXml.Append("<HotelListRequest>");
               if (HtlSearchQuery.HotelName == "")
               {
                   ReqXml.Append("<city>" + HtlSearchQuery.SearchCity + "</city>");
                   ReqXml.Append("<countryCode>" + HtlSearchQuery.CountryCode + "</countryCode>");
                   if (HtlSearchQuery.StarRating == "0")
                       ReqXml.Append("<minStarRating>" + Convert.ToString(HtlSearchQuery.StarRating) + "</minStarRating>");
               }
               else
                   ReqXml.Append("<hotelIdList>" + HtlSearchQuery.HotelName + "</hotelIdList>");

               string[] checkindate = HtlSearchQuery.CheckInDate.Split('-');
               string[] checkoutdate = HtlSearchQuery.CheckOutDate.Split('-');
               ReqXml.Append("<arrivalDate>" + checkindate[1] + "/" + checkindate[2] + "/" + checkindate[0] + "</arrivalDate>");
               ReqXml.Append("<departureDate>" + checkoutdate[1] + "/" + checkoutdate[2] + "/" + checkoutdate[0] + "</departureDate>");
               if (HtlSearchQuery.HotelName == "")
                    ReqXml.Append("<numberOfResults>200</numberOfResults>");
               ReqXml.Append("<maxRatePlanCount>4</maxRatePlanCount >");
               ReqXml.Append("<RoomGroup>");
               for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
               {
                   ReqXml.Append("<Room>");
                   ReqXml.Append("<numberOfAdults>" + HtlSearchQuery.AdtPerRoom[i].ToString() + "</numberOfAdults>");
                   if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                   {
                       ReqXml.Append("<numberOfChildren>" + HtlSearchQuery.ChdPerRoom[i].ToString() + "</numberOfChildren>");
                       for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                       {
                           ReqXml.Append("<childAges>" + HtlSearchQuery.ChdAge[i, j] + "</childAges>");
                       }
                   }
                   ReqXml.Append("</Room>");
               }
               ReqXml.Append("</RoomGroup>");

               ReqXml.Append("<includeDetails>true</includeDetails>");
               ReqXml.Append("</HotelListRequest>");
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXHotelSearchRequest");
           }
           HtlSearchQuery.EX_HotelSearchReq = ReqXml.ToString();
           return HtlSearchQuery;
       }

       public HotelSearch EXHotelInformation(HotelSearch HtlSearch)
       {
           XDocument doc = XDocument.Parse(((HotelShared.HotelSearch)(HttpContext.Current.Session["HotelSearch"])).EX_HotelSearchRes);
           string serssionId = doc.Descendants("customerSessionId").Select(x => x.Value).SingleOrDefault(); 
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               int Timestamp = (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
               string v_sig = MD5GenerateHash(HtlSearch.EXAPIKEY + HtlSearch.EXSecretKey + Timestamp);

               ReqXml.Append("&sig=" + v_sig + "&apiKey=" + HtlSearch.EXAPIKEY + "&cid=" + HtlSearch.EXCID + "&xml=");
               ReqXml.Append("<HotelInformationRequest>");
               ReqXml.Append("<customerSessionId>" + serssionId + "</customerSessionId>");
               ReqXml.Append("<hotelId>" + HtlSearch.HtlCode + "</hotelId>");
               ReqXml.Append("</HotelInformationRequest>");
              
           }
           catch (Exception ex)
           {
               ReqXml.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXHotelInformation");
           }
           HtlSearch.EXHtlInfoReq = ReqXml.ToString();
           return HtlSearch;
       }

       public HotelBooking ExHotelAvailability(HotelBooking HtlSearch)
       {
           XDocument doc = XDocument.Parse(((HotelShared.HotelSearch)(HttpContext.Current.Session["HotelSearch"])).EX_HotelSearchRes);
           string serssionId = doc.Descendants("customerSessionId").Select(x=>x.Value).SingleOrDefault();
         
           StringBuilder sb = new StringBuilder();
           try
           {
               int Timestamp = (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
               string v_sig = MD5GenerateHash(HtlSearch.EXAPIKEY + HtlSearch.EXSecretKey + Timestamp);
               sb.Append("&sig=" + v_sig + "&apiKey=" + HtlSearch.EXAPIKEY + "&cid=" + HtlSearch.EXCID + "&xml=");
               sb.Append("<HotelRoomAvailabilityRequest>");
               sb.Append("<customerSessionId>" + serssionId + "</customerSessionId>");
               sb.Append(" <hotelId>" + HtlSearch.HtlCode + "</hotelId>");
               sb.Append("<arrivalDate>" + Convert.ToDateTime(HtlSearch.CheckInDate).ToString("MM/dd/yyyy") + "</arrivalDate>");
               sb.Append("<departureDate>" + Convert.ToDateTime(HtlSearch.CheckOutDate).ToString("MM/dd/yyyy") + "</departureDate>");
               sb.Append("<rateKey>" + HtlSearch.EXRateKey + "</rateKey>");
               sb.Append("<roomTypeCode>" + HtlSearch.RoomTypeCode + "</roomTypeCode>");
               sb.Append("<rateCode>" + HtlSearch.RoomPlanCode + "</rateCode>");
               sb.Append("<RoomGroup>");

               for (int i = 0; i < Convert.ToInt32(HtlSearch.NoofRoom); i++)
               {
                   sb.Append("<Room>");
                   sb.Append("<numberOfAdults>" + HtlSearch.AdtPerRoom[i].ToString() + "</numberOfAdults>");
                   if (Convert.ToInt32(Convert.ToInt32(HtlSearch.ChdPerRoom[i])) > 0)
                   {
                       sb.Append("<numberOfChildren>" + HtlSearch.ChdPerRoom[i].ToString() + "</numberOfChildren>");
                       for (int j = 0; j < Convert.ToInt32(HtlSearch.ChdPerRoom[i]); j++)
                       {
                           sb.Append("<childAges>" + HtlSearch.ChdAge[i, j] + "</childAges>");
                       }
                   }
                   sb.Append("</Room>");
               }
               sb.Append("</RoomGroup>");

               sb.Append("<includeDetails>true</includeDetails>");
               sb.Append("<includeRoomImages>true</includeRoomImages>");
               sb.Append("</HotelRoomAvailabilityRequest>");
              
           }
           catch (Exception ex)
           {
               sb.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "ExHotelAvailability");
           }
           HtlSearch.ProBookingReq = sb.ToString();
           return HtlSearch;
       }
      
       public HotelBooking EXHotelBookingRequestXML(HotelBooking Hbooking)
       {
          
           HotelDA db = new HotelDA();
           XDocument doc = XDocument.Parse(((HotelShared.HotelBooking)(HttpContext.Current.Session["HotelBooking"])).ProBookingRes);
           string serssionId = doc.Descendants("customerSessionId").Select(x => x.Value).SingleOrDefault(); 
           string UserAgent = (string)HttpContext.Current.Session["UserAgent"];
           StringBuilder sb = new StringBuilder();
           try
           {
               DataTable dt;//= db.getCreditCardDetails("EX");
               int Timestamp = (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
               string v_sig = MD5GenerateHash(Hbooking.EXAPIKEY + Hbooking.EXSecretKey + Timestamp);

               sb.Append("&sig=" + v_sig + "&apiKey=" + Hbooking.EXAPIKEY + "&cid=" + Hbooking.EXCID + "&xml=");
               sb.Append("<HotelRoomReservationRequest>");
               sb.Append("<customerSessionId>" + serssionId + "</customerSessionId>");
               sb.Append("<affiliateConfirmationId>" + Hbooking.Orderid + "</affiliateConfirmationId>");
               sb.Append("<customerUserAgent>" + UserAgent + "</customerUserAgent>");
               sb.Append("<customerIpAddress>" + getIPAddress() + "</customerIpAddress>");
               sb.Append("<hotelId>" + Hbooking.HtlCode + "</hotelId>");
               sb.Append("<arrivalDate>" + Convert.ToDateTime(Hbooking.CheckInDate).ToString("MM/dd/yyyy") + "</arrivalDate>");
               sb.Append("<departureDate>" + Convert.ToDateTime(Hbooking.CheckOutDate).ToString("MM/dd/yyyy") + "</departureDate>");
               sb.Append("<supplierType>E</supplierType>");
               sb.Append("<rateKey>" + Hbooking.EXRateKey + "</rateKey>");
               sb.Append("<rateCode>" + Hbooking.RoomPlanCode + "</rateCode>");
               sb.Append("<roomTypeCode>" + Hbooking.RoomTypeCode + "</roomTypeCode>");
               sb.Append("<chargeableRate>" + Hbooking.Total_Org_Roomrate + "</chargeableRate>");
               sb.Append("<RoomGroup>");

               ArrayList Guestlist = new ArrayList();
               Guestlist = Hbooking.AdditinalGuest;

               for (int i = 0; i < Convert.ToInt32(Hbooking.NoofRoom); i++)
               {
                  
                   sb.Append("<Room>");
                   sb.Append("<numberOfAdults>" + Hbooking.AdtPerRoom[i].ToString() + "</numberOfAdults>");
                   if (Convert.ToInt32(Convert.ToInt32(Hbooking.ChdPerRoom[i])) > 0)
                   {
                       sb.Append("<numberOfChildren>" + Hbooking.ChdPerRoom[i].ToString() + "</numberOfChildren>");
                       for (int j = 0; j < Convert.ToInt32(Hbooking.ChdPerRoom[i]); j++)
                       {
                           sb.Append("<childAges>" + Hbooking.ChdAge[i, j] + "</childAges>");
                       }
                   }
                   if (i == 0)
                   {
                       sb.Append("<firstName>" + Hbooking.PGFirstName + "</firstName>");
                       sb.Append("<lastName>" + Hbooking.PGLastName + "</lastName>");
                   }
                   else
                   {
                       ArrayList li = new ArrayList();
                       li = (ArrayList)Guestlist[i-1];
                       sb.Append("<firstName>"+li[1].ToString()+"</firstName>");
                       sb.Append("<lastName>" + li[2].ToString() + "</lastName>");
                   }
                  
                   sb.Append("<bedTypeId>" + Hbooking.EXBedTypeId + "</bedTypeId>");
                   sb.Append("<smokingPreference>" + Hbooking.EXSmoking + "</smokingPreference>");
                   sb.Append("</Room>");
               }
               sb.Append("</RoomGroup>");
               ///////////////////////////
               sb.Append("<ReservationInfo>");

               //if (Convert.ToString(dt.Rows[0]["Email"]).Length > 0)
               //{
               //    sb.Append("<email>" + Convert.ToString(dt.Rows[0]["Email"]) + "</email>");
               //}
               //else
               //{
                   sb.Append("<email>" + Hbooking.PGEmail + "</email>");
              // }

               //sb.Append("<firstName>" + Convert.ToString(dt.Rows[0]["holderFName"]) + "</firstName>");
               //sb.Append("<lastName>" + Convert.ToString(dt.Rows[0]["holderLName"]) + "</lastName>");
               sb.Append("<homePhone>" + Hbooking.PGContact + "</homePhone>");
               sb.Append("<workPhone>" + Hbooking.PGContact + "</workPhone>");
               //sb.Append("<creditCardType>" + Convert.ToString(dt.Rows[0]["CardType"]) + "</creditCardType>");
               //sb.Append("<creditCardNumber>" + Convert.ToString(dt.Rows[0]["CardNo"]) + "</creditCardNumber>");
               //sb.Append("<creditCardExpirationMonth>" + Convert.ToString(dt.Rows[0]["ExpMonth"]) + "</creditCardExpirationMonth>");
               //sb.Append("<creditCardExpirationYear>" + Convert.ToString(dt.Rows[0]["ExpYear"]) + "</creditCardExpirationYear>");
               //sb.Append("<creditCardIdentifier>" + Convert.ToString(dt.Rows[0]["CardCVV"]) + "</creditCardIdentifier>");
               sb.Append("</ReservationInfo>");
               ///////////////////////////
               
               sb.Append("<AddressInfo>");

               //sb.Append("<address1>" + Convert.ToString(dt.Rows[0]["HolderAddress"]) + "</address1>");
               //sb.Append("<city>" + Convert.ToString(dt.Rows[0]["HolderCity"]) + "</city>");
               //if (Hbooking.CountryCode.ToUpper() == "US" || Hbooking.CountryCode.ToUpper() == "CA" || Hbooking.CountryCode.ToUpper() == "AU")
               //{
               //    sb.Append("<stateProvinceCode>" + Hbooking.CityCode + "</stateProvinceCode>");
               //}
               //sb.Append("<countryCode>" + Convert.ToString(dt.Rows[0]["holdercountryCode"]) + "</countryCode>");
               //sb.Append("<postalCode>" + Convert.ToString(dt.Rows[0]["PostalCode"]) + "</postalCode>");

               sb.Append("</AddressInfo>");
               sb.Append("</HotelRoomReservationRequest>");

               
           }
           catch (Exception ex)
           {
               sb.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXHotelBookingRequestXML");
           }
           Hbooking.BookingConfReq = sb.ToString();
           return Hbooking;
       }

       public HotelCancellation EXBookingCanellationReq(HotelCancellation HCancellation, string roombookingid)
       {
           StringBuilder sb = new StringBuilder(); HotelDA db = new HotelDA();
           try
           {
               int Timestamp = (Int32)(DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds;
               string v_sig = MD5GenerateHash(HCancellation.Can_UserID + HCancellation.Can_PropertyId + Timestamp);
               DataTable ccdt;// = db.getCreditCardDetails("EX");
               sb.Append("&sig=" + v_sig + "&apiKey=" + HCancellation.Can_UserID + "&cid=" + HCancellation.Can_Password + "&xml=");
               sb.Append("<HotelRoomCancellationRequest>");
               sb.Append("<itineraryId>" + HCancellation.BookingID + "</itineraryId>");
               //if (Convert.ToString(ccdt.Rows[0]["Email"]).Length > 0)
               //    sb.Append("<email>" + Convert.ToString(ccdt.Rows[0]["Email"]) + "</email>");
               //else
                   sb.Append("<email>" + HCancellation.PGEmail + "</email>");
               sb.Append("<confirmationNumber>" + roombookingid + "</confirmationNumber>");
               sb.Append("</HotelRoomCancellationRequest>");
           }
           catch (Exception ex)
           {
               sb.Append(ex.Message);
               HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXBookingCanellationReq");
           }
           HCancellation.BookingCancelReq = sb.ToString();
           return HCancellation;
       }

       public string getIPAddress()
       {
           string ipaddress;
           ipaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
           if (ipaddress == "" || ipaddress == null)
               ipaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
           return ipaddress;
       }
       public string getGUID()
       {
          Guid obj = Guid.NewGuid();
          return obj.ToString();
          
       }
       
       private string MD5GenerateHash(string strInput)
       {
           // Create a new instance of the MD5CryptoServiceProvider object.
           MD5 md5Hasher = MD5.Create();

           // Convert the input string to a byte array and compute the hash.
           byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(strInput));

           // Create a new Stringbuilder to collect the bytes and create a string.
           StringBuilder sBuilder = new StringBuilder();

           // Loop through each byte of the hashed data and format each one as a hexadecimal string.
           for (int nIndex = 0; nIndex < data.Length; ++nIndex)
           {
               sBuilder.Append(data[nIndex].ToString("x2"));
           }

           // Return the hexadecimal string.
           return sBuilder.ToString();
       
       }

    }
}
