﻿using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using HotelShared;
using HotelDAL;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;

namespace HotelBAL.OYO_V2
{
   public class OYOHotelSearchResponse
    {
       public AllCombindHotel OYOHotelSearchResponses(AllCombindHotel objALLcombindHotel)
       {
           HotelDA objhtlDa = new HotelDA(); ArrayList objHotelResultList = new ArrayList();
           List<HotelResult> objHotellist = new List<HotelResult>();
           objALLcombindHotel.OYOHotelList = new List<HotelResult>();
           HotelSearch objHotelSearch= new HotelSearch();
           try
           {
             //  objHotelSearch.OYO_HotelSearchReq = "{\"MeargeHotelsReq\":["; objHotelSearch.OYO_HotelSearchRes = "{\"MeargeHotelsResp\":[";
               OYOAvaibilityCalling objoyo = new OYOAvaibilityCalling(objALLcombindHotel);

               objALLcombindHotel = objcALLINGHotelinThread(objALLcombindHotel);
               //for (int i = 0; i < objHotelResultList.Count; i++)
               //{
               //    objHotellist = (List<HotelResult>)objHotelResultList[i];
               //    objALLcombindHotel.OYOHotelList = objALLcombindHotel.OYOHotelList.Union(objHotellist).ToList();
               //}
               //objHotelSearch.OYO_HotelSearchRes += "]}";
               //objHotelSearch.OYO_HotelSearchReq += "]}";
               objALLcombindHotel.AllHotelSearch = objHotelSearch;
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "OYOHotelSearchResponses");
               objHotellist.Add(new HotelResult { HotelName = " ", Provider = "OYO", hotelPrice = 0, HtlError = ex.Message });
               objALLcombindHotel.OYOHotelList = objHotellist;
           }
           return objALLcombindHotel;
       }

       public AllCombindHotel objcALLINGHotelinThread(AllCombindHotel objALLcombindHotel)
       {
           HotelDA objhtlDa = new HotelDA(); List<HotelResult> hotelList = new List<HotelResult>(); 
           try
           {
               objALLcombindHotel.AllHotelSearch.OYO_HotelSearchReq = "{\"MeargeHotelsReq\":["; objALLcombindHotel.AllHotelSearch.OYO_HotelSearchRes = "{\"MeargeHotelsResp\":[";
               List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
               CredencialsListnew = objALLcombindHotel.AllHotelSearch.CredencialsList.Where(x => x.HotelProvider == "OYO" && (x.HotelTrip == objALLcombindHotel.AllHotelSearch.HtlType || x.HotelTrip == "ALL")).ToList();
               List<HotelInformation> HotelInfo = new List<HotelInformation>();
               HotelInfo = objhtlDa.OYOHotelInfoList(objALLcombindHotel.AllHotelSearch.SearchCity.Replace("NEW", "").Trim(), "OYO", HotelInfo);
              
               if (CredencialsListnew.Count > 0 && HotelInfo.Count > 0)
               {
                   for (int i = 0; i < HotelInfo.Count; i = i + 50)
                   {
                       List<HotelResult> hotelListnew = new List<HotelResult>();
                       hotelListnew = OYOResult(CredencialsListnew, objALLcombindHotel, HotelInfo, i, i + 1);
                       hotelList = hotelList.Union(hotelListnew).ToList();
                   }
                   objALLcombindHotel.OYOHotelList = hotelList;
                   objALLcombindHotel.AllHotelSearch.OYO_HotelSearchRes += "]}";
                   objALLcombindHotel.AllHotelSearch.OYO_HotelSearchReq += "]}";
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "objcALLINGHotelinThread");
           }
           return objALLcombindHotel;
       }
     
       public  List<HotelResult> OYOResult(List<HotelCredencials> CredencialsListnew, AllCombindHotel objALLcombindHotel, List<HotelInformation> HotelInfo, int startIndex, int k)
       {
           List<HotelResult> objHotellist = new List<HotelResult>(); HotelDA objhtlDa = new HotelDA();
           HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList(); OYOPostJson objoyo = new OYOPostJson();
           string PrevReq = "", prevresp = "";
           try
           {
               PrevReq = SetAvailabilityRequest(objALLcombindHotel.AllHotelSearch, startIndex, "Availability", HotelInfo);;
               prevresp = objoyo.HotelPostJSON(CredencialsListnew[0].HotelUrl + "availability", PrevReq, CredencialsListnew[0].HotelUsername.Trim());
              
               if (!prevresp.Contains("error"))
               {
                   HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject Bookresult = JsonConvert.DeserializeObject<HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject>(prevresp.Replace("\"1\":", "\"single\":").Replace("\"2\":", "\"doble\":").Replace("\"3\":", "\"triple\":"));
                   if (Bookresult.hotels !=null)
                   {
                       objALLcombindHotel.AllHotelSearch.OYO_HotelSearchReq += PrevReq + ","; objALLcombindHotel.AllHotelSearch.OYO_HotelSearchRes += prevresp + ",";
                       if (Bookresult.hotels.Count > 0)
                       {
                       foreach (var hoteldetails in Bookresult.hotels)
                       {
                           try
                           {
                               List<HotelInformation> HotelInfonew = new List<HotelInformation>();
                               HotelInfonew = HotelInfo.Where(x => x.HotelCode.Trim() == hoteldetails.id.Trim()).ToList();

                               if (HotelInfonew.Count() > 0)
                               {
                                   if (hoteldetails.rooms.Count() > 0)
                                   {
                                   decimal roomrate = Convert.ToDecimal(hoteldetails.rooms[0].sellingAmount);
                                   MarkList = objHtlMrk.markupCalculation(objALLcombindHotel.AllHotelSearch.MarkupDS, "0", objALLcombindHotel.AllHotelSearch.AgentID, objALLcombindHotel.AllHotelSearch.SearchCity, objALLcombindHotel.AllHotelSearch.Country, "OYO", roomrate, 0);
                                   //MarkList = objHtlMrk.markupCalculation(objALLcombindHotel.AllHotelSearch.MarkupDS, "0", objALLcombindHotel.AllHotelSearch.AgentID, objALLcombindHotel.AllHotelSearch.SearchCity, objALLcombindHotel.AllHotelSearch.Country, roomrate, 0);
                                   objHotellist.Add(new HotelResult
                                   {
                                       RatePlanCode = "",
                                       RoomTypeCode = "",
                                       hotelDiscoutAmt = 0,
                                       hotelPrice = MarkList.TotelAmt,
                                       AgtMrk = MarkList.AgentMrkAmt,
                                       DiscountMsg = "",
                                       inclusions = "",
                                       HotelChain = "",
                                       HotelCode = hoteldetails.id.Trim(),
                                       HotelName = HotelInfonew[0].HotelName.Trim().Replace("'", ""),
                                       HotelCityCode = objALLcombindHotel.AllHotelSearch.SearchCityCode,
                                       HotelCity = objALLcombindHotel.AllHotelSearch.SearchCity,
                                       StarRating = "0",
                                       HotelAddress = HotelInfonew[0].Address,
                                       HotelDescription = "",
                                       HotelThumbnailImg = HotelInfonew[0].ThumbImg != "" ? HotelInfonew[0].ThumbImg : objALLcombindHotel.AllHotelSearch.BaseURL + "/Hotel/Images/NoImage.jpg",
                                       Lati_Longi = HotelInfonew[0].Latitude + "##"+ HotelInfonew[0].Longitude,
                                       HotelServices = SetAmenity_Image(HotelInfonew[0].HotelServices),
                                       ReviewRating = "",
                                       Currency = hoteldetails.currencyCode,
                                       Provider = "OYO",
                                       PopulerId = k++
                                   });
                               }
                           }
                           }
                           catch (Exception ex)
                           {
                               HotelDA.InsertHotelErrorLog(ex, "GetOYOResult_List");
                           }
                       }
                        }
                        else
                        {
                            int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", PrevReq, prevresp, "OYO", "HotelInsert");
                            objHotellist.Add(new HotelResult { HotelName = " ", Provider = "OYO", hotelPrice = 0, HtlError = "Hotel not available", Location = "" });
                        }
                   }
                   else
                   {
                       int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", PrevReq, prevresp, "OYO", "HotelInsert");
                       objHotellist.Add(new HotelResult { HotelName = " ", Provider = "OYO", hotelPrice = 0, HtlError = "Hotel not available", Location = "" });
                   }
               }
               else
               {
                   int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", PrevReq, prevresp, "OYO", "HotelInsert");
                   objHotellist.Add(new HotelResult { HotelName = " ", Provider = "OYO", hotelPrice = 0, HtlError = "Hotel not available", Location = "" });
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "GetOYOResult _" + objALLcombindHotel.AllHotelSearch.SearchCity);
               objHotellist.Add(new HotelResult { HotelName = " ", Provider = "OYO", hotelPrice = 0, HtlError = ex.Message });
               int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", PrevReq, prevresp, "OYO", "HotelInsert");
           }
           
           return objHotellist;
       }
       public RoomComposite OYOHotelRoomDetals(HotelSearch SearchDetails)
       {
           RoomComposite objRoomDetals = new RoomComposite(); HotelDA objhtlDa = new HotelDA();
           List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
           HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();MarkupList TaxMarkList = new MarkupList();
           try
           {
               #region Seting request and response
               List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
               CredencialsListnew = SearchDetails.CredencialsList.Where(x => x.HotelProvider == "OYO" && (x.HotelTrip == SearchDetails.HtlType || x.HotelTrip == "ALL")).ToList();               
               #endregion
               OYOPostJson objoyo = new OYOPostJson();
               List<HotelInformation> HotelInfo = new List<HotelInformation>();
               HotelInfo = objhtlDa.OYOHotelInfoList_Room(SearchDetails.HtlCode, "OYORoom", HotelInfo);
              // SearchDetails.OYOHotelDetailReq = SetAvailabilityRequest(SearchDetails, 0, "Availability", HotelInfo);
               SearchDetails.OYOHotelDetailReq = SetHotelDetailsRequest(SearchDetails, SearchDetails.HtlCode);
               SearchDetails.OYOHotelDetailRes = objoyo.HotelPostJSON(CredencialsListnew[0].HotelUrl.Trim() + "availability?fields=taxBreakup", SearchDetails.OYOHotelDetailReq, CredencialsListnew[0].HotelUsername.Trim());

               if (!SearchDetails.OYOHotelDetailRes.Contains("error"))
               {
                   HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject Bookresult = JsonConvert.DeserializeObject<HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject>(SearchDetails.OYOHotelDetailRes);
                   if (Bookresult.hotels != null)
                       {
                           if (Bookresult.hotels.Count >0 )
                           {
                               #region Hotel Details
                               if (HotelInfo.Count() > 0)
                               {
                                   HotelDetail.HotelAddress = HotelInfo[0].Address;
                                  
                               if (HotelInfo[0].Latitude != null)
                                       HotelDetail.Lati_Longi = HotelInfo[0].Latitude + "##"+ HotelInfo[0].Longitude;
                               
                               HotelDetail.StarRating = "0";
                               HotelDetail.HotelName = HotelInfo[0].HotelName.Trim().Replace("'", "");
                               HotelDetail.HotelCode = Bookresult.hotels[0].id.Trim();
                               HotelDetail.HotelChain = "";
                               HotelDetail.CheckInTime = HotelInfo[0].CheckInPolicy;
                               HotelDetail.CheckOutTime = HotelInfo[0].CheckOutPolicy;
                               string imgdiv = "";
                               try
                               {
                                   HotelDetail.ThumbnailUrl = HotelInfo[0].ThumbImg;
                                   if (HotelInfo[0].AllImages != "")
                                   {
                                       string[] Images = HotelInfo[0].AllImages.Split(',');
                                       for (int im = 0; im < Images.Length; im++)
                                       {
                                           imgdiv += "<img id='img" + im + "' src='" + Images[im] + "' onmouseover='return ShowHtlImg(this);' alt='' title='' class='imageHtlDetailsshow' />";
                                       }
                                   }
                                   else
                                   {
                                       HotelDetail.ThumbnailUrl = SearchDetails.BaseURL + "/Hotel/Images/NoImage.jpg";
                                       imgdiv = "<img id='img0' src='Images/NoImage.jpg' onmouseover='return ShowHtlImg(this);' alt='' title='' class='imageHtlDetailsshow' />";
                                   }
                               }
                               catch (Exception ex)
                               {
                                   HotelDA.InsertHotelErrorLog(ex, "Hotel Detals Image " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                               }
                               HotelDetail.HotelImage = imgdiv;
                               HotelDetail.HotelAmenities = "";
                               string[] amenities = HotelInfo[0].HotelServices.Split(',');
                               for (int f = 0; f < amenities.Length -1;f++ )
                               {
                                   HotelDetail.HotelAmenities += "<div class='check1'>" + amenities[f] + "</div>";
                               }
                               if (HotelInfo[0].HotelDiscription != "")
                                    HotelDetail.HotelDescription += HotelInfo[0].HotelDiscription;

                               if (Bookresult.hotels[0].restrictions != null)
                               {
                                   foreach (HotelBAL.OYO_V2.OYOAvailabilityModel.Restriction restict in Bookresult.hotels[0].restrictions)
                                   {
                                       
                                       if(restict.notes != null)
                                       {
                                           HotelDetail.HotelDescription += restict.displayName ;
                                           if (restict.notes != null)
                                           {
                                               for (int i = 0; i < restict.notes.Count; i++)
                                               {
                                                   if (i > 0)
                                                       HotelDetail.HotelDescription += ", ";
                                                   HotelDetail.HotelDescription += restict.notes[i];
                                               }
                                           }
                                       }
                                       else
                                           HotelDetail.HotelDescription += restict.displayName;
                                       HotelDetail.HotelDescription += "<br />";
                                   }
                               }
                               #endregion
                               #region Room Details
                               foreach (HotelBAL.OYO_V2.OYOAvailabilityModel.Room RoomRateInfo in Bookresult.hotels[0].rooms)
                               {
                                   try
                                   {
                                       string Org_RoomRateStr = "", MrkRoomrateStr = "", Inclusion = "";
                                       decimal baserate = 0, taxes = 0, TotalRoomRate_WithMrk = 0, MrkTotalBaseRate = 0, adminMrkAmt = 0, AgtMrkAmt = 0, TotalTax_WithMrk = 0;
                                      
                                       if (RoomRateInfo.priceDetails != null)
                                       {
                                           baserate = RoomRateInfo.priceDetails.roomPrice;

                                           for (int ni = 0; ni < RoomRateInfo.priceDetails.taxBreakup.Count; ni++)
                                           {
                                               taxes += RoomRateInfo.priceDetails.taxBreakup[ni].taxPrice;
                                           }
                                       }
                                       else
                                       {
                                           baserate = RoomRateInfo.sellingAmount;
                                           taxes = 0;
                                       }

                                       MarkList = objHtlMrk.MarkupCalculationPerRoomPerNight(SearchDetails.MarkupDS, HotelDetail.StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "OYO", baserate , SearchDetails.NoofNight, SearchDetails.NoofRoom);
                                       adminMrkAmt = MarkList.AdminMrkAmt;
                                       AgtMrkAmt = MarkList.AgentMrkAmt;
                                      // TotalMrkAmt = (MarkList.AdminMrkAmt + MarkList.AgentMrkAmt);
                                       MrkTotalBaseRate = MarkList.TotelAmt;
                                       MrkRoomrateStr += MarkList.TotelAmt.ToString() + "/";

                                       if(taxes > 0)
                                      {
                                          TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, taxes , 0);
                                          adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                          AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                         // TotalMrkAmt += (TaxMarkList.AdminMrkAmt + TaxMarkList.AgentMrkAmt);
                                          TotalTax_WithMrk = TaxMarkList.TotelAmt;
                                       }
                                       TotalRoomRate_WithMrk = MarkList.TotelAmt + TotalTax_WithMrk;
                                       int rm = 0;
                                       decimal pernight =Math.Round(MarkList.TotelAmt / SearchDetails.NoofNight);
                                       for (int ni = 0; ni < SearchDetails.NoofNight; ni++)
                                       {
                                           if (rm > 0)
                                               MrkRoomrateStr += "/";
                                           MrkRoomrateStr += pernight.ToString();
                                           rm++;
                                       }
                                       Org_RoomRateStr += "Bs:" + baserate.ToString() + "-Tx:" + taxes.ToString() + "/";

                                       if (Bookresult.hotels[0].mealPlan != null)
                                       {
                                           if (Bookresult.hotels[0].mealPlan == "")
                                               Inclusion += Bookresult.hotels[0].mealPlan;
                                       }
                                      
                                       string vatandtax="";
                                       //if (Bookresult.hotels[0].b)
                                       //    vatandtax = "<ul>Client have to paid City Tax " + Bookresult.Hotels.Hotel[0].CityTax.ToString() + " at hotel.</ul>";
                                       // if(Bookresult.Hotels.Hotel[0].Vat > 0)
                                       //     vatandtax = "<ul>Client have to paid VAT " + Bookresult.Hotels.Hotel[0].Vat.ToString() + " at hotel.</ul>";
                                       string Policy = "<div><ul>" + Bookresult.hotels[0].cancellationPolicy + "</ul></div>";
                                       objRoomList.Add(new RoomList
                                       {
                                           HotelCode = HotelDetail.HotelCode,
                                           RatePlanCode = RoomRateInfo.categoryId.ToString(),
                                           RoomTypeCode = "",
                                           RoomName = RoomRateInfo.categoryName,
                                           Total_Org_Roomrate = baserate + taxes,
                                           AmountBeforeTax = baserate,
                                           Taxes = taxes,
                                           MrkTaxes = TotalTax_WithMrk,
                                           TotalRoomrate = TotalRoomRate_WithMrk,
                                           AdminMarkupPer = MarkList.AdminMrkPercent,
                                           AdminMarkupAmt = adminMrkAmt,
                                           AdminMarkupType = MarkList.AdminMrkType,
                                           AgentMarkupPer = MarkList.AgentMrkPercent,
                                           AgentMarkupAmt = AgtMrkAmt,
                                           AgentMarkupType = MarkList.AgentMrkType,
                                           inclusions = Inclusion,
                                           CancelationPolicy = "<div><ul>" + Bookresult.hotels[0].cancellationPolicy + "</ul>" + vatandtax + "</div>",
                                           OrgRateBreakups = Org_RoomRateStr,
                                           MrkRateBreakups = MrkRoomrateStr,
                                           DiscRoomrateBreakups = "",
                                           GuaranteeType = "NetRate",
                                           RoomDescription = "",
                                           AgentServiseTaxAmt = 0,
                                           V_ServiseTaxAmt = 0,
                                           ExtraGuest_Charge = 0,
                                           discountMsg = "",
                                           DiscountAMT = 0,
                                           EssentialInformation = "",
                                           Smoking = "",
                                           AdminCommissionAmt = RoomRateInfo.commission,
                                           GSTPercentage = 0,
                                           TDSPercentage = 0,
                                           RetaintionPer = 0,
                                           MinCapvalue = 0,
                                           AgentCommissionAmt = 0,
                                           GSTAmt = 0,
                                           TDSAmt = 0,
                                           SupplierCommissionAmt = 0,
                                           SupplierCommissionPer = "0",
                                           SupplierCommisionTaxIncluded = "false",
                                           Provider = "OYO",
                                           RoomImage = HotelDetail.ThumbnailUrl,
                                           RoomRateKey = "",
                                           CurrencyCode = Bookresult.hotels[0].currencyCode                                         
                                       });
                                   }
                                   catch (Exception ex)
                                   {
                                       objRoomList.Add(new RoomList { TotalRoomrate = 0, Room_Error = ex.Message });
                                       HotelDA.InsertHotelErrorLog(ex, "Roomsadding " + SearchDetails.HotelCityCode + "_" + HotelDetail.HotelName);
                                   }
                               }
                               #endregion
                           }
                       }
                   }
               }

               objRoomDetals.RoomDetails = objRoomList;
               objRoomDetals.SelectedHotelDetail = HotelDetail;
           }
           catch (Exception ex)
           {
               objRoomList.Add(new RoomList { TotalRoomrate = 0, Room_Error = ex.Message });
               HotelDA.InsertHotelErrorLog(ex, "OYOHotelRoomDetals " + SearchDetails.HotelCityCode);
           }
           return objRoomDetals;
       }
       protected string SetAvailabilityRequest(HotelSearch SearchDetails, int startIndex, string SearchType, List<HotelInformation> HotelInfo)
       {
           CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
           TextInfo textInfo = cultureInfo.TextInfo;
           OYOPostJson objoyos = new OYOPostJson();
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               ReqXml.Append("{\"hotelIds\": [");
               if (SearchType == "Availability")
               {
                  
                   for (int i = startIndex; i < startIndex + 50; i++)
                   {
                       if (i > HotelInfo.Count - 1)
                           break;
                       if (i != startIndex)
                           ReqXml.Append(",");
                       ReqXml.Append("\"" + HotelInfo[i].HotelCode + "\"");
                   }             
               }
               else
                   ReqXml.Append("\"" + SearchType + "\"");
               ReqXml.Append("], ");
               ReqXml.Append(" \"checkin\": " + objoyos.SetEpochTimes(SearchDetails.CheckInDate).ToString() + ", \"checkout\": " + objoyos.SetEpochTimes(SearchDetails.CheckOutDate).ToString() + ",");
               ReqXml.Append(" \"adults\": " + SearchDetails.TotAdt.ToString() + ",");
               int chdcount = 0;
               if (SearchDetails.TotChd > 0)
               {
                   for (int a = 0; a < SearchDetails.NoofRoom; a++)
                   {
                       for (int j = 0; j < Convert.ToInt32(SearchDetails.ChdPerRoom[a]); j++)
                       {
                           if ( Convert.ToInt32(SearchDetails.ChdAge[a, j]) > 4)
                           chdcount++;
                       }

                   }
               }
               ReqXml.Append(" \"children\": " + chdcount.ToString() + ",");                             
               ReqXml.Append(" \"rooms\":" + SearchDetails.NoofRoom.ToString() + "}");
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "SetAvailabilityRequest");
           }
           return ReqXml.ToString();
       }
       protected string SetHotelDetailsRequest(HotelSearch SearchDetails, string Hotelcode)
       {
           CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;
           TextInfo textInfo = cultureInfo.TextInfo;
           OYOPostJson objoyos = new OYOPostJson();
           StringBuilder ReqXml = new StringBuilder();
           try
           {
               ReqXml.Append("{\"hotelIds\": [\"" + Hotelcode + "\"],");

               ReqXml.Append(" \"checkin\": " + objoyos.SetEpochTimes(SearchDetails.CheckInDate).ToString() + ", \"checkout\": " + objoyos.SetEpochTimes(SearchDetails.CheckOutDate).ToString() + ",");
               ReqXml.Append(" \"adults\": " + SearchDetails.TotAdt.ToString() + ",");
               int chdcount = 0;
               if (SearchDetails.TotChd > 0)
               {
                   for (int a = 0; a < SearchDetails.NoofRoom; a++)
                   {
                       for (int j = 0; j < Convert.ToInt32(SearchDetails.ChdPerRoom[a]); j++)
                       {
                           if (Convert.ToInt32(SearchDetails.ChdAge[a, j]) > 4)
                               chdcount++;
                       }
                   }
               }
               ReqXml.Append(" \"children\": " + chdcount.ToString() + ",");
               ReqXml.Append(" \"rooms\":" + SearchDetails.NoofRoom.ToString() + "}");
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "SetHotelDetailsRequest");
           }
           return ReqXml.ToString();
       }
       protected string SetAmenity_Image(string AmenitiesList)
       {
           string InclImg = "";
           try
           {
               if (AmenitiesList != "")
               {
                   string[] Amenities = AmenitiesList.Split(',');
                   if (Amenities.Count() > 0)
                   {
                       int j = 0, k = 0, l = 0, m = 0, n = 0, o = 0, p = 0, q = 0, r = 0, s = 0, t = 0, u = 0, v = 0, w = 0, x = 0, y = 0;
                       for (int a = 0; a < Amenities.Length;a++ )
                       {
                           if (Amenities[a] != "")
                           {
                               switch (Amenities[a].Trim())
                               {
                                   case "Parking Facility":
                                           InclImg += "<img src='../Hotel/Images/Facility/Parking.png' title='Parking' class='IconImageSize' /><span class='hide'>Parking</span>";                                         
                                       break;
                                  
                                   case "Reception":
                                           InclImg += "<img src='../Hotel/Images/Facility/lounge.png' title='Room Service' class='IconImageSize' /><span class='hide'>Room Services</span>";
                                           break;
                                   case "Swimming Pool":
                                           InclImg += "<img src='../Hotel/Images/Facility/swimming.png' title='Outdoor Swimming Pool' class='IconImageSize' /><span class='hide'>Swimming Pool</span>";
                                        
                                       break;

                                   case "BathTub":
                                           InclImg += "<img src='../Hotel/Images/Facility/jacuzzi.png' title='Indoor Swimming Pool' class='IconImageSize' /><span class='hide'>Tub Bath</span>";                                       
                                       break;
                                   case "Laundry":
                                       InclImg += "<img src='../Hotel/Images/Facility/laundary.png' title='Laundry facilities' class='IconImageSize' /><span class='hide'>Laundry Services</span>";
                                       break;
                                   case "Banquet Hall":
                                   case "Conference Room":
                                       if (o == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Business centre' class='IconImageSize' /><span class='hide'>Business Facilities</span>";
                                           o = 1;
                                       }
                                       break;
                                  
                                   case "Wheelchair Accessible":
                                           InclImg += "<img src='../Hotel/Images/Facility/handicap.png' title='Disabled facilities' class='IconImageSize' /><span class='hide'>Disabled Facilities</span>";
                                          
                                       break;
                                   case "Wellness Centre":
                                   case "Gym":
                                       if (q == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/health_club.png' title='Gymnasium' class='IconImageSize' /><span class='hide'>Gym</span>";
                                           q = 1;
                                       }
                                       break;
                                   
                                   case "WiFi":
                                   case "Free Wifi":
                                   case "Inernet":
                                       if (r == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/wifi.gif' title='Internet' class='IconImageSize' /><span class='hide'>Internet/Wi-Fi</span>";
                                           r = 1;
                                       }
                                       break;
                                   
                                   case "Bar":
                                   case "Restaurant":
                                        if (j == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/bar.png' title='Mini bar' class='IconImageSize' /><span class='hide'>Restaurant/Bar</span>";
                                           j = 1;
                                       }
                                       break;
                                   
                                   case "Spa":
                                           InclImg += "<img src='../Hotel/Images/Facility/sauna.png' title='Sauna' class='IconImageSize' /><span class='hide'>Spa/Massage/Wellness/Sauna</span>";
                                          
                                       break;
                                   case "478":
                                       InclImg += "<img src='../Hotel/Images/Facility/lobby.png' title='Lobby' class='IconImageSize' /><span class='hide'>Lobby</span>";
                                       break;
                                  
                                   case "HDTV":
                                   case "TV":
                                       if (u == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/TV.png' title='TV' class='IconImageSize' /><span class='hide'>TV</span>";
                                           u++;
                                       }
                                       break;
                                   case "hair dryer":
                                   case "hair dresser":
                                       if (v == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/bath.png' title='Hair Dryer' class='IconImageSize' /><span class='hide'>Hair Dryer</span>";
                                           v++;
                                       }
                                       break;
                                   case "9":
                                   case "378":
                                   case "218":
                                   case "356":
                                       if (v == 0)
                                       {
                                           InclImg += "<img src='../Images/Hotel/Facility/tennis.png' title='AC' class='IconImageSize' /><span class='hide'>Tennis</span>";
                                           v++;
                                       }
                                       break;
                                   case "172":
                                       InclImg += "<img src='../Images/Hotel/Facility/golf.png' title='Lifts' class='IconImageSize' /><span class='hide'>golf</span>";
                                       break;
                                   case "114":
                                   case "126":
                                       if (w == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/beauty.png' title='Beauty parlour' class='IconImageSize' /><span class='hide'>Beauty Parlour</span>";
                                           w++;
                                       }
                                       break;
                                   case "123":
                                   case "159":
                                   case "342":
                                   case "386":
                                   case "427":
                                       if (x == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/babysitting.png' title='Baby' class='IconImageSize' /><span class='hide'>Baby Facilities</span>";
                                           x++;
                                       }
                                       break;
                                   case "Elevator":
                                       InclImg += "<img src='../Hotel/Images/Facility/elevator.png' title='Lift' class='IconImageSize' /><span class='hide'>Lift</span>";
                                       break;

                                   case "139":
                                   case "274":
                                   case "286":
                                       if (y == 0)
                                       {
                                           InclImg += "<img src='../Hotel/Images/Facility/Airport_transfer.png' title='Travel agency facilities' class='IconImageSize' /><span class='hide'>Travel & Transfers</span>";
                                           y++;
                                       }
                                       break;
                               }
                           }
                       }
                   }
               }
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex, "SetAmenity_Image");
           }
           return InclImg;
       }
       //protected decimal SetEpochTimes(string dates)
       //{
       //    DateTime chekout = Convert.ToDateTime(dates);
       //    TimeSpan tsTimeSpan = chekout.Subtract(DateTime.Now);
       //    return Math.Round(Convert.ToDecimal((DateTime.UtcNow.AddDays(tsTimeSpan.Days) - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds));            
       //}

    }
}
