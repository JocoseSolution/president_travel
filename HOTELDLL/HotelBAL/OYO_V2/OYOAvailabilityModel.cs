﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelBAL.OYO_V2
{
    class OYOAvailabilityModel
    {
        public class Restriction
        {
            public string name { get; set; }
            public string display_name { get; set; }
            public List<string> notes { get; set; }
            public List<object> icons { get; set; }
            public string advance { get; set; }
            public string displayName { get; set; }
        }
        public class Room
        {
            public string description { get; set; }
            public decimal rate_per_night { get; set; }
            public PriceDetails priceDetails { get; set; }
            public int categoryId { get; set; }
            public string categoryName { get; set; }
            public decimal sellingAmount { get; set; }
            public decimal commission { get; set; }
            public OccupancyWisePerNightPrice occupancyWisePerNightPrice { get; set; }
        }
      
        public class PriceDetails
        {
            public decimal roomPrice { get; set; }
            public List<TaxBreakup> taxBreakup { get; set; }
            public decimal commission { get; set; }
        }

        public class TaxBreakup
        {
            public string taxType { get; set; }
            public decimal taxPrice { get; set; }
        }
        public class Rooms
        {
            public List<Room> room { get; set; }
        }

        public class Hotel
        {
            public List<Restriction> restrictions { get; set; }
            public string id { get; set; }
            public string ID { get; set; }
            public string currency_code { get; set; }
            public string CurrencyCode { get; set; }
            public string mealplan { get; set; }
           // public Rooms rooms { get; set; }
            public string cancellation_policy { get; set; }
            public decimal RatePerNight { get; set; }
            public int CityTax { get; set; }
            public int Vat { get; set; }
            public string RoomDescription { get; set; }
            public string Currency { get; set; }
            public string BreakfastIncluded { get; set; }
            public string MealCodes { get; set; }
            public int RoomsLeft { get; set; }
            public string FreeCancellation { get; set; }
            public string PaymentType { get; set; }
            public string CreditCard { get; set; }
            public string DeepLinkUrl { get; set; }

            public string currencyCode { get; set; }
            public string mealPlan { get; set; }
            public List<Room> rooms { get; set; }
            public string cancellationPolicy { get; set; }
        }

        public class Hotels
        {
            public List<Hotel> Hotel { get; set; }
        }

        public class RootObject
        {
            //public Hotels Hotels { get; set; }
            public Error error { get; set; }

            public string id { get; set; }
            public string checkin { get; set; }
            public string checkout { get; set; }
            public string invoice { get; set; }         
            public int roomCount { get; set; }
            public string status { get; set; }
            public string country_name { get; set; }
            public string currency_symbol { get; set; }
            public decimal cancellationCharge { get; set; }

            public List<object> cancellation_charges_breakup_hash { get; set; }
            public List<Hotel> hotels { get; set; }
           
            //public int checkin { get; set; }
            //public int checkout { get; set; }           
            public int guestCount { get; set; }
            public string hotelId { get; set; }
            public string currencyCode { get; set; }
            public string externalReferenceId { get; set; }
            public string entityId { get; set; }
            public decimal finalAmount { get; set; }
           // public decimal commission { get; set; }
            public decimal refundAmount { get; set; }
            public string billingId { get; set; }
            public GuestDetails guestDetails { get; set; }
            public PriceDetails priceDetails { get; set; }
        }
        public class Amenities
        {
            public bool AmazonPrime { get; set; }
            public bool SwimmingPool { get; set; }
            public bool PrivateEntrance { get; set; }
            public bool Reception { get; set; }
            public bool Security { get; set; }
            public bool AC { get; set; }
            public bool PreBookMeals { get; set; }
            public bool _247Checkin { get; set; }
            public bool DishWasher { get; set; }
            public bool CutleryCrockery { get; set; }
            public bool Stove { get; set; }
            public bool Toaster { get; set; }
            public bool ElectricKettle { get; set; }
            public bool BoardGames { get; set; }
            public bool FireExtinguisher { get; set; }
            public bool SharedKitchen { get; set; }
            public bool StepFreeAccess { get; set; }
            public bool DailyHousekeeping { get; set; }
            public bool AttachedBathroom { get; set; }
            public bool PrivateLivingRoom { get; set; }
            public bool BuzzerDoorBell { get; set; }
            public bool FirstAid { get; set; }
            public bool FullPowerbackup { get; set; }
            public bool PartialPowerbackup { get; set; }
            public bool SmartLock { get; set; }
            public bool Pantry { get; set; }
            public bool DiningArea { get; set; }
            public bool HDTV { get; set; }
            public bool ElectricityChargesIncluded { get; set; }
            public bool ComplimentaryVegBreakfast { get; set; }
            public bool Geyser { get; set; }
            public bool RoomHeater { get; set; }
            public bool MiniFridge { get; set; }
            public bool TV { get; set; }
            public bool HairDryer { get; set; }
            public bool BathTub { get; set; }
            public bool ComplimentaryBreakfast { get; set; }
            public bool InhouseRestaurant { get; set; }
            public bool ParkingFacility { get; set; }
            public bool Garden { get; set; }
            public bool Elevator { get; set; }
            public bool FacilityManager { get; set; }
            public bool WiFi { get; set; }
            public bool HotWater { get; set; }
            public bool Induction { get; set; }
            public bool CardPayment { get; set; }
            public bool FreeWifi { get; set; }
            public bool Powerbackup { get; set; }
            public bool ConferenceRoom { get; set; }
            public bool BanquetHall { get; set; }
            public bool Gym { get; set; }
            public bool WheelchairAccessible { get; set; }
            public bool Laundry { get; set; }
            public bool Bar { get; set; }
            public bool Kindle { get; set; }
            public bool Spa { get; set; }
            public bool WellnessCentre { get; set; }
            public bool Netflix { get; set; }
            public bool PetFriendly { get; set; }
            public bool CCTVCameras { get; set; }
            public bool Refrigerator { get; set; }
            public bool LivingRoom { get; set; }
            public bool Microwave { get; set; }
            public bool Iron { get; set; }
            public bool RO { get; set; }
            public bool Kitchen { get; set; }
            public bool GardenBackyard { get; set; }
            public bool WifiDongle { get; set; }
            public bool WashingMachine { get; set; }
            public bool LaptopFriendly { get; set; }
            public bool Oven { get; set; }
            public bool CoffeeTeaMaker { get; set; }
            public bool DTH { get; set; }
            public bool Bathroom { get; set; }
            public bool Washroom { get; set; }
            public bool Backyard { get; set; }
            public bool FireExit { get; set; }
            public bool Hall { get; set; }
            public bool HouseKeeping { get; set; }
          
        }
        public class Error
        {
            public int code { get; set; }
            public string type { get; set; }
            public string message { get; set; }
        }

        public class OccupancyWisePerNightPrice
        {
            public decimal single { get; set; }
            public decimal doble { get; set; }
            public decimal triple { get; set; }
        }

        public class GuestDetails
        {
            public string firstName { get; set; }
            public string lastName { get; set; }
            public string email { get; set; }
            public string countryCode { get; set; }
            public string phone { get; set; }
        }
    }
}
