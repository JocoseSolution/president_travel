﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using HotelShared;
using HotelDAL;
namespace HotelBAL.OYO_V2
{
  public  class OYOAvaibilityCalling
    {
       #region Constructor
        public OYOAvaibilityCalling(AllCombindHotel objSearchcombindHotel)
        {
            objALLcombindHotel = objSearchcombindHotel;
        }
        #endregion
        ArrayList objHotelResultList = new ArrayList();
        AllCombindHotel objALLcombindHotel;

        private async Task CallOYOHotelAvaibility(List<HotelCredencials> CredencialsList, AllCombindHotel objALLcombindHotel, List<HotelInformation> HotelInfo, int startIndex)
        {
            try
            {
                if (HotelInfo.Count >= startIndex)
                {
                    OYOHotelSearchResponse objoyo = new OYOHotelSearchResponse();
                    await Task.Delay(0);
                    
                    //var t1 = Task.Factory.StartNew(() => objoyo.OYOResult(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, startIndex + 4000));

                    var t1 = Task.Run(() =>  objoyo.OYOResult(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, startIndex + 4000));

                    //var t2 =  Task.Run(() => Task.Delay(300) (objoyo.OYOResult(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, startIndex + 4000)));
                    Task.WaitAll(t1);


                    var tt = Task.WhenAll(t1).Result;
                    objHotelResultList.Add(tt[0]);
                    
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "CallOYOHotelAvaibility");
            }
        }

        public async  Task<ArrayList> objcALLINGHotelinThread()
        {
            HotelDA objhtlDa = new HotelDA();
           
            try
            {
                ConcurrentBag<Task> OyotaskList = new ConcurrentBag<Task>();
                List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
                CredencialsListnew = objALLcombindHotel.AllHotelSearch.CredencialsList.Where(x => x.HotelProvider == "OYO" && (x.HotelTrip == objALLcombindHotel.AllHotelSearch.HtlType || x.HotelTrip == "ALL")).ToList();
                List<HotelInformation> HotelInfo = new List<HotelInformation>();
                HotelInfo = objhtlDa.OYOHotelInfoList(objALLcombindHotel.AllHotelSearch.SearchCity.Replace("NEW","").Trim(), "OYO", HotelInfo);
                int thrCnt = 20, loop = 0;
              
                int totalthread = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(HotelInfo.Count) / 50));
                if (CredencialsListnew.Count > 0 && HotelInfo.Count > 0)
                {
                    for (int i = 0; i < HotelInfo.Count;i = i + 50)
                    {
                        loop++;
                        OyotaskList.Add(Task.Run(() => CallOYOHotelAvaibility(CredencialsListnew, objALLcombindHotel, HotelInfo, i)));
                        Thread.Sleep(100);
                        if (loop == thrCnt || loop == totalthread)
                        {
                            Task.WhenAll(OyotaskList).Wait();
                            OyotaskList = new ConcurrentBag<Task>();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "objcALLINGHotelinThread");
            }
            return objHotelResultList ;
        }

        private async Task OyoMultipleAsyncMethods(List<HotelCredencials> CredencialsList, AllCombindHotel objALLcombindHotel)
        {
            await Task.Delay(0);
            var t1 = (dynamic)null; var t2 = (dynamic)null; var t3 = (dynamic)null; var t4 = (dynamic)null; var t5 = (dynamic)null; var t6 = (dynamic)null; var t7 = (dynamic)null;
            var t8 = (dynamic)null; var t9 = (dynamic)null; var t10 = (dynamic)null; var t11 = (dynamic)null; var t12 = (dynamic)null; var t13 = (dynamic)null; var t14 = (dynamic)null;
            var t15 = (dynamic)null; var t16 = (dynamic)null; var t17 = (dynamic)null; var t18 = (dynamic)null; var t19 = (dynamic)null; var t20 = (dynamic)null; var t0 = (dynamic)null;
            try
            {
                HotelDA objhtlDa = new HotelDA();
                ConcurrentBag<Task> OyotaskList = new ConcurrentBag<Task>();
                List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
                CredencialsListnew = objALLcombindHotel.AllHotelSearch.CredencialsList.Where(x => x.HotelProvider == "OYO" && (x.HotelTrip == objALLcombindHotel.AllHotelSearch.HtlType || x.HotelTrip == "ALL")).ToList();
                
                List<HotelInformation> HotelInfo = new List<HotelInformation>();
                HotelInfo = objhtlDa.OYOHotelInfoList(objALLcombindHotel.AllHotelSearch.SearchCity.Replace("NEW", "").Trim(), "OYO", HotelInfo);

                int thrCnt = 20, loop = 0, totalthread = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(HotelInfo.Count) / 50));
                OYOHotelSearchResponse objoyo = new OYOHotelSearchResponse();
                if (CredencialsListnew.Count > 0 && HotelInfo.Count > 0)
                {
                    for (int i = 0; i < HotelInfo.Count; i = i + 50)
                    {
                        loop++;
                        t1 = Task.Factory.StartNew(() => objoyo.OYOResult(CredencialsList, objALLcombindHotel, HotelInfo, i, 4000 + i));

                        OyotaskList.Add(Task.Run(() => CallOYOHotelAvaibility(CredencialsListnew, objALLcombindHotel, HotelInfo, i)));
                        Thread.Sleep(200);
                        if (loop == thrCnt || loop == totalthread)
                        {
                            await Task.WhenAll(OyotaskList);
                            OyotaskList = new ConcurrentBag<Task>();
                        }  
                    }
                    

                    CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
                    CancellationToken cancellationToken = cancellationTokenSource.Token;

                  
                   // Task<List<HotelResult>> t1 = Task.Run(() => objoyo.OYOResult(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, populerid));
                   // Task<List<HotelResult>> t2 = Task.Run(() => objoyo.OYOResult(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, populerid));
                   // Task<List<HotelResult>> t1 = GreetingAsync(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, populerid);
                    //Task<List<HotelResult>> t2 =  GreetingAsync(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, populerid);
                    //await Task.WhenAll(t1, t2);
                    //objHotelResultList.Add(Task.WhenAll(t1).Result[0]);
                    //objHotelResultList.Add(Task.WhenAll(t2).Result[0]);
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "CallOYOHotelAvaibility");
            }
        }

        //static Task<List<HotelResult>> GreetingAsync(List<HotelCredencials> CredencialsList, AllCombindHotel objALLcombindHotel, List<HotelInformation> HotelInfo, int startIndex, int populerid)
        //{
        //    OYOHotelSearchResponse objoyo = new OYOHotelSearchResponse();
        //   // int populerid = startIndex + 4000;
        //    return Task.Run<List<HotelResult>>(() =>
        //    {
        //        return objoyo.OYOResult(CredencialsList, objALLcombindHotel, HotelInfo, startIndex, populerid);
        //    });
        //}
    }
}
