﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Summary description for ClsHotelFacilityMappingMaster
/// </summary>
public class ClsHotelFacilityMappingMaster
{
	public ClsHotelFacilityMappingMaster()
	{
		//
		// TODO: Add constructor logic here
		//
    }

    #region Property Define For Mst HotelFacilityMappingMaster
    public int HotelFacilityMapId
    { get; set; }
    public string HotelCode
    { get; set; }
    public int FacilityId
    { get; set; }
    public int CityId
    { get; set; }
    public int CountryId
    { get; set; }
    public int RoomCategoryId
    { get; set; }

    public string CreatedBy
    { get; set; }

    public string ModifyBy
    { get; set; }

    public string Status
    { get; set; }
    public string Chargeble
    { get; set; }
    
    public string Remarks
    { get; set; }

    public string MessageOut
    { get; set; }

    public string SearchCriteria
    { get; set; }

    public string ColumnName
    { get; set; }

    public int ReturnValue
    { get; set; }

    public string Action_Type
    { get; set; }

    public string SortColumnName
    { get; set; }

    public string SortOrderBy
    { get; set; }

    public int RowCount
    { get; set; }
    #endregion

    #region Define Constant for SP Name and Sp Type

    //All the details Used the SP in Currency Master Pages 

    public const string SP_uspHotelFacilityMapping = "[ExtranetHotel].[usp_HotelFacilityMapping]";
    public const string Type_SEARCH_HOTELFACILITYMAPPING = "SEARCH";
    public const string Type_ON_ROW_SELECT = "ON_SELECT";
    public const string Type_INSERT_HOTELFACILITYMAPPING = "INSERT";
    public const string Type_UPDATE_HOTELFACILITYMAPPING = "UPDATE";
    public const string Type_SELECT_COUNTRY = "BIND_DRPDOWN_COUNTRY";
    public const string Type_SELECT_CITY = "BIND_DRPDOWN_CITY";
    public const string Type_SELECT_HOTEL = "BIND_DRPDOWN_HOTEL";
    public const string Type_SELECT_FACILITY = "BIND_CHKL_FACILITY";
    public const string Type_SELECT_FACILITYHOTEL = "BIND_CHKL_FACILITY_HOTEL";
    public const string Type_SELECT_FACILITY_HOTELS = "SELECT_FACILITY_HOTEL";
    public const string Type_SELECT_HOTELROOMCATEGORY = "BIND_DRPDOWN_HOTELROOMCATEGORY";
    public const string Type_Save_Remarks = "Save_Remarks";
    


    #endregion
}
