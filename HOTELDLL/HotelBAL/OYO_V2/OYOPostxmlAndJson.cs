﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Net.Http;
//using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Diagnostics;
using System.Security.Cryptography;
using Newtonsoft.Json;
using System.Web;
namespace HotelBAL.OYO_V2
{
  public  class OYOPostJson
    {
      public string HotelPostJSON(string url, string json, string AccessToken)
      {
          StringBuilder sbResult = new StringBuilder();
          try
          {
              if (!string.IsNullOrEmpty(json))
              {
                  HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                  byte[] requestBytes = Encoding.ASCII.GetBytes(json);
                  ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                  request.ContentLength = requestBytes.Length;
                  request.Method = "POST";
                  request.Timeout = 20000;
                  request.ContentType = "application/json";
                  request.Headers["x-access-token"] = AccessToken;
                  request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                  Stream requestStream = request.GetRequestStream();
                  requestStream.Write(requestBytes, 0, requestBytes.Length);
                  requestStream.Close();

                  using (HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse())
                  {
                      if (WebResponse.StatusCode != HttpStatusCode.OK)
                      {
                          string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                          throw new ApplicationException(message);
                      }
                      else
                      {
                          Stream responseStream = WebResponse.GetResponseStream();
                          if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                          {
                              responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                          }
                          else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                          {
                              responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                          }
                          StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                          sbResult.Append(reader.ReadToEnd());
                          responseStream.Close();
                      }
                  }
              }

          }
          catch (WebException WebEx)
          {
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(WebEx, "HotelPostJSON");
              WebResponse response = WebEx.Response;
              if (response != null)
              {
                  Stream stream = response.GetResponseStream();
                  string responseMessage = new StreamReader(stream).ReadToEnd();
                  sbResult.Append(responseMessage);
                  if (!responseMessage.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, responseMessage, "OYO", "HotelInsert");
              }
              else
              {
                  int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, WebEx.Message, "OYO", "HotelInsert");
                  sbResult.Append(WebEx.Message);
                  if (!WebEx.Message.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
              }
          }
          catch (Exception ex)
          {
              sbResult.Append(ex.Message + "<error>" + ex.Message + "</error>");
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelPostJSON");
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, ex.Message, "OYO", "HotelInsert");
          }

          return sbResult.ToString();
      }

      public string HotelPostJSONBooking(string url, string json, string AccessToken)
      {
          StringBuilder sbResult = new StringBuilder();
          try
          {
              if (!string.IsNullOrEmpty(json))
              {
                  HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                  byte[] requestBytes = Encoding.ASCII.GetBytes(json);
                  ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                  request.ContentLength = requestBytes.Length;
                  request.Method = "POST";
                  request.ContentType = "application/json";
                  request.Headers["x-access-token"] = AccessToken;
                  request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                  Stream requestStream = request.GetRequestStream();
                  requestStream.Write(requestBytes, 0, requestBytes.Length);
                  requestStream.Close();

                  using (HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse())
                  {
                      if (WebResponse.StatusCode != HttpStatusCode.OK)
                      {
                          string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                          throw new ApplicationException(message);
                      }
                      else
                      {
                          Stream responseStream = WebResponse.GetResponseStream();
                          if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                          {
                              responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                          }
                          else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                          {
                              responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                          }
                          StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                          sbResult.Append(reader.ReadToEnd());
                          responseStream.Close();
                      }
                  }
              }

          }
          catch (WebException WebEx)
          {
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(WebEx, "HotelPostJSON");
              WebResponse response = WebEx.Response;
              if (response != null)
              {
                  Stream stream = response.GetResponseStream();
                  string responseMessage = new StreamReader(stream).ReadToEnd();
                  sbResult.Append(responseMessage);
                  if (!responseMessage.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, responseMessage, "OYO", "HotelInsert");
              }
              else
              {
                  int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, WebEx.Message, "OYO", "HotelInsert");
                  sbResult.Append(WebEx.Message);
                  if (!WebEx.Message.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
              }
          }
          catch (Exception ex)
          {
              sbResult.Append(ex.Message + "<error>" + ex.Message + "</error>");
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelPostJSON");
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, ex.Message, "OYO", "HotelInsert");
          }

          return sbResult.ToString();
      }
      public string HotelGetJSON(string url, string AccessToken)
      {
          StringBuilder sbResult = new StringBuilder();
          try
          {
                  HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);                
                  ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;              
                  request.Method = "GET";
                  request.Timeout = 20000;
                  request.ContentType = "application/json";
                  request.Headers["x-access-token"] = AccessToken;
                  request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");                 
                  using (HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse())
                  {
                      if (WebResponse.StatusCode != HttpStatusCode.OK)
                      {
                          string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                          throw new ApplicationException(message);
                      }
                      else
                      {
                          Stream responseStream = WebResponse.GetResponseStream();
                          if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                          {
                              responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                          }
                          else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                          {
                              responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                          }
                          StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                          sbResult.Append(reader.ReadToEnd());
                          responseStream.Close();
                      }
                  }
          }
          catch (WebException WebEx)
          {
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(WebEx, "HotelPostJSON");
              WebResponse response = WebEx.Response;
              if (response != null)
              {
                  Stream stream = response.GetResponseStream();
                  string responseMessage = new StreamReader(stream).ReadToEnd();
                  sbResult.Append(responseMessage);
                  if (!responseMessage.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", url, responseMessage, "OYO", "HotelInsert");
              }
              else
              {
                  int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", url, WebEx.Message, "OYO", "HotelInsert");
                  sbResult.Append(WebEx.Message);
                  if (!WebEx.Message.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
              }
          }
          catch (Exception ex)
          {
              sbResult.Append(ex.Message + "<error>" + ex.Message + "</error>");
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelPostJSON");
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", url, ex.Message, "OYO", "HotelInsert");
          }

          return sbResult.ToString();
      }
      public string HotelPutJSONBooking(string url, string json, string AccessToken)
      {
          StringBuilder sbResult = new StringBuilder();
          try
          {
              if (!string.IsNullOrEmpty(json))
              {
                  HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                  byte[] requestBytes = Encoding.ASCII.GetBytes(json);
                  ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                  request.ContentLength = requestBytes.Length;
                  request.Method = "PUT";
                 // request.ContentType = "application/json";
                  request.Headers["x-access-token"] = AccessToken;
                  //request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                  Stream requestStream = request.GetRequestStream();
                  requestStream.Write(requestBytes, 0, requestBytes.Length);
                  requestStream.Close();

                  using (HttpWebResponse WebResponse = (HttpWebResponse)request.GetResponse())
                  {
                      if (WebResponse.StatusCode != HttpStatusCode.OK)
                      {
                          string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                          throw new ApplicationException(message);
                      }
                      else
                      {
                          Stream responseStream = WebResponse.GetResponseStream();
                          if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                          {
                              responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                          }
                          else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                          {
                              responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                          }
                          StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                          sbResult.Append(reader.ReadToEnd());
                          responseStream.Close();
                      }
                  }
              }

          }
          catch (WebException WebEx)
          {
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(WebEx, "HotelPostJSON");
              WebResponse response = WebEx.Response;
              if (response != null)
              {
                  Stream stream = response.GetResponseStream();
                  string responseMessage = new StreamReader(stream).ReadToEnd();
                  sbResult.Append(responseMessage);
                  if (!responseMessage.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
                  int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, responseMessage, "OYO", "HotelInsert");
              }
              else
              {
                  int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, WebEx.Message, "OYO", "HotelInsert");
                  sbResult.Append(WebEx.Message);
                  if (!WebEx.Message.Contains("error"))
                      sbResult.Append("<error>" + WebEx.Message + "</error>");
              }
          }
          catch (Exception ex)
          {
              sbResult.Append(ex.Message + "<error>" + ex.Message + "</error>");
              HotelDAL.HotelDA objhtlDa = new HotelDAL.HotelDA();
              HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelPostJSON");
              int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", json, ex.Message, "OYO", "HotelInsert");
          }

          return sbResult.ToString();
      }
      public decimal SetEpochTimes(string dates)
      {
          DateTime chekout = Convert.ToDateTime(dates + "T13:00:00");
          TimeSpan tsTimeSpan = chekout.Subtract(DateTime.Now.Date.AddHours(13));
         // return Math.Round(Convert.ToDecimal((DateTime.UtcNow.AddDays(tsTimeSpan.Days) - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds));
          return Math.Ceiling(Convert.ToDecimal((DateTime.Now.AddDays(tsTimeSpan.Days).AddHours(-5.5) - new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds));
      }
    }
}
