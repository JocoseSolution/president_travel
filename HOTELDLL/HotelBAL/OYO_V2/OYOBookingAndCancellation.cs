﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HotelShared;
using HotelDAL;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Globalization;
namespace HotelBAL.OYO_V2
{
  public  class OYOBookingAndCancellation
    {
        public HotelBooking OYOoPreBooking(HotelBooking HotelDetail)
        {
            HotelDetail.ProBookingID = "false";
            try
            {
                if (HotelDetail.HotelUrl != null)
                {
                    OYOPostJson objoyo = new OYOPostJson();
                    HotelDetail.ProBookingReq = PrebookingRequest(HotelDetail);
                    HotelDetail.ProBookingRes = objoyo.HotelPostJSONBooking(HotelDetail.HotelBookingUrl.Trim(), HotelDetail.ProBookingReq, HotelDetail.HotelUsername.Trim());

                    if (!HotelDetail.ProBookingRes.Contains("error"))
                    {
                        HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject Bookresult = JsonConvert.DeserializeObject<HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject>(HotelDetail.ProBookingRes);
                        if (Bookresult!= null)
                        {                                  
                                if (Bookresult.finalAmount <= HotelDetail.Total_Org_Roomrate)
                                {
                                    HotelDetail.ProBookingID = Bookresult.id.ToString();
                                    HotelDetail.ReferenceNo = Bookresult.invoice;
                                }
                                else
                                {
                                    HotelDetail.BookingID = "Rate has been changed."; HotelDetail.ProBookingID = "false"; HotelDetail.Status = HotelStatus.Rejected.ToString();   
                                }
                         }
                        else
                        {
                            HotelDetail.HotelContactNo = "Exception"; HotelDetail.BookingID = ""; HotelDetail.HotelInformation = ""; HotelDetail.Status = HotelStatus.Rejected.ToString();
                        }
                    }
                    else
                    {
                        HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false"; HotelDetail.Status = HotelStatus.Rejected.ToString();
                    }
                }
                else
                {
                    HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false"; HotelDetail.Status = HotelStatus.Rejected.ToString();
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "OYOoPreBooking");
                HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false"; HotelDetail.Status = HotelStatus.Rejected.ToString();
            }
            return HotelDetail;
        }
        public HotelBooking OYOHotelsBooking(HotelBooking HtlBooking)
        {
            try
            {
                OYOPostJson objoyo = new OYOPostJson();
                //HtlBooking.BookingConfReq = "{ \"booking\": { \"status\": \"Confirm Booking\", \"billingId\": \"" + HtlBooking.ProBookingID + "\", \"externalReferenceId\": \"" + HtlBooking.Orderid + "\"},";
                HtlBooking.BookingConfReq = "{ \"booking\": { \"status\": \"Confirm Booking\", \"externalReferenceId\": \"" + HtlBooking.Orderid + "\"},";
                HtlBooking.BookingConfReq += " \"payments\": { \"billToAffiliate\": \"true\"} }";
                HtlBooking.BookingConfRes = objoyo.HotelPutJSONBooking(HtlBooking.HotelBookingUrl.Trim() + "/" + HtlBooking.ProBookingID, HtlBooking.BookingConfReq, HtlBooking.HotelUsername.Trim());

                if (!HtlBooking.BookingConfRes.Contains("error"))
                {
                    HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject Bookresult = JsonConvert.DeserializeObject<HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject>(HtlBooking.BookingConfRes);
                    if (Bookresult != null)
                    {
                        if (Bookresult.status == "Confirm Booking")
                        {
                            HtlBooking.Status = HotelStatus.Confirm.ToString();
                            HtlBooking.BookingID = Bookresult.id;
                            HtlBooking.ReferenceNo = Bookresult.invoice;
                            HtlBooking.ProBookingID = Bookresult.invoice;
                        }
                    }
                    else
                    {
                        HtlBooking.HotelContactNo = "Exception"; HtlBooking.BookingID = ""; HtlBooking.HotelInformation = ""; HtlBooking.Status = HotelStatus.Rejected.ToString();
                    }
                }
                else
                {
                    HtlBooking.BookingConfRes = objoyo.HotelGetJSON(HtlBooking.HotelPassword.Trim() + "?id=" + HtlBooking.ProBookingID, HtlBooking.HotelUsername.Trim());
                    if (!HtlBooking.BookingConfRes.Contains("error"))
                    {
                        HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject Bookresult = JsonConvert.DeserializeObject<HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject>(HtlBooking.BookingConfRes);
                        if (Bookresult != null)
                        {
                            if (Bookresult.status == "Confirm Booking")
                            {
                                HtlBooking.Status = HotelStatus.Confirm.ToString();
                                HtlBooking.BookingID = Bookresult.id;
                                HtlBooking.ReferenceNo = Bookresult.invoice;
                                HtlBooking.ProBookingID = Bookresult.invoice;
                            }
                        }
                        else
                        {
                            HtlBooking.HotelContactNo = "Exception"; HtlBooking.BookingID = ""; HtlBooking.HotelInformation = ""; HtlBooking.Status = HotelStatus.Rejected.ToString();
                        }
                    }
                    else
                    {
                        HtlBooking.HotelContactNo = "Exception"; HtlBooking.BookingID = ""; HtlBooking.HotelInformation = ""; HtlBooking.Status = HotelStatus.Rejected.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "OYOHotelsBooking");
                HtlBooking.HotelContactNo = ex.Message;
                HtlBooking.BookingID = "";
                HtlBooking.HotelInformation = "";
                HtlBooking.Status = HotelStatus.Rejected.ToString();
            }

            return HtlBooking;
        }
        public HotelCancellation OYOHotelsCancelation(HotelCancellation HotelDetail)
        {
            HotelDetail.CancellationCharge = 0; HotelDetail.CancellationID = "";
            StringBuilder sb = new StringBuilder();
            try
            {
                HotelDetail.BookingCancelReq = " {\"booking\": { \"status\": \"Cancelled Booking\" }}";
                OYOPostJson objoyo = new OYOPostJson();
                HotelDetail.BookingCancelRes = objoyo.HotelPutJSONBooking(HotelDetail.HotelBookingUrl.Trim() + "/" + HotelDetail.BookingID.Trim(), HotelDetail.BookingCancelReq, HotelDetail.Can_UserID.Trim());

                if (!HotelDetail.BookingCancelRes.Contains("error"))
                {
                    HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject Bookresult = JsonConvert.DeserializeObject<HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject>(HotelDetail.BookingCancelRes);
                    if (Bookresult != null)
                    {
                        if (Bookresult.status == "Cancelled Booking")
                        {
                            HotelDetail.CancelStatus = HotelStatus.Cancelled.ToString();
                            HotelDetail.CancellationID = Bookresult.id;
                            HotelDetail.CancellationCharge = Bookresult.cancellationCharge;
                            HotelDetail.SupplierRefundAmt = Bookresult.refundAmount;
                            //if (HotelDetail.CancellationCharge == 0)
                            //    HotelDetail.SupplierRefundAmt = HotelDetail.Total_Org_Roomrate;
                            //else
                            //    HotelDetail.SupplierRefundAmt = HotelDetail.Total_Org_Roomrate - HotelDetail.CancellationCharge;

                            //if (Bookresult.finalAmount == HotelDetail.Total_Org_Roomrate && Bookresult.cancellationCharge == 0)
                            //    HotelDetail.SupplierRefundAmt = HotelDetail.Total_Org_Roomrate;
                            //else
                            //{
                            //    HotelDetail.CancellationCharge = HotelDetail.Total_Org_Roomrate - Bookresult.finalAmount;
                            //    HotelDetail.SupplierRefundAmt = Bookresult.finalAmount;
                            //}
                        }
                        else
                        { HotelDetail.CancellationCharge = 0; HotelDetail.SupplierRefundAmt = 0; }
                    }
                    else
                    { HotelDetail.CancellationCharge = 0; HotelDetail.SupplierRefundAmt = 0; }
                }
                else
                {
                    string cancelresponse = objoyo.HotelGetJSON(HotelDetail.Can_Password.Trim() + "?id=" + HotelDetail.BookingID, HotelDetail.Can_UserID.Trim());
                    if (!cancelresponse.Contains("error"))
                    {
                        HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject Bookresult = JsonConvert.DeserializeObject<HotelBAL.OYO_V2.OYOAvailabilityModel.RootObject>(cancelresponse);
                        if (Bookresult != null)
                        {
                            if (Bookresult.status == "Cancelled Booking")
                            {
                                HotelDetail.CancelStatus = HotelStatus.Cancelled.ToString();
                                HotelDetail.CancellationID = Bookresult.id;
                            }
                        }
                        else
                        { HotelDetail.CancellationCharge = 0; HotelDetail.SupplierRefundAmt = 0; }
                    }
                    else
                    { HotelDetail.CancellationCharge = 0; HotelDetail.SupplierRefundAmt = 0; }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "OYOHotelsCancelation");
            }
            return HotelDetail;
        }

        protected string PrebookingRequest(HotelBooking HtlBooking)
        {
            StringBuilder sb = new StringBuilder();
            OYOPostJson objoyos = new OYOPostJson();
            try
            {
                sb.Append("{\"guest\": {");
                sb.Append(" \"firstName\": \"" + HtlBooking.PGFirstName + "\",");
                sb.Append(" \"lastName\": \"" + HtlBooking.PGLastName + "\",");
                sb.Append(" \"email\": \"" + HtlBooking.PGEmail + "\",");
                sb.Append(" \"countryCode\": \"+91\",");
                sb.Append(" \"phone\": \"" + HtlBooking.PGContact + "\"},");
                
                sb.Append("\"booking\": {");
                int singleguest = 0, doubleguest=0, extrabed=0;
                for (int i = 0; i < HtlBooking.NoofRoom; i++)
                {
                    int paxcount = Convert.ToInt32(HtlBooking.AdtPerRoom[i]) + Convert.ToInt32(HtlBooking.ChdPerRoom[i]);
                    if (paxcount == 1)
                        singleguest++;
                    else if (paxcount > 1)
                        doubleguest++;
                    else if (paxcount == 3)
                        extrabed++;
                    else if (paxcount > 3)
                        extrabed += paxcount - 2;
                }
                sb.Append(" \"single\":" + singleguest.ToString() + ",");
                sb.Append(" \"double\":" + doubleguest.ToString() + ",");
                sb.Append(" \"extra\":" + extrabed.ToString() + ",");

                sb.Append(" \"checkin\": " + objoyos.SetEpochTimes(HtlBooking.CheckInDate).ToString() + ", \"checkout\": " + objoyos.SetEpochTimes(HtlBooking.CheckOutDate).ToString() + ",");
                sb.Append(" \"hotelId\": \""+ HtlBooking.HtlCode +"\",");
                sb.Append(" \"roomCategoryId\": " + HtlBooking.RoomPlanCode + ",");
                sb.Append(" \"sellingAmount\": " + HtlBooking.Total_Org_Roomrate + ",");
                sb.Append(" \"isProvisional\": true,");
                sb.Append(" \"externalReferenceId\": \"" + HtlBooking.Orderid + "\"} }");    
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "PrebookingRequest");
            }
            return sb.ToString();
        }
    }
}
