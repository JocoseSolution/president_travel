﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using System.Xml;
using HotelShared;
using HotelDAL;
using System.Data;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Configuration;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web;
using System.Web.Services;




namespace HotelBAL
{
    public class TBOHotelResponse
    {

        TBOHotelRequest tboreq = new TBOHotelRequest();
        public AllCombindHotel TBOHotel(AllCombindHotel objALLcombindHotel)
        {

           // HotelComposite obgHotelsCombo = new HotelComposite();
            try
            {
                List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
                CredencialsListnew = objALLcombindHotel.AllHotelSearch.CredencialsList.Where(x => x.HotelProvider == "TBO" && (x.HotelTrip == objALLcombindHotel.AllHotelSearch.HtlType || x.HotelTrip == "ALL")).ToList();

                if (CredencialsListnew.Count > 0)
                {
                    DateTime datetime = DateTime.UtcNow.Date;



                    if (HotelSearch.TokenId == null || HotelSearch.datetime != datetime)
                    {
                        objALLcombindHotel = tboreq.HotelSearchAvailabilityRequest(objALLcombindHotel, CredencialsListnew);
                        objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes = TBOPostXml(CredencialsListnew[0].HotelUrl, objALLcombindHotel.AllHotelSearch.TBO_HotelSearchReq);
                        string responseXML = objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes;
                        if (responseXML.Contains("<TokenId>"))
                        {
                            XmlDocument doc1 = new XmlDocument();
                            doc1.LoadXml(responseXML);
                            string jsonText = JsonConvert.SerializeXmlNode(doc1);
                            var obj = JObject.Parse(jsonText);
                            string tokenid = (string)obj["root"]["TokenId"].ToString();
                            HotelSearch.TokenId = tokenid;

                            HotelSearch.datetime = DateTime.UtcNow.Date;


                        }
                    }


                    objALLcombindHotel.AllHotelSearch = tboreq.BookingRequesttbo(objALLcombindHotel, CredencialsListnew);
                    SaveFile(objALLcombindHotel.AllHotelSearch.TBO_HotelSearchReq, "htlsearchreq");


                    objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes = TBOPostXml(CredencialsListnew[0].HotelUrl, objALLcombindHotel.AllHotelSearch.TBO_HotelSearchReq);
                    SaveFile(objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes, "htlsearchresp");
                    //xmltojson(SearchDetails.TBO_HotelSearchRes, "htlsearchresp");


                    objALLcombindHotel.TBOHotelLst = GetTBOHotelsPrice(objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes, objALLcombindHotel.AllHotelSearch);


                    //XDocument document1;
                    //string XMLFile = "D:\\HotelPriject\\B2BHotel_25_10_2014\\Home\\TGHotelSearch.xml";
                    //document1 = XDocument.Load(XMLFile);
                    //SearchDetails.RoomXML_HotelSearchRes = document1.ToString();
                    //obgHotelsCombo.Hotelresults = GetRoomXMLHotelsPrice(SearchDetails.RoomXML_HotelSearchRes, SearchDetails); 

                }
                else
                {
                    objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes = "Hotel Not available for " + objALLcombindHotel.AllHotelSearch.SearchCity + ", " + objALLcombindHotel.AllHotelSearch.Country;
                    objALLcombindHotel.AllHotelSearch.TBO_HotelSearchReq = "not allow";
                    List<HotelResult> objHotellist = new List<HotelResult>();
                    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes });
                    objALLcombindHotel.TBOHotelLst  = objHotellist;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TBOHotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", objALLcombindHotel.AllHotelSearch.TBO_HotelSearchReq, objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes, objALLcombindHotel.AllHotelSearch.AgentID, "HotelInsert");
                objALLcombindHotel.AllHotelSearch.TBO_HotelSearchRes = ex.Message;
            }
            objALLcombindHotel.AllHotelSearch = objALLcombindHotel.AllHotelSearch;
            return objALLcombindHotel;
        }



        //public static string TBOPostXml(string url, string requestData)
        //{
        //    //HotelSearch searchtoken = new HotelSearch();
        //    string responseXML = string.Empty;

        //    //XmlDocument doc = new XmlDocument();
        //    //doc.LoadXml(requestData);
        //    //string json = JsonConvert.SerializeXmlNode(doc);
        //    try
        //    {
        //        byte[] data = Encoding.UTF8.GetBytes(requestData);
        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";
        //        request.Headers.Add("Accept-Encoding", "gzip");
        //        Stream dataStream = request.GetRequestStream();
        //        dataStream.Write(data, 0, data.Length);
        //        dataStream.Close();
        //        WebResponse webResponse = request.GetResponse();
        //        var rsp = webResponse.GetResponseStream();
        //        if (rsp == null)
        //        {
        //            //throw exception
        //        }
        //        using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
        //        {
        //            responseXML = JsonConvert.DeserializeXmlNode(readStream.ReadToEnd(), "root").InnerXml;
        //            //XmlDocument doc = (XmlDocument)JsonConvert.DeserializeXmlNode(json);
        //            //var result = JsonConvert.DeserializeXmlNode(responseXML, "root");




        //            //HttpContext.Current.Session["tokenid"] = tokenid.ToString();
        //            //string newtoken = HttpContext.Current.Session["tokenid"].ToString();
        //            //HttpContext.Current.Response.Cookies["tokenid"].Value = tokenid.ToString();


        //        }
        //        if (responseXML.Contains("<TokenId>"))
        //        {
        //            XmlDocument doc1 = new XmlDocument();
        //            doc1.LoadXml(responseXML);
        //            string jsonText = JsonConvert.SerializeXmlNode(doc1);
        //            var obj = JObject.Parse(jsonText);
        //            string tokenid = (string)obj["root"]["TokenId"].ToString();
        //            HotelSearch.TokenId = tokenid;

        //            HotelSearch.datetime = DateTime.UtcNow.Date;
        //            //TimeSpan duration = DateTime.UtcNow.Subtract(HotelSearch.datetime);
        //            //HotelSearch.hours = duration.TotalHours;

        //        }



        //    }
        //    //catch (System.Threading.ThreadAbortException lException)
        //    catch (WebException webEx)
        //    {
        //        //get the response stream
        //        WebResponse response = webEx.Response;
        //        Stream stream = response.GetResponseStream();
        //        String responseMessage = new StreamReader(stream).ReadToEnd();
        //    }

        //    return responseXML;
        //}

        //public static string TBOPostXml(string url, string requestData)
        //{
        //    string responseXML = string.Empty;

        //    try
        //    {
        //        System.Net.ServicePointManager.Expect100Continue = false;
        //        byte[] data = Encoding.UTF8.GetBytes(requestData);
        //        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        //        request.Method = "POST";
        //        request.ContentType = "application/json";
        //        request.Headers.Add("Accept-Encoding", "gzip");
        //        using (Stream dataStream = request.GetRequestStream())
        //        {
        //            dataStream.Write(data, 0, data.Length);
        //            dataStream.Close();
        //        }


        //        using (HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse())
        //        {
        //            var rsp = webResponse.GetResponseStream();

        //            if (rsp == null)
        //            {
        //                //throw exception
        //            }

        //            if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
        //            {
        //                using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
        //                {
        //                    responseXML = JsonConvert.DeserializeXmlNode(readStream.ReadToEnd(), "root").InnerXml;
        //                }
        //                //if (responseXML.Contains("<TokenId>"))
        //                //{
        //                //    XmlDocument doc1 = new XmlDocument();
        //                //    doc1.LoadXml(responseXML);
        //                //    string jsonText = JsonConvert.SerializeXmlNode(doc1);
        //                //    var obj = JObject.Parse(jsonText);
        //                //    string tokenid = (string)obj["root"]["TokenId"].ToString();
        //                //    HotelSearch.TokenId = tokenid;

        //                //    HotelSearch.datetime = DateTime.UtcNow.Date;


        //                //}

        //            }
        //            else
        //            {
        //                using (StreamReader reader = new StreamReader(rsp, Encoding.Default))
        //                {
        //                    responseXML = JsonConvert.DeserializeXmlNode(reader.ReadToEnd(), "root").InnerXml;
        //                }
        //                //if (responseXML.Contains("<TokenId>"))
        //                //{
        //                //    XmlDocument doc1 = new XmlDocument();
        //                //    doc1.LoadXml(responseXML);
        //                //    string jsonText = JsonConvert.SerializeXmlNode(doc1);
        //                //    var obj = JObject.Parse(jsonText);
        //                //    string tokenid = (string)obj["root"]["TokenId"].ToString();
        //                //    HotelSearch.TokenId = tokenid;

        //                //    HotelSearch.datetime = DateTime.UtcNow.Date;


        //                //}
        //            }
        //        }
        //    }

        //    catch (WebException webEx)
        //    {
        //        //get the response stream
        //        WebResponse response = webEx.Response;
        //        Stream stream = response.GetResponseStream();
        //        String responseMessage = new StreamReader(stream).ReadToEnd();

        //    }


        //    return responseXML;

        //}

        protected string TBOPostXml(string url, string xml)
        {
            StringBuilder sbResult = new StringBuilder();
            string responseXML = string.Empty;

            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "application/json";
                    Http.Timeout = 56000;

                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    Stream responseStream = WebResponse.GetResponseStream();
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {

                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);

                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);

                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        responseXML = JsonConvert.DeserializeXmlNode(reader.ReadToEnd(), "root").InnerXml;

                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelDA objhtlDa = new HotelDA();
                HotelDA.InsertHotelErrorLog(WebEx, "TBOPOSTXML");
                WebResponse response = WebEx.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "TBO", "HotelInsert");
                }
                else
                {
                    int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, WebEx.Message, "TBO", "HotelInsert");
                    sbResult.Append(WebEx.Message + "<Error>");
                }
            }
            catch (Exception ex)
            {
                sbResult.Append(ex.Message + "<Error>");
                HotelDA.InsertHotelErrorLog(ex, "TBOPOSTXML");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, ex.Message, "TBO", "HotelInsert");
            }
            //return sbResult.ToString();
            return responseXML;
        }

        protected List<HotelResult> GetTBOHotelsPrice(string XmlRes, HotelSearch SearchDetails)
        {
            HotelDA objhtlDa = new HotelDA(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            List<HotelResult> objHotellist = new List<HotelResult>();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(XmlRes);
            string json = JsonConvert.SerializeXmlNode(doc);
            string traceid = (string)JObject.Parse(json)["root"]["HotelSearchResult"]["TraceId"].ToString();
            HotelSearch.TRACEID = (string)traceid;


            try
            {
                if (!XmlRes.Contains("<ErrorCode>0</ErrorCode>"))
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TBO_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");

                    XDocument document = XDocument.Parse(XmlRes);
                    var Hotels_temps = (from Hotel in document.Descendants("Error") select new { Errormsg = Hotel.Element("Description"), ErrorID = Hotel.Element("Code") }).ToList();
                    objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].ErrorID.Value + " : " + Hotels_temps[0].Errormsg.Value });
                }
                else
                {



                    XDocument document = XDocument.Parse(XmlRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    //var traceid = (from Hotel in document.Descendants("HotelSearchResult") select new { HotelDetails = Hotel }).ToList();
                    //foreach (var Htl in traceid)
                    //{
                    //    string Traceid = " ";

                    //    {
                    //        Traceid = Htl.HotelDetails.Element("HotelSearchResult").Element("TraceId").Value;

                    //    }
                    //}
                    var Hotels_temps = (from Hotel in document.Descendants("root").Descendants("HotelResults")
                                        select new { HotelRooms = Hotel }).ToList();

                    int k = 1100;
                    if (Hotels_temps.Count > 0)
                    {

                        foreach (var Hotels in Hotels_temps)
                        {
                            //if (k < 20)
                            //{
                            try
                            {
                                HotelInformation HotelInfo = new HotelInformation();
                                // HotelInfo = TBOHotelInfo(SearchDetails, Hotels.HotelRooms.Element("HotelCode").Value.Trim(), Hotels.HotelRooms.Element("ResultIndex").Value.Trim());
                                //k++;

                                int c = 0; decimal orgrates = 0; string RatePlanCode = "", location = "";
                                foreach (var hotelPrice in Hotels.HotelRooms.Descendants("Price"))
                                {
                                    decimal RoomPrice = 0;
                                    RoomPrice += Convert.ToDecimal(hotelPrice.Element("PublishedPriceRoundedOff").Value);

                                    if (c == 0)
                                    {
                                        orgrates = RoomPrice;// RatePlanCode = hotelPrice.Attribute("id").Value; c = 1;
                                    }
                                    if (RoomPrice < orgrates)
                                    {
                                        orgrates = RoomPrice; RatePlanCode = hotelPrice.Attribute("id").Value;
                                    }
                                }
                                orgrates = orgrates / (SearchDetails.NoofNight * SearchDetails.NoofRoom);
                                MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelInfo.StarRating, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country,"TBO", orgrates, SearchDetails.TBO_servicetax);
                                //MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelFilterArray[0]["Hotel_Star"].ToString().Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "TG", (baserate - disamt), 0);
                                //if (HotelInfo.Address.Split(',')[1].Length > 4 && HotelInfo.Address.Split(',')[1].Length < 16)
                                //    location = HotelInfo.Address.Split(',')[1];
                                //else if (HotelInfo.Address.Split(',')[1].Length > 15)
                                //    location = HotelInfo.Address.Split(',')[1].Substring(0, 15);

                                //Add Hotels Detaisl in List start
                                objHotellist.Add(new HotelResult
                                {
                                    HotelCityCode = SearchDetails.RegionId,
                                    HotelCity = SearchDetails.SearchCity,
                                    HotelCode = Hotels.HotelRooms.Element("HotelCode").Value.Trim(),
                                    HotelName = Hotels.HotelRooms.Element("HotelName").Value.Trim().Replace("'", ""),
                                    StarRating = Hotels.HotelRooms.Element("StarRating").Value.Trim(),
                                    Location = Hotels.HotelRooms.Element("HotelLocation").Value.Trim(),
                                    hotelPrice = MarkList.TotelAmt,
                                    AgtMrk = MarkList.AgentMrkAmt,
                                    DiscountMsg = "",
                                    hotelDiscoutAmt = 0,
                                    HotelAddress = Hotels.HotelRooms.Element("HotelAddress").Value.Trim(),
                                    HotelThumbnailImg = Hotels.HotelRooms.Element("HotelPicture").Value.Trim(),
                                    HotelServices = "",
                                    HotelDescription = "<strong>Description</strong>:" + System.Web.HttpUtility.HtmlDecode(Hotels.HotelRooms.Element("HotelDescription").Value),
                                    Lati_Longi = Hotels.HotelRooms.Element("Latitude").Value.Trim() + "##" + Hotels.HotelRooms.Element("Longitude").Value.Trim(),
                                    inclusions = "",
                                    Provider = "TBO",
                                    ReviewRating = "",
                                    ResultIndex = Hotels.HotelRooms.Element("ResultIndex").Value.Trim(),
                                    PopulerId = k++
                                });
                                //Add Hotels Detaisl in List end
                            }
                            catch (Exception ex)
                            {
                                objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                                HotelDA.InsertHotelErrorLog(ex, "InnerGetTBOHotelsPrice " + SearchDetails.SearchCity);
                            }
                            //}
                        }
                    }
                    else
                    {
                        objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search." });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TBO_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");
                    }
                }
            }
            catch (Exception ex)
            {
                objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                HotelDA.InsertHotelErrorLog(ex, "GetTBOHotelsPrice " + SearchDetails.SearchCity);
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TBO_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");
            }
            return objHotellist;
        }



        protected HotelInformation TBOHotelInfo(HotelSearch SearchDetails, string HotelCode, string ResultIndex)
        {
            HotelInformation HotelInfo = new HotelInformation();
            XDocument document;
            string HotelCodenew = "";
            if (HotelCode.Contains('|'))
            {
                string[] HOTELCODE = HotelCode.Split('|');
                HotelCodenew = (HOTELCODE[0] + HOTELCODE[1]).ToString();
            }
            else
            {
                HotelCodenew = HotelCode;
            }
            try
            {
                string XMLFile = ConfigurationManager.AppSettings["TBOHotelDetail"] + "\\" + HotelCodenew + ".xml";
                if (File.Exists(XMLFile))
                    document = XDocument.Load(XMLFile);
                else
                {
                    string TBOResponce = TBOHotelInformation(SearchDetails, HotelCode, ResultIndex);

                    document = XDocument.Parse(TBOResponce);
                    document.Save(XMLFile);
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", SearchDetails.TBO_HotelSearchReq, TBOResponce, "TBO", "HotelInsert");
                }

                var Hotelinfos = (from Hotel in document.Descendants("root").Descendants("HotelInfoResult") select new { HotelDetails = Hotel }).ToList();
                if (Hotelinfos.Count == 0)
                    Hotelinfos = (from Hotel in document.Elements("HotelInfoResult").Descendants("HotelDetails") select new { HotelDetails = Hotel }).ToList();
                if (Hotelinfos.Count > 0)
                {
                    foreach (var Htl in Hotelinfos)
                    {
                        string Addresss = " ,Address Not Found ";
                        try
                        {
                            Addresss = Htl.HotelDetails.Element("HotelDetails").Element("Address").Value;
                            //if (Htl.HotelDetails.Element("Address").Element("Address2") != null)
                            //  Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Address2").Value;
                            //if (Htl.HotelDetails.Element("Address").Element("City") != null)
                            //    Addresss += ", " + Htl.HotelDetails.Element("Address").Element("City").Value;
                            //if (Htl.HotelDetails.Element("Address").Element("Country") != null)
                            //    Addresss += ", " + Htl.HotelDetails.Element("Address").Element("Country").Value;
                        }
                        catch (Exception ex)
                        {
                            HotelDA.InsertHotelErrorLog(ex, ResultIndex + "_" + HotelCode + "-Addresss");
                            Addresss = " ," + SearchDetails.SearchCity;
                        }
                        HotelInfo.Address = Addresss;

                        HotelInfo.HotelDiscription = "";
                        if (Htl.HotelDetails.Element("HotelDetails").Element("Description") != null)
                        {


                            // HotelInfo.HotelDiscription = "<strong>Description</strong>: " + System.Web.HttpUtility.HtmlDecode(discrepsion.Element("Text").Value).Replace("&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt;&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt;&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt;&lt;p&gt;&lt;b&gt;", " ").Replace("&lt;/b&gt; &lt;br /&gt;", " ").Replace("&lt;/p&gt; &lt;br/&gt;", " ").Replace("&lt;br/&gt;", " ").Replace("&lt;br/&gt;", "").Replace("&lt;br /&gt;", " ").Replace("&lt;br /&gt;", " ").Replace("&lt;br /&gt; &lt;br/&gt;", " ").Replace("&lt;br /&gt;", " ").Replace("&lt;br /&gt;", " ");
                            HotelInfo.HotelDiscription = "<strong>Description</strong>: " + System.Web.HttpUtility.HtmlDecode(Htl.HotelDetails.Element("HotelDetails").Element("Description").Value);

                        }



                        HotelInfo.HotelServices = "";
                        if (Htl.HotelDetails.Element("HotelDetails").Element("HotelFacilities") != null)
                            foreach (var flt in Htl.HotelDetails.Descendants("HotelDetails").Elements("HotelFacilities"))
                            {

                                HotelInfo.HotelServices += flt.Value + "#";

                            }

                        HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg"; HotelInfo.Latitude = ""; HotelInfo.Longitude = "";

                        if (Htl.HotelDetails.Element("HotelDetails").Element("Images") != null)


                            //foreach (var image in Htl.HotelDetails.Descendants("HotelDetails").Elements("Images"))
                            //{
                            //    HotelInfo.ThumbImg += image.Value;

                            //}
                            if (Htl.HotelDetails.Element("HotelDetails").Element("Images").Value.Contains("https:"))
                                HotelInfo.ThumbImg = Htl.HotelDetails.Element("HotelDetails").Element("Images").Value;
                            //else
                            //    HotelInfo.ThumbImg = "https://www.travelboutiqueonline.com" + Htl.HotelDetails.Element("Photo").Element("ThumbnailUrl").Value;
                        //if (Htl.HotelDetails.Element("HotelDetails").Element("GeneralInfo") != null)
                        if (Htl.HotelDetails.Element("HotelDetails").Element("Latitude") != null)
                        {
                            HotelInfo.Latitude = Htl.HotelDetails.Element("HotelDetails").Element("Latitude").Value;
                            HotelInfo.Longitude = Htl.HotelDetails.Element("HotelDetails").Element("Longitude").Value;
                        }
                        if (Htl.HotelDetails.Element("HotelDetails").Element("StarRating") != null)
                            HotelInfo.StarRating = Htl.HotelDetails.Element("HotelDetails").Element("StarRating").Value;
                        else
                            HotelInfo.StarRating = Htl.HotelDetails.Element("HotelDetails").Attribute("StarRating").Value;
                    }
                }
                else
                {
                    HotelInfo.StarRating = "0";
                    HotelInfo.Address = " ,Address Not Found ";
                    HotelInfo.HotelServices = "";
                    HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg";
                    HotelInfo.Latitude = ""; HotelInfo.Longitude = "";
                    HotelInfo.HotelDiscription = "";
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", ResultIndex + "_" + HotelCode, document.ToString(), "", "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                HotelInfo.Address = " ,Address Not Found ";
                HotelInfo.HotelServices = "";
                HotelInfo.ThumbImg = "Images/Hotel/NoImage.jpg";
                HotelInfo.Latitude = ""; HotelInfo.Longitude = "";
                HotelInfo.HotelDiscription = "";
                HotelInfo.StarRating = "0";
                HotelDA.InsertHotelErrorLog(ex, ResultIndex + "_" + HotelCode);
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", ResultIndex + "_" + HotelCode, "GetTBOHotelInformation", "", "HotelInsert");
            }
            return HotelInfo;
        }

        private string TBOHotelInformation(HotelSearch SearchDetails, string HotelCode, string ResultIndex)
        {
            string TBOResponce = "";
            try
            {
                SearchDetails = tboreq.SearchItemInformationRequest(SearchDetails, HotelCode, ResultIndex);
                SaveFile(SearchDetails.TBO_HotelSearchReq, "htlinforeq");
                TBOResponce = TBOPostXml(HotelSearch.TBOSEARCHURL, SearchDetails.TBO_HotelSearchReq);
                xmltojson(TBOResponce, "htlinfores");
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, HotelCode);
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTAItemInformationReq, TBOResponce, "TBO", "HotelInsert");

            }
            return TBOResponce;
        }

        //public RoomComposite TBORoomDetails(HotelSearch SearchDetails)
        //{
        //    RoomComposite objRoomDetals = new RoomComposite();
        //    List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
        //    HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();




        //    try
        //    {


        //        HotelInformation HotelInfo = new HotelInformation();
        //        HotelInfo = TBOHotelInfo(SearchDetails, SearchDetails.HtlCode, SearchDetails.tbo_Resultindex);

        //        SearchDetails.TBOroomdetail = getroomdetail(SearchDetails);
        //        XDocument document = XDocument.Parse(SearchDetails.TBOroomdetail);

        //        var RoomsDetails1 = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Descendants("HotelRoomsDetails")
        //                             select new { HotelRooms = Hotel }).ToList();


        //        var tboromindexlist = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Element("RoomCombinations").Descendants("RoomCombination")
        //                               select new { Hotelroomindex = Hotel }).ToList();
        //        //if(SearchDetails.TBOroomdetail.Contains("<InfoSource>FixedCombination</InfoSource>") || SearchDetails.NoofRoom ==1)
        //        //{
        //        // List<IEnumerable> roominextbo = new List<IEnumerable>();
        //        //string str;
        //        foreach (var roomindexer in tboromindexlist)
        //        {

        //            var count_no = roomindexer.Hotelroomindex.Descendants("RoomIndex").ToList();
        //            //if (SearchDetails.NoofRoom == count_no.Count || SearchDetails.NoofRoom==1)
        //            //{
        //            foreach (var num in count_no)
        //            {
        //                var RoomsDetails = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Descendants("HotelRoomsDetails")
        //                                    where (int)Hotel.Element("RoomIndex") == Convert.ToInt32(num.Value)
        //                                    select new { HotelRooms = Hotel }).ToList();

        //                //var RoomsDetails = (from Hotel in RoomsDetails1.Where(s => s.HotelRooms.Element("RoomIndex").Value == num.Value)
        //                //     select new { HotelRooms = Hotel }).ToList();


        //                if (RoomsDetails.Count > 0)
        //                {
        //                    #region Hotel Details
        //                    HotelDetail = GetTBORoomHotelInformation(SearchDetails.HtlCode);
        //                    #endregion
        //                    #region Room Details
        //                    foreach (var Hotels in RoomsDetails)
        //                    {
        //                        try
        //                        {
        //                            decimal Rate_Org = 0, MrktotalRate = 0, AdmMkr = 0, Agtmrk = 0, VenderTax = 0, Agttax = 0;
        //                            decimal tbotax = 0, ChildCharge = 0, OtherCharges = 0, Discount = 0, PublishedPriceRoundedOff = 0;
        //                            decimal OfferedPrice = 0, OfferedPriceRoundedOff = 0, AgentCommission = 0, AgentMarkUp = 0,
        //                                ServiceTax = 0, TDS = 0, ExtraGuestCharge = 0;



        //                            string RoomName = "", Inclusion = "", Org_RoomRateStr = "", MrkRoomrateStr = "", tboroomname = "";
        //                            tboroomname = Hotels.HotelRooms.Element("RoomTypeName").Value;
        //                            if (SearchDetails.TBOroomdetail.Contains("<InfoSource>FixedCombination</InfoSource>"))
        //                            {
        //                                for (int i = 0; i < SearchDetails.NoofRoom; i++)
        //                                {
        //                                    RoomName += Hotels.HotelRooms.Element("RoomTypeName").Value;
        //                                    if (SearchDetails.NoofRoom > 1)
        //                                        RoomName += "<div class='clear1'></div>";
        //                                    decimal OrgRate = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPriceRoundedOff").Value);

        //                                    MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, OrgRate, SearchDetails.TBO_servicetax);

        //                                    Rate_Org += OrgRate;
        //                                    MrktotalRate += MarkList.TotelAmt;
        //                                    AdmMkr += MarkList.AdminMrkAmt;
        //                                    Agtmrk += MarkList.AgentMrkAmt;
        //                                    Agttax += MarkList.AgentServiceTaxAmt;
        //                                    VenderTax += MarkList.VenderServiceTaxAmt;

        //                                    Org_RoomRateStr += Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + Hotels.HotelRooms.Element("DayRates").Element("Amount").Value + "/";
        //                                    MrkRoomrateStr = Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + MarkList.TotelAmt.ToString() + "/"; ;
        //                                }
        //                            }
        //                            else
        //                            {

        //                                RoomName += Hotels.HotelRooms.Element("RoomTypeName").Value;
        //                                decimal OrgRate = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPriceRoundedOff").Value);

        //                                MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, OrgRate, SearchDetails.TBO_servicetax);

        //                                Rate_Org += OrgRate;
        //                                MrktotalRate += MarkList.TotelAmt;
        //                                AdmMkr += MarkList.AdminMrkAmt;
        //                                Agtmrk += MarkList.AgentMrkAmt;
        //                                Agttax += MarkList.AgentServiceTaxAmt;
        //                                VenderTax += MarkList.VenderServiceTaxAmt;

        //                                Org_RoomRateStr += Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + Hotels.HotelRooms.Element("DayRates").Element("Amount").Value + "/";
        //                                MrkRoomrateStr = Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + MarkList.TotelAmt.ToString() + "/"; ;

        //                            }

        //                            //if (Hotels.HotelRooms.Element("Inclusion").Value != null)
        //                            //    Inclusion = Hotels.HotelRooms.Element("Inclusion").Value;
        //                            //Add Hotels Detaisl in List start
        //                            objRoomList.Add(new RoomList
        //                            {

        //                                HotelCode = SearchDetails.HtlCode,
        //                                RatePlanCode = Hotels.HotelRooms.Element("RatePlanCode").Value,
        //                                RoomTypeCode = Hotels.HotelRooms.Element("RoomTypeCode").Value,
        //                                tboroomindex = Hotels.HotelRooms.Element("RoomIndex").Value,
        //                                tboroomprice = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("RoomPrice").Value),
        //                                tbotax = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("Tax").Value),
        //                                tboChildCharge = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("ChildCharge").Value),
        //                                tboOtherCharges = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("OtherCharges").Value),
        //                                tboDiscount = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("Discount").Value),
        //                                tboPublishedPrice = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPrice").Value),
        //                                tboPublishedPriceRoundedOff = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPriceRoundedOff").Value),
        //                                tboOfferedPrice = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("OfferedPrice").Value),
        //                                tboOfferedPriceRoundedOff = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("OfferedPriceRoundedOff").Value),
        //                                tboAgentCommission = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("AgentCommission").Value),
        //                                tboAgentMarkUp = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("AgentMarkUp").Value),
        //                                tboServiceTax = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("ServiceTax").Value),
        //                                tboTDS = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("TDS").Value),
        //                                tboExtraGuestCharge = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("ExtraGuestCharge").Value),
        //                                RoomName = RoomName,
        //                                discountMsg = "",
        //                                DiscountAMT = 0,
        //                                Total_Org_Roomrate = Rate_Org,
        //                                TotalRoomrate = MrktotalRate,
        //                                AdminMarkupPer = MarkList.AdminMrkPercent,
        //                                AdminMarkupAmt = AdmMkr,
        //                                AdminMarkupType = MarkList.AdminMrkType,
        //                                AgentMarkupPer = MarkList.AgentMrkPercent,
        //                                AgentMarkupAmt = Agtmrk,
        //                                AgentMarkupType = MarkList.AgentMrkType,
        //                                AgentServiseTaxAmt = Agttax,
        //                                V_ServiseTaxAmt = VenderTax,
        //                                AmountBeforeTax = 0,
        //                                Taxes = 0,
        //                                MrkTaxes = 0,
        //                                ExtraGuest_Charge = 0,
        //                                Smoking = "",
        //                                inclusions = Inclusion,
        //                                CancelationPolicy = "", //RoomXmlCancellationpolicy(SearchDetails, Hotels.HotelRooms.Attribute("id").Value, HotelDetail.StarRating),
        //                                OrgRateBreakups = Org_RoomRateStr,
        //                                MrkRateBreakups = MrkRoomrateStr,
        //                                DiscRoomrateBreakups = "",
        //                                EssentialInformation = "",
        //                                RoomDescription = "",
        //                                Provider = "TBO",
        //                                RoomImage = "",
        //                                tboroomname = tboroomname,
        //                                tborateplancod = Hotels.HotelRooms.Element("RatePlanCode").Value,
        //                                tboroomtypecode = Hotels.HotelRooms.Element("RoomTypeCode").Value,
        //                                tboinfosource = Hotels.HotelRooms.Element("InfoSource").Value
        //                            });
        //                        }
        //                        catch (Exception ex)
        //                        {
        //                            objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
        //                            HotelDA.InsertHotelErrorLog(ex, "Roomsadding");
        //                        }
        //                    }

        //                    #endregion
        //                    objRoomDetals.RoomDetails = objRoomList;
        //                    objRoomDetals.SelectedHotelDetail = HotelDetail;
        //                }
        //                else
        //                {
        //                    objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
        //                    HotelDA objhtlDa = new HotelDA();
        //                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TBO_HotelSearchReq, SearchDetails.TBO_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
        //                }

        //            }

        //            //}
        //            //else
        //            //{ 

        //            //}
        //        }
        //        //}

        //        //else { }


        //    }
        //    catch (Exception ex)
        //    {
        //        objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
        //        HotelDA objhtlDa = new HotelDA();
        //        int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TBO_HotelSearchReq, SearchDetails.TBO_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
        //        HotelDA.InsertHotelErrorLog(ex, "TBO RoomDetals");
        //    }
        //    return objRoomDetals;
        //}


        public RoomComposite TBORoomDetails(HotelSearch SearchDetails)
        {
            RoomComposite objRoomDetals = new RoomComposite();
            List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
            HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();




            try
            {



                HotelInformation HotelInfo = new HotelInformation();
                HotelInfo = TBOHotelInfo(SearchDetails, SearchDetails.HtlCode, SearchDetails.tbo_Resultindex);


                SearchDetails.TBOroomdetail = getroomdetail(SearchDetails);
                XDocument document = XDocument.Parse(SearchDetails.TBOroomdetail);

                var RoomsDetails1 = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Descendants("HotelRoomsDetails")
                                     select new { HotelRooms = Hotel }).ToList();


                var tboromindexlist = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Element("RoomCombinations").Descendants("RoomCombination")
                                       select new { Hotelroomindex = Hotel }).ToList();
                int com_open = 0;

                if (SearchDetails.TBOroomdetail.Contains("<InfoSource>OpenCombination</InfoSource>") && SearchDetails.NoofRoom > 1)
                {
                    objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TBO_HotelSearchReq, SearchDetails.TBO_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");

                }
                else
                {

                    foreach (var roomindexer in tboromindexlist)
                    {


                        com_open++;
                        var count_no = roomindexer.Hotelroomindex.Descendants("RoomIndex").ToList();
                        foreach (var num in count_no)
                        {

                            var RoomsDetails = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Descendants("HotelRoomsDetails")
                                                where (int)Hotel.Element("RoomIndex") == Convert.ToInt32(num.Value)
                                                select new { HotelRooms = Hotel }).ToList();

                            if (RoomsDetails.Count > 0)
                            {
                                #region Hotel Details
                                HotelDetail = GetTBORoomHotelInformation(SearchDetails.HtlCode);
                                #endregion
                                #region Room Details
                                foreach (var Hotels in RoomsDetails)
                                {
                                    try
                                    {
                                        decimal Rate_Org = 0, MrktotalRate = 0, AdmMkr = 0, Agtmrk = 0, VenderTax = 0, Agttax = 0;
                                        decimal tbotax = 0, ChildCharge = 0, OtherCharges = 0, Discount = 0, PublishedPriceRoundedOff = 0;
                                        decimal OfferedPrice = 0, OfferedPriceRoundedOff = 0, AgentCommission = 0, AgentMarkUp = 0,
                                            ServiceTax = 0, TDS = 0, ExtraGuestCharge = 0; string commonid = "";
                                        string RoomAmenities = ""; string inclusion = "";


                                        string RoomName = "", Inclusion = "", Org_RoomRateStr = "", MrkRoomrateStr = "", tboroomname = "";
                                        tboroomname = Hotels.HotelRooms.Element("RoomTypeName").Value;
                                        if (SearchDetails.TBOroomdetail.Contains("<InfoSource>FixedCombination</InfoSource>"))
                                        {
                                            var count_n1 = roomindexer.Hotelroomindex.Descendants("RoomIndex").ToList();
                                            foreach (var cnt in count_n1)
                                            {
                                                var RoomsDetailsnew = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Descendants("HotelRoomsDetails")
                                                                       where (int)Hotel.Element("RoomIndex") == Convert.ToInt32(cnt.Value)
                                                                       select new { HotelRooms = Hotel }).ToList();

                                                foreach (var newhtl in RoomsDetailsnew)
                                                {
                                                    RoomName += newhtl.HotelRooms.Element("RoomTypeName").Value;
                                                    if (SearchDetails.NoofRoom > 1)
                                                        RoomName += "<div class='clear1'></div>";
                                                    decimal OrgRate = Convert.ToDecimal(newhtl.HotelRooms.Element("Price").Element("PublishedPriceRoundedOff").Value);

                                                    MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "TBO", OrgRate, SearchDetails.TBO_servicetax);

                                                    Rate_Org += OrgRate;
                                                    MrktotalRate += MarkList.TotelAmt;
                                                    AdmMkr += MarkList.AdminMrkAmt;
                                                    Agtmrk += MarkList.AgentMrkAmt;
                                                    Agttax += MarkList.AgentServiceTaxAmt;
                                                    VenderTax += MarkList.VenderServiceTaxAmt;

                                                    Org_RoomRateStr += newhtl.HotelRooms.Element("RoomTypeCode").Value + "_" + newhtl.HotelRooms.Element("DayRates").Element("Amount").Value + "/";
                                                    MrkRoomrateStr = newhtl.HotelRooms.Element("RoomTypeCode").Value + "_" + MarkList.TotelAmt.ToString() + "/"; ;
                                                    commonid += "_" + newhtl.HotelRooms.Element("RoomIndex").Value;
                                                }

                                            }

                                        }
                                        //else if (SearchDetails.TBOroomdetail.Contains("<InfoSource>OpenCombination</InfoSource>") && SearchDetails.NoofRoom > 1)
                                        //{

                                        //    RoomName += Hotels.HotelRooms.Element("RoomTypeName").Value;
                                        //    decimal OrgRate = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPriceRoundedOff").Value);

                                        //    MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, OrgRate, SearchDetails.TBO_servicetax);

                                        //    Rate_Org += OrgRate;
                                        //    MrktotalRate += MarkList.TotelAmt;
                                        //    AdmMkr += MarkList.AdminMrkAmt;
                                        //    Agtmrk += MarkList.AgentMrkAmt;
                                        //    Agttax += MarkList.AgentServiceTaxAmt;
                                        //    VenderTax += MarkList.VenderServiceTaxAmt;

                                        //    Org_RoomRateStr += Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + Hotels.HotelRooms.Element("DayRates").Element("Amount").Value + "/";
                                        //    MrkRoomrateStr = Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + MarkList.TotelAmt.ToString() + "/"; ;
                                        //    commonid = Convert.ToString(com_open);
                                        //}

                                        else
                                        {

                                            RoomName += Hotels.HotelRooms.Element("RoomTypeName").Value;
                                            decimal OrgRate = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPriceRoundedOff").Value);

                                            MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "TBO", OrgRate, SearchDetails.TBO_servicetax);

                                            Rate_Org += OrgRate;
                                            MrktotalRate += MarkList.TotelAmt;
                                            AdmMkr += MarkList.AdminMrkAmt;
                                            Agtmrk += MarkList.AgentMrkAmt;
                                            Agttax += MarkList.AgentServiceTaxAmt;
                                            VenderTax += MarkList.VenderServiceTaxAmt;

                                            Org_RoomRateStr += Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + Hotels.HotelRooms.Element("DayRates").Element("Amount").Value + "/";
                                            MrkRoomrateStr = Hotels.HotelRooms.Element("RoomTypeCode").Value + "_" + MarkList.TotelAmt.ToString() + "/"; ;

                                        }
                                        //var roomametiies = (from roomam in Hotels.HotelRooms.Descendants("Amenities")
                                        //                    select new { rroomamen = roomam }).ToList();
                                        //foreach (var amenties in roomametiies)
                                        //{
                                        //    RoomAmenities += amenties.rroomamen.Value + ",";
                                        //}


                                        ////if (Hotels.HotelRooms.Element("Inclusion").Value != null)
                                        ////{
                                        ////    inclusion = Hotels.HotelRooms.Element("Inclusion").Value;
                                        ////}

                                        objRoomList.Add(new RoomList
                                        {

                                            HotelCode = SearchDetails.HtlCode,
                                            RatePlanCode = Hotels.HotelRooms.Element("RatePlanCode").Value,
                                            RoomTypeCode = Hotels.HotelRooms.Element("RoomTypeCode").Value,
                                            tboroomindex = Hotels.HotelRooms.Element("RoomIndex").Value,
                                            tboroomprice = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("RoomPrice").Value),
                                            tbotax = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("Tax").Value),
                                            tboChildCharge = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("ChildCharge").Value),
                                            tboOtherCharges = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("OtherCharges").Value),
                                            tboDiscount = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("Discount").Value),
                                            tboPublishedPrice = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPrice").Value),
                                            tboPublishedPriceRoundedOff = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("PublishedPriceRoundedOff").Value),
                                            tboOfferedPrice = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("OfferedPrice").Value),
                                            tboOfferedPriceRoundedOff = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("OfferedPriceRoundedOff").Value),
                                            tboAgentCommission = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("AgentCommission").Value),
                                            tboAgentMarkUp = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("AgentMarkUp").Value),
                                            tboServiceTax = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("ServiceTax").Value),
                                            tboTDS = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("TDS").Value),
                                            tboExtraGuestCharge = Convert.ToDecimal(Hotels.HotelRooms.Element("Price").Element("ExtraGuestCharge").Value),
                                            tboroomamentities = RoomAmenities,
                                            CancelationPolicy = Hotels.HotelRooms.Element("CancellationPolicy").Value,
                                            RoomName = RoomName,
                                            discountMsg = "",
                                            DiscountAMT = 0,
                                            Total_Org_Roomrate = Rate_Org,
                                            TotalRoomrate = MrktotalRate,
                                            AdminMarkupPer = MarkList.AdminMrkPercent,
                                            AdminMarkupAmt = AdmMkr,
                                            AdminMarkupType = MarkList.AdminMrkType,
                                            AgentMarkupPer = MarkList.AgentMrkPercent,
                                            AgentMarkupAmt = Agtmrk,
                                            AgentMarkupType = MarkList.AgentMrkType,
                                            AgentServiseTaxAmt = Agttax,
                                            V_ServiseTaxAmt = VenderTax,
                                            AmountBeforeTax = 0,
                                            Taxes = 0,
                                            MrkTaxes = 0,
                                            ExtraGuest_Charge = 0,
                                            Smoking = "",
                                            inclusions = inclusion,
                                            //CancelationPolicy = "", //RoomXmlCancellationpolicy(SearchDetails, Hotels.HotelRooms.Attribute("id").Value, HotelDetail.StarRating),
                                            OrgRateBreakups = Org_RoomRateStr,
                                            MrkRateBreakups = MrkRoomrateStr,
                                            DiscRoomrateBreakups = "",
                                            EssentialInformation = "",
                                            RoomDescription = "",
                                            Provider = "TBO",
                                            RoomImage = "",
                                            tboroomname = tboroomname,
                                            tborateplancod = Hotels.HotelRooms.Element("RatePlanCode").Value,
                                            tboroomtypecode = Hotels.HotelRooms.Element("RoomTypeCode").Value,
                                            tboinfosource = Hotels.HotelRooms.Element("InfoSource").Value,
                                            tbocommonid = commonid,
                                        });


                                    }

                                    catch (Exception ex)
                                    {
                                        objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                                        HotelDA.InsertHotelErrorLog(ex, "Roomsadding");
                                    }
                                }

                                #endregion
                                objRoomDetals.RoomDetails = objRoomList;
                                objRoomDetals.SelectedHotelDetail = HotelDetail;
                            }
                            else
                            {
                                objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
                                HotelDA objhtlDa = new HotelDA();
                                int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TBO_HotelSearchReq, SearchDetails.TBO_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                            }

                        }

                    }
                }

            }
            //else
            //{ 

            //        }

                //}

                //else { }



            catch (Exception ex)
            {
                objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TBO_HotelSearchReq, SearchDetails.TBO_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                HotelDA.InsertHotelErrorLog(ex, "TBO RoomDetals");
            }
            return objRoomDetals;
        }




        private string getroomdetail(HotelSearch SearchDetails)
        {
            string roomdetailresponse = "";
            try
            {
                SearchDetails = tboreq.roomdetailsearchreq(SearchDetails);
                SaveFile(SearchDetails.tbo_roomsearchreq, "htlroomsearchreq");
                roomdetailresponse = TBOPostXml(HotelSearch.TBOSEARCHURL, SearchDetails.tbo_roomsearchreq);
                xmltojson(roomdetailresponse, "htlroomsearchresp");
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, SearchDetails.HtlCode);
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.GTAItemInformationReq, roomdetailresponse, "TBO", "HotelInsert");

            }
            return roomdetailresponse;
        }

        public SelectedHotel GetTBORoomHotelInformation(string HotelCode)
        {
            string HotelCodenew = "";
            if (HotelCode.Contains('|'))
            {
                string[] HOTELCODE = HotelCode.Split('|');
                HotelCodenew = (HOTELCODE[0] + HOTELCODE[1]).ToString();
            }
            else
            {
                HotelCodenew = HotelCode;
            }
            SelectedHotel HotelInfo = new SelectedHotel();
            try
            {
                XDocument document = XDocument.Load(ConfigurationManager.AppSettings["TBOHotelDetail"] + "\\" + HotelCodenew + ".xml");
                var Hoteldtl = (from Hotel in document.Descendants("root").Descendants("HotelInfoResult") select new { HotelDetails = Hotel }).ToList();

                if (Hoteldtl.Count == 0)
                    Hoteldtl = (from Hotel in document.Elements("HotelInfoResult").Descendants("HotelDetails") select new { HotelDetails = Hotel }).ToList();
                if (Hoteldtl.Count > 0)
                {
                    foreach (var Htl in Hoteldtl)
                    {
                        string Addresss = "Address Not Found", Telephones = "";
                        try
                        {
                            Addresss = Htl.HotelDetails.Element("HotelDetails").Element("Address").Value;



                            if (Htl.HotelDetails.Element("HotelDetails").Element("HotelContactNo").Value != null)
                                Telephones = Htl.HotelDetails.Element("HotelDetails").Element("HotelContactNo").Value;
                            HotelInfo.HotelContactNo = Telephones;
                        }
                        catch (Exception ex)
                        {
                            HotelDA.InsertHotelErrorLog(ex, HotelCode + "-Addresss");
                        }
                        HotelInfo.HotelAddress = Addresss;

                        string hoteldiscription = "", facliStr = "";
                        HotelInfo.Attraction = ""; HotelInfo.RoomAmenities = "";
                        HotelInfo.HotelDescription = "<strong>Description</strong>: ";
                        if (Htl.HotelDetails.Element("HotelDetails").Element("Description") != null)
                        {
                            hoteldiscription += "<div style='padding: 4px 0;'><span style='text-transform:uppercase; font-size:13px;font-weight: bold;color:#B21E55;'>" + Htl.HotelDetails.Element("HotelDetails").Element("Description").Value + ": </span></div><hr />";
                        }
                        HotelInfo.HotelDescription = hoteldiscription;
                        //Hotel facility Facilities
                        try
                        {
                            if (Htl.HotelDetails.Element("HotelDetails").Element("HotelFacilities") != null)
                                foreach (var flt in Htl.HotelDetails.Descendants("HotelDetails").Elements("HotelFacilities"))
                                {
                                    facliStr += "<div class='check1'>" + flt.Value + "</div>";


                                }
                            HotelInfo.HotelAmenities = facliStr;
                            //if (Htl.HotelDetails.Element("Amenity") != null)
                            //    foreach (var flt in Htl.HotelDetails.Descendants("Amenity"))
                            //    {
                            //        facliStr += "<div class='check1'>" + flt.Element("Text").Value.Trim() + "</div>"; ;
                            //    }
                        }
                        catch (Exception ex)
                        {
                            HotelDA.InsertHotelErrorLog(ex, HotelCode + "-Facilities");
                        }
                        //HotelInfo.HotelAmenities = facliStr;
                        string imgdiv = "";
                        //Images Links ImageLinks
                        ArrayList Images = new ArrayList();
                        try
                        {
                            int im = 0;
                            if (Htl.HotelDetails.Element("HotelDetails").Element("Images") != null)
                            {
                                foreach (var img in Htl.HotelDetails.Descendants("Images"))
                                {
                                    im++;
                                    if (img.Value.Contains("https:"))
                                    {
                                        imgdiv += "<img id='img" + im + "' src='" + img.Value + "' onmouseover='return ShowHtlImg(this);' title='view of guest room' alt='' class='imageHtlDetailsshow' />";
                                        Images.Add(img.Value);
                                    }
                                    else
                                    {
                                        imgdiv += "<img id='img" + im + "' src='https://www.travelboutiqueonline.com" + img.Element("Url").Value + "' onmouseover='return ShowHtlImg(this);' title='" + img.Element("PhotoType").Value + "' alt='' class='imageHtlDetailsshow' />";
                                        Images.Add("https://www.travelboutiqueonline.com" + img.Element("Url").Value);
                                    }
                                }
                            }
                            else Images.Add("Images/Hotel/NoImage.jpg");
                            HotelInfo.ThumbnailUrl = Images[0].ToString();
                            HotelInfo.HotelImage = imgdiv;
                        }
                        catch (Exception ex)
                        {
                            HotelDA.InsertHotelErrorLog(ex, HotelCode + "-ImageLinks");
                        }
                        HotelInfo.Lati_Longi = "";
                        if (Htl.HotelDetails.Element("GeneralInfo") != null)
                            if (Htl.HotelDetails.Element("GeneralInfo").Element("Latitude") != null)
                            {
                                HotelInfo.Lati_Longi = Htl.HotelDetails.Element("GeneralInfo").Element("Latitude").Value + "," + Htl.HotelDetails.Element("GeneralInfo").Element("Longitude").Value;
                            }
                        HotelInfo.StarRating = "0";
                        if (Htl.HotelDetails.Element("Stars") != null)
                            HotelInfo.StarRating = Htl.HotelDetails.Element("Stars").Value;
                        else
                            HotelInfo.StarRating = Htl.HotelDetails.Attribute("stars").Value;
                    }
                }
                else
                {
                    HotelInfo.StarRating = "0";
                    HotelInfo.HotelAddress = "";
                    HotelInfo.HotelContactNo = "";
                    HotelInfo.Attraction = "";
                    HotelInfo.HotelDescription = "";
                    HotelInfo.RoomAmenities = "";
                    HotelInfo.HotelAmenities = "";
                    HotelInfo.Lati_Longi = "";
                    HotelInfo.HotelImage = "<li><a href='Images/NoImage.jpg'><img src='Images/NoImage.jpg' width='40px' height='40px' title='Image not found' /></a></li>";
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", HotelCode, document.ToString(), "", "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, HotelCode + "-GetTBORoomHotelInformation");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", "", "", HotelCode, "HotelInsert");
            }
            return HotelInfo;
        }



        public string TBOCancellationpolicy(string resultindex, string hotelcode, string provider)
        {
            string Cancellationpolcy = "", SupplierNotes = "";

            HotelSearch HotelDetail = new HotelSearch();
            HotelDetail.tbo_Resultindex = resultindex;
            HotelDetail.HtlCode = hotelcode;
            try
            {
                int i = 0;

                HotelDetail = tboreq.cancellationreq(HotelDetail);
                SaveFile(HotelDetail.tbo_roomsearchreq, "htlcancelreq");
                string canpolyResponse = TBOPostXml(HotelSearch.TBOSEARCHURL, HotelDetail.tbo_roomsearchreq);
                xmltojson(canpolyResponse, "htlcancelresp");

                if (canpolyResponse.Contains("<Error>"))
                {
                    HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
                    MarkupDS = objhtlDa.GetHtlMarkup("", HotelDetail.AgentID, HotelDetail.Country, HotelDetail.SearchCity, "", HotelDetail.HtlType, "", "", 0,"", HotelDetail.AgencyGroup, "SEL");
                    //objhtlDa.GetHtlMarkup("", SearchDetails.AgentID, SearchDetails.Country, SearchDetails.SearchCity, "", SearchDetails.HtlType, "", "", 0, "", SearchDetails.AgencyGroup, "SEL");
                    XDocument document = XDocument.Parse(canpolyResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    var Hotels_temps = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Descendants("HotelRoomsDetails")
                                        select new { CanxFees = Hotel }).ToList();


                    foreach (var Htl in Hotels_temps)
                    {
                        Cancellationpolcy += "<li>The Last Cancellation date . " + Htl.CanxFees.Element("LastCancellationDate").Value + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";

                        if (i > 0)
                            Cancellationpolcy += "<div class='clear1'></div>";
                        if (Hotels_temps.Count > 1)
                            Cancellationpolcy += Htl.CanxFees.Element("RoomTypeName").Value + " policy<div class='clear1'></div>";



                        foreach (var htlcanc in Htl.CanxFees.Descendants("CancellationPolicies"))
                        {
                            if (Htl.CanxFees.Element("CancellationPolicies") != null)
                            {

                                Cancellationpolcy += "<li>Cancellation done from " + htlcanc.Element("FromDate").Value.Replace("T00:00:00", string.Empty) + " to " + htlcanc.Element("ToDate").Value + " the cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlcanc.Element("Charge").Value), 2).ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";

                                //foreach (var htlPoliy in htlcanc.Descendants("Fee"))
                                //{
                                //    if (htlPoliy.Attribute("from") == null || htlPoliy.Element("Amount") == null)
                                //    {
                                //        if (htlPoliy.Element("Amount") != null && htlPoliy.Element("from") == null)
                                //        {
                                //            //  decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                //            Cancellationpolcy += "<li>The cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), 2).ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                //        }
                                //        else if (htlPoliy.Element("from") == null && htlPoliy.Element("Amount") == null)
                                //            Cancellationpolcy += "<li>No refund if you cancel this booking. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                //        // break;
                                //    }
                                //    if (htlPoliy.Attribute("from") != null && htlPoliy.Element("Amount") != null)
                                //    {
                                //        // decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                //        Cancellationpolcy += "<li>Cancellation done from " + htlPoliy.Attribute("from").Value.Replace("T00:00:00", string.Empty) + " the cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), 2).ToString() + "/-.</li>";
                                //    }
                                //}
                            }

                            else if (Htl.CanxFees.Element("CancellationPolicy") == null)
                            {
                                Cancellationpolcy += "<li>" + htlcanc.Value + "</li>";
                            }
                        }
                        i++;


                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TBOCancellationpolicy");
                Cancellationpolcy += Cancellationpolcy + SupplierNotes + ex.Message;
            }
            //return "<span>" + Cancellationpolcy + "<li>" + SupplierNotes.Replace("lt;bgt;", "").Replace("lt;bgt;", "").Replace("lt;/brgt;", "").Replace("lt;/brgt;", "") + "</li></span>";
            return System.Web.HttpUtility.HtmlDecode("<span>" + Cancellationpolcy + "<li>" + SupplierNotes + "</li></span>");
        }



        public HotelBooking TBOHotelsPreBooking(HotelBooking HotelDetail)
        {
            try
            {
                if (HotelDetail.TBOURL != null)
                {
                    HotelDetail.BookingType = "prepare";
                    HotelDetail.ProBookingReq = tboreq.BookingTBORequest(HotelDetail);
                    SaveFile(HotelDetail.ProBookingReq, "htlblockroomreq");

                    HotelDetail.ProBookingRes = TBOPostXml(HotelSearch.TBOSEARCHURL, HotelDetail.ProBookingReq);
                    xmltojson(HotelDetail.ProBookingRes, "htlblockroomres");

                    //XDocument document1;
                    //string XMLFile = "D:\\HotelPriject\\B2BHotel_25_10_2014\\HotelRoomXML\\BookingPrepare_Response.xml";
                    //document1 = XDocument.Load(XMLFile);
                    //HotelDetail.ProBookingRes = document1.ToString();

                    if (HotelDetail.ProBookingRes.Contains("<ErrorCode>0</ErrorCode>"))
                    {
                        string resp = HotelDetail.ProBookingRes;
                        XDocument document = XDocument.Parse(resp.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                        var Hotels_temps = (from Hotel in document.Elements("root").Elements("BlockRoomResult").Descendants("HotelRoomsDetails")
                                            where Hotel.Element("RoomTypeCode").Value == HotelDetail.RoomTypeCode
                                            select new { ItemPrice = Hotel.Element("Price").Element("PublishedPriceRoundedOff").Value }).ToList();
                        decimal orgrateNow = 0;
                        foreach (var Htl in Hotels_temps)
                        {
                            orgrateNow += Convert.ToDecimal(Htl.ItemPrice);
                        }
                        //if (HotelDetail.Total_Org_Roomrate <= orgrateNow)
                        if (HotelDetail.Total_Org_Roomrate >= orgrateNow)
                            HotelDetail.ProBookingID = "true";
                        else
                        {
                            HotelDetail.BookingID = "Rate Not matching"; HotelDetail.ProBookingID = "false";
                        }
                    }
                    else
                    {
                        HotelDetail.BookingID = "Exception"; HotelDetail.ProBookingID = "false";
                    }
                }
                else
                {
                    HotelDetail.BookingID = "BookingNotAlowed"; HotelDetail.ProBookingID = "false";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TBOHotelsPreBooking");
                HotelDetail.BookingID = ex.Message; HotelDetail.ProBookingID = "false";
            }
            return HotelDetail;
        }




        public HotelBooking TBOHotelsBooking(HotelBooking HotelDetail)
        {
            try
            {
                HotelDetail.BookingConfRes = "";
                HotelDetail.BookingType = "confirm";
                HotelDetail.BookingConfReq = tboreq.TBOBookingRequest(HotelDetail);
                SaveFile(HotelDetail.BookingConfReq, "htlbookreq");
                HotelDetail.BookingConfRes = TBOPostXml(HotelSearch.TBOSEARCHURL, HotelDetail.BookingConfReq);
                xmltojson(HotelDetail.BookingConfRes, "htlbookresp");

                if (HotelDetail.BookingConfRes.Contains("<ErrorCode>0</ErrorCode>"))
                {
                    XDocument document = XDocument.Parse(HotelDetail.BookingConfRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    var HtlBooking = (from book in document.Elements("root").Descendants("BookResult")
                                      select new
                                      {
                                          bookConfirmation = book.Element("BookingId"),
                                          BookingStatus = book.Element("HotelBookingStatus"),
                                          BookingReferences = book.Element("BookingRefNo")
                                      }).ToList();

                    foreach (var Htlbook in HtlBooking)
                    {
                        HotelDetail.BookingID = Htlbook.bookConfirmation.Value;
                        HotelDetail.Status = Htlbook.BookingStatus.Value;
                        HotelDetail.ProBookingID = Htlbook.BookingReferences.Value;
                    }
                }
                else
                {
                    HotelDetail.ProBookingID = "Exception";
                    HotelDetail.BookingID = "";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GTAHotelsBooking");
                HotelDetail.ProBookingID = ex.Message; HotelDetail.BookingID = "";
            }
            return HotelDetail;
        }



        public static void SaveFile(string Res, string module)
        {
            string newFileName = module + DateTime.Now.ToString("hh_mm_ss");

            string activeDir = ConfigurationManager.AppSettings["HTLTBOSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\";// +securityToken + @"\";
            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }
            try
            {
                //XDocument xmlDoc = XDocument.Parse(Res);

                //xmlDoc.Save(activeDir + @"\" + filename + ".xml");

                string path = activeDir + newFileName + ".txt";//@"c:\temp\MyTest.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {

                        sw.Write(Res);

                    }
                }



            }
            catch (Exception ex)
            {
            }
        }

        public static void xmltojson(string Res, string mod)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(Res);
            string searchresponse = JsonConvert.SerializeXmlNode(doc);
            SaveFile(searchresponse, mod);

        }



        public string TBOCancellationPolicy_ForRoom(HotelSearch HotelDetail, string Quadid)
        {
            string Cancellationpolcy = "", SupplierNotes = "";
            try
            {
                int i = 0;
                HotelDetail.HtlRoomCode = Quadid;
                HotelDetail = tboreq.cancellationreq(HotelDetail);
                string canpolyResponse = TBOPostXml(HotelSearch.TBOSEARCHURL, HotelDetail.tbo_roomsearchreq);
                if (canpolyResponse.Contains("<Error>"))
                {
                    HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA(); DataSet MarkupDS = new DataSet();
                    MarkupDS = objhtlDa.GetHtlMarkup("", HotelDetail.AgentID, HotelDetail.Country, HotelDetail.SearchCity, "", HotelDetail.HtlType, "", "",0,"", HotelDetail.AgencyGroup , "SEL");
                   // objhtlDa.GetHtlMarkup("", SearchDetails.AgentID, SearchDetails.Country, SearchDetails.SearchCity, "", SearchDetails.HtlType, "", "", 0, "", SearchDetails.AgencyGroup, "SEL");
                    XDocument document = XDocument.Parse(canpolyResponse.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                    var Hotels_temps = (from Hotel in document.Element("root").Element("GetHotelRoomResult").Descendants("HotelRoomsDetails")
                                        select new { CanxFees = Hotel }).ToList();


                    foreach (var Htl in Hotels_temps)
                    {
                        Cancellationpolcy += "<li>The Last Cancellation date . " + Htl.CanxFees.Element("LastCancellationDate").Value.Replace("T00:00:00", string.Empty) + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";

                        if (i > 0)
                            Cancellationpolcy += "<div class='clear1'></div>";
                        if (Hotels_temps.Count > 1)
                            Cancellationpolcy += Htl.CanxFees.Element("RoomTypeName").Value + " policy<div class='clear1'></div>";



                        foreach (var htlcanc in Htl.CanxFees.Descendants("CancellationPolicies"))
                        {
                            if (Htl.CanxFees.Element("CancellationPolicies") != null)
                            {

                                Cancellationpolcy += "<li>Cancellation done from " + htlcanc.Element("FromDate").Value.Replace("T00:00:00", string.Empty) + "to" + htlcanc.Element("ToDate").Value.Replace("T00:00:00", string.Empty) + " the cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlcanc.Element("Charge").Value), 2).ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                                //foreach (var htlPoliy in htlcanc.Descendants("Fee"))
                                //{
                                //    if (htlPoliy.Attribute("from") == null || htlPoliy.Element("Amount") == null)
                                //    {
                                //        if (htlPoliy.Element("Amount") != null && htlPoliy.Element("from") == null)
                                //        {
                                //            //  decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                                //            Cancellationpolcy += "<li>The cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), 2).ToString() + "/-. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                            }
                            else if (Htl.CanxFees.Element("CancellationPolicies") == null)
                                Cancellationpolcy += "<li>No refund if you cancel this booking. You might be charged upto the full cost of stay, if you do not check-in to the hotel.</li>";
                            // break;

                                //    if (htlPoliy.Attribute("from") != null && htlPoliy.Element("Amount") != null)
                            //    {
                            //        // decimal cancelAmt = objHtlMrk.PolicyMrkCalculation(MarkupDS, StarRating, HotelDetail.AgentID, HotelDetail.SearchCity, HotelDetail.Country, Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), HotelDetail.servicetax);
                            //        Cancellationpolcy += "<li>Cancellation done from " + htlPoliy.Attribute("from").Value.Replace("T00:00:00", string.Empty) + " the cancellation charge will be Rs. " + Math.Round(Convert.ToDecimal(htlPoliy.Element("Amount").Attribute("amt").Value), 2).ToString() + "/-.</li>";
                            //    }
                            //}


                            else if (Htl.CanxFees.Element("CancellationPolicy") == null)
                            {
                                Cancellationpolcy += "<li>" + htlcanc.Value + "</li>";
                            }
                        }
                        i++;


                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TBOCancellationPolicy_ForRoom");
                Cancellationpolcy += Cancellationpolcy + SupplierNotes + ex.Message;
            }
            // return "<span>" + Cancellationpolcy + "<li>" + SupplierNotes.Replace("lt;bgt;", "").Replace("lt;bgt;", "").Replace("lt;/brgt;", "").Replace("lt;/brgt;", "") + "</li></span>";
            return System.Web.HttpUtility.HtmlDecode("<span>" + Cancellationpolcy + "<li>" + SupplierNotes + "</li></span>");
        }



        public HotelCancellation TBOHotelCancelation(HotelCancellation HotelDetail)
        {
            HotelDetail.CancellationCharge = 0; HotelDetail.CancellationID = "";
            try
            {
                HotelDetail.BookingCancelReq = tboreq.HotelCancellationRequest(HotelDetail, "confirm");
                SaveFile(HotelDetail.BookingCancelReq, "htlcancellationreq");

                HotelDetail.BookingCancelRes = TBOPostXml(HotelSearch.TBOSEARCHURL, HotelDetail.BookingCancelReq);
                // SaveFile(HotelDetail.BookingCancelRes, "htlCancellationresp");
                xmltojson(HotelDetail.BookingCancelRes, "htlCancellationresp");


                if (HotelDetail.BookingCancelRes.Contains("<ErrorCode>0</ErrorCode>"))
                {
                    XDocument document = XDocument.Parse(HotelDetail.BookingCancelRes);
                    var HtlBooking = (from book in document.Elements("root").Descendants("HotelChangeRequestResult")
                                      select new { Cancelltion = book }).ToList();

                    foreach (var Htlbook in HtlBooking)
                    {
                        HotelDetail.CancellationID = Htlbook.Cancelltion.Element("ChangeRequestId").Value;
                        HotelDetail.CancelStatus = Htlbook.Cancelltion.Element("ChangeRequestStatus").Value;
                        if (HotelDetail.CancelStatus == "1")
                        {
                            HotelDetail.CancelStatus = "Pending";
                        }
                        else if (HotelDetail.CancelStatus == "2")
                        {
                            HotelDetail.CancelStatus = "InProgress";
                        }
                        else if (HotelDetail.CancelStatus == "3")
                        {
                            HotelDetail.CancelStatus = "Cancelled";
                        }
                        else if (HotelDetail.CancelStatus == "4")
                        {
                            HotelDetail.CancelStatus = "Rejected";
                        }
                        //if (Htlbook.CancelAmt != null)
                        HotelDetail.CancellationCharge = Convert.ToDecimal(Htlbook.Cancelltion.Element("CancellationCharge").Value);
                    }
                }

                if (HotelDetail.CancelStatus == "cancelled")
                    HotelDetail.CancelStatus = "Cancelled";
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TBOPreCancelBooking");
            }

            return HotelDetail;
        }

    }
}
