﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using HotelShared;
using System.Web;


namespace HotelBAL
{
    public class TBOHotelRequest
    {

        public AllCombindHotel HotelSearchAvailabilityRequest(AllCombindHotel objALLcombindHotel, List<HotelCredencials> CredencialsListnew)
        {
            //StringBuilder ReqXml = new StringBuilder();
            string ReqXml = "";
            
            try
            {
                ReqXml = "{\"ClientId\":\"" + CredencialsListnew[0].OtherData + "\"," + "\"UserName\":\"" + CredencialsListnew[0].HotelUsername + "\"," + "\"Password\":\"" + CredencialsListnew[0].HotelPassword + "\"," + "\"EndUserIp\":\"" + CredencialsListnew[0].OtherData + "\"}";
                //ReqXml = "{\"ClientId\":\"ApiIntegrationNew\",\"UserName\":\"boknlok\",\"Password\":\"boknlok@123\",\"EndUserIp\":\"192.168.11.120\"}";
                //ReqXml.Append("<root>");
                //ReqXml.Append("<ClientId>" + HtlSearchQuery.TBOOrgID + "</ClientId>");
                //ReqXml.Append("<UserName>" + HtlSearchQuery.TBOUserID + "</UserName>");
                //ReqXml.Append("<Password>" + HtlSearchQuery.TBOPassword + "</Password>");
                //ReqXml.Append("<EndUserIp>192.168.11.120</EndUserIp>");
                //ReqXml.Append("</root>");

            }
            catch (Exception ex)
            {
                //ReqXml(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelSearchAvailabilityRequest");
            }
            objALLcombindHotel.AllHotelSearch.TBO_HotelSearchReq = ReqXml.ToString();
            return objALLcombindHotel;
        }

        public HotelSearch BookingRequesttbo(AllCombindHotel objALLcombindHotel, List<HotelCredencials> CredencialsListnew)
        {
            CitySearch citysearch = new CitySearch();
            StringBuilder ReqXml = new StringBuilder();
            HotelSearch SearchDetails = new HotelSearch();
            SearchDetails = objALLcombindHotel.AllHotelSearch;
            int cnt = 0;
            int cntt = 0;
            try
            {
                string CheckInsDate = SearchDetails.CheckInDate;
                string[] chkindate = CheckInsDate.Split('-');
                string CheckInDate = chkindate[2] + "/" + chkindate[1] + "/" + chkindate[0];

                ReqXml.Append("{\"CheckInDate\":\"" + CheckInDate + "\",");
                ReqXml.Append("\"NoOfNights\":\"" + SearchDetails.NoofNight + "\",");
                ReqXml.Append("\"CountryCode\":\"" + SearchDetails.CountryCode + "\",");
                ReqXml.Append("\"CityId\":\"" + Convert.ToInt32(SearchDetails.cityid) + "\",");
                ReqXml.Append("\"ResultCount\":null,");
                ReqXml.Append("\"PreferredCurrency\":\"INR\",");
                ReqXml.Append("\"GuestNationality\":\"" + SearchDetails.CountryCode + "\",");
                ReqXml.Append("\"NoOfRooms\":\"" + SearchDetails.NoofRoom + "\",");
                ReqXml.Append("\"RoomGuests\":[");
                for (int i = 0; i < Convert.ToInt32(SearchDetails.NoofRoom); i++)
                {
                    cnt++;

                    ReqXml.Append("{\"NoOfAdults\":" + SearchDetails.AdtPerRoom[i] + ",");
                    ReqXml.Append("\"NoOfChild\":" + SearchDetails.ChdPerRoom[i] + ",");
                    if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) != 0)
                    {

                        ReqXml.Append("\"ChildAge\":[");

                        for (int j = 0; j < Convert.ToInt32(SearchDetails.ChdPerRoom[i]); j++)
                        {
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) == 1)
                            {
                                ReqXml.Append("" + SearchDetails.ChdAge[i, j] + "");
                            }
                            if (Convert.ToInt32(SearchDetails.ChdPerRoom[i]) > 1)
                            {
                                cntt++;
                                ReqXml.Append("" + SearchDetails.ChdAge[i, j] + "");
                                int b = Convert.ToInt32(SearchDetails.ChdPerRoom[i]);
                                if (b != cntt)
                                {
                                    ReqXml.Append(",");
                                }
                            }


                        }
                        ReqXml.Append("]");
                    }
                    else
                    {
                        ReqXml.Append("\"ChildAge\":null");
                    }


                    ReqXml.Append("}");

                    int a = Convert.ToInt32(SearchDetails.NoofRoom);

                    if (a != cnt)
                    {
                        ReqXml.Append(",");
                    }

                    //if (a == i)
                    //{
                    //    ReqXml.Append("}");
                    //}
                    //else
                    //{
                    //    ReqXml.Append("},");
                    //}


                }


                ReqXml.Append("],");
                ReqXml.Append("\"PreferredHotel\":\"\",");
                ReqXml.Append("\"MaxRating\":5,");
                ReqXml.Append("\"MinRating\":0,");
                ReqXml.Append("\"ReviewScore\":null,");
                ReqXml.Append("\"IsNearBySearchAllowed\":false,");
                ReqXml.Append("\"EndUserIp\":\"123.1.1.1\",");
                ReqXml.Append("\"TokenId\":\"" + HotelSearch.TokenId + "\"}");





                //ReqXml = "{\"CheckInDate\":\"" + CheckInDate + "\"," + "\"NoOfNights\":\"" + SearchDetails.NoofNight + "\"," + "\"CountryCode\":\"" + SearchDetails.CountryCode + "\"," + "\"CityId\":\"" + Convert.ToInt32(SearchDetails.cityid) + "\"," + "\"ResultCount\":null," + "\"PreferredCurrency\":\"INR\"," + "\"GuestNationality\":\"" + SearchDetails.CountryCode + "\"," + "\"NoOfRooms\":\"" + SearchDetails.NoofRoom + "\"," + "\"RoomGuests\":[" + "{\"NoOfAdults\":" + SearchDetails.TotAdt + "," + "\"NoOfChild\":" + SearchDetails.TotChd + "," + "\"ChildAge\":null}" + "]," + "\"PreferredHotel\":\"\"," + "\"MaxRating\":5," + "\"MinRating\":0," + "\"ReviewScore\":null," + "\"IsNearBySearchAllowed\":false," + "\"EndUserIp\":\"123.1.1.1\"," + "\"TokenId\":\"" + HotelSearch.TokenId + "\" }";
                //HotelSearch.TBOSEARCHURL = "http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelResult/";

                HotelSearch.TBOSEARCHURL = "https://api.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/GetHotelResult/ ";//live url
            }
            catch (Exception ex)
            {
                //ReqXml(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelSearchAvailabilityRequest");
            }
            SearchDetails.TBO_HotelSearchReq = ReqXml.ToString();
            return SearchDetails;
        }


        public HotelSearch SearchItemInformationRequest(HotelSearch HtlSearchQuery, string HotelCode, string ResultIndex)
        {
            string ReqXml = "";
            try
            {
                ReqXml = "{\"ResultIndex\":\"" + Convert.ToInt32(ResultIndex) + "\"," + "\"HotelCode\":\"" + HotelCode + "\"," + "\"EndUserIp\":\"123.1.1.1\"," + "\"TokenId\":\"" + HotelSearch.TokenId + "\"," + "\"TraceId\":\"" + HotelSearch.TRACEID + "\"}";
                //HotelSearch.TBOSEARCHURL = "http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelInfo";
               HotelSearch.TBOSEARCHURL = "https://api.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/GetHotelInfo";//live url
            }
            catch (Exception ex)
            {
                //ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SearchItemInformationRequest");
            }
            HtlSearchQuery.TBO_HotelSearchReq = ReqXml.ToString();
            return HtlSearchQuery;
        }





        public HotelSearch roomdetailsearchreq(HotelSearch SearchDetails)
        {
            string ReqXml = "";
            try
            {
                ReqXml = "{\"ResultIndex\":\"" + SearchDetails.tbo_Resultindex + "\"," + "\"HotelCode\":\"" + SearchDetails.HtlCode + "\"," + "\"EndUserIp\":\"123.1.1.1\"," + "\"TokenId\":\"" + HotelSearch.TokenId + "\"," + "\"TraceId\":\"" + HotelSearch.TRACEID + "\"}";
                //HotelSearch.TBOSEARCHURL = "http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelRoom";
                HotelSearch.TBOSEARCHURL = "https://api.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/GetHotelRoom";//live url
            }
            catch (Exception ex)
            {
                //ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SearchItemInformationRequest");
            }
            SearchDetails.tbo_roomsearchreq = ReqXml.ToString();
            return SearchDetails;
        }



        public HotelSearch cancellationreq(HotelSearch cancpolicy)
        {
            string ReqXml = "";
            try
            {
                ReqXml = "{\"ResultIndex\":\"" + cancpolicy.tbo_Resultindex + "\"," + "\"HotelCode\":\"" + cancpolicy.HtlCode + "\"," + "\"EndUserIp\":\"123.1.1.1\"," + "\"TokenId\":\"" + HotelSearch.TokenId + "\"," + "\"TraceId\":\"" + HotelSearch.TRACEID + "\"}";
                //HotelSearch.TBOSEARCHURL = "http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/GetHotelRoom";
                HotelSearch.TBOSEARCHURL = "https://api.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/GetHotelRoom";//live url
            }
            catch (Exception ex)
            {
                //ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SearchItemInformationRequest");
            }
            cancpolicy.tbo_roomsearchreq = ReqXml.ToString();
            return cancpolicy;
        }


        public string BookingTBORequest(HotelBooking SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();

            int cnt = 0;
            try
            {
                ReqXml.Append("{\"ResultIndex\":\"" + SearchDetails.resultindex + "\",");
                ReqXml.Append("\"HotelCode\":\"" + SearchDetails.HtlCode + "\",");
                ReqXml.Append("\"HotelName\":\"" + SearchDetails.HotelName + "\",");
                ReqXml.Append("\"GuestNationality\":\"" + SearchDetails.CountryCode + "\",");
                ReqXml.Append("\"NoOfRooms\":\"" + SearchDetails.NoofRoom + "\",");
                ReqXml.Append("\"ClientReferenceNo\":\"0\",");
                ReqXml.Append("\"IsVoucherBooking\":\"true\",");
                ReqXml.Append("\"HotelRoomsDetails\":[");
                for (int a = 0; a < SearchDetails.NoofRoom; a++)
                {
                    cnt++;
                    ReqXml.Append("{");
                    ReqXml.Append("\"RoomIndex\":\"" + Convert.ToInt32(SearchDetails.RoomDetails[a].tboroomindex) + "\",");


                    ReqXml.Append("\"RoomTypeCode\":\"" + SearchDetails.RoomDetails[a].tboroomtypecode + "\",");
                    ReqXml.Append("\"RoomTypeName\":\"" + SearchDetails.tboroomname + "\",");
                    ReqXml.Append("\"RatePlanCode\":\"" + SearchDetails.RoomDetails[a].tborateplancod + "\",");
                    ReqXml.Append("\"BedTypeCode\":null,");
                    ReqXml.Append("\"SmokingPreference\":0,");
                    ReqXml.Append("\"Supplements\":null,");
                    ReqXml.Append("\"Price\":{");
                    ReqXml.Append("\"CurrencyCode\":\"INR\",");
                    ReqXml.Append("\"RoomPrice\":\"" + SearchDetails.RoomDetails[a].tboroomprice + "\",");
                    ReqXml.Append("\"Tax\":\"" + SearchDetails.RoomDetails[a].tbotax + "\",");
                    ReqXml.Append("\"ExtraGuestCharge\":\"" + SearchDetails.RoomDetails[a].tboExtraGuestCharge + "\",");
                    ReqXml.Append("\"ChildCharge\":\"" + SearchDetails.RoomDetails[a].tboChildCharge + "\",");
                    ReqXml.Append("\"OtherCharges\":\"" + SearchDetails.RoomDetails[a].tboOtherCharges + "\",");
                    ReqXml.Append("\"Discount\":\"" + SearchDetails.RoomDetails[a].tboDiscount + "\",");
                    ReqXml.Append("\"PublishedPrice\":\"" + SearchDetails.RoomDetails[a].tboPublishedPrice + "\",");
                    ReqXml.Append("\"PublishedPriceRoundedOff\":\"" + SearchDetails.RoomDetails[a].tboPublishedPriceRoundedOff + "\",");
                    ReqXml.Append("\"OfferedPrice\":\"" + SearchDetails.RoomDetails[a].tboOfferedPrice + "\",");
                    ReqXml.Append("\"OfferedPriceRoundedOff\":\"" + SearchDetails.RoomDetails[a].tboOfferedPriceRoundedOff + "\",");
                    ReqXml.Append("\"AgentCommission\":\"" + SearchDetails.RoomDetails[a].tboAgentCommission + "\",");
                    ReqXml.Append("\"AgentMarkUp\":\"" + SearchDetails.RoomDetails[a].tboAgentMarkUp + "\",");
                    ReqXml.Append("\"ServiceTax\":\"" + SearchDetails.RoomDetails[a].tboServiceTax + "\",");
                    ReqXml.Append("\"TDS\":\"" + SearchDetails.RoomDetails[a].tboTDS + "\"");
                    ReqXml.Append("}");
                    ReqXml.Append("}");

                    int b = Convert.ToInt32(SearchDetails.NoofRoom);

                    if (b != cnt)
                    {
                        ReqXml.Append(",");
                    }
                }
                ReqXml.Append("],");
                ReqXml.Append("\"EndUserIp\":\"123.1.1.1\",");
                ReqXml.Append("\"TokenId\":\"" + HotelSearch.TokenId + "\",");
                ReqXml.Append("\"TraceId\":\"" + HotelSearch.TRACEID + "\"");
                ReqXml.Append("}");
                //HotelSearch.TBOSEARCHURL = "http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/BlockRoom";
                HotelSearch.TBOSEARCHURL = "https://api.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/BlockRoom";//live url
                //HotelSearch.TBOSEARCHURL = "https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/BlockRoom"; //new live url
            }


            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "PreBookHotelPriceRoomXmlRequest");
            }
            //SearchDetails.ProBookingReq = ReqXml.ToString();
            return ReqXml.ToString();
        }










        public string TBOBookingRequest(HotelBooking SearchDetails)
        {
            StringBuilder ReqXml = new StringBuilder();
            int cnt = 0;
            int e = 0;
            //bool roomHeade1 = true, roomHeade2 = true, roomHeade3 = true, roomHeade4 = true;
            try
            {
                ReqXml.Append("{\"ResultIndex\":\"" + SearchDetails.resultindex + "\",");
                ReqXml.Append("\"HotelCode\":\"" + SearchDetails.HtlCode + "\",");
                ReqXml.Append("\"HotelName\":\"" + SearchDetails.HotelName + "\",");
                ReqXml.Append("\"GuestNationality\":\"" + SearchDetails.CountryCode + "\",");
                ReqXml.Append("\"NoOfRooms\":\"" + SearchDetails.NoofRoom + "\",");
                ReqXml.Append("\"ClientReferenceNo\":\"0\",");
                ReqXml.Append("\"IsVoucherBooking\":\"true\",");
                ReqXml.Append("\"HotelRoomsDetails\":[");
                for (int i = 0; i < SearchDetails.NoofRoom; i++)
                {
                    cnt++;
                    ReqXml.Append("{");
                    ReqXml.Append("\"RoomIndex\":\"" + Convert.ToInt32(SearchDetails.RoomDetails[i].tboroomindex) + "\",");


                    ReqXml.Append("\"RoomTypeCode\":\"" + SearchDetails.RoomDetails[i].tboroomtypecode + "\",");
                    ReqXml.Append("\"RoomTypeName\":\"" + SearchDetails.tboroomname + "\",");
                    ReqXml.Append("\"RatePlanCode\":\"" + SearchDetails.RoomDetails[i].tborateplancod + "\",");
                    ReqXml.Append("\"BedTypeCode\":null,");
                    ReqXml.Append("\"SmokingPreference\":0,");
                    ReqXml.Append("\"Supplements\":null,");
                    ReqXml.Append("\"Price\":{");
                    ReqXml.Append("\"CurrencyCode\":\"INR\",");
                    ReqXml.Append("\"RoomPrice\":\"" + SearchDetails.RoomDetails[i].tboroomprice + "\",");
                    ReqXml.Append("\"Tax\":\"" + SearchDetails.RoomDetails[i].tbotax + "\",");
                    ReqXml.Append("\"ExtraGuestCharge\":\"" + SearchDetails.RoomDetails[i].tboExtraGuestCharge + "\",");
                    ReqXml.Append("\"ChildCharge\":\"" + SearchDetails.RoomDetails[i].tboChildCharge + "\",");
                    ReqXml.Append("\"OtherCharges\":\"" + SearchDetails.RoomDetails[i].tboOtherCharges + "\",");
                    ReqXml.Append("\"Discount\":\"" + SearchDetails.RoomDetails[i].tboDiscount + "\",");
                    ReqXml.Append("\"PublishedPrice\":\"" + SearchDetails.RoomDetails[i].tboPublishedPrice + "\",");
                    ReqXml.Append("\"PublishedPriceRoundedOff\":\"" + SearchDetails.RoomDetails[i].tboPublishedPriceRoundedOff + "\",");
                    ReqXml.Append("\"OfferedPrice\":\"" + SearchDetails.RoomDetails[i].tboOfferedPrice + "\",");
                    ReqXml.Append("\"OfferedPriceRoundedOff\":\"" + SearchDetails.RoomDetails[i].tboOfferedPriceRoundedOff + "\",");
                    ReqXml.Append("\"AgentCommission\":\"" + SearchDetails.RoomDetails[i].tboAgentCommission + "\",");
                    ReqXml.Append("\"AgentMarkUp\":\"" + SearchDetails.RoomDetails[i].tboAgentMarkUp + "\",");
                    ReqXml.Append("\"ServiceTax\":\"" + SearchDetails.RoomDetails[i].tboServiceTax + "\",");
                    ReqXml.Append("\"TDS\":\"" + SearchDetails.RoomDetails[i].tboTDS + "\"");
                    ReqXml.Append("},");

                    ReqXml.Append("\"HotelPassenger\":[");
                    if (cnt == 1)
                    {
                        ReqXml.Append("{");
                        ReqXml.Append("\"Title\":\"" + SearchDetails.PGTitle + "\",");
                        ReqXml.Append("\"FirstName\":\"" + SearchDetails.PGFirstName + "\",");
                        ReqXml.Append("\"Middlename\":null,");
                        ReqXml.Append("\"LastName\":\"" + SearchDetails.PGLastName + "\",");
                        ReqXml.Append("\"Phoneno\":\"" + SearchDetails.PGContact + "\",");
                        ReqXml.Append("\"Email\":null,");
                        ReqXml.Append("\"PaxType\":1,");
                        ReqXml.Append("\"LeadPassenger\":true,");
                        ReqXml.Append("\"Age\":0,");
                        ReqXml.Append("\"PassportNo\":null,");
                        ReqXml.Append("\"PassportIssueDate\":\"0001-01-01T00:00:00\",");
                        ReqXml.Append("\"PassportExpDate\":\"0001-01-01T00:00:00\"");
                        ReqXml.Append("}");
                    }
                    if (SearchDetails.AdditinalGuest != null)
                    {



                        ArrayList list = new ArrayList();
                        list = SearchDetails.AdditinalGuest;

                        //int j = Convert.ToInt32(SearchDetails.AdtPerRoom[i]) + Convert.ToInt32(SearchDetails.ChdPerRoom[i]);
                        int cntt = 0;

                        //if (cnt == 1)
                        //{
                        //    j = j - 1;
                        //}

                        ArrayList li = new ArrayList();
                        //li = (ArrayList)list[i];

                        for (e = 0; e < list.Count; e++)
                        {
                            //cntt++;

                            li = (ArrayList)list[e];

                            if (Convert.ToInt32(li[5]) == 1 && cnt == 1)
                            {
                                cntt++;

                                if (cntt == 1)
                                {
                                    ReqXml.Append(",");
                                }
                                ReqXml.Append("{");
                                ReqXml.Append("\"Title\":\"" + li[0] + "\",");
                                ReqXml.Append("\"FirstName\":\"" + li[1] + "\",");
                                ReqXml.Append("\"Middlename\":\"\",");
                                ReqXml.Append("\"LastName\":\"" + li[2] + "\",");
                                ReqXml.Append("\"Phoneno\":\"\",");
                                ReqXml.Append("\"Email\":\"\",");
                                if ("" + li[3] + "" == "Child")
                                {
                                    ReqXml.Append("\"PaxType\":2,");
                                    ReqXml.Append("\"LeadPassenger\":false,");
                                    ReqXml.Append("\"Age\":" + li[4] + ",");
                                }
                                else
                                {
                                    ReqXml.Append("\"PaxType\":1,");
                                    ReqXml.Append("\"LeadPassenger\":true,");
                                    ReqXml.Append("\"Age\":0,");
                                }


                                ReqXml.Append("\"PassportNo\":null,");
                                ReqXml.Append("\"PassportIssueDate\":\"0001-01-01T00:00:00\",");
                                ReqXml.Append("\"PassportExpDate\":\"0001-01-01T00:00:00\"");
                                ReqXml.Append("}");
                                int d = (Convert.ToInt32(SearchDetails.AdtPerRoom[i]) + Convert.ToInt32(SearchDetails.ChdPerRoom[i])) - 1;

                                if (d != cntt)
                                {
                                    ReqXml.Append(",");
                                }
                            }




                            if (Convert.ToInt32(li[5]) == 2 && cnt == 2)
                            {

                                cntt++;
                                ReqXml.Append("{");
                                ReqXml.Append("\"Title\":\"" + li[0] + "\",");
                                ReqXml.Append("\"FirstName\":\"" + li[1] + "\",");
                                ReqXml.Append("\"Middlename\":\"\",");
                                ReqXml.Append("\"LastName\":\"" + li[2] + "\",");
                                ReqXml.Append("\"Phoneno\":\"\",");
                                ReqXml.Append("\"Email\":\"\",");
                                if ("" + li[3] + "" == "Child")
                                {
                                    ReqXml.Append("\"PaxType\":2,");
                                    ReqXml.Append("\"LeadPassenger\":false,");
                                    ReqXml.Append("\"Age\":" + li[4] + ",");
                                }
                                else
                                {
                                    ReqXml.Append("\"PaxType\":1,");
                                    ReqXml.Append("\"LeadPassenger\":true,");
                                    ReqXml.Append("\"Age\":0,");
                                }
                                ReqXml.Append("\"PassportNo\":null,");
                                ReqXml.Append("\"PassportIssueDate\":\"0001-01-01T00:00:00\",");
                                ReqXml.Append("\"PassportExpDate\":\"0001-01-01T00:00:00\"");
                                ReqXml.Append("}");
                                int d = Convert.ToInt32(SearchDetails.AdtPerRoom[i]) + Convert.ToInt32(SearchDetails.ChdPerRoom[i]);

                                if (d != cntt)
                                {
                                    ReqXml.Append(",");
                                }
                            }




                            if (Convert.ToInt32(li[5]) == 3 && cnt == 3)
                            {
                                cntt++;
                                ReqXml.Append("{");
                                ReqXml.Append("\"Title\":\"" + li[0] + "\",");
                                ReqXml.Append("\"FirstName\":\"" + li[1] + "\",");
                                ReqXml.Append("\"Middlename\":\"\",");
                                ReqXml.Append("\"LastName\":\"" + li[2] + "\",");
                                ReqXml.Append("\"Phoneno\":\"\",");
                                ReqXml.Append("\"Email\":\"\",");
                                if ("" + li[3] + "" == "Child")
                                {
                                    ReqXml.Append("\"PaxType\":2,");
                                    ReqXml.Append("\"LeadPassenger\":false,");
                                    ReqXml.Append("\"Age\":" + li[4] + ",");
                                }
                                else
                                {
                                    ReqXml.Append("\"PaxType\":1,");
                                    ReqXml.Append("\"LeadPassenger\":true,");
                                    ReqXml.Append("\"Age\":0,");
                                }


                                ReqXml.Append("\"PassportNo\":null,");
                                ReqXml.Append("\"PassportIssueDate\":\"0001-01-01T00:00:00\",");
                                ReqXml.Append("\"PassportExpDate\":\"0001-01-01T00:00:00\"");
                                ReqXml.Append("}");
                                int d = Convert.ToInt32(SearchDetails.AdtPerRoom[i]) + Convert.ToInt32(SearchDetails.ChdPerRoom[i]);

                                if (d != cntt)
                                {
                                    ReqXml.Append(",");
                                }
                            }




                            if (Convert.ToInt32(li[5]) == 4 && cnt == 4)
                            {
                                cntt++;
                                ReqXml.Append("{");
                                ReqXml.Append("\"Title\":\"" + li[0] + "\",");
                                ReqXml.Append("\"FirstName\":\"" + li[1] + "\",");
                                ReqXml.Append("\"Middlename\":\"\",");
                                ReqXml.Append("\"LastName\":\"" + li[2] + "\",");
                                ReqXml.Append("\"Phoneno\":\"\",");
                                ReqXml.Append("\"Email\":\"\",");
                                if ("" + li[3] + "" == "Child")
                                {
                                    ReqXml.Append("\"PaxType\":2,");
                                    ReqXml.Append("\"LeadPassenger\":false,");
                                    ReqXml.Append("\"Age\":" + li[4] + ",");
                                }
                                else
                                {
                                    ReqXml.Append("\"PaxType\":1,");
                                    ReqXml.Append("\"LeadPassenger\":true,");
                                    ReqXml.Append("\"Age\":0,");
                                }

                                ReqXml.Append("\"PassportNo\":null,");
                                ReqXml.Append("\"PassportIssueDate\":\"0001-01-01T00:00:00\",");
                                ReqXml.Append("\"PassportExpDate\":\"0001-01-01T00:00:00\"");
                                ReqXml.Append("}");
                                int g = Convert.ToInt32(SearchDetails.AdtPerRoom[i]) + Convert.ToInt32(SearchDetails.ChdPerRoom[i]);

                                if (g != cntt)
                                {
                                    ReqXml.Append(",");
                                }

                            }


                        }

                    }
                    ReqXml.Append("]");
                    ReqXml.Append("}");
                    int b = Convert.ToInt32(SearchDetails.NoofRoom);

                    if (b != cnt)
                    {
                        ReqXml.Append(",");
                    }
                }
                ReqXml.Append("],");
                ReqXml.Append("\"EndUserIp\":\"123.1.1.1\",");
                ReqXml.Append("\"TokenId\":\"" + HotelSearch.TokenId + "\",");
                ReqXml.Append("\"TraceId\":\"" + HotelSearch.TRACEID + "\"");
                ReqXml.Append("}");
                //HotelSearch.TBOSEARCHURL = "http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/Book";
                //HotelSearch.TBOSEARCHURL = "https://api.travelboutiqueonline.com/HotelAPI_V10/hotelservice.svc/rest/Book";//live url
                HotelSearch.TBOSEARCHURL = "https://booking.travelboutiqueonline.com/HotelAPI_V10/HotelService.svc/rest/Book";//new live url

            }


            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "BookHotelPriceTBORequest");
            }
            //SearchDetails.ProBookingReq = ReqXml.ToString();
            return ReqXml.ToString();
        }

        public string HotelCancellationRequest(HotelCancellation bookingDetails, string CancelType)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {


                ReqXml.Append("{");
                ReqXml.Append("\"BookingMode\":5,");
                ReqXml.Append("\"RequestType\":4,");
                ReqXml.Append("\"Remarks\":\"" + bookingDetails.tboremark + "\",");
                ReqXml.Append("\"BookingId\":" + bookingDetails.BookingID + ",");
                //ReqXml.Append("\"EndUserIp\":\"192.168.11.120\",");
                ReqXml.Append("\"EndUserIp\":\"182.75.66.42\",");//LIVE SERVERIP
                ReqXml.Append("\"TokenId\":\"" + HotelSearch.TokenId + "\"");
                ReqXml.Append("}");

                //HotelSearch.TBOSEARCHURL = "http://api.tektravels.com/BookingEngineService_Hotel/hotelservice.svc/rest/SendChangeRequest/";
                HotelSearch.TBOSEARCHURL = "https://api.travelboutiqueonline.com/HotelAPI_V10/rest/SendChangeRequest/";//live url
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelCancellationRequest");
            }
            return ReqXml.ToString();
        }

    }
}
