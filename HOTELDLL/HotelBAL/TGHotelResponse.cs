﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using HotelShared;
using HotelDAL;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Data;

namespace HotelBAL
{
    public class TGHotelResponse
    {
        public AllCombindHotel TGHotelSearchResponse(AllCombindHotel objALLcombindHotel)
        {
            // HotelComposite obgHotelsCombo = new HotelComposite();
            try
            {
                List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
                CredencialsListnew = objALLcombindHotel.AllHotelSearch.CredencialsList.Where(x => x.HotelProvider == "TG" && (x.HotelTrip == objALLcombindHotel.AllHotelSearch.HtlType || x.HotelTrip == "ALL")).ToList();

                if (CredencialsListnew.Count > 0)
                {
                    if (objALLcombindHotel.AllHotelSearch.CorporateID == "Flywidus")
                    {
                        //objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq = HotelSearchRequestWithPagination(objALLcombindHotel.AllHotelSearch, CredencialsListnew,1);
                        //objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes = TGSearchPostXml(CredencialsListnew[0].HotelUrl, objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq);

                        //objALLcombindHotel.AllHotelSearch = TGHotels(objALLcombindHotel.AllHotelSearch, CredencialsListnew);
                        //objALLcombindHotel.TGHotelList = GetTGResult(objALLcombindHotel.AllHotelSearch);
                        objALLcombindHotel = GetTGPaginationHotelsPrice(objALLcombindHotel, CredencialsListnew);
                    }
                    else
                    {
                        objALLcombindHotel.AllHotelSearch = TGHotels(objALLcombindHotel.AllHotelSearch, CredencialsListnew);
                        objALLcombindHotel.TGHotelList = GetTGResult(objALLcombindHotel.AllHotelSearch);
                    }
                }
                else
                {
                    objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes = "Hotel Not available for " + objALLcombindHotel.AllHotelSearch.SearchCity + ", " + objALLcombindHotel.AllHotelSearch.Country;
                    objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq = "not allow";
                    List<HotelResult> objHotellist = new List<HotelResult>();
                    objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes });
                    objALLcombindHotel.TGHotelList = objHotellist;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelSearchResponse");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq, objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes, objALLcombindHotel.AllHotelSearch.AgentID, "HotelInsert");
                List<HotelResult> objHotellist = new List<HotelResult>();
                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = ex.Message });
                objALLcombindHotel.TGHotelList = objHotellist;
            }
            //obgHotelsCombo.HotelSearchDetail = SearchDetails;
            return objALLcombindHotel;
        }
        public RoomComposite TGSelectedHotelResponse(HotelSearch SearchDetails)
        {
            RoomComposite objroomlist = new RoomComposite();
            try
            {
                List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
                CredencialsListnew = SearchDetails.CredencialsList.Where(x => x.HotelProvider == "TG" && (x.HotelTrip == SearchDetails.HtlType || x.HotelTrip == "ALL")).ToList();

                SearchDetails = TGHotels(SearchDetails, CredencialsListnew);
                objroomlist = GetTGRoomList(SearchDetails);
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGSelectedHotelResponse");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
            }
            return objroomlist;
        }
        protected HotelSearch TGHotels(HotelSearch SearchDetails, List<HotelCredencials> CredencialsListnew)
        {
            try
            {
                SearchDetails.TG_HotelSearchReq = HotelSearchRequest(SearchDetails, CredencialsListnew);
                SearchDetails.TG_HotelSearchRes = TGSearchPostXml(CredencialsListnew[0].HotelUrl, SearchDetails.TG_HotelSearchReq);
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                SearchDetails.TG_HotelSearchRes = ex.Message;
            }
            return SearchDetails;
        }
        protected string TGSearchPostXml(string url, string xml)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    Http.Timeout = 29000;
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelSendMail_Log Objsendmail = new HotelSendMail_Log();
                HotelDA.InsertHotelErrorLog(WebEx, "");
                HotelDA objhtlDa = new HotelDA();

                WebResponse response = WebEx.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", url, responseMessage, "TG", "HotelInsert");
                }
                else
                {
                    int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", url, WebEx.Message, "TG", "HotelInsert");
                    sbResult.Append("<Errors>" + WebEx.Message + "</Errors>");
                }
            }
            catch (Exception exx)
            {
                sbResult.Append(exx.Message);
                HotelDA.InsertHotelErrorLog(exx, "");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, exx.Message, "TG", "HotelInsert");
            }
            return sbResult.ToString();
        }
        protected string TGPostXml(string url, string xml)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelSendMail_Log Objsendmail = new HotelSendMail_Log();
                HotelDA.InsertHotelErrorLog(WebEx, "");
                HotelDA objhtlDa = new HotelDA();

                WebResponse response = WebEx.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", url, responseMessage, "TG", "HotelInsert");
                }
                else
                {
                    int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", url, WebEx.Message, "TG", "HotelInsert");
                    sbResult.Append("<Errors>" + WebEx.Message + "</Errors>");
                }
            }
            catch (Exception exx)
            {
                sbResult.Append(exx.Message);
                HotelDA.InsertHotelErrorLog(exx, "");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, exx.Message, "TG", "HotelInsert");
            }
            return sbResult.ToString();
        }

        protected List<HotelResult> GetTGResult(HotelSearch SearchDetails)
        {
            List<HotelResult> objHotellist = new List<HotelResult>(); HotelDA objhtlDa = new HotelDA();
            HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList(); MarkupList MarkListTax = new MarkupList();
            try
            {
                if (SearchDetails.TG_HotelSearchRes.Contains("<Success />") || SearchDetails.TG_HotelSearchRes.Contains("<Success/>"))
                {
                    string responses = SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));

                    var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                      select new { BasicPropertyInfo = htlprice.Element("BasicPropertyInfo"), TPA_Extensions = htlprice.Element("TPA_Extensions") }).ToList();
                    if (hotelprice.Count > 0)
                    {

                        string HotelCodes = "";
                        foreach (var m in hotelprice)
                        {
                            HotelCodes += m.BasicPropertyInfo.Attribute("HotelCode").Value + ",";
                        }
                        HotelCodes = HotelCodes.TrimEnd(',');
                        // DtTripAdvisor = objBa.GetTripAdvisorDetails(HotelCodes);
                        DataTable HotelDt = objhtlDa.GetHotelOverview(SearchDetails.SearchCity, HotelCodes, "TG");
                        int k = 400;
                        if (HotelDt.Rows.Count > 0)
                        {
                            foreach (var hoteldetails in hotelprice)
                            {
                                string HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                                DataRow[] HotelFilterArray = objHtlMrk.FilterHotelDT(HotelDt, HotelCode);
                                if (HotelFilterArray.Length > 0)
                                {
                                    var HotelsData = hoteldetails.BasicPropertyInfo.Parent;
                                    if (HotelsData.Element("RoomRates").Element("RoomRate") != null && HotelsData.Element("RatePlans").Element("RatePlan") != null)
                                    {
                                        try
                                        {
                                            decimal baserate = 0, taxs = 0, disamt = 0, ExtraGuest = 0, TotaldiscAmt = 0;
                                            foreach (var htldtl in HotelsData.Descendants("RoomRates").Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value))
                                            {
                                                foreach (var htl in htldtl.Descendants("Rates"))
                                                {
                                                    baserate += Convert.ToDecimal(htl.Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value.Trim());
                                                    if (htl.Element("Rate").Element("Base").Element("Taxes") != null)
                                                        taxs += Convert.ToDecimal(htl.Element("Rate").Element("Base").Element("Taxes").Attribute("Amount").Value.Trim());

                                                    if (htl.Element("Rate").Element("Discount") != null)
                                                        disamt += Convert.ToDecimal(htl.Element("Rate").Element("Discount").Attribute("AmountBeforeTax").Value.Trim());

                                                    if (htl.Element("Rate").Element("AdditionalGuestAmounts") != null)
                                                    {
                                                        decimal extrarate = 0;
                                                        foreach (var addigust in htl.Elements("Rate").Elements("AdditionalGuestAmounts").Descendants("AdditionalGuestAmount"))
                                                        {
                                                            extrarate += Convert.ToDecimal(addigust.Element("Amount").Attribute("AmountBeforeTax").Value.Trim());
                                                        }
                                                        ExtraGuest += extrarate;
                                                    }
                                                }
                                            }
                                            MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelFilterArray[0]["Hotel_Star"].ToString().Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "TG", (baserate - disamt), 0);
                                            if ((taxs + ExtraGuest) > 0)
                                            {
                                                MarkListTax = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (taxs + ExtraGuest), 0);
                                                MarkList.TotelAmt += MarkListTax.TotelAmt;
                                                MarkList.AgentMrkAmt = MarkListTax.AgentMrkAmt;
                                            }

                                            if (disamt > 0)
                                                TotaldiscAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (baserate + taxs + ExtraGuest), 0);
                                            HotelRatePlan objRatePlans = new HotelRatePlan();
                                            objRatePlans = RatePlanlist(hoteldetails.BasicPropertyInfo, hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value);


                                            // string Reviews;
                                            //if (HotelFilterArray[0]["ReviewRating"].ToString().Trim() != "" && HotelFilterArray[0]["ReviewRating"].ToString().Trim() != null)
                                            //  Reviews = objHtlMrk.SetTripAdvisorRating(Convert.ToDouble(HotelFilterArray[0]["ReviewRating"].ToString().Trim()), SearchDetails.BaseURL) + " (" + HotelFilterArray[0]["ReviewCount"].ToString() + " Reviews)";
                                            //  string htlpaln = DateTime.Now.ToString("hh.mm.ss.ff");
                                            objHotellist.Add(new HotelResult
                                            {
                                                RatePlanCode = hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value,
                                                hotelDiscoutAmt = Math.Round(TotaldiscAmt / (SearchDetails.NoofNight * SearchDetails.NoofRoom)),
                                                hotelPrice = Math.Round(MarkList.TotelAmt / (SearchDetails.NoofNight * SearchDetails.NoofRoom)),
                                                AgtMrk = MarkList.AgentMrkAmt,
                                                DiscountMsg = objRatePlans.discountMsg,
                                                inclusions = objRatePlans.inclusions,
                                                RoomTypeCode = objRatePlans.RatePlanCode,
                                                HotelCode = HotelCode,
                                                HotelName = HotelFilterArray[0]["VendorName"].ToString().Trim().Replace("'", ""),
                                                HotelCityCode = SearchDetails.SearchCityCode,
                                                HotelCity = HotelFilterArray[0]["City"].ToString().Trim(),
                                                StarRating = HotelFilterArray[0]["Hotel_Star"].ToString().Trim(),
                                                HotelAddress = HotelFilterArray[0]["Address"].ToString().Trim(),
                                                HotelDescription = "",//"<strong>Description</strong>: " + HotelFilterArray[0]["HotelOverview"].ToString(),
                                                HotelThumbnailImg = HotelFilterArray[0]["ImagePath"].ToString().Trim().Replace("_TN", String.Empty),
                                                Lati_Longi = HotelFilterArray[0]["Latitude"].ToString().Trim() + "##" + HotelFilterArray[0]["Longitude"].ToString().Trim(),
                                                Location = HotelFilterArray[0]["Location"].ToString().Trim(),

                                                HotelServices = SetHotelService_Image(HotelCode),
                                                ReviewRating = HotelFilterArray[0]["ReviewRating"].ToString().Trim() + "#" + HotelFilterArray[0]["ReviewCount"].ToString(),
                                                Provider = "TG",
                                                PopulerId = k++
                                            });
                                        }
                                        catch (Exception ex)
                                        {
                                            HotelDA.InsertHotelErrorLog(ex, "GetTGResult_List-" + HotelCode);
                                            // objHotellist.Add(new HotelResult { HotelName = "", hotelPrice =0, HtlError = ex.Message });
                                        }
                                    }
                                }
                                //else
                                //{
                                //    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = "Hotel not found in DB,  Please modify your search.. " });
                                //}
                            }
                        }
                        else
                        {
                            objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found in DB,  Please modify your search.. " });
                            int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                        }
                    }
                    else
                    {
                        objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found,  Please modify your search.. " });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                    }
                }
                else
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                    try
                    {
                        if (SearchDetails.TG_HotelSearchRes.Contains("<Errors>"))
                        {
                            XDocument document = XDocument.Parse(SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                            var Hotels_temps = (from Hotel in document.Descendants("Errors") select new { Errormsg = Hotel.Element("Error") }).ToList();
                            if (Hotels_temps.Count > 0)
                            {
                                if (Hotels_temps[0].Errormsg.Attribute("Type") != null && Hotels_temps[0].Errormsg.Attribute("ShortText") != null)
                                    objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = Hotels_temps[0].Errormsg.Attribute("ShortText").Value + "  " + Hotels_temps[0].Errormsg.Attribute("Type").Value });
                                else
                                    objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                            }
                            else
                                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                        }
                        else
                        {
                            if (SearchDetails.TG_HotelSearchRes.Contains("<p>"))
                            {
                                string Serviceerror = SearchDetails.TG_HotelSearchRes.Substring(SearchDetails.TG_HotelSearchRes.IndexOf("<p>"), SearchDetails.TG_HotelSearchRes.IndexOf("</p>") - SearchDetails.TG_HotelSearchRes.IndexOf("<p>") - 2);
                                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = Serviceerror });
                            }
                            else
                                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                        }
                    }
                    catch (Exception gd)
                    {
                        objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                    }
                }
                if (objHotellist.Count == 0)
                {
                    objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GetTGResult _" + SearchDetails.SearchCity);
                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = ex.Message });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
            }
            return objHotellist;
        }
        protected AllCombindHotel GetTGPaginationHotelsPrice(AllCombindHotel objALLcombindHotel, List<HotelCredencials> CredencialsListnew)
        {
            List<HotelResult> objHotellist, AllHotelList = new List<HotelResult>();
            HotelDA objhtlDa = new HotelDA();
            try
            {
                XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                int htlcount = 0, PaginationFrom = 1;
                objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq = HotelSearchRequestWithPagination(objALLcombindHotel.AllHotelSearch, CredencialsListnew, PaginationFrom);
                objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes = TGSearchPostXml(CredencialsListnew[0].HotelUrl, objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq);

                if (objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.Contains("<Success />") || objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.Contains("<Success/>"))
                {
                    DataTable HotelDt = objhtlDa.GetHotelOverview(objALLcombindHotel.AllHotelSearch.SearchCity, "", "TG");
                    AllHotelList = OnlyHotelSearchAvailability(objALLcombindHotel.AllHotelSearch, HotelDt);

                    string prevresp = objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes, PrevReq = objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq;
                    try
                    {
                    FirSuruHoJa:

                        XDocument xmlresp = XDocument.Parse(objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                        XElement TotalAvailable = xmlresp.Element(soap + "Envelope").Element(soap + "Body").Element("OTA_HotelAvailRS").Element("TPA_Extensions").Element("HotelsInfo");
                        if (Convert.ToInt32(TotalAvailable.Attribute("available").Value) == 50)
                        {
                            PaginationFrom += 50;
                            objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq = HotelSearchRequestWithPagination(objALLcombindHotel.AllHotelSearch, CredencialsListnew, PaginationFrom);
                            objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes = TGSearchPostXml(CredencialsListnew[0].HotelUrl, objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq);
                            objHotellist = OnlyHotelSearchAvailability(objALLcombindHotel.AllHotelSearch, HotelDt);

                            PrevReq += objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq;
                            prevresp += objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes;

                            AllHotelList = AllHotelList.Union(objHotellist).ToList();

                            htlcount++;
                            if (htlcount > 4)
                                goto NikalLe;
                            goto FirSuruHoJa;
                        }
                    NikalLe: string ks = "Om Shanti";

                    }
                    catch (Exception exx)
                    {
                        //ExecptionLogger.FileHandling("GetGALHotelsPrice", "Error_007", exx, "Hotel");
                        AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Exception on search", Location = "" });
                        HotelDA.InsertHotelErrorLog(exx, "GetTGPaginationHotelsPrice");
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq, objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes, "TG", "HotelInsert");
                    }
                    objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq = PrevReq;
                    objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes = prevresp;
                }
                else
                {
                    #region Excetion Response Parsing
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq, objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes, "TG", "HotelInsert");
                    try
                    {
                        if (objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.Contains("<Errors>"))
                        {
                            XDocument document = XDocument.Parse(objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                            var Hotels_temps = (from Hotel in document.Descendants("Errors") select new { Errormsg = Hotel.Element("Error") }).ToList();
                            if (Hotels_temps.Count > 0)
                            {
                                if (Hotels_temps[0].Errormsg.Attribute("Type") != null && Hotels_temps[0].Errormsg.Attribute("ShortText") != null)
                                    AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = Hotels_temps[0].Errormsg.Attribute("ShortText").Value + "  " + Hotels_temps[0].Errormsg.Attribute("Type").Value });
                                else
                                    AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                            }
                            else
                                AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                        }
                        else
                        {
                            if (objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.Contains("<p>"))
                            {
                                string Serviceerror = objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.Substring(objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.IndexOf("<p>"), objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.IndexOf("</p>") - objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes.IndexOf("<p>") - 2);
                                AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = Serviceerror });
                            }
                            else
                                AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                        }
                    }
                    catch (Exception gd)
                    {
                        AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                    }
                    #endregion
                }
            }
            catch (Exception ex)
            {
                AllHotelList.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = ex.Message, Location = "" });
                HotelDA.InsertHotelErrorLog(ex, "GetTGPaginationHotelsPrice");
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", objALLcombindHotel.AllHotelSearch.TG_HotelSearchReq, objALLcombindHotel.AllHotelSearch.TG_HotelSearchRes, "TG", "HotelInsert");
            }
            objALLcombindHotel.TGHotelList = AllHotelList;
            return objALLcombindHotel;
        }
        protected List<HotelResult> OnlyHotelSearchAvailability(HotelSearch SearchDetails, DataTable HotelDt)
        {
            List<HotelResult> objHotellist = new List<HotelResult>();
            HotelDA objhtlDa = new HotelDA(); CommonHotelBL objcombal = new CommonHotelBL();
            HotelBAL.HotelMarkups objHtlMrk = new HotelBAL.HotelMarkups(); MarkupList MarkList = new MarkupList();
            try
            {
                if (SearchDetails.TG_HotelSearchRes.Contains("<Success />") || SearchDetails.TG_HotelSearchRes.Contains("<Success/>"))
                {
                    string responses = SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));

                    var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                      select new { BasicPropertyInfo = htlprice.Element("BasicPropertyInfo"), TPA_Extensions = htlprice.Element("TPA_Extensions") }).ToList();
                    if (hotelprice.Count > 0)
                    {
                        //string AllHotelCode = "";
                        //foreach (var hoteldetails in hotelprice)
                        //{
                        //    AllHotelCode += "'" + hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value + "'";
                        //}
                        ////AllHotelCode = AllHotelCode.Replace("''", "','");
                        //DataTable HtlServices = objhtlDa.GetHotelServices(AllHotelCode.Replace("''", "','"), "ALLproperty");
                        int k = 0;
                        foreach (var hoteldetails in hotelprice)
                        {
                            string HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                            DataRow[] HotelFilterArray = objHtlMrk.FilterHotelDT(HotelDt, HotelCode);
                            if (HotelFilterArray.Length > 0)
                            {
                                var HotelsData = hoteldetails.BasicPropertyInfo.Parent;
                                if (HotelsData.Element("RoomRates").Element("RoomRate") != null && HotelsData.Element("RatePlans").Element("RatePlan") != null)
                                {
                                    try
                                    {
                                        decimal baserate = 0, taxs = 0, disamt = 0, ExtraGuest = 0, TotaldiscAmt = 0;
                                        foreach (var htldtl in HotelsData.Descendants("RoomRates").Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value))
                                        {
                                            foreach (var htl in htldtl.Descendants("Rates"))
                                            {
                                                baserate += Convert.ToDecimal(htl.Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value.Trim());
                                                if (htl.Element("Rate").Element("Discount") != null)
                                                    disamt += Convert.ToDecimal(htl.Element("Rate").Element("Discount").Attribute("AmountBeforeTax").Value.Trim());
                                            }
                                        }
                                        MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelFilterArray[0]["Hotel_Star"].ToString().Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "TG", (baserate + taxs + ExtraGuest - disamt) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), 0);
                                        if (disamt > 0)
                                            TotaldiscAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (baserate + taxs + ExtraGuest) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), 0);
                                        HotelRatePlan objRatePlans = new HotelRatePlan();
                                        objRatePlans = RatePlanlist(hoteldetails.BasicPropertyInfo, hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value);

                                        objHotellist.Add(new HotelResult
                                        {
                                            RatePlanCode = hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value,
                                            hotelDiscoutAmt = TotaldiscAmt,
                                            hotelPrice = MarkList.TotelAmt,
                                            AgtMrk = MarkList.AgentMrkAmt,
                                            DiscountMsg = objRatePlans.discountMsg,
                                            inclusions = objRatePlans.inclusions,
                                            RoomTypeCode = objRatePlans.RatePlanCode,
                                            HotelCode = HotelCode,
                                            HotelName = HotelFilterArray[0]["VendorName"].ToString().Trim().Replace("'", ""),
                                            HotelCityCode = SearchDetails.SearchCityCode,
                                            HotelCity = HotelFilterArray[0]["City"].ToString().Trim(),
                                            StarRating = HotelFilterArray[0]["Hotel_Star"].ToString().Trim(),
                                            HotelAddress = HotelFilterArray[0]["Address"].ToString().Trim(),
                                            HotelDescription = "",//"<strong>Description</strong>: " + HotelFilterArray[0]["HotelOverview"].ToString(),
                                            HotelThumbnailImg = HotelFilterArray[0]["ImagePath"].ToString().Trim().Replace("_TN", String.Empty),
                                            Lati_Longi = HotelFilterArray[0]["Latitude"].ToString().Trim() + "##" + HotelFilterArray[0]["Longitude"].ToString().Trim(),
                                            Location = HotelFilterArray[0]["Location"].ToString().Trim(),

                                            HotelServices = SetHotelService_Image(HotelCode),
                                            ReviewRating = HotelFilterArray[0]["ReviewRating"].ToString().Trim() + "#" + HotelFilterArray[0]["ReviewCount"].ToString(),
                                            Provider = "TG",
                                            PopulerId = k++
                                        });
                                    }
                                    catch (Exception ex)
                                    {
                                        HotelDA.InsertHotelErrorLog(ex, "GetTGResult_List-" + HotelCode);
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found,  Please modify your search.. " });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                    }
                }
                else
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                    try
                    {
                        if (SearchDetails.TG_HotelSearchRes.Contains("<Errors>"))
                        {
                            XDocument document = XDocument.Parse(SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                            var Hotels_temps = (from Hotel in document.Descendants("Errors") select new { Errormsg = Hotel.Element("Error") }).ToList();
                            if (Hotels_temps.Count > 0)
                            {
                                if (Hotels_temps[0].Errormsg.Attribute("Type") != null && Hotels_temps[0].Errormsg.Attribute("ShortText") != null)
                                    objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = Hotels_temps[0].Errormsg.Attribute("ShortText").Value + "  " + Hotels_temps[0].Errormsg.Attribute("Type").Value });
                                else
                                    objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                            }
                            else
                                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                        }
                        else
                        {
                            if (SearchDetails.TG_HotelSearchRes.Contains("<p>"))
                            {
                                string Serviceerror = SearchDetails.TG_HotelSearchRes.Substring(SearchDetails.TG_HotelSearchRes.IndexOf("<p>"), SearchDetails.TG_HotelSearchRes.IndexOf("</p>") - SearchDetails.TG_HotelSearchRes.IndexOf("<p>") - 2);
                                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = Serviceerror });
                            }
                            else
                                objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                        }
                    }
                    catch (Exception gd)
                    {
                        objHotellist.Add(new HotelResult { HotelName = " ", Provider = "TG", hotelPrice = 0, HtlError = "Hotel not found for given search criteria. Please modify your search" });
                    }
                }
            }
            catch (Exception ex)
            {
                objHotellist.Add(new HotelResult { HotelName = " ", hotelPrice = 0, Provider = "TG", HtlError = ex.Message, Location = "" });
                HotelDA.InsertHotelErrorLog(ex, "OnlyHotelSearchAvailability");
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
            }
            return objHotellist;
        }

        protected HotelRatePlan RatePlanlist(XElement hoteldetails, string rateplan)
        {
            HotelRatePlan objRatePlans = new HotelRatePlan();
            try
            {
                foreach (var htldtl in hoteldetails.Parent.Descendants("RatePlans").Elements("RatePlan").Where(x => x.Attribute("RatePlanCode").Value == rateplan))
                {
                    string discount = "";
                    foreach (var htl in htldtl.Descendants("RatePlanDescription").Elements("Text"))
                    {
                        discount += htl.Value;
                    }
                    string inclusions = "";
                    foreach (var htl in htldtl.Descendants("RatePlanInclusions").Descendants("RatePlanInclusionDesciption").Elements("Text"))
                    {
                        inclusions += htl.Value;
                    }
                    objRatePlans.RatePlanCode = htldtl.Attribute("RatePlanCode").Value;
                    objRatePlans.discountMsg = discount;
                    objRatePlans.inclusions = inclusions;
                }
            }
            catch (Exception ex)
            { HotelDA.InsertHotelErrorLog(ex, "RatePlanlist"); }
            return objRatePlans;
        }
        public string TGCancellationPolicy(HotelSearch SearchDetails)
        {
            string Policy = "";
            try
            {
                List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
                CredencialsListnew = SearchDetails.CredencialsList.Where(x => x.HotelProvider == "TG" && (x.HotelTrip == SearchDetails.HtlType || x.HotelTrip == "ALL")).ToList();

                SearchDetails = TGHotels(SearchDetails, CredencialsListnew);
                if (SearchDetails.TG_HotelSearchRes.Contains("<Success />") || SearchDetails.TG_HotelSearchRes.Contains("<Success/>"))
                {
                    string responses = SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);

                    var hotelPolicy = (from htlPolicy in xmlresp.Descendants("RoomStays").Descendants("RoomStay").Descendants("RatePlans").Descendants("RatePlan")
                                       where htlPolicy.Attribute("RatePlanCode").Value == SearchDetails.HtlRoomCode
                                       select new { RoomName = htlPolicy.Attribute("RatePlanName"), Cancellation = htlPolicy.Element("CancelPenalties").Element("CancelPenalty") }).ToList();

                    if (hotelPolicy.Count > 0)
                    {
                        //Policy = "<Div><span style='font-weight: bold;font-style:normal;font-size:13px;'>Cancellation Policy</span>";
                        foreach (var hoteldetails in hotelPolicy)
                        {
                            foreach (var hotelpoly in hoteldetails.Cancellation.Descendants("PenaltyDescription").Where(x => x.Attribute("Name").Value != "FREE_CANCELLATION"))
                            {
                                Policy += "<li style='margin:4px 0 4px 0;'>" + hotelpoly.Element("Text").Value + "</li>";
                            }
                        }
                        //Policy += "</Div>";
                        if (Policy == "")
                        {
                            if (hotelPolicy.ElementAt(0).Cancellation.Attribute("NonRefundable").Value == "true")
                                Policy += "<li style='margin:4px 0 4px 0;'>Non Refundable Hotel.</li>";
                        }
                    }
                    else
                        Policy += "<li style='margin:4px 0 4px 0;'>Cancellation policy not availbale.</li>";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGCancellationPolicy");
                return ex.Message;
            }
            return Policy;
        }
        protected RoomComposite GetTGRoomList(HotelSearch SearchDetails)
        {
            List<RoomList> objRoomList = new List<RoomList>(); SelectedHotel HotelDetail = new SelectedHotel();
            RoomComposite objRoomDetals = new RoomComposite(); HotelMarkups objHtlMrk = new HotelMarkups(); HotelDA objhtlDa = new HotelDA();
            try
            {
                if (SearchDetails.TG_HotelSearchRes.Contains("<Success />") || SearchDetails.TG_HotelSearchRes.Contains("<Success/>"))
                {
                    XDocument xmlresp = XDocument.Parse(SearchDetails.TG_HotelSearchRes.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                    var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                      select new
                                      {
                                          RoomTypes = htlprice.Element("RoomTypes"),
                                          RatePlans = htlprice.Element("RatePlans"),
                                          RoomRates = htlprice.Element("RoomRates"),
                                          BasicPropertyInfo = htlprice.Element("BasicPropertyInfo"),
                                          TPA_Extensions = htlprice.Element("TPA_Extensions")
                                      }).ToList();
                    if (hotelprice.Count > 0)
                    {
                        foreach (var hoteldetails in hotelprice)
                        {
                            #region Hotel Details
                            try
                            {
                                HotelDetail.HotelName = hoteldetails.BasicPropertyInfo.Attribute("HotelName").Value;
                                HotelDetail.HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                                HotelDetail.StarRating = hoteldetails.BasicPropertyInfo.Element("Award").Attribute("Rating").Value;

                                string address = hoteldetails.BasicPropertyInfo.Element("Address").Element("AddressLine").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName") != null)
                                    address += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CityName").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("StateProv") != null)
                                    address += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("StateProv").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("CountryName") != null)
                                    address += ", " + hoteldetails.BasicPropertyInfo.Element("Address").Element("CountryName").Value;
                                if (hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode") != null)
                                    address += " - " + hoteldetails.BasicPropertyInfo.Element("Address").Element("PostalCode").Value;
                                HotelDetail.HotelAddress = address;

                                try
                                {
                                    HotelDetail.Lati_Longi = "";
                                    if (hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Latitude") != null)
                                        HotelDetail.Lati_Longi = hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Latitude").Value + "," + hoteldetails.BasicPropertyInfo.Element("Position").Attribute("Longitude").Value;

                                    HotelDetail.ThumbnailUrl = SearchDetails.BaseURL + "/Hotel/Images/NoImage.jpg";
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Multimedia").Attribute("ThumbnailUrl") != null)
                                        HotelDetail.ThumbnailUrl = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Multimedia").Attribute("ThumbnailUrl").Value.Replace("_TN", String.Empty);

                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("AmenityDescription") != null)
                                        HotelDetail.AmenityDescription = "<strong>DESCRIPTION: </strong>" + hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("AmenityDescription").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Description") != null)
                                        HotelDetail.HotelDescription = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Description").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Area") != null)
                                        HotelDetail.Location = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("Area").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckInTime") != null)
                                        HotelDetail.CheckInTime = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckInTime").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckOutTime") != null)
                                        HotelDetail.CheckOutTime = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("CheckOutTime").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("NumberOfRooms") != null)
                                        HotelDetail.NumberOfRooms = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("NumberOfRooms").Value;
                                    if (hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("isFlexibleCheckIn") != null)
                                        HotelDetail.FlexibleCheckIn = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Attribute("isFlexibleCheckIn").Value;
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals basic " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                                }
                                //HotelDetail.ReviewComment = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewComment").Value;
                                //HotelDetail.ReviewPostDate = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewPostDate").Value;
                                //HotelDetail.ReviewRating = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewRating").Value;
                                //HotelDetail.ReviewerName = hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Reviews").Attribute("ReviewerName").Value;

                                string imgdiv = "", Attract = "", RoomFacilty = "", HotelFacilty = "";
                                try
                                {
                                    int im = 0;

                                    foreach (var HtlImg in hoteldetails.TPA_Extensions.Element("HotelBasicInformation").Element("Multimedia").Element("ImageJSON").Elements("ImagesList"))
                                    {
                                        im++;
                                        imgdiv += "<img id='img" + im + "' src='" + HtlImg.Element("LargeImageObj").Attribute("url").Value + "' onmouseover='return ShowHtlImg(this);' alt='' title='" + HtlImg.Element("LargeImageObj").Attribute("category").Value + "' class='imageHtlDetailsshow' />";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals Image " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                                }
                                HotelDetail.HotelImage = imgdiv;
                                // Distance from 
                                foreach (var POI in hoteldetails.TPA_Extensions.Descendants("HotelBasicInformation").Descendants("POI").Elements("HotelPOI"))
                                {
                                    Attract += "<div class='check'>  Distance from " + POI.Attribute("POIName").Value + " : Approx. " + POI.Attribute("POIDistance").Value + " km</div>";
                                }
                                HotelDetail.Attraction = Attract;
                                try
                                {
                                    foreach (var Amenities in hoteldetails.TPA_Extensions.Descendants("HotelBasicInformation").Descendants("Amenities").Elements("PropertyAmenities"))
                                    {
                                        RoomFacilty += "<div class='check1'>" + Amenities.Attribute("description").Value + "</div>";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals Room Facility " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                                }
                                HotelDetail.HotelAmenities = RoomFacilty;
                                try
                                {
                                    foreach (var Amenities in hoteldetails.TPA_Extensions.Descendants("HotelBasicInformation").Descendants("Amenities").Elements("RoomAmenities"))
                                    {
                                        HotelFacilty += "<div class='check1'>" + Amenities.Attribute("description").Value + "</div>";
                                    }
                                }
                                catch (Exception ex)
                                {
                                    HotelDA.InsertHotelErrorLog(ex, "Hotel Detals Hotel Facility " + HotelDetail.HotelName + "" + SearchDetails.SearchCity);
                                }
                                HotelDetail.RoomAmenities = HotelFacilty;
                            }
                            catch (Exception ex)
                            {
                                HotelDA.InsertHotelErrorLog(ex, "Hotel Detals " + HotelDetail.HotelName + "_" + SearchDetails.SearchCity);
                            }
                            #endregion
                            #region Room Details
                            try
                            {
                                // DataTable roomdt = objhtlDa.GetRoomDiscription(HotelDetail.HotelCode);
                                foreach (var rateplane in hoteldetails.RatePlans.Elements("RatePlan"))
                                {
                                    string discountmsg = "", inclusions = "";

                                    //foreach (var htl in rateplane.Descendants("RatePlanInclusions").Descendants("RatePlanInclusionDesciption").Elements("Text"))
                                    //{
                                    //    if (htl.Value.Trim() != "")
                                    //        inclusions += htl.Value.Trim() + "  ";
                                    //}
                                    if (rateplane.Element("RatePlanDescription") != null)
                                    {
                                        foreach (var htl in rateplane.Descendants("RatePlanDescription").Elements("Text"))
                                        {
                                            discountmsg += htl.Value.Trim() + " ";
                                        }
                                    }
                                    if (rateplane.Element("RatePlanInclusions") != null)
                                    {
                                        if (rateplane.Element("RatePlanInclusions").Element("Text") != null)
                                            if (rateplane.Element("RatePlanInclusions").Element("Text").Value != "")
                                                inclusions = rateplane.Element("RatePlanInclusions").Element("Text").Value.Trim() + "  ";
                                    }
                                    bool IsHold = false; DateTime HoldTillDate = Convert.ToDateTime(SearchDetails.CheckInDate).AddDays(-2);
                                    string Policy = "<div>";
                                    try
                                    {
                                        foreach (var hotelpoly in rateplane.Descendants("CancelPenalties").Descendants("CancelPenalty").Elements("PenaltyDescription").Where(x => x.Attribute("Name").Value != "FREE_CANCELLATION"))
                                        {
                                            if (hotelpoly.Element("Text").Value.Trim() != "Y")
                                                Policy += "<li style='margin:4px 0 4px 0;'>" + hotelpoly.Element("Text").Value.Trim() + "</li>";
                                        }

                                        foreach (var hotelpoly in rateplane.Element("CancelPenalties").Elements("CancelPenalty").Where(x => x.Element("AmountPercent") != null && x.Element("Deadline") != null))
                                        {
                                            if (hotelpoly.Element("Deadline").Attribute("OffsetUnitMultiplier") != null && hotelpoly.Element("AmountPercent").Attribute("NmbrOfNights") != null)
                                            {
                                                if (Convert.ToInt32(hotelpoly.Element("Deadline").Attribute("OffsetUnitMultiplier").Value) > 48 && Convert.ToInt32(hotelpoly.Element("AmountPercent").Attribute("NmbrOfNights").Value) == 0)
                                                    IsHold = true;
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        HotelDA.InsertHotelErrorLog(ex, "GetTGRoomList CancellationPlicy");
                                    }
                                    RoomRate objRoomRate = new RoomRate();
                                    SearchDetails.StarRating = HotelDetail.StarRating;
                                    objRoomRate = RoomRatelist(hoteldetails.RoomRates, rateplane.Attribute("RatePlanCode").Value, SearchDetails);

                                    RoomType objRoomType = new RoomType();
                                    objRoomType = RoomTypelist(hoteldetails.RoomTypes, objRoomRate.RoomTypeCode);

                                    objRoomList.Add(new RoomList
                                    {
                                        HotelCode = HotelDetail.HotelCode,
                                        RatePlanCode = rateplane.Attribute("RatePlanCode").Value,
                                        RoomTypeCode = objRoomType.RoomTypeCode,
                                        RoomName = rateplane.Attribute("RatePlanName").Value,
                                        discountMsg = discountmsg,
                                        DiscountAMT = objRoomRate.DiscountAMT,
                                        Total_Org_Roomrate = objRoomRate.Total_Org_Roomrate,
                                        TotalRoomrate = objRoomRate.TotalRoomrate,
                                        AdminMarkupPer = objRoomRate.AdminMarkupPer,
                                        AdminMarkupAmt = objRoomRate.AdminMarkupAmt,
                                        AdminMarkupType = objRoomRate.AdminMarkupType,
                                        AgentMarkupPer = objRoomRate.AgentMarkupPer,
                                        AgentMarkupAmt = objRoomRate.AgentMarkupAmt,
                                        AgentMarkupType = objRoomRate.AgentMarkupType,
                                        AgentServiseTaxAmt = objRoomRate.ServiseTaxAmt,
                                        V_ServiseTaxAmt = objRoomRate.V_ServiseTaxAmt,
                                        AmountBeforeTax = objRoomRate.AmountBeforeTax,
                                        Taxes = objRoomRate.Taxes,
                                        MrkTaxes = objRoomRate.MrkTaxes,
                                        ExtraGuest_Charge = objRoomRate.ExtraGuest_Charge,
                                        Smoking = objRoomType.Smoking,
                                        inclusions = inclusions,
                                        CancelationPolicy = Policy + "</div>",
                                        OrgRateBreakups = objRoomRate.Org_RoomrateBreakups,
                                        MrkRateBreakups = objRoomRate.Mrk_RoomrateBreakups,
                                        DiscRoomrateBreakups = objRoomRate.Disc_RoomrateBreakups,
                                        EssentialInformation = "",
                                        RoomDescription = objRoomType.RoomDescription,
                                        Provider = "TG",
                                        RoomImage = objRoomType.RoomImage.Trim() != "" ? objRoomType.RoomImage.Trim() : HotelDetail.ThumbnailUrl,
                                        AdminCommissionAmt = objRoomRate.AdminCommissionAmt,
                                        GSTPercentage = objRoomRate.GSTPercentage,
                                        TDSPercentage = objRoomRate.TDSPercentage,
                                        RetaintionPer = objRoomRate.RetaintionPer,
                                        MinCapvalue = objRoomRate.MinCapvalue,
                                        AgentCommissionAmt = objRoomRate.AgentCommissionAmt,
                                        GSTAmt = objRoomRate.GSTAmt,
                                        TDSAmt = objRoomRate.TDSAmt,
                                        SupplierCommissionAmt = objRoomRate.SupplierCommissionAmt,
                                        SupplierCommissionPer = objRoomRate.SupplierCommissionPer,
                                        SupplierCommisionTaxIncluded = objRoomRate.SupplierCommisionTaxIncluded,
                                        RoomRateKey = "",
                                        IsHold = IsHold,
                                        HoldTillDate = HoldTillDate
                                        //maxAdult = objRoomType.maxAdult,
                                        //maxChild = objRoomType.maxChild,
                                        //maxGuest = objRoomType.maxGuest,
                                    });
                                }
                            }
                            catch (Exception ex)
                            {
                                HotelDA.InsertHotelErrorLog(ex, "ADDRoomList");
                                objRoomList.Add(new RoomList
                                {
                                    TotalRoomrate = 0,
                                    Room_Error = ex.Message
                                });
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        objRoomList.Add(new RoomList
                        {
                            TotalRoomrate = 0,
                            Room_Error = "Room Details Not found"
                        });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                    }
                }
                else
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");

                    string XmlRess = SearchDetails.TG_HotelSearchRes.Replace("xmlns='urn:Hotel_Search' xmlns:xs='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' PrimaryLangID='en-us' AltLangID='en-us'", "");
                    XDocument document = XDocument.Parse(XmlRess);
                    var Hotels_temps = (from Hotel in document.Descendants("Error") select new { Errormsg = Hotel }).ToList();

                    objRoomList.Add(new RoomList
                    {
                        TotalRoomrate = 0,
                        Room_Error = Hotels_temps[0].Errormsg.Value
                    });
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GetTGRoomList");
                objRoomList.Add(new RoomList
                {
                    TotalRoomrate = 0,
                    Room_Error = ex.Message
                });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
            }
            objRoomDetals.SelectedHotelDetail = HotelDetail;
            objRoomDetals.RoomDetails = objRoomList;
            return objRoomDetals;
        }
        protected RoomType RoomTypelist(XElement hoteldetails, string RoomTypeCode)
        {
            RoomType objRoomType = new RoomType();
            try
            {
                foreach (var htldtl in hoteldetails.Elements("RoomType").Where(x => x.Attribute("RoomTypeCode").Value == RoomTypeCode))
                {
                    objRoomType.RoomTypeName = htldtl.Attribute("RoomType").Value;
                    objRoomType.RoomTypeCode = htldtl.Attribute("RoomTypeCode").Value;
                    objRoomType.Smoking = htldtl.Attribute("NonSmoking").Value;
                    objRoomType.RoomDescription = htldtl.Element("RoomDescription").Element("Text").Value;
                    if (htldtl.Element("RoomDescription").Element("Image") != null)
                        objRoomType.RoomImage = htldtl.Element("RoomDescription").Element("Image").Value.Replace("_TN", String.Empty);

                    objRoomType.maxAdult = "";// htldtl.Element("TPA_Extensions").Element("RoomType").Attribute("maxAdult").Value;
                    objRoomType.maxChild = "";//htldtl.Element("TPA_Extensions").Element("RoomType").Attribute("maxChild").Value;
                    objRoomType.maxGuest = "";//htldtl.Element("TPA_Extensions").Element("RoomType").Attribute("maxGuest").Value;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "RoomTypelist_" + RoomTypeCode);
            }
            return objRoomType;
        }
        protected RoomRate RoomRatelist(XElement hoteldetails, string RoomPlanCode, HotelSearch SearchDetails)
        {
            RoomRate objRoomRate = new RoomRate(); HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            MarkupList TaxMarkList = new MarkupList(); MarkupList ExtraMarkList = new MarkupList(); HotelDA objhtlDa = new HotelDA();
            try
            {
                string Org_RoomRateStr = "", MrkRoomrateStr = "", Disc_RoomrateStr = "", RoomTypeCode = "", taxinfos = "";
                decimal AmountBeforeTax = 0, MrkTotalPrice = 0, TotaldiscAmt = 0, adminMrkAmt = 0, AgtMrkAmt = 0, SericeTaxAmt = 0, VSericeTaxAmt = 0, TotalTaxes = 0, MrkTaxes = 0, MrkExtraGuest = 0, TotalSupplierCommissionAmt = 0;
                int rm = 0;
                foreach (var roomrate in hoteldetails.Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == RoomPlanCode))
                {
                    string taxinf = "";
                    if (rm > 0)
                    {
                        taxinf += "/";
                        MrkRoomrateStr += "/";
                        Org_RoomRateStr += "/";
                        Disc_RoomrateStr += "/";
                    }
                    rm++;

                    decimal Basefare = 0, SupplierCommissionAmount = 0, Taxes = 0, ExtraGuesttax = 0, disamt_ForBaseRate = 0, disamt_ForAdditionalGuest = 0, TotalDiscount = 0;
                    RoomTypeCode = roomrate.Attribute("RoomID").Value;
                    Basefare = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value);
                    Taxes = Convert.ToDecimal(roomrate.Element("Rates").Element("Rate").Element("Base").Element("Taxes").Attribute("Amount").Value);
                    try
                    {
                        foreach (var TaxeAmtsString in roomrate.Element("Rates").Element("Rate").Element("Base").Element("Taxes").Elements("Tax"))
                        {
                            taxinf += TaxeAmtsString.Attribute("Code").Value + ":" + TaxeAmtsString.Attribute("Amount").Value + ",";
                        }
                        taxinfos += taxinf;
                    }
                    catch (Exception tx)
                    {
                        HotelDA.InsertHotelErrorLog(tx, "RoomRatelist_tax" + RoomPlanCode + "_" + SearchDetails.HtlCode);
                    }
                    if (roomrate.Element("Rates").Element("Rate").Element("AdditionalGuestAmounts") != null)
                    {
                        decimal extrarate = 0;
                        foreach (var addigust in roomrate.Elements("Rates").Elements("Rate").Elements("AdditionalGuestAmounts").Elements("AdditionalGuestAmount"))
                        {
                            extrarate += Convert.ToDecimal(addigust.Element("Amount").Attribute("AmountBeforeTax").Value.Trim());
                        }
                        ExtraGuesttax = extrarate;
                    }
                    try
                    {
                        IEnumerable<XElement> Discountss = roomrate.Element("Rates").Element("Rate").Elements("Discount");
                        if (Discountss != null)
                        {
                            foreach (var Discount in Discountss)
                            {
                                if (Discount.Attribute("AppliesTo").Value == "Base")
                                    disamt_ForBaseRate += Convert.ToDecimal(Discount.Attribute("AmountBeforeTax").Value);
                                if (Discount.Attribute("AppliesTo").Value == "AdditionalGuestAmount")
                                    disamt_ForAdditionalGuest += Convert.ToDecimal(Discount.Attribute("AmountBeforeTax").Value);
                            }
                            TotalDiscount = disamt_ForBaseRate + disamt_ForAdditionalGuest;
                        }
                    }
                    catch (Exception xxx)
                    {
                        HotelDA.InsertHotelErrorLog(xxx, "RoomRatelist_Discount_Calculation");
                    }

                    Org_RoomRateStr += "Bs:" + Basefare + "-Tx:" + Taxes + "-EG:" + ExtraGuesttax + "-Ds" + disamt_ForBaseRate + "-DsT" + disamt_ForAdditionalGuest;

                    AmountBeforeTax += Basefare + ExtraGuesttax - disamt_ForBaseRate - disamt_ForAdditionalGuest;
                    TotalTaxes += Taxes;

                    MarkList = objHtlMrk.MarkupCalculationPerRoom(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "TG", (Basefare - disamt_ForBaseRate), 0, SearchDetails.NoofRoom);
                    TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Taxes, 0);
                    ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, ExtraGuesttax - disamt_ForAdditionalGuest, 0);

                    MrkTotalPrice += MarkList.TotelAmt + TaxMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                    MrkTaxes += TaxMarkList.TotelAmt;
                    MrkExtraGuest += ExtraMarkList.TotelAmt;
                    adminMrkAmt += MarkList.AdminMrkAmt + TaxMarkList.AdminMrkAmt + ExtraMarkList.AdminMrkAmt;
                    AgtMrkAmt += MarkList.AgentMrkAmt + TaxMarkList.AgentMrkAmt + ExtraMarkList.AgentMrkAmt;
                    SericeTaxAmt += MarkList.AgentServiceTaxAmt + TaxMarkList.AgentServiceTaxAmt + ExtraMarkList.AgentServiceTaxAmt;
                    VSericeTaxAmt += MarkList.VenderServiceTaxAmt + TaxMarkList.VenderServiceTaxAmt + ExtraMarkList.VenderServiceTaxAmt;
                    MrkRoomrateStr += MarkList.TotelAmt.ToString();

                    decimal MrkDiscAmt = 0;
                    if (TotalDiscount > 0)
                    {
                        MrkDiscAmt = objHtlMrk.DiscountDesiyaRoomMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (Basefare + ExtraGuesttax), 0, SearchDetails.NoofRoom);
                        TotaldiscAmt += MrkDiscAmt + TaxMarkList.TotelAmt + ExtraMarkList.TotelAmt;
                    }
                    Disc_RoomrateStr += MrkDiscAmt.ToString();

                    #region Commission calculation
                    try
                    {
                        XElement TPA_Extensions = roomrate.Element("Rates").Element("Rate").Element("TPA_Extensions");
                        if (TPA_Extensions != null)
                            if (TPA_Extensions.Element("AffiliateCommission") != null)
                            {
                                SupplierCommissionAmount = TPA_Extensions.Element("AffiliateCommission").Attribute("Amount") != null ? Convert.ToDecimal(TPA_Extensions.Element("AffiliateCommission").Attribute("Amount").Value.Trim()) : 0;
                                objRoomRate.SupplierCommissionPer = TPA_Extensions.Element("AffiliateCommission").Attribute("Percent") != null ? TPA_Extensions.Element("AffiliateCommission").Attribute("Percent").Value : "";
                                objRoomRate.SupplierCommisionTaxIncluded = TPA_Extensions.Element("AffiliateCommission").Attribute("HotelTaxIncluded") != null ? TPA_Extensions.Element("AffiliateCommission").Attribute("HotelTaxIncluded").Value : "";
                            }
                        TotalSupplierCommissionAmt += SupplierCommissionAmount;
                    }
                    catch (Exception exco)
                    {
                        HotelDA.InsertHotelErrorLog(exco, "RoomRatelist_Discount_Commission");
                    }
                    #endregion
                }

                objRoomRate.TotalRoomrate = MrkTotalPrice;
                if (TotalSupplierCommissionAmt > 0)
                {
                    DataSet CommissionDs = new DataSet();
                    CommissionDs = objhtlDa.GetCommisionInRoomDetails("TG", SearchDetails.StarRating.Trim(), SearchDetails.AgentID, TotalSupplierCommissionAmt, MrkTotalPrice);

                    if (CommissionDs.Tables.Count > 0)
                        if (CommissionDs.Tables[0].Rows.Count > 0)
                        {
                            objRoomRate.AdminCommissionAmt = Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["AdminCommissionAmt"]);
                            objRoomRate.GSTPercentage = Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["GSTPer"]);
                            objRoomRate.TDSPercentage = CommissionDs.Tables[0].Rows[0]["TDSPer"].ToString() != "" ? Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["TDSPer"]) : 0;
                            objRoomRate.RetaintionPer = Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["RetaintionPer"]);
                            objRoomRate.MinCapvalue = Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["MinCapvalue"]);
                            objRoomRate.GSTAmt = Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["GSTAmt"]);
                            objRoomRate.TDSAmt = Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["TDSAmt"]);
                            objRoomRate.AgentCommissionAmt = Convert.ToDecimal(CommissionDs.Tables[0].Rows[0]["AgentCommissionAmt"]);
                            objRoomRate.SupplierCommissionAmt = TotalSupplierCommissionAmt;
                        }
                }
                objRoomRate.RoomTypeCode = RoomTypeCode;
                objRoomRate.RatePlanCode = RoomPlanCode;

                objRoomRate.Total_Org_Roomrate = (AmountBeforeTax + TotalTaxes);
                objRoomRate.TotalRoomrate = MrkTotalPrice;
                objRoomRate.DiscountAMT = TotaldiscAmt;
                objRoomRate.AmountBeforeTax = AmountBeforeTax;
                objRoomRate.Taxes = TotalTaxes;
                objRoomRate.ExtraGuest_Charge = MrkExtraGuest;
                objRoomRate.MrkTaxes = MrkTaxes;
                objRoomRate.Org_RoomrateBreakups = Org_RoomRateStr;
                objRoomRate.Mrk_RoomrateBreakups = MrkRoomrateStr;
                objRoomRate.Disc_RoomrateBreakups = Disc_RoomrateStr;
                objRoomRate.AdminMarkupPer = MarkList.AdminMrkPercent;
                objRoomRate.AdminMarkupAmt = adminMrkAmt;
                objRoomRate.AdminMarkupType = MarkList.AdminMrkType;
                objRoomRate.AgentMarkupPer = MarkList.AgentMrkPercent;
                objRoomRate.AgentMarkupAmt = AgtMrkAmt;
                objRoomRate.AgentMarkupType = MarkList.AgentMrkType;
                objRoomRate.ServiseTaxAmt = SericeTaxAmt;
                objRoomRate.V_ServiseTaxAmt = VSericeTaxAmt;
                objRoomRate.TaxInfo = taxinfos;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "RoomRatelist_" + RoomPlanCode + "_" + SearchDetails.HtlCode);
            }
            return objRoomRate;
        }

        public HotelBooking TGHotelsPreBooking(HotelBooking HotelDetail)
        {
            HotelDetail.ProBookingID = "Not Available";
            try
            {
                if (HotelDetail.HotelUrl != null)
                {
                    HotelDetail = ProvosinalBookingRequest(HotelDetail);
                    HotelDetail.ProBookingRes = TGPostXml(HotelDetail.HotelBookingUrl, HotelDetail.ProBookingReq);

                    if (HotelDetail.ProBookingRes.Contains("<Success />") || HotelDetail.ProBookingRes.Contains("<Success/>"))
                    {
                        string proresp = HotelDetail.ProBookingRes.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                        XDocument xmlresp = XDocument.Parse(proresp);

                        var hotelprice = (from htlprice in xmlresp.Descendants("HotelReservation").Descendants("UniqueID")
                                          select new { proid = htlprice.Attribute("ID").Value }).ToList();

                        HotelDetail.ProBookingID = hotelprice[0].proid;
                    }
                    else
                    {
                        HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                    }
                }
                else
                {
                    HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelsPreBooking");
                HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
            }
            return HotelDetail;
        }
        public HotelBooking TGHotelsBooking(HotelBooking SearchDetails)
        {
            try
            {
                SearchDetails = BookingRequest(SearchDetails);
                SearchDetails.BookingConfRes = TGPostXml(SearchDetails.HotelBookingUrl, SearchDetails.BookingConfReq);

                if (SearchDetails.BookingConfRes.Contains("<Success />") || SearchDetails.BookingConfRes.Contains("<Success/>"))
                {
                    XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");

                    string proresp = SearchDetails.BookingConfRes.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(proresp);
                    XElement htlbookings = xmlresp.Element(soap + "Envelope").Element(soap + "Body").Element("OTA_HotelResRS").Element("HotelReservations");
                    //var Bookingss = (from booking in xmlresp.Descendants("HotelReservation")
                    //                 select new
                    //                 {
                    //                     Bookingid = booking.Element("UniqueID"),
                    //                     Contact = booking.Element("RoomStays").Element("RoomStay").Element("BasicPropertyInfo").Element("ContactNumbers")
                    //                 }).ToList();

                    // foreach (var htls in Bookingss)
                    if (htlbookings != null)
                    {
                        XElement Bookingss = htlbookings.Element("HotelReservation");
                        if (Bookingss != null)
                        {

                            SearchDetails.BookingID = Bookingss.Element("UniqueID").Attribute("ID").Value;
                            SearchDetails.Status = HotelStatus.Confirm.ToString();

                            XElement ContactNumbers = Bookingss.Element("RoomStays").Element("RoomStay").Element("BasicPropertyInfo").Element("ContactNumbers");
                            string contactno = ""; int n = 0;
                            if (ContactNumbers != null)
                            {
                                foreach (var contactdtl in ContactNumbers.Elements("ContactNumber"))
                                {
                                    try
                                    {
                                        if (n > 0)
                                            contactno += " / ";
                                        if (contactdtl.Attribute("CountryAccessCode") != null)
                                            if (contactdtl.Attribute("CountryAccessCode").Value.Length > 1)
                                                contactno += contactdtl.Attribute("CountryAccessCode").Value + " -";

                                        if (contactdtl.Attribute("AreaCityCode") != null)
                                            if (contactdtl.Attribute("AreaCityCode").Value.Length > 1)
                                                contactno += contactdtl.Attribute("AreaCityCode").Value + "  ";

                                        if (contactdtl.Attribute("PhoneNumber").Value != null)
                                            if (contactdtl.Attribute("PhoneNumber").Value.Length > 6)
                                                contactno += contactdtl.Attribute("PhoneNumber").Value;
                                        n++;
                                        //        if (contactdtl.Attribute("PhoneNumber").Value != "")
                                        // contactno += contactdtl.Attribute("CountryAccessCode").Value + " -" + contactdtl.Attribute("AreaCityCode").Value + "  " + contactdtl.Attribute("PhoneNumber").Value + "   /  ";
                                    }
                                    catch (Exception exx)
                                    {
                                        HotelDA.InsertHotelErrorLog(exx, "TGHotelsBooking PhoneNumber");
                                    }
                                }
                            }
                            SearchDetails.HotelContactNo = contactno;
                        }
                    }
                }
                else
                {
                    SearchDetails.HotelContactNo = "Exception"; SearchDetails.BookingID = "";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelsBooking");
                SearchDetails.HotelContactNo = ex.Message;
                SearchDetails.BookingID = "";
            }
            return SearchDetails;
        }

        public HotelCancellation TGHotelsCancelation(HotelCancellation HotelDetail)
        {
            HotelDetail.CancellationCharge = 0; HotelDetail.CancellationID = "";
            try
            {
                HotelDetail = CancellationRequest(HotelDetail);
                HotelDetail.BookingCancelRes = TGPostXml(HotelDetail.HotelBookingUrl, HotelDetail.BookingCancelReq);

                if (HotelDetail.BookingCancelRes.Contains("<Success />") || HotelDetail.BookingCancelRes.Contains("<Success/>"))
                {
                    string proresp = HotelDetail.BookingCancelRes.Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(proresp);

                    var hotelcancle = (from htlprice in xmlresp.Descendants("OTA_CancelRS") select new { Cancellation = htlprice }).ToList();
                    foreach (var Hotelcan in hotelcancle)
                    {
                        HotelDetail.CancelStatus = Hotelcan.Cancellation.Attribute("Status").Value;
                        HotelDetail.CancellationID = Hotelcan.Cancellation.Element("CancelInfoRS").Element("UniqueID").Attribute("ID").Value;
                        //HotelDetail.CancellationCharge = Convert.ToDecimal(Hotelcan.Cancellation.Element("CancelInfoRS").Element("CancelRules").Element("CancelRule").Attribute("Amount").Value);
                        HotelDetail.SupplierRefundAmt = Convert.ToDecimal(Hotelcan.Cancellation.Element("CancelInfoRS").Element("CancelRules").Element("CancelRule").Attribute("Amount").Value);
                        if (HotelDetail.SupplierRefundAmt == 0)
                            HotelDetail.CancellationCharge = HotelDetail.Total_Org_Roomrate;
                        else
                            HotelDetail.CancellationCharge = HotelDetail.Total_Org_Roomrate - HotelDetail.SupplierRefundAmt;
                    }
                }
                else
                { HotelDetail.CancellationCharge = 0; HotelDetail.SupplierRefundAmt = 0; }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "TGHotelsCancelation");
            }
            return HotelDetail;
        }

        protected string SetHotelService_Image(string HotelCode)
        {
            HotelDA objhtlDa = new HotelDA();
            string InclImg = ""; int i = 0, j = 0, k = 0, l = 0, m = 0, n = 0, p = 0, r = 0;
            try
            {
                DataTable HtlServices = objhtlDa.GetHotelServices(HotelCode, "property");
                if (HtlServices.Rows.Count == 0)
                    InclImg = "&nbsp;";
                foreach (DataRow Services in HtlServices.Rows)
                {
                    switch (Services["Amenity_id"].ToString().Trim())
                    {
                        //case "01":
                        //case "344":
                        //    if (n == 0)
                        //    {
                        //        InclImg += "<img src='../Hotel/Images/Facility/travel_desk.png' title='Tagency Help Desk' class='IconImageSize' /><span class='hide'>Travel & Transfers<span>";
                        //        n = 0;
                        //    }
                        //    break;
                        case "344":
                        case "01":
                        case "03":
                        case "04":
                            if (i == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Airport_transfer.png' title='Car rental facilities' class='IconImageSize' /><span class='hide'>Travel & Transfers</span>";
                                i = 1;
                            }
                            break;
                        //case "08":
                        //    InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Banquet hall' class='IconImageSize' />"; Travel & Transfers
                        //    break;
                        case "09":
                            InclImg += "<img src='../Hotel/Images/Facility/bar.png' title='Mini bar' class='IconImageSize' /><span class='hide'>Restaurant/Bar</span>";
                            break;
                        case "11":
                            InclImg += "<img src='../Hotel/Images/Facility/beauty.png' title='Beauty parlour' class='IconImageSize' /><span class='hide'>Beauty Porlour</span>";
                            break;
                        case "22":
                            InclImg += "<img src='../Hotel/Images/Facility/babysitting.png' title='Baby sitting' class='IconImageSize' /><span class='hide'>Baby Sitting</span>";
                            break;
                        case "08":
                        case "24":
                            if (p == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Banquet_hall.png' title='Business centre' class='IconImageSize' /><span class='hide'>Business Facilities</span>";
                                p = 1;
                            }
                            break;
                        case "26":
                            InclImg += "<img src='../Hotel/Images/Facility/Phone.png' title='Direct dial phone' class='IconImageSize' /><span class='hide'>Phone</span>";
                            break;
                        case "30":
                        case "32":
                            if (n == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/breakfast.png' title='Tea/Coffee' class='IconImageSize' /><span class='hide'>Tea/Coffee</span>";
                                n = 1;
                            }
                            break;
                        case "31":
                        case "44":
                        case "65":
                            if (j == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/lobby.png' title='Lobby' class='IconImageSize' /><span class='hide'>lobby</span>";
                                j = 1;
                            }
                            break;
                        case "40":
                            InclImg += "<img src='../Hotel/Images/Facility/elevator.png' title='Lifts' class='IconImageSize' /><span class='hide'>Lift</span>";
                            break;
                        case "52":
                            InclImg += "<img src='../Hotel/Images/Facility/health_club.png' title='Gym' class='IconImageSize' /><span class='hide'>Gym</span>";
                            break;
                        case "54":
                        case "55":
                        case "56":
                        case "57":
                        case "58":
                            if (k == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/wifi.gif' title='Internet/wifi' class='IconImageSize' /><span class='hide'>Internet/Wi-Fi</span>";
                                k = 1;
                            }
                            break;
                        case "59":
                            InclImg += "<img src='../Hotel/Images/Facility/laundary.png' title='Laundry services' class='IconImageSize' /><span class='hide'>Laundry Services</span>";
                            break;
                        case "71":
                        case "72":
                        case "73":
                        case "74":
                        case "75":
                            if (l == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/Parking.png' title='Parking' class='IconImageSize' /><span class='hide'>Parking</span>";
                                l = 1;
                            }
                            break;
                        case "88":
                            InclImg += "<img src='../Hotel/Images/Facility/sauna.png' title='Spa/Massage/Wellness' class='IconImageSize' /><span class='hide'>Spa/Massage/Wellness/Sauna</span>";
                            break;
                        case "122":
                        case "360":
                            if (r == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/AC.png' title='AC' class='IconImageSize' /><span class='hide'>AC</span>";
                                r = 1;
                            }
                            break;
                        case "126":
                        case "325":
                        case "131":
                            if (m == 0)
                            {
                                InclImg += "<img src='../Hotel/Images/Facility/TV.png' title='TV' class='IconImageSize' /><span class='hide'>TV</span>";
                                m = 1;
                            }
                            break;
                        case "334":
                            InclImg += "<img src='../Hotel/Images/Facility/handicap.png' title='Disabled facilities' class='IconImageSize' /><span class='hide'>Disabled Facilities</span>";
                            break;
                        case "338":
                            InclImg += "<img src='../Hotel/Images/Facility/golf.png' title='Golf' class='IconImageSize' /><span class='hide'>Sports</span>";
                            break;
                        case "345":
                            InclImg = "<img src='../Hotel/Images/Facility/swimming.png' title='Outdoor Swimming Pool' class='IconImageSize' /><span class='hide'>Swimming Pool</span>";
                            break;
                        case "355":
                            InclImg += "<img src='../Hotel/Images/Facility/jacuzzi.png' title='Indoor Swimming Pool' class='IconImageSize' /><span class='hide'>Tub Bath</span>";
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "SetHotelService_TG");
            }
            return InclImg;
        }

        public List<HotelResult> Promotion_Hotels(HotelSearch SearchDetails)
        {
            //HotelComposite obgHotelsList = new HotelComposite();
            List<HotelResult> objHotellist = new List<HotelResult>();
            try
            {
                List<HotelCredencials> CredencialsListnew = new List<HotelCredencials>();
                CredencialsListnew = SearchDetails.CredencialsList.Where(x => x.HotelProvider == "TG" && (x.HotelTrip == SearchDetails.HtlType || x.HotelTrip == "ALL")).ToList();

                SearchDetails = Promotion_HotelSearchRequest(SearchDetails, CredencialsListnew);
                SearchDetails.TG_HotelSearchRes = TGPostXml(CredencialsListnew[0].HotelUrl, SearchDetails.TG_HotelSearchReq);
                objHotellist = Promotion_GetTGResult(SearchDetails.TG_HotelSearchRes, SearchDetails);
                //obgHotelsList.HotelSearchDetail = SearchDetails;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "Promotion_Hotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                SearchDetails.TG_HotelSearchRes = ex.Message;
                objHotellist.Add(new HotelResult { HtlError = ex.Message });
            }
            return objHotellist;
        }
        protected List<HotelResult> Promotion_GetTGResult(string HotelResponse, HotelSearch SearchDetails)
        {
            List<HotelResult> objHotellist = new List<HotelResult>(); HotelDA objhtlDa = new HotelDA();
            HotelMarkups objHtlMrk = new HotelMarkups(); MarkupList MarkList = new MarkupList();
            try
            {
                if (HotelResponse.Contains("<Success />") || HotelResponse.Contains("<Success/>"))
                {
                    string responses = HotelResponse.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);

                    var hotelprice = (from htlprice in xmlresp.Descendants("RoomStays").Descendants("RoomStay")
                                      select new { BasicPropertyInfo = htlprice.Element("BasicPropertyInfo"), TPA_Extensions = htlprice.Element("TPA_Extensions") }).ToList();
                    if (hotelprice.Count > 0)
                    {
                        DataTable HotelDt = objhtlDa.GetHotelOverview(SearchDetails.SearchCity, "", "TG");
                        foreach (var hoteldetails in hotelprice)
                        {
                            string HotelCode = hoteldetails.BasicPropertyInfo.Attribute("HotelCode").Value;
                            DataRow[] HotelFilterArray = objHtlMrk.FilterHotelDT(HotelDt, HotelCode);
                            if (HotelFilterArray.Length > 0)
                            {
                                var HotelsData = hoteldetails.BasicPropertyInfo.Parent;
                                if (HotelsData.Element("RoomRates").Element("RoomRate") != null && HotelsData.Element("RatePlans").Element("RatePlan") != null)
                                {
                                    try
                                    {
                                        decimal baserate = 0, taxs = 0, disamt = 0, ExtraGuest = 0, TotaldiscAmt = 0;
                                        foreach (var htldtl in HotelsData.Descendants("RoomRates").Elements("RoomRate").Where(x => x.Attribute("RatePlanCode").Value == hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value))
                                        {
                                            foreach (var htl in htldtl.Descendants("Rates"))
                                            {
                                                baserate += Convert.ToDecimal(htl.Element("Rate").Element("Base").Attribute("AmountBeforeTax").Value.Trim());
                                                if (htl.Element("Rate").Element("Base").Element("Taxes") != null)
                                                    taxs += Convert.ToDecimal(htl.Element("Rate").Element("Base").Element("Taxes").Attribute("Amount").Value.Trim());

                                                if (htl.Element("Rate").Element("Discount") != null)
                                                    disamt += Convert.ToDecimal(htl.Element("Rate").Element("Discount").Attribute("AmountBeforeTax").Value.Trim());

                                                if (htl.Element("Rate").Element("AdditionalGuestAmounts") != null)
                                                {
                                                    decimal extrarate = 0;
                                                    foreach (var addigust in htl.Elements("Rate").Elements("AdditionalGuestAmounts").Descendants("AdditionalGuestAmount"))
                                                    {
                                                        extrarate += Convert.ToDecimal(addigust.Element("Amount").Attribute("AmountBeforeTax").Value.Trim());
                                                    }
                                                    ExtraGuest += extrarate;
                                                }
                                            }
                                        }
                                        MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, HotelFilterArray[0]["Hotel_Star"].ToString().Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, "TG", (baserate + taxs + ExtraGuest - disamt) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.servicetax);
                                        if (disamt > 0)
                                            TotaldiscAmt = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, (baserate + taxs + ExtraGuest) / (SearchDetails.NoofNight * SearchDetails.NoofRoom), SearchDetails.servicetax);
                                        HotelRatePlan objRatePlans = new HotelRatePlan();
                                        objRatePlans = RatePlanlist(hoteldetails.BasicPropertyInfo, hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value);
                                        objHotellist.Add(new HotelResult
                                        {
                                            RatePlanCode = hoteldetails.TPA_Extensions.Attribute("LowestRatePlanId").Value,
                                            hotelDiscoutAmt = TotaldiscAmt,
                                            hotelPrice = MarkList.TotelAmt - MarkList.AgentMrkAmt,
                                            AgtMrk = 0,
                                            DiscountMsg = objRatePlans.discountMsg,
                                            inclusions = "",
                                            RoomTypeCode = objRatePlans.RatePlanCode,
                                            HotelCode = HotelCode,
                                            HotelName = HotelFilterArray[0]["VendorName"].ToString().Trim().Replace("'", ""),
                                            HotelCityCode = SearchDetails.SearchCityCode,
                                            HotelCity = HotelFilterArray[0]["City"].ToString().Trim(),
                                            StarRating = HotelFilterArray[0]["Hotel_Star"].ToString().Trim(),
                                            HotelAddress = "",
                                            HotelDescription = "",
                                            HotelThumbnailImg = HotelFilterArray[0]["ImagePath"].ToString().Trim(),
                                            Lati_Longi = "",
                                            Location = "",
                                            HotelServices = "",
                                            ReviewRating = "",
                                            Provider = "TG",
                                            PopulerId = 0
                                        });
                                    }
                                    catch (Exception ex)
                                    {
                                        HotelDA.InsertHotelErrorLog(ex, "GetTGResult_List" + "-" + HotelCode);
                                        objHotellist.Add(new HotelResult { HtlError = ex.Message });
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        objHotellist.Add(new HotelResult { HtlError = "Hotel not found,  Please modify your search.. " });
                        int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                    }
                }
                else
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
                    if (HotelResponse.Contains("<Errors>"))
                    {
                        XDocument document = XDocument.Parse(HotelResponse.Replace(" xmlns=\"http://www.opentravel.org/OTA/2003/05\"", String.Empty));
                        var Hotels_temps = (from Hotel in document.Descendants("Errors") select new { Errormsg = Hotel.Element("Error") }).ToList();
                        if (Hotels_temps.Count > 0)
                        {
                            if (Hotels_temps[0].Errormsg.Attribute("Type") != null && Hotels_temps[0].Errormsg.Attribute("ShortText") != null)
                                objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].Errormsg.Attribute("ShortText").Value + "  " + Hotels_temps[0].Errormsg.Attribute("Type").Value });
                        }
                    }
                    else
                    {
                        if (HotelResponse.Contains("<p>"))
                        {
                            string Serviceerror = HotelResponse.Substring(HotelResponse.IndexOf("<p>"), HotelResponse.IndexOf("</p>") - HotelResponse.IndexOf("<p>") - 2);
                            objHotellist.Add(new HotelResult { HtlError = Serviceerror });
                        }
                        else
                            objHotellist.Add(new HotelResult { HtlError = "Hotel not found for given search criteria. Please modify your search" });
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GetTGResult" + "_" + SearchDetails.SearchCity);
                objHotellist.Add(new HotelResult { HtlError = ex.Message });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.TG_HotelSearchReq, SearchDetails.TG_HotelSearchRes, "TG", "HotelInsert");
            }
            return objHotellist;
        }

        public string HotelSearchRequest(HotelSearch HtlSearchQuery, List<HotelCredencials> CredencialsListnew)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>");
                ReqXml.Append("<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='DEALS' Version='0.0' PrimaryLangID='en' SearchCacheLevel='Live'><AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria><Criterion>");

                if (HtlSearchQuery.HotelName == "")
                {
                    ReqXml.Append("<Address>");
                    ReqXml.Append("<CityName>" + HtlSearchQuery.SearchCity + "</CityName>");
                    ReqXml.Append("<CountryName Code='" + HtlSearchQuery.Country + "'></CountryName>");
                    ReqXml.Append("</Address>");
                    ReqXml.Append("<HotelRef  /> ");
                }
                else
                    ReqXml.Append(" <HotelRef HotelCode='" + HtlSearchQuery.HtlCode + "' />");
                //Single Hotel Search
                ReqXml.Append("<StayDateRange End='" + HtlSearchQuery.CheckOutDate + "' Start='" + HtlSearchQuery.CheckInDate + "'/>");

                //  ReqXml.Append("<RateRange MinRate='500' MinRate='1000' />");<HotelRef HotelCode="the-infantry-hotel-00001065"/>
                ReqXml.Append("<RoomStayCandidates>");
                for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
                {
                    ReqXml.Append("<RoomStayCandidate><GuestCounts>");
                    ReqXml.Append("<GuestCount AgeQualifyingCode='10' Count='" + HtlSearchQuery.AdtPerRoom[i].ToString() + "'/>");
                    if (Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<GuestCount Age='" + HtlSearchQuery.ChdAge[i, j] + "' AgeQualifyingCode='8'/>");
                        }
                    }
                    ReqXml.Append("</GuestCounts></RoomStayCandidate>");
                }
                ReqXml.Append("</RoomStayCandidates>");
                if (HtlSearchQuery.HotelName == "")
                {
                    if (HtlSearchQuery.StarRating != "0")
                        ReqXml.Append("<Award Rating='" + HtlSearchQuery.StarRating + "'/>");
                    else
                    {
                        //if (HtlSearchQuery.CorporateID != "Seeingo")
                        //    ReqXml.Append("<Award Rating='1'/>");
                        ReqXml.Append("<Award Rating='2'/>");
                        ReqXml.Append("<Award Rating='3'/>");
                        ReqXml.Append("<Award Rating='4'/>");
                        ReqXml.Append("<Award Rating='5'/>");
                    }
                }
                ReqXml.Append("<TPA_Extensions>");
                // ReqXml.Append("<Pagination enabled='true' hotelsFrom='1' hotelsTo='50'/>");
                if (HtlSearchQuery.HotelName == "")
                {
                    ReqXml.Append("<Pagination enabled='false' />");
                    ReqXml.Append("<HotelBasicInformation><Reviews/></HotelBasicInformation>");
                }
                ReqXml.Append("<UserAuthentication password='" + CredencialsListnew[0].HotelPassword + "' propertyId='" + CredencialsListnew[0].HotelCompanyID + "' username='" + CredencialsListnew[0].HotelUsername + "'/>");
                ReqXml.Append("</TPA_Extensions></Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ>");
                ReqXml.Append("</soap:Body></soap:Envelope>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelSearchRequest");
            }
            return ReqXml.ToString();
        }

        public string HotelSearchRequestWithPagination(HotelSearch HtlSearchQuery, List<HotelCredencials> CredencialsListnew, int PaginationFrom)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"><soap:Body>");
                ReqXml.Append("<OTA_HotelAvailRQ xmlns=\"http://www.opentravel.org/OTA/2003/05\" RequestedCurrency=\"INR\" SortOrder=\"TG_RANKING\" Version=\"0.0\" PrimaryLangID=\"en\" SearchCacheLevel=\"Live\"><AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria><Criterion>");

                if (HtlSearchQuery.HotelName == "")
                {
                    ReqXml.Append("<Address>");
                    ReqXml.Append("<CityName>" + HtlSearchQuery.SearchCity + "</CityName>");
                    ReqXml.Append("<CountryName Code=\"" + HtlSearchQuery.Country + "\"></CountryName>");
                    ReqXml.Append("</Address>");
                    ReqXml.Append("<HotelRef  /> ");
                }
                else
                    ReqXml.Append(" <HotelRef HotelCode=\"" + HtlSearchQuery.HtlCode + "\" />");
                //Single Hotel Search
                ReqXml.Append("<StayDateRange End=\"" + HtlSearchQuery.CheckOutDate + "\" Start=\"" + HtlSearchQuery.CheckInDate + "\"/>");

                //  ReqXml.Append("<RateRange MinRate='500' MinRate='1000' />");<HotelRef HotelCode="the-infantry-hotel-00001065"/>
                ReqXml.Append("<RoomStayCandidates>");
                for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
                {
                    ReqXml.Append("<RoomStayCandidate><GuestCounts>");
                    ReqXml.Append("<GuestCount AgeQualifyingCode=\"10\" Count=\"" + HtlSearchQuery.AdtPerRoom[i].ToString() + "\"/>");
                    if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<GuestCount Age=\"" + HtlSearchQuery.ChdAge[i, j] + "\" AgeQualifyingCode=\"8\"/>");
                        }
                    }
                    ReqXml.Append("</GuestCounts></RoomStayCandidate>");
                }
                ReqXml.Append("</RoomStayCandidates>");
                if (HtlSearchQuery.HotelName == "")
                {
                    if (HtlSearchQuery.StarRating != "0")
                        ReqXml.Append("<Award Rating=\"" + HtlSearchQuery.StarRating + "\"/>");
                    else
                    {
                        ReqXml.Append("<Award Rating=\"1\"/>");
                        ReqXml.Append("<Award Rating=\"2\"/>");
                        ReqXml.Append("<Award Rating=\"3\"/>");
                        ReqXml.Append("<Award Rating=\"4\"/>");
                        ReqXml.Append("<Award Rating=\"5\"/>");
                    }
                }
                ReqXml.Append("<TPA_Extensions>");
                // ReqXml.Append("<Pagination enabled='true' hotelsFrom='1' hotelsTo='50'/>");
                if (HtlSearchQuery.HotelName == "")
                {
                    // ReqXml.Append("<Pagination enabled='false' />");
                    ReqXml.Append("<Pagination enabled=\"true\" hotelsFrom=\"" + PaginationFrom.ToString() + "\" hotelsTo=\"" + (PaginationFrom + 49).ToString() + "\"/>/>");
                    ReqXml.Append("<HotelBasicInformation><Reviews/></HotelBasicInformation>");
                }
                ReqXml.Append("<UserAuthentication password='" + CredencialsListnew[0].HotelPassword + "' propertyId='" + CredencialsListnew[0].HotelCompanyID + "' username='" + CredencialsListnew[0].HotelUsername + "'/>");
                ReqXml.Append("</TPA_Extensions></Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ>");
                ReqXml.Append("</soap:Body></soap:Envelope>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "HotelSearchRequestWithPagination");
            }
            return ReqXml.ToString();
        }
        public HotelBooking ProvosinalBookingRequest(HotelBooking BookingDetals)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>");
                // ReqXml.Append("<OTA_HotelResRQ xmlns='http://www.opentravel.org/OTA/2003/05' CorrelationID='" + BookingDetals.Orderid + "' TransactionIdentifier='' Version='1.003'><POS><Source ISOCurrency='INR'>");
                ReqXml.Append("<OTA_HotelResRQ xmlns='http://www.opentravel.org/OTA/2003/05' CorrelationID='" + BookingDetals.Orderid + "'  Version='1.003'><POS><Source ISOCurrency='INR'>");
                ReqXml.Append("<RequestorID MessagePassword='" + BookingDetals.HotelPassword + "' ID='" + BookingDetals.HotelCompanyID + "' >");
                ReqXml.Append("<CompanyName Code='" + BookingDetals.HotelUsername + "'></CompanyName>");
                ReqXml.Append("</RequestorID></Source></POS><UniqueID Type='' ID=''/>");
                //ReqXml.Append("</RequestorID></Source></POS><UniqueID/>");
                //Single Hotel Search >
                ReqXml.Append(" <HotelReservations><HotelReservation><RoomStays><RoomStay><RoomTypes>");
                ReqXml.Append("<RoomType NumberOfUnits='" + BookingDetals.NoofRoom + "' RoomTypeCode='" + BookingDetals.RoomTypeCode + "'/></RoomTypes>");
                ReqXml.Append(" <RatePlans><RatePlan RatePlanCode='" + BookingDetals.RoomPlanCode + "' /> </RatePlans>");
                ReqXml.Append(" <GuestCounts IsPerRoom='false'> ");

                for (int i = 0; i < Convert.ToInt32(BookingDetals.NoofRoom); i++)
                {
                    ReqXml.Append("<GuestCount ResGuestRPH='" + i + "' AgeQualifyingCode='10' Count='" + BookingDetals.AdtPerRoom[i].ToString() + "' />");

                    if (Convert.ToInt32(Convert.ToInt32(BookingDetals.ChdPerRoom[i])) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(BookingDetals.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<GuestCount ResGuestRPH='" + i + "' AgeQualifyingCode='8' Age='" + BookingDetals.ChdAge[i, j] + "' Count='1' />");
                        }
                    }
                }
                ReqXml.Append("</GuestCounts>");


                ReqXml.Append("<TimeSpan End='" + BookingDetals.CheckOutDate + "' Start='" + BookingDetals.CheckInDate + "'  />");
                ReqXml.Append("<Total AmountBeforeTax='" + BookingDetals.AmountBeforeTax + "' CurrencyCode='INR' >");

                ReqXml.Append("<Taxes Amount='" + BookingDetals.Taxes + "'/>");
                ReqXml.Append("</Total>");
                ReqXml.Append("<BasicPropertyInfo HotelCode='" + BookingDetals.HtlCode + "'/>");

                if (!string.IsNullOrEmpty(BookingDetals.CustomerRemark))
                    ReqXml.Append("<Comments><Comment><Text>" + BookingDetals.CustomerRemark + "</Text></Comment></Comments>");

                #region GST Information
                // if (BookingDetals.GSTAgencyInfo.Is_GST_Apply)
                // {
                ReqXml.Append("<TPA_Extensions>");
                ReqXml.Append("<CustomerGST>");
                if (BookingDetals.GSTAgencyInfo.GSTNumber != "")
                    ReqXml.Append("<GSTNumber>" + BookingDetals.GSTAgencyInfo.GSTNumber + "</GSTNumber>");
                if (BookingDetals.GSTAgencyInfo.GSTCompanyName != "")
                    ReqXml.Append("<GSTCompanyName>" + BookingDetals.GSTAgencyInfo.GSTCompanyName + "</GSTCompanyName>");
                if (BookingDetals.GSTAgencyInfo.GSTCompanyEmailId != "")
                    ReqXml.Append("<GSTCompanyEmailId>" + BookingDetals.GSTAgencyInfo.GSTCompanyEmailId + "</GSTCompanyEmailId>");
                if (BookingDetals.GSTAgencyInfo.GSTCompanyAddress != "")
                    ReqXml.Append("<GSTCompanyAddress>" + BookingDetals.GSTAgencyInfo.GSTCompanyAddress + "</GSTCompanyAddress>");
                if (BookingDetals.GSTAgencyInfo.GSTCity != "")
                    ReqXml.Append("<GSTCity>" + BookingDetals.GSTAgencyInfo.GSTCity + "</GSTCity>");
                if (BookingDetals.GSTAgencyInfo.GSTPinCode != "")
                    ReqXml.Append("<GSTPinCode>" + BookingDetals.GSTAgencyInfo.GSTPinCode + "</GSTPinCode>");
                if (BookingDetals.GSTAgencyInfo.GSTState != "")
                    ReqXml.Append("<GSTState>" + BookingDetals.GSTAgencyInfo.GSTState + "</GSTState>");
                if (BookingDetals.GSTAgencyInfo.GSTPhoneNumber != "")
                    ReqXml.Append("<GSTPhoneNumber>" + BookingDetals.GSTAgencyInfo.GSTPhoneNumber + "</GSTPhoneNumber>");

                ReqXml.Append("</CustomerGST>");
                ReqXml.Append("</TPA_Extensions>");
                // }
                #endregion

                ReqXml.Append("</RoomStay></RoomStays><ResGuests><ResGuest><Profiles><ProfileInfo><Profile ProfileType='1'>");
                ReqXml.Append("<Customer><PersonName>");
                ReqXml.Append("<NamePrefix>" + BookingDetals.PGTitle + "</NamePrefix>");
                ReqXml.Append("<GivenName>" + BookingDetals.PGFirstName + "</GivenName>");
                ReqXml.Append("<MiddleName></MiddleName>");
                ReqXml.Append("<Surname>" + BookingDetals.PGLastName + "</Surname>");
                ReqXml.Append("</PersonName>");

                ReqXml.Append("<Telephone AreaCityCode='" + BookingDetals.PGContact.Substring(1, 3) + "' CountryAccessCode='91' Extension='0' PhoneNumber='" + BookingDetals.PGContact + "' PhoneTechType='1' />");
                ReqXml.Append("<Email>" + BookingDetals.PGEmail + "</Email>");

                ReqXml.Append("<Address>");
                ReqXml.Append("<AddressLine>" + BookingDetals.PGAddress + "</AddressLine>");
                ReqXml.Append("<CityName>" + BookingDetals.PGCity + "</CityName>");
                ReqXml.Append("<PostalCode>" + BookingDetals.PGPin + "</PostalCode>");
                ReqXml.Append("<StateProv>" + BookingDetals.PGState + "</StateProv>");
                ReqXml.Append("<CountryName>" + BookingDetals.PGCountry + "</CountryName>");
                ReqXml.Append("</Address></Customer></Profile></ProfileInfo></Profiles></ResGuest></ResGuests>");
                ReqXml.Append("<ResGlobalInfo><Guarantee GuaranteeType='PrePay'/></ResGlobalInfo>");
                ReqXml.Append("</HotelReservation></HotelReservations> </OTA_HotelResRQ>");
                ReqXml.Append("</soap:Body></soap:Envelope>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            BookingDetals.ProBookingReq = ReqXml.ToString();
            return BookingDetals;
        }

        public HotelBooking BookingRequest(HotelBooking BookingDetals)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>");
                ReqXml.Append("<OTA_HotelResRQ xmlns='http://www.opentravel.org/OTA/2003/05' CorrelationID='" + BookingDetals.Orderid + "' TransactionIdentifier='' Version='1.003'>");
                ReqXml.Append("<POS><Source ISOCurrency='INR'>");
                ReqXml.Append("<RequestorID MessagePassword='" + BookingDetals.HotelPassword + "' ID='" + BookingDetals.HotelCompanyID + "' >");
                ReqXml.Append("<CompanyName Code='" + BookingDetals.HotelUsername + "'></CompanyName>");
                ReqXml.Append("</RequestorID></Source></POS>");
                ReqXml.Append("<UniqueID Type='23' ID='" + BookingDetals.ProBookingID + "' />");
                ReqXml.Append("<HotelReservations><HotelReservation><ResGlobalInfo><Guarantee GuaranteeType='PrePay'/></ResGlobalInfo>");
                ReqXml.Append("</HotelReservation></HotelReservations>");
                ReqXml.Append("</OTA_HotelResRQ></soap:Body></soap:Envelope>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "");
            }
            BookingDetals.BookingConfReq = ReqXml.ToString();
            return BookingDetals;
        }

        public HotelCancellation CancellationRequest(HotelCancellation BookingDetals)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<?xml version='1.0' encoding='utf-8' ?><soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:ns='http://www.opentravel.org/OTA/2003/05'> <soapenv:Header/> <soapenv:Body>");
                ReqXml.Append("<ns:OTA_CancelRQ CancelType='Cancel' Version='1.0'> <ns:POS> <ns:Source>");
                ReqXml.Append("<ns:RequestorID ID='" + BookingDetals.Can_PropertyId + "' MessagePassword='" + BookingDetals.Can_Password + "' />");
                ReqXml.Append("</ns:Source> </ns:POS><ns:UniqueID Type='14' ID='" + BookingDetals.BookingID + "' />");
                ReqXml.Append("<ns:Verification>");
                ReqXml.Append("<ns:PersonName><ns:Surname>" + BookingDetals.PGLastName + "</ns:Surname></ns:PersonName>");
                ReqXml.Append("<ns:Email>" + BookingDetals.PGEmail + "</ns:Email>");
                ReqXml.Append("</ns:Verification> <ns:TPA_Extensions>");
                ReqXml.Append("<ns:CancelDates>");

                string FromDate = "", ToDate = "";
                // string[] StartDate = BookingDetals.CheckInDate.Split('-');
                // FromDate = StartDate[2] + "-" + StartDate[1] + "-" + StartDate[0] + " " + "12:00:00.000";
                System.DateTime checkinDate = Convert.ToDateTime(BookingDetals.CheckInDate + " 12:00:00.000");

                //string[] EndDate = BookingDetals.CheckOutDate.Split('/');
                //ToDate = EndDate[2] + "/" + EndDate[1] + "/" + EndDate[0];
                //System.DateTime checkout = Convert.ToDateTime(ToDate);

                // BookingDetals.CheckOutDate = checkout.ToString("yyyy-MM-dd");
                //checkinDate = checkinDate.AddDays(BookingDetals.NoofNight);
                //BookingDetals.CheckInDate = checkinDate.ToString("yyyy-MM-dd");
                //BookingDetals.CheckOutDate = checkout.ToString("yyyy-MM-dd");
                switch (BookingDetals.CancellationType)
                {
                    case "ALL":
                        ReqXml.Append("<ns:Dates>" + BookingDetals.CheckInDate + "</ns:Dates>");
                        for (int i = 1; i < BookingDetals.NoofNight; i++)
                        {
                            checkinDate = checkinDate.AddDays(1);
                            ReqXml.Append("<ns:Dates>" + checkinDate.ToString("yyyy-MM-dd") + "</ns:Dates>");
                        }
                        break;

                    case "CheckIN":
                        ReqXml.Append("<ns:Dates>" + BookingDetals.CheckInDate + "</ns:Dates>");
                        for (int i = 0; i < BookingDetals.NightForCancel; i++)
                        {
                            checkinDate = checkinDate.AddDays(1);
                            ReqXml.Append("<ns:Dates>" + checkinDate.ToString("yyyy-MM-dd") + "</ns:Dates>");
                        }
                        break;

                    case "CheckOut":
                        string[] EndDate = BookingDetals.CheckOutDate.Split('/');
                        ToDate = EndDate[2] + "/" + EndDate[1] + "/" + EndDate[0];
                        System.DateTime checkout = Convert.ToDateTime(ToDate);
                        checkout = checkout.AddDays(-BookingDetals.NightForCancel);
                        for (int i = 0; i < BookingDetals.NightForCancel; i++)
                        {
                            checkout = checkout.AddDays(1);
                            ReqXml.Append("<ns:Dates>" + checkout.ToString("yyyy-MM-dd") + "</ns:Dates>");
                        }
                        break;
                }
                ReqXml.Append("</ns:CancelDates>");
                ReqXml.Append("</ns:TPA_Extensions> </ns:OTA_CancelRQ> </soapenv:Body> </soapenv:Envelope>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "CancellationRequest");
            }
            BookingDetals.BookingCancelReq = ReqXml.ToString();
            return BookingDetals;
        }

        public HotelSearch Promotion_HotelSearchRequest(HotelSearch HtlSearchQuery, List<HotelCredencials> CredencialsListnew)
        {
            StringBuilder ReqXml = new StringBuilder();
            try
            {
                ReqXml.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>");
                //"<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='DEALS' Version='0.0' PrimaryLangID='en' SearchCacheLevel='Live'>";
                ReqXml.Append("<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='" + HtlSearchQuery.Sorting + "' Version='0.0' PrimaryLangID='en' SearchCacheLevel='Live'>");
                ReqXml.Append("<AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria><Criterion>");
                ReqXml.Append("<Address>");
                ReqXml.Append("<CityName>" + HtlSearchQuery.SearchCity + "</CityName>");
                ReqXml.Append("<CountryName Code='" + HtlSearchQuery.Country + "'></CountryName>");
                ReqXml.Append("</Address>");
                ReqXml.Append("<HotelRef  /> ");
                ReqXml.Append("<StayDateRange End='" + HtlSearchQuery.CheckOutDate + "' Start='" + HtlSearchQuery.CheckInDate + "'/>");
                //ReqXml.Append("<RateRange MinRate='1000' />");
                ReqXml.Append("<RoomStayCandidates>");
                for (int i = 0; i < Convert.ToInt32(HtlSearchQuery.NoofRoom); i++)
                {
                    ReqXml.Append("<RoomStayCandidate><GuestCounts>");
                    ReqXml.Append("<GuestCount AgeQualifyingCode='10' Count='" + HtlSearchQuery.AdtPerRoom[i].ToString() + "'/>");
                    if (Convert.ToInt32(Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i])) > 0)
                    {
                        for (int j = 0; j < Convert.ToInt32(HtlSearchQuery.ChdPerRoom[i]); j++)
                        {
                            ReqXml.Append("<GuestCount Age='" + HtlSearchQuery.ChdAge[i, j] + "' AgeQualifyingCode='8'/>");
                        }
                    }
                    ReqXml.Append("</GuestCounts></RoomStayCandidate>");
                }
                ReqXml.Append("</RoomStayCandidates>");
                if (HtlSearchQuery.StarRating != "0")
                    ReqXml.Append("<Award Rating='" + Convert.ToString(HtlSearchQuery.StarRating) + "'/>");
                //ReqXml.Append("<Award Rating='3' />");
                ReqXml.Append("<TPA_Extensions>");
                ReqXml.Append("<Pagination enabled='true' hotelsFrom='01' hotelsTo='05'/>");
                ReqXml.Append("<HotelBasicInformation><Reviews/></HotelBasicInformation>");
                ReqXml.Append("<UserAuthentication password='" + CredencialsListnew[0].HotelPassword + "' propertyId='" + CredencialsListnew[0].HotelCompanyID + "' username='" + CredencialsListnew[0].HotelUsername + "'/>");
                ReqXml.Append("</TPA_Extensions></Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ>");
                ReqXml.Append("</soap:Body></soap:Envelope>");
            }
            catch (Exception ex)
            {
                ReqXml.Append(ex.Message);
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "Promotion_HotelSearchRequest");
            }
            HtlSearchQuery.TG_HotelSearchReq = ReqXml.ToString();
            return HtlSearchQuery;
        }
    }
}
