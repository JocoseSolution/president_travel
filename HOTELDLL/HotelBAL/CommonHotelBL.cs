﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using HotelShared;
using HotelDAL;

namespace HotelBAL
{
    public class CommonHotelBL
    {
        public string GetCancelationPolicy(HotelSearch HotelDetails, string Provider, string RatePlaneCode, string HotelCode, string StrPolicy)
        {
            try
            {
                if (Provider == "TG")
                {
                    HotelDetails.HtlRoomCode = RatePlaneCode;
                    TGHotelResponse objTGHotel = new TGHotelResponse();
                    StrPolicy = objTGHotel.TGCancellationPolicy(HotelDetails);
                }
                //else if (Provider == "ZUMATA")
                //{
                //    ZumataHotelBooking objZUHotel = new ZumataHotelBooking();
                //    StrPolicy = objZUHotel.ZumataCancelationPolicy(HotelDetails, HotelCode, RatePlaneCode, StrPolicy);
                //    //if(StrPolicy=="")
                //    //    StrPolicy = objZUHotel.ZumataCancelationPolicy(HotelDetails, HotelCode, RatePlaneCode, StrPolicy);
                //}
                //else if (Provider == "ROOMXML")
                //{
                //    HotelBAL.RoomXMLHotelResponse Roomxmlobj = new HotelBAL.RoomXMLHotelResponse();
                //    StrPolicy = Roomxmlobj.RoomXmlCancellationpolicy(HotelDetails, RatePlaneCode, HotelCode);
                //}
                //else if (Provider == "INNSTANT")
                //    StrPolicy = "Cancellation policy not availble. Please see cancelltion policy in Room Detail.";
                else
                {
                    StrPolicy = "Cancellation policy not availble. Please see cancelltion policy in Room Detail.";
                    //XDocument documen = XDocument.Parse(HotelDetails.GTA_HotelSearchRes);
                    //var HotelsPlicy = (from Hotel in documen.Descendants("HotelDetails").Descendants("Hotel")
                    //                   where Hotel.Element("City").Attribute("Code").Value == RatePlaneCode && Hotel.Element("Item").Attribute("Code").Value == HotelCode
                    //                   select new { CanPolicy = Hotel.Element("RoomCategories").Element("RoomCategory") }).ToList();
                    //HotelBAL.GTAHotelResponse GTAobj = new HotelBAL.GTAHotelResponse();
                    //StrPolicy = GTAobj.SetHotelPolicy(HotelsPlicy[0].CanPolicy, HotelDetails);
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "GetCancelationPolicy");
            }
            return StrPolicy;
        }
        public CancelationPolicy RoomCancelationPolicy(HotelSearch HotelDetails, string Provider, string RatePlaneCode, string HotelCode, CancelationPolicy objpolicy)
        {
            try
            {
                //if (Provider == "ZUMATA")
                //{
                //    ZumataHotelBooking objZUHotel = new ZumataHotelBooking();
                //    objpolicy = objZUHotel.Room_ZumataCancelationPolicy(HotelDetails, HotelCode, RatePlaneCode, objpolicy);
                //}
                //else if (Provider == "ROOMXML")
                //{
                //    HotelBAL.RoomXMLHotelResponse Roomxmlobj = new HotelBAL.RoomXMLHotelResponse();
                //    objpolicy = Roomxmlobj.RoomXmlCancellationPolicy_ForRoom(HotelDetails, RatePlaneCode, objpolicy);
                //}
                //else if (Provider == "INNSTANT")
                //{
                //    HotelBAL.InnstantSearchAndBooking Innstantobj = new HotelBAL.InnstantSearchAndBooking();
                //    objpolicy = Innstantobj.InnstantCancellationPolicy(HotelDetails, RatePlaneCode, HotelCode, objpolicy);

                //}
                //else if (Provider == "GAL")
                //{
                //    HotelBAL.UAPI_1GHotelResponse objGAL = new HotelBAL.UAPI_1GHotelResponse();
                //    objpolicy = objGAL.GALHotelHotelRules(HotelDetails, RatePlaneCode, HotelCode, objpolicy);
                //}
                //else if (Provider == "SEEINGO")
                //{
                //    HotelBAL.Seeingo.SeeingoBookingAndCancellation objseeingo = new HotelBAL.Seeingo.SeeingoBookingAndCancellation();
                //    objpolicy = objseeingo.Room_SeeingoHotelRules(HotelDetails, HotelCode, RatePlaneCode, objpolicy);
                //}
                //else
                //{
                //    XDocument documen = XDocument.Parse(HotelDetails.GTA_HotelSearchRes);
                //    var HotelsPlicy = (from Hotel in documen.Descendants("HotelDetails").Descendants("Hotel")
                //                       where Hotel.Element("City").Attribute("Code").Value == RatePlaneCode && Hotel.Element("Item").Attribute("Code").Value == HotelCode
                //                       select new { CanPolicy = Hotel.Element("RoomCategories").Element("RoomCategory") }).ToList();
                //    HotelBAL.GTAHotelResponse GTAobj = new HotelBAL.GTAHotelResponse();
                //    objpolicy.HotelPolicy = GTAobj.SetHotelPolicy(HotelsPlicy[0].CanPolicy, HotelDetails);
                //}
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "RoomDCancelationPolicy");
            }
            return objpolicy;
        }
        public HotelBooking PreBookingHotelRequest(HotelBooking HotelDetails)
        {
            try
            {
                HotelDA objhtlDa = new HotelDA();
                DataSet AgencyDs = new DataSet();
                AgencyDs = objhtlDa.GetAgencyDetails(HotelDetails.AgentID);

                HotelDetails.GSTAgencyInfo = new GSTAgencyInformation();
                if (AgencyDs.Tables.Count > 0)
                    if (AgencyDs.Tables[0].Rows.Count > 0)
                    {
                        HotelDetails.GSTAgencyInfo.GSTNumber = AgencyDs.Tables[0].Rows[0]["GSTNO"].ToString();
                        HotelDetails.GSTAgencyInfo.GSTCompanyName = AgencyDs.Tables[0].Rows[0]["GST_Company_Name"].ToString();
                        HotelDetails.GSTAgencyInfo.GSTCompanyAddress = AgencyDs.Tables[0].Rows[0]["GST_Company_Address"].ToString();
                        HotelDetails.GSTAgencyInfo.GSTState = AgencyDs.Tables[0].Rows[0]["GST_State"].ToString();
                        HotelDetails.GSTAgencyInfo.GSTCity = AgencyDs.Tables[0].Rows[0]["GST_City"].ToString();
                        HotelDetails.GSTAgencyInfo.GSTPinCode = AgencyDs.Tables[0].Rows[0]["GST_Pincode"].ToString();
                        HotelDetails.GSTAgencyInfo.GSTCompanyEmailId = AgencyDs.Tables[0].Rows[0]["GST_Email"].ToString();
                        HotelDetails.GSTAgencyInfo.GSTPhoneNumber = AgencyDs.Tables[0].Rows[0]["GST_PhoneNo"].ToString();
                        HotelDetails.GSTAgencyInfo.Is_GST_Apply = Convert.ToBoolean(AgencyDs.Tables[0].Rows[0]["Is_GST_Apply"]);
                    }
                if (HotelDetails.Provider == "TG")
                {
                    TGHotelResponse objTGHotel = new TGHotelResponse();
                    // HotelDetails.Provider = "TGBooking";
                    // HotelDetails = objhtlDa.GetBookingCredentials(HotelDetails, "");
                    HotelDetails = objTGHotel.TGHotelsPreBooking(HotelDetails);
                }
                //else if (HotelDetails.Provider == "ZUMATA")
                //{
                //    ZumataHotelBooking objZUHotel = new ZumataHotelBooking();
                //    HotelDetails = objZUHotel.ZUMATAHotelsPreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "GTA")
                //{
                //    GTAHotelResponse objGTAHotel = new GTAHotelResponse();
                //    HotelDetails = objGTAHotel.GTAHotelsPreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "ROOMXML")
                //{
                //    RoomXMLHotelResponse objRoomXmlHotel = new RoomXMLHotelResponse();
                //    HotelDetails = objRoomXmlHotel.RoomXMLHotelsPreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "INNSTANT")
                //{
                //    // InnstantSearchAndBooking objInnstantHotel = new InnstantSearchAndBooking();
                //    // HotelDetails = objInnstantHotel.InnstantHotelsPreBooking(HotelDetails);
                //    HotelDetails.ProBookingID = "true";
                //    HotelDetails.ProBookingReq = "";
                //    HotelDetails.ProBookingRes = "";
                //}
                //else if (HotelDetails.Provider == "GRN")
                //{
                //    GRNConnectHotelBooking objGRNHotel = new GRNConnectHotelBooking();
                //    HotelDetails = objGRNHotel.GRNConnectHotelsPreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "SuperShopper")
                //{
                //    UAPISuperShopperHotelBooking objSSHotel = new UAPISuperShopperHotelBooking();
                //    HotelDetails = objSSHotel.SuperShopperPreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "GAL")
                //{
                //    UAPI_1GHotelResponse objSSHotel = new UAPI_1GHotelResponse();
                //    HotelDetails = objSSHotel.GALHotelsPreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "AGODA")
                //{
                //    AGODA.AgodaBookingAndCancellation objAgodaHotel = new AGODA.AgodaBookingAndCancellation();
                //    HotelDetails = objAgodaHotel.Agoda_PreBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "SEEINGO")
                //{
                //    HotelBAL.Seeingo.SeeingoBookingAndCancellation objseeingo = new Seeingo.SeeingoBookingAndCancellation();
                //    HotelDetails = objseeingo.SeeingoPreBooking(HotelDetails);
                //}
                else if (HotelDetails.Provider == "OYO")
                {
                    HotelBAL.OYO_V2.OYOBookingAndCancellation objoyo = new OYO_V2.OYOBookingAndCancellation();
                    //HotelBAL.OYO.OYOBookingAndCancellation objoyo = new OYO.OYOBookingAndCancellation();
                    HotelDetails = objoyo.OYOoPreBooking(HotelDetails);
                }
                //Update PreBook Hotels Price Request and Response XML Log
                int m = objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.ProBookingReq, HotelDetails.ProBookingRes, HotelDetails.Provider, "HtlUpdateProBooking");
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "PreBookingHotelRequest");
            }
            return HotelDetails;
        }

        public HotelBooking HotelBookingRequest(HotelBooking HotelDetails)
        {
            HotelDA objhtlDa = new HotelDA();
            try
            {
                if (HotelDetails.Provider == "TG")
                {
                    TGHotelResponse objTGHotel = new TGHotelResponse();
                    HotelDetails = objTGHotel.TGHotelsBooking(HotelDetails);
                }
                //else if (HotelDetails.Provider == "GTA")
                //{
                //    GTAHotelResponse objGTAHotel = new GTAHotelResponse();
                //    HotelDetails = objGTAHotel.GTAHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "ZUMATA")
                //{

                //    ZumataHotelBooking objZUHotel = new ZumataHotelBooking();
                //    HotelDetails = objZUHotel.ZUMATAHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "ROOMXML")
                //{
                //    RoomXMLHotelResponse objRoomXmlHotel = new RoomXMLHotelResponse();
                //    HotelDetails = objRoomXmlHotel.RoomXMLHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "INNSTANT")
                //{
                //    InnstantSearchAndBooking objInnstantHotel = new InnstantSearchAndBooking();
                //    HotelDetails = objInnstantHotel.InnstantHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "GRN")
                //{
                //    GRNConnectHotelBooking objGrnConnectHotel = new GRNConnectHotelBooking();
                //    HotelDetails = objGrnConnectHotel.GRNConnectHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "SuperShopper")
                //{
                //    UAPISuperShopperHotelBooking objSSHotel = new UAPISuperShopperHotelBooking();
                //    HotelDetails = objSSHotel.SuperShopperHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "GAL")
                //{
                //    UAPI_1GHotelResponse objSSHotel = new UAPI_1GHotelResponse();
                //    HotelDetails = objSSHotel.UAPIHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "AGODA")
                //{
                //    AGODA.AgodaBookingAndCancellation objAgodaHotel = new AGODA.AgodaBookingAndCancellation();
                //    HotelDetails = objAgodaHotel.AgodaHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "SEEINGO")
                //{
                //    HotelBAL.Seeingo.SeeingoBookingAndCancellation objSeeingoHotel = new HotelBAL.Seeingo.SeeingoBookingAndCancellation();
                //    HotelDetails = objSeeingoHotel.SeeingoHotelsBooking(HotelDetails);
                //}
                else if (HotelDetails.Provider == "OYO")
                {
                    HotelBAL.OYO_V2.OYOBookingAndCancellation objoyo = new OYO_V2.OYOBookingAndCancellation();
                    // HotelBAL.OYO.OYOBookingAndCancellation objoyo = new OYO.OYOBookingAndCancellation();
                    HotelDetails = objoyo.OYOHotelsBooking(HotelDetails);
                }
                //Update Booking Confirmation Request and Response XML Log
                objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.BookingConfReq, HotelDetails.BookingConfRes, HotelDetails.Provider, "HtlUpdateBooking");
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "HotelBookingRequest");
            }
            return HotelDetails;
        }
        public HotelCancellation CancellationHotelBooking(HotelCancellation HotelDetails)
        {
            HotelDA objhtlDa = new HotelDA();
            try
            {
                //get credencials
                HotelDetails = objhtlDa.GetCancleCredentials(HotelDetails, "");

                if (HotelDetails.Provider == "TG")
                {
                    TGHotelResponse objTGHotel = new TGHotelResponse();
                    HotelDetails = objTGHotel.TGHotelsCancelation(HotelDetails);
                }
                //else if (HotelDetails.Provider == "GTA")
                //{
                //    GTAHotelResponse objGTAHotel = new GTAHotelResponse();
                //    HotelDetails = objGTAHotel.GTAHotelCancalltion(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "ZUMATA")
                //{
                //    ZumataHotelBooking objZUHotel = new ZumataHotelBooking();
                //    HotelDetails = objZUHotel.ZUMATAHotelCancalltion(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "ROOMXML")
                //{
                //    RoomXMLHotelResponse objRoomXmlHotel = new RoomXMLHotelResponse();
                //    HotelDetails = objRoomXmlHotel.RoomXMLCancelBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "INNSTANT")
                //{
                //    InnstantSearchAndBooking objInnstantHotel = new InnstantSearchAndBooking();
                //    HotelDetails = objInnstantHotel.InnstantCancelBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "GRN")
                //{
                //    GRNConnectHotelBooking objGrnConnect = new GRNConnectHotelBooking();
                //    HotelDetails = objGrnConnect.GRNConnectCancelBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "SuperShopper")
                //{
                //    UAPISuperShopperHotelBooking objSSHotel = new UAPISuperShopperHotelBooking();
                //    HotelDetails = objSSHotel.UniversalCancalltion(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "GAL")
                //{
                //    UAPI_1GHotelResponse objSSHotel = new UAPI_1GHotelResponse();
                //    HotelDetails = objSSHotel.UniversalCancalltion(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "AGODA")
                //{
                //    AGODA.AgodaBookingAndCancellation objAgodaHotel = new AGODA.AgodaBookingAndCancellation();
                //    HotelDetails = objAgodaHotel.AgodaHotelCancalltion(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "SEEINGO")
                //{
                //    Seeingo.SeeingoBookingAndCancellation objSeeingodaHotel = new Seeingo.SeeingoBookingAndCancellation();
                //    HotelDetails = objSeeingodaHotel.SeeingoHotelsCancelation(HotelDetails);
                //}
                else if (HotelDetails.Provider == "OYO")
                {
                    HotelBAL.OYO_V2.OYOBookingAndCancellation objoyo = new OYO_V2.OYOBookingAndCancellation();
                    // HotelBAL.OYO.OYOBookingAndCancellation objoyo = new OYO.OYOBookingAndCancellation();
                    HotelDetails = objoyo.OYOHotelsCancelation(HotelDetails);
                }
                //Update Booking Confirmation Request and Response XML Log
                objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.BookingCancelReq, HotelDetails.BookingCancelRes, HotelDetails.Provider, "HtlUpdateCancle");
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "CancellationHotelBooking");
            }
            return HotelDetails;
        }
        public HotelBooking CheckHotelBookingStaus(HotelBooking HotelDetails)
        {
            HotelDA objhtlDa = new HotelDA();
            try
            {
                HotelDetails = objhtlDa.GetBookingCredentials(HotelDetails, "");

                //if (HotelDetails.Provider == "ZUMATABooking")
                //{
                //    ZumataHotelBooking objZUHotel = new ZumataHotelBooking();
                //    HotelDetails = objZUHotel.ZUMATAHotelsBookingStatusByZeus(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "INNSTANT")
                //{
                //    InnstantSearchAndBooking objInnstantHotel = new InnstantSearchAndBooking();
                //    HotelDetails = objInnstantHotel.InnstantHotelsBookingStatus(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "ROOMXML")
                //{
                //    RoomXMLHotelResponse objRoomXmlHotel = new RoomXMLHotelResponse();
                //    // HotelDetails = objRoomXmlHotel.RoomXMLHotelsBooking(HotelDetails);
                //}
                if (HotelDetails.Provider == "TG")
                {
                    TGHotelResponse objTGHotel = new TGHotelResponse();
                    // HotelDetails = objTGHotel.TGHotelsBooking(HotelDetails);
                }
                //else if (HotelDetails.Provider == "GTA")
                //{
                //    GTAHotelResponse objGTAHotel = new GTAHotelResponse();
                //    // HotelDetails = objGTAHotel.GTAHotelsBooking(HotelDetails);
                //}
                //else if (HotelDetails.Provider == "AGODA")
                //{
                //    AGODA.AgodaBookingAndCancellation objagoHotel = new AGODA.AgodaBookingAndCancellation();
                //    HotelDetails = objagoHotel.AgodaBookingDetails(HotelDetails);
                //}


                //Update Booking Confirmation Request and Response XML Log
                //objhtlDa.SP_Htl_InsUpdBookingLog(HotelDetails.Orderid, HotelDetails.BookingConfReq, HotelDetails.BookingConfRes, HotelDetails.Provider, "HtlUpdateBooking");
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "HotelBookingRequest");
            }
            return HotelDetails;
        }

        #region Corency Converter from Google API
        public decimal CurrancyConvert_USD_To_INR()
        {
            HotelDA objhtlDa = new HotelDA();
            try
            {
                WebClient web = new WebClient();
                string url = string.Format("http://www.google.com/finance/converter?a=1&from=USD&to=INR");
                byte[] databuffer = Encoding.ASCII.GetBytes("test=postvar&test2=another");
                HttpWebRequest _webreqquest = (HttpWebRequest)WebRequest.Create(url);
                _webreqquest.Method = "POST";
                _webreqquest.ContentType = "application/x-www-form-urlencoded";
                _webreqquest.Timeout = 11000;
                _webreqquest.ContentLength = databuffer.Length;
                Stream PostData = _webreqquest.GetRequestStream();
                PostData.Write(databuffer, 0, databuffer.Length);
                PostData.Close();
                HttpWebResponse WebResp = (HttpWebResponse)_webreqquest.GetResponse();
                Stream finalanswer = WebResp.GetResponseStream();
                StreamReader _answer = new StreamReader(finalanswer);
                string[] value = Regex.Split(_answer.ReadToEnd(), "&nbsp;");
                var a = Regex.Split(value[1], "<div id=currency_converter_result>1 USD = <span class=bld>");
                decimal rate = 0;
                try
                {
                    rate = Convert.ToDecimal(Regex.Split(a[1], " INR</span>")[0].Trim());
                    int i = objhtlDa.UpdateCurrancyValue(rate);
                }
                catch (Exception ex)
                {
                    ConvertCurrancy_USD_To_INR_from_xe();
                    HotelDA.InsertHotelErrorLog(ex, a.ToString());
                }
                return rate;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "CurrancyConvert_USD_To_INR");
                decimal curval = objhtlDa.SelectCurrancyValue();
                if (curval > 0)
                    return curval;
                else
                    return ConvertCurrancy_USD_To_INR_from_xe();
            }
        }
        #endregion

        #region Corency Converter from http://www.xe.com
        public decimal ConvertCurrancy_USD_To_INR_from_xe()
        {
            HotelDA HTLST = new HotelDA();
            try
            {
                HttpWebRequest HttpWebReq = (HttpWebRequest)WebRequest.Create("http://www.xe.com/ucc/convert/?Amount=1&From=USD&To=INR");
                HttpWebReq.Timeout = 11000;
                HttpWebResponse WebResponse = (HttpWebResponse)HttpWebReq.GetResponse();
                Stream responseStream = WebResponse.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                reader = new StreamReader(HttpWebReq.GetResponse().GetResponseStream());
                string response = reader.ReadToEnd();

                string convert__1 = response.Substring(response.IndexOf("XE Currency Converter"), response.IndexOf("View") - response.IndexOf("XE Currency Converter"));
                convert__1 = convert__1.Substring(convert__1.IndexOf("uccRes"), convert__1.IndexOf("uccResRgn") - convert__1.IndexOf("uccRes"));
                string[] str = Regex.Split(convert__1, "<td");
                string[] str1 = str[3].Split('>');
                string[] inr__2 = str1[1].Split('&');
                decimal rate = 0;
                try
                {
                    rate = Convert.ToDecimal(inr__2[0].Trim());
                    HTLST.UpdateCurrancyValue(rate);
                }
                catch (Exception ex)
                {
                    HotelDA.InsertHotelErrorLog(ex, convert__1);
                }
                return rate;
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "ConvertCurrancy_USD_To_INR_from_xe");
                decimal curval = HTLST.SelectCurrancyValue();
                if (curval > 0)
                    return curval;
                else
                    return 0;
            }
        }
        #endregion

        public string SetHotelService_UAPI(string AllServices, string baseUrl)
        {
            string InclImg = ""; int j = 0, k = 0, l = 0, m = 0, n = 0, o = 0, p = 0, q = 0, r = 0, s = 0, t = 0, u = 0, v = 0;
            try
            {
                string[] Services = AllServices.Split('#');
                for (int i = 0; i < Services.Length - 1; i++)
                {
                    switch (Services[i])
                    {
                        case "76":
                        case "143":
                        case "158":
                        case "165":
                        case "173":
                        case "174":
                        case "175":
                            if (j == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/bar.png' title='Mini bar' class='IconImageSize' /><span class='hide'>Restaurant/Bar</span>";
                                j = 1;
                            }
                            break;
                        case "42":
                        case "53":
                        case "68":
                            if (k == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/Parking.png' title='Parking' class='IconImageSize' /><span class='hide'>Parking</span>";
                                k = 1;
                            }
                            break;

                        case "2":
                        case "77":
                        case "126":
                            if (r == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/lounge.png' title='Room Service' class='IconImageSize' /><span class='hide'>Room Services</span>";
                                r++;
                            }
                            break;
                        case "190":
                            InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/Phone.png' title='Direct dial phone' class='IconImageSize' /><span class='hide'>Phone</span>";
                            break;
                        case "66":
                        case "71":
                            if (s == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/swimming.png' title='Outdoor Swimming Pool' class='IconImageSize' /><span class='hide'>Swimming Pool</span>";
                                s++;
                            }
                            break;
                        case "54":
                        case "253":
                            if (t == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/jacuzzi.png' title='Indoor Swimming Pool' class='IconImageSize' /><span class='hide'>Tub Bath</span>";
                                t++;
                            } break;
                        case "93":
                        case "110":
                        case "TRDK":
                            if (n == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/Airport_transfer.png' title='Travel agency facilities' class='IconImageSize' /><span class='hide'>Travel & Transfers</span>";
                                n = 1;
                            }
                            break;
                        case "58":
                            InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/laundary.png' title='Laundry facilities' class='IconImageSize' /><span class='hide'>Laundry Services</span>";
                            break;
                        case "228":
                        case "229":
                            if (l == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/Banquet_hall.png' title='Business centre' class='IconImageSize' /><span class='hide'>Business Facilities</span>";
                                l = 1;
                            }
                            break;
                        case "258":
                        case "101":
                            if (m == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/handicap.png' title='Disabled facilities' class='IconImageSize' /><span class='hide'>Disabled Facilities</span>";
                                m = 1;
                            }
                            break;
                        case "35":
                        case "48":
                            if (v == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/health_club.png' title='Gymnasium' class='IconImageSize' /><span class='hide'>Gym</span>";
                                v = 1;
                            } break;
                        case "ELEV":
                            InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/elevator.png' title='Lifts' class='IconImageSize' /><span class='hide'>Lift</span>";
                            break;
                        case "178":
                        case "179":
                        case "222":
                        case "223":
                        case "259":
                            if (o == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/wifi.gif' title='Internet' class='IconImageSize' /><span class='hide'>Internet/Wi-Fi</span>";
                                o = 1;
                            }
                            break;

                        case "8":
                        case "CHPR":
                            if (q == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/babysitting.png' title='Baby' class='IconImageSize' /><span class='hide'>Baby Facilities</span>";
                                q = 1;
                            }
                            break;
                        case "107":
                            InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/beauty.png' title='Beauty parlour' class='IconImageSize' /><span class='hide'>Beauty Parlour</span>";
                            break;
                        case "84":
                        case "79":
                        case "61":
                            if (r == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/sauna.png' title='Sauna' class='IconImageSize' /><span class='hide'>Spa/Massage/Wellness/Sauna</span>";
                                r = 1;
                            }
                            break;
                        case "140":
                        case "145":
                        case "211":
                            InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/lobby.png' title='Lobby' class='IconImageSize' /><span class='hide'>Lobby</span>";
                            break;
                        case "70":
                        case "193":
                            if (p == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/golf.png' title='Golf' class='IconImageSize' /><span class='hide'>Sports</span>";
                                p = 1;
                            } break;
                        case "220":
                        case "TEVV":
                            if (l == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/TV.png' title='TV' class='IconImageSize' /><span class='hide'>TV</span>";
                                l = 1;
                            }
                            break;
                        case "5":
                            InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/AC.png' title='AC' class='IconImageSize' /><span class='hide'>AC</span>";
                            break;
                        case "240":
                            InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/bath.png' title='Hair Dryer' class='IconImageSize' /><span class='hide'>Hair Dryer</span>";
                            break;
                        case "138":
                        case "141":
                        case "152":
                        case "157":
                        case "159":
                        case "251":
                        case "250":
                        case "227":
                            if (u == 0)
                            {
                                InclImg += "<img src='" + baseUrl + "/Hotel/Images/Facility/breakfast.png' title='Breakfast' class='IconImageSize' /><span class='hide'>Breakfast</span>";
                                u = 1;
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            { HotelDA.InsertHotelErrorLog(ex, "SetHotelService_UAPI"); }
            return InclImg;
        }
    }
}
