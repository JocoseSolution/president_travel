﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Configuration;
using HotelShared;
using HotelDAL;
using System.Xml;
using System.Web;
using System.Collections;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
namespace HotelBAL
{
   public class EXHotelResponse
    { 
        

        EXHotelRequest HtlReq = new EXHotelRequest();
        public HotelComposite EXHotels(HotelSearch SearchDetails)
        {
            HotelComposite obgHotelsCombo = new HotelComposite();
            try
            {
                if (SearchDetails.EXURL != null && (SearchDetails.EXTrip == SearchDetails.HtlType || SearchDetails.EXTrip == "ALL"))
                {
                    SearchDetails = HtlReq.EXHotelSearchRequest(SearchDetails);
                    SearchDetails.EX_HotelSearchRes = EXPostXml(SearchDetails.EXURL + SearchDetails.EX_HotelSearchReq);
                    obgHotelsCombo.Hotelresults = GetEXHotelsPrice(SearchDetails.EX_HotelSearchRes, SearchDetails);
                }
                else 
                {
                    SearchDetails.EX_HotelSearchRes = "Hotel Not available for " + SearchDetails.SearchCity + ", " + SearchDetails.Country;
                    SearchDetails.EX_HotelSearchReq = "not allow Expedia";
                    List<HotelResult> objHotellist = new List<HotelResult>();
                    objHotellist.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = SearchDetails.EX_HotelSearchRes });
                    obgHotelsCombo.Hotelresults = objHotellist;
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "EXHotels");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchRes, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                SearchDetails.EX_HotelSearchRes = ex.Message;
            }
            obgHotelsCombo.HotelSearchDetail = SearchDetails;
            return obgHotelsCombo;
        }

        protected string EXPostXml(string url)
        {
            StringBuilder sbResult = new StringBuilder();
            string xml = "";
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    Http.Timeout = 56000;
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelDA objhtlDa = new HotelDA();
                HotelDA.InsertHotelErrorLog(WebEx, "Expedia Post methode");
                WebResponse response = WebEx.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "EX", "HotelInsert");
                }
                else
                {
                    int n = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, WebEx.Message, "EX", "HotelInsert");
                    sbResult.Append(WebEx.Message + "<Errors>");
                   
                }
            }
            catch (Exception ex)
            {
                sbResult.Append(ex.Message + "<Errors>");
                HotelDA.InsertHotelErrorLog(ex, "Expedia Post methode");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, ex.Message, "EX", "HotelInsert");
            }
            return sbResult.ToString();
        }
        protected string EXPostBook(string url, string xml)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    ServicePointManager.ServerCertificateValidationCallback = ((sender, certificate, chain, sslPolicyErrors) => true);
                    ////System.Net.ServicePointManager.CertificatePolicy = new MyPolicy();
                   // ServicePointManager.ServerCertificateValidationCallback = delegate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };
                   // ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback ( delegate { return true; });
                    
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                   // Http.Timeout = 300 * 1000;
                   // Http.ReadWriteTimeout = 300 * 1000;
                   //// Http.ContentType = "text/xml";
                    Http.ContentType = "application/x-www-form-urlencoded";
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException WebEx)
            {
                HotelSendMail_Log Objsendmail = new HotelSendMail_Log();
                HotelDA.InsertHotelErrorLog(WebEx, "Expedia Post methode - EXPostBook ");
                WebResponse response = WebEx.Response;
                if (response != null)
                {
                    Stream stream = response.GetResponseStream();
                    string responseMessage = new StreamReader(stream).ReadToEnd();
                    sbResult.Append(responseMessage);
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, responseMessage, "EX", "HotelInsert");
                    Objsendmail.ExceptionEmail(responseMessage);
                }
                else
                    Objsendmail.ExceptionEmail(WebEx.Message);
            }
            catch (Exception exx)
            {
                sbResult.Append(exx.Message);
                HotelDA.InsertHotelErrorLog(exx, "");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", xml, exx.Message, "EX", "HotelInsert");
                HotelSendMail_Log Objsendmail = new HotelSendMail_Log();
                Objsendmail.ExceptionEmail(exx.Message);
            }
            return sbResult.ToString();
        }
        protected List<HotelResult> GetEXHotelsPrice(string XmlRes, HotelSearch SearchDetails)
        {

            HotelDA objhtlDa = new HotelDA();  
            HotelMarkups objHtlMrk = new HotelMarkups();    
            MarkupList MarkList = new MarkupList();
            List<HotelResult> HResult = new List<HotelResult>();
            try
            {
                if (XmlRes.Contains("<EanWsError>"))
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, XmlRes, SearchDetails.AgentID, "HotelInsert");

                    XDocument Xdocument = XDocument.Parse(XmlRes);
                    var Hotels_temps = (from Hotel in Xdocument.Descendants("EanWsError") select new { Errormsg = Hotel.Element("verboseMessage"), ErrorID = Hotel.Element("exceptionConditionId") }).ToList();
                    HResult.Add(new HotelResult {HotelName="" , hotelPrice=0 , HtlError = Hotels_temps[0].ErrorID.Value + " : " + Hotels_temps[0].Errormsg.Value });
                }

                XmlDocument Xdoc = new XmlDocument();
                Xdoc.LoadXml(XmlRes);

                if (Xdoc.SelectSingleNode("//HotelList") != null)
                {
                    //// = Xdoc.SelectSingleNode("//customerSessionId").InnerText;

                    XmlNodeList hotelSummaryList = Xdoc.SelectNodes("//HotelList//HotelSummary");
                    int v_counter = 1300;

                    foreach (XmlNode hotel in hotelSummaryList)
                    {
                        ////if (hotel.Attributes["order"].InnerText == ConfigurationManager.AppSettings["orderId"].ToString())
                        ////{
                        ////    string s = hotel.Attributes["order"].InnerText;
                        ////}
                        ////string s1 = hotel.Attributes["order"].InnerText;

                        string RoomCode = "", Highrate = "", MinRate="";
                       //// getMinRoomRate(hotel, hotel.SelectSingleNode("lowRate").InnerText, out RoomCode, out Highrate);
                        getMinRoomRate(hotel, out MinRate, out RoomCode, out Highrate);

                        
                        string DisMsg = "", HotelAddon = "", HServices = "";
                        if (hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//RateInfos//RateInfo").Attributes["promo"].InnerText == "true")
                        {
                            if (hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//RateInfos//RateInfo//promoDescription") != null)
                            {
                                DisMsg = hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//RateInfos//RateInfo//promoDescription").InnerText;
                            }
                        }
                        if (hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//ValueAdds") != null)
                        {
                            XmlNodeList ValueAddNodeList = hotel.SelectNodes("RoomRateDetailsList//RoomRateDetails//ValueAdds//ValueAdd");
                            foreach (XmlNode ValueAddNode in ValueAddNodeList)
                            {
                                HotelAddon += ValueAddNode.SelectSingleNode("description").InnerText + ",";
                            }
                        }
                        if (hotel.SelectSingleNode("amenityMask").InnerText != "0")
                        {
                            HServices = convertDecimalToBinary(hotel.SelectSingleNode("amenityMask").InnerText);
                        }
                        MarkList = objHtlMrk.EXmarkupCalculation(SearchDetails.MarkupDS, hotel.SelectSingleNode("hotelRating").InnerText, SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(MinRate), SearchDetails.EX_servicetax);
                       
                        decimal discountrate = 0;
                        ////if (Convert.ToDecimal(Highrate) > Convert.ToDecimal(hotel.SelectSingleNode("lowRate").InnerText))
                        if (Convert.ToDecimal(Highrate) > Convert.ToDecimal(MinRate))
                            discountrate = objHtlMrk.DiscountMarkupCalculation(0, MarkList.AgentMrkPercent, "Fixed", MarkList.AgentMrkType, Convert.ToDecimal(Highrate), 0);
                        string postalCode =hotel.SelectSingleNode("postalCode")!=null ? hotel.SelectSingleNode("postalCode").InnerText : "";
                        HResult.Add(new HotelResult
                        {

                            PopulerId = v_counter++,
                            HotelCode = hotel.SelectSingleNode("hotelId").InnerText,
                            HotelCity = hotel.SelectSingleNode("city").InnerText,
                            HotelCityCode = SearchDetails.SearchCityCode,
                            ////CountryCode = hotel.SelectSingleNode("countryCode").InnerText,
                            HotelName = hotel.SelectSingleNode("name").InnerText,
                            StarRating = hotel.SelectSingleNode("hotelRating").InnerText,
                            HotelAddress = (hotel.SelectSingleNode("address1").InnerText + "," + hotel.SelectSingleNode("city").InnerText + "-" + postalCode).Replace(",,", ","),
                            HotelThumbnailImg = "http://images.travelnow.com" + hotel.SelectSingleNode("thumbNailUrl").InnerText,
                            Location =  getLocation(hotel.SelectSingleNode("locationDescription").InnerText),
                            HotelDescription = "<strong>Description</strong>: " + (hotel.SelectSingleNode("shortDescription") != null ? hotel.SelectSingleNode("shortDescription").InnerText.Replace("&lt;p&gt;&lt;b&gt;Property Location&lt;/b&gt; &lt;br /&gt;", "") : ""),
                            Lati_Longi = hotel.SelectSingleNode("latitude").InnerText + "##" + hotel.SelectSingleNode("longitude").InnerText,
                            Provider = "EX",
                            ReviewRating = (hotel.SelectSingleNode("tripAdvisorRating") != null ? hotel.SelectSingleNode("tripAdvisorRating").InnerText.Trim() : "") + "#" + (hotel.SelectSingleNode("tripAdvisorReviewCount") != null ? hotel.SelectSingleNode("tripAdvisorReviewCount").InnerText.Trim() : ""),

                            RatePlanCode = hotel.Attributes["order"].InnerText,//For testing Purposes
                            //// CurrencyCode = hotel.SelectSingleNode("rateCurrencyCode").InnerText,

                            // TripAdvisorRatingUrl = hotel.SelectSingleNode("tripAdvisorRatingUrl") != null ? hotel.SelectSingleNode("tripAdvisorRatingUrl").InnerText : "",

                            // RatePlanCode = hotel.SelectSingleNode("RoomRateDetailsList//RoomRateDetails//rateCode").InnerText,
                            hotelPrice = MarkList.TotelAmt,////Convert.ToDecimal(hotel.SelectSingleNode("lowRate").InnerText) 
                            AgtMrk = MarkList.AgentMrkAmt,
                            DiscountMsg = DisMsg,
                            hotelDiscoutAmt = discountrate,
                            // hotelDiscoutAmt = Convert.ToDecimal(hotel.SelectSingleNode("highRate").InnerText)-Convert.ToDecimal(hotel.SelectSingleNode("lowRate").InnerText)>0 ? Convert.ToDecimal(hotel.SelectSingleNode("highRate").InnerText): 0,//objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(hotel.SelectSingleNode("highRate").InnerText), SearchDetails.EX_servicetax),
                            inclusions = "",// HotelAddon.Length > 0 ? HotelAddon.Remove(HotelAddon.LastIndexOf(','), 1) : HotelAddon,
                            HotelServices = HServices,
                            RoomTypeCode = RoomCode

                        });

                    }

                }
                else
                {
                    HResult.Add(new HotelResult {HotelName="", hotelPrice = 0, HtlError = "Hotel  Not found" });
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                }

                return HResult; 
            }
            catch (Exception ex)
            {
                HResult.Add(new HotelResult { HotelName = "", hotelPrice = 0, HtlError = ex.Message });
                HotelDA.InsertHotelErrorLog(ex, "GetEXHotelsPrice");
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
            }
            return HResult;
        }
        

        public string EXCancellationPolicy(HotelSearch SearchDetails)
        {
            string Policy = "";
            try
            {
               // SearchDetails =  TGHotels(SearchDetails);
                if (SearchDetails.EX_HotelSearchRes.Contains("<RoomRateDetailsList>"))
                {
                    string responses = SearchDetails.EX_HotelSearchRes.Replace(@"xmlns:ns2=\u0022http://v3.hotel.wsapi.ean.com/\u0022", String.Empty);
                    XDocument xmlresp = XDocument.Parse(responses);
                    var hotelPolicy = xmlresp.Descendants("RoomRateDetails").Where(x => x.Element("rateCode").Value == SearchDetails.HtlRoomCode).Descendants("cancellationPolicy").Select(p => p.Value).FirstOrDefault();

                    if (hotelPolicy!=null)
                    {
                           Policy = "<Div><span style='font-weight: bold;font-style:normal;font-size:13px;'>Cancellation Policy</span>";
                       
                                Policy += "<li style='margin:4px 0 4px 0;'>" +hotelPolicy + "</li></Div>";
                           
                       
                    }
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "Expidia CancellationPolicy");
                return ex.Message;
            }
            return Policy;
        }

        private string EXPHotelInformation(HotelSearch SearchDetails, string HotelCode)
        {
            string EXResponce = "";
            XDocument document = new XDocument();
            try
            {
                string XMLFile = ConfigurationManager.AppSettings["ExpediaHotelDetail"] + "\\" + HotelCode + ".xml";
                if (File.Exists(XMLFile))
                    EXResponce = XDocument.Load(XMLFile).ToString();
                else
                {
                    SearchDetails = HtlReq.EXHotelInformation(SearchDetails);
                    EXResponce = EXPostXml(SearchDetails.EXURL.Replace("list?", "info?") + SearchDetails.EXHtlInfoReq);
                    document = XDocument.Parse(EXResponce);
                    document.Save(XMLFile);
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", SearchDetails.EXHtlInfoReq, EXResponce, "EX", "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "EXPHotelInformation");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EXHtlInfoReq, HotelCode + "_" + SearchDetails.SearchCity, "EX", "HotelInsert");
            }
            return EXResponce;
        }
        public HotelBooking EXHotelPreBooking(HotelBooking HotelDetail)
        {
           
            try
            {
                if (HotelDetail.EXURL != null)
                 {
                    HotelDetail = HtlReq.ExHotelAvailability(HotelDetail);
                    HotelDetail.ProBookingRes = EXPostXml(HotelDetail.EXURL.Replace("list?", "avail?") + HotelDetail.ProBookingReq);

                    if (!HotelDetail.ProBookingRes.Contains("<EanWsError>"))
                    {
                        XDocument xmlresp = XDocument.Parse(HotelDetail.ProBookingRes);

                        var hotelprice = (from hotel in xmlresp.Descendants("HotelRoomResponse")
                                          where hotel.Element("rateCode").Value == HotelDetail.RoomPlanCode && hotel.Element("roomTypeCode").Value == HotelDetail.RoomTypeCode
                                          select new
                                          {
                                              rateCode = hotel.Element("rateCode").Value,roomTypeCode = hotel.Element("roomTypeCode").Value,
                                              rateKey = hotel.Element("RateInfos").Element("RateInfo").Element("RoomGroup").Element("Room").Element("rateKey").Value,
                                              totalAmount = hotel.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Attribute("total").Value
                                          }).ToList();
                        if (hotelprice.Count > 0)
                        {
                            if (HotelDetail.TotalRoomrate >= Convert.ToDecimal(hotelprice[0].totalAmount))
                                HotelDetail.ProBookingID = "true";
                            else
                            {
                                HotelDetail.ReferenceNo = "Rate Not matching";
                                HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                            }
                        }
                        else
                        {
                            HotelDetail.ReferenceNo = "Hotel Not found";
                            HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                        }
                    }
                    else
                    {
                        HotelDetail.ReferenceNo = "Exception";
                        HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                    }
                 }
                 else
                 {
                     HotelDetail.ReferenceNo = "BookingNotAlowed";
                     HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
                 }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "EXHotelPreBooking");
                HotelDetail.ReferenceNo = ex.Message;
                HotelDetail.BookingID = ""; HotelDetail.ProBookingID = "false";
            }
          
            return HotelDetail;
        }
        public HotelBooking EXHotelsBooking(HotelBooking HotelDetail)
        {
            try
            {
                if (HotelDetail.EXURL != null)
                {
                    HotelDetail = HtlReq.EXHotelBookingRequestXML(HotelDetail);
                    HotelDetail.BookingConfRes = EXPostBook(HotelDetail.EXURL, HotelDetail.BookingConfReq);
                    if (!HotelDetail.BookingConfRes.Contains("<EanWsError>"))
                    {
                        XDocument document = XDocument.Parse(HotelDetail.BookingConfRes);

                        HotelDetail.Status = document.Descendants().Elements("reservationStatusCode").ElementAt(0).Value;
                        HotelDetail.BookingID  = document.Descendants().Elements("itineraryId").ElementAt(0).Value;
                        string roombookingId = "";
                        foreach (var room in document.Descendants("confirmationNumbers"))
                        {
                            roombookingId += room.Value + "/";  
                        }
                        HotelDetail.ProBookingID = roombookingId.TrimEnd('/');

                        switch (HotelDetail.Status)
                        {
                            case "CF":
                                HotelDetail.Status = HotelStatus.Confirm.ToString();
                                break;
                            case "UC":
                                HotelDetail.Status = "Unconfirmed";
                                break;
                            case "PS":
                                HotelDetail.Status = "Pending Supplier";
                                break;
                            case "ER":
                                HotelDetail.Status = "Error";
                                break;
                            case "DT":
                                HotelDetail.Status = "Deleted Itinerary";
                                break;
                        }
                       
                    }
                    else
                    {
                        HotelDetail.ProBookingID = "Exception";
                        HotelDetail.BookingID = "";
                    }
                }
                else
                {
                    HotelDetail.ProBookingID = "Booking Not allow";
                    HotelDetail.BookingID = "";
                }
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, "EXHotelsBooking");
                HotelDetail.ProBookingID = ex.Message;
                HotelDetail.BookingID = "";
            }

            return HotelDetail;
        }
        public RoomComposite EXRoomDetals(HotelSearch SearchDetails)
        {
            RoomComposite objRoomDetals = new RoomComposite();
            List<RoomList> objRoomList = new List<RoomList>();
            SelectedHotel HotelDetail = new SelectedHotel();
            HotelMarkups objHtlMrk = new HotelMarkups();
            MarkupList MarkList = new MarkupList();
            MarkupList TaxMarkList = new MarkupList();
            MarkupList ExtraMarkList = new MarkupList();
            List<HotelResult> objHotellist = new List<HotelResult>();
            HotelDA objhtlDa = new HotelDA();
            try
            {
                XDocument document = XDocument.Parse(SearchDetails.EX_HotelSearchRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                var RoomsDetails = (from Hotel in document.Descendants("HotelList").Elements("HotelSummary")
                                    where Hotel.Element("hotelId").Value == SearchDetails.HtlCode
                                    select new { RoomCategory = Hotel.Element("RoomRateDetailsList") }).ToList();

                #region Hotel Details
                string roomDetailResult = EXPHotelInformation(SearchDetails, SearchDetails.HtlCode);
                if (roomDetailResult.Contains("<EanWsError>"))
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, roomDetailResult, SearchDetails.AgentID, "HotelInsert");

                    XDocument Xdocument = XDocument.Parse(roomDetailResult);
                    var Hotels_temps = (from Hotel in Xdocument.Descendants("EanWsError").Elements("verboseMessage") select new { Errormsg = Hotel.Element("verboseMessage"), ErrorID = Hotel.Element("exceptionConditionId") }).ToList();
                    objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].ErrorID.Value + " : " + Hotels_temps[0].Errormsg.Value });
                }
                HotelDetail = EXHotelInformation_Room(SearchDetails, roomDetailResult);
                #endregion

                if (RoomsDetails.Count > 0)
                {

                    #region Room Details
                    int j = 0;
                    decimal v_HotelFees=0;
                    foreach (var Rooms in RoomsDetails[0].RoomCategory.Descendants("RoomRateDetails"))
                    {
                        try
                        {
                            decimal v_extraguestFee = 0, v_otherTaxes_charges = 0, v_TotalPriceToHotel = 0;
                            if (Rooms.Element("RateInfos").Element("RateInfo").Element("HotelFees") != null)
                            {
                                foreach(var Hotelfee in Rooms.Element("RateInfos").Element("RateInfo").Element("HotelFees").Elements("HotelFee"))
                                {
                                    v_HotelFees += Convert.ToDecimal(Hotelfee.Attribute("amount").Value);
                                }

                            }
                            v_TotalPriceToHotel = Convert.ToDecimal(Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Attribute("total").Value);
                            string smoking = "", discountmsg = "", Inclusion = "", Org_RoomRateStr = "", MrkRoomrateStr = "";
                            decimal Rate_Org = 0, DiscountRate = 0, MrkTotalPrice = 0, adminMrkAmt = 0, AgtMrkAmt = 0, MrkTaxes = 0, MrkExtraGuest = 0, SericeTaxAmt = 0, VSericeTaxAmt = 0, MarkListBaseRt=0;

                            //Hotel Price
                            foreach (var Roomsrates in Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate"))
                            {
                                Rate_Org += Convert.ToDecimal(Roomsrates.Attribute("rate").Value) * SearchDetails.NoofRoom;
                                MarkList =  objHtlMrk.EXmarkupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(Roomsrates.Attribute("rate").Value), SearchDetails.EX_servicetax);

                                MrkTotalPrice += MarkList.TotelAmt * SearchDetails.NoofRoom;////Convert.ToDecimal(Roomsrates.Attribute("rate").Value) * SearchDetails.NoofRoom;
                                ////adminMrkAmt += MarkList.AdminMrkAmt * SearchDetails.NoofRoom;
                                AgtMrkAmt += MarkList.AgentMrkAmt * SearchDetails.NoofRoom;
                                SericeTaxAmt += MarkList.AgentServiceTaxAmt * SearchDetails.NoofRoom;
                                VSericeTaxAmt += MarkList.VenderServiceTaxAmt * SearchDetails.NoofRoom;
                                MrkRoomrateStr += (MarkList.TotelAmt * SearchDetails.NoofRoom).ToString() + "/"; ////Convert.ToDecimal(Roomsrates.Attribute("rate").Value) * SearchDetails.NoofRoom + "/";
                                
                                Org_RoomRateStr += Roomsrates.Attribute("baseRate").Value + "/";
                            }
                            if (Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges") != null)
                            {
                                foreach (var sercharge in Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges").Descendants("Surcharge"))
                                {
                                    if (sercharge.Attribute("type").Value == "ExtraPersonFee")
                                    {
                                        v_extraguestFee = Convert.ToDecimal(sercharge.Attribute("amount").Value);
                                        ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(0, MarkList.AgentMrkPercent, "Fixed", MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                        MrkTotalPrice += ExtraMarkList.TotelAmt;
                                        MrkExtraGuest += ExtraMarkList.TotelAmt;
                                        adminMrkAmt += ExtraMarkList.AdminMrkAmt;
                                        AgtMrkAmt += ExtraMarkList.AgentMrkAmt;
                                        SericeTaxAmt += ExtraMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += ExtraMarkList.VenderServiceTaxAmt;
                                       //// Rate_Org += Convert.ToDecimal(sercharge.Attribute("amount").Value);
                                        v_extraguestFee = Math.Round(Convert.ToDecimal(sercharge.Attribute("amount").Value),2);
                                    }
                                    else if (sercharge.Attribute("type").Value == "TaxAndServiceFee")
                                    {
                                        TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(0, MarkList.AgentMrkPercent,"Fixed", MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                     ////   MrkTotalPrice += TaxMarkList.TotelAmt;
                                        MrkTaxes += TaxMarkList.TotelAmt;
                                        adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                        AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                        SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                        v_otherTaxes_charges += Math.Round(Convert.ToDecimal(sercharge.Attribute("amount").Value),2);
                                    }
                                    else if (sercharge.Attribute("type").Value == "Tax")
                                    {
                                        TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(0, MarkList.AgentMrkPercent,"Fixed", MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                      ////  MrkTotalPrice += TaxMarkList.TotelAmt;
                                        MrkTaxes += TaxMarkList.TotelAmt;
                                        adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                        AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                        SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                        v_otherTaxes_charges += Math.Round(Convert.ToDecimal(sercharge.Attribute("amount").Value),2);
                                    }
                                    else if (sercharge.Attribute("type").Value == "ServiceFee")
                                    {
                                        TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(0, MarkList.AgentMrkPercent, "Fixed", MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                     ////   MrkTotalPrice += TaxMarkList.TotelAmt;
                                        MrkTaxes += TaxMarkList.TotelAmt;
                                        adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                        AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                        SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                        v_otherTaxes_charges += Math.Round(Convert.ToDecimal(sercharge.Attribute("amount").Value),2);
                                    }
                                    else if (sercharge.Attribute("type").Value == "SalesTax")
                                    {
                                        TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(0, MarkList.AgentMrkPercent, "Fixed", MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                      ////  MrkTotalPrice += TaxMarkList.TotelAmt;
                                        MrkTaxes += TaxMarkList.TotelAmt;
                                        adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                        AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                        SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                        v_otherTaxes_charges +=Math.Round(Convert.ToDecimal(sercharge.Attribute("amount").Value),2);
                                    }
                                    else if (sercharge.Attribute("type").Value == "HotelOccupancyTax")
                                    {
                                        TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(0, MarkList.AgentMrkPercent, "Fixed", MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                      ////  MrkTotalPrice += TaxMarkList.TotelAmt;
                                        MrkTaxes += TaxMarkList.TotelAmt;
                                        adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                        AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                        SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                        v_otherTaxes_charges +=Math.Round(Convert.ToDecimal(sercharge.Attribute("amount").Value),2);
                                    }
                                }
                            }
                            string v_promo =Rooms.Element("RateInfos").Element("RateInfo").Attribute("promo").Value;
                            if (v_promo == "true")
                                DiscountRate = Math.Round(Convert.ToDecimal(Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Attribute("averageBaseRate").Value) * SearchDetails.NoofRoom * SearchDetails.NoofNight,2);//// objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Attribute("averageBaseRate").Value) * SearchDetails.NoofRoom * SearchDetails.NoofNight, 0);
                            if (Rooms.Element("RateInfos").Element("promoDescription") != null)
                                discountmsg = Rooms.Element("RateInfos").Element("RateInfo").Element("promoDescription").Value;

                            //Display Hotel Inclution strat  
                            if (Rooms.Element("ValueAdds") != null)
                                foreach (var inclu in Rooms.Element("ValueAdds").Descendants("ValueAdd"))
                                {
                                    Inclusion += inclu.Element("description").Value + " ,";
                                }
                            //Display Hotel Inclution End
                            if (Rooms.Element("smokingPreferences") != null)
                                smoking = Rooms.Element("smokingPreferences").Value;


                            string RoomDiscription = "", CancellationPolicy = "";
                            //Room Disciption
                            try
                            {
                                if (j <= HotelDetail.RoomcatList.Count - 1)
                                {
                                    string[] roomdisArr = (string[])HotelDetail.RoomcatList[j];
                                    roomdisArr.Select(x => x[0].ToString() == Rooms.Element("roomTypeCode").Value);
                                    RoomDiscription += roomdisArr[2];
                                }
                            }
                            catch (Exception ex)
                            {
                                HotelDA.InsertHotelErrorLog(ex, "Expidia RoomcatList" + SearchDetails.SearchCity + "_" + SearchDetails.HtlCode);
                            }
                            j++;
                            CancellationPolicy = "<span>" + (Rooms.Element("RateInfos").Element("RateInfo").Element("cancellationPolicy") != null ? Rooms.Element("RateInfos").Element("RateInfo").Element("cancellationPolicy").Value : "") + "</span>";
                            CancellationPolicy += "<div class='clear'></div><span style='font-weight: bold;'>Please note refund amount may take up to 30 days on credit your account from cancellation date.</span>";
                            
                            objRoomList.Add(new RoomList
                            {
                                HotelCode = SearchDetails.HtlCode,
                                RatePlanCode = Rooms.Element("rateCode").Value,
                                RoomTypeCode = Rooms.Element("roomTypeCode").Value + "|" + Rooms.Element("RateInfos").Element("RateInfo").Element("RoomGroup").Element("Room").Element("rateKey").Value + "|" + Rooms.Element("BedTypes").Element("BedType").Attribute("id").Value,
                                RoomName = System.Web.HttpUtility.HtmlDecode(Rooms.Element("roomDescription").Value.Replace("amp;amp;", "and").Replace("amp;", "and")),
                                discountMsg = discountmsg,
                                DiscountAMT = v_promo == "true"  ?  DiscountRate + v_extraguestFee + v_otherTaxes_charges :0         ,//DiscountRate + v_extraguestFee + v_otherTaxes_charges > 0 ? DiscountRate + v_extraguestFee + v_otherTaxes_charges : 0,
                                Total_Org_Roomrate =v_TotalPriceToHotel,//// Rate_Org,
                                TotalRoomrate = MrkTotalPrice + Math.Ceiling(v_otherTaxes_charges),//// Math.Round(Rate_Org + v_extraguestFee + v_otherTaxes_charges,2),
                                AdminMarkupPer = 0,
                                AdminMarkupAmt = adminMrkAmt,
                                AdminMarkupType = HotelStatus.Percentage.ToString(),
                                AgentMarkupPer = MarkList.AgentMrkPercent,
                                AgentMarkupAmt = AgtMrkAmt,
                                AgentMarkupType = MarkList.AgentMrkType,
                                AgentServiseTaxAmt = SericeTaxAmt,
                                V_ServiseTaxAmt = VSericeTaxAmt,
                                AmountBeforeTax = 0,
                                Taxes = 0,
                                MrkTaxes = Math.Ceiling(v_otherTaxes_charges),////MrkTaxes,
                                ExtraGuest_Charge = Math.Round(MrkExtraGuest,2),////Math.Round(v_extraguestFee,2),
                                Smoking = Rooms.Element("smokingPreferences").Value,
                                inclusions = Inclusion.TrimEnd(','),
                                CancelationPolicy = System.Web.HttpUtility.HtmlDecode(CancellationPolicy.Replace("amp;amp;", " & ").Replace("amp;", " & ")) + "<div class='clear1'></div><span style='font-size:15px;font-weight: bold;'>Hotel Policy</span><div class='clear'></div><span>" + HotelDetail.EXHotelPolicy + "</span>",
                                OrgRateBreakups = Org_RoomRateStr.TrimEnd('/'),
                                MrkRateBreakups = MrkRoomrateStr.TrimEnd('/'),
                                DiscRoomrateBreakups = "",
                                EssentialInformation = Rooms.Element("RateInfos").Element("RateInfo").Element("nonRefundable").Value == "true" ? "Non-Refundable":"Refundable",
                                RoomDescription = RoomDiscription,
                                ////  EXRateKey =Rooms.Element("RateInfos").Element("RateInfo").Element("RoomGroup").Element("Room").Element("rateKey").Value,
                                Provider = "EX",
                                RoomImage = "http://images.travelnow.com" + Rooms.Parent.Parent.Element("thumbNailUrl").Value,
                                EXHotelFee = Math.Round(v_HotelFees,2)
                            });
                        }
                        catch (Exception ex)
                        {
                            objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                            HotelDA.InsertHotelErrorLog(ex, "expidia Rooms adding" + SearchDetails.SearchCity + "_" + SearchDetails.HtlCode);
                        }
                    }
                    #endregion
                    objRoomDetals.RoomDetails = objRoomList;
                    objRoomDetals.SelectedHotelDetail = HotelDetail;
                }
                else
                {
                    objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                HotelDA.InsertHotelErrorLog(ex, "EXRoomDetals" + SearchDetails.SearchCity);
            }
            return objRoomDetals;
        }
      

        public SelectedHotel EXHotelInformation_Room(HotelSearch hotelSearch,string xmlresult)
        {
            string HotelCode = hotelSearch.HtlCode;
            SelectedHotel HotelInfo = new SelectedHotel();
            ArrayList RoomcatList = new ArrayList();
            try
            {
                XmlDocument document = new XmlDocument();
                document.LoadXml(xmlresult);
                string Addresss = "Address Not Found", Telephones = "", hotelPolicy = "";
                if (document.SelectSingleNode("//HotelDetails//hotelPolicy") != null)
                {
                 hotelPolicy = document.SelectSingleNode("//HotelDetails//hotelPolicy").InnerText;
                }
                HotelInfo.EXHotelPolicy = hotelPolicy;
                if (document.SelectSingleNode("//HotelSummary") != null)
                {

                   

                    if (document.SelectSingleNode("//HotelSummary//address1") != null)
                    {
                        Addresss = document.SelectSingleNode("//HotelSummary//address1").InnerText;
                    }
                    if (document.SelectSingleNode("//HotelSummary//address2") != null)
                    {
                        Addresss += "," + document.SelectSingleNode("//HotelSummary//address2").InnerText;
                    }
                    if (document.SelectSingleNode("//HotelSummary//city") != null)
                    {
                        Addresss += "," + document.SelectSingleNode("//HotelSummary//city").InnerText;
                    }
                    if (document.SelectSingleNode("//HotelSummary//postalCode") != null)
                    {
                        Addresss += "-" + document.SelectSingleNode("//HotelSummary//postalCode").InnerText;
                    }
                    HotelInfo.HotelAddress = Addresss;

                    string hoteldiscription = "", roomdescription = ""; ;

                    if (document.SelectSingleNode("//roomDetailDescription") != null)
                        {
                            roomdescription += "<div style='padding: 4px 0;'>" + System.Web.HttpUtility.HtmlDecode(document.SelectSingleNode("//roomDetailDescription").InnerText) + "</div>";
                            roomdescription += "<div class='clear1'></div>";
                        }
                        if (document.SelectSingleNode("//areaInformation") != null)
                        {
                            hoteldiscription += "<div style='padding: 4px 0;'><span  style='text-transform:uppercase; font-size:13px;font-weight: bold;'>SurroundingArea:</span>" + System.Web.HttpUtility.HtmlDecode(document.SelectSingleNode("//areaInformation").InnerText) + "</div>";
                            hoteldiscription += "<div class='clear1'></div>";
                        }
                        if (document.SelectSingleNode("//propertyDescription") != null)
                        {
                            hoteldiscription += "<div style='padding: 4px 0;'><span  style='text-transform:uppercase; font-size:13px;font-weight: bold;'>Hotel Description:</span>" + System.Web.HttpUtility.HtmlDecode(document.SelectSingleNode("//propertyDescription").InnerText) + "</div>";
                            hoteldiscription += "<div class='clear1'></div>";
                        }

                        if (document.SelectSingleNode("//businessAmenitiesDescription") != null)
                        {
                            hoteldiscription += "<div style='padding: 4px 0;'><span  style='text-transform:uppercase; font-size:13px;font-weight: bold;'>Business Amenities Description:</span>" + System.Web.HttpUtility.HtmlDecode(document.SelectSingleNode("//businessAmenitiesDescription").InnerText) + "</div>";
                            hoteldiscription += "<div class='clear1'></div>";
                        }

                        if (document.SelectSingleNode("//checkInInstructions") != null)
                        {
                            hoteldiscription += "<div style='padding: 4px 0;'><span  style='text-transform:uppercase; font-size:13px;font-weight: bold;'>Check-In Instructions:</span>" + System.Web.HttpUtility.HtmlDecode(document.SelectSingleNode("//checkInInstructions").InnerText) + "</div>";
                            hoteldiscription += "<div class='clear1'></div>";
                        }

                    HotelInfo.HotelDescription = hoteldiscription.Trim();
                    XmlNodeList hotelNodes = document.SelectNodes("//RoomTypes//RoomType");
                    foreach (XmlNode Roomnode in hotelNodes)
                    {
                        string  facliStr = "";
                        HotelInfo.Attraction = ""; HotelInfo.RoomAmenities = "";
                       
                        if (Roomnode.SelectSingleNode("descriptionLong") != null)
                        {
                            hoteldiscription += Roomnode.SelectSingleNode("descriptionLong").InnerText;
                           
                        }
                        if (Roomnode.SelectSingleNode("roomAmenities//RoomAmenity") != null)
                            foreach (XmlNode flt in Roomnode.SelectNodes("roomAmenities//RoomAmenity"))
                            {
                                if (flt.SelectSingleNode("amenity") != null)
                                {
                                    facliStr += "<div class='check1'>" + flt.SelectSingleNode("amenity").InnerText + "</div>";
                                }
                            }
                        HotelInfo.RoomAmenities = facliStr;
                        RoomcatList.Add(new string[] { (Roomnode).Attributes["roomCode"].Value, hoteldiscription, roomdescription });
                    }
                    HotelInfo.RoomcatList = RoomcatList;
                    if (document.SelectNodes("//PropertyAmenities//PropertyAmenity") != null)
                    {
                        string hotelAmenity = "";
                        XmlNodeList HotelAmenityList = document.SelectNodes("//PropertyAmenities//PropertyAmenity");
                        foreach (XmlNode hAmenity in HotelAmenityList)
                        {
                            if (hAmenity.SelectSingleNode("amenity") != null)
                            {
                                hotelAmenity += "<div class='check1'>" + hAmenity.SelectSingleNode("amenity").InnerText + "</div>";
                            }
                        }
                        HotelInfo.HotelAmenities =  hotelAmenity;
                    }
                    if (document.SelectSingleNode("//HotelSummary//hotelRating") != null)
                    {
                        HotelInfo.StarRating = document.SelectSingleNode("//HotelSummary//hotelRating").InnerText;
                    }
                    HotelInfo.Lati_Longi = "";
                    if (document.SelectSingleNode("//HotelSummary//latitude") != null)
                    {
                        HotelInfo.Lati_Longi = document.SelectSingleNode("//HotelSummary//latitude").InnerText + "," + document.SelectSingleNode("//HotelSummary//longitude").InnerText;
                    }
                    string imgdiv = "";
                    ArrayList Images = new ArrayList();
                    try
                    {
                        int im = 0;

                        if (document.SelectNodes("//HotelImages") != null)
                        {
                            foreach (XmlNode img in document.SelectNodes("//HotelImages/HotelImage"))
                            {
                                im++;

                                imgdiv += "<img id='img" + im + "' src='" + img.SelectSingleNode("url").InnerText + "' onmouseover='return ShowHtlImg(this);' title='" + img.SelectSingleNode("caption").InnerText + "' alt='' class='imageHtlDetailsshow' />";
                                Images.Add(img.SelectSingleNode("//HotelImages//url").InnerText);
                                
                            }
                        }
                        else
                        {
                            Images.Add("Images/Hotel/NoImage.jpg");
                        }
                        HotelInfo.ThumbnailUrl = Images[0].ToString();
                        HotelInfo.HotelImage = imgdiv;



                    }
                    catch (Exception ex)
                    {
                        HotelDA.InsertHotelErrorLog(ex, HotelCode + "-ImageLinks");
                    }

                }
                else
                {
                    HotelInfo.StarRating = "0";
                    HotelInfo.HotelAddress = "";
                    HotelInfo.HotelContactNo = "";
                    HotelInfo.Attraction = "";
                    HotelInfo.HotelDescription = "";
                    HotelInfo.RoomAmenities = "";
                    HotelInfo.HotelAmenities = "";
                    HotelInfo.Lati_Longi = "";
                    HotelInfo.HotelImage = "<li><a href='Images/NoImage.jpg'><img src='Images/NoImage.jpg' width='40px' height='40px' title='Image not found' /></a></li>";
                    HotelDA objhtlDa = new HotelDA();
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", HotelCode, document.ToString(), "", "HotelInsert");
                }
                 
            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, HotelCode + "-GetRoomXMLRoomHotelInformation");
                HotelDA objhtlDa = new HotelDA();
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("ItemDownLode", "", "", HotelCode, "HotelInsert");
            }
            return HotelInfo;
        }


        public  void getMinRoomRate(XmlNode HotelSummery, out string Minrate, out string Roomcode,out string Baserate)
        {
           
           try
           {
               XElement ele = XElement.Parse(HotelSummery.OuterXml);
               var minVal = (from item in ele.Descendants("NightlyRate")
                        select
                           (
                            decimal.Parse(item.Attribute("rate").Value)
                )).Where(price => price > 0).Min().ToString();


               Minrate = minVal;
               var x = from item in ele.Descendants("NightlyRate").Where(r => r.Attribute("rate").Value == minVal)
                       select item;
              if (!x.Any())
              {
                  var y = from item in ele.Descendants("NightlyRate").Where(r => r.Attribute("baseRate").Value == minVal)
                          select item;
                  Roomcode = y.FirstOrDefault().Parent.Parent.Parent.Parent.Parent.Element("rateCode").Value;
                  Baserate = y.FirstOrDefault().Attribute("rate").Value;
                  
              }
              else
              {
                  Roomcode = x.FirstOrDefault().Parent.Parent.Parent.Parent.Parent.Element("rateCode").Value;
                  Baserate = x.FirstOrDefault().Attribute("baseRate").Value;
                  
              }
           }
           catch (Exception ex)
           {

               HotelDA.InsertHotelErrorLog(ex, HotelSummery + "-getMinRoomRate" );
               Baserate = "0";
               Roomcode = "";
               Minrate = "0";
           }
       }
        public  string convertDecimalToBinary(string hService)
       {
           int amenities = Convert.ToInt32(hService);
           string v_amenity = "";
           string v_amenityresult = "";
           try
           {
               ArrayList arrary = new ArrayList();
               StringBuilder amenitiesval = new StringBuilder();
               int x = amenities;
               int y = 0;
               while (x > 0)
               {
                   if (x % 2 == 1)
                   {
                       y = x % 2;
                       amenitiesval.Append(y);
                   }
                   else
                   {
                       y = x % 2;
                       amenitiesval.Append(y);
                   }
                   x = x / 2;
               }
               v_amenity = amenitiesval.ToString();
              
               double p = Math.Pow(2, 3);
               for (int i = 0; i < v_amenity.Length; i++)
               {
                   if (v_amenity.Substring(i, 1) != "0")
                   {
                       v_amenityresult += (Convert.ToInt16(v_amenity.Substring(i, 1)) * Convert.ToInt64(Math.Pow(2, i))) + "#";
                   }
               }
               v_amenityresult = v_amenityresult.Remove(v_amenityresult.LastIndexOf('#'), 1);
           }
           catch (Exception ex)
           {
               HotelDA.InsertHotelErrorLog(ex,  "-convertDecimalToBinary" + hService);
           }
           return v_amenityresult;
       }

        public HotelBooking EXHotelRoomValidate(HotelBooking HtlBook)
        {
            HtlBook = EXHotelPreBooking(HtlBook);
            return HtlBook;
        }

        public HotelCancellation EXHotelCancalltion(HotelCancellation HtlCancel)
        {
            string failedCancellation = "";
            string v_cancellationResponse = "";
            HotelDA db = new HotelDA();
            try
            {
                string bookingRes = db.SelectHotelLog("1", HtlCancel.Orderid, "", "", "Booking").Tables[0].Rows[0]["resp"].ToString();
                XDocument XResponse = XDocument.Parse(bookingRes);
                var x = XResponse.Descendants("CancelPolicyInfo");
                string[] roomRefArr = HtlCancel.ConfirmationNo.Split('/');
                for (int i = 0; i < roomRefArr.Length; i++)
                {
                    HtlCancel = HtlReq.EXBookingCanellationReq(HtlCancel, roomRefArr[i]);
                    HtlCancel.BookingCancelRes = EXPostXml(HtlCancel.HotelUrl.Replace("list", "cancel") + HtlCancel.BookingCancelReq);
                    if (HtlCancel.BookingCancelRes.Contains("<EanWsError>"))
                    {
                        failedCancellation += roomRefArr[i] + "/";
                    }
                    else
                    {
                        v_cancellationResponse += v_cancellationResponse;
                    }
                }
                HtlCancel.CancelStatus = HotelStatus.CancelPending.ToString();
                //HtlCancel.BookingCancelRes = v_cancellationResponse;
                //failedCancellation = failedCancellation.TrimEnd('/');
                //if (failedCancellation.Split('/').Length == roomRefArr.Length)
                //{
                //    HtlCancel.CancelStatus = "Failed";
                //}
                //else if (failedCancellation.Split('/').Length < roomRefArr.Length)
                //{
                //    HtlCancel.CancelStatus = "Partially Failed";
                //}
                //else if (failedCancellation.Split('/').Length == 0)
                //{
                //    HtlCancel.CancelStatus = "Success";
                //    EXCalcellationRefund(HtlCancel, bookingRes);
                //}
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXHotelCancalltion");
            }
            return HtlCancel;
        }

        public void EXCalcellationRefund(HotelCancellation HtlCancel, string bookingRes)
        {
            try
            {
            XDocument Xdoc = XDocument.Parse(bookingRes);
            var val = Xdoc.Descendants("CancelPolicyInfo");
            //string startWindowHours = Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("startWindowHours").Value;
            int startWindowHours = Convert.ToInt16(Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("startWindowHours").Value);
            string NonrefundStatus = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("nonRefundable").Value;
            decimal totalAccomodationCharge = Convert.ToDecimal(Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Attribute("total").Value);
            string timeZoneDescription = Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("timeZoneDescription").Value;
            int indexfrom = timeZoneDescription.IndexOf("(");
            int indexend = timeZoneDescription.IndexOf(')');
            timeZoneDescription = timeZoneDescription.Substring(indexfrom + 1, indexend - 1).Replace("GMT", "");
            double timeZone = Convert.ToDouble(TimeSpan.Parse(timeZoneDescription).TotalHours);
            var Canceltime = Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("cancelTime").Value.Split(':');
            decimal totalRefund = -1,Totalcancelcharge=0;

            if (NonrefundStatus == "true")
            {
                string msg = "Non-Refundable";
                totalRefund = 0;
            }
            else
            {
                DateTime startime = new DateTime(Convert.ToInt16(DateTime.Now.Year), Convert.ToInt16(DateTime.Now.Month), Convert.ToInt16(DateTime.Now.Date), Convert.ToInt16(Canceltime[0]), Convert.ToInt16(Canceltime[1]), Convert.ToInt16(Canceltime[2])).AddHours(timeZone);
                DateTime endtime = startime.AddHours(-startWindowHours);
                DateTime cancellationTime = DateTime.UtcNow.AddHours(timeZone);

                int timediff = cancellationTime.CompareTo(endtime);

                if (timediff == 1)
                {
                    string msg = "Out of Window , Cnacellation Charge will be applied";

                    if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(0).Element("amount") != null)
                    {
                        if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(0).Element("amount").Value.Length != 0)
                        {
                            Totalcancelcharge = Convert.ToDecimal(Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("amount").Value);
                            totalRefund = totalAccomodationCharge - Totalcancelcharge;
                        }

                    }
                    if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(0).Element("nightCount") != null)
                    {
                        if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(0).Element("nightCount").Value.Length != 0)
                        {
                            int nightCount = Convert.ToInt16(Xdoc.Descendants("CancelPolicyInfo").ElementAt(0).Element("nightCount").Value);
                            if (nightCount == 1)
                            {
                                string nightlyTotalCost = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Attribute("nightlyRateTotal").Value.ToString();
                                string taxes = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges").Descendants("Surcharge").Where(p => p.Attribute("type").Value == "TaxAndServiceFee").Select(y => y.Attribute("amount").Value).ToString();
                                var firstnightcharge = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate").Attributes("rate").ElementAt(0);

                                Totalcancelcharge = (Convert.ToDecimal(firstnightcharge) / Convert.ToDecimal(nightlyTotalCost)) * (Convert.ToDecimal(taxes) == 0 ? 1 : Convert.ToDecimal(taxes));
                                totalRefund = totalAccomodationCharge - Totalcancelcharge;
                            }
                            if (nightCount == 2)
                            {
                                string nightlyTotalCost = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Attribute("nightlyRateTotal").Value.ToString();
                                string taxes = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges").Descendants("Surcharge").Where(p => p.Attribute("type").Value == "TaxAndServiceFee").Select(y => y.Attribute("amount").Value).ToString();
                                var firstnightcharge = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate").Attributes("rate").ElementAt(0);
                                var secondnightcharge = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate").Attributes("rate").ElementAt(1);

                                decimal night1cnacelcharge = (Convert.ToDecimal(firstnightcharge) / Convert.ToDecimal(nightlyTotalCost)) * (Convert.ToDecimal(taxes) == 0 ? 1 : Convert.ToDecimal(taxes));
                                decimal night2cnacelcharge = (Convert.ToDecimal(secondnightcharge) / Convert.ToDecimal(nightlyTotalCost)) * (Convert.ToDecimal(taxes) == 0 ? 1 : Convert.ToDecimal(taxes));
                                Totalcancelcharge = night1cnacelcharge + night2cnacelcharge;
                                totalRefund = totalAccomodationCharge - Totalcancelcharge;
                            }
                        }
                    }
                    if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(0).Element("percent") != null)
                    {
                        if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(0).Element("percent").Value.Length != 0)
                        {
                            decimal cancelPercent = Convert.ToDecimal(Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("percent").Value);
                            Totalcancelcharge = totalAccomodationCharge * (cancelPercent / 100);
                            totalRefund = totalAccomodationCharge - Totalcancelcharge;
                        }
                    }
                }
                else if (timediff == -1)
                {
                    string msg = "Inside of Window , Cnacellation Charge will not be applied";
                    if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("amount") != null)
                    {
                        if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("amount").Value.Length != 0)
                        {
                            Totalcancelcharge = Convert.ToDecimal(Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("amount").Value);
                            totalRefund = totalAccomodationCharge - Totalcancelcharge;
                        }
                    }
                    if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("nightCount") != null)
                    {
                        if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("nightCount").Value.Length != 0)
                        {
                            int nightCount = Convert.ToInt16(Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("nightCount").Value);
                            if (nightCount == 1)
                            {
                                string nightlyTotalCost = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Attribute("nightlyRateTotal").Value.ToString();
                                string taxes = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges").Descendants("Surcharge").Where(p => p.Attribute("type").Value == "TaxAndServiceFee").Select(y => y.Attribute("amount").Value).ToString();
                                var firstnightcharge = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate").Attributes("rate").ElementAt(0);

                                Totalcancelcharge = (Convert.ToDecimal(firstnightcharge) / Convert.ToDecimal(nightlyTotalCost)) * (Convert.ToDecimal(taxes) == 0 ? 1 : Convert.ToDecimal(taxes));
                                totalRefund = totalAccomodationCharge - Totalcancelcharge;
                            }
                            if (nightCount == 2)
                            {
                                string nightlyTotalCost = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Attribute("nightlyRateTotal").Value.ToString();
                                string taxes = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges").Descendants("Surcharge").Where(p => p.Attribute("type").Value == "TaxAndServiceFee").Select(y => y.Attribute("amount").Value).ToString();
                                var firstnightcharge = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate").Attributes("rate").ElementAt(0);
                                var secondnightcharge = Xdoc.Descendants("RateInfos").ElementAt(0).Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate").Attributes("rate").ElementAt(1);

                                decimal night1cnacelcharge = (Convert.ToDecimal(firstnightcharge) / Convert.ToDecimal(nightlyTotalCost)) * (Convert.ToDecimal(taxes) == 0 ? 1 : Convert.ToDecimal(taxes));
                                decimal night2cnacelcharge = (Convert.ToDecimal(secondnightcharge) / Convert.ToDecimal(nightlyTotalCost)) * (Convert.ToDecimal(taxes) == 0 ? 1 : Convert.ToDecimal(taxes));
                                Totalcancelcharge = night1cnacelcharge + night2cnacelcharge;
                                totalRefund = totalAccomodationCharge - Totalcancelcharge;
                            }
                        }
                    }
                    if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("percent") != null)
                    {
                        if (Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("percent").Value.Length != 0)
                        {
                            decimal cancelPercent = Convert.ToDecimal(Xdoc.Descendants("CancelPolicyInfo").ElementAt(1).Element("percent").Value);
                            Totalcancelcharge = totalAccomodationCharge * (cancelPercent / 100);
                            totalRefund = totalAccomodationCharge - Totalcancelcharge;
                        }
                    }

                }
                
            }
            HtlCancel.CancellationCharge = Totalcancelcharge;
            }
            catch (Exception ex)
            {
                HotelDAL.HotelDA.InsertHotelErrorLog(ex, "EXCalcellationRefund");
            }
        }
        public string getLocation(string loc)
        {
            string location = loc;
            try
            {
                if (loc.Contains('('))
                {
                    int findex = loc.IndexOf('(') + 1;
                    int lIndex = loc.IndexOf(')');

                    location = loc.Substring(findex, lIndex - findex);
                }

            }
            catch (Exception ex)
            {
                HotelDA.InsertHotelErrorLog(ex, loc + "getLocation");
            }
            return location.Replace("Near ", String.Empty).Replace("&apos;", String.Empty);
        }
        public RoomComposite EXRoomDetals_old(HotelSearch SearchDetails)
        {
            RoomComposite objRoomDetals = new RoomComposite();
            List<RoomList> objRoomList = new List<RoomList>();
            SelectedHotel HotelDetail = new SelectedHotel();
            HotelMarkups objHtlMrk = new HotelMarkups();
            MarkupList MarkList = new MarkupList();
            MarkupList TaxMarkList = new MarkupList();
            MarkupList ExtraMarkList = new MarkupList();
            List<HotelResult> objHotellist = new List<HotelResult>();
            HotelDA objhtlDa = new HotelDA();
            try
            {
                XDocument document = XDocument.Parse(SearchDetails.EX_HotelSearchRes.Replace("&nbsp", "").Replace("&", "").Replace("&amp;", ""));
                var RoomsDetails = (from Hotel in document.Descendants("HotelList").Elements("HotelSummary")
                                    where Hotel.Element("hotelId").Value == SearchDetails.HtlCode
                                    select new { RoomCategory = Hotel.Element("RoomRateDetailsList") }).ToList();

                #region Hotel Details
                string roomDetailResult = EXPHotelInformation(SearchDetails, SearchDetails.HtlCode);
                if (roomDetailResult.Contains("<EanWsError>"))
                {
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, roomDetailResult, SearchDetails.AgentID, "HotelInsert");

                    XDocument Xdocument = XDocument.Parse(roomDetailResult);
                    var Hotels_temps = (from Hotel in Xdocument.Descendants("EanWsError").Elements("verboseMessage") select new { Errormsg = Hotel.Element("verboseMessage"), ErrorID = Hotel.Element("exceptionConditionId") }).ToList();
                    objHotellist.Add(new HotelResult { HtlError = Hotels_temps[0].ErrorID.Value + " : " + Hotels_temps[0].Errormsg.Value });
                }
                HotelDetail = EXHotelInformation_Room(SearchDetails, roomDetailResult);
                #endregion

                if (RoomsDetails.Count > 0)
                {

                    #region Room Details
                    int j = 0;
                    foreach (var Rooms in RoomsDetails[0].RoomCategory.Descendants("RoomRateDetails"))
                    {
                        try
                        {

                            string smoking = "", discountmsg = "", Inclusion = "", Org_RoomRateStr = "", MrkRoomrateStr = "";
                            decimal Rate_Org = 0, DiscountRate = 0, MrkTotalPrice = 0, adminMrkAmt = 0, AgtMrkAmt = 0, MrkTaxes = 0, MrkExtraGuest = 0, SericeTaxAmt = 0, VSericeTaxAmt = 0;

                            //SericeTaxAmt += MarkList.AgentServiceTaxAmt + TaxMarkList.AgentServiceTaxAmt + ExtraMarkList.AgentServiceTaxAmt;
                            // VSericeTaxAmt += MarkList.VenderServiceTaxAmt + TaxMarkList.VenderServiceTaxAmt + ExtraMarkList.VenderServiceTaxAmt;
                            //Hotel Price
                            foreach (var Roomsrates in Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("NightlyRatesPerRoom").Descendants("NightlyRate"))
                            {
                                Rate_Org += Convert.ToDecimal(Roomsrates.Attribute("rate").Value) * SearchDetails.NoofRoom;
                               // MarkList = objHtlMrk.markupCalculation(SearchDetails.MarkupDS, SearchDetails.StarRating.Trim(), SearchDetails.AgentID, SearchDetails.SearchCity, SearchDetails.Country, Convert.ToDecimal(Roomsrates.Attribute("rate").Value), SearchDetails.EX_servicetax);
                                MrkTotalPrice += MarkList.TotelAmt * SearchDetails.NoofRoom;
                                adminMrkAmt += MarkList.AdminMrkAmt * SearchDetails.NoofRoom;
                                AgtMrkAmt += MarkList.AgentMrkAmt * SearchDetails.NoofRoom;
                                SericeTaxAmt += MarkList.AgentServiceTaxAmt * SearchDetails.NoofRoom;
                                VSericeTaxAmt += MarkList.VenderServiceTaxAmt * SearchDetails.NoofRoom;
                                MrkRoomrateStr += (MarkList.TotelAmt * SearchDetails.NoofRoom).ToString() + "/";
                                Org_RoomRateStr += Roomsrates.Attribute("rate").Value + "/";
                            }
                            if (Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges") != null)
                            {
                                foreach (var sercharge in Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Element("Surcharges").Descendants("Surcharge"))
                                {
                                    if (sercharge.Attribute("type").Value == "ExtraPersonFee")
                                    {
                                        ExtraMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                        MrkTotalPrice += ExtraMarkList.TotelAmt;
                                        MrkExtraGuest += ExtraMarkList.TotelAmt;
                                        adminMrkAmt += ExtraMarkList.AdminMrkAmt;
                                        AgtMrkAmt += ExtraMarkList.AgentMrkAmt;
                                        SericeTaxAmt += ExtraMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += ExtraMarkList.VenderServiceTaxAmt;
                                        Rate_Org += Convert.ToDecimal(sercharge.Attribute("amount").Value);
                                    }
                                    else
                                    {
                                        TaxMarkList = objHtlMrk.OnlyPercentMrkCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(sercharge.Attribute("amount").Value), SearchDetails.EX_servicetax);
                                        MrkTotalPrice += TaxMarkList.TotelAmt;
                                        MrkTaxes += TaxMarkList.TotelAmt;
                                        adminMrkAmt += TaxMarkList.AdminMrkAmt;
                                        AgtMrkAmt += TaxMarkList.AgentMrkAmt;
                                        SericeTaxAmt += TaxMarkList.AgentServiceTaxAmt;
                                        VSericeTaxAmt += TaxMarkList.VenderServiceTaxAmt;
                                        Rate_Org += Convert.ToDecimal(sercharge.Attribute("amount").Value);
                                    }
                                }
                            }
                            if (Rooms.Element("RateInfos").Element("RateInfo").Attribute("promo").Value == "true")
                                DiscountRate = objHtlMrk.DiscountMarkupCalculation(MarkList.AdminMrkPercent, MarkList.AgentMrkPercent, MarkList.AdminMrkType, MarkList.AgentMrkType, Convert.ToDecimal(Rooms.Element("RateInfos").Element("RateInfo").Element("ChargeableRateInfo").Attribute("averageBaseRate").Value) * SearchDetails.NoofRoom * SearchDetails.NoofNight, 0);
                            if (Rooms.Element("RateInfos").Element("promoDescription") != null)
                                discountmsg = Rooms.Element("RateInfos").Element("RateInfo").Element("promoDescription").Value;

                            //Display Hotel Inclution strat  
                            if (Rooms.Element("ValueAdds") != null)
                                foreach (var inclu in Rooms.Element("ValueAdds").Descendants("ValueAdd"))
                                {
                                    Inclusion += inclu.Element("description").Value + " ,";
                                }
                            //Display Hotel Inclution End
                            if (Rooms.Element("smokingPreferences") != null)
                                smoking = Rooms.Element("smokingPreferences").Value;


                            string RoomDiscription = "";
                            //Room Disciption
                            try
                            {
                                if (j <= HotelDetail.RoomcatList.Count - 1)
                                {
                                    string[] roomdisArr = (string[])HotelDetail.RoomcatList[j];
                                    roomdisArr.Select(x => x[0].ToString() == Rooms.Element("roomTypeCode").Value);
                                    RoomDiscription += roomdisArr[2];
                                }
                            }
                            catch (Exception ex)
                            {
                                HotelDA.InsertHotelErrorLog(ex, "Expidia RoomcatList" + SearchDetails.SearchCity + "_" + SearchDetails.HtlCode);
                            }
                            j++;

                            objRoomList.Add(new RoomList
                            {
                                HotelCode = SearchDetails.HtlCode,
                                RatePlanCode = Rooms.Element("rateCode").Value,////Rooms.Element("RateInfos").Element("RateInfo").Element("RoomGroup").Element("Room").Element("rateKey").Value,// Rooms.Element("rateCode").Value,
                                RoomTypeCode = Rooms.Element("roomTypeCode").Value + "|" + Rooms.Element("RateInfos").Element("RateInfo").Element("RoomGroup").Element("Room").Element("rateKey").Value + "|" + Rooms.Element("BedTypes").Element("BedType").Attribute("id").Value,
                                RoomName = Rooms.Element("roomDescription").Value,
                                discountMsg = discountmsg,
                                DiscountAMT = DiscountRate > 0 ? (DiscountRate + MrkTaxes + MrkExtraGuest) : 0,//DiscountRate + MrkTaxes + MrkExtraGuest
                                Total_Org_Roomrate = Rate_Org,
                                TotalRoomrate = MrkTotalPrice,
                                AdminMarkupPer = MarkList.AdminMrkPercent,
                                AdminMarkupAmt = adminMrkAmt,
                                AdminMarkupType = MarkList.AdminMrkType,
                                AgentMarkupPer = MarkList.AgentMrkPercent,
                                AgentMarkupAmt = AgtMrkAmt,
                                AgentMarkupType = MarkList.AgentMrkType,
                                AgentServiseTaxAmt = SericeTaxAmt,
                                V_ServiseTaxAmt = VSericeTaxAmt,
                                AmountBeforeTax = 0,
                                Taxes = 0,
                                MrkTaxes = MrkTaxes,
                                ExtraGuest_Charge = MrkExtraGuest,
                                Smoking = Rooms.Element("smokingPreferences").Value,
                                inclusions = Inclusion.TrimEnd(','),
                                CancelationPolicy = Rooms.Element("RateInfos").Element("RateInfo").Element("cancellationPolicy") != null ? Rooms.Element("RateInfos").Element("RateInfo").Element("cancellationPolicy").Value : "",
                                OrgRateBreakups = Org_RoomRateStr.TrimEnd('/'),
                                MrkRateBreakups = MrkRoomrateStr.TrimEnd('/'),
                                DiscRoomrateBreakups = "",
                                EssentialInformation = "",
                                RoomDescription = RoomDiscription,
                                ////  EXRateKey =Rooms.Element("RateInfos").Element("RateInfo").Element("RoomGroup").Element("Room").Element("rateKey").Value,
                                Provider = "EX",
                                RoomImage = "http://images.travelnow.com" + Rooms.Parent.Parent.Element("thumbNailUrl").Value
                            });
                        }
                        catch (Exception ex)
                        {
                            objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                            HotelDA.InsertHotelErrorLog(ex, "expidia Rooms adding" + SearchDetails.SearchCity + "_" + SearchDetails.HtlCode);
                        }
                    }
                    #endregion
                    objRoomDetals.RoomDetails = objRoomList;
                    objRoomDetals.SelectedHotelDetail = HotelDetail;
                }
                else
                {
                    objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = "Room Details Not found" });
                    int m = objhtlDa.SP_Htl_InsUpdBookingLog("HotelNotFound", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                }
            }
            catch (Exception ex)
            {
                objRoomList.Add(new RoomList { TotalRoomrate = 0, HtlError = ex.Message });
                int m = objhtlDa.SP_Htl_InsUpdBookingLog("EXCEPTION", SearchDetails.EX_HotelSearchReq, SearchDetails.EX_HotelSearchRes, SearchDetails.AgentID, "HotelInsert");
                HotelDA.InsertHotelErrorLog(ex, "EXRoomDetals" + SearchDetails.SearchCity);
            }
            return objRoomDetals;
        }
    }

   
}
