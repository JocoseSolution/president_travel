﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
namespace HotelShared
{
    public class RoomList
    {
        public string HotelCode { get; set; }
        public string RoomName { get; set; }
        public string RoomTypeCode { get; set; }
        public string RatePlanCode { get; set; }
        public string discountMsg { get; set; }
        public string inclusions { get; set; }
        public string MealInclude { get; set; }
        public string CancelationPolicy { get; set; }
        public string EssentialInformation { get; set; }
        public string Smoking { get; set; }
        public string RoomDescription { get; set; }
        public string RoomImage { get; set; }
        public string maxAdult { get; set; }
        public string maxChild { get; set; }
        public string maxGuest { get; set; }
        public string Refundable { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal Taxes { get; set; }
        public decimal Surcharge { get; set; }
        public decimal DiscountAMT { get; set; }
        public decimal ExtraGuest_Charge { get; set; }
        public decimal Total_Org_Roomrate { get; set; }
        public decimal TotalRoomrate { get; set; }
        public decimal MrkTaxes { get; set; }
        public decimal AdminMarkupPer { get; set; }
        public decimal AdminMarkupAmt { get; set; }
        public string AdminMarkupType { get; set; }
        public decimal AgentMarkupPer { get; set; }
        public decimal AgentMarkupAmt { get; set; }
        public string AgentMarkupType { get; set; }
        public decimal distriMarkupPer { get; set; }
        public decimal distriMarkupAmt { get; set; }
        public string distriMarkupType { get; set; }
        public decimal V_ServiseTaxAmt { get; set; }
        public decimal AgentServiseTaxAmt { get; set; }
        public decimal exchangerate { get; set; }
        public string OrgRateBreakups { get; set; }
        public string MrkRateBreakups { get; set; }
        public string DiscRoomrateBreakups { get; set; }
        public string Room_Error { get; set; }
        public string Provider { get; set; }
        public decimal EXHotelFee { get; set; }
        public string EXRateKey { get; set; }
        public string RoomRateKey { get; set; }
        public List<RacCodeDetails> RacCode { get; set; }
        public string GuaranteeType { get; set; }
        public decimal MrkTotalBaseRate { get; set; }
        public string CurrencyCode { get; set; }
        public decimal AdminCommissionAmt { get; set; }
        public decimal GSTPercentage { get; set; }
        public decimal TDSPercentage { get; set; }
        public decimal RetaintionPer { get; set; }
        public decimal MinCapvalue { get; set; }
        public decimal AgentCommissionAmt { get; set; }
        public decimal GSTAmt { get; set; }
        public decimal TDSAmt { get; set; }
        public decimal SupplierCommissionAmt { get; set; }
        public string SupplierCommissionPer { get; set; }
        public string SupplierCommisionTaxIncluded { get; set; }
        public string OriginalFare { get; set; }
        public List<Tax_Surcharge> Tax_Surcharges { get; set; }
        public bool IsHold { get; set; }
        public DateTime HoldTillDate { get; set; }
        public string HtlError { get; set; }


        ///TBO///

        public string tboroomtypecode { get; set; }
        public string tborateplancod { get; set; }
        public string tboroomtype { get; set; }
        public string tboroomname { get; set; }
        public decimal tboroomprice { get; set; }
        public decimal tbotax { get; set; }
        public decimal tboChildCharge { get; set; }
        public decimal tboOtherCharges { get; set; }
        public decimal tboDiscount { get; set; }
        public decimal tboPublishedPrice { get; set; }
        public decimal tboPublishedPriceRoundedOff { get; set; }
        public decimal tboOfferedPrice { get; set; }
        public decimal tboOfferedPriceRoundedOff { get; set; }
        public decimal tboAgentCommission { get; set; }
        public decimal tboAgentMarkUp { get; set; }
        public decimal tboServiceTax { get; set; }
        public decimal tboTDS { get; set; }
        public decimal tboExtraGuestCharge { get; set; }
        public string tboroomindex { get; set; }
        public string tboinfosource { get; set; }
        public string tbocommonid { get; set; }
        public string tboroomamentities { get; set; }


        ///
    }
    public class RoomType
    {
        public string RoomTypeName { get; set; }
        public string RoomTypeCode { get; set; }
        public string Smoking { get; set; }
        public string RoomDescription { get; set; }
        public string RoomImage { get; set; }
        public string maxAdult { get; set; }
        public string maxChild { get; set; }
        public string maxGuest { get; set; }
    }
    public class RatePlan
    {
        public string RatePlanName { get; set; }
        public string RatePlanCode { get; set; }
        public string discountMsg { get; set; }
        public string inclusions { get; set; }
        public string CancelationPolicy { get; set; }
    }
    public class RoomRate
    {
        public string RatePlanCode { get; set; }
        public string RoomTypeCode { get; set; }
        public decimal DiscountAMT { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal Taxes { get; set; }
        public decimal MrkTaxes { get; set; }
        public decimal ExtraGuest_Charge { get; set; }
        public decimal Total_Org_Roomrate { get; set; }
        public decimal TotalRoomrate { get; set; }
        public decimal AdminMarkupPer { get; set; }
        public decimal AdminMarkupAmt { get; set; }
        public string AdminMarkupType { get; set; }
        public decimal AgentMarkupPer { get; set; }
        public decimal AgentMarkupAmt { get; set; }
        public string AgentMarkupType { get; set; }
        public decimal distriMarkupPer { get; set; }
        public decimal distriMarkupAmt { get; set; }
        public string distriMarkupType { get; set; }
        public decimal V_ServiseTaxAmt { get; set; }
        public decimal ServiseTaxAmt { get; set; }
        public string Org_RoomrateBreakups { get; set; }
        public string Mrk_RoomrateBreakups { get; set; }
        public string Disc_RoomrateBreakups { get; set; }

        public string TaxInfo { get; set; }
        public decimal AdminCommissionAmt { get; set; }
        public decimal TDSPercentage { get; set; }
        public decimal GSTPercentage { get; set; }
        public decimal RetaintionPer { get; set; }
        public decimal MinCapvalue { get; set; }
        public decimal AgentCommissionAmt { get; set; }
        public decimal GSTAmt { get; set; }
        public decimal TDSAmt { get; set; }
        public decimal RetensionCommissionAmt { get; set; }
        public decimal SupplierCommissionAmt { get; set; }
        public string SupplierCommissionPer { get; set; }
        public string SupplierCommisionTaxIncluded { get; set; }


    }
    public class SelectedHotel
    {
        public string HotelName { get; set; }
        public string HotelCode { get; set; }
        public string StarRating { get; set; }
        public string Lati_Longi { get; set; }
        public string Location { get; set; }
        public string HotelAddress { get; set; }
        public string HotelContactNo { get; set; }
        public string HotelFaxNo { get; set; }
        public string ThumbnailUrl { get; set; }
        public string HotelDescription { get; set; }
        public string AmenityDescription { get; set; }
        public string CheckInTime { get; set; }
        public string CheckOutTime { get; set; }
        public string FlexibleCheckIn { get; set; }
        public string NumberOfRooms { get; set; }
        public string ReviewComment { get; set; }
        public string ReviewPostDate { get; set; }
        public string ReviewRating { get; set; }
        public string ReviewerName { get; set; }
        //public List<RoomInfo> RoomcatList { get; set; }
        public ArrayList RoomcatList { get; set; }
        public string HotelImage { get; set; }
        public string Attraction { get; set; }
        public string HotelAmenities { get; set; }
        public string RoomAmenities { get; set; }
        public string HtlError { get; set; }
        public string EXHotelPolicy { get; set; }
        public string HotelChain { get; set; }
        public string HotelResponse { get; set; }
        public string HotelRequest { get; set; }
        public List<CardType> supportCard { get; set; }
        public string HotelSessionID { get; set; }
        public string HotelCityID { get; set; }
        public string HotelCountryId { get; set; }
    }
    public class CitySearch
    {
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public string SearchType { get; set; }
        public string RegionID { get; set; }
        public string NoOfHotel { get; set; }
        public string ZumataRegionID { get; set; }
        public string MTSCity { get; set; }
        public string InnstantCityID { get; set; }
        public string ExpediaRegionID { get; set; }
        public string GRNCityCode { get; set; }
        //   public bool Availability_0Star { get; set; }
    }
}
