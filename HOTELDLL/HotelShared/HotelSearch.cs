﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
namespace HotelShared
{
    public class HotelSearch
    {
        public string selectedvalue { get; set; }
        public string SearchCity { get; set; }
        public string RegionId { get; set; }
        public string ResultType { get; set; }
        public string Currency { get; set; }
        public string HTLCityList { get; set; }
        public string SearchCityCode { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }
        public string SearchType { get; set; }
        public string SearchID { get; set; }
        public string ZumataRegionID { get; set; }
        public string ExpediaRegionID { get; set; }
        public string GRNCityCode { get; set; }
        public string MTSCity { get; set; }
        public string InnstantCityID { get; set; }
        public string HotelCityCode { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string HotelName { get; set; }
        public string HtlCode { get; set; }
        public string HotelchainCode { get; set; }
        public string HotelAddress { get; set; }
        public string StarRating { get; set; }
        public string ThumbImage { get; set; }
        public string HotelContactNo { get; set; }
        public int NoofRoom { get; set; }
        public int NoofNight { get; set; }
        public string GuestDetails { get; set; }
        public int TotAdt { get; set; }
        public int TotChd { get; set; }
        public string ExtraCot { get; set; }
        public DataSet MarkupDS { get; set; }
        public string Sorting { get; set; }
        public string HtlType { get; set; }
        public string Provider { get; set; }
        public decimal CurrancyRate { get; set; }
        public decimal servicetax { get; set; }
        public int ExtraRoom { get; set; }
        public string ExtraRoomType { get; set; }

        public DataSet CommisionDS { get; set; }

        public ArrayList AdtPerRoom { get; set; }
        public ArrayList ChdPerRoom { get; set; }
        public int[,] ChdAge { get; set; }
        public decimal TG_servicetax { get; set; }
        public decimal EX_servicetax { get; set; }
        public decimal ROOMXML_servicetax { get; set; }
        public decimal GTA_servicetax { get; set; }
        public decimal RZ_servicetax { get; set; }



        public string TGUrl { get; set; }
        public string TGUsername { get; set; }
        public string TGPassword { get; set; }
        public string TGPropertyId { get; set; }
        public string TGTrip { get; set; }

        public string GTAClintID { get; set; }
        public string GTAPassword { get; set; }
        public string GTAEmailAddress { get; set; }
        public string GTAURL { get; set; }
        public string GTATrip { get; set; }

        public string RoomXMLURL { get; set; }
        public string RoomXMLUserID { get; set; }
        public string RoomXMLPassword { get; set; }
        public string RoomXMLOrgID { get; set; }
        public string RoomXMLTrip { get; set; }

        public string RezNextUrl { get; set; }
        public string RezNextUserName { get; set; }
        public string RezNextPassword { get; set; }
        public string RezNextSystemId { get; set; }
        public string RezNextTrip { get; set; }

        public string EXURL { get; set; }
        public string EXAPIKEY { get; set; }
        public string EXCID { get; set; }
        public string EXSecretKey { get; set; }
        public string EXHtlInfoReq { get; set; }
        public string EXTrip { get; set; }

        public string TG_HotelSearchReq { get; set; }
        public string TG_HotelSearchRes { get; set; }
        public string GTA_HotelSearchReq { get; set; }
        public string GTA_HotelSearchRes { get; set; }
        public string GTAItemInformationReq { get; set; }
        public string GTAItemInformationRes { get; set; }
        public string RoomXML_HotelSearchReq { get; set; }
        public string RoomXML_HotelSearchRes { get; set; }
        public string EX_HotelSearchReq { get; set; }
        public string EX_HotelSearchRes { get; set; }
        public string RZ_HotelSearchReq { get; set; }
        public string RZ_HotelSearchRes { get; set; }
        public List<HotelResult> HotelList { get; set; }

        /// <TBO Properties>
        public string cityid { get; set; }
        public string tbo_Resultindex { get; set; }
        public decimal tbotax { get; set; }
        public decimal tboChildCharge { get; set; }
        public decimal tboOtherCharges { get; set; }
        public decimal tboDiscount { get; set; }
        public decimal tboPublishedPrice { get; set; }
        public decimal tboPublishedPriceRoundedOff { get; set; }
        public decimal tboOfferedPrice { get; set; }
        public decimal tboOfferedPriceRoundedOff { get; set; }
        public decimal tboAgentCommission { get; set; }
        public decimal tboAgentMarkUp { get; set; }
        public decimal tboServiceTax { get; set; }
        public decimal tboTDS { get; set; }
        public decimal tboExtraGuestCharge { get; set; }
        public string tboroomindex { get; set; }

        public string TBOROOMTYPECODE { get; set; }
        public static string resultindex { get; set; }
        public static string TRACEID { get; set; }
        public static string TBOSEARCHURL { get; set; }
        public static DateTime datetime { get; set; }
        public static double hours { get; set; }



        public string TBOURL { get; set; }
        public string TBOUserID { get; set; }
        public string TBOPassword { get; set; }
        public string TBOOrgID { get; set; }
        public string TBOTrip { get; set; }
        public string TBOIP { get; set; }
        public string TBOroomdetail { get; set; }
        public string TBOEmailID { get; set; }


        public string tbo_roomsearchreq { get; set; }
        public static string TokenId { get; set; }
        public string TBO_HotelSearchReq { get; set; }
        public string TBO_HotelSearchRes { get; set; }

        public decimal TBO_servicetax { get; set; }
        /// </summary>

        public string LoginID { get; set; }
        public string AgentID { get; set; }
        public string Status { get; set; }
        public string CorporateID { get; set; }
        public string Orderid { get; set; }
        public string ProBookingID { get; set; }
        public string BookingID { get; set; }
        public string ReferenceNo { get; set; }
        public string IPAddress { get; set; }
        public string SessionID { get; set; }
        public string AgencyGroup { get; set; }

        public string PGTitle { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PGAddress { get; set; }
        public string PGCity { get; set; }
        public string PGState { get; set; }
        public string PGCountry { get; set; }
        public string PGPin { get; set; }
        public string PGEmail { get; set; }
        public string PGContact { get; set; }
        public int NoofHotel { get; set; }
        public string ReviewRating { get; set; }

        public List<RacCodeDetails> RacCode { get; set; }
        public List<HotelCredencials> CredencialsList { get; set; }

        public string Zumata_HotelSearchReq { get; set; }
        public string Zumata_HotelSearchRes { get; set; }
        public string Innstant_HotelSearchRes { get; set; }
        public string Innstant_HotelSearchReq { get; set; }
        public string GRN_HotelSearchRes { get; set; }
        public string GRN_HotelSearchReq { get; set; }
        public string Expedia_HotelSearchRes { get; set; }
        public string Expedia_HotelSearchReq { get; set; }
        public string GWS_HotelSearchReq { get; set; }
        public string GWS_HotelSearchRes { get; set; }
        public string GAL_HotelSearchReq { get; set; }
        public string GAL_HotelSearchRes { get; set; }
        public string GALWithoutRacHotelSearchRes { get; set; }
        public string GALWithoutRacHotelSearchReq { get; set; }
        public string MTS_HotelSearchRes { get; set; }
        public string MTS_HotelSearchReq { get; set; }

        public string SuperShopper_HotelSearchReq { get; set; }
        public string SuperShopper_HotelSearchRes { get; set; }
        public string SuperShopperWithoutRacHotelSearchRes { get; set; }
        public string SuperShopperWithoutRacHotelSearchReq { get; set; }
        public string Agoda_HotelSearchReq { get; set; }
        public string Agoda_HotelSearchRes { get; set; }
        public string Seeingo_HotelSearchReq { get; set; }
        public string Seeingo_HotelSearchRes { get; set; }
        public string OYO_HotelSearchReq { get; set; }
        public string OYO_HotelSearchRes { get; set; }

        public string AdminMrkType { get; set; }
        public decimal AdminMrkPer { get; set; }
        public string AgentMrkType { get; set; }
        public decimal AgentMrkPer { get; set; }
        public string DistrMrkType { get; set; }
        public decimal DistrMrkPer { get; set; }
        public decimal MrkAmt { get; set; }
        public string HtlRoomCode { get; set; }
        public string BaseURL { get; set; }
        public string ZUMATAHotelDetailRes { get; set; }
        public string ZUMATAHotelDetailReq { get; set; }
        public string INNSTANTHotelDetailReq { get; set; }
        public string INNSTANTHotelDetailRes { get; set; }
        public string GWSHotelDetailReq { get; set; }
        public string GWSHotelDetailRes { get; set; }
        public string GALHotelDetailReq { get; set; }
        public string GALHotelDetailRes { get; set; }
        public string MTSHotelDetailReq { get; set; }
        public string MTSHotelDetailRes { get; set; }
        public string GTAHotelDetailReq { get; set; }
        public string GTAHotelDetailRes { get; set; }
        public string GRNHotelDetailReq { get; set; }
        public string GRNHotelDetailRes { get; set; }
        public string GTAPolicyReq { get; set; }
        public string GTAPolicyResp { get; set; }
        public string AgodaHotelDetailReq { get; set; }
        public string AgodaHotelDetailRes { get; set; }
        public string SeeingoHotelDetailReq { get; set; }
        public string SeeingoHotelDetailRes { get; set; }
        public string OYOHotelDetailReq { get; set; }
        public string OYOHotelDetailRes { get; set; }


        public string TGXmlHeader
        {
            get { return "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>"; }
        }
        public string TGXmlFooter
        {
            get { return "</soap:Body></soap:Envelope>"; }
        }
        string _TGHotelSeachHeader = null;
        public string TGHotelSeachHeader
        {
            get
            {
                _TGHotelSeachHeader = "<OTA_HotelAvailRQ xmlns='http://www.opentravel.org/OTA/2003/05' RequestedCurrency='INR' SortOrder='DEALS' Version='0.0' PrimaryLangID='en' SearchCacheLevel='Live'>";
                return _TGHotelSeachHeader += "<AvailRequestSegments><AvailRequestSegment><HotelSearchCriteria><Criterion>";
            }
        }
        public string TGHotelSeachFooter
        {
            //get { return "<Promotion Type='BOTH' Name='ALLPromotions' /></TPA_Extensions></Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ>"; }
            get { return "</TPA_Extensions></Criterion></HotelSearchCriteria></AvailRequestSegment></AvailRequestSegments></OTA_HotelAvailRQ>"; }
        }

        public string GTAXmlHeader
        {
            get { return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Source>"; }
        }
        public string GTARequestMode
        {
            get { return "<RequestMode>SYNCHRONOUS</RequestMode></RequestorPreferences></Source><RequestDetails>"; }
        }
        public string GTAXmlFooter
        {
            get { return "</RequestDetails></Request>"; }
        }
        public string RZXmlHeader
        {
            get { return "<Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:wsa=\"http://www.w3.org/2005/08/addressing\">"; }
        }


        public string ProBookingReq { get; set; }
        public string ProBookingRes { get; set; }
        public string BookingConfReq { get; set; }
        public string BookingConfRes { get; set; }
        public string BookingCancelReq { get; set; }
        public string BookingCancelRes { get; set; }



    }


}
