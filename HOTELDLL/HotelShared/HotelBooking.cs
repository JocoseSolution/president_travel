﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace HotelShared
{
    public class HotelBooking
    {
        public string HotelCityList { get; set; }
        public string CityName { get; set; }
        public string CityCode { get; set; }
        public string CountryCode { get; set; }
        public string Country { get; set; }
        public string SearchType { get; set; }
        public string HotelLocation { get; set; }
        public string RoomRPH { get; set; }
        public string BookingType { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public string HotelName { get; set; }
        public string HtlCode { get; set; }
        public string HotelChain { get; set; }
        public string StarRating { get; set; }
        public string RoomName { get; set; }
        public string RoomPlanCode { get; set; }
        public string RoomTypeCode { get; set; }
        public string HotelAddress { get; set; }
        public string SharingBedding { get; set; }
        public string Refundable { get; set; }
        public int NoofRoom { get; set; }
        public int NoofNight { get; set; }
        public string GuestDetails { get; set; }
        public int TotAdt { get; set; }
        public int TotChd { get; set; }
        public string ExtraCot { get; set; }
        public int ExtraRoom { get; set; }
        public string MealInclude { get; set; }
        public string CorporateID { get; set; }

        public ArrayList AdtPerRoom { get; set; }
        public ArrayList ChdPerRoom { get; set; }
        public int[,] ChdAge { get; set; }
        public ArrayList AdditinalGuest { get; set; }

        public string HtlType { get; set; }
        public string Provider { get; set; }
        public decimal CurrancyRate { get; set; }
        public decimal servicetax { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal Taxes { get; set; }
        public decimal ExtraGuestTax { get; set; }
        public decimal DiscountAMT { get; set; }
        public decimal Total_Org_Roomrate { get; set; }
        public decimal TotalRoomrate { get; set; }
        public string ThumbImg { get; set; }
        public string Currency { get; set; }
        public string AgencyName { get; set; }
        public string AgentID { get; set; }
        public string AdminMrkType { get; set; }
        public decimal AdminMrkPer { get; set; }
        public decimal AdminMrkAmt { get; set; }
        public string AgentMrkType { get; set; }
        public decimal AgentMrkPer { get; set; }
        public decimal AgentMrkAmt { get; set; }
        public string DistrMrkType { get; set; }
        public decimal DistrMrkPer { get; set; }
        public decimal DistrMrkAmt { get; set; }
        public decimal V_ServiseTaxAmt { get; set; }
        public decimal ServiseTaxAmt { get; set; }
        public decimal MrkAmt { get; set; }
        public string CommisionType { get; set; }
        public decimal CommisionPer { get; set; }
        public decimal CommisionAmt { get; set; }
        public decimal AgentDebitAmt { get; set; }
        public string TaxType { get; set; }
        public string RacCode { get; set; }
        public string HotelPolicy { get; set; }
        public string HotelContactNo { get; set; }

        public string Status { get; set; }
        public string LoginID { get; set; }
        public string Orderid { get; set; }
        public string ProBookingID { get; set; }
        public string BookingID { get; set; }
        public string ReferenceNo { get; set; }
        public string IPAddress { get; set; }
        public string SessionID { get; set; }

        public string TGUrl { get; set; }
        public string TGUsername { get; set; }
        public string TGPassword { get; set; }
        public string TGPropertyId { get; set; }

        public string GTAURL { get; set; }
        public string GTAClintID { get; set; }
        public string GTAPassword { get; set; }
        public string GTAEmailAddress { get; set; }

        public string RegionId { get; set; }
        public string RoomXMLURL { get; set; }
        public string RoomXMLUserID { get; set; }
        public string RoomXMLPassword { get; set; }
        public string RoomXMLOrgID { get; set; }

        public string EXURL { get; set; }
        public string EXAPIKEY { get; set; }
        public string EXCID { get; set; }

        public string RezNextUrl { get; set; }
        public string RezNextUserName { get; set; }
        public string RezNextPassword { get; set; }
        public string RezNextSystemId { get; set; }

        public string HotelUrl { get; set; }
        public string HotelUsername { get; set; }
        public string HotelPassword { get; set; }
        public string HotelCompanyID { get; set; }
        public string CancelUrl { get; set; }
        public string HotelBookingUrl { get; set; }
        public string OtherData { get; set; }

        public string HotelSearchReq { get; set; }
        public string HotelSearchRes { get; set; }
        public string ProBookingReq { get; set; }
        public string ProBookingRes { get; set; }
        public string BookingConfReq { get; set; }
        public string BookingConfRes { get; set; }
        public string BookingCancelReq { get; set; }
        public string BookingCancelRes { get; set; }

        public string PGTitle { get; set; }
        public string PGFirstName { get; set; }
        public string PGLastName { get; set; }
        public string PGAddress { get; set; }
        public string PGCity { get; set; }
        public string PGState { get; set; }
        public string PGCountry { get; set; }
        public string PGPin { get; set; }
        public string PGEmail { get; set; }
        public string PGContact { get; set; }
        public string PGNationality { get; set; }
        public string PgCharges { get; set; }
        public string PaymentMode { get; set; }
        public GSTAgencyInformation GSTAgencyInfo { get; set; }
        public CreditCardDetails GDSCreditCardInfo { get; set; }
        public string HotelErrorMsg { get; set; }
        public string ActualErrorMsg { get; set; }
        public string MembershipNo { get; set; }
        public string CustomerRemark { get; set; }
        public string HotelInformation { get; set; }
        public string URILocator { get; set; }
        public string PRILocator { get; set; }
        public string OwningPCC { get; set; }
        public string HRBookingConfirmation { get; set; }
        public string BSIataNumber { get; set; }
        public string Version { get; set; }
        public string SelectedRoomsData { get; set; }
        public string GuaranteeType { get; set; }
        public List<PaxInfo> PaxInformation { get; set; }
        public Dictionary<string, string> RoomReference { get; set; }
        public List<RoomList> SelectedRoomList { get; set; }
        public bool IsHold { get; set; }
        public DateTime HoldTillDate { get; set; }

        /// <TBO PROPERTIES>
        public string resultindex { get; set; }
        public List<RoomList> RoomDetails { get; set; }

        public string TBOURL { get; set; }
        public string TBOUserID { get; set; }
        public string TBOPassword { get; set; }
        public string TBOOrgID { get; set; }

        public string tboroomname { get; set; }
        public ArrayList tbotax { get; set; }
        public ArrayList tboChildCharge { get; set; }
        public ArrayList tboOtherCharges { get; set; }
        public ArrayList tboDiscount { get; set; }
        public ArrayList tboPublishedPrice { get; set; }
        public ArrayList tboPublishedPriceRoundedOff { get; set; }
        public ArrayList tboOfferedPrice { get; set; }
        public ArrayList tboOfferedPriceRoundedOff { get; set; }
        public ArrayList tboAgentCommission { get; set; }
        public ArrayList tboAgentMarkUp { get; set; }
        public ArrayList tboServiceTax { get; set; }
        public ArrayList tboTDS { get; set; }
        public ArrayList tboExtraGuestCharge { get; set; }
        public ArrayList tboroomprice { get; set; }
        public ArrayList tboroomindex { get; set; }
        public ArrayList tboRatePlanCod { get; set; }
        public ArrayList tboRoomTypeCode { get; set; }
        public string tbocommoid { get; set; }




        /// </summary>

        public string TGXmlHeader
        {
            get { return "<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'><soap:Body>"; }
        }
        public string TGXmlFooter
        {
            get { return "<ResGlobalInfo><Guarantee GuaranteeType='PrePay'/></ResGlobalInfo></HotelReservation></HotelReservations></OTA_HotelResRQ></soap:Body></soap:Envelope>"; }
        }
        public string TGHotelBookingHeader
        {
            get { return "<OTA_HotelResRQ xmlns='http://www.opentravel.org/OTA/2003/05' CorrelationID='Orderids' TransactionIdentifier='' Version='1.003'><POS><Source ISOCurrency='INR'>"; }
        }

        public string GTAXmlHeader
        {
            get { return "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Request><Source>"; }
        }
        public string GTARequestMode
        {
            get { return "<RequestMode>SYNCHRONOUS</RequestMode></RequestorPreferences></Source><RequestDetails>"; }
        }
        public string GTAXmlFooter
        {
            get { return "</RequestDetails></Request>"; }
        }
        public string EXSecretKey { get; set; }
        public string EXRateKey { get; set; }
        public string EXBedTypeId { get; set; }
        public string EXSmoking { get; set; }
    }

    public class GSTAgencyInformation
    {
        public string GSTNumber { get; set; }
        public string GSTCompanyName { get; set; }
        public string GSTCompanyEmailId { get; set; }
        public string GSTCompanyAddress { get; set; }
        public string GSTCity { get; set; }
        public string GSTPinCode { get; set; }
        public string GSTState { get; set; }
        public string GSTPhoneISD { get; set; }
        public string GSTPhoneNumber { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public string CustomerState { get; set; }
        public bool Is_GST_Apply { get; set; }
    }
    public class PaxInfo
    {
        public string Title { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string Gender { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string DOB { get; set; }
        public string PaxType { get; set; }
        public string PNationality { get; set; }
        public string RoomNo { get; set; }

    }
}
