﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelShared
{
    public class HotelCancellation
    {
        public string HotelName { get; set; }
        public string HotelCode { get; set; }
        public string CheckInDate { get; set; }
        public string CheckOutDate { get; set; }
        public int NoofRoom { get; set; }
        public int NoofNight { get; set; }
        public int NightForCancel { get; set; }
        public string HtlType { get; set; }
        public string Provider { get; set; }
        public string LoginID { get; set; }
        public string AgentID { get; set; }
        public string AgencyName { get; set; }
        public decimal AmountBeforeTax { get; set; }
        public decimal Taxes { get; set; }
        public decimal DiscountAMT { get; set; }
        public decimal Total_Org_Roomrate { get; set; }
        public decimal CancellationCharge { get; set; }
        public string AdminMrkType { get; set; }
        public decimal AdminMrkPer { get; set; }
        public decimal AdminMrkAmt { get; set; }
        public decimal servicetax { get; set; }
        public decimal ServiseTaxAmt { get; set; }
        public decimal RefundAmt { get; set; }
        public decimal SupplierRefundAmt { get; set; }
        public decimal CurrancyRate { get; set; }
        public string CancelStatus { get; set; }
        public string HotelPolicy { get; set; }
        public string Orderid { get; set; }
        public string CancellationID { get; set; }
        public string BookingID { get; set; }
        public string ReferenceNo { get; set; }
        public string IPAddress { get; set; }
        public string PGTitle { get; set; }
        public string PGFirstName { get; set; }
        public string PGLastName { get; set; }
        public string PGEmail { get; set; }
        public string HotelUrl { get; set; }
        public string Can_UserID { get; set; }
        public string Can_Password { get; set; }
        public string Can_PropertyId { get; set; }
        public string CancelUrl { get; set; }
        public string HotelBookingUrl { get; set; }
        public string OtherData { get; set; }
        public string BookingCancelReq { get; set; }
        public string BookingCancelRes { get; set; }
        public string CancellationType { get; set; }
        public string CancellationRemark { get; set; }
        public string ConfirmationNo { get; set; }
        public string PGContact { get; set; }
        public string tboremark { get; set; }
    }

    public class CancelationPolicy
    {
        public string CancelationPolicyReq { get; set; }
        public string CancelationPolicyResp { get; set; }
        public string PolicyID { get; set; }
        public string CancelationPolicyInfo { get; set; }
         public string HotelPolicy { get; set; }
         public string OtherInfo { get; set; }
         public bool Bookable { get; set; }
         public bool IsHold { get; set; }
         public string AlertInfo { get; set; }
         public RoomList RoomRateDetails { get; set; }
         public DateTime HoldTilDate { get; set; }
    }
}
