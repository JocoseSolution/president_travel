﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelShared
{
    public class HotelComposite
    {
        public HotelSearch HotelSearchDetail { get; set; }
        public List<HotelResult> Hotelresults { get; set; }
        public List<HotelResult> HotelOrgList { get; set; }
    }
    public class HotelCompositeList
    {
        public List<HotelCompositeNew> objHotelALLLists { get; set; }
    }
    public class HotelCompositeNew
    {
        public String HotelSearchRequest { get; set; }
        public String HotelSearchResponse { get; set; }
        public List<HotelResult> Hotelresults { get; set; }
        public string provider { get; set; }
        public string OtherInfo { get; set; }
        public string SessionID { get; set; }

    }
    public class AllCombindHotel
    {
        public HotelSearch AllHotelSearch { get; set; }
        public List<HotelResult> TGHotelList { get; set; }
        public List<HotelResult> GTAHotelList { get; set; }
        public List<HotelResult> ROOMXMLHotelList { get; set; }
        public List<HotelResult> ZUMATAHotelList { get; set; }
        public List<HotelResult> MTSHotelList { get; set; }
        public List<HotelResult> InnstantHotelList { get; set; }
        public List<HotelResult> GALHotelList { get; set; }
        public List<HotelResult> GALNormalHotelList { get; set; }
        public List<HotelResult> GWSHotelList { get; set; }
        public List<HotelResult> ExpediaHotelList { get; set; }
        public List<HotelResult> GRNHotelList { get; set; }
         public List<HotelResult> SuperShopperHotelList { get; set; }
         public List<HotelResult> SuperShopperNormalHotelList { get; set; }
         public List<HotelResult> OffLineHotelList { get; set; }
         public List<HotelResult> AgodaHotelList { get; set; }
         public List<HotelResult> SeeingoHotelList { get; set; }
         public List<HotelResult> OYOHotelList { get; set; }
         public List<HotelResult> TBOHotelLst { get; set; }
    }
    public class RoomComposite
    {
        public SelectedHotel SelectedHotelDetail { get; set; }
        public List<RoomList> RoomDetails { get; set; }
        public List<RoomList> tboRoomDetail { get; set; }
    }
    public class RacCodeDetails
    {
        public string Rac_Code { get; set; }
        public string Provider { get; set; }
    }
    public class CardType
    {
        public string Code { get; set; }
        public string name { get; set; }
    }
    public class HotelCredencials
    {
        public string HotelUrl { get; set; }
        public string HotelUsername { get; set; }
        public string HotelPassword { get; set; }
        public string HotelCompanyID { get; set; }
        public string BookingUrl { get; set; }
        public string CancelUrl { get; set; }
        public string HotelBookingUrl { get; set; }
        public string OtherData { get; set; }
        public string HotelTrip { get; set; }
        public string HotelProvider { get; set; }
        public bool Status { get; set; }
        public int Counter { get; set; }
    }
    public class CurrencyRate
    {
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string CurrancyCode { get; set; }
        public string CurrancyName { get; set; }
        public string CurrancySymble { get; set; }
        public decimal ExchangeRate { get; set; }
        public string UpdateDate { get; set; }
        public int IDS { get; set; }
    }
    public class CurrencyRateSetting
    {
        public string Code { get; set; }
        public string CountryName { get; set; }
        public string CountryCode { get; set; }
        public string CurrancyCode { get; set; }
        public decimal ExchangeRate { get; set; }
        public string UpdateDate { get; set; }
        public string UpdatedBy { get; set; }
        public string IPAddress { get; set; }
        public string ReqType { get; set; }
        public int IDS { get; set; }
        public List<CurrencyRate> CurrancyList { get; set; }
    }
    public class CreditCardDetails
    {
        public string CardType { get; set; }
        public string CardNo { get; set; }
        public string ExpMonth { get; set; }
        public string ExpYear { get; set; }
        public int CardCVV { get; set; }
        public string holderName { get; set; }
        public string Orderid { get; set; }
        public string HotelSupplier { get; set; }
        public string CorporateName { get; set; }
    }
    public class Tax_Surcharge
    {
        public string TaxName { get; set; }
        public string TaxType { get; set; }       
        public string TaxCode { get; set; }
        public Decimal TaxAmountInclude { get; set; }
        public Decimal TaxAmountExclude { get; set; }
    }
    public class RoomInfo
    {
        public string RoomPlanCode { get; set; }
        public string RoomTypeCode { get; set; }
        public string name { get; set; }
        public string RoomDesc { get; set; }
    }
}
