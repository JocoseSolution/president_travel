﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HotelShared
{
   public class HotelResult
   {
        public string RoomName { get; set; }
        public string RatePlanCode { get; set; }
        public string RoomTypeCode { get; set; }
        public decimal BaseFare { get; set; }
        public decimal Taxes { get; set; }
        public decimal hotelDiscoutAmt { get; set; }
        public decimal ExtraGuest_Charge { get; set; }
        public decimal hotelPrice { get; set; }
        public decimal AgtMrk { get; set; }
        public string DiscountMsg { get; set; }
        public string inclusions { get; set; }
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public string HotelCity { get; set; }
        public string HotelCityCode { get; set; }
        public string StarRating { get; set; }
        public string HotelAddress { get; set; }
        public string HotelDescription { get; set; }
        public string HotelServices { get; set; }
        public string HotelThumbnailImg { get; set; }
        public string ReviewRating { get; set; }
        public string Lati_Longi { get; set; }
        public string Location { get; set; }
        public int PopulerId { get; set; }
        public string HtlError { get; set; }
        public string Provider { get; set; }
        public string HotelCountryId { get; set; }
        public List<RacCodeDetails> RacCode { get; set; }
        public string RateType { get; set; }
        public string HotelChain { get; set; }
        public string Currency { get; set; }
        public string ResultIndex { get; set; }
       

    }
}
