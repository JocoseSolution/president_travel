﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Net
Imports System.IO
Imports System.Web
Imports System.Data.SqlClient
Imports System.Configuration
Imports System.Data
Public Class SMS
    Private result As String = "NOT IN USE"
    Private request As WebRequest
    Private response As HttpWebResponse
    Private con As SqlConnection
    Private adap As SqlDataAdapter
    Private cmd As SqlCommand

    Public Sub New()
        con = New SqlConnection(ConfigurationManager.ConnectionStrings("myAmdDB").ConnectionString)
    End Sub
    Public Function agency_id(ByVal orderid As String) As DataSet
        Dim ds As New DataSet()

        adap = New SqlDataAdapter("SP_SMSFULLNAMESEND", con)
        adap.SelectCommand.CommandType = CommandType.StoredProcedure
        adap.SelectCommand.Parameters.AddWithValue("@OrderID", orderid)
        adap.Fill(ds)
        Return ds
    End Function
    Public Function sendSms(ByVal orderid As String, ByVal mobno As String, ByVal sector As String, ByVal Airno As String, ByVal FltNo As String, ByVal depdt As String, _
     ByVal pnr As String, ByRef smstext As String, ByVal DtCrd As DataTable) As String
        Dim sendToPhoneNumber As String = mobno
        Dim m As String = ""
        Dim CUSName As String = ""
        Dim HOURSS As String = ""
        Try

            Dim dss As New DataSet()
            dss = agency_id(orderid)
            CUSName = dss.Tables(0).Rows(0)("fullname").ToString
            HOURSS = dss.Tables(0).Rows(0)("HOURSS").ToString
            depdt = Convert.ToDateTime(Convert.ToString(dss.Tables(0).Rows(0)("DEPDATE"))).ToString("yyyy-MM-dd")
            If pnr.Contains("-FQ") Or pnr.Contains("FQ-") Then
                m = "Dear " & CUSName & ",Onward Tkt Status:PENDING : JDetails:" & sector.Replace(":", " To ") & " on  " & depdt & ":" & Airno & " " & FltNo & " at " & HOURSS.Replace(":", "") & " Hrs, Airline Pnr:" & pnr & "."
            Else
                m = "Dear " & CUSName & ",Onward Tkt Status:CONFIRMED : JDetails:" & sector.Replace(":", " To ") & " on  " & depdt & ":" & Airno & " " & FltNo & " at " & HOURSS.Replace(":", "") & " Hrs, Airline Pnr:" & pnr & "."
            End If

            ''Dear MR ASHISH SHARMA***Onward Tkt***Status:CONFIRMED : JDetails:GOI to DEL on 2018-04-07: SGF 8172 at 1435 Hrs, Airline Pnr: ADJ1KK 
        Catch ex As Exception
            m = "Dear Customer,Your booking for " & sector & " is confirmed on " & Airno & "-" & FltNo & " for " & " departure date " & depdt & ". Your airline PNR No. is " & pnr & "."
        End Try
        smstext = m
        Dim m1 As String = HttpUtility.UrlEncode(m)

        Dim UrlLink As String = DtCrd.Rows(0)("Url")
        Dim FeedId As String = DtCrd.Rows(0)("FeedId")
        Dim username As String = DtCrd.Rows(0)("UserId")
        Dim password As String = DtCrd.Rows(0)("Password")
        Dim ToMob As String = mobno
        Dim Text As String = m1
        Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        Dim senderid As String = DtCrd.Rows(0)("SenderId")
        'Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password=" & password & "&To=" & ToMob & "&Text=" & Text & "&time=" & time & "&senderid=" & senderid
        Dim url As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & ToMob & "&msg=" & Text

        Dim AdminMobileNo As String = "9825326799"
        Dim adminurl As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & AdminMobileNo & "&msg=" & Text


        Dim Adminurldetail As String = sendtoadmin(adminurl)

        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result & url
        Return result
    End Function

    'Public Function sendSms(ByVal orderid As String, ByVal mobno As String, ByVal sector As String, ByVal Airno As String, ByVal FltNo As String, ByVal depdt As String, _
    ' ByVal pnr As String, ByRef smstext As String, ByVal DtCrd As DataTable) As String
    '    Dim sendToPhoneNumber As String = mobno

    '    Dim m As String = "Dear Customer,Your booking for " & sector & " is confirmed on " & Airno & "-" & FltNo & " for " & " departure date " & depdt & ". Your airline PNR No. is " & pnr & "."
    '    smstext = m
    '    Dim m1 As String = HttpUtility.UrlEncode(m)

    '    Dim UrlLink As String = DtCrd.Rows(0)("Url")
    '    Dim FeedId As String = DtCrd.Rows(0)("FeedId")
    '    Dim username As String = DtCrd.Rows(0)("UserId")
    '    Dim password As String = DtCrd.Rows(0)("Password")
    '    Dim ToMob As String = mobno
    '    Dim Text As String = m1
    '    Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
    '    Dim senderid As String = DtCrd.Rows(0)("SenderId")
    '    Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid

    '    Try
    '        Dim client As New WebClient()
    '        Dim data As Stream = client.OpenRead(url)
    '        Dim reader As New StreamReader(data)
    '        result = reader.ReadToEnd()
    '        data.Close()
    '        reader.Close()
    '    Catch ex As Exception
    '        result = ex.InnerException.Message
    '    End Try
    '    result = result & url
    '    Return result
    'End Function
    Public Function sendUploadSms(ByVal orderid As String, ByVal mobno As String, ByVal sector As String, ByVal Airno As String, ByVal FltNo As String, ByVal depdt As String, _
     ByVal pnr As String, ByRef smstext As String, ByVal DtCrd As DataTable) As String
        'Dim sendToPhoneNumber As String = mobno
        'Dim userid As String = "springtravels" '"demvfnorthttp16" ' "springtravels"    '"demvfnorthttp16"
        'Dim passwd As String = "spring12trav" '"spring321" '"spring@123"       '"spring321"
        ' ''Dim senderid As String = "SPRING"
        'Dim route As String = "11" 'For Transactional Route
        'Dim apipassword = "bbd107mmpmd5tn00g"
        Dim m As String = "Dear Customer,Your booking for " & sector & " is confirmed on " & Airno & "-" & FltNo & " for " & " departure date " & depdt & ". Your airline PNR No. is " & pnr & "."
        smstext = m
        Dim m1 As String = HttpUtility.UrlEncode(m)

        ''Dim url As String = "http://www.myvaluefirst.com/smpp/sendsms?username=" & userid & "&password=" & passwd & "&to=" & mobno & "&from=TICKET" & "&text=" & m1

        'Dim FeedId As String = "339159"
        'Dim username As String = "9911333666"
        'Dim password As String = "jgjpd"
        'Dim ToMob As String = mobno
        'Dim Text As String = m1
        'Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        'Dim senderid As String = "testSenderID"
        'Dim url As String = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid
        Dim UrlLink As String = DtCrd.Rows(0)("Url")
        Dim FeedId As String = DtCrd.Rows(0)("FeedId")
        Dim username As String = DtCrd.Rows(0)("UserId")
        Dim password As String = DtCrd.Rows(0)("Password")
        Dim ToMob As String = mobno
        Dim Text As String = m1
        Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        Dim senderid As String = DtCrd.Rows(0)("SenderId")
        'Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid
        Dim url As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & ToMob & "&msg=" & Text

        Dim AdminMobileNo As String = "9825326799"
        Dim adminurl As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & AdminMobileNo & "&msg=" & Text
        Dim Adminurldetail As String = sendtoadmin(adminurl)

        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result
        Return result
    End Function
    Public Function SendBusSms(ByVal orderid As String, ByVal PnrNo As String, ByVal MobNo As String, ByVal Sector As String, ByVal JourneyDate As String, ByVal SeatNo As String, ByVal Type As String, ByVal MSGBookingType As String, ByVal DtCrd As DataTable) As String
        Dim smstext As String
        Dim m As String
        If MSGBookingType = "Book" Then
            m = "Dear Customer,Your Bus booking confirmed for " & Sector & " on " & " departure date " & JourneyDate & " seat no " & SeatNo & ". Your bus PNR No. is " & PnrNo & "."
        ElseIf MSGBookingType = "Cancel" Then
            m = "Dear Customer,Your Bus cancellation is process for " & Sector & " on " & " departure date " & JourneyDate & ". Your bus PNR No. is " & PnrNo & "."
        End If
        smstext = m
        Dim m1 As String = HttpUtility.UrlEncode(m)
        Dim UrlLink As String = DtCrd.Rows(0)("Url")
        Dim FeedId As String = DtCrd.Rows(0)("FeedId")
        Dim username As String = DtCrd.Rows(0)("UserId")
        Dim password As String = DtCrd.Rows(0)("Password")
        Dim ToMob As String = MobNo
        Dim Text As String = m1
        Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        Dim senderid As String = DtCrd.Rows(0)("SenderId")
        'Dim url As String = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid
        'Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid
        Dim url As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & ToMob & "&msg=" & Text

        Dim AdminMobileNo As String = "9825326799"
        Dim adminurl As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & AdminMobileNo & "&msg=" & Text
        Dim Adminurldetail As String = sendtoadmin(adminurl)

        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result & url
        Return result
    End Function
    Public Function SendHotelSms(ByVal orderid As String, ByVal PnrNo As String, ByVal MobNo As String, ByVal CityName As String, ByVal CheckinDate As String, ByVal CheckoutDate As String, ByVal MSGBookingType As String, ByRef smstext As String, ByVal DtCrd As DataTable) As String

        'If MSGBookingType = "Book" Then
        '    smstext = "Dear Customer,Your Hotel booking for " & CityName & " is confirmed." & " Checkin date " & CheckinDate & " Checkout Date " & CheckoutDate & " Boooking ID is " & PnrNo & "."
        'ElseIf MSGBookingType = "Cancel" Then
        '    smstext = "Dear Customer, Your hotel cancellation for " & CityName & " is process." & " Checkin date " & CheckinDate & " Checkout Date " & CheckoutDate & " Boooking ID is " & PnrNo & "."
        'End If
        If MSGBookingType = "Book" Then
            smstext = "Your Hotel Booking at " & CityName & " is confirmed. " & "Checkin date " & CheckinDate & ", Checkout Date " & CheckoutDate & ", Boooking ID " & PnrNo & ", reference no " & orderid & "."
        ElseIf MSGBookingType = "Cancel" Then
            smstext = "Your Hotel booking at " & CityName & " is Canceled. " & "Checkin date " & CheckinDate & ", Checkout Date " & CheckoutDate & ", Boooking ID " & PnrNo & ", reference no " & orderid & "."
        ElseIf MSGBookingType = "Hold" Then
            smstext = "Your Hotel Booking at " & CityName & " is on Hold. Reference no is " & orderid & ". Please contact our customer care."
        ElseIf MSGBookingType = "Reject" Then
            smstext = "Your Hotel booking of " & CityName & " is Rejected. Reference no is " & orderid & ". Please contact our customer care."
        ElseIf MSGBookingType = "CancelInprocess" Then
            smstext = "Your Hotel booking of " & CityName & " is Cancellation Inprocess. Reference no is " & orderid & ". Please contact our customer care."
        End If

        Dim m1 As String = HttpUtility.UrlEncode(smstext)

        'Dim FeedId As String = "339159"
        'Dim username As String = "9911333666"
        'Dim password As String = "jgjpd"
        'Dim ToMob As String = MobNo
        'Dim Text As String = m1
        'Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        'Dim senderid As String = "testSenderID"
        'Dim url As String = "http://bulkpush.mytoday.com/BulkSms/SingleMsgApi?feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid

        Dim UrlLink As String = DtCrd.Rows(0)("Url")
        Dim FeedId As String = DtCrd.Rows(0)("FeedId")
        Dim username As String = DtCrd.Rows(0)("UserId")
        Dim password As String = DtCrd.Rows(0)("Password")
        Dim ToMob As String = MobNo
        Dim Text As String = m1
        Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        Dim senderid As String = DtCrd.Rows(0)("SenderId")
        Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid

        Dim AdminMobileNo As String = "9825326799"
        Dim adminurl As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & AdminMobileNo & "&msg=" & Text
        Dim Adminurldetail As String = sendtoadmin(adminurl)

        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result & url
        Return result
        'Dim url As String = "http://sms.itzcash.com/servlet/PushSMSServlet?mobileno=" & MobNo & "&entity=TRAVEL&message=" & m1 & "&orderid =" & orderid
        'Try
        '    request = DirectCast(WebRequest.Create(url), HttpWebRequest)
        '    request.Method = "POST"
        '    request.ContentType = "application/x-www-form-urlencoded"
        '    response = DirectCast(request.GetResponse(), HttpWebResponse)
        '    Dim stream As Stream = response.GetResponseStream()
        '    Dim ec As Encoding = System.Text.Encoding.GetEncoding("utf-8")
        '    Dim reader As StreamReader = New System.IO.StreamReader(stream, ec)
        '    result = reader.ReadToEnd()
        '    reader.Close()
        '    stream.Close()
        'Catch ex As Exception
        '    result = ex.InnerException.Message
        'End Try
        'result = result & url
        'Return result
    End Function
    Public Function SendSmsUserId(ByVal Name As String, ByVal UserId As String, ByVal Pwd As String, ByVal mobno As String, ByRef smstext As String, ByVal DtCrd As DataTable) As String
        Dim sendToPhoneNumber As String = mobno

        Dim m As String = "Dear " & Name & ",Your UserId:" & UserId & " & Password:" & Pwd & ".Thank you for registering with RWT.com,If any support to email us at support@RWT.com or call +91 79 4910 9999"
        'Dim m As String = "Dear " & Name & ",Your UserId:" & UserId & " & Password:" & Pwd & ".Thank you for registering with RWT.co,If any support to email us at info@RWT.com or call +911148444444."
        'Dim m As String = "Dear Customer,Your booking for " & sector & " is confirmed on " & Airno & "-" & FltNo & " for " & " departure date " & depdt & ". Your airline PNR No. is " & pnr & "."
        smstext = m
        Dim m1 As String = HttpUtility.UrlEncode(m)

        Dim UrlLink As String = DtCrd.Rows(0)("Url")
        Dim FeedId As String = DtCrd.Rows(0)("FeedId")
        Dim username As String = DtCrd.Rows(0)("UserId")
        Dim password As String = DtCrd.Rows(0)("Password")
        Dim ToMob As String = mobno
        Dim Text As String = m1
        Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        Dim senderid As String = DtCrd.Rows(0)("SenderId")
        'Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid
        Dim url As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & ToMob & "&msg=" & Text

        Dim AdminMobileNo As String = "9825326799"
        Dim adminurl As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & AdminMobileNo & "&msg=" & Text
        Dim Adminurldetail As String = sendtoadmin(adminurl)

        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result & url
        Return result
    End Function

    Public Function SendSmsForAnyService(ByRef mobno As String, ByRef smstext As String, ByVal dtCrd As DataTable) As String
        Dim m As String = ""
        Dim msg As String = smstext
        'Dim msg As String = "Dear " & Name & ",Your Fly Wid Us password has been reset successfully.Your User Id:" & UserId & " ,and your new password is:" & pwd & ".Thank you to connect with RWT.co,For any query please email us on info@RWT.com or call +911148444444."
        'smstext = msg

        m = smstext
        'smstext = m
        Dim m1 As String = HttpUtility.UrlEncode(m)
        Dim UrlLink As String = dtCrd.Rows(0)("Url")
        Dim FeedId As String = dtCrd.Rows(0)("FeedId")
        Dim username As String = dtCrd.Rows(0)("UserId")
        Dim password As String = dtCrd.Rows(0)("Password")
        Dim ToMob As String = mobno
        Dim Text As String = m1
        Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        Dim senderid As String = dtCrd.Rows(0)("SenderId")
        'Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid
        Dim url As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & ToMob & "&msg=" & Text

        Dim AdminMobileNo As String = "9825326799"
        Dim adminurl As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & AdminMobileNo & "&msg=" & Text
        Dim Adminurldetail As String = sendtoadmin(adminurl)

        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result & url
        Return result
    End Function


    ''new update on 17-05-18 
    Public Function sendUploadSmsCreditDebit(ByVal TrackID As String, ByVal AgecyId As String, ByVal mobno As String, ByVal CreditAmount As String, ByVal CurrentAvlBal As String, ByVal UploadType As String, ByVal depdt As String,
    ByVal pnr As String, ByRef smstext As String, ByVal DtCrd As DataTable, ByVal ActionType As String) As String
        Dim m As String = ""
        If ActionType = "DEBIT" Then


            ''   m = "Dear Customer,Your user id is  " & AgecyId & " and debited Rs. " & CreditAmount & " on " & Convert.ToString(DateAndTime.Now) & " in your a/c and current available balance  " & CurrentAvlBal & "."
            m = "Amount Debited, Txid : " & TrackID & " Amount : " & CreditAmount & ", AgentId : " & AgecyId & ", Towards : Fund Reversal"

        End If
        If ActionType = "CREDIT" Then
            '' m = "Dear Customer,Your user id is  " & AgecyId & " and credited Rs. " & CreditAmount & " on " & Convert.ToString(DateAndTime.Now) & " in your a/c and current available balance  " & CurrentAvlBal & "."
            m = "Amount Credited, Txid : " & TrackID & " , Amount : " & CreditAmount & ", AgentId : " & AgecyId & ", Towards : Fund Transfer"
        End If
        smstext = m
        Dim m1 As String = HttpUtility.UrlEncode(m)
        Dim UrlLink As String = DtCrd.Rows(0)("Url")
        Dim FeedId As String = DtCrd.Rows(0)("FeedId")
        Dim username As String = DtCrd.Rows(0)("UserId")
        Dim password As String = DtCrd.Rows(0)("Password")
        Dim ToMob As String = mobno
        Dim Text As String = m1
        Dim time As String = System.DateTime.Now.ToString("yyyyMMddhhmm")
        Dim senderid As String = DtCrd.Rows(0)("SenderId")
        'Dim url As String = "" & UrlLink & "feedid=" & FeedId & "&username=" & username & "&password =" & password & "&To=" & ToMob & "&Text =" & Text & "&time=" & time & "&senderid =" & senderid
        Dim url As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & ToMob & "&msg=" & Text

        Dim AdminMobileNo As String = "9825326799"
        Dim adminurl As String = "" & UrlLink & "mobile=" & username & "&pass=" & password & "&senderid=" & FeedId & "&to=" & AdminMobileNo & "&msg=" & Text
        Dim Adminurldetail As String = sendtoadmin(adminurl)

        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(url)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result
        Return result
    End Function

    Public Function sendtoadmin(ByVal adminurl As String)
        Try
            Dim client As New WebClient()
            Dim data As Stream = client.OpenRead(adminurl)
            Dim reader As New StreamReader(data)
            result = reader.ReadToEnd()
            data.Close()
            reader.Close()
        Catch ex As Exception
            result = ex.InnerException.Message
        End Try
        result = result & adminurl
        Return result
    End Function


End Class

