﻿using System.Linq;
using STD.Shared;
using System.Collections;
using System;
using System.Web;
using STD.BAL.TBO;
using System.Collections.Generic;
using STD.BAL;
using System.Data;
using System.Configuration;

namespace GALWS
{
    public class InsertFareRule
    {
        string Con = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString.ToString();
        string strFQCSReq = "", url = "", Userid = "", pcc = "", Pwd = "", strFQCSRes = "", strFQMDRes = "", strFQMDReq = "", StrFareRules = "", Htmllist = "", strCorporateID = "";
        private List<CredentialList> ProviderList;
       // FlightCommonBAL objFR = new FlightCommonBAL();
        STD.BAL.FltRequest flt = new STD.BAL.FltRequest();

       
        
        public FareRuleResponse GetFareRule(DataSet JsonArr, string Orderid)
        {
            int i = 0, j = 0, k = 0;
            string sno = JsonArr.Tables[0].Rows[0]["sno"].ToString();
            string Provider = JsonArr.Tables[0].Rows[0]["Provider"].ToString();
            FareRuleResponse tt = new FareRuleResponse();
            //object[] ListOW = null;
            //ListOW = (object[])ArrLst.ToArray();
            //i = ListOW.Length;
           
            string DepLocation = "", ArvlLoction = "", Sector = "";
            string baseURL = HttpContext.Current.Request.Url.Host;

            //  Dictionary<string, object> hh = new Dictionary<string, object>();
            try
            {
                DataRow[] dic = JsonArr.Tables[0].AsEnumerable().ToArray<DataRow>();
                int Rcount = dic.Length;
                if (Rcount > 0)
                {
                    if (Provider != null && Provider == "1G")
                    {
                        for (i = 0; i < Rcount; i++)
                        {
                            Dictionary<string, object> Rec = new Dictionary<string, object>();
                            foreach (DataColumn col in dic[i].Table.Columns)
                            {
                                Rec.Add(col.ColumnName, dic[i][col]);
                            }
                            //strFQCSReq = flt.FQCSReq_FareRule(Rec, Con, Convert.ToString(JsonArr.Tables[0].Rows[0]["SearchId"]));
                            strFQCSReq = flt.FQCSReq_FareRule(Rec, Con);
                            GetCredentials(Con.ToString(),Convert.ToString(Rec["SearchId"]));
                            FlightCommonBAL objFCB = new FlightCommonBAL();
                            strFQCSRes = objFCB.PostXml(url, strFQCSReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");
                            //strFQMDReq = flt.FQMD_FullRules_23(strFQCSRes, Con, Convert.ToString(JsonArr.Tables[0].Rows[0]["SearchId"]));
                            strFQMDReq = flt.FQMD_FullRules_23(strFQCSRes, Con,Convert.ToString(Rec["SearchId"]));
                            strFQMDRes = objFCB.PostXml(url, strFQMDReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");

                            DepLocation = Rec["DepartureLocation"].ToString();
                            ArvlLoction = Rec["ArrivalLocation"].ToString();
                            Sector = (DepLocation + "-" + ArvlLoction).Trim();

                            StrFareRules += flt.FQMD_ResponseResult_23(strFQMDRes, Con, Sector);
                        }
                        for (k = 0; k < Rcount; k++)
                        {
                            //Rec = (Dictionary<string, object>)((object[])(ListOW))[k];
                            Dictionary<string, object> Rec = new Dictionary<string, object>();
                            foreach (DataColumn col in dic[k].Table.Columns)
                            {
                                Rec.Add(col.ColumnName, dic[k][col]);
                            }
                            DepLocation = Rec["DepartureLocation"].ToString();
                            ArvlLoction = Rec["ArrivalLocation"].ToString();
                            Sector = (DepLocation + "<img src='/Images/air.png' />" + ArvlLoction).Trim();
                            Htmllist += "<li><a class='tablinks' title='Click for fare rules' onclick=\"SelectedSector(event,'" + DepLocation + "-" + ArvlLoction + "')\">" + Sector + " </a></li>";
                        }
                        Dictionary<string, object> Rec1 = new Dictionary<string, object>();
                        foreach (DataColumn col in dic[0].Table.Columns)
                        {
                            Rec1.Add(col.ColumnName, dic[0][col]);
                        }
                        DepLocation = Rec1["DepartureLocation"].ToString();
                        ArvlLoction = Rec1["ArrivalLocation"].ToString();
                        Sector = DepLocation + "-" + ArvlLoction;
                        StrFareRules = "<div id='Finalfarerule'><ul class=tab>" + Htmllist + "</ul>" + StrFareRules.Trim() + "<div/>";

                        FareRule1 obj = new FareRule1();
                        tt.Response = new ResponseFareRule();
                        tt.Response.FareRules = new List<FareRule1>();
                        tt.Response.FareRules.Add(obj);
                        tt.Response.FareRules[0].FareRuleDetail = StrFareRules.ToString();

                    }
                    else if (Provider == "AK")
                    {
                        GALWS.AirAsia.AirAsiaFareRules objrules = new GALWS.AirAsia.AirAsiaFareRules();
                        for (k = 0; k < i; k++)
                        {
                            Dictionary<string, object> Rec = new Dictionary<string, object>();
                            foreach (DataColumn col in dic[i].Table.Columns)
                            {
                                Rec.Add(col.ColumnName, dic[i][col]);
                            }
                            DepLocation = Rec["DepartureLocation"].ToString();
                            ArvlLoction = Rec["ArrivalLocation"].ToString();
                            Sector = (DepLocation + "<img src='/Images/air.png' />" + ArvlLoction).Trim();
                            Htmllist += "<li><a class='tablinks' title='Click for fare rules' onclick=\"SelectedSector(event,'" + DepLocation + "-" + ArvlLoction + "')\">" + Sector + " </a></li>";

                            StrFareRules += objrules.GetFareRule(Con, sno, Rec["fareBasis"].ToString(), Rec["AdtRbd"].ToString(), Rec["Searchvalue"].ToString());
                        }

                        StrFareRules = "<div id='Finalfarerule'><ul class=tab>" + Htmllist + "</ul>" + StrFareRules.Trim() + "</div>";

                        FareRule1 obj = new FareRule1();
                        tt.Response = new ResponseFareRule();
                        tt.Response.FareRules = new List<FareRule1>();
                        tt.Response.FareRules.Add(obj);
                        tt.Response.FareRules[0].FareRuleDetail = StrFareRules;
                    }
                    else
                    {
                        STD.BAL.TBO.TBOFareRule obj = new TBOFareRule();
                        tt = obj.GetFareRule("", sno);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            //int i = 0, j = 0, k = 0;
            //ArrayList ArrLst = new ArrayList();
            //if (JsonArr.Tables[0].Rows.Count > 0)
            //{
            //    //for (int Z = 0; Z < JsonArr.Tables[0].Rows.Count; Z++)
            //    //{
            //    foreach (DataRow dtrow in JsonArr.Tables[0].Rows)
            //    {
            //        foreach (var item in dtrow.ItemArray)
            //        {
            //            ArrLst.Add(item);
            //        }
            //        // ArrLst.Add(dtrow);
            //    }
            //    // }

            //}



            ////try
            ////{
            ////    if (ListOW.Length > 0)
            ////    {
            ////        if (Provider != null && Provider == "1G")
            ////        {
            ////            STD.BAL.FltRequest flt = new STD.BAL.FltRequest();

            ////            if (i > 0)
            ////            {
            ////                for (j = 0; j < i; j++)
            ////                {
            ////                    Rec = (Dictionary<string, object>)((object[])(ListOW))[j];
            ////                    strFQCSReq = flt.FQCSReq_FareRule(Rec, Con);
            ////                    GetCredentials(Con.ToString());
            ////                    FlightCommonBAL objFCB = new FlightCommonBAL();
            ////                    strFQCSRes = objFCB.PostXml(url, strFQCSReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");
            ////                    strFQMDReq = flt.FQMD_FullRules_23(strFQCSRes, Con);
            ////                    strFQMDRes = objFCB.PostXml(url, strFQMDReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");

            ////                    DepLocation = Rec["DepartureLocation"].ToString();
            ////                    ArvlLoction = Rec["ArrivalLocation"].ToString();
            ////                    Sector = (DepLocation + "-" + ArvlLoction).Trim();

            ////                    StrFareRules += flt.FQMD_ResponseResult_23(strFQMDRes, Con, Sector);
            ////                }
            ////                for (k = 0; k < i; k++)
            ////                {
            ////                    Rec = (Dictionary<string, object>)((object[])(ListOW))[k];
            ////                    DepLocation = Rec["DepartureLocation"].ToString();
            ////                    ArvlLoction = Rec["ArrivalLocation"].ToString();
            ////                    Sector = (DepLocation + "<img src='/Images/air.png' />" + ArvlLoction).Trim();
            ////                    Htmllist += "<li><a class='tablinks' title='Click for fare rules' onclick=\"SelectedSector(event,'" + DepLocation + "-" + ArvlLoction + "')\">" + Sector + " </a></li>";
            ////                }
            ////                Rec = (Dictionary<string, object>)((object[])(ListOW))[0];
            ////                DepLocation = Rec["DepartureLocation"].ToString();
            ////                ArvlLoction = Rec["ArrivalLocation"].ToString();
            ////                Sector = DepLocation + "-" + ArvlLoction;
            ////                StrFareRules = "<div id='Finalfarerule'><ul class=tab>" + Htmllist + "</ul>" + StrFareRules.Trim() + "<div/>";

            ////                FareRule1 obj = new FareRule1();
            ////                tt.Response = new ResponseFareRule();
            ////                tt.Response.FareRules = new List<FareRule1>();
            ////                tt.Response.FareRules.Add(obj);
            ////                tt.Response.FareRules[0].FareRuleDetail = StrFareRules.ToString();

            ////            }
            ////            //else if (i == 1)
            ////            //{

            ////            //    Rec = (Dictionary<string, object>)((object[])(ListOW))[0];
            ////            //    strFQCSReq = flt.FQCSReq_FareRule(Rec, flt.connectionString);
            ////            //    GetCredentials(flt.connectionString);
            ////            //    FlightCommonBAL objFCB = new FlightCommonBAL();
            ////            //    strFQCSRes = objFCB.PostXml(url, strFQCSReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");
            ////            //    strFQMDReq = flt.FQMD_FullRules_23(strFQCSRes, flt.connectionString);
            ////            //    strFQMDRes = objFCB.PostXml(url, strFQMDReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");
            ////            //    //DepLocation=Rec[""];
            ////            //    //ArvlLoction=Rec[""];
            ////            //    Sector = (DepLocation + "-->>" + ArvlLoction).Trim();
            ////            //    StrFareRules = flt.FQMD_ResponseResult_23(strFQMDRes, flt.connectionString, Sector);
            ////            //    FareRule1 obj = new FareRule1();
            ////            //    tt.Response = new ResponseFareRule();
            ////            //    tt.Response.FareRules = new List<FareRule1>();
            ////            //    tt.Response.FareRules.Add(obj);
            ////            //    tt.Response.FareRules[0].FareRuleDetail = StrFareRules.ToString();
            ////            //}

            ////        }
            ////        else
            ////        {
            ////            STD.BAL.TBO.TBOFareRule obj = new TBOFareRule();
            ////            tt = obj.GetFareRule("", sno);

            ////        }
            ////    }
            ////}
            ////catch (Exception ex)
            ////{

            ////    throw ex;
            ////}

            FlightCommonBAL objbal = new FlightCommonBAL(Con);
            objbal.AddFareRule(Orderid, tt.Response.FareRules[0].FareRuleDetail);
            return tt;
        }
        private void GetCredentials(string conn,string ProviderUserId)
        {
            ProviderList = Data.GetProviderCrd(conn);
            if (!string.IsNullOrEmpty(ProviderUserId))
            {
                url = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].AvailabilityURL;
                Userid = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].UserID;
                Pwd = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].Password;
                pcc = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].CarrierAcc;
                strCorporateID = ((from crd in ProviderList where crd.Provider == "1G" && crd.UserID == ProviderUserId select crd).ToList())[0].CorporateID;
            }
            else
            {
                url = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                Userid = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                Pwd = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].Password;
                pcc = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc;
                strCorporateID = ((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].CorporateID;
            }
            
            
        }
    }
}
