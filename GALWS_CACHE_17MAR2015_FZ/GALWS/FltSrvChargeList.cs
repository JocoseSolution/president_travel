﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace STD.Shared
{
    /// <summary>
    /// Summary description for FltSrvChargeList
    /// </summary>
    public class FltSrvChargeList
    {
        public string AirlineCode { get; set; }
        public decimal SrviceTax { get; set; }
        public decimal TransactionFee { get; set; }
        //public int IATACommissiom { get; set; }
        public decimal IATACommissiom { get; set; }
    }
}