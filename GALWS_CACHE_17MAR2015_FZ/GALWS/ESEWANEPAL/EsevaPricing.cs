﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.ESEWANEPAL
{
    public class EsevaPricing
    {
        string TokenId = string.Empty;
        string AuthUrl = string.Empty;
        public EsevaPricing(string TokenIds,string AuthUrls)
        {
            TokenId = TokenIds;
            AuthUrl = AuthUrls;

        }
        public string Getreservation(string searchId, string flightId, int source , ref Dictionary<string, string> Log, ref string exep)
        {
            string strJsonResponse = string.Empty;

            try
            {

                string strJsonData = ESEWANEPAL.EsevaRequest.GetEsevaReservationReq(searchId, flightId, source);
                if (Log.ContainsKey("RepriceReq") == true) { Log["RepriceReq"] = Log["RepriceReq"] + strJsonData; } else { Log.Add("RepriceReq", strJsonData); }              
                EsevaUtility.SaveFile(strJsonData, "GetreservationReq_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));
                strJsonResponse = EsevaUtility.GetResponse(AuthUrl+ "reservation", strJsonData, TokenId);             
                if (Log.ContainsKey("RepriceResp") == true) { Log["RepriceResp"] = Log["RepriceResp"] + strJsonResponse; } else { Log.Add("RepriceResp", strJsonResponse); }
                EsevaUtility.SaveFile(strJsonResponse, "GetreservationRes_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));
                
            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
            }

            return strJsonResponse;


        }
        public string GetRe_reservation(string searchId, string flightId, int source, ref Dictionary<string, string> Log, ref string exep)
        {
            string strJsonResponse = string.Empty;

            try
            {

                string strJsonData = ESEWANEPAL.EsevaRequest.GetEsevaReservationReq(searchId, flightId, source);
                if (Log.ContainsKey("RepriceReq") == true) { Log["RepriceReq"] = Log["RepriceReq"] + strJsonData; } else { Log.Add("RepriceReq", strJsonData); }
                EsevaUtility.SaveFile(strJsonData, "GetreservationReq_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));
                strJsonResponse = EsevaUtility.GetResponse(AuthUrl + "reservation", strJsonData, TokenId);
                if (Log.ContainsKey("RepriceResp") == true) { Log["RepriceResp"] = Log["RepriceResp"] + strJsonResponse; } else { Log.Add("RepriceResp", strJsonResponse); }
                EsevaUtility.SaveFile(strJsonResponse, "GetreservationRes_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));

            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
            }

            return strJsonResponse;


        }
    }
}
