﻿using STD.BAL;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.ESEWANEPAL
{
    public class EsevaSearch
    {
        string TokenId = string.Empty;
        string UserId = string.Empty;
        string Password = string.Empty;
        string TokenType = string.Empty;
        string AuthUrl = string.Empty;
        string ConStr = string.Empty;
        string AuthEmail = string.Empty;

        public EsevaSearch(string Authurl, string Authemail, string password)
        {
            AuthUrl = Authurl;
            AuthEmail = Authemail;
            Password = password;
        }

        public List<FlightSearchResults> GetEsevaAvailibility(FlightSearch objFlt, bool isSplFare, string fareType, bool isLCC, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, float srvCharge, List<AirlineList> Airlist, List<FlightCityList> CityList)
        {


            FlightCommonBAL objFltComm = new FlightCommonBAL(constr);
            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            try
            {
                string strTokenID = GALWS.ESEWANEPAL.EsevaUtility.GetEsevaToken(AuthUrl + "get-token", AuthEmail, Password, out TokenType); //"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC91YXQtYWRtaW4uZXNld2F0cmF2ZWxzLmNvbVwvYXBpXC9kb21lc3RpY1wvdjFcL2dldC10b2tlbiIsImlhdCI6MTYwMTkxOTY0OSwiZXhwIjoxNjA1MDczMjQ5LCJuYmYiOjE2MDE5MTk2NDksImp0aSI6Im56MG91UnFZY0x3SU1aVW4iLCJzdWIiOjYsInBydiI6IjQyZWRlMzMzNGEwNGNkNjVjYjJiZTNmNWFkZmViMmMxZWRkMzA3NzQifQ.4ClyW79wsTIv7hvGwB9tdQmVYHNkSTKDYPGZsIkLmeg";//
                if (!string.IsNullOrEmpty(strTokenID))
                {
                    #region 1
                    string strJsonData = GALWS.ESEWANEPAL.EsevaRequest.GetSearchReq(objFlt, fareType, isSplFare);
                    EsevaUtility.SaveFile(strJsonData, "SearchReq_" + objFlt.HidTxtAirLine);
                    SaveResponse.SAVElOGFILE(strJsonData, "REQ", "txt", "", objFlt.HidTxtAirLine, fareType);
                    string strJsonResponse = "";
                    STD.BAL.TBO.FlightResult items = new STD.BAL.TBO.FlightResult();
                    try
                    {
                        int reStrToken = 0;
                    reTokenStart:
                        strJsonResponse = GALWS.ESEWANEPAL.EsevaUtility.GetResponse(AuthUrl + "flight-search", strJsonData, strTokenID);

                        if ((strJsonResponse.Contains("Unauthorized") == true || strJsonResponse.Contains("Error") == true) && (reStrToken == 0 || reStrToken == 1))
                        {
                            strTokenID = GALWS.ESEWANEPAL.EsevaUtility.GetEsevaToken(AuthUrl + "get-token", AuthEmail, Password, out TokenType);
                            reStrToken++;
                            goto reTokenStart;
                        }


                    }
                    catch (Exception ex1)
                    {

                        strJsonResponse = strJsonResponse + "error: " + ex1.Message + "stack:" + ex1.StackTrace.ToString();// +"" + Convert.ToString(ex1.InnerException.Message);
                    }

                    EsevaUtility.SaveFile(strJsonResponse, "SearchRes_" + objFlt.HidTxtAirLine);
                    SaveResponse.SAVElOGFILE(strJsonResponse, "REQ", "txt", "", objFlt.HidTxtAirLine, fareType);

                    List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
                    List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();


                    if (!string.IsNullOrEmpty(strJsonResponse))
                    {
                        Newtonsoft.Json.Linq.JObject array = Newtonsoft.Json.Linq.JObject.Parse(strJsonResponse);

                        if (Convert.ToString(array["success"]) == "True")
                        {
                            if (Convert.ToString(array["data"]) != "" && Convert.ToString(array["data"]) != "null")
                            {

                                string type = array["data"]["flights"].Type.ToString();
                                if (type == "Array")
                                {
                                    Newtonsoft.Json.Linq.JArray outboundsArr = (Newtonsoft.Json.Linq.JArray)array["data"]["flights"];
                                    int flight = 1;
                                    int lineNum = 1;
                                    int leg = 1;
                                    if (fareType == "INB")
                                    {
                                        flight = 1;
                                    }
                                    else
                                    {
                                        flight = 2;
                                    }

                                    foreach (var seg in outboundsArr)
                                    {
                                        FlightSearchResults fsr = new FlightSearchResults();
                                        fsr.Leg = leg;
                                        fsr.LineNumber = lineNum;
                                        fsr.Flight = flight.ToString();
                                        fsr.Stops = 0 + "-Stop";

                                        fsr.Adult = objFlt.Adult;
                                        fsr.Child = objFlt.Child;
                                        fsr.Infant = objFlt.Infant;


                                        fsr.depdatelcc = Convert.ToString(seg["departDateTime"]);
                                        fsr.arrdatelcc = Convert.ToString(seg["arrivalDateTime"]);
                                        fsr.Departure_Date = Convert.ToDateTime(Convert.ToString(seg["departDateTime"])).ToString("dd MMM"); // legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                                        fsr.Arrival_Date = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"])).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                                        fsr.DepartureDate = Convert.ToDateTime(Convert.ToString(seg["departDateTime"]).Trim()).ToString("ddMMyy");
                                        fsr.ArrivalDate = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"]).Trim()).ToString("ddMMyy");

                                        string flightNo = System.Text.RegularExpressions.Regex.Replace(Convert.ToString(seg["flightNo"]), "[^0-9]+", string.Empty);
                                        // getAirlineName(objFS.ValiDatingCarrier, AirLineList);
                                        //getCityName(objFS.DepartureLocation, CityList);
                                        fsr.FlightIdentification = flightNo;
                                        fsr.AirLineName = getNPAirlineName(Convert.ToString(seg["validatingCarrier"]));// getAirlineName(Convert.ToString(seg["validatingCarrier"]), Airlist);

                                        fsr.ValiDatingCarrier = Convert.ToString(seg["validatingCarrier"]);
                                        fsr.OperatingCarrier = Convert.ToString(seg["validatingCarrier"]);
                                        fsr.MarketingCarrier = Convert.ToString(seg["validatingCarrier"]);//seg.Airline.OperatingCarrier.Trim();
                                        fsr.fareBasis = Convert.ToString(seg["bookingClassCode"]);
                                        if (fareType == "INB")
                                        {
                                            fsr.DepartureLocation = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.DepartureCityName = getCityName(objFlt.HidTxtDepCity.Split(',')[0], CityList);
                                            fsr.DepAirportCode = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.ArrivalLocation = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.ArrivalCityName = getCityName(objFlt.HidTxtArrCity.Split(',')[0], CityList);
                                            fsr.ArrAirportCode = objFlt.HidTxtArrCity.Split(',')[0];                                           
                                        }
                                        else
                                        {
                                            fsr.DepartureLocation = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.DepartureCityName = getCityName(objFlt.HidTxtArrCity.Split(',')[0], CityList);
                                            fsr.DepAirportCode = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.ArrivalLocation = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.ArrivalCityName = getCityName(objFlt.HidTxtDepCity.Split(',')[0], CityList);
                                            fsr.ArrAirportCode = objFlt.HidTxtDepCity.Split(',')[0];
                                        }
                                        fsr.Trip = objFlt.Trip.ToString();
                                        fsr.DepartureTime = Convert.ToDateTime(Convert.ToString(seg["departDateTime"])).ToString("HH:mm");
                                        fsr.DepartureAirportName = Convert.ToString(seg["departure"]);
                                        fsr.DepartureTerminal = "1";
                                       

                                      
                                        fsr.ArrivalTime = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"])).ToString("HH:mm");
                                        fsr.ArrivalAirportName = Convert.ToString(seg["arrival"]);
                                        fsr.ArrivalTerminal = "1";
                                       
                                       

                                        DateTime startTime = Convert.ToDateTime(Convert.ToString(seg["departDateTime"]));
                                        DateTime endTime = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"]));

                                        TimeSpan span = endTime.Subtract(startTime);
                                        int difmin = 0;
                                        difmin = Convert.ToInt16(span.TotalMinutes);
                                        fsr.TotDur = GetTimeInHrsAndMin(difmin);
                                        fsr.TripCnt = fsr.TotDur;
                                        #region Fare
                                        float ChargeBU = 0;
                                        DataTable CommDt = new DataTable();
                                        Hashtable STTFTDS = new Hashtable();
                                        if (seg["fare"]["adtFare"] != null)
                                        {

                                            fsr.AdtBfare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["adtFare"]["baseFare"])));
                                            fsr.AdtCabin = "Y";
                                            fsr.AdtFSur = 0;
                                            fsr.AdtTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["adtFare"]["taxes"])) + srvCharge);
                                            fsr.AdtOT = (float)Math.Ceiling(fsr.AdtTax - fsr.AdtFSur);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                            fsr.AdtRbd = Convert.ToString(seg["bookingClassCode"]);
                                            fsr.AdtFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                            fsr.AdtFareType = Convert.ToString(seg["fare"]["refundable"]) == "True" ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                                                                                                                      // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                            fsr.AdtFare = (float)Math.Ceiling(float.Parse((fsr.AdtTax + fsr.AdtBfare).ToString())); //  + srvCharge
                                            fsr.sno = Convert.ToString(array["data"]["search"]["searchId"]) + ":" + Convert.ToString(seg["flightId"]) + ":" + Convert.ToString(seg["source"]) + ":" + Convert.ToString(seg["flightClassCode"]) + ":" + strTokenID + ":" + Convert.ToString(seg["carrierLogo"]).Replace(":", "##");//fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                            fsr.fareBasis = fsr.AdtFarebasis;
                                            fsr.FBPaxType = "ADT";
                                            fsr.AdtFar = "NRM";
                                            fsr.BagInfo = Convert.ToString(seg["freeBaggage"]);
                                            fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());
                                            fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());

                                            CommDt.Clear();
                                            CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                            fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                            fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                            STTFTDS.Clear();
                                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, objFlt.TDS);
                                            fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                            fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                            fsr.AdtEduCess = 0;
                                            fsr.AdtHighEduCess = 0;
                                            fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                            fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());






                                            if (objFlt.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                                    fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }


                                        }

                                        if (seg["fare"]["chdFare"] != null)
                                        {
                                            fsr.ChdBFare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["chdFare"]["baseFare"])));
                                            fsr.ChdCabin = "Y";
                                            fsr.ChdFSur = 0;
                                            fsr.ChdTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["chdFare"]["taxes"])) + srvCharge);
                                            fsr.ChdOT = (float)Math.Ceiling(fsr.ChdTax - fsr.ChdFSur);// + paxFare.AdditionalTxnFeePub + srvCharge);
                                            fsr.ChdRbd = Convert.ToString(seg["bookingClassCode"]);
                                            fsr.ChdFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                            fsr.ChdFare = (float)Math.Ceiling(float.Parse((fsr.ChdTax + fsr.ChdBFare).ToString())); //  + srvCharge

                                            fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                            fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, objFlt.Trip.ToString());
                                            CommDt.Clear();
                                            CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.ChdFarebasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                            fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                            fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                            STTFTDS.Clear();
                                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, objFlt.TDS);
                                            fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                            fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                            fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                            fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                            fsr.ChdEduCess = 0;
                                            fsr.ChdHighEduCess = 0;

                                            if (objFlt.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.ChdFare.ToString()));
                                                    fsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    fsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }

                                        }

                                        if (seg["fare"]["infFare"] != null)
                                        {
                                            fsr.InfBfare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["infFare"]["baseFare"])));
                                            fsr.InfCabin = "";
                                            fsr.InfFSur = 0;
                                            fsr.InfTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["infFare"]["taxes"])));
                                            fsr.InfOT = (float)Math.Ceiling(fsr.InfTax - fsr.InfFSur);//+ paxFare.AdditionalTxnFeePub);
                                            fsr.InfRbd = Convert.ToString(seg["bookingClassCode"]);
                                            fsr.InfFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                            fsr.InfFare = (float)Math.Ceiling(float.Parse((fsr.InfTax + fsr.InfBfare).ToString()));

                                            if (objFlt.IsCorp == true)
                                            {
                                                try
                                                {
                                                    DataTable MGDT = new DataTable();
                                                    MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.InfBfare.ToString()), decimal.Parse(fsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.InfFare.ToString()));
                                                    fsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                    fsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                }
                                                catch { }
                                            }

                                        }

                                        fsr.Searchvalue = Convert.ToString(seg) + "##" + strTokenID;
                                        fsr.OriginalTF = float.Parse(Convert.ToString(seg["fare"]["totalFare"]));
                                        fsr.OriginalTT = srvCharge;
                                        fsr.Provider = "NT";

                                        fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;


                                        fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                                        fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(objlist.InfTF * objlist.Infant);
                                        fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                                        fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                                        fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +objFS.InfTds;
                                        fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + (fsr.InfMgtFee * fsr.Infant);



                                        fsr.AvailableSeats = "20";
                                        fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                                        fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                                        fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                                        fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                                        fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                        fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                                        fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                        fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));






                                        if (objFlt.RTF == true || objFlt.GDSRTF == true || isSplFare == true)
                                        {
                                            fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                        }
                                        else
                                        {
                                            if (fareType == "INB")
                                            {
                                                fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                            }
                                            else
                                            {
                                                fsr.Sector = objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                            }

                                        }


                                        #endregion
                                        if (flight == 1)
                                        {
                                            if (fareType == "INB")
                                            {
                                                fsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                                fsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                            }
                                            else
                                            {
                                                fsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                                fsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                            }
                                            fsr.TripType = TripType.O.ToString();
                                        }
                                        else if (flight == 2)
                                        {
                                            fsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.TripType = TripType.R.ToString();
                                        }

                                        fsrList.Add(fsr);
                                        //if (flight == 1 || objFlt.Trip == Trip.I)
                                        //{

                                        //    fsrList.Add(fsr);
                                        //}
                                        //else if (flight == 2 && objFlt.Trip == Trip.D)
                                        //{

                                        //    RfsrList.Add(fsr);
                                        //}

                                        lineNum++;
                                    }
                                    FinalList = fsrList;
                                    //if (isSplFare && objFlt.Trip == Trip.D)
                                    //{

                                    //    FinalList = RoundTripFare(fsrList, RfsrList, srvCharge);
                                    //}
                                    //else
                                    //{
                                    //    FinalList = fsrList;
                                    //}

                                }
                                else
                                {
                                    string type2 = array["data"]["flights"]["outbounds"].Type.ToString();
                                    string type3 = array["data"]["flights"]["inbounds"].Type.ToString();
                                    if (type2 == "Array")
                                    {
                                        Newtonsoft.Json.Linq.JArray outboundsArr = (Newtonsoft.Json.Linq.JArray)array["data"]["flights"]["outbounds"];
                                        int flight = 1;
                                        int lineNum = 1;
                                        int leg = 1;
                                        foreach (var seg in outboundsArr)
                                        {
                                            FlightSearchResults fsr = new FlightSearchResults();
                                            fsr.Leg = leg;
                                            fsr.LineNumber = lineNum;
                                            fsr.Flight = flight.ToString();
                                            fsr.Stops = 0 + "-Stop";

                                            fsr.Adult = objFlt.Adult;
                                            fsr.Child = objFlt.Child;
                                            fsr.Infant = objFlt.Infant;


                                            fsr.depdatelcc = Convert.ToString(seg["departDateTime"]);
                                            fsr.arrdatelcc = Convert.ToString(seg["arrivalDateTime"]);
                                            fsr.Departure_Date = Convert.ToDateTime(Convert.ToString(seg["departDateTime"])).ToString("dd MMM"); // legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                                            fsr.Arrival_Date = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"])).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                                            fsr.DepartureDate = Convert.ToDateTime(Convert.ToString(seg["departDateTime"]).Trim()).ToString("ddMMyy");
                                            fsr.ArrivalDate = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"]).Trim()).ToString("ddMMyy");

                                            string flightNo = System.Text.RegularExpressions.Regex.Replace(Convert.ToString(seg["flightNo"]), "[^0-9]+", string.Empty);
                                            // getAirlineName(objFS.ValiDatingCarrier, AirLineList);
                                            //getCityName(objFS.DepartureLocation, CityList);
                                            fsr.FlightIdentification = flightNo;
                                            fsr.AirLineName = getNPAirlineName(Convert.ToString(seg["validatingCarrier"]));// getAirlineName(Convert.ToString(seg["validatingCarrier"]), Airlist);

                                            fsr.ValiDatingCarrier = Convert.ToString(seg["validatingCarrier"]);
                                            fsr.OperatingCarrier = Convert.ToString(seg["validatingCarrier"]);
                                            fsr.MarketingCarrier = Convert.ToString(seg["validatingCarrier"]);//seg.Airline.OperatingCarrier.Trim();
                                            fsr.fareBasis = Convert.ToString(seg["bookingClassCode"]);
                                            fsr.DepartureLocation = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.DepartureCityName = getCityName(objFlt.HidTxtDepCity.Split(',')[0], CityList);
                                            fsr.DepartureTime = Convert.ToDateTime(Convert.ToString(seg["departDateTime"])).ToString("HH:mm");
                                            fsr.DepartureAirportName = Convert.ToString(seg["departure"]);
                                            fsr.DepartureTerminal = "1";
                                            fsr.DepAirportCode = objFlt.HidTxtDepCity.Split(',')[0];

                                            fsr.ArrivalLocation = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.ArrivalCityName = getCityName(objFlt.HidTxtArrCity.Split(',')[0], CityList);
                                            fsr.ArrivalTime = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"])).ToString("HH:mm");
                                            fsr.ArrivalAirportName = Convert.ToString(seg["arrival"]);
                                            fsr.ArrivalTerminal = "1";
                                            fsr.ArrAirportCode = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.Trip = objFlt.Trip.ToString();

                                            DateTime startTime = Convert.ToDateTime(Convert.ToString(seg["departDateTime"]));
                                            DateTime endTime = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"]));

                                            TimeSpan span = endTime.Subtract(startTime);
                                            int difmin = 0;
                                            difmin = Convert.ToInt16(span.TotalMinutes);
                                            fsr.TotDur = GetTimeInHrsAndMin(difmin);
                                            fsr.TripCnt = fsr.TotDur;
                                            #region Fare
                                            float ChargeBU = 0;
                                            DataTable CommDt = new DataTable();
                                            Hashtable STTFTDS = new Hashtable();
                                            if (seg["fare"]["adtFare"] != null)
                                            {

                                                fsr.AdtBfare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["adtFare"]["baseFare"])));
                                                fsr.AdtCabin = "Y";
                                                fsr.AdtFSur = 0;
                                                fsr.AdtTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["adtFare"]["taxes"])) + srvCharge);
                                                fsr.AdtOT = (float)Math.Ceiling(fsr.AdtTax - fsr.AdtFSur);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                fsr.AdtRbd = Convert.ToString(seg["bookingClassCode"]);
                                                fsr.AdtFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                                fsr.AdtFareType = Convert.ToString(seg["fare"]["refundable"]) == "True" ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                                                                                                                          // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                                fsr.AdtFare = (float)Math.Ceiling(float.Parse((fsr.AdtTax + fsr.AdtBfare).ToString())); //  + srvCharge
                                                fsr.sno = Convert.ToString(array["data"]["search"]["searchId"]) + ":" + Convert.ToString(seg["flightId"]) + ":" + Convert.ToString(seg["source"]) + ":" + Convert.ToString(seg["flightClassCode"]) + ":" + strTokenID + ":" + Convert.ToString(seg["carrierLogo"]).Replace(":", "##");//fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                                fsr.fareBasis = fsr.AdtFarebasis;
                                                fsr.FBPaxType = "ADT";
                                                fsr.AdtFar = "NRM";
                                                fsr.BagInfo = Convert.ToString(seg["freeBaggage"]);
                                                fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());
                                                fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());

                                                CommDt.Clear();
                                                CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                                fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS.Clear();
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, objFlt.TDS);
                                                fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                                fsr.AdtEduCess = 0;
                                                fsr.AdtHighEduCess = 0;
                                                fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());

                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                                        fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                            }

                                            if (seg["fare"]["chdFare"] != null)
                                            {
                                                fsr.ChdBFare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["chdFare"]["baseFare"])));
                                                fsr.ChdCabin = "Y";
                                                fsr.ChdFSur = 0;
                                                fsr.ChdTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["chdFare"]["taxes"])) + srvCharge);
                                                fsr.ChdOT = (float)Math.Ceiling(fsr.ChdTax - fsr.ChdFSur);// + paxFare.AdditionalTxnFeePub + srvCharge);
                                                fsr.ChdRbd = Convert.ToString(seg["bookingClassCode"]);
                                                fsr.ChdFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                                fsr.ChdFare = (float)Math.Ceiling(float.Parse((fsr.ChdTax + fsr.ChdBFare).ToString())); //  + srvCharge

                                                fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                                fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, objFlt.Trip.ToString());
                                                CommDt.Clear();
                                                CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.ChdFarebasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                                fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS.Clear();
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, objFlt.TDS);
                                                fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                                fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                fsr.ChdEduCess = 0;
                                                fsr.ChdHighEduCess = 0;

                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.ChdFare.ToString()));
                                                        fsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        fsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                            }

                                            if (seg["fare"]["infFare"] != null)
                                            {
                                                fsr.InfBfare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["infFare"]["baseFare"])));
                                                fsr.InfCabin = "";
                                                fsr.InfFSur = 0;
                                                fsr.InfTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["infFare"]["taxes"])));
                                                fsr.InfOT = (float)Math.Ceiling(fsr.InfTax - fsr.InfFSur);//+ paxFare.AdditionalTxnFeePub);
                                                fsr.InfRbd = Convert.ToString(seg["bookingClassCode"]);
                                                fsr.InfFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                                fsr.InfFare = (float)Math.Ceiling(float.Parse((fsr.InfTax + fsr.InfBfare).ToString()));

                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.InfBfare.ToString()), decimal.Parse(fsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.InfFare.ToString()));
                                                        fsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        fsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                            }

                                            fsr.Searchvalue = Convert.ToString(seg) + "##" + strTokenID;
                                            fsr.OriginalTF = float.Parse(Convert.ToString(seg["fare"]["totalFare"]));
                                            fsr.OriginalTT = srvCharge;
                                            fsr.Provider = "NT";

                                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;


                                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(objlist.InfTF * objlist.Infant);
                                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +objFS.InfTds;
                                            fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + (fsr.InfMgtFee * fsr.Infant);



                                            fsr.AvailableSeats = "20";
                                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                                            fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));

                                            if (objFlt.RTF == true || objFlt.GDSRTF == true || isSplFare == true)
                                            {
                                                fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                            }
                                            else
                                            {
                                                if (fareType == "INB")
                                                {
                                                    fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                                }
                                                else
                                                {
                                                    fsr.Sector = objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                }

                                            }


                                            #endregion

                                           
                                            
                                                fsr.OrgDestFrom = objFlt.HidTxtDepCity.Split(',')[0];
                                                fsr.OrgDestTo = objFlt.HidTxtArrCity.Split(',')[0];
                                            
                                           
                                            fsr.TripType = TripType.O.ToString();
                                            fsrList.Add(fsr);
                                            lineNum++;
                                        }
                                    }
                                    if (type3 == "Array")
                                    {
                                        Newtonsoft.Json.Linq.JArray outboundsArr = (Newtonsoft.Json.Linq.JArray)array["data"]["flights"]["inbounds"];
                                        int flight = 2;
                                        int lineNum = 1;
                                        int leg = 1;
                                        foreach (var seg in outboundsArr)
                                        {
                                            FlightSearchResults fsr = new FlightSearchResults();
                                            fsr.Leg = leg;
                                            fsr.LineNumber = lineNum;
                                            fsr.Flight = flight.ToString();
                                            fsr.Stops = 0 + "-Stop";

                                            fsr.Adult = objFlt.Adult;
                                            fsr.Child = objFlt.Child;
                                            fsr.Infant = objFlt.Infant;


                                            fsr.depdatelcc = Convert.ToString(seg["departDateTime"]);
                                            fsr.arrdatelcc = Convert.ToString(seg["arrivalDateTime"]);
                                            fsr.Departure_Date = Convert.ToDateTime(Convert.ToString(seg["departDateTime"])).ToString("dd MMM"); // legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                                            fsr.Arrival_Date = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"])).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                                            fsr.DepartureDate = Convert.ToDateTime(Convert.ToString(seg["departDateTime"]).Trim()).ToString("ddMMyy");
                                            fsr.ArrivalDate = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"]).Trim()).ToString("ddMMyy");

                                            string flightNo = System.Text.RegularExpressions.Regex.Replace(Convert.ToString(seg["flightNo"]), "[^0-9]+", string.Empty);
                                            // getAirlineName(objFS.ValiDatingCarrier, AirLineList);
                                            //getCityName(objFS.DepartureLocation, CityList);
                                            fsr.FlightIdentification = flightNo;
                                            fsr.AirLineName = getNPAirlineName(Convert.ToString(seg["validatingCarrier"]));// getAirlineName(Convert.ToString(seg["validatingCarrier"]), Airlist);

                                            fsr.ValiDatingCarrier = Convert.ToString(seg["validatingCarrier"]);
                                            fsr.OperatingCarrier = Convert.ToString(seg["validatingCarrier"]);
                                            fsr.MarketingCarrier = Convert.ToString(seg["validatingCarrier"]);//seg.Airline.OperatingCarrier.Trim();
                                            fsr.fareBasis = Convert.ToString(seg["bookingClassCode"]);
                                            fsr.DepartureLocation = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.DepartureCityName = getCityName(objFlt.HidTxtArrCity.Split(',')[0], CityList);
                                            fsr.DepartureTime = Convert.ToDateTime(Convert.ToString(seg["departDateTime"])).ToString("HH:mm");
                                            fsr.DepartureAirportName = Convert.ToString(seg["departure"]);
                                            fsr.DepartureTerminal = "1";
                                            fsr.DepAirportCode = objFlt.HidTxtArrCity.Split(',')[0];

                                            fsr.ArrivalLocation = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.ArrivalCityName = getCityName(objFlt.HidTxtDepCity.Split(',')[0], CityList);
                                            fsr.ArrivalTime = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"])).ToString("HH:mm");
                                            fsr.ArrivalAirportName = Convert.ToString(seg["arrival"]);
                                            fsr.ArrivalTerminal = "1";
                                            fsr.ArrAirportCode = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.Trip = objFlt.Trip.ToString();

                                            DateTime startTime = Convert.ToDateTime(Convert.ToString(seg["departDateTime"]));
                                            DateTime endTime = Convert.ToDateTime(Convert.ToString(seg["arrivalDateTime"]));

                                            TimeSpan span = endTime.Subtract(startTime);
                                            int difmin = 0;
                                            difmin = Convert.ToInt16(span.TotalMinutes);
                                            fsr.TotDur = GetTimeInHrsAndMin(difmin);
                                            fsr.TripCnt = fsr.TotDur;
                                            #region Fare
                                            float ChargeBU = 0;
                                            DataTable CommDt = new DataTable();
                                            Hashtable STTFTDS = new Hashtable();
                                            if (seg["fare"]["adtFare"] != null)
                                            {

                                                fsr.AdtBfare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["adtFare"]["baseFare"])));
                                                fsr.AdtCabin = "Y";
                                                fsr.AdtFSur = 0;
                                                fsr.AdtTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["adtFare"]["taxes"])) + srvCharge);
                                                fsr.AdtOT = (float)Math.Ceiling(fsr.AdtTax - fsr.AdtFSur);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                                fsr.AdtRbd = Convert.ToString(seg["bookingClassCode"]);
                                                fsr.AdtFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                                fsr.AdtFareType = Convert.ToString(seg["fare"]["refundable"]) == "True" ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                                                                                                                                          // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                                fsr.AdtFare = (float)Math.Ceiling(float.Parse((fsr.AdtTax + fsr.AdtBfare).ToString())); //  + srvCharge
                                                fsr.sno = Convert.ToString(array["data"]["search"]["searchId"]) + ":" + Convert.ToString(seg["flightId"]) + ":" + Convert.ToString(seg["source"]) + ":" + Convert.ToString(seg["flightClassCode"]) + ":" + strTokenID + ":" + Convert.ToString(seg["carrierLogo"]).Replace(":", "##");//fareInfo.FareID.Trim() + ":" + securityGUID + ":" + fareInfo.FareTypeName.Trim();
                                                fsr.fareBasis = fsr.AdtFarebasis;
                                                fsr.FBPaxType = "ADT";
                                                fsr.AdtFar = "NRM";
                                                fsr.BagInfo = Convert.ToString(seg["freeBaggage"]);
                                                fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());
                                                fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, objFlt.Trip.ToString());

                                                CommDt.Clear();
                                                CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.fareBasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                                fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                                fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS.Clear();
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, objFlt.TDS);
                                                fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                                fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                                fsr.AdtEduCess = 0;
                                                fsr.AdtHighEduCess = 0;
                                                fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                                fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                                        fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }
                                            }

                                            if (seg["fare"]["chdFare"] != null)
                                            {
                                                fsr.ChdBFare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["chdFare"]["baseFare"])));
                                                fsr.ChdCabin = "Y";
                                                fsr.ChdFSur = 0;
                                                fsr.ChdTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["chdFare"]["taxes"])) + srvCharge);
                                                fsr.ChdOT = (float)Math.Ceiling(fsr.ChdTax - fsr.ChdFSur);// + paxFare.AdditionalTxnFeePub + srvCharge);
                                                fsr.ChdRbd = Convert.ToString(seg["bookingClassCode"]);
                                                fsr.ChdFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                                fsr.ChdFare = (float)Math.Ceiling(float.Parse((fsr.ChdTax + fsr.ChdBFare).ToString())); //  + srvCharge

                                                fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                                fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, objFlt.Trip.ToString());
                                                CommDt.Clear();
                                                CommDt = objFltComm.GetFltComm_Gal(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, objFlt.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, objFlt.RetDate, fsr.ChdFarebasis, objFlt.HidTxtDepCity.Split(',')[1].ToString().Trim(), objFlt.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                                fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                                fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                                STTFTDS.Clear();
                                                STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, objFlt.TDS);
                                                fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                                fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                                fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                                fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                                fsr.ChdEduCess = 0;
                                                fsr.ChdHighEduCess = 0;

                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.ChdFare.ToString()));
                                                        fsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        fsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                            }

                                            if (seg["fare"]["infFare"] != null)
                                            {
                                                fsr.InfBfare = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["infFare"]["baseFare"])));
                                                fsr.InfCabin = "";
                                                fsr.InfFSur = 0;
                                                fsr.InfTax = (float)Math.Ceiling(float.Parse(Convert.ToString(seg["fare"]["infFare"]["taxes"])));
                                                fsr.InfOT = (float)Math.Ceiling(fsr.InfTax - fsr.InfFSur);//+ paxFare.AdditionalTxnFeePub);
                                                fsr.InfRbd = Convert.ToString(seg["bookingClassCode"]);
                                                fsr.InfFarebasis = Convert.ToString(seg["bookingClassCode"]);//; fareInfo.FBCode.Trim();
                                                fsr.InfFare = (float)Math.Ceiling(float.Parse((fsr.InfTax + fsr.InfBfare).ToString()));

                                                if (objFlt.IsCorp == true)
                                                {
                                                    try
                                                    {
                                                        DataTable MGDT = new DataTable();
                                                        MGDT = objFltComm.clac_MgtFee(objFlt.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.InfBfare.ToString()), decimal.Parse(fsr.InfFSur.ToString()), objFlt.Trip.ToString(), decimal.Parse(fsr.InfFare.ToString()));
                                                        fsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                        fsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                                    }
                                                    catch { }
                                                }

                                            }

                                            fsr.Searchvalue = Convert.ToString(seg) + "##" + strTokenID;
                                            fsr.OriginalTF = float.Parse(Convert.ToString(seg["fare"]["totalFare"]));
                                            fsr.OriginalTT = srvCharge;
                                            fsr.Provider = "NT";

                                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;


                                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(objlist.InfTF * objlist.Infant);
                                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(objlist.InfTF * objlist.Infant);
                                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +objFS.InfTds;
                                            fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + (fsr.InfMgtFee * fsr.Infant);

                                            fsr.AvailableSeats = "20";
                                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                                            fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));

                                            if (objFlt.RTF == true || objFlt.GDSRTF == true || isSplFare == true)
                                            {
                                                fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                            }
                                            else
                                            {
                                                if (fareType == "INB")
                                                {
                                                    fsr.Sector = objFlt.HidTxtDepCity.Split(',')[0] + ":" + objFlt.HidTxtArrCity.Split(',')[0];
                                                }
                                                else
                                                {
                                                    fsr.Sector = objFlt.HidTxtArrCity.Split(',')[0] + ":" + objFlt.HidTxtDepCity.Split(',')[0];
                                                }

                                            }
                                            #endregion

                                            fsr.OrgDestFrom = objFlt.HidTxtArrCity.Split(',')[0];
                                            fsr.OrgDestTo = objFlt.HidTxtDepCity.Split(',')[0];
                                            fsr.TripType = TripType.R.ToString();


                                            if (objFlt.Trip == Trip.I)
                                            {

                                                fsrList.Add(fsr);
                                            }
                                            else if (objFlt.Trip == Trip.D)
                                            {

                                                RfsrList.Add(fsr);
                                            }

                                            lineNum++;
                                        }
                                    }
                                    if (isSplFare && objFlt.Trip == Trip.D)
                                    {

                                        FinalList = RoundTripFare(fsrList, RfsrList, srvCharge);
                                    }
                                    else
                                    {
                                        FinalList = fsrList;
                                    }
                                }
                            }
                        }

                    }

                }
                #endregion

            }
            catch (Exception ex)
            {

            }

            return objFltComm.AddFlightKey(FinalList, isSplFare);
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Clear();
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        private List<FlightSearchResults> RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR, float srvCharge)
        {

            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            List<FlightSearchResults> Comb = new List<FlightSearchResults>();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            for (int k = 1; k <= LnOb; k++)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                for (int l = 1; l <= LnIb; l++)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    //List<FlightSearchResults> st = new List<FlightSearchResults>();

                    if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            // st = Merge(OB, IB, ln);
                            Final.AddRange(Merge(OB, IB, ln, srvCharge));
                            ln++;///Increment Total Ln
                        }
                    }
                    else
                    {
                        Final.AddRange(Merge(OB, IB, ln, srvCharge));
                        ln++;
                    }

                }

            }
            Comb.AddRange(Final);
            return Comb;
        }
        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln, float srvCharge)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
            //ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
            //CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;//,
            //InfSrvTax = 0, InfTF = 0;
            float ADTAgentMrk = 0, CHDAgentMrk = 0;

            float AdtDiscount1 = 0, AdtCB = 0, AdtSrvTax1 = 0, AdtDiscount = 0, AdtTF = 0, AdtTds = 0;
            float ChdDiscount1 = 0, ChdCB = 0, ChdSrvTax1 = 0, ChdDiscount = 0, ChdTF = 0, ChdTds = 0;
            float STax = 0, TFee = 0, TotDis = 0, TotCB = 0, TotTds = 0;
            float ADTAdminMrk = 0, ChdAdminMrk = 0;
            float TotMgtFee = 0, AdtMgtFee = 0, ChdMgtFee = 0, InfMgtFee = 0;







            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT - srvCharge;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare - srvCharge;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax - srvCharge;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            ADTAdminMrk = ADTAdminMrk + item.ADTAdminMrk + itemib.ADTAdminMrk;

            AdtDiscount1 = AdtDiscount1 + item.AdtDiscount1 + itemib.AdtDiscount1;
            AdtCB = AdtCB + item.AdtCB + itemib.AdtCB; ;
            AdtSrvTax1 = AdtSrvTax1 + item.AdtSrvTax1 + itemib.AdtSrvTax1; ;
            AdtDiscount = AdtDiscount + item.AdtDiscount + itemib.AdtDiscount; ;
            AdtTF = AdtTF + item.AdtTF + itemib.AdtTF; ;
            AdtTds = AdtTds + item.AdtTds + itemib.AdtTds;
            AdtMgtFee = AdtMgtFee + item.AdtMgtFee + itemib.AdtMgtFee;

            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT - srvCharge;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare - srvCharge;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax - srvCharge;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;
            ChdAdminMrk = ChdAdminMrk + item.CHDAdminMrk + itemib.CHDAdminMrk;


            ChdDiscount1 = ChdDiscount1 + item.ChdDiscount1 + itemib.ChdDiscount1;
            ChdCB = ChdCB + item.ChdCB + itemib.ChdCB;
            ChdSrvTax1 = ChdSrvTax1 + item.ChdSrvTax1 + itemib.ChdSrvTax1;
            ChdDiscount = ChdDiscount + item.ChdDiscount + itemib.ChdDiscount;
            ChdTF = ChdTF + item.ChdTF + itemib.ChdTF;
            ChdTds = ChdTds + item.ChdTds + itemib.ChdTds;
            ChdMgtFee = ChdMgtFee + item.ChdMgtFee + itemib.ChdMgtFee;


            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            InfMgtFee = InfMgtFee + item.InfMgtFee + itemib.InfMgtFee;

            #endregion

            #region TOTAL

            STax = 0;// (AdtSrvTax1 * Adult) + (ChdSrvTax1 * Child);
            TFee = (AdtTF * Adult) + (ChdTF * Child);// +(objlist.InfTF * objlist.Infant);
            TotDis = (AdtDiscount * Adult) + (ChdDiscount * Child);
            TotCB = (AdtCB * Adult) + (ChdCB * Child);
            TotTds = (AdtTds * Adult) + (ChdTds * Child);
            TotMgtFee = (AdtMgtFee * Adult) + (ChdMgtFee * Child) + (InfMgtFee * Infant);

            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = srvCharge;// item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child) + (ADTAdminMrk * Adult) + (ChdAdminMrk * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            TotalFare = TotalFare + TotMrkUp + STax + TFee + TotMgtFee;
            float NetFare = (TotalFare + TotTds) - (TotDis + TotCB + (ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;


                y.AdtDiscount1 = AdtDiscount1;
                y.AdtCB = AdtCB;
                y.AdtSrvTax1 = AdtSrvTax1;
                y.AdtDiscount = AdtDiscount;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                y.AdtMgtFee = AdtMgtFee;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;


                y.ChdDiscount1 = ChdDiscount1;
                y.ChdCB = ChdCB;
                y.ChdSrvTax1 = ChdSrvTax1;
                y.ChdDiscount = ChdDiscount;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;
                y.ChdMgtFee = ChdMgtFee;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                y.InfMgtFee = InfMgtFee;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;


                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotCB = TotCB;
                y.TotTds = TotTds;
                y.TotMgtFee = TotMgtFee;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;

                y.AdtDiscount1 = AdtDiscount1;
                y.AdtCB = AdtCB;
                y.AdtSrvTax1 = AdtSrvTax1;
                y.AdtDiscount = AdtDiscount;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                y.AdtMgtFee = AdtMgtFee;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;

                y.ChdDiscount1 = ChdDiscount1;
                y.ChdCB = ChdCB;
                y.ChdSrvTax1 = ChdSrvTax1;
                y.ChdDiscount = ChdDiscount;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;
                y.ChdMgtFee = ChdMgtFee;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                y.InfMgtFee = InfMgtFee;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = srvCharge;//OriginalTT;
                y.NetFare = NetFare;


                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotCB = TotCB;
                y.TotTds = TotTds;
                y.TotMgtFee = TotMgtFee;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }

        private string getAirlineName(string airl, List<AirlineList> AirList)
        {
            string airl1 = "";
            try
            {


                airl1 = ((from ar in AirList where ar.AirlineCode == airl select ar).ToList())[0].AlilineName;
            }
            catch { airl1 = airl; }
            return airl1;
        }
        private string getNPAirlineName(string airl)
        {
            string airl1 = "";
            try
            {
                List<AirlineList> AirList = new List<AirlineList>();
                AirList.Add(new AirlineList { AirlineCode = "U4", AlilineName = "Buddha Air" });
                AirList.Add(new AirlineList { AirlineCode = "S1", AlilineName = "Saurya Airlines" });
                AirList.Add(new AirlineList { AirlineCode = "RMK", AlilineName = "Simrik Airlines" });
                AirList.Add(new AirlineList { AirlineCode = "YT", AlilineName = "Yeti Airlines" });
                AirList.Add(new AirlineList { AirlineCode = "GA", AlilineName = "Goma Airlines" });
                AirList.Add(new AirlineList { AirlineCode = "NA", AlilineName = "Nepal Airlines" });
                airl1 = ((from ar in AirList where ar.AirlineCode == airl select ar).ToList())[0].AlilineName;
            }
            catch { airl1 = airl; }
            return airl1;
        }
        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }
    }

}
