﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.ESEWANEPAL
{
    public class EsevaUtility
    {

        public static string GetEsevaToken(string Authurl, string Email, string Password, out string TokenType)
        {
            string TokenId = string.Empty;
            TokenType = string.Empty;
            //https://uat-admin.esewatravels.com/api/domestic/v1/get-token
            try
            {
                string StrReq = GALWS.ESEWANEPAL.EsevaRequest.GetEsevaTokenReq(Email, Password);
                EsevaUtility.SaveFile(StrReq, "GetEsevaTokenReq_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));
             

                string StrRes = GALWS.ESEWANEPAL.EsevaUtility.GetResponse(Authurl, StrReq);
                EsevaUtility.SaveFile(StrReq, "GetEsevaTokenRes_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));
                if (!string.IsNullOrEmpty(StrRes))
                {
                   
                    var stuff = JObject.Parse(StrRes);
                    if (Convert.ToString(stuff["success"]) == "True")
                    {
                        TokenType = Convert.ToString(stuff["data"]["tokenType"]);//stuff.data.tokenType;
                        TokenId = Convert.ToString(stuff["data"]["accessToken"]); //stuff.data.accessToken;
                    }
                }

            }
            catch (Exception ex)
            {
                EsevaUtility.SaveFile(ex.Message, "GetEsevaTokenRes_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));
            }
            return TokenId;
        }
        public static string GetResponse(string url, string requestData ,string token="")
        {
            string responseXML = string.Empty;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                System.Net.ServicePointManager.Expect100Continue = false;
                byte[] data = Encoding.UTF8.GetBytes(requestData);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "POST";
                request.ContentType = "application/json";
                request.Accept = "application/json";                
                request.Headers.Add("Accept-Encoding", "gzip");
                if (token!="")
                {
                    request.Headers.Add("Authorization", "Bearer " + token);
                }
               
                Stream dataStream = request.GetRequestStream();
                dataStream.Write(data, 0, data.Length);
                dataStream.Close();
                HttpWebResponse webResponse = (HttpWebResponse)request.GetResponse();
                var rsp = webResponse.GetResponseStream();
                if (rsp == null)
                {
                    //throw exception
                }



                if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
                {
                    using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
                    {
                        responseXML = readStream.ReadToEnd();
                    }


                }
                else
                {
                    StreamReader reader = new StreamReader(rsp, Encoding.Default);
                    responseXML = reader.ReadToEnd();
                }

            }
            catch (WebException webEx)
            {
                //get the response stream
                WebResponse response = webEx.Response;
                Stream stream = response.GetResponseStream();
                 responseXML = new StreamReader(stream).ReadToEnd();
            }
            return responseXML;
        }
        #region String Utility Functions
        public static string[] Split(string input, string seperator)
        {
            return input.Split(new string[1] { seperator }, StringSplitOptions.None);
        }
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex, int endIndex)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, endIndex);
            //return the result of the operation
            return result;
        }

        public static void Split_Tax_YQ_OT(string[] tax, out double YQ, out double OT)
        {
            string[] tax1 = null;
            YQ = 0;
            OT = 0;

            try
            {
                for (int i = 0; i <= tax.Length - 2; i++)
                {
                    if (tax[i].Contains("YQ"))//Strings.InStr(tax[i], "YQ")
                    {
                        tax1 = tax[i].Split(':');
                        YQ = YQ + Convert.ToInt32(tax1[1]);
                    }
                    else
                    {
                        tax1 = tax[i].Split(':');
                        OT = OT + Convert.ToInt32(tax1[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }


            //totTax = YQ + YR + WO + OT;
            //totFare = basefare + totTax + SrvTax + TF + admrk;
            //netFare = (totFare + tds) - comm;
        }

        public static bool SaveTextToFile(string strData, string FullPath, ref string ErrInfo)
        {
            string Contents = null;
            bool Saved = false;
            StreamWriter objReader = default(StreamWriter);
            try
            {
                objReader = new StreamWriter(FullPath);
                objReader.Write(strData);
                objReader.Close();
                Saved = true;
            }
            catch (Exception Ex)
            {
                ErrInfo = Ex.Message;
            }
            return Saved;
        }

        public static string datecon(string MM)
        {
            string mm_str = "";
            switch (MM)
            {
                case "01":
                    mm_str = "JAN";
                    break;
                case "02":
                    mm_str = "FEB";
                    break;
                case "03":
                    mm_str = "MAR";
                    break;
                case "04":
                    mm_str = "APR";
                    break;
                case "05":
                    mm_str = "MAY";
                    break;
                case "06":
                    mm_str = "JUN";
                    break;
                case "07":
                    mm_str = "JUL";
                    break;
                case "08":
                    mm_str = "AUG";
                    break;
                case "09":
                    mm_str = "SEP";
                    break;
                case "10":
                    mm_str = "OCT";
                    break;
                case "11":
                    mm_str = "NOV";
                    break;
                case "12":
                    mm_str = "DEC";
                    break;
                default:

                    break;
            }

            return mm_str;

        }


        #endregion
        public static void SaveFile(string Res, string module)
        {
            string newFileName = module + DateTime.Now.ToString("hh_mm_ss");

            string activeDir = ConfigurationManager.AppSettings["TBOSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\";// +securityToken + @"\";
            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }
            try
            {
                //XDocument xmlDoc = XDocument.Parse(Res);

                //xmlDoc.Save(activeDir + @"\" + filename + ".xml");

                string path = activeDir + newFileName + ".txt";//@"c:\temp\MyTest.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {

                        sw.Write(Res);

                    }
                }



            }
            catch (Exception ex)
            {
            }
        }
    }
}
