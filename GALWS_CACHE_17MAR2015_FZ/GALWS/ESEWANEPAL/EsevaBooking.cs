﻿using STD.BAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.ESEWANEPAL
{
    public class EsevaBooking
    {
        public string NPLFightBook(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, ref ArrayList TktNoArray, string Constr, ref string bookingID)
        {
            Dictionary<string, string> Log = new Dictionary<string, string>();
            string PNR = "";

            string exep = "";
            TktNoArray = new ArrayList();
            try
            {

                string bookingRefId = string.Empty;
                string FlightId = string.Empty;
                string PgMobile = string.Empty;
                string PgEmail = string.Empty;
                string[] snoArr = Convert.ToString(FltDT.Rows[0]["sno"]).Split(':');

                string strTokenID = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC91YXQtYWRtaW4uZXNld2F0cmF2ZWxzLmNvbVwvYXBpXC9kb21lc3RpY1wvdjFcL2dldC10b2tlbiIsImlhdCI6MTYwMTkxOTY0OSwiZXhwIjoxNjA1MDczMjQ5LCJuYmYiOjE2MDE5MTk2NDksImp0aSI6Im56MG91UnFZY0x3SU1aVW4iLCJzdWIiOjYsInBydiI6IjQyZWRlMzMzNGEwNGNkNjVjYjJiZTNmNWFkZmViMmMxZWRkMzA3NzQifQ.4ClyW79wsTIv7hvGwB9tdQmVYHNkSTKDYPGZsIkLmeg";
                string AuthUrl = "";
                Dictionary<string, string> log = new Dictionary<string, string>();
                Dictionary<string, string> log1 = new Dictionary<string, string>();
                AuthUrl = "https://uat-admin.esewatravels.com/api/domestic/v1/";



                string FLT_STAT = "";
                bool islcc = false;
                if (FltDT.Rows[FltDT.Rows.Count - 1]["TripType"].ToString().Trim() == "R")
                {
                    FLT_STAT = "RTF";
                }
                string fResp = "";
                string fRResp = "";
                strTokenID = snoArr[4];
                GALWS.ESEWANEPAL.EsevaPricing ObjEseva = new GALWS.ESEWANEPAL.EsevaPricing(strTokenID, AuthUrl);

                fResp = ObjEseva.Getreservation(snoArr[0], snoArr[1], Convert.ToInt32(snoArr[2]), ref log, ref exep);
                if (FLT_STAT == "RTF" && Convert.ToString(FltDT.Rows[0]["Trip"]).Trim().ToUpper() == "D")
                {
                    string[] snoArrR = Convert.ToString(FltDT.Rows[FltDT.Rows.Count - 1]["sno"]).Split(':');
                    fRResp = ObjEseva.Getreservation(snoArrR[0], snoArrR[1], Convert.ToInt32(snoArrR[2]), ref log, ref exep);
                }
                decimal totfare = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"]);
                decimal Changefare = 0;
                if (!string.IsNullOrEmpty(fResp))
                {
                    Newtonsoft.Json.Linq.JObject array = Newtonsoft.Json.Linq.JObject.Parse(fResp);

                    if (Convert.ToString(array["success"]) == "True")
                    {
                        if (Convert.ToString(array["data"]) != "" && Convert.ToString(array["data"]) != "null")
                        {

                            string type = array["data"]["flights"]["outbound"].Type.ToString();
                            //totalFares
                            if (type != "Array")
                            {
                                Changefare = Convert.ToDecimal(Convert.ToDecimal(Convert.ToString(array["data"]["totalFares"]["totalFare"])));
                                bookingRefId = Convert.ToString(array["data"]["bookingRefId"]);
                                FlightId = Convert.ToString(array["data"]["flights"]["outbound"]["flightId"]);
                                PgMobile = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]);
                                PgEmail = Convert.ToString(FltHdrs.Tables[0].Rows[0]["PGEmail"]);
                                if (totfare > 0 && Convert.ToDecimal(Convert.ToDecimal(Convert.ToString(array["data"]["totalFares"]["totalFare"]))) > 0 && totfare != Convert.ToDecimal(Convert.ToDecimal(Convert.ToString(array["data"]["totalFares"]["totalFare"]))))
                                {
                                    goto FareMisMatch;
                                }
                                string strJsonData = EsevaRequest.GetBookingReq(PaxDs, bookingRefId, FlightId, PgMobile, PgEmail);

                                Log.Add("BookReq", strJsonData);
                                EsevaUtility.SaveFile(strJsonData, "BookReq_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));
                                string strJsonResponse = EsevaUtility.GetResponse(AuthUrl + "issue-ticket", strJsonData, strTokenID);
                                Log.Add("BookResp", strJsonResponse);
                                EsevaUtility.SaveFile(strJsonResponse, "BookRes_" + System.DateTime.Now.ToString("ddMMyyyyHHmmssfff"));

                                if (!string.IsNullOrEmpty(strJsonResponse))
                                {
                                    Newtonsoft.Json.Linq.JObject array2 = Newtonsoft.Json.Linq.JObject.Parse(strJsonResponse);
                                    if (Convert.ToString(array2["success"]) == "True")
                                    {
                                        TktNoArray = new ArrayList();
                                        bookingID = "";
                                        
                                        Newtonsoft.Json.Linq.JArray passengersArr = (Newtonsoft.Json.Linq.JArray)array2["data"]["flights"]["outbound"]["passengers"];
                                        foreach (var M in passengersArr)
                                        {
                                            string ssb = Convert.ToString(M["firstName"]).Replace(" ", "");//+ Convert.ToString(M["lastName"]).Replace(" ", "");
                                          //  ssb += Convert.ToString(M["title"]) == "NA" ? "" : Convert.ToString(M["title"]);
                                          //  ssb += Convert.ToString(M["lastName"]) == "N/A" ? "" : Convert.ToString(M["lastName"]);
                                            PNR = Convert.ToString(M["pnrNo"]) == "" ? "-FQ" : Convert.ToString(M["pnrNo"]);
                                            bookingID = Convert.ToString(array["data"]["ticketRefId"]);
                                            if (Convert.ToString(M["ticketNo"]) != "")
                                            {
                                                TktNoArray.Add(ssb.ToUpper().Trim() + "/" + Convert.ToString(M["ticketNo"]));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                exep = exep + fResp + fRResp;


            FareMisMatch:
                exep = exep + "Fare Amount Diffrence new fare:" + Changefare + " old Fare:" + totfare.ToString();
            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
                PNR = PNR + "-FQ";
            }
            finally
            {
                if (string.IsNullOrEmpty(PNR) == true)
                {
                    PNR = PNR + "-FQ";
                }

                if (TktNoArray.Count == 0)
                {
                    TktNoArray.Add("AIRLINE");
                }
                FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

                objCommonBAL.InsertTBOBookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
                    , Log.ContainsKey("BookReq") == true ? Log["BookReq"] : "", Log.ContainsKey("BookResp") == true ? Log["BookResp"] : ""
                    , Log.ContainsKey("RepriceReq") == true ? Log["RepriceReq"] : ""
                    , Log.ContainsKey("RepriceResp") == true ? Log["RepriceResp"] : "", exep);
            }

            return PNR;


        }
    }
}
