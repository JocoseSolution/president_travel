﻿using Newtonsoft;
using Newtonsoft.Json.Linq;
using STD.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.ESEWANEPAL
{
    public class EsevaRequest
    {
        string TokenId = string.Empty;
        string UserId = string.Empty;
        string Password = string.Empty;
        string TokenType = string.Empty;
        string AuthUrl = string.Empty;
        string ConStr = string.Empty;
        public EsevaRequest()
        {

        }
        public static string GetEsevaTokenReq(string Email, string Password)
        {
            string req = string.Empty;
            try
            {
                StringBuilder Jreq = new StringBuilder();
                Jreq.Append("{\"email\":\"" + Email + "\",\"password\":\"" + Password + "\"}");
                req = Jreq.ToString();
            }
            catch (Exception ex)
            {

            }
            return req;
        }


        public static string GetSearchReq(FlightSearch objFlt, string FareType,bool IsSpl)
        {
            string req = string.Empty;
            try
            {

                StringBuilder jreq = new StringBuilder();
                if (IsSpl==true && FareType == "INB")
                {
                    //jreq.Append("{\"tripType\":\"" + objFlt.TripType.ToString() + "\"");
                    jreq.Append("{\"tripType\":\"RoundTrip\"");
                    jreq.Append(",\"departureCity\":\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\"");
                    jreq.Append(",\"destinationCity\":\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\"");
                    jreq.Append(",\"departureDate\":\"" + EsevaUtility.Right(objFlt.DepDate, 4) + "-" + EsevaUtility.Mid(objFlt.DepDate, 3, 2) + "-" + EsevaUtility.Left(objFlt.DepDate, 2) + "\"");
                    if (objFlt.TripType.ToString() == "RoundTrip")
                    {
                        jreq.Append(",\"returnDate\":\"" + EsevaUtility.Right(objFlt.RetDate, 4) + "-" + EsevaUtility.Mid(objFlt.RetDate, 3, 2) + "-" + EsevaUtility.Left(objFlt.RetDate, 2) + "\"");
                    }
                    jreq.Append(",\"adults\":\"" + objFlt.Adult + "\"");
                    jreq.Append(",\"children\":\"" + objFlt.Child + "\"");
                    jreq.Append(",\"infants\":\"" + objFlt.Infant + "\"");
                    jreq.Append(",\"nationality\":\"NP\"}");
                }
                else if (IsSpl == false && FareType == "OutB")
                {
                    jreq.Append("{\"tripType\":\"OneWay\"");
                    jreq.Append(",\"departureCity\":\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\"");
                    jreq.Append(",\"destinationCity\":\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\"");
                    jreq.Append(",\"departureDate\":\"" + EsevaUtility.Right(objFlt.RetDate, 4) + "-" + EsevaUtility.Mid(objFlt.RetDate, 3, 2) + "-" + EsevaUtility.Left(objFlt.RetDate, 2) + "\"");
                    jreq.Append(",\"adults\":\"" + objFlt.Adult + "\"");
                    jreq.Append(",\"children\":\"" + objFlt.Child + "\"");
                    jreq.Append(",\"infants\":\"" + objFlt.Infant + "\"");
                    jreq.Append(",\"nationality\":\"NP\"}");
                }
                else if (IsSpl==false && FareType == "INB")
                {
                    jreq.Append("{\"tripType\":\"OneWay\"");
                    jreq.Append(",\"departureCity\":\"" + objFlt.HidTxtDepCity.Split(',')[0] + "\"");
                    jreq.Append(",\"destinationCity\":\"" + objFlt.HidTxtArrCity.Split(',')[0] + "\"");
                    jreq.Append(",\"departureDate\":\"" + EsevaUtility.Right(objFlt.DepDate, 4) + "-" + EsevaUtility.Mid(objFlt.DepDate, 3, 2) + "-" + EsevaUtility.Left(objFlt.DepDate, 2) + "\"");
                    jreq.Append(",\"adults\":\"" + objFlt.Adult + "\"");
                    jreq.Append(",\"children\":\"" + objFlt.Child + "\"");
                    jreq.Append(",\"infants\":\"" + objFlt.Infant + "\"");
                    jreq.Append(",\"nationality\":\"NP\"}");
                }
              

                req = jreq.ToString();
            }
            catch (Exception ex)
            {

            }
            return req;
        }
        public static string GetEsevaReservationReq(string searchId, string flightId, int source)
        {
            string req = string.Empty;
            try
            {
                StringBuilder Jreq = new StringBuilder();
                //Jreq.Append("{\"searchId\":\"" + searchId + "\",\"flightId\":\"" + flightId + "\",\"source\":\"" + source + "\"}");
                Jreq.Append("{\"searchId\":\"" + searchId + "\",\"flightId\":\"" + flightId + "\",\"source\":" + source + "}");
                req = Jreq.ToString();
            }
            catch (Exception ex)
            {

            }
            return req;
        }

        public static string GetBookingReq(DataSet PaxDs, string bookingRefId, string flightId, string PgMobile, string email)
        {
            string req = string.Empty;
            try
            {
                DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");
                StringBuilder jreq = new StringBuilder();

                jreq.Append("{\"bookingRefId\":\"" + bookingRefId + "\"");
                jreq.Append(",\"flightId\":\"" + flightId + "\"");

                jreq.Append(",\"contactName\":\"" + Convert.ToString(ADTPax[0]["Title"]) + " " + Convert.ToString(ADTPax[0]["FName"]) + " " + Convert.ToString(ADTPax[0]["LName"]) + "\"");
                jreq.Append(",\"contactEmail\":\"" + email + "\"");
                jreq.Append(",\"contactPhone\":\"" + PgMobile + "\"");

                //   jreq.Append(",\"contacts\":{\"name\": \"" + Convert.ToString(ADTPax[0]["Title"]) + " " + Convert.ToString(ADTPax[0]["FName"]) + " " + Convert.ToString(ADTPax[0]["LName"]) + "\"");
                //   jreq.Append(",\"email\": \"" + email + "\"");
                //   jreq.Append(",\"phone\": \"" + PgMobile + "\"}");
                jreq.Append(",\"passengers\":[");
                #region adult

                if (ADTPax.Count() > 0)
                {
                    for (int i = 0; i < ADTPax.Count(); i++)
                    {
                        if (i == 0)
                        {
                            // jreq.Append("{\"paxType\": \"ADT\"");
                            jreq.Append("{\"type\": \"ADT\"");
                        }
                        else
                        {
                            //jreq.Append(",{\"paxType\": \"ADT\"");
                            jreq.Append(",{\"type\": \"ADT\"");
                        }
                        jreq.Append(",\"title\": \"" + Convert.ToString(ADTPax[i]["Title"]).ToUpper() + "\"");
                        jreq.Append(",\"gender\": \"" + Convert.ToString(ADTPax[i]["Gender"]).Trim().ToUpper() + "\"");
                        jreq.Append(",\"firstName\": \"" + Convert.ToString(ADTPax[i]["FName"]) + "\"");
                        jreq.Append(",\"lastName\": \"" + Convert.ToString(ADTPax[i]["LName"]) + "\"");
                        jreq.Append(",\"nationality\": \"NP\"");
                        if (Convert.ToString(ADTPax[i]["PassportNo"]) != "")
                        {
                            jreq.Append(",\"documentType\": \"P\"");
                            jreq.Append(",\"documentNumber\": \"" + Convert.ToString(ADTPax[i]["PassportNo"]) + "\"}");
                            //jreq.Append(",\"document\":{\"type\": \"P\"");
                            // jreq.Append(",\"number\": \"" + Convert.ToString(ADTPax[i]["PassportNo"]) + "\"}}");
                        }
                        else
                        {
                            jreq.Append(",\"documentType\": \"\"");
                            jreq.Append(",\"documentNumber\": \"\"}");
                        }

                    }


                }
                #endregion
                #region Child

                if (CHDPax.Count() > 0)
                {
                    for (int i = 0; i < CHDPax.Count(); i++)
                    {

                        jreq.Append(",{\"paxType\": \"CHILD\"");
                        jreq.Append(",\"type\": \"CHILD\"");
                        jreq.Append(",\"title\": \"" + Convert.ToString(CHDPax[i]["Title"]) + "\"");
                        jreq.Append(",\"gender\": \"" + Convert.ToString(CHDPax[i]["Gender"]).Trim().ToUpper() + "\"");
                        jreq.Append(",\"firstName\": \"" + Convert.ToString(CHDPax[i]["FName"]) + "\"");
                        jreq.Append(",\"lastName\": \"" + Convert.ToString(CHDPax[i]["LName"]) + "\"");
                        jreq.Append(",\"nationality\": \"NP\"");
                        if (Convert.ToString(CHDPax[i]["PassportNo"]) != "")
                        {
                            jreq.Append(",\"documentType\": \"P\"");
                            jreq.Append(",\"documentNumber\": \"" + Convert.ToString(CHDPax[i]["PassportNo"]) + "\"}");

                        }
                        else
                        {
                            jreq.Append(",\"documentType\": \"\"");
                            jreq.Append(",\"documentNumber\": \"\"}");
                        }
                    }
                }
                #endregion
                //#region Infant

                //if (INFPax.Count() > 0)
                //{
                //    for (int i = 0; i < INFPax.Count(); i++)
                //    {
                //        jreq.Append(",{\"paxType\": \"INFANT\"");
                //        jreq.Append(",\"type\": \"INFANT\"");
                //        jreq.Append(",\"title\": \"" + Convert.ToString(INFPax[i]["Title"]) + "\"");
                //        jreq.Append(",\"gender\": \"" + Convert.ToString(INFPax[i]["Gender"]).Trim().ToUpper() + "\"");
                //        jreq.Append(",\"firstName\": \"" + Convert.ToString(INFPax[i]["FName"]) + "\"");
                //        jreq.Append(",\"lastName\": \"" + Convert.ToString(INFPax[i]["LName"]) + "\"");
                //        jreq.Append(",\"nationality\": \"NP\"");
                //        if (Convert.ToString(INFPax[i]["PassportNo"]) != "")
                //        {
                //            jreq.Append(",\"document\":{\"type\": \"P\"");
                //            jreq.Append(",\"number\": \"" + Convert.ToString(INFPax[i]["PassportNo"]) + "\"}}");
                //        }
                //        else
                //        {
                //            jreq.Append(",\"document\":{\"type\": \"\"");
                //            jreq.Append(",\"number\": \"\"}}");
                //        }
                //    }
                //}
                //#endregion
                jreq.Append("]}");
                req = jreq.ToString();
            }
            catch (Exception ex)
            {

            }
            return req;
        }
    }
}
