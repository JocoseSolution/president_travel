﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.AirArabia
{
   public class AirArabiaShared
    {
    }

    public class PriceQuoteRequest
    {
        public string ArrivalDate { get; set; }
        public string DepartureDate { get; set; }
        public string RPH { get; set; }
        public string FlightNumber { get; set; }
        public string ArrAirportCode { get; set; }
        public string DepAirportCode { get; set; }
        public int Segment { get; set; }
        public int LineNo { get; set; }
        public string TripType { get; set; }
        public string ServerUrlorIp { get; set; }
        public string Port { get; set; }      
        public string JourneyDuration { get; set; }
        public string CorporateId { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
 
    }
    public class OriginDestinationInformation
     {
         public List<OriginDestinationOption> OriginDestinationOptions { get; set; }
         public string DepartureDateTime { get; set; }
         public string ArrivalDateTime { get; set; }
         public string OriginLocationCode { get; set; }
         public string DestinationLocationCode { get; set; }
         public string OriginLocationName { get; set; }
         public string DestinationLocationName { get; set; }
         public int LineNo { get; set; }
         public string FlightNOKey { get; set; }
     }
    public class OriginDestinationOption
    {
        public string DepartureDateTime { get; set; }
        public string ArrivalDateTime { get; set; }
        public string ArrivalAirport { get; set; }
        public string DepartureAirport { get; set; }
        public string FlightNumber { get; set; }
        public string RPH { get; set; }
        public string JourneyDuration { get; set; }
        public int Segment { get; set; }
    }
    public class AirItinerary
    {
        public string DirectionInd { get; set; }
        public List<OriginDestinationOption> OriginDestinationOptions { get; set; }
        public List<SSRDetails> SpecialReqDetails { get; set; }
    }
   
    public class SSRDetails
    {
        public string SSRType { get; set; }
        public List<SSRRequest> SSRRequests { get; set; }

    }
    public class SSRRequest
    {
        public string SSRCode { get; set; }
        public string Quantity { get; set; }
        public string DepartureDateTime { get; set; }
        public string FlightNumber { get; set; }
        public string TravelerRefNumberRPHList { get; set; }
        public string FlightRefNumberRPHList { get; set; }
       
    }
    public class AirArabiaSSR
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public string SSRType { get; set; }
        public string Destination { get; set; }
        public string Origin { get; set; }
        public string WayType { get; set; }
        public List<OriginDestinationOption> OriginDestinationOptions { get; set; }
    }
}
