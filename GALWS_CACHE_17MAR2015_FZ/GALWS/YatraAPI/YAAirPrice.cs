﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using STD.Shared;
using System.Configuration;

namespace STD.BAL
{
    public class YAAirPrice
    {


        public string AirPrice(string OriginDestinationO, string OriginDestinationR, int adultCount, int chdCount, int infCount, string serviceUrl, string methodName, ref Dictionary<string, string> Log, ref string exep,string orderID,string constr, string trip="")
        {
            StringBuilder YAPriceReq = new StringBuilder();
            string res = "";

            string dest = "";

            if (trip.ToUpper().Trim()== "I")
            {
                dest = "INT";
            }

            try
            {
                YAPriceReq.Append("<?xml version='1.0' encoding='UTF-8'?>");

                YAPriceReq.Append("<soap:Envelope xmlns='http://www.opentravel.org/OTA/2003/05' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wsse='http://schemas.xmlsoap.org/ws/2002/12/secext'>");
                YAPriceReq.Append("<soap:Body>");

                YAPriceReq.Append("<OTA_AirPriceRQ xmlns='http://www.opentravel.org/OTA/2003/05'>");
                YAPriceReq.Append("<POS xmlns='http://www.opentravel.org/OTA/2003/05'>");
                YAPriceReq.Append("<Source AgentSine='' PseudoCityCode='NPCK' TerminalID='1'>");
                YAPriceReq.Append("<RequestorID ID='AFFILIATE' />");
                YAPriceReq.Append("</Source>");
                YAPriceReq.Append("<YatraRequests>");
                string spl = string.IsNullOrEmpty(OriginDestinationR) ? "" : " RequestType='SR' ";
                string midOfficeID = ConfigurationManager.AppSettings["MidOfficeAgentID"].ToString();
                YAPriceReq.Append("<YatraRequest DoNotHitCache='true' DoNotCache='false' MidOfficeAgentID='" + midOfficeID + "' AffiliateID='YTFABTRAVEL' YatraRequestTypeCode='SMPA' " + spl + "     Destination='" + dest + "'  />");//RequestType='SR' SplitResponses='true'
                YAPriceReq.Append("</YatraRequests>");
                YAPriceReq.Append("</POS>");
                YAPriceReq.Append("<SpecificFlightInfo>");
                YAPriceReq.Append("<BookingClassPref ResBookDesigCode=''></BookingClassPref>");
                YAPriceReq.Append("</SpecificFlightInfo>");
                YAPriceReq.Append("<AirItinerary>");
                YAPriceReq.Append("<OriginDestinationOptions>");
                #region OriginDestination

                XDocument xmlDoc = XDocument.Parse(OriginDestinationO);

                IEnumerable<XElement> xlOrgDestOptionO =  from el in xmlDoc.Descendants("OriginDestinationOptions").Descendants("OriginDestinationOption")
                                                                                select el;
                string odopO = "";

                xlOrgDestOptionO.ToList().ForEach(x => { odopO = odopO + x.ToString(); });

                YAPriceReq.Append(odopO);

                if (!string.IsNullOrEmpty(OriginDestinationR))
                {

                    XDocument xmlDocR = XDocument.Parse(OriginDestinationR);

                    IEnumerable<XElement> xlOrgDestOptionR = from el in xmlDocR.Descendants("OriginDestinationOptions").Descendants("OriginDestinationOption")
                                                             select el;
                    string odopR = "";

                    xlOrgDestOptionR.ToList().ForEach(x => { odopR = odopR + x.ToString(); });


                    YAPriceReq.Append(odopR);
                }
                #endregion


                YAPriceReq.Append("</OriginDestinationOptions>");
                YAPriceReq.Append("</AirItinerary>");
                YAPriceReq.Append("<TravelerInfoSummary>");
                YAPriceReq.Append("<AirTravelerAvail>");
                YAPriceReq.Append("<PassengerTypeQuantity Code='ADT' Quantity='" + adultCount + "'></PassengerTypeQuantity>");
                if (chdCount > 0)
                {
                    YAPriceReq.Append("<PassengerTypeQuantity Code='CHD' Quantity='" + chdCount + "'></PassengerTypeQuantity>");
                }
                if (infCount > 0)
                {
                    YAPriceReq.Append("<PassengerTypeQuantity Code='INF' Quantity='" + infCount + "'></PassengerTypeQuantity>");
                }

                YAPriceReq.Append("</AirTravelerAvail>");
                YAPriceReq.Append("</TravelerInfoSummary>");
                YAPriceReq.Append("</OTA_AirPriceRQ>");
                YAPriceReq.Append("</soap:Body>");
                YAPriceReq.Append("</soap:Envelope>");

                res = YAUtility.PostXml(serviceUrl, YAPriceReq.ToString(), "", "", methodName, ref exep);

                try
                {
                    FlightCommonBAL objbal = new FlightCommonBAL(constr);

                    objbal.InserAndGetYAPricingLog(orderID, YAPriceReq.ToString(), res, "insert");
                }
                catch (Exception ex1)
                {
                    exep = exep + ex1.Message + " stackTrace:" + ex1.StackTrace.ToString();
                }
            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + " stackTrace:" + ex.StackTrace.ToString();

            }
            finally
            {
                YAUtility.SaveXml(YAPriceReq.ToString(), "", "OTA_AirPriceRS_Req");
                YAUtility.SaveXml(res, "", "OTA_AirPriceRS_Res");

                Log.Add("OTA_AirPriceRS_Req", YAPriceReq.ToString());

                Log.Add("OTA_AirPriceRS_Res", res);
            }

            

            return res;

        }


        public string GetItineraryDeatilsFromPriceRS(string response)
        {
            string result = "";
            string str = response.Replace("xmlns:soapenv='\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                 .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                 .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                 .Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "")
                 .Replace("xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\"", "")
                 .Replace("ns1:", "");

            //XDocument xmlDoc = XDocument.Parse(str);

            //IEnumerable<XElement> xlFPricedItinerary = from el in xmlDoc.Descendants("OTA_AirPriceRS").Descendants("PricedItineraries").Descendants("PricedItinerary")
            //                                           select el;
           
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(str);
           System.Xml.XmlNodeList  nodeList=  doc.GetElementsByTagName("PricedItinerary");

           for (int ptn = 0; ptn < nodeList.Count; ptn++)
            {
                result = result + String.Concat(nodeList[ptn].InnerXml);
            }

            result =result.Replace("AirItineraryPricingInfo", "PriceInfo");
            return result;
        }

        public decimal ParseAirPrice(string response)
        {
            string str = response.Replace("xmlns:soapenv='\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                 .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                 .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                 .Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "")
                 .Replace("xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\"", "")
                 .Replace("ns1:", "");

            XDocument xmlDoc = XDocument.Parse(str);

            IEnumerable<XElement> xlFPricedItinerary = from el in xmlDoc.Descendants("OTA_AirPriceRS").Descendants("PricedItineraries").Descendants("PricedItinerary")
                                                       select el;

            decimal totalfare = 0;

            for (int ptn = 0; ptn < xlFPricedItinerary.Count(); ptn++)
            {
                totalfare = totalfare + Convert.ToDecimal(xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo").Descendants("ItinTotalFare").Descendants("TotalFare").FirstOrDefault().Attribute("Amount").Value);


                //IEnumerable<XElement> AdtMarkup = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo").Descendants("ItinTotalFare").Descendants("Markups").Descendants("Markup");


                //for (int m = 0; m < AdtMarkup.Count(); m++)
                //{
                //    totalfare = totalfare + Convert.ToDecimal(AdtMarkup.ElementAt(m).Attribute("Amount").Value);
                //}

                //IEnumerable<XElement> TFee = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo").Descendants("ItinTotalFare").Descendants("Fees").Descendants("Fee");
                //for (int t = 0; t < TFee.Count(); t++)
                //{
                //    totalfare = totalfare + Convert.ToDecimal(AdtMarkup.ElementAt(t).Attribute("Amount").Value);
                //}

               // totalfare = totalfare + Convert.ToDecimal(xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo").Descendants("ItinTotalFare").Descendants("ServiceTax").FirstOrDefault().Attribute("Amount").Value);

            }


            return totalfare;
        }


        public List<YASSR> GetSSR(string rersponse)
        {
            List<YASSR> ssrList = new List<YASSR>();


            string str = rersponse.Replace("xmlns:soapenv='\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                  .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                  .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                  .Replace("xmlns=\"http://www.opentravel.org/OTA/2003/05\"", "")
                  .Replace("xmlns:ns1=\"http://www.opentravel.org/OTA/2003/05\"", "")
                  .Replace("ns1:", "");

            XDocument xmlDoc = XDocument.Parse(str);

            IEnumerable<XElement> xlFPricedItinerary = from el in xmlDoc.Descendants("OTA_AirPriceRS").Descendants("PricedItineraries").Descendants("PricedItinerary")
                                                       select el;

            for (int ptn = 0; ptn < xlFPricedItinerary.Count(); ptn++)
            {

                IEnumerable<XElement> xlMeal = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo").Descendants("Meal").Descendants("SSRCode");

                for (int m = 0; m < xlMeal.Count(); m++)
                {
                    YASSR objssr = new YASSR();

                    objssr.SSRType = YASSRType.Meal;
                    objssr.TripType = xlMeal.ElementAt(m).Attribute("UniqueIdentifier").Value.Trim() == "2" ? TripType.R : TripType.O;
                    objssr.Code = xlMeal.ElementAt(m).Attribute("Code").Value.Trim();
                    objssr.Amount = Convert.ToDecimal(xlMeal.ElementAt(m).Attribute("Amount").Value.Trim());
                    objssr.Desc = xlMeal.ElementAt(m).Attribute("Desc").Value.Trim();
                    ssrList.Add(objssr);




                }


                IEnumerable<XElement> xlBag = xlFPricedItinerary.ElementAt(ptn).Descendants("AirItineraryPricingInfo").Descendants("FareBaggage").Descendants("SSRCode");

                for (int m = 0; m < xlBag.Count(); m++)
                {
                    YASSR objssr = new YASSR();

                    objssr.SSRType = YASSRType.Baggage;
                    objssr.TripType = xlBag.ElementAt(m).Attribute("UniqueIdentifier").Value.Trim()=="2" ? TripType.R : TripType.O;
                    objssr.Code = xlBag.ElementAt(m).Attribute("Code").Value.Trim();
                    objssr.Amount = Convert.ToDecimal(xlBag.ElementAt(m).Attribute("Amount").Value.Trim());
                    objssr.Desc = xlBag.ElementAt(m).Attribute("Desc").Value.Trim();
                    ssrList.Add(objssr);


                }




            }


            return ssrList;

        }

        public List<YASSR> GetSSRTripTypeWise(TripType trptype, List<YASSR> ssrList)
        {



            return ssrList.Where(x => x.TripType == trptype).ToList();

        }


        public List<YASSR> GetYaSSrMealBagList(YASSRType ssrType, List<YASSR> ssrList)
        {

            return ssrList.Where(x => x.SSRType == ssrType).ToList();

        }



    }
}
