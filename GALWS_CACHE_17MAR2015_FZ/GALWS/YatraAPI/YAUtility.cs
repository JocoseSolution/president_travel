﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;

namespace STD.BAL
{
    public class YAUtility
    {


        public static void SaveXml(string Res, string securityToken, string filename)
        {
            string activeDir = ConfigurationManager.AppSettings["YASaveUrl"] + DateTime.Now.ToString("dd-MM-yyyy") + @"\";// +securityToken + @"\";
            filename = filename + DateTime.Now.ToString("hh_mm_ss");
            try
            {

                //ConfigurationManager.AppSettings["YASaveUrl"]

                DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
                if (!Directory.Exists(objDirectoryInfo.FullName))
                {
                    Directory.CreateDirectory(activeDir);
                }
                try
                {
                    XDocument xmlDoc = XDocument.Parse(Res);
                    xmlDoc.Save(activeDir + filename + ".xml");
                }
                catch (Exception ex)
                {
                    WriteErrorLog(Res, activeDir + filename + ".xml");
                }
            }
            catch (Exception ex1)
            {

                WriteErrorLog(Res, activeDir + filename + ".xml");
            }
        }


        public static bool WriteErrorLog(string logMessage, string url)
        {
            bool successStatus = false;
            //LogDirectory = ConfigurationManager.AppSettings["LogDirectory"].ToString();
            DateTime currentDateTime = DateTime.Now;
            string currentDateTimeString = currentDateTime.ToString();
            // CheckCreateLogDirectory(LogDirectory);
            string logLine = BuildLogLine(currentDateTime, logMessage);
            // LogDirectory = (LogDirectory + "Log_" + LogFileName(DateTime.Now) + ".txt");

            StreamWriter m_logSWriter = null;
            try
            {
                m_logSWriter = new StreamWriter(url, true);
                m_logSWriter.WriteLine(logLine);
                successStatus = true;
            }
            catch
            {
            }
            finally
            {
                if (m_logSWriter != null)
                {
                    m_logSWriter.Close();
                }
            }

            return successStatus;
        }

        private static string BuildLogLine(DateTime currentDateTime, string logMessage)
        {
            StringBuilder loglineStringBuilder = new StringBuilder();
            loglineStringBuilder.Append(LogFileEntryDateTime(currentDateTime));
            loglineStringBuilder.Append("\t");
            loglineStringBuilder.Append(logMessage);
            return loglineStringBuilder.ToString();
        }


        public static string LogFileEntryDateTime(DateTime currentDateTime)
        {
            return currentDateTime.ToString("dd-MM-yyyy HH:mm:ss");
        }








        public static string PostXml(string url, string xml, string Userid, string Pwd, string SOAPAction, ref string exep)
        {
            StringBuilder sbResult = new StringBuilder();
            string rawResponse = "";
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    //Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = @"text/xml";
                    Http.KeepAlive = false;
                    // Http.Credentials = new NetworkCredential(Userid, Pwd);
                    //string auth = Userid + ":" + Pwd;
                    //auth = Convert.ToBase64String(Encoding.Default.GetBytes(auth));
                    // Http.Headers["Authorization"] = "Basic " + auth;
                    Http.Headers.Add(String.Format("SOAPAction: \"{0}\"", SOAPAction));
                    // Http.PreAuthenticate = true;
                    Http.AllowAutoRedirect = false;
                    Http.ReadWriteTimeout = 3000000;

                    //Http.KeepAlive = true;
                    //Http.Credentials = new NetworkCredential(Userid, Pwd);
                    //Http.Headers.Add(String.Format("SOAPAction: \"{0}\"", SOAPAction));
                    //Http.PreAuthenticate = true;

                    //using (Stream PostStream = new GZipStream(Http.GetRequestStream(), CompressionMode.Compress, true))


                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        //else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        //{
                        //    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        //}
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (WebException wex)
            {
                try
                {
                    rawResponse = "Error: " + new StreamReader(wex.Response.GetResponseStream()).ReadToEnd();

                    exep = " PostXML Method (" + SOAPAction + ") :" + wex.Message + " StackTrace:" + wex.StackTrace;
                }
                catch { rawResponse = "Error: " + wex.Message + wex.InnerException.Message + url + " /" + SOAPAction + xml; }
            }

            catch (Exception ex)
            {
                //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, ex, "699", rawResponse, string.Empty, "");
                rawResponse = "Error: " + ex.Message + " StackTrace: " + ex.StackTrace;
                exep = exep + " PostXML Method (" + SOAPAction + ") :" + ex.Message + " StackTrace: " + ex.StackTrace;
            }
            sbResult.Append(rawResponse);
            return sbResult.ToString();
        }
    }
}
