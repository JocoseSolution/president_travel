﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace STD.BAL
{
  public  class FZTravelAgent
    {
      string SecurityToken;
      string IATANumber;
      string UserName;
      string Password;
      string IP;

      public FZTravelAgent(string securityToken,string Iatanumber,string username,string password,string ip)
      {
          SecurityToken = securityToken;
          IATANumber = Iatanumber;
          UserName = username;
          Password = password;
          IP = ip;
      }

      public bool LoginTA(string methodUrl, string serviceUrl, ref string exep)
      {
          bool status = false;

         StringBuilder LoginTAReq = new StringBuilder();
          LoginTAReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.TravelAgents.Request\">");
          LoginTAReq.Append("<soapenv:Header/><soapenv:Body><tem:LoginTravelAgent><!--Optional:--><tem:LoginTravelAgentRequest>");
          LoginTAReq.Append("<rad:SecurityGUID>"+SecurityToken+"</rad:SecurityGUID>");
          LoginTAReq.Append("<rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode><rad:AccessibleCarrierCode>FZ</rad:AccessibleCarrierCode></rad:CarrierCode></rad:CarrierCodes><!--Optional:-->");
          LoginTAReq.Append("<rad:ClientIPAddress>"+IP+"</rad:ClientIPAddress>");
         // LoginTAReq.Append("<!--Optional:--><rad:HistoricUserName>${empty}</rad:HistoricUserName>"); optinal 
          LoginTAReq.Append("<rad1:IATANumber>"+IATANumber+"</rad1:IATANumber>");
          LoginTAReq.Append("<rad1:UserName>"+UserName+"</rad1:UserName>");
          LoginTAReq.Append("<rad1:Password>"+Password+"</rad1:Password>");
          LoginTAReq.Append("</tem:LoginTravelAgentRequest></tem:LoginTravelAgent></soapenv:Body></soapenv:Envelope>");

          try
          {
              string resXml = FZUtility.PostXml(LoginTAReq.ToString(), methodUrl, serviceUrl,ref exep);
              XDocument xmlDoc = XDocument.Parse(resXml);


              XNamespace myNamespace = "http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.TravelAgents.Response";

              //var results = from result in xmlDoc.Descendants(myNamespace + "SecurityToken")
              //              select result;//.Element("SecurityToken").Value;
              string loginStatus = xmlDoc.Descendants(myNamespace + "LoggedIn").First().Value;
              if (!string.IsNullOrEmpty(loginStatus) && loginStatus.ToLower().Trim() == "true")
              {
                  status = true;
              }

              FZUtility.SaveXml(LoginTAReq.ToString(), SecurityToken, "LoginTA_Req");
              FZUtility.SaveXml(resXml, SecurityToken, "LoginTA_Res");
          }
          catch (Exception ex)
          {

          }

          return status;
      
      }

      public decimal TranFeeTA(string methodUrl, string serviceUrl, ref string exep)
      {

          decimal transFee = -1;
          StringBuilder TransTAReq = new StringBuilder();
          TransTAReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.TravelAgents.Request\">");
          TransTAReq.Append("<soapenv:Header/><soapenv:Body><tem:RetrieveAgencyCommission> <!--Optional:--><tem:RetrieveAgencyCommissionRequest>");
          TransTAReq.Append("<rad:SecurityGUID>"+SecurityToken+"</rad:SecurityGUID>");
          TransTAReq.Append(" <rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode><rad:AccessibleCarrierCode>FZ</rad:AccessibleCarrierCode></rad:CarrierCode></rad:CarrierCodes><!--Optional:-->");
          TransTAReq.Append("<rad:ClientIPAddress>"+IP+"</rad:ClientIPAddress>");
         // TransTAReq.Append("<!--Optional:--><rad:HistoricUserName>${empty}</rad:HistoricUserName>"); optinal 
          TransTAReq.Append("<rad1:CurrencyCode>INR</rad1:CurrencyCode>");
          TransTAReq.Append("</tem:RetrieveAgencyCommissionRequest></tem:RetrieveAgencyCommission></soapenv:Body></soapenv:Envelope>");


          try
          {
              string resXml = FZUtility.PostXml(TransTAReq.ToString(), methodUrl, serviceUrl,ref exep);
              XDocument xmlDoc = XDocument.Parse(resXml);


              XNamespace myNamespace = "http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.TravelAgents.Response";


              transFee = Convert.ToDecimal(xmlDoc.Descendants(myNamespace + "Amount").First().Value);

              FZUtility.SaveXml(TransTAReq.ToString(), SecurityToken, "TranFee_Req");
              FZUtility.SaveXml(resXml, SecurityToken, "TranFee_Res");
              
          }
          catch (Exception ex)
          {

          }

          return transFee;

      }





    }
}
