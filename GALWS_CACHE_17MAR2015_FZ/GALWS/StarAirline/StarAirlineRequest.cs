﻿using STD.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GALWS.StarAirline
{
    public class StarAirlineRequest
    {
        internal string AvailabilityRequest(string Url, string Userid, string Pwd, FlightSearch SearchInputs, bool RTF, string FareType)
        {
            string Request = "";
            try
            {
                //if(SearchInputs.TripType == TripType.R)
                //{
                //    string DepartureDate = SearchInputs.DepDate.Split('/')[2] + SearchInputs.DepDate.Split('/')[1] + SearchInputs.DepDate.Split('/')[0];
                //    string ReturnDate = SearchInputs.RetDate.Split('/')[2] + SearchInputs.RetDate.Split('/')[1] + SearchInputs.RetDate.Split('/')[0];
                //    Request = Url + "?rqid="+ Userid + "&airline_code="+ Pwd + "&app=information&action=get_schedule_v2&org=" + SearchInputs.HidTxtDepCity.Split(',')[0] + "&des="+ SearchInputs.HidTxtArrCity.Split(',')[0] + "&flight_date="+ DepartureDate + "&return_flight=1&ret_flight_date="+ ReturnDate + "";
                //}
                //else
                //{
                
                string DepartureDate = "";
                if(FareType == "OB")
                {
                    DepartureDate = SearchInputs.DepDate.Split('/')[2] + SearchInputs.DepDate.Split('/')[1] + SearchInputs.DepDate.Split('/')[0];
                    Request = Url + "?rqid=" + Userid + "&airline_code=" + Pwd + "&app=information&action=get_schedule_v2&org=" + SearchInputs.HidTxtDepCity.Split(',')[0] + "&des=" + SearchInputs.HidTxtArrCity.Split(',')[0] + "&flight_date=" + DepartureDate + "&return_flight=0";
                }
                else
                {
                    DepartureDate = SearchInputs.RetDate.Split('/')[2] + SearchInputs.RetDate.Split('/')[1] + SearchInputs.RetDate.Split('/')[0];
                    Request = Url + "?rqid=" + Userid + "&airline_code=" + Pwd + "&app=information&action=get_schedule_v2&org=" + SearchInputs.HidTxtArrCity.Split(',')[0] + "&des=" + SearchInputs.HidTxtDepCity.Split(',')[0] + "&flight_date=" + DepartureDate + "&return_flight=0";
                }
                
                // }

            }
            catch (Exception ex)
            {

            }
            return Request;
        }

        internal string PricingRequest(string Url, string Userid, string Pwd, string FareType, bool isspecialfare, FlightSearch SearchInputs, string FlightNo, string OnFlightDate, string ReturnFlightDate = "", string ReturnFlightNo="")
        {
            string PricingRequest = "";
            try
            {

                if (FareType == "OB")
                {
                    PricingRequest = Url + "?rqid=" + Userid + "&airline_code=" + Pwd + "&app=information&action=get_fare_v2_new&org=" + SearchInputs.HidTxtDepCity.Split(',')[0] + "&des=" + SearchInputs.HidTxtArrCity.Split(',')[0] + "&flight_no=" + FlightNo + "&flight_date=" + OnFlightDate + "";
                }
                else
                {
                    PricingRequest = Url + "?rqid=" + Userid + "&airline_code=" + Pwd + "&app=information&action=get_fare_v2_new&org=" + SearchInputs.HidTxtArrCity.Split(',')[0] + "&des=" + SearchInputs.HidTxtDepCity.Split(',')[0] + "&flight_no=" + FlightNo + "&flight_date=" + OnFlightDate + "";
                }
                //if (SearchInputs.TripType == TripType.RoundTrip)
                //{
                //    PricingRequest = Url + "?rqid=" + Userid + "&airline_code=" + Pwd + "&app=information&action=get_fare_v2_new&org=" + SearchInputs.HidTxtDepCity.Split(',')[0] + "&des=" + SearchInputs.HidTxtArrCity.Split(',')[0] + "&flight_no=" + FlightNo + "&flight_date=" + OnFlightDate + "&return_flight=1&ret_flight_date="+ ReturnFlightDate + "&ret_flight_no"+ ReturnFlightNo + "";
                //}
                //else
                //{
                    
                //}
                
            

            }
            catch (Exception ex)
            {

            }
            return PricingRequest;
        }

        internal string BookingRequest(DataTable FltDT, DataSet PaxDs, DataSet FltHdrs, string Url, string Userid, string Pwd)
        {
            StringBuilder Build = new StringBuilder();
            try
            {
                DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");


                string Sample = "http://ws.demo.awan.sqiva.com/?rqid=5EB9FE68-8915-11E0-BEA0-C9892766ECF2&airline_code=W2&app=transaction&action=booking_v2&des=UPG&" +
                    "org=CGK&round_trip=1&ret_flight_no=213,214&dep_flight_no=600&dep_date=20130910&subclass_dep=A/A&subclass_ret=E/E,E/Y&" +
                    "caller=JONIE&caller_1=00898989&num_pax_adult=1&num_pax_child=0&num_pax_infant=1&a_first_name_1=IVHAN&a_last_name_1=FG&a_salutation_1=MR&a_birthdate_1=19901010&a_mobile_1=0812345&a_passport_1=pass1&a_nationality_1=ID&a_passport_exp_1=20120303&i_first_name_1=LOUIS&i_birthdate_1=20120101&i_parent_1=1&i_salutation_1=MSTR&contact_1=08989899&ret_date=20130915";

                Build.Append("" + Url + "?rqid=" + Userid + "&airline_code=" + Pwd + "&app=transaction&action=booking_v2&");
                Build.Append("des=" + Convert.ToString(FltDT.Rows[0]["ArrAirportCode"]) + "&org=" + Convert.ToString(FltDT.Rows[0]["DepAirportCode"]) + "&");
                if (Convert.ToString(FltHdrs.Tables[0].Rows[0]["TripType"].ToString()) == "O")
                {
                    string FlightNo = "";
                    string RBDClass = "";
                    DataRow[] Flt = FltDT.Select("TripType='O'");
                    for (int F = 0; F < Flt.Count(); F++)
                    {
                        FlightNo += Convert.ToString(Flt[F]["FlightIdentification"]) + ",";
                        RBDClass += Convert.ToString(Flt[F]["sno"]) + ",";
                    }
                    Build.Append("dep_flight_no=" + FlightNo.TrimEnd(',') + "&dep_date=" + Flt[0]["depdatelcc"] + "&subclass_dep=" + RBDClass.TrimEnd(',') + "&");
                }
                if (Convert.ToString(FltHdrs.Tables[0].Rows[0]["TripType"].ToString()) == "R")
                {
                    string FlightNo = "";
                    string RBDClass = "";
                    DataRow[] Flt = FltDT.Select("TripType='R'");
                    for (int F = 0; F < Flt.Count(); F++)
                    {
                        FlightNo += Convert.ToString(Flt[F]["FlightIdentification"]) + ",";
                        RBDClass += Convert.ToString(Flt[F]["sno"]) + ",";
                    }

                    Build.Append("round_trip=1&ret_flight_no=" + FlightNo.TrimEnd(',') + "&ret_date=" + Flt[0]["depdatelcc"] + "&subclass_ret=" + RBDClass + "&");
                }
                Build.Append("caller=" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["AgentId"].ToString()) + "&caller_1=" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]).Trim() + "&");
                Build.Append("num_pax_adult=" + Convert.ToString(ADTPax.Count()) + "&");
                Build.Append("num_pax_child=" + Convert.ToString(CHDPax.Count()) + "&");
                Build.Append("num_pax_infant=" + Convert.ToString(INFPax.Count()) + "&");
                bool Infantover = true;
                if (ADTPax.Count() > 0)
                {
                    
                    for (int A = 0; A < ADTPax.Count(); A++)
                    {

                        Build.Append("a_first_name_" + (A + 1) + "=" + Convert.ToString(ADTPax[A]["FName"]) + "&a_last_name_" + (A + 1) + "=" + Convert.ToString(ADTPax[A]["LName"]) + "&a_salutation_" + (A + 1) + "=" + Convert.ToString(ADTPax[A]["Title"]).ToUpper() + "&");

                        if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[A]["DOB"])) && Convert.ToString(ADTPax[A]["DOB"]).Trim().Length > 7)
                        {
                            Build.Append("a_birthdate_" + (A + 1) + "=" + Convert.ToString(ADTPax[A]["DOB"]).Split('/')[2] + Convert.ToString(ADTPax[A]["DOB"]).Split('/')[1] + Convert.ToString(ADTPax[A]["DOB"]).Split('/')[0] + "&");

                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(ADTPax[A]["PassportNo"])) && !string.IsNullOrEmpty(Convert.ToString(ADTPax[A]["NationalityCode"])) && !string.IsNullOrEmpty(Convert.ToString(ADTPax[A]["PassportExpireDate"])))
                        {
                            Build.Append("a_passport_" + (A + 1) + "=" + Convert.ToString(ADTPax[A]["PassportNo"]) + "&a_passport_exp_" + (A + 1) + "=" + Convert.ToString(ADTPax[A]["PassportExpireDate"]).Split('/')[2] + Convert.ToString(ADTPax[A]["PassportExpireDate"]).Split('/')[1] + Convert.ToString(ADTPax[A]["PassportExpireDate"]).Split('/')[0] + "&a_nationality_" + (A + 1) + "=" + Convert.ToString(ADTPax[A]["NationalityCode"]) + "&");
                        }
                        if (INFPax.Count() > 0 && Infantover == true && INFPax.Count() > A)
                        {

                            Build.Append("i_first_name_" + (A + 1) + "=" + Convert.ToString(INFPax[A]["FName"]) + "&i_last_name_" + (A + 1) + "=" + Convert.ToString(INFPax[A]["LName"]) + "&i_salutation_" + (A + 1) + "=" + Convert.ToString(INFPax[A]["Title"]).ToUpper() + "&");

                            if (!string.IsNullOrEmpty(Convert.ToString(INFPax[A]["DOB"])) && Convert.ToString(INFPax[A]["DOB"]).Trim().Length > 7)
                            {
                                Build.Append("i_birthdate_" + (A + 1) + "=" + Convert.ToString(INFPax[A]["DOB"]).Split('/')[2] + Convert.ToString(INFPax[A]["DOB"]).Split('/')[1] + Convert.ToString(INFPax[A]["DOB"]).Split('/')[0] + "&");

                            }
                            if (!string.IsNullOrEmpty(Convert.ToString(INFPax[A]["PassportNo"])) && !string.IsNullOrEmpty(Convert.ToString(INFPax[A]["NationalityCode"])) && !string.IsNullOrEmpty(Convert.ToString(INFPax[A]["PassportExpireDate"])))
                            {
                                Build.Append("i_passport_" + (A + 1) + "=" + Convert.ToString(INFPax[A]["PassportNo"]) + "&i_passport_exp_" + (A + 1) + "=" + Convert.ToString(INFPax[A]["PassportExpireDate"]).Split('/')[2] + Convert.ToString(INFPax[A]["PassportExpireDate"]).Split('/')[1] + Convert.ToString(INFPax[A]["PassportExpireDate"]).Split('/')[0] + "&a_nationality_" + (A + 1) + "=" + Convert.ToString(INFPax[A]["NationalityCode"]) + "&");
                            }
                            Build.Append("i_parent_" + (A + 1) + "=" + (A + 1) + "&");

                            if (INFPax.Count() == A + 1)
                                Infantover = false;
                        }

                    }
                    if (!string.IsNullOrEmpty(Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"])))
                    {
                        
                        Build.Append("a_mobile_1=" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]) + "&");
                        Build.Append("contact_1=" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]) + "&");
                    }
                    
                    

                }
                if (CHDPax.Count() > 0)
                {

                    for (int I = 0; I < CHDPax.Count(); I++)
                    {

                        Build.Append("c_first_name_" + (I + 1) + "=" + Convert.ToString(CHDPax[I]["FName"]) + "&c_last_name_" + (I + 1) + "=" + Convert.ToString(CHDPax[I]["LName"]) + "&c_salutation_" + (I + 1) + "=" + Convert.ToString(CHDPax[I]["Title"]).ToUpper() + "&");

                        if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[I]["DOB"])) && Convert.ToString(CHDPax[I]["DOB"]).Trim().Length > 7)
                        {
                            Build.Append("c_birthdate_" + (I + 1) + "=" + Convert.ToString(CHDPax[I]["DOB"]).Split('/')[2] + Convert.ToString(CHDPax[I]["DOB"]).Split('/')[1] + Convert.ToString(CHDPax[I]["DOB"]).Split('/')[0] + "&");

                        }
                        if (!string.IsNullOrEmpty(Convert.ToString(CHDPax[I]["PassportNo"])) && !string.IsNullOrEmpty(Convert.ToString(CHDPax[I]["NationalityCode"])) && !string.IsNullOrEmpty(Convert.ToString(CHDPax[I]["PassportExpireDate"])))
                        {
                            Build.Append("c_passport_" + (I + 1) + "=" + Convert.ToString(CHDPax[I]["PassportNo"]) + "&c_passport_exp_" + (I + 1) + "=" + Convert.ToString(CHDPax[I]["PassportExpireDate"]).Split('/')[2] + Convert.ToString(CHDPax[I]["PassportExpireDate"]).Split('/')[1] + Convert.ToString(CHDPax[I]["PassportExpireDate"]).Split('/')[0] + "&a_nationality_" + (I + 1) + "=" + Convert.ToString(CHDPax[I]["NationalityCode"]) + "&");
                        }


                    }


                }

            }
            catch (Exception ex)
            {

            }
            return Build.ToString().TrimEnd('&');
        }

        internal string AfterBookPaymentRequest(string Url, string Userid, string Pwd , string BookingCode)
        {
            StringBuilder Build = new StringBuilder();
            try
            {
                Build.Append("" + Url + "?rqid=" + Userid + "&airline_code=" + Pwd + "&app=transaction&action=payment&book_code="+ BookingCode + "");
            }
            catch(Exception ex)
            {

            }
            return Build.ToString();
        }
    }
}
