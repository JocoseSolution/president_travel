﻿using STD.BAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;

namespace GALWS.StarAirline
{
    public class StarAirlineService
    {
        internal string ResponseGetter(string Request , HttpContext ctx=null)
        {
            string Response = "";
            try
            {
                HttpWebRequest HttpRequest = WebRequest.Create(Request) as HttpWebRequest;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11 | SecurityProtocolType.Tls | SecurityProtocolType.Tls12;
                HttpRequest.Method = "GET";
                //HttpRequest.Host = "ws.demo.awan.sqiva.com";
                HttpRequest.KeepAlive = true;
                //HttpRequest.Headers.Add("Upgrade-Insecure-Requests:1");
                HttpRequest.UserAgent = "WS-AWAN";//"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/84.0.4147.125 Safari/537.36";
                HttpRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
                HttpRequest.Headers.Add("Accept-Encoding: gzip, deflate");
                HttpRequest.Headers.Add("Accept-Language: en-US,en;q=0.9");
                HttpWebResponse webResponse = (HttpWebResponse)HttpRequest.GetResponse();
                if(webResponse.StatusCode == HttpStatusCode.OK)
                {
                    var rsp = webResponse.GetResponseStream();

                    if ((webResponse.ContentEncoding.ToLower().Contains("gzip")))
                    {
                        using (StreamReader readStream = new StreamReader(new GZipStream(rsp, CompressionMode.Decompress)))
                        {
                            Response = readStream.ReadToEnd();
                        }
                    }
                    else
                    {
                        StreamReader reader = new StreamReader(rsp, Encoding.Default);
                        Response = reader.ReadToEnd();
                    }
                }
                
            }

            catch(WebException ex)
            {
                Response = "ERROR:::";
            }
            catch(Exception ex)
            {
                Response = "ERROR:::";
            }
            finally
            {
                Utility.SaveFile(Request + "Response" + Response , "SearchReq_" , ctx != null ? ctx.Session["UID"].ToString() : "LiveLogs" , "OG");
            }
            return Response;
        }
    }
}
