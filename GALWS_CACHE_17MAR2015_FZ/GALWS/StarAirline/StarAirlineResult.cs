﻿using STD.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;
using System.Collections;
using STD.BAL;
using System.Security.Cryptography;

namespace GALWS.StarAirline
{   
    public class StarAirlineResult
    {
        string Url = "", UID = "", Pd = "", OCode = "", con = "";
        public StarAirlineResult(string BaseUrl , string UserID , string Pwd , string OrgCode , string Connection)
        {
            Url = BaseUrl; UID = UserID; Pd = Pwd; OCode = OrgCode; con= Connection;
        }
        public StarAirlineResult()
        {
                
        }
        public List<FlightSearchResults> GetFlightResult(string Response, FlightSearch SearchInputs, bool RTF, string faretype, bool isspecialfare, List<FltSrvChargeList> srvChargeList, DataSet markupDs,  List<FlightCityList> cityList,  HttpContext ctx, List<MISCCharges> miscList)
        {
            List<FlightSearchResults> FinalLists = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL(con);
            try
            {
                //Response = File.ReadAllText(@"C:\StarAirlines\OneWayAvailabilityResponse.txt");
                List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
                List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
                

                if (!string.IsNullOrEmpty(Response))
                {
                    JObject objResult = JObject.Parse(Response);

                    if (objResult != null && Convert.ToString(objResult["err_code"]) == "0")
                    {
                        //dynamic ReturnSchedule = null;
                        var schedule = objResult["schedule"].ToList();
                        var ReturnSchedule = SearchInputs.TripType == TripType.RoundTrip ? objResult["ret_schedule"].ToList() : null;
                        int Flight = 1;
                        var DirectFlight = schedule.Count > 0 ? schedule[0] : null;
                        var ConnecteFlight = schedule.Count() >= 2 ? schedule[1] : null;
                        int DLineNo = 1;
                        if (DirectFlight != null)
                        {
                            DirectFlight.ToList().ForEach(EachFlight =>
                            {
                                int Leg = 0;

                                if (((JArray)EachFlight).Count >= 0)
                                {

                                    StarAirlineRequest ObjRequest = new StarAirlineRequest();
                                    StarAirlineService ObjServ = new StarAirlineService();

                                    string PricingResponse = ObjServ.ResponseGetter(ObjRequest.PricingRequest(Url, UID, Pd,  faretype, isspecialfare, SearchInputs, Convert.ToString(EachFlight[0]).Split('-')[1], (string)EachFlight[3]), ctx);
                                    //PricingResponse = File.ReadAllText(@"C:\StarAirlines\OneWayPricingResponse.txt");
                                    JObject PriceResult = JObject.Parse(PricingResponse);

                                    if (!string.IsNullOrEmpty(PricingResponse) && Convert.ToString(PriceResult["err_code"]) == "0")
                                    {
                                        FlightSearchResults fsr = new FlightSearchResults();

                                        int TotalPax = SearchInputs.Adult + SearchInputs.Child;
                                        List<Tuple<string, int, int>> FareClassSeat = new List<Tuple<string, int, int>>();
                                        EachFlight[10].Reverse().ToList().Where(Y => Convert.ToInt32(Y[1]) > 0).ToList().ForEach(EachFareClass =>
                                        {
                                            bool ExpetationMeet = false;
                                            Tuple<string, int, int> Matched = FareClassAvailability(TotalPax, EachFareClass, 0, out ExpetationMeet);
                                            if (ExpetationMeet == true) { FareClassSeat.Add(Matched); }

                                        });
                                        var FareInfo = PriceResult["fare_info"];//Leg + 1 > 1 ? PriceResult["fare_info_" + (Leg + 1) + ""] : PriceResult["fare_info"];
                                        var PriceFareClassMatched = FareInfo.Where(x => FareClassSeat.Any(y => x[0].ToString().Split('/')[0] == y.Item1) && (x[0].ToString() == FareClassSeat.FirstOrDefault().Item1 + "/" + FareClassSeat.FirstOrDefault().Item1 + "SC" || x[0].ToString() == FareClassSeat.FirstOrDefault().Item1 + "/" + FareClassSeat.FirstOrDefault().Item1 + "SF" || x[0].ToString() == FareClassSeat.FirstOrDefault().Item1 + "/" + FareClassSeat.FirstOrDefault().Item1 + "SR" || x[0].ToString() == FareClassSeat.FirstOrDefault().Item1 + "/" + FareClassSeat.FirstOrDefault().Item1 + "TBF" || (x[0].ToString() == FareClassSeat.FirstOrDefault().Item1 + "/" + FareClassSeat.FirstOrDefault().Item1 + "FNF" && TotalPax >= 2))).ToList();
                                        bool Error = false;

                                        PriceFareClassMatched.ForEach(EachFareType =>
                                        {
                                            fsr = SetFlightResult(EachFareType, Leg, DLineNo, Flight, faretype, EachFlight, Response, PriceResult, SearchInputs, srvChargeList, markupDs, cityList, miscList, FareClassSeat, isspecialfare, out Error);

                                            if (Error == false)
                                            {
                                                if (Flight == 1 || SearchInputs.Trip == Trip.I)
                                                {

                                                    fsrList.Add(fsr);
                                                }
                                                else if (Flight == 2 && SearchInputs.Trip == Trip.D)
                                                {

                                                    RfsrList.Add(fsr);
                                                }
                                            }
                                            DLineNo++;
                                        });

                                        Leg++;
                                    }
                                }
                            });
                        }


                        if (ConnecteFlight != null)
                        {
                            int CLineNo = DLineNo;
                            ConnecteFlight.ToList().ForEach(EachConnectFlight =>
                            {
                                string SFlighNo = "";
                                int TotalPax = SearchInputs.Adult + SearchInputs.Child;
                                EachConnectFlight.ToList().ForEach(FlightNo => { SFlighNo += Convert.ToString(FlightNo[0]).Split('-')[1] + ","; });
                                StarAirlineRequest ObjRequest = new StarAirlineRequest();
                                StarAirlineService ObjServ = new StarAirlineService();
                                string PricingResponse = ObjServ.ResponseGetter(ObjRequest.PricingRequest(Url, UID, Pd, faretype, isspecialfare, SearchInputs, SFlighNo.TrimEnd(','), (string)EachConnectFlight[0][3]), ctx);
                                //PricingResponse = File.ReadAllText(@"H:\StarAirlines\ConnectPricingResponse.txt");//File.ReadAllText(@"H:\StarAirlines\OneWayPricingResponse.txt");
                                JObject PriceResult = JObject.Parse(PricingResponse);
                                int FareLeg = 0;
                                List<List<Tuple<string, int, int>>> AllSegFareInfo = new List<List<Tuple<string, int, int>>>();
                                List<JToken> FareInfoList = new List<JToken>();
                                EachConnectFlight.ToList().ForEach(EachLegFareClass =>
                                {
                                    
                                    List<Tuple<string, int, int>> FareClassSeat = new List<Tuple<string, int, int>>();
                                    EachLegFareClass[10].Reverse().ToList().Where(Y => Convert.ToInt32(Y[1]) > 0).ToList().ForEach(EachFareClass =>
                                    {
                                        bool ExpetationMeet = false;
                                        Tuple<string, int, int> Matched = FareClassAvailability(TotalPax, EachFareClass, FareLeg, out ExpetationMeet);
                                        if (ExpetationMeet == true) { FareClassSeat.Add(Matched); }

                                    });
                                    var FareInfo = FareLeg == 0 ? PriceResult["fare_info"] : PriceResult["fare_info_"+ (FareLeg + 1) + ""];
                                    FareInfoList.Add(FareInfo);
                                    AllSegFareInfo.Add(FareClassSeat);
                                    FareLeg++;
                                });

                                //EachConnectFlight.ToList().ForEach(EachLegFlight => 
                                //{

                                //if (((JArray)EachLegFlight).Count >= 0)
                                //{

                                if (!string.IsNullOrEmpty(PricingResponse) && Convert.ToString(PriceResult["err_code"]) == "0")
                                {

                                    var MatchedConnectedRBD = AllSegFareInfo.Count() == 2 ? AllSegFareInfo[0].Where(ML => AllSegFareInfo[1].Any(CL => CL.Item1 == ML.Item1)).ToList() : null;
                                    //int TotalPax = SearchInputs.Adult + SearchInputs.Child;
                                    //List<Tuple<string, int>> FareClassSeat = new List<Tuple<string, int>>();
                                    //EachLegFlight[10].Reverse().ToList().Where(Y => Convert.ToInt32(Y[1]) > 0).ToList().ForEach(EachFareClass =>
                                    //{
                                    //    //bool ExpetationMeet = false;
                                    //    //Tuple<string, int> Matched = FareClassAvailability(TotalPax, EachFareClass, out ExpetationMeet);
                                    //    //if (ExpetationMeet == true) { FareClassSeat.Add(Matched); }

                                    //});
                                    

                                    var FareInfo = PriceResult["fare_info"];//Leg + 1 > 1 ? PriceResult["fare_info_" + (Leg + 1) + ""] : PriceResult["fare_info"];
                                    var PriceFareClassMatched = FareInfo.Where(x => MatchedConnectedRBD != null && MatchedConnectedRBD.Any(y => x[0].ToString().Split('/')[0] == y.Item1) && (x[0].ToString() == MatchedConnectedRBD[0].Item1 + "/" + MatchedConnectedRBD[0].Item1 + "SC" || x[0].ToString() == MatchedConnectedRBD[0].Item1 + "/" + MatchedConnectedRBD[0].Item1 + "SF" || x[0].ToString() == MatchedConnectedRBD[0].Item1 + "/" + MatchedConnectedRBD[0].Item1 + "SR" || (x[0].ToString() == MatchedConnectedRBD[0].Item1 + "/" + MatchedConnectedRBD[0].Item1 + "FNF" && TotalPax >= 2))).ToList();
                                    bool Error = false;

                                    PriceFareClassMatched.ForEach(EachFareType =>
                                    {
                                        int Leg = 0;
                                        EachConnectFlight.ToList().ForEach(LegFlight =>
                                        {
                                            FlightSearchResults fsr = new FlightSearchResults();
                                            fsr = SetFlightResult(EachFareType, EachConnectFlight.ToList().Count - 1, CLineNo, Flight, faretype, LegFlight, Response, PriceResult, SearchInputs, srvChargeList, markupDs, cityList, miscList, AllSegFareInfo.FirstOrDefault(), isspecialfare, out Error);

                                            if (Error == false)
                                            {
                                                if (Flight == 1 || SearchInputs.Trip == Trip.I)
                                                {

                                                    fsrList.Add(fsr);
                                                }
                                                else if (Flight == 2 && SearchInputs.Trip == Trip.D)
                                                {

                                                    RfsrList.Add(fsr);
                                                }
                                            }
                                            Leg++;
                                        });
                                        CLineNo++;
                                    });

                                    //Leg++;
                                }
                                //}


                                // });


                            });
                        }


                    }
                }
                if (isspecialfare && SearchInputs.Trip == Trip.D)
                {

                   // FinalList = RoundTripFare(fsrList, RfsrList, srvCharge);
                }
                else
                {
                    FinalLists = fsrList;
                }

                
            }
            catch(Exception ex) 
            {

            }
            return objFltComm.AddFlightKey(FinalLists, isspecialfare);
        }

        private FlightSearchResults SetFlightResult(JToken FareInfo , int Leg, int LineNo, int Flight,string faretype,JToken EachFlight, string Availabilityresponse, JObject pricingResponse, FlightSearch SearchInputs, List<FltSrvChargeList> srvChargeList, DataSet markupDs, List<FlightCityList> cityList, List<MISCCharges> miscList , List<Tuple<string, int,int>> FareClassSeat,bool isspecialfare, out bool Error)
        {
            Error = false;
            FlightSearchResults FlightResult = new FlightSearchResults();
            try
            {
                                
                FlightResult.Leg = Leg;
                FlightResult.LineNumber = LineNo;
                FlightResult.Flight = Flight.ToString();
                FlightResult.Stops = Leg + "-Stop";//(EachFlight.ToList().Count - 1).ToString() + "-Stop";
                FlightResult.Adult = SearchInputs.Adult;
                FlightResult.Child = SearchInputs.Child;
                FlightResult.Infant = SearchInputs.Infant;
                FlightResult.depdatelcc = (string)EachFlight[3];
                FlightResult.arrdatelcc = (string)EachFlight[4];
                FlightResult.Departure_Date = Convert.ToDateTime((string)EachFlight[3].ToString().Substring(0, 4) + "-" + (string)EachFlight[3].ToString().Substring(4, 2) + "-" + (string)EachFlight[3].ToString().Substring(6, 2)).ToString("dd MMM");
                FlightResult.Arrival_Date = Convert.ToDateTime((string)EachFlight[4].ToString().Substring(0, 4) + "-" + (string)EachFlight[4].ToString().Substring(4, 2) + "-" + (string)EachFlight[4].ToString().Substring(6, 2)).ToString("dd MMM");
                FlightResult.DepartureDate = Convert.ToDateTime((string)EachFlight[3].ToString().Substring(0, 4) + "-" + (string)EachFlight[3].ToString().Substring(4, 2) + "-" + (string)EachFlight[3].ToString().Substring(6, 2)).ToString("ddMMyy");
                FlightResult.ArrivalDate = Convert.ToDateTime((string)EachFlight[4].ToString().Substring(0, 4) + "-" + (string)EachFlight[4].ToString().Substring(4, 2) + "-" + (string)EachFlight[4].ToString().Substring(6, 2)).ToString("ddMMyy");
                FlightResult.FlightIdentification = Convert.ToString(EachFlight[0]).Split('-')[1];
                FlightResult.AirLineName = "Star Air";//Convert.ToString(EachFlight[0]).Split('-')[0];
                FlightResult.ValiDatingCarrier = Convert.ToString(EachFlight[0]).Split('-')[0];
                FlightResult.OperatingCarrier = Convert.ToString(EachFlight[0]).Split('-')[0];
                FlightResult.MarketingCarrier = Convert.ToString(EachFlight[0]).Split('-')[0];
                FlightResult.DepartureLocation = Convert.ToString(EachFlight[1]);
                FlightResult.DepartureCityName = GetAirPortAndLocationName(cityList, 2, (string)EachFlight[1]);
                FlightResult.DepartureTime = (string)EachFlight[5];
                FlightResult.DepartureAirportName = GetAirPortAndLocationName(cityList, 1, (string)EachFlight[1]);
                FlightResult.DepartureTerminal = "";
                FlightResult.DepAirportCode = Convert.ToString(EachFlight[1]);
                FlightResult.ArrivalLocation = (string)EachFlight[2];
                FlightResult.ArrivalCityName = GetAirPortAndLocationName(cityList, 2, (string)EachFlight[2]);
                FlightResult.ArrivalTime = (string)EachFlight[6];
                FlightResult.ArrivalAirportName = GetAirPortAndLocationName(cityList, 1, (string)EachFlight[2]);
                FlightResult.ArrivalTerminal = "";
                FlightResult.ArrAirportCode = Convert.ToString(EachFlight[2]);
                FlightResult.Trip = SearchInputs.Trip.ToString();
                FlightResult.TotDur = Convert.ToString(EachFlight[7]).ToLower().Replace("h", ":").Replace("m", "");
                FlightResult.TripCnt = "";
                

                DataTable CommDt = new DataTable();
                Hashtable STTFTDS = new Hashtable();
                FlightCommonBAL SearchInputsComm = new FlightCommonBAL(con);
                if(FareInfo != null)
                {

                    if (SearchInputs.RTF == true || SearchInputs.GDSRTF == true || isspecialfare == true)
                    {
                        FlightResult.Sector = SearchInputs.HidTxtDepCity.Split(',')[0] + ":" + SearchInputs.HidTxtArrCity.Split(',')[0] + ":" + SearchInputs.HidTxtDepCity.Split(',')[0];
                    }
                    else
                    {
                        if (faretype == "IB")
                        {
                            FlightResult.Sector = SearchInputs.HidTxtArrCity.Split(',')[0]  + ":" + SearchInputs.HidTxtDepCity.Split(',')[0];
                        }
                        else
                        {
                            FlightResult.Sector = SearchInputs.HidTxtDepCity.Split(',')[0]  + ":" + SearchInputs.HidTxtArrCity.Split(',')[0];
                        }

                    }
                
                    if (Flight == 1)
                    {
                        if (faretype == "IB")
                        {
                            FlightResult.OrgDestFrom = SearchInputs.HidTxtArrCity.Split(',')[0]; 
                            FlightResult.OrgDestTo = SearchInputs.HidTxtDepCity.Split(',')[0];
                        }
                        else
                        {
                            FlightResult.OrgDestFrom = SearchInputs.HidTxtDepCity.Split(',')[0]; 
                            FlightResult.OrgDestTo = SearchInputs.HidTxtArrCity.Split(',')[0];
                        }
                        FlightResult.TripType = TripType.O.ToString();
                    }
                    else if (Flight == 2)
                    {
                        FlightResult.OrgDestFrom = SearchInputs.HidTxtArrCity.Split(',')[0];
                        FlightResult.OrgDestTo = SearchInputs.HidTxtDepCity.Split(',')[0];
                        FlightResult.TripType = TripType.R.ToString();
                    }
                    JArray PriceArray = (JArray)(FareInfo);
                    var AdultFare = PriceArray[1];
                    var ChildFare = PriceArray[2];
                    var InfantFare = PriceArray[3];
                    #region Adult
                    if (SearchInputs.Adult > 0 && AdultFare != null)
                    {
                        FlightResult.AdtAvlStatus = Convert.ToString(FareClassSeat.FirstOrDefault().Item2).Trim();
                        FlightResult.fareBasis = Convert.ToString((((JValue)PriceArray.First).Value)).Split('/')[1];
                        FlightResult.AdtBfare = (float)Math.Ceiling(float.Parse((string)(AdultFare[1]).ToString())) ;
                        FlightResult.AdtCabin = "Y";
                        FlightResult.AdtFSur = (float)Math.Ceiling(float.Parse((AdultFare[4]).ToString())) ;
                        float TotalTax = (float)Math.Ceiling(float.Parse((AdultFare[4]).ToString())) + (float)Math.Ceiling(float.Parse((AdultFare[2]).ToString())) + (float)Math.Ceiling(float.Parse((AdultFare[3]).ToString())) + (float)Math.Ceiling(float.Parse((AdultFare[5]).ToString())) + (float)Math.Ceiling(float.Parse((AdultFare[6]).ToString())) + +(float)Math.Ceiling(float.Parse((AdultFare[7]).ToString()));
                        FlightResult.AdtTax = TotalTax ;
                        FlightResult.AdtOT = (float)Math.Ceiling(float.Parse((AdultFare[2]).ToString())) + (float)Math.Ceiling(float.Parse((AdultFare[3]).ToString())) + (float)Math.Ceiling(float.Parse((AdultFare[5]).ToString())) + (float)Math.Ceiling(float.Parse((AdultFare[6]).ToString())) + +(float)Math.Ceiling(float.Parse((AdultFare[7]).ToString())) ;// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                        FlightResult.AdtRbd = Convert.ToString(((JValue)PriceArray.First).Value).Split('/')[0];
                        FlightResult.AdtFarebasis = FlightResult.fareBasis;
                        FlightResult.AdtFareType = CalCulateDepartureTime((string)EachFlight[3] , (string)EachFlight[5]) <=120 ? "Non-Refundable" : "Refundable";// fareInfo.FareTypeID.Trim();
                        //fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                        FlightResult.AdtFare = (float)Math.Ceiling(float.Parse((FlightResult.AdtTax + FlightResult.AdtBfare).ToString())); //  + srvCharge
                        FlightResult.sno = Convert.ToString((((JValue)PriceArray.First).Value));
                        FlightResult.FBPaxType = "ADT";
                        FlightResult.AdtFar = Convert.ToString((((JValue)PriceArray.First).Value)).Contains("SR") ? "SR" : Convert.ToString((((JValue)PriceArray.First).Value)).Contains("SC") ? "SC" : Convert.ToString((((JValue)PriceArray.First).Value)).Contains("SF") ? "SF" : Convert.ToString((((JValue)PriceArray.First).Value)).Contains("FNF") ? "FNF" : "";
                        FlightResult.BagInfo = PriceArray[18].ToString() + " Kg + 5 Kg (Baggage Allowance + Hand Carry)";
                        FlightResult.ADTAdminMrk = CalcMarkup(markupDs.Tables["AdminMarkUp"], FlightResult.ValiDatingCarrier, FlightResult.AdtFare, SearchInputs.Trip.ToString());
                        FlightResult.ADTAgentMrk = CalcMarkup(markupDs.Tables["AgentMarkUp"], FlightResult.ValiDatingCarrier, FlightResult.AdtFare, SearchInputs.Trip.ToString());

                        CommDt.Clear();
                        CommDt = SearchInputsComm.GetFltComm_Gal(SearchInputs.AgentType, FlightResult.ValiDatingCarrier, decimal.Parse(FlightResult.AdtBfare.ToString()), decimal.Parse(FlightResult.AdtFSur.ToString()), 1, FlightResult.AdtRbd, FlightResult.AdtCabin, SearchInputs.DepDate, FlightResult.OrgDestFrom + "-" + FlightResult.OrgDestTo, SearchInputs.RetDate, FlightResult.fareBasis, SearchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), SearchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), FlightResult.FlightIdentification, FlightResult.OperatingCarrier, FlightResult.MarketingCarrier, "NRM", "");
                        FlightResult.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                        FlightResult.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                        STTFTDS.Clear();
                        STTFTDS = CalcSrvTaxTFeeTds(srvChargeList, FlightResult.ValiDatingCarrier, FlightResult.AdtDiscount1, FlightResult.AdtBfare, FlightResult.AdtFSur, SearchInputs.TDS);
                        FlightResult.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                        FlightResult.AdtDiscount = FlightResult.AdtDiscount1 - FlightResult.AdtSrvTax1;
                        FlightResult.AdtEduCess = 0;
                        FlightResult.AdtHighEduCess = 0;
                        FlightResult.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                        FlightResult.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                        if (SearchInputs.IsCorp == true)
                        {
                            try
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = SearchInputsComm.clac_MgtFee(SearchInputs.AgentType, FlightResult.ValiDatingCarrier, decimal.Parse(FlightResult.AdtBfare.ToString()), decimal.Parse(FlightResult.AdtFSur.ToString()), SearchInputs.Trip.ToString(), decimal.Parse(FlightResult.AdtFare.ToString()));
                                FlightResult.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                FlightResult.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            catch { }
                        }
                    }
                    #endregion

                    #region Child
                    if (ChildFare != null && SearchInputs.Child > 0)
                    {
                        FlightResult.ChdBFare = (float)Math.Ceiling(float.Parse((string)(ChildFare[1]).ToString())) ;
                        FlightResult.ChdCabin = "Y";
                        FlightResult.ChdFSur = (float)Math.Ceiling(float.Parse((ChildFare[4]).ToString())) ;
                        float TotalTaxChd = (float)Math.Ceiling(float.Parse((ChildFare[4]).ToString())) + (float)Math.Ceiling(float.Parse((ChildFare[2]).ToString())) + (float)Math.Ceiling(float.Parse((ChildFare[3]).ToString())) + (float)Math.Ceiling(float.Parse((ChildFare[5]).ToString())) + (float)Math.Ceiling(float.Parse((ChildFare[6]).ToString()))  +(float)Math.Ceiling(float.Parse((ChildFare[7]).ToString()));
                        FlightResult.ChdTax = TotalTaxChd  ;// + srvCharge);
                        FlightResult.ChdOT = (float)Math.Ceiling(float.Parse((ChildFare[2]).ToString())) + (float)Math.Ceiling(float.Parse((ChildFare[3]).ToString())) + (float)Math.Ceiling(float.Parse((ChildFare[5]).ToString())) + (float)Math.Ceiling(float.Parse((ChildFare[6]).ToString())) + +(float)Math.Ceiling(float.Parse((ChildFare[7]).ToString()));// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);// + paxFare.AdditionalTxnFeePub + srvCharge);
                        FlightResult.ChdRbd = Convert.ToString(((JValue)PriceArray.First).Value).Split('/')[0];
                        FlightResult.ChdFarebasis = FlightResult.fareBasis;//; fareInfo.FBCode.Trim();
                        FlightResult.ChdFare = (float)Math.Ceiling(float.Parse((FlightResult.ChdTax + FlightResult.ChdBFare).ToString())); //  + srvCharge

                        FlightResult.CHDAdminMrk = CalcMarkup(markupDs.Tables["AdminMarkUp"], FlightResult.ValiDatingCarrier, FlightResult.ChdFare, FlightResult.Trip.ToString());
                        FlightResult.CHDAgentMrk = CalcMarkup(markupDs.Tables["AgentMarkUp"], FlightResult.ValiDatingCarrier, FlightResult.ChdFare, SearchInputs.Trip.ToString());
                        CommDt.Clear();
                        CommDt = SearchInputsComm.GetFltComm_Gal(SearchInputs.AgentType, FlightResult.ValiDatingCarrier, decimal.Parse(FlightResult.ChdBFare.ToString()), decimal.Parse(FlightResult.ChdFSur.ToString()), 1, FlightResult.ChdRbd, FlightResult.ChdCabin, SearchInputs.DepDate, FlightResult.OrgDestFrom + "-" + FlightResult.OrgDestTo, SearchInputs.RetDate, FlightResult.ChdFarebasis, SearchInputs.HidTxtDepCity.Split(',')[1].ToString().Trim(), SearchInputs.HidTxtArrCity.Split(',')[0].ToString().Trim(), FlightResult.FlightIdentification, FlightResult.OperatingCarrier, FlightResult.MarketingCarrier, "NRM", "");
                        FlightResult.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                        FlightResult.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                        STTFTDS.Clear();
                        STTFTDS = CalcSrvTaxTFeeTds(srvChargeList, FlightResult.ValiDatingCarrier, FlightResult.ChdDiscount1, FlightResult.ChdBFare, FlightResult.ChdFSur, SearchInputs.TDS);
                        FlightResult.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                        FlightResult.ChdDiscount = FlightResult.ChdDiscount1 - FlightResult.ChdSrvTax1;
                        FlightResult.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                        FlightResult.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                        FlightResult.ChdEduCess = 0;
                        FlightResult.ChdHighEduCess = 0;
                        if (SearchInputs.IsCorp == true)
                        {
                            try
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = SearchInputsComm.clac_MgtFee(SearchInputs.AgentType, FlightResult.ValiDatingCarrier, decimal.Parse(FlightResult.ChdBFare.ToString()), decimal.Parse(FlightResult.ChdFSur.ToString()), SearchInputs.Trip.ToString(), decimal.Parse(FlightResult.ChdFare.ToString()));
                                FlightResult.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                FlightResult.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            catch { }
                        }
                    }
                    #endregion

                    #region Infant
                    if (InfantFare != null && SearchInputs.Infant > 0)
                    {
                        FlightResult.InfBfare = (float)Math.Ceiling(float.Parse((string)(InfantFare[1]).ToString())) ;
                        FlightResult.InfCabin = "Y";
                        FlightResult.InfFSur = (float)Math.Ceiling(float.Parse((InfantFare[4]).ToString())) ;
                        float TotalTaxInf = (float)Math.Ceiling(float.Parse((InfantFare[4]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[2]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[3]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[5]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[6]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[7]).ToString()));
                        FlightResult.InfTax = TotalTaxInf * SearchInputs.Infant; ;// + srvCharge);
                        FlightResult.InfOT = (float)Math.Ceiling(float.Parse((InfantFare[2]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[3]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[5]).ToString())) + (float)Math.Ceiling(float.Parse((InfantFare[6]).ToString())) + +(float)Math.Ceiling(float.Parse((InfantFare[7]).ToString())) ;
                        
                        
                        
                        FlightResult.InfRbd = Convert.ToString(((JValue)PriceArray.First).Value).Split('/')[0];
                        FlightResult.InfFarebasis = FlightResult.fareBasis;
                        FlightResult.InfFare = (float)Math.Ceiling(float.Parse((FlightResult.InfTax + FlightResult.InfBfare).ToString()));


                        if (SearchInputs.IsCorp == true)
                        {
                            try
                            {
                                DataTable MGDT = new DataTable();
                                MGDT = SearchInputsComm.clac_MgtFee(SearchInputs.AgentType, FlightResult.ValiDatingCarrier, decimal.Parse(FlightResult.InfBfare.ToString()), decimal.Parse(FlightResult.InfFSur.ToString()), SearchInputs.Trip.ToString(), decimal.Parse(FlightResult.InfFare.ToString()));
                                FlightResult.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                FlightResult.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                            }
                            catch { }
                        }
                    }

                    #endregion

                    FlightResult.Searchvalue = "";
                    FlightResult.OriginalTF = FlightResult.AdtFare + FlightResult.ChdFare + FlightResult.InfFare;
                    FlightResult.OriginalTT = 0;//srvCharge;
                    FlightResult.Provider = "OG";
                    FlightResult.RBD = FlightResult.AdtRbd + ":" + FlightResult.ChdRbd + ":" + FlightResult.InfRbd;



                    FlightResult.STax = (FlightResult.AdtSrvTax * FlightResult.Adult) + (FlightResult.ChdSrvTax * FlightResult.Child) + (FlightResult.InfSrvTax * FlightResult.Infant);
                    FlightResult.TFee = (FlightResult.AdtTF * FlightResult.Adult) + (FlightResult.ChdTF * FlightResult.Child);// +(objlist.InfTF * objlist.Infant);
                    FlightResult.TotDis = (FlightResult.AdtDiscount * FlightResult.Adult) + (FlightResult.ChdDiscount * FlightResult.Child);
                    FlightResult.TotCB = (FlightResult.AdtCB * FlightResult.Adult) + (FlightResult.ChdCB * FlightResult.Child);
                    FlightResult.TotTds = (FlightResult.AdtTds * FlightResult.Adult) + (FlightResult.ChdTds * FlightResult.Child);// +objFS.InfTds;
                    FlightResult.TotMgtFee = (FlightResult.AdtMgtFee * FlightResult.Adult) + (FlightResult.ChdMgtFee * FlightResult.Child) + (FlightResult.InfMgtFee * FlightResult.Infant);



                    FlightResult.AvailableSeats = FlightResult.AdtAvlStatus + ":" + FlightResult.ChdAvlStatus + ":" + FlightResult.InfAvlStatus;
                    FlightResult.TotalFare = (FlightResult.Adult * FlightResult.AdtFare) + (FlightResult.Child * FlightResult.ChdFare) + (FlightResult.Infant * FlightResult.InfFare);
                    FlightResult.TotalFuelSur = (FlightResult.Adult * FlightResult.AdtFSur) + (FlightResult.Child * FlightResult.ChdFSur) + (FlightResult.Infant * FlightResult.InfFSur);
                    FlightResult.TotalTax = (FlightResult.Adult * FlightResult.AdtTax) + (FlightResult.Child * FlightResult.ChdTax) + (FlightResult.Infant * FlightResult.InfTax);
                    FlightResult.TotBfare = (FlightResult.Adult * FlightResult.AdtBfare) + (FlightResult.Child * FlightResult.ChdBFare) + (FlightResult.Infant * FlightResult.InfBfare);
                    FlightResult.AvailableSeats = FlightResult.AdtAvlStatus + ":" + FlightResult.ChdAvlStatus + ":" + FlightResult.InfAvlStatus;
                    FlightResult.TotMrkUp = (FlightResult.ADTAdminMrk * FlightResult.Adult) + (FlightResult.ADTAgentMrk * FlightResult.Adult) + (FlightResult.CHDAdminMrk * FlightResult.Child) + (FlightResult.CHDAgentMrk * FlightResult.Child);
                    FlightResult.TotalFare = FlightResult.TotalFare + FlightResult.TotMrkUp + FlightResult.STax + FlightResult.TFee + FlightResult.TotMgtFee;
                    FlightResult.NetFare = (FlightResult.TotalFare + FlightResult.TotTds) - (FlightResult.TotDis + FlightResult.TotCB + (FlightResult.ADTAgentMrk * FlightResult.Adult) + (FlightResult.CHDAgentMrk * FlightResult.Child));
                }
                else
                {
                    Error = true;
                }

                
               


            }
            catch (Exception ex)
            {
                Error = true;
            }
            return FlightResult;
        }

        private int CalCulateDepartureTime(string Date, string Time)
        {
            int TotalMinutes = 0;
            try
            {
                DateTime newDate = new DateTime(Convert.ToInt32(Date.Substring(0, 4)), Convert.ToInt32(Date.ToString().Substring(4, 2)), Convert.ToInt32(Date.Substring(6, 2)), Convert.ToInt32(Time.Substring(0, 2)), Convert.ToInt32(Time.Substring(2, 2)),0);
                DateTime NowDateTime = DateTime.Now;

                TotalMinutes = (int)newDate.Subtract(NowDateTime).TotalMinutes;
            }
            catch(Exception ex)
            {

            }
            return TotalMinutes;
        }

        private Tuple<string, int , int> FareClassAvailability(int TotalPax, JToken eachFareClass,int Leg, out bool ExpetationMeet)
        {
            ExpetationMeet = false;
            Tuple<string, int , int> FareClass = new Tuple<string, int ,int>("", 0 , 0);
            try
            {
                if (TotalPax <= Convert.ToInt32(eachFareClass[1]))
                {
                    ExpetationMeet = true;
                    FareClass = new Tuple<string, int , int>((string)eachFareClass[0], Convert.ToInt32(eachFareClass[1]),Leg);
                }
            }
            catch (Exception EX)
            {

            }
            return FareClass;
        }

        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].City;
            }

            return name;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private string SetFareClass(string productClass)
        {
            string fareclass = "";

            switch (productClass)
            {
                case "EC":
                    fareclass = "Economy";
                    break;
                case "EP":
                    fareclass = "Economy Promo";
                    break;
                case "PM":
                    fareclass = "Premium";
                    break;
                case "HF":
                    fareclass = "High Flyer";
                    break;
                case "DT":
                    fareclass = "Duty Travel";
                    break;
                case "FS":
                    fareclass = "Free SSR";
                    break;
                case "PP":
                    fareclass = "Premium Promo";
                    break;
                default:
                    fareclass = "Economy";
                    break;

            }
            return fareclass;
        }


        public string StarAirlineBooking(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, DataTable MBDT, ref ArrayList TktNoArray, string Constr)
        {
            string Pnr = "-FQ";
            StarAirlineRequest ObjRequest = new StarAirlineRequest();
            StarAirlineService ObjPost = new StarAirlineService();
            try
            {
                DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");


                string BookingResponse = ObjPost.ResponseGetter(ObjRequest.BookingRequest(FltDT,PaxDs, FltHdrs, Convert.ToString(Crd.Tables[0].Rows[0]["ServerUrlOrIP"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"])));

                //BookingResponse = File.ReadAllText(@"H:\StarAirlines\BookingResponse.txt");
                if (!string.IsNullOrEmpty(BookingResponse) && BookingResponse != "ERROR:::")
                {
                    JObject BookingRes = JObject.Parse(BookingResponse);

                    if (BookingRes != null && BookingRes["err_code"].ToString() == "0")
                    {
                        Pnr = BookingRes["book_code"].ToString();
                        string BookingStatus = BookingRes["status"].ToString();

                        if (BookingStatus == "HK")
                        {
                            string PaymentStatus = ObjPost.ResponseGetter(ObjRequest.AfterBookPaymentRequest(Convert.ToString(Crd.Tables[0].Rows[0]["ServerUrlOrIP"]), Convert.ToString(Crd.Tables[0].Rows[0]["UserID"]), Convert.ToString(Crd.Tables[0].Rows[0]["Password"]), Pnr));
                            //PaymentStatus = File.ReadAllText(@"H:\StarAirlines\PaymentResponse.txt");
                            if (!string.IsNullOrEmpty(BookingResponse) && PaymentStatus != "ERROR:::")
                            {
                                JObject JPaymentStatus = JObject.Parse(PaymentStatus);

                                if (JPaymentStatus != null && JPaymentStatus["err_code"].ToString() == "0")
                                {
                                    Pnr = JPaymentStatus["book_code"].ToString();
                                }
                                else
                                {
                                    Pnr = Pnr + "-FQ";
                                }
                            }
                        }
                        else
                        {
                            Pnr = Pnr + "-FQ";
                        }

                    }
                }
                

            }
            catch(Exception ex)
            {

            }
            return Pnr;
        }
    }
}
