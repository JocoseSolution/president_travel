﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Class1
/// </summary>
namespace AirArabia
{
    public class RequestResponse
    {
        public string getAvailabilty(FltSearch obj)
        {
            string xml = pxml(obj);
            string sResponse = "";
            sResponse = PostXml(obj.ServerUrlorIp, xml,"");
            return sResponse;


        }

        public string PostXml(string url, string xml, string JSessionId)
        {
            StringBuilder sbResult = new StringBuilder();

            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                    if (xml.Contains("OTA_AirAvailRQ") == false)
                        Http.Headers.Add(HttpRequestHeader.Cookie, JSessionId);
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = "text/xml";
                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        //throw new ApplicationException(message);
                    }
                    else
                    {
                        if (xml.Contains("OTA_AirAvailRQ") == true)
                            JSessionId = STD.BAL.Utility.Split(WebResponse.Headers["Set-Cookie"].ToString(), ";")[0].ToString();

                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();

                    }
                }
            }
            catch (Exception ex)
            {
                string ss = ex.Message;
            }
            if (xml.Contains("OTA_AirAvailRQ") == true)
                return JSessionId + "~" + sbResult.ToString();
            else
                return sbResult.ToString();
        }

        public string pxml(FltSearch ObjFltSearch)
        {
            StringBuilder sb = new StringBuilder();
            string xyz;

            sb.Append(string.Format("<?xml version='1.0' encoding='utf-8'?>"));
            sb.Append(string.Format("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>"));
            sb.Append(string.Format("<soap:Header>"));
            sb.Append(string.Format("<wsse:Security soap:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:UsernameToken wsu:Id='UsernameToken-17855236' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:Username>" + ObjFltSearch.UserId + "</wsse:Username>"));
            sb.Append(string.Format("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>" + ObjFltSearch.Password + "</wsse:Password>"));
            sb.Append(string.Format("</wsse:UsernameToken>"));
            sb.Append(string.Format("</wsse:Security>"));
            sb.Append(string.Format("</soap:Header>"));
            sb.Append(string.Format("<soap:Body xmlns:ns2='http://www.opentravel.org/OTA/2003/05'>"));
            sb.Append(string.Format("<ns2:OTA_AirAvailRQ EchoToken='11868765275150-1300257933' PrimaryLangID='en-us' SequenceNmbr='1' Target='TEST' TimeStamp='2012-12-22T17:15:04' Version='20061.00'>"));
            sb.Append(string.Format("<ns2:POS>"));
            sb.Append(string.Format("<ns2:Source TerminalID='TestUser/Test Runner'>"));
            sb.Append(string.Format("<ns2:RequestorID ID='WSSPRINGT' Type='4' />"));
            sb.Append(string.Format("<ns2:BookingChannel Type='12' />"));
            sb.Append(string.Format("</ns2:Source>"));
            sb.Append(string.Format("</ns2:POS>"));
            sb.Append(string.Format("<ns2:OriginDestinationInformation>"));
            sb.Append(string.Format("<ns2:DepartureDateTime>" + ObjFltSearch.DepartureDateTime + "</ns2:DepartureDateTime>"));
            sb.Append(string.Format("<ns2:OriginLocation LocationCode='" + ObjFltSearch.OriginLocation + "' />"));
            sb.Append(string.Format("<ns2:DestinationLocation LocationCode='" + ObjFltSearch.DestinationLocation + "' />"));

            sb.Append(string.Format("</ns2:OriginDestinationInformation>"));

            xyz = sb.ToString();

            if (ObjFltSearch.TripType == "OneWay")
            {
                xyz = sb.ToString();
                sb.Append(string.Format("<ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("<ns2:AirTravelerAvail>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='ADT'  Quantity='" + ObjFltSearch.dTotalNo_Adt + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='CHD'  Quantity='" + ObjFltSearch.dTotalNo_CHD + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='INF'  Quantity='" + ObjFltSearch.dTotalNo_INF + "'/>"));
                sb.Append(string.Format("</ns2:AirTravelerAvail>"));
                sb.Append(string.Format("</ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("</ns2:OTA_AirAvailRQ>"));
                sb.Append(string.Format("</soap:Body>"));
                sb.Append(string.Format("</soap:Envelope>"));
                sb.ToString();


            }
            else if (ObjFltSearch.TripType == "Return")
            {
                xyz = sb.ToString();

                sb.Append(string.Format("<ns2:OriginDestinationInformation>"));
                sb.Append(string.Format("<ns2:DepartureDateTime>" + ObjFltSearch.ArrivalDateTime + "</ns2:DepartureDateTime>"));
                sb.Append(string.Format("<ns2:OriginLocation LocationCode= '" + ObjFltSearch.DestinationLocation + "'/>"));
                sb.Append(string.Format("<ns2:DestinationLocation LocationCode='" + ObjFltSearch.OriginLocation + "'/>"));

                sb.Append(string.Format("</ns2:OriginDestinationInformation>"));
                sb.Append(string.Format(" <ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("<ns2:AirTravelerAvail>"));

                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='ADT'  Quantity='" + ObjFltSearch.dTotalNo_Adt + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='CHD'  Quantity='" + ObjFltSearch.dTotalNo_CHD + "'/>"));
                sb.Append(string.Format("<ns2:PassengerTypeQuantity Code='INF'  Quantity='" + ObjFltSearch.dTotalNo_INF + "'/>"));

                sb.Append(string.Format("</ns2:AirTravelerAvail>"));
                sb.Append(string.Format("</ns2:TravelerInfoSummary>"));
                sb.Append(string.Format("</ns2:OTA_AirAvailRQ>"));
                sb.Append(string.Format("</soap:Body>"));
                sb.Append(string.Format("</soap:Envelope>"));

            }
            return sb.ToString();
        }

        public string PriceQuote(Request_Input ObjReqInput)
        {

            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("<?xml version='1.0' encoding='utf-8' ?>"));
            sb.Append(string.Format("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'>"));
            sb.Append(string.Format("<soap:Header>"));
            sb.Append(string.Format("<wsse:Security soap:mustUnderstand='1' xmlns:wsse='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:UsernameToken wsu:Id='UsernameToken-32124385' xmlns:wsu='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>"));
            sb.Append(string.Format("<wsse:Username>" + ObjReqInput.CorporateId + "</wsse:Username>"));
            sb.Append(string.Format("<wsse:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText'>" + ObjReqInput.Password + "</wsse:Password>"));
            sb.Append(string.Format("</wsse:UsernameToken>"));
            sb.Append(string.Format("</wsse:Security>"));
            sb.Append(string.Format("</soap:Header>"));
            sb.Append(string.Format("<soap:Body xmlns:ns1='http://www.opentravel.org/OTA/2003/05'>"));
            sb.Append(string.Format("<ns1:OTA_AirPriceRQ EchoToken='11868765275150-1300257933' PrimaryLangID='en-us' SequenceNmbr='1' TimeStamp='2012-21-12T11:25:00' TransactionIdentifier='" + ObjReqInput.TransactionIdentifier + "' Version='20061.00'>"));
            sb.Append(string.Format("<ns1:POS>"));
            sb.Append(string.Format("<ns1:Source TerminalID='TestUser/Test Runner'>"));
            sb.Append(string.Format("<ns1:RequestorID ID='WSSPRINGT' Type='4'/>"));
            sb.Append(string.Format("<ns1:BookingChannel Type='12' />"));
            sb.Append(string.Format("</ns1:Source>"));
            sb.Append(string.Format("</ns1:POS>"));
            sb.Append(string.Format("<ns1:AirItinerary DirectionInd='" + ObjReqInput.TripType + "'>"));
            sb.Append(string.Format("<ns1:OriginDestinationOptions>"));

            for (int i = 0; i <= ObjReqInput.NoOfStop; i++)
            {

                sb.Append(string.Format("<ns1:OriginDestinationOption>"));
                sb.Append(string.Format("<ns1:FlightSegment ArrivalDateTime='" + ObjReqInput.ArrivalDateTime[i] + "' DepartureDateTime='" + ObjReqInput.DepartureDateTime[i] + "' FlightNumber='" + ObjReqInput.FlightNumber[i] + "' RPH='" + ObjReqInput.RPH[i] + "'>"));
                sb.Append(string.Format("<ns1:DepartureAirport LocationCode='" + ObjReqInput.OriginLocation[i] + "' Terminal='TerminalX'/>"));
                sb.Append(string.Format("<ns1:ArrivalAirport LocationCode='" + ObjReqInput.DestinationLocation[i] + "' Terminal='TerminalX'/>"));
                sb.Append(string.Format("</ns1:FlightSegment>"));
                sb.Append(string.Format("</ns1:OriginDestinationOption>"));

            }

            sb.Append(string.Format("</ns1:OriginDestinationOptions>"));
            sb.Append(string.Format("</ns1:AirItinerary>"));
            sb.Append(string.Format(" <ns1:TravelerInfoSummary>"));
            sb.Append(string.Format("<ns1:AirTravelerAvail>"));
            sb.Append(string.Format("<ns1:PassengerTypeQuantity Code='ADT' Quantity='" + ObjReqInput.dTotalNo_Adt + "'/>"));
            sb.Append(string.Format("<ns1:PassengerTypeQuantity Code='CHD' Quantity='" + ObjReqInput.dTotalNo_CHD + "'/>"));
            sb.Append(string.Format("<ns1:PassengerTypeQuantity Code='INF' Quantity='" + ObjReqInput.dTotalNo_INF + "'/>"));
            sb.Append(string.Format("</ns1:AirTravelerAvail>"));
            sb.Append(string.Format("</ns1:TravelerInfoSummary>"));
            sb.Append(string.Format("</ns1:OTA_AirPriceRQ>"));
            sb.Append(string.Format("</soap:Body>"));
            sb.Append(string.Format("</soap:Envelope>"));

            string result = PostXml(ObjReqInput.ServerUrl, sb.ToString(),ObjReqInput.JSessionId);
            return result.ToString();


        }










    }
}
