﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using STD.Shared;
using System.Collections;
//using ExceptionLog;

namespace STD.DAL
{
    public class FlightSearchDAL
    {
        SqlConnection conn;
        SqlCommand cmd;

        public FlightSearchDAL()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);

        }

        public bool Insert_Selected_Flight(Dictionary<string, object> Row, string TID)
        {
            //Add TrackId and other Fields to Datatable and then Insert
            SelectedFlightDetail a = new SelectedFlightDetail();
            a = SetVal(Row, TID);

            try
            {
                conn.Open();
                cmd = new SqlCommand("SP_FLTDETAILS_INSERT_GAL", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@ORGDESTFROM", a.ORGDESTFROM);
                cmd.Parameters.AddWithValue("@ORGDESTTO", a.ORGDESTTO);
                cmd.Parameters.AddWithValue("@DEPARTURELOCATION", a.DEPARTURELOCATION);
                cmd.Parameters.AddWithValue("@DEPARTURECITYNAME", a.DEPARTURECITYNAME);
                cmd.Parameters.AddWithValue("@DEPAIRPORTCODE", a.DEPAIRPORTCODE);
                cmd.Parameters.AddWithValue("@DEPARTURETERMINAL", a.DEPARTURETERMINAL);
                cmd.Parameters.AddWithValue("@ARRIVALLOCATION", a.ARRIVALLOCATION);
                cmd.Parameters.AddWithValue("@ARRIVALCITYNAME", a.ARRIVALCITYNAME);
                cmd.Parameters.AddWithValue("@ARRAIRPORTCODE", a.ARRAIRPORTCODE);
                cmd.Parameters.AddWithValue("@ARRIVALTERMINAL", a.ARRIVALTERMINAL);
                cmd.Parameters.AddWithValue("@DEPARTUREDATE", a.DEPARTUREDATE);
                cmd.Parameters.AddWithValue("@DEPARTURE_DATE", a.DEPARTURE_DATE);
                cmd.Parameters.AddWithValue("@DEPARTURETIME", a.DEPARTURETIME.Length == 3 ? "0" + a.DEPARTURETIME : a.DEPARTURETIME);
                cmd.Parameters.AddWithValue("@ARRIVALDATE", a.ARRIVALDATE);
                cmd.Parameters.AddWithValue("@ARRIVAL_DATE", a.ARRIVAL_DATE);
                cmd.Parameters.AddWithValue("@ARRIVALTIME", a.ARRIVALTIME.Length == 3 ? "0" + a.ARRIVALTIME : a.ARRIVALTIME);
                cmd.Parameters.AddWithValue("@ADULT", a.ADULT);
                cmd.Parameters.AddWithValue("@CHILD", a.CHILD);
                cmd.Parameters.AddWithValue("@INFANT", a.INFANT);
                cmd.Parameters.AddWithValue("@TOTPAX", a.TOTPAX);
                cmd.Parameters.AddWithValue("@MARKETINGCARRIER", a.MARKETINGCARRIER);
                cmd.Parameters.AddWithValue("@OPERATINGCARRIER", a.OPERATINGCARRIER);
                cmd.Parameters.AddWithValue("@FLIGHTIDENTIFICATION", a.FLIGHTIDENTIFICATION);
                cmd.Parameters.AddWithValue("@VALIDATINGCARRIER", a.VALIDATINGCARRIER);
                cmd.Parameters.AddWithValue("@AIRLINENAME", a.AIRLINENAME);
                cmd.Parameters.AddWithValue("@AVAILABLESEATS", a.AVAILABLESEATS);
                cmd.Parameters.AddWithValue("@ADTCABIN", a.ADTCABIN);
                cmd.Parameters.AddWithValue("@CHDCABIN", a.CHDCABIN);
                cmd.Parameters.AddWithValue("@INFCABIN", a.INFCABIN);
                cmd.Parameters.AddWithValue("@ADTRBD", a.ADTRBD);
                cmd.Parameters.AddWithValue("@CHDRBD", a.CHDRBD);
                cmd.Parameters.AddWithValue("@INFRBD", a.INFRBD);
                cmd.Parameters.AddWithValue("@RBD", a.RBD);
                cmd.Parameters.AddWithValue("@ADTFARE", a.ADTFARE);
                cmd.Parameters.AddWithValue("@ADTBFARE", a.ADTBFARE);
                cmd.Parameters.AddWithValue("@ADTTAX", a.ADTTAX);
                cmd.Parameters.AddWithValue("@CHDFARE", a.CHDFARE);
                cmd.Parameters.AddWithValue("@CHDBFARE", a.CHDBFARE);
                cmd.Parameters.AddWithValue("@CHDTAX", a.CHDTAX);
                cmd.Parameters.AddWithValue("@INFFARE", a.INFFARE);
                cmd.Parameters.AddWithValue("@INFBFARE", a.INFBFARE);
                cmd.Parameters.AddWithValue("@INFTAX", a.INFTAX);
                cmd.Parameters.AddWithValue("@TOTALBFARE", a.TOTALBFARE);
                cmd.Parameters.AddWithValue("@TOTALFARE", a.TOTALFARE);
                cmd.Parameters.AddWithValue("@TOTALTAX", a.TOTALTAX);
                cmd.Parameters.AddWithValue("@NETFARE", a.NETFARE);
                cmd.Parameters.AddWithValue("@STAX", a.STAX);
                cmd.Parameters.AddWithValue("@TFEE", a.TFEE);
                cmd.Parameters.AddWithValue("@DISCOUNT", a.DISCOUNT);
                cmd.Parameters.AddWithValue("@SEARCHVALUE", a.SEARCHVALUE);
                cmd.Parameters.AddWithValue("@LINEITEMNUMBER", a.LINEITEMNUMBER);
                cmd.Parameters.AddWithValue("@LEG", a.LEG);
                cmd.Parameters.AddWithValue("@FLIGHT", a.FLIGHT);
                cmd.Parameters.AddWithValue("@PROVIDER", a.PROVIDER);
                cmd.Parameters.AddWithValue("@TOT_DUR", a.TOT_DUR);
                cmd.Parameters.AddWithValue("@TRIPTYPE", a.TRIPTYPE);
                cmd.Parameters.AddWithValue("@EQ", a.EQ);
                cmd.Parameters.AddWithValue("@STOPS", a.STOPS);
                cmd.Parameters.AddWithValue("@TRIP", a.TRIP);
                cmd.Parameters.AddWithValue("@SECTOR", a.SECTOR);
                cmd.Parameters.AddWithValue("@TRIPCNT", a.TRIPCNT);
                cmd.Parameters.AddWithValue("@CURRENCY", "INR");
                cmd.Parameters.AddWithValue("@ADTADMINMRK", a.ADTADMINMRK);
                cmd.Parameters.AddWithValue("@ADTAGENTMRK", a.ADTAGENTMRK);
                //cmd.Parameters.AddWithValue("@ADTDISTMRK", a.ADTDISTMRK); //Added
                cmd.Parameters.AddWithValue("@CHDADMINMRK", a.CHDADMINMRK);
                cmd.Parameters.AddWithValue("@CHDAGENTMRK", a.CHDAGENTMRK);
                //cmd.Parameters.AddWithValue("@CHDDISTMRK", a.CHDDISTMRK);//ADDED
                cmd.Parameters.AddWithValue("@INFADMINMRK", a.INFADMINMRK);
                cmd.Parameters.AddWithValue("@INFAGENTMRK", a.INFAGENTMRK);
                cmd.Parameters.AddWithValue("@IATACOMM", a.IATACOMM);         
                cmd.Parameters.AddWithValue("@ADTFARETYPE", a.ADTFARETYPE);
                cmd.Parameters.AddWithValue("@ADTFAREBASIS", a.ADTFAREBASIS);
                cmd.Parameters.AddWithValue("@CHDFARETYPE", a.CHDFARETYPE);
                cmd.Parameters.AddWithValue("@CHDFAREBASIS", a.CHDFAREBASIS);
                cmd.Parameters.AddWithValue("@INFFARETYPE", a.INFFARETYPE);
                cmd.Parameters.AddWithValue("@INFFAREBASIS", a.INFFAREBASIS);  
                cmd.Parameters.AddWithValue("@FAREBASIS", a.FAREBASIS);
                cmd.Parameters.AddWithValue("@FBPAXTYPE", a.FBPAXTYPE);
                cmd.Parameters.AddWithValue("@ADTFSUR", a.ADTFSUR);
                cmd.Parameters.AddWithValue("@CHDFSUR", a.CHDFSUR);
                cmd.Parameters.AddWithValue("@INFFSUR", a.INFFSUR);
                cmd.Parameters.AddWithValue("@TOTALFUELSUR", a.TOTALFUELSUR);
                cmd.Parameters.AddWithValue("@SNO", a.SNO);
                cmd.Parameters.AddWithValue("@DEPDATELCC", a.DEPDATELCC);
                cmd.Parameters.AddWithValue("@ARRDATELCC", a.ARRDATELCC);
                cmd.Parameters.AddWithValue("@ORIGINALTF", a.ORIGINALTF);
                cmd.Parameters.AddWithValue("@ORIGINALTT", a.ORIGINALTT);
                cmd.Parameters.AddWithValue("@TRACK_ID", a.TRACK_ID); //Insert TrackId generated by Random Function.
                cmd.Parameters.AddWithValue("@FLIGHTSTATUS", a.FLIGHTSTATUS);
                cmd.Parameters.AddWithValue("@ADT_TAX", a.ADT_TAX);
                cmd.Parameters.AddWithValue("@ADTOT", a.ADTOT);
                cmd.Parameters.AddWithValue("@ADTSRVTAX", a.ADTSRVTAX);
                cmd.Parameters.AddWithValue("@CHD_TAX", a.CHD_TAX);
                cmd.Parameters.AddWithValue("@CHDOT", a.CHDOT);
                cmd.Parameters.AddWithValue("@CHDSRVTAX", a.CHDSRVTAX);
                cmd.Parameters.AddWithValue("@INF_TAX", a.INF_TAX);
                cmd.Parameters.AddWithValue("@INFOT", a.INFOT);
                cmd.Parameters.AddWithValue("@INFSRVTAX", a.INFSRVTAX);
                cmd.Parameters.AddWithValue("@SRVTAX", a.SRVTAX);
                cmd.Parameters.AddWithValue("@TF", a.TF);
                cmd.Parameters.AddWithValue("@TC", a.TC);
                cmd.Parameters.AddWithValue("@ADTTDS", a.ADTTDS);
                cmd.Parameters.AddWithValue("@CHDTDS", a.CHDTDS);
                cmd.Parameters.AddWithValue("@INFTDS", a.INFTDS);
                cmd.Parameters.AddWithValue("@ADTCOMM", a.ADTCOMM);
                cmd.Parameters.AddWithValue("@CHDCOMM", a.CHDCOMM);
                cmd.Parameters.AddWithValue("@INFCOMM", a.INFCOMM);
                cmd.Parameters.AddWithValue("@ADTCB", a.ADTCB);
                cmd.Parameters.AddWithValue("@CHDCB", a.CHDCB);
                cmd.Parameters.AddWithValue("@INFCB", a.INFCB);
                cmd.Parameters.AddWithValue("@ElectronicTicketing", a.ElectronicTicketing);
                cmd.Parameters.AddWithValue("@USERID", a.USERID);
                cmd.Parameters.AddWithValue("@ADTMGTFEE", a.ADTMGTFEE);
                cmd.Parameters.AddWithValue("@CHDMGTFEE", a.CHDMGTFEE);
                cmd.Parameters.AddWithValue("@INFMGTFEE", a.INFMGTFEE);
                cmd.Parameters.AddWithValue("@TOTMGTFEE", a.TOTMGTFEE);
                cmd.Parameters.AddWithValue("@ISCORP", a.ISCORP);
                cmd.Parameters.AddWithValue("@ADTSRVTAX1", a.ADTSRVTAX1);
                cmd.Parameters.AddWithValue("@CHDSRVTAX1", a.CHDSRVTAX1);
                cmd.Parameters.AddWithValue("@ADTCOMM1", a.ADTCOMM1);
                cmd.Parameters.AddWithValue("@CHDCOMM1", a.CHDCOMM1);
                cmd.Parameters.AddWithValue("@RESULTTYPE", a.RESULTTYPE);
                cmd.Parameters.AddWithValue("@OldTrack_Id", Row.ContainsKey("Track_Old") ? Row["Track_Old"] : "");
                cmd.Parameters.AddWithValue("@AirlineRemark", Convert.ToString( a.AIRLINEREMARK));
                cmd.Parameters.AddWithValue("@SearchId", Convert.ToString(a.SEARCHID));
                cmd.Parameters.AddWithValue("@IsBagFare", Convert.ToBoolean(a.IsBagFare));
                cmd.Parameters.AddWithValue("@SSRCode", Convert.ToString(a.SSRCode));
                cmd.Parameters.AddWithValue("@IsSMEFare", Convert.ToBoolean(a.IsSMEFare));

                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;  //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
            }
            finally
            {
                cmd.Dispose();
                conn.Close();
                conn.Dispose();
            }
            return true;
        }

        public static SelectedFlightDetail SetVal(Dictionary<string, object> Row, string TID)
        {
            SelectedFlightDetail obj = new SelectedFlightDetail();
            int length = Row.Count;
            int i = 0;
            Type t;
            //List<object> a = new List<object>();
            Dictionary<string, object> InsertRow = new Dictionary<string, object>();
            try
            {
                // Add Records to Final Dictionary  by Removing Null values.
                foreach (KeyValuePair<string, object> pair in Row)
                {
                    if (pair.Value == null)
                    {
                        InsertRow.Add(pair.Key, "");
                    }
                    else
                    {
                        InsertRow.Add(pair.Key, pair.Value);
                    }
                }
                //Set Values in the Object
                //Note :-Insert TrackId generated by Random Function.
                //obj.DISTRID = InsertRow["FltInfoID"].ToString();
                //obj.DISTRID = InsertRow["DISTRID"].ToString(); //From Session
                obj.ORGDESTFROM = InsertRow["OrgDestFrom"].ToString();
                obj.ORGDESTTO = InsertRow["OrgDestTo"].ToString();
                obj.DEPARTURELOCATION = InsertRow["DepartureLocation"].ToString();
                obj.DEPARTURECITYNAME = InsertRow["DepartureCityName"].ToString();
                obj.DEPAIRPORTCODE = InsertRow["DepAirportCode"].ToString();
                obj.DEPARTURETERMINAL = InsertRow["DepartureTerminal"].ToString();
                obj.ARRIVALLOCATION = InsertRow["ArrivalLocation"].ToString();
                obj.ARRIVALCITYNAME = InsertRow["ArrivalCityName"].ToString();
                obj.ARRAIRPORTCODE = InsertRow["ArrAirportCode"].ToString();
                obj.ARRIVALTERMINAL = InsertRow["ArrivalTerminal"].ToString();
                obj.DEPARTUREDATE = InsertRow["DepartureDate"].ToString();
                obj.DEPARTURE_DATE = InsertRow["Departure_Date"].ToString();
                obj.DEPARTURETIME = InsertRow["DepartureTime"].ToString();
                obj.ARRIVALDATE = InsertRow["ArrivalDate"].ToString();
                obj.ARRIVAL_DATE = InsertRow["Arrival_Date"].ToString();
                obj.ARRIVALTIME = InsertRow["ArrivalTime"].ToString();
                obj.ADULT = Convert.ToInt32(InsertRow["Adult"].ToString());
                obj.CHILD = Convert.ToInt32(InsertRow["Child"].ToString());
                obj.INFANT = Convert.ToInt32(InsertRow["Infant"].ToString());
                obj.TOTPAX = obj.ADULT + obj.CHILD + obj.INFANT;
                obj.MARKETINGCARRIER = InsertRow["MarketingCarrier"].ToString();
                obj.OPERATINGCARRIER = InsertRow["OperatingCarrier"].ToString();
                obj.FLIGHTIDENTIFICATION = InsertRow["FlightIdentification"].ToString();
                obj.VALIDATINGCARRIER = InsertRow["ValiDatingCarrier"].ToString();
                obj.AIRLINENAME = InsertRow["AirLineName"].ToString();
                obj.AVAILABLESEATS = InsertRow["AvailableSeats"].ToString();
                obj.ADTCABIN = InsertRow["AdtCabin"].ToString();
                obj.CHDCABIN = InsertRow["ChdCabin"].ToString();
                obj.INFCABIN = InsertRow["InfCabin"].ToString();
                obj.ADTRBD = InsertRow["AdtRbd"].ToString();
                obj.CHDRBD = InsertRow["ChdRbd"].ToString();
                obj.INFRBD = InsertRow["InfRbd"].ToString();
                obj.RBD = InsertRow["RBD"].ToString();  
                obj.ADTFARE = InsertRow["AdtFare"].ToString();
                obj.ADTBFARE = InsertRow["AdtBfare"].ToString();
                obj.ADTTAX = InsertRow["AdtTax"].ToString();
                obj.CHDFARE = InsertRow["ChdFare"].ToString();
                obj.CHDBFARE = InsertRow["ChdBFare"].ToString();
                obj.CHDTAX = InsertRow["ChdTax"].ToString();
                obj.INFFARE = InsertRow["InfFare"].ToString();
                obj.INFBFARE = InsertRow["InfBfare"].ToString();
                obj.INFTAX = InsertRow["InfTax"].ToString();
                obj.TOTALBFARE = InsertRow["TotBfare"].ToString();
                obj.TOTALFARE = InsertRow["TotalFare"].ToString(); //TotalFare
                obj.TOTALTAX = InsertRow["TotalTax"].ToString();
                obj.NETFARE = Convert.ToDecimal(InsertRow["NetFare"].ToString());
                obj.STAX = InsertRow["STax"].ToString();
                obj.TFEE = InsertRow["TFee"].ToString();
                obj.DISCOUNT = InsertRow["TotDis"].ToString();
                obj.SEARCHVALUE = InsertRow["Searchvalue"].ToString();
                obj.LINEITEMNUMBER = InsertRow["LineNumber"].ToString();
                obj.LEG = InsertRow["Leg"].ToString();
                obj.FLIGHT = InsertRow["Flight"].ToString();
                obj.PROVIDER = InsertRow["Provider"].ToString();
                obj.TOT_DUR = InsertRow["TotDur"].ToString();
                obj.TRIPTYPE = InsertRow["TripType"].ToString();
                obj.EQ = InsertRow["EQ"].ToString();
                obj.STOPS = InsertRow["Stops"].ToString();
                obj.TRIP = InsertRow["Trip"].ToString();
                obj.SECTOR = InsertRow["Sector"].ToString();
                obj.TRIPCNT = InsertRow["TripCnt"].ToString();
                obj.ADTADMINMRK = InsertRow["ADTAdminMrk"].ToString();
                //obj.ADTDISTMRK = InsertRow["ADTDistMrk"].ToString();
                obj.ADTAGENTMRK = InsertRow["ADTAgentMrk"].ToString();
                obj.CHDADMINMRK = InsertRow["CHDAdminMrk"].ToString();
                //obj.CHDDISTMRK = InsertRow["CHDDistMrk"].ToString();
                obj.CHDAGENTMRK = InsertRow["CHDAgentMrk"].ToString();
                obj.INFADMINMRK = InsertRow["InfAdminMrk"].ToString();
                obj.INFAGENTMRK = InsertRow["InfAgentMrk"].ToString();
                obj.IATACOMM = InsertRow["IATAComm"].ToString();
                obj.ADTFARETYPE = InsertRow["AdtFareType"].ToString();
                obj.ADTFAREBASIS = InsertRow["AdtFarebasis"].ToString();
                obj.CHDFARETYPE = InsertRow["ChdfareType"].ToString();
                obj.CHDFAREBASIS = InsertRow["ChdFarebasis"].ToString();
                obj.INFFARETYPE = InsertRow["InfFareType"].ToString();
                obj.INFFAREBASIS = InsertRow["InfFarebasis"].ToString();
                obj.FAREBASIS = InsertRow["fareBasis"].ToString();
                obj.FBPAXTYPE = InsertRow["FBPaxType"].ToString();
                obj.ADTFSUR = Convert.ToDecimal(InsertRow["AdtFSur"].ToString());
                obj.CHDFSUR = Convert.ToDecimal(InsertRow["ChdFSur"].ToString());
                obj.INFFSUR = Convert.ToDecimal(InsertRow["InfFSur"].ToString());
                obj.TOTALFUELSUR = Convert.ToDecimal(InsertRow["AdtFSur"].ToString()) + Convert.ToDecimal(InsertRow["ChdFSur"].ToString()) + Convert.ToDecimal(InsertRow["InfFSur"].ToString());
                obj.SNO = InsertRow["sno"].ToString();
                obj.DEPDATELCC = InsertRow["depdatelcc"].ToString();
                obj.ARRDATELCC = InsertRow["arrdatelcc"].ToString();
                obj.ORIGINALTF = Convert.ToDecimal(InsertRow["OriginalTF"]);
                obj.ORIGINALTT = Convert.ToDecimal(InsertRow["OriginalTT"]);
                obj.TRACK_ID = TID;  //TrackId by Random Function //InsertRow["Track_id"].ToString();
                //Fare BreakUp Fields 
                obj.FLIGHTSTATUS = InsertRow["FType"].ToString();
                obj.ADT_TAX = Append_Tax(InsertRow["AdtFSur"].ToString(), InsertRow["AdtYR"].ToString(), InsertRow["AdtWO"].ToString(), InsertRow["AdtIN"].ToString(), InsertRow["AdtQ"].ToString(), InsertRow["AdtJN"].ToString(), InsertRow["AdtOT"].ToString());
                obj.ADTOT = InsertRow["AdtOT"].ToString();
                obj.ADTSRVTAX = InsertRow["AdtSrvTax"].ToString();
                obj.ADTSRVTAX1 = InsertRow["AdtSrvTax1"].ToString();
                obj.CHD_TAX = Append_Tax(InsertRow["ChdFSur"].ToString(), InsertRow["ChdYR"].ToString(), InsertRow["ChdWO"].ToString(), InsertRow["ChdIN"].ToString(), InsertRow["ChdQ"].ToString(), InsertRow["ChdJN"].ToString(), InsertRow["ChdOT"].ToString());
                obj.CHDOT = InsertRow["ChdOT"].ToString();
                obj.CHDSRVTAX = InsertRow["ChdSrvTax"].ToString();
                obj.CHDSRVTAX1 = InsertRow["ChdSrvTax1"].ToString(); 
                obj.INF_TAX = Append_Tax(InsertRow["InfFSur"].ToString(), InsertRow["InfYR"].ToString(), InsertRow["InfWO"].ToString(), InsertRow["InfIN"].ToString(), InsertRow["InfQ"].ToString(), InsertRow["InfJN"].ToString(), InsertRow["InfOT"].ToString());
                obj.INFOT = InsertRow["InfOT"].ToString();
                obj.INFSRVTAX = InsertRow["InfSrvTax"].ToString();
                obj.SRVTAX = Convert.ToDecimal(InsertRow["STax"].ToString());//Convert.ToDecimal(obj.ADTSRVTAX) + Convert.ToDecimal(obj.CHDSRVTAX) + Convert.ToDecimal(obj.INFSRVTAX);
                obj.TF = Convert.ToDecimal(InsertRow["TFee"].ToString());
                obj.TC = Convert.ToDecimal(InsertRow["TotMrkUp"].ToString());//((Convert.ToDecimal(obj.ADTADMINMRK) + Convert.ToDecimal(obj.ADTAGENTMRK)) * obj.ADULT) + ((Convert.ToDecimal(obj.CHDADMINMRK) + Convert.ToDecimal(obj.CHDAGENTMRK)) * obj.CHILD) + ((Convert.ToDecimal(obj.INFADMINMRK) + Convert.ToDecimal(obj.INFAGENTMRK)) * obj.INFANT);
                obj.ADTTDS = Convert.ToDecimal(InsertRow["AdtTds"].ToString());
                obj.CHDTDS = Convert.ToDecimal(InsertRow["ChdTds"].ToString());
                obj.INFTDS = Convert.ToDecimal(InsertRow["InfTds"].ToString());
                obj.ADTCOMM = Convert.ToDecimal(InsertRow["AdtDiscount"].ToString());
                obj.ADTCOMM1 = Convert.ToDecimal(InsertRow["AdtDiscount1"].ToString());
                obj.CHDCOMM = Convert.ToDecimal(InsertRow["ChdDiscount"].ToString());
                obj.CHDCOMM1 = Convert.ToDecimal(InsertRow["ChdDiscount1"].ToString());
                obj.INFCOMM = Convert.ToDecimal(InsertRow["InfDiscount"].ToString());
                obj.ADTCB = Convert.ToDecimal(InsertRow["AdtCB"].ToString());
                obj.CHDCB = Convert.ToDecimal(InsertRow["ChdCB"].ToString());
                obj.INFCB = Convert.ToDecimal(InsertRow["InfCB"].ToString());
                obj.ElectronicTicketing = InsertRow["ElectronicTicketing"].ToString();
                obj.ADTMGTFEE = Convert.ToDecimal(InsertRow["AdtMgtFee"].ToString());
                obj.CHDMGTFEE = Convert.ToDecimal(InsertRow["ChdMgtFee"].ToString());
                obj.INFMGTFEE = Convert.ToDecimal(InsertRow["InfMgtFee"].ToString());
                obj.TOTMGTFEE = Convert.ToDecimal(InsertRow["TotMgtFee"].ToString());
                obj.USERID = InsertRow["User_id"].ToString();
                obj.ISCORP = Convert.ToBoolean(InsertRow["IsCorp"].ToString());
                obj.RESULTTYPE = InsertRow["AdtFar"].ToString();//InsertRow["ProductDetailQualifier"].ToString();
                obj.AIRLINEREMARK = Convert.ToString(InsertRow["BagInfo"]);
                obj.SEARCHID = Convert.ToString(InsertRow["SearchId"]);
                obj.IsBagFare = Convert.ToBoolean(InsertRow["IsBagFare"]);
                obj.SSRCode = Convert.ToString(InsertRow["SSRCode"]);
                obj.IsSMEFare = Convert.ToBoolean(InsertRow["IsSMEFare"]);
            }
            catch (Exception ex)
            {
                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
            }

            return obj;
        }

        public static string Append_Tax(string YQ, string YR, string WO, string IN, string Q, string JN,string OT)
        {
            string Tax = "";
            if (YQ != "" || YQ != null)
            {
                Tax = Tax +"YQ:" +YQ + "#";
            }
            if (YR != "" || YR != null)
            {
                Tax = Tax + "YR:" + YR + "#";
            }
            if (WO != "" || WO != null)
            {
                Tax = Tax + "WO:" + WO + "#";
            }

            if (IN != "" || IN != null)
            {
                Tax = Tax + "IN:" + IN + "#";
            }
            if (Q != "" || Q != null)
            {
                Tax = Tax + "Q:" + Q + "#";
            }
            //if (JN != "" || JN != null)
            //{
            //    Tax = Tax + "JN:" + JN + "#";
            //}
            if (JN != "" || JN != null)
            {
                Tax = Tax + "K3:" + JN + "#";
            }
            if (OT != "" || OT != null)
            {
                Tax = Tax + "OT:" + OT + "#";
            }
            return Tax;
        }

    }
}
