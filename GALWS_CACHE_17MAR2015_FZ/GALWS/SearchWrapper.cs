﻿using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Collections.Concurrent;
using System.Data.SqlClient;
using System.Configuration;
using STD.DAL;

namespace STD.BAL
{
    public class SearchWrapper
    {
        DateTime now;
        public SearchWrapper()
        {
            now = DateTime.Now;
        }

        private SqlConnection Con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        List<string> ResultList = new List<string>();

        public List<string> FltSearchResult(FlightSearch searchParam, bool isCoupon, string aircodeWithProvider, bool isLCC, HttpContext context, string callApi)
        {
            ArrayList vlist1 = new ArrayList();
            ArrayList vlist = new ArrayList();


            // List<Task> cList = new List<Task>();
            ConcurrentBag<Task> cList = new ConcurrentBag<Task>();
            HttpContext.Current = context;

            if (searchParam.Trip == Trip.D)
            {
                #region Domestic Flight
                if (isLCC)
                {
                    if ((aircodeWithProvider.ToUpper().Contains("SG") || aircodeWithProvider.ToUpper().Contains("GSCPN")) && callApi.Trim().ToUpper() != "6E")
                    {

                        if (aircodeWithProvider.ToUpper().Contains("SS") && callApi.Trim().ToUpper() != "6E")
                        {
                            int a = 0;
                            SequeneFare objSeq = new SequeneFare(Con.ConnectionString);
                            if (a == 0)
                            {
                                a = 1;
                                if (aircodeWithProvider.ToUpper().Contains("SS") && isCoupon == false)
                                {
                                    string resultJsonSequence = "";
                                    FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                    newFltSearch.HidTxtAirLine = "SG";
                                    resultJsonSequence = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objSeq.GetSequenceFareListonResult(newFltSearch, "FDD", "")) + ",\"Provider\":\"FDD\"}";
                                    if (resultJsonSequence.Length > 100)
                                    {
                                        ResultList.Add(resultJsonSequence);
                                    }
                                }

                            }
                        }

                        //Thread thG8 = new Thread(() => ArrangeServiceCall("SG", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                        //thG8.Start();
                        //vlist.Add(thG8);
                        //vlist1.Add(DateTime.Now);
                        string resultJsonCache = "";
                        if (aircodeWithProvider.ToUpper().Contains("SG") && isCoupon == false)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "SG";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "SG")) + ",\"Provider\":\"SGNRML\"}";
                            //  }
                        }

                        if (aircodeWithProvider.ToUpper().Contains("SG") && isCoupon == true)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "SG";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "CPN", "SGCPN")) + ",\"Provider\":\"SGCPN\"}";
                            //  }
                        }

                        if (resultJsonCache.Length > 100)
                        {
                            ResultList.Add(resultJsonCache);
                        }
                        else
                        {

                            if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "SG")
                                cList.Add(Task.Run(() => ArrangeServiceCall("SG", aircodeWithProvider, searchParam, isCoupon, context)));
                        }

                    }
                    if ((aircodeWithProvider.ToUpper().Contains("6E") || aircodeWithProvider.ToUpper().Contains("E6CPN")) && callApi.Trim().ToUpper() == "6E")
                    {
                        //Thread thG8 = new Thread(() => ArrangeServiceCall("6E", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                        //thG8.Start();
                        //vlist.Add(thG8);
                        //vlist1.Add(DateTime.Now);
                        string resultJsonCache = "";
                        if (callApi.Trim().ToUpper() == "6E" && isCoupon == false)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "6E";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "6E")) + ",\"Provider\":\"6ENRML\"}";
                            //  }
                        }

                        if (callApi.Trim().ToUpper() == "6E" && isCoupon == true)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "6E";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "CPN", "6ECPN")) + ",\"Provider\":\"6ECPN\"}";
                            //  }
                        }



                        if (resultJsonCache.Length > 100)
                        {
                            ResultList.Add(resultJsonCache);
                        }
                        else
                        {
                            if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "6E")
                                cList.Add(Task.Run(() => ArrangeServiceCall("6E", aircodeWithProvider, searchParam, isCoupon, context)));
                        }

                    }

                    if ((aircodeWithProvider.ToUpper().Contains("G8") || aircodeWithProvider.ToUpper().Contains("8GCPN")) && callApi.Trim().ToUpper() != "6E")
                    {
                        //Thread thG8 = new Thread(() => ArrangeServiceCall("G8", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                        //thG8.Start();
                        //vlist.Add(thG8);
                        //vlist1.Add(DateTime.Now);
                        string resultJsonCache = "";
                        if (aircodeWithProvider.ToUpper().Contains("G8") && isCoupon == false)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "G8";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "G8")) + ",\"Provider\":\"G8NRML\"}";
                            //  }
                        }

                        if (aircodeWithProvider.ToUpper().Contains("8GCPN") && isCoupon == true)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "G8";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "CPN", "G8CPN")) + ",\"Provider\":\"G8CPN\"}";
                            //  }
                        }

                        if (resultJsonCache.Length > 100)
                        {
                            ResultList.Add(resultJsonCache);
                        }
                        else
                        {

                            if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "G8")
                                cList.Add(Task.Run(() => ArrangeServiceCall("G8", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                    }
                    if ((aircodeWithProvider.ToUpper().Contains("IX") || aircodeWithProvider.ToUpper().Contains("8GCPN")) && callApi.Trim().ToUpper() != "6E" && callApi.Trim().ToUpper() != "G8" && callApi.Trim().ToUpper() != "SG")
                    {
                        //Thread thG8 = new Thread(() => ArrangeServiceCall("G8", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                        //thG8.Start();
                        //vlist.Add(thG8);
                        //vlist1.Add(DateTime.Now);
                        string resultJsonCache = "";
                        if (aircodeWithProvider.ToUpper().Contains("IX") && isCoupon == false)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "IX";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "IX")) + ",\"Provider\":\"IXNRML\"}";
                            //  }
                        }

                        if (aircodeWithProvider.ToUpper().Contains("8GCPN") && isCoupon == true)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "IX";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "CPN", "IXCPN")) + ",\"Provider\":\"IXCPN\"}";
                            //  }
                        }

                        if (resultJsonCache.Length > 100)
                        {
                            ResultList.Add(resultJsonCache);
                        }
                        else
                        {

                            if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "IX")
                                cList.Add(Task.Run(() => ArrangeServiceCall("IX", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                    }
                    if ((aircodeWithProvider.ToUpper().Contains("AK")) && callApi.Trim().ToUpper() != "6E")
                    {
                        string resultJsonCache = "";
                        if (aircodeWithProvider.ToUpper().Contains("AK") && isCoupon == false)
                        {
                            //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                            //{
                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "AK";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "AK")) + ",\"Provider\":\"AKNRML\"}";
                            //  }
                        }

                        //if (aircodeWithProvider.ToUpper().Contains("AK") && isCoupon == true)
                        //{
                        //    //if (searchParam.TripType.ToString() != "RoundTrip")  // block for RoundTrip Search only cahce for oneways to 6E
                        //    //{
                        //    FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                        //    newFltSearch.HidTxtAirLine = "AK";
                        //    CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                        //    resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "CPN", "AKCPN")) + ",\"Provider\":\"AKCPN\"}";
                        //    //  }
                        //}

                        if (resultJsonCache.Length > 100)
                        {
                            ResultList.Add(resultJsonCache);
                        }
                        else
                        {
                            if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "AK")
                                cList.Add(Task.Run(() => ArrangeServiceCall("AK", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                    }
                    if ((aircodeWithProvider.ToUpper().Contains("OG")) && callApi.Trim().ToUpper() != "6E")
                    {
                        string resultJsonCache = "";
                        if (aircodeWithProvider.ToUpper().Contains("OG") && isCoupon == false)
                        {

                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "OG";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "OG")) + ",\"Provider\":\"OGNRML\"}";

                        }
                        if (resultJsonCache.Length > 100)
                        {
                            ResultList.Add(resultJsonCache);
                        }
                        else
                        {
                            if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "OG")
                                cList.Add(Task.Run(() => ArrangeServiceCall("OG", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                    }
                    if ((aircodeWithProvider.ToUpper().Contains("NT:NT")) && callApi.Trim().ToUpper() != "6E")
                    {
                        string resultJsonCache = "";
                        if (aircodeWithProvider.ToUpper().Contains("NT") && isCoupon == false)
                        {

                            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                            newFltSearch.HidTxtAirLine = "NT";
                            CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                            resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "NT")) + ",\"Provider\":\"NTNRML\"}";

                        }
                        if (resultJsonCache.Length > 100)
                        {
                            ResultList.Add(resultJsonCache);
                        }
                        else
                        {
                            if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == " NT")
                                cList.Add(Task.Run(() => ArrangeServiceCall("NT", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                    }
                }
                else
                {
                    if (searchParam.TripType == TripType.MultiCity)
                    {
                        cList.Add(Task.Run(() => ArrangeServiceCall(searchParam.HidTxtAirLine, "SPL:1G", searchParam, isCoupon, context)));

                    }
                    else
                    {

                        if (aircodeWithProvider.ToUpper().Contains("AI"))
                        {
                            //Thread thAI = new Thread(() => ArrangeServiceCall("AI", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                            //thAI.Start();
                            //vlist.Add(thAI);
                            //vlist1.Add(DateTime.Now);

                            string resultJsonCache = "";
                            if (aircodeWithProvider.ToUpper().Contains("AI") && isCoupon == false)
                            {
                                FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                newFltSearch.HidTxtAirLine = "AI";
                                CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                                resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "AI")) + ",\"Provider\":\"AINRML\"}";
                            }
                            if (resultJsonCache.Length > 100)
                            {
                                ResultList.Add(resultJsonCache);
                            }
                            else
                            {
                                if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "AI")
                                    cList.Add(Task.Run(() => ArrangeServiceCall("AI", aircodeWithProvider, searchParam, isCoupon, context)));
                            }

                        }
                        if (aircodeWithProvider.ToUpper().Contains("9W"))
                        {
                            //Thread th9W = new Thread(() => ArrangeServiceCall("9W", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                            //th9W.Start();
                            //vlist.Add(th9W);
                            //vlist1.Add(DateTime.Now);
                            string resultJsonCache = "";
                            if (aircodeWithProvider.ToUpper().Contains("9W") && isCoupon == false)
                            {
                                FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                newFltSearch.HidTxtAirLine = "9W";
                                CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                                resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "9W")) + ",\"Provider\":\"9WNRML\"}";
                            }
                            if (resultJsonCache.Length > 100)
                            {
                                ResultList.Add(resultJsonCache);
                            }
                            else
                            {
                                if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "9W")
                                    cList.Add(Task.Run(() => ArrangeServiceCall("9W", aircodeWithProvider, searchParam, isCoupon, context)));
                            }
                        }

                        if (aircodeWithProvider.ToUpper().Contains("UK"))
                        {
                            //Thread thUK = new Thread(() => ArrangeServiceCall("UK", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                            //thUK.Start();
                            //vlist.Add(thUK);
                            //vlist1.Add(DateTime.Now);
                            string resultJsonCache = "";
                            if (aircodeWithProvider.ToUpper().Contains("UK") && isCoupon == false)
                            {
                                FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                newFltSearch.HidTxtAirLine = "UK";
                                CacheFareUpdate chache = new CacheFareUpdate(Con.ConnectionString);
                                resultJsonCache = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(chache.GetCacheList(newFltSearch, "NRM", "UK")) + ",\"Provider\":\"UKNRML\"}";
                            }
                            if (resultJsonCache.Length > 100)
                            {
                                ResultList.Add(resultJsonCache);
                            }
                            else
                            {
                                if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "UK")
                                    cList.Add(Task.Run(() => ArrangeServiceCall("UK", aircodeWithProvider, searchParam, isCoupon, context)));
                            }
                        }
                    }
                }
                #endregion

            }
            else if (searchParam.Trip == Trip.I)
            {
                if (isLCC)
                {

                    if (aircodeWithProvider.ToUpper().Contains("SS") && callApi.Trim().ToUpper() != "6E")
                    {
                        int a = 0;
                        SequeneFare objSeq = new SequeneFare(Con.ConnectionString);
                        if (a == 0)
                        {
                            a = 1;
                            if (aircodeWithProvider.ToUpper().Contains("SS") && isCoupon == false)
                            {
                                string resultJsonSequence = "";
                                FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
                                newFltSearch.HidTxtAirLine = "";
                                resultJsonSequence = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objSeq.GetSequenceFareListonResult(newFltSearch, "FDD", "")) + ",\"Provider\":\"FDD\"}";
                                if (resultJsonSequence.Length > 100)
                                {
                                    ResultList.Add(resultJsonSequence);
                                }
                            }

                        }
                    }
                    if ((aircodeWithProvider.ToUpper().Contains("SG")) && callApi.Trim().ToUpper() != "6E")
                    {
                        //Thread thG8 = new Thread(() => ArrangeServiceCall("SG", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                        //thG8.Start();
                        //vlist.Add(thG8);
                        //vlist1.Add(DateTime.Now);

                        //if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "SG")
                        //    cList.Add(Task.Run(() => ArrangeServiceCall("SG", aircodeWithProvider, searchParam, isCoupon, context)));

                        if ((searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "SG") && isCoupon == false)
                        {
                            cList.Add(Task.Run(() => ArrangeServiceCall("SG", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                        if (aircodeWithProvider.ToUpper().Contains("GSCPN"))
                        {
                            cList.Add(Task.Run(() => ArrangeServiceCall("SG", aircodeWithProvider, searchParam, true, context)));
                        }                        
                    }
                    if (aircodeWithProvider.ToUpper().Contains("6E") && callApi.Trim().ToUpper() == "6E")
                    {
                        //Thread thG8 = new Thread(() => ArrangeServiceCall("6E", aircodeWithProvider, searchParam, isCoupon, context));// new Thread(new ThreadStart(SpicejetAvailability_R));
                        //thG8.Start();
                        //vlist.Add(thG8);
                        //vlist1.Add(DateTime.Now);
                        if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "6E")
                        {
                            cList.Add(Task.Run(() => ArrangeServiceCall("6E", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                        if (aircodeWithProvider.ToUpper().Contains("6ECPN"))
                        {
                            cList.Add(Task.Run(() => ArrangeServiceCall("6E", aircodeWithProvider, searchParam, true, context)));
                        }

                    }
                    if (aircodeWithProvider.ToUpper().Contains("G8") && callApi.Trim().ToUpper() != "6E")
                    {
                        if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "G8")
                        {
                            cList.Add(Task.Run(() => ArrangeServiceCall("G8", aircodeWithProvider, searchParam, isCoupon, context)));
                        }
                        if (aircodeWithProvider.ToUpper().Contains("8GCPN"))
                        {
                            cList.Add(Task.Run(() => ArrangeServiceCall("G8", aircodeWithProvider, searchParam, true, context)));
                        }
                    }
					  if (aircodeWithProvider.ToUpper().Contains("IX") && callApi.Trim().ToUpper() != "6E" && callApi.Trim().ToUpper() != "G8")
                    {
                        if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "IX")
                            cList.Add(Task.Run(() => ArrangeServiceCall("IX", aircodeWithProvider, searchParam, isCoupon, context)));
                    }
                    if ((aircodeWithProvider.ToUpper().Contains("AK")) && callApi.Trim().ToUpper() != "6E")
                    {
                        if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "AK")
                            cList.Add(Task.Run(() => ArrangeServiceCall("AK", aircodeWithProvider, searchParam, isCoupon, context)));
                    }
                    if ((aircodeWithProvider.ToUpper().Contains("G9")) && callApi.Trim().ToUpper() != "6E")
                    {
                        if (searchParam.HidTxtAirLine == "" || searchParam.HidTxtAirLine == "G9")
                            cList.Add(Task.Run(() => ArrangeServiceCall("G9", aircodeWithProvider, searchParam, isCoupon, context)));
                    }
                }
                else
                {
                    if (searchParam.HidTxtAirLine.ToUpper().Trim() != "6E" && searchParam.HidTxtAirLine.ToUpper().Trim() != "SG" && searchParam.HidTxtAirLine.ToUpper().Trim() != "G8" && searchParam.HidTxtAirLine.ToUpper().Trim() != "AK")
                    {
                        cList.Add(Task.Run(() => ArrangeServiceCall(searchParam.HidTxtAirLine, "SPL:1G", searchParam, isCoupon, context)));
                    }

                }

            }


            // Task.WhenAll(cList).Wait(300000);

            var continuation = Task.WhenAll(cList);
            try
            {
                continuation.Wait(300000);// testing for local host
               // continuation.Wait(80000);  // 80 sec or 1min 20 sec
               //continuation.Wait(50000);  // 50 sec  //Live- Current running
              //  continuation.Wait(40000);  // 40 sec 
            }
            catch (AggregateException)
            { }



            //int counter = 0;
            //while ((counter < vlist.Count))
            //{
            //    Thread TH = (Thread)vlist[counter];
            //    if ((TH.ThreadState == ThreadState.WaitSleepJoin))
            //    {
            //        TimeSpan DIFF = DateTime.Now.Subtract((DateTime)vlist1[counter]);
            //        if ((DIFF.Seconds > 45))
            //        {
            //            TH.Abort();
            //            counter += 1;
            //        }
            //    }
            //    else if ((TH.ThreadState == ThreadState.Stopped))
            //    {
            //        counter += 1;
            //    }
            //}





            return ResultList;
        }


        public async Task ArrangeServiceCall(string airCode, string aircodeWithProvider, FlightSearch searchParam, bool isCoupon, HttpContext context)
        {

            string[] sep = { airCode };

            if (aircodeWithProvider.Contains("SPL"))
            {
                sep = new string[] { "SPL" };
            }


            string[] sep1 = { "-" };
            string prov = aircodeWithProvider.ToUpper().Split(sep, StringSplitOptions.None)[1].Split(sep1, StringSplitOptions.None)[0];
            await CallSearch(searchParam, airCode, prov.Replace(":", ""), isCoupon, context);// new Thread(new ThreadStart(SpicejetAvailability_R));



        }



        public async Task CallSearch(FlightSearch searchParam, string aircode, string provider, bool isCoupon, HttpContext context)
        {
            string resultJson = "";
            FlightSearch newFltSearch = (FlightSearch)searchParam.Clone();
            newFltSearch.HidTxtAirLine = aircode;//aircode == "SPL" ? "" :
            await Task.Delay(0);
            try
            {
                if (isCoupon)
                { newFltSearch.Provider = aircode; }
                else
                {
                    newFltSearch.Provider = provider;
                }
                STD.Shared.IFlt objI = new STD.BAL.FltService();
                HttpContext.Current = context;
                //if (isCoupon)
                //{
                //    resultJson = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objI.FltSearchResultCoupon(newFltSearch)) + ",\"Provider\":\"" + aircode + "CPN\"}";
                //}
                //else
                //{
                    resultJson = "{ \"result\":" + Newtonsoft.Json.JsonConvert.SerializeObject(objI.FltSearchResult(newFltSearch)) + ",\"Provider\":\"" + aircode + "NRML\"}";
                //}

                try
                {
                    var date2 = DateTime.Now;
                    int seconds = Convert.ToInt32((date2 - now).TotalSeconds);

                    if (seconds >= 40)
                    {
                        FlightCommonDAL objFCDAL = new FlightCommonDAL(Con.ConnectionString);
                        objFCDAL.waitInsert(searchParam, seconds, isCoupon, aircode);

                    }
                }
                catch(Exception ex)
                {
                  ITZERRORLOG.ExecptionLogger.FileHandling("SearchWrapper.cs-CallSearch", "Error_001", ex, "waitInsert");
                }
                

            }
            catch (Exception ex) { }

            ResultList.Add(resultJson);
        }

    }
}
