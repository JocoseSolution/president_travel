﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.BAL;
using System.Xml;
using System.Xml.Linq;
using STD.Shared;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Collections;
using System.Data.SqlClient;
using System.Configuration;

namespace STD.BAL
{
    public class GALTransanctions
    {
        string pcc = "", url = "", Userid = "", Pwd = "";
        string strVc = "", strTrip = "", strProvider = "", crdType = "";
        private List<CredentialList> CrdList;
        private List<CredentialList> TktCrdList;
        private FlightSearch searchInputs;
        public string connectionString { get; set; }
        public List<FareRule> GetFareRule(string adtFareRule, string chdFareRule, string infFareRule, int noOfAdult, int noOfChild, int noOfInfant)
        {
            GetCredentials();
            string strResponse = "";
            FltRequest objFQ = new FltRequest(searchInputs, CrdList);
            FlightCommonBAL objFCB = new FlightCommonBAL();
            strResponse = objFCB.PostXml(url, objFQ.FQMD_Request(adtFareRule, chdFareRule, infFareRule, noOfAdult, noOfChild, noOfInfant, pcc), Userid, Pwd, "http://webservices.galileo.com/MultiSubmitXml");
            return GetFareRule(strResponse);

        }
        private List<FareRule> GetFareRule(string strResponse)
        {
            List<FareRule> FareRuleList = new List<FareRule>();
            try
            {
                string uniqueKey = "";
                string flag = "";
                string header = "";
                string message = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(strResponse);
                XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                int i = 1;
                var data = from item in xdo.Descendants("FareInfo").Elements("RulesData")
                           select new
                           {
                               rules = item.Element("RulesDataType"),
                               UniqueKey = item.Element("UniqueKey"),
                               RulesText = item.Element("RulesText")
                           };




                foreach (var p in data)
                {
                    FareRule objFR = new FareRule();

                    i = i + 1;

                    if (p.rules.Value == "F")
                    {

                        uniqueKey = p.UniqueKey.Value;
                        objFR.header = p.rules.Value;

                    }
                    if (flag != "" && flag != uniqueKey)
                    {
                        objFR.message = message;
                        FareRuleList.Add(objFR);
                        flag = p.UniqueKey.Value;
                    }
                    if (p.rules.Value == "T" && p.UniqueKey.Value == uniqueKey.ToString())
                    {
                        if (p.RulesText.Value.Contains("----"))
                            message = message + "<br/> ";
                        else
                            message = message + " " + p.RulesText.Value;

                        flag = p.UniqueKey.Value;
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return FareRuleList;
        }
        private void GetGalTicketingCrd()
        {
            TktCrdList = Data.GetGalTicketingCrd(connectionString, strProvider, strTrip, strVc, crdType);
        }

        private void GetCredentials()
        {
            //CrdList = Data.GetProviderCrd(connectionString);
            CrdList = Data.GetGalBookingCrd(connectionString, strProvider, strTrip, strVc, crdType);
            url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
            Userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
            Pwd = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
            pcc = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc;
        }

        /// <summary>
        /// Pnr creation process for 1G
        /// </summary>
        /// <param name="SlcFltDs">Flight Details</param>
        /// <param name="FltHdr">Flight Details</param>
        /// <param name="FltPaxDs">Pax details</param>
        /// <returns>hash table</returns>
        public Hashtable CreateGdsPnrGAL(DataSet SlcFltDs, DataSet FltHdr, DataSet FltPaxDs, DataSet TktDS)
        {
            string ErrorLogPath = ConfigurationManager.AppSettings["TBOSaveUrl"].ToString();
            strVc = FltHdr.Tables[0].Rows[0]["VC"].ToString().Trim();
            strTrip = FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim();
            strProvider = "1G";
            crdType = SlcFltDs.Tables[0].Rows[0]["RESULTTYPE"].ToString().Trim();

            string SSReq = "", SSRes = "", SEReq = "", SERes = "";
            string PNBF1Res = "", PNBF2Res = "", PNBF3Res = "", strToken = "";
            string PNBF1Req = "", PNBF2Req = "", PNBF3Req = "";
            string ADTPnr = "", CHDPnr = "", AirADTPnr = "", AirCHDPnr = "", QReq = "", QRes = "";
            Hashtable PnrHT = new Hashtable();
            string strCode = "";
            try
            {

                STD.DAL.FlightCommonDAL objFltCommonDal = new STD.DAL.FlightCommonDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                try
                {
                    strCode = objFltCommonDal.GetDealCodeFromDB(FltHdr.Tables[0].Rows[0]["OrderId"].ToString().Trim());
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CreateGdsPnrGAL)", "Error_003", ex, "GetDealCodeFromDB");
                }

                GetCredentials();
                DataTable FOPDt = new DataTable();
                FOPDt = Data.GetFOP(connectionString, FltHdr.Tables[0].Rows[0]["VC"].ToString().Trim(), FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim(), strProvider);

                FltRequest objFQ = new FltRequest(searchInputs, CrdList);
                int adt = 0, chd = 0, inf = 0, NoOfPnr = 0;
                bool WOCHD = false, InfWIDiffFBC = false, chkStatus = false;
                string AdtFBC = "", ChdFBC = "", InfFBC = "";
                string tktPCC = "", Qno = "", rtReq = "", rtRes = "";
                //if (FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim() == "D")
                //{
                try
                {
                    tktPCC = TktDS.Tables[0].Rows[0]["PCC"].ToString().Trim();
                    Qno = TktDS.Tables[0].Rows[0]["QNO"].ToString().Trim();
                }
                catch (Exception ex1)
                {
                    tktPCC = ""; Qno = "";
                    ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CreateGdsPnrGAL)", "Error_003", ex1, "CreateGdsPnrGAL");
                }
                //}
                bool PrivateFare = false;
                bool ChkBal = true;
                try
                {
                    string SearchingId = (Convert.ToString(SlcFltDs.Tables[0].Rows[0]["SearchId"]).ToUpper()).Trim();
                    string BookingId = Userid.ToUpper();//((from crd in ProviderList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                    string TicketingId = (Convert.ToString(TktDS.Tables[0].Rows[0]["USERID"]).ToUpper()).Trim();
                    if (SearchingId == TicketingId && TicketingId == BookingId && crdType == "CRP")
                    {
                        PrivateFare = true;
                    }
                    if (SearchingId == TicketingId && TicketingId != BookingId && crdType == "CRP")
                    {
                        ChkBal = false;
                    }

                }
                catch (Exception ex1)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CreateGdsPnrGAL)", "Error_003", ex1, "CreateGdsPnrGAL");
                }


                adt = int.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                chd = int.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                inf = int.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
                AdtFBC = SlcFltDs.Tables[0].Rows[0]["AdtRbd"].ToString().Trim();//AdtFarebasis
                ChdFBC = SlcFltDs.Tables[0].Rows[0]["ChdRbd"].ToString().Trim();//ChdFarebasis
                InfFBC = SlcFltDs.Tables[0].Rows[0]["InfRbd"].ToString().Trim();//InfFarebasis

                #region Check pnr count with ADT,CHD,INF
                if (chd > 0 || inf > 0)
                {
                    if (chd > 0)
                    {
                        #region pnr with CHD,INF
                        if (inf > 0)
                        {
                            if (AdtFBC == ChdFBC && AdtFBC == InfFBC)
                            {
                                //Single Pnr
                            }
                            else if ((AdtFBC != ChdFBC) && (AdtFBC == InfFBC))
                            {
                                WOCHD = true;
                                NoOfPnr = 1;
                                //Multiple Pnrs
                            }
                            else if ((AdtFBC != ChdFBC) && (AdtFBC != InfFBC))
                            {
                                WOCHD = true;
                                InfWIDiffFBC = true;
                                NoOfPnr = 1;
                                //multiple pnr with DiffFBC in case on Inf
                            }
                            else if ((AdtFBC == ChdFBC) && (AdtFBC != InfFBC))
                            {
                                InfWIDiffFBC = false;
                                //single pnr with DiffFBC in case on Inf
                            }
                        }
                        #endregion
                        #region pnr with CHD
                        else
                        {
                            if (AdtFBC == ChdFBC)
                            {
                                //Single Pnr 
                            }
                            else if ((AdtFBC != ChdFBC))
                            {
                                WOCHD = true;
                                //Multiple Pnrs
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region pnr with INF
                        if (inf > 0)
                        {
                            if (AdtFBC != InfFBC)
                            { InfWIDiffFBC = true; }
                            else if (AdtFBC == InfFBC)
                            { }
                        }
                        #endregion
                    }
                }
                #endregion

                #region Pnr Creation Process
                for (int i = 0; i <= NoOfPnr; i++)
                {
                    SSReq = objFQ.BeginSessionReq();
                    SSRes = PostXml(url, SSReq, Userid, Pwd, "http://webservices.galileo.com/BeginSession");
                    strToken = GetTokenFromSessionStratResponse(SSRes);
                    if (WOCHD)
                    {
                        if (i == 0)
                        {
                            //PNBF1Req = objFQ.PNRBFMgnt_CreatePnr(strToken, SlcFltDs, FltHdr, FltPaxDs, WOCHD, false, InfWIDiffFBC, FOPDt, strCode);
                            PNBF1Req = objFQ.PNRBFMgnt_CreatePnr(strToken, SlcFltDs, FltHdr, FltPaxDs, WOCHD, false, InfWIDiffFBC, FOPDt, strCode, PrivateFare);
                        }
                        else
                        {
                            //PNBF1Req = objFQ.PNRBFMgnt_CreatePnr(strToken, SlcFltDs, FltHdr, FltPaxDs, WOCHD, true, InfWIDiffFBC, FOPDt, strCode);
                            PNBF1Req = objFQ.PNRBFMgnt_CreatePnr(strToken, SlcFltDs, FltHdr, FltPaxDs, WOCHD, true, InfWIDiffFBC, FOPDt, strCode, PrivateFare);
                        }
                    }
                    else
                    {
                        // PNBF1Req = objFQ.PNRBFMgnt_CreatePnr(strToken, SlcFltDs, FltHdr, FltPaxDs, WOCHD, false, InfWIDiffFBC, FOPDt, strCode);
                        PNBF1Req = objFQ.PNRBFMgnt_CreatePnr(strToken, SlcFltDs, FltHdr, FltPaxDs, WOCHD, false, InfWIDiffFBC, FOPDt, strCode, PrivateFare);
                    }
                    PNBF1Res = PostXml(url, PNBF1Req, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                    chkStatus = CheckPnrCreationStatus(PNBF1Res, SlcFltDs, i, WOCHD, ChkBal);
                    if (chkStatus)
                    {
                        PNBF2Req = objFQ.PNRBFMgntWithET(strToken);
                        PNBF2Res = PostXml(url, PNBF2Req, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                        if (CheckForFareGuarantee(PNBF2Res))
                        {
                            PNBF3Req = objFQ.PNRBFMgntWithET(strToken);
                            PNBF3Res = PostXml(url, PNBF3Req, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                            if (i == 0)
                            {
                                ADTPnr = GetPnrFromResponse(PNBF3Res, out AirADTPnr);
                                if ((pcc != tktPCC) && (tktPCC != ""))
                                {
                                    //*** Wait for 5 sec*************

                                    {
                                        DateTime tm = DateTime.Now.AddSeconds(5);
                                        while (DateTime.Now < tm)
                                        {
                                            //------Do nothing-------- 
                                        }
                                    }
                                    //*** Wait for 5 sec*************
                                    //***Retrieve Pnr*****
                                    rtReq = objFQ.RetrivePnrReq(ADTPnr, strToken, false);
                                    rtRes = PostXml(url, rtReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                    //***End Retrieve
                                    //Queue the pnr in different PCC
                                    QReq = objFQ.QueuePnrReq(strToken, Qno, tktPCC);
                                    QRes = PostXml(url, QReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                    //
                                    var tval = "";

                                    try
                                    {
                                        string str = QRes.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                                                    .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                                                    .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                                                    .Replace("xmlns=\"http://webservices.galileo.com\"", "");
                                        XDocument xmlDoc = XDocument.Parse(str);

                                        if (xmlDoc.Descendants("SubmitXmlOnSessionResult").Descendants("EndTransactMessage").Any())
                                        {

                                            tval = xmlDoc.Descendants("SubmitXmlOnSessionResult").Descendants("EndTransactMessage").Elements("Text").Where(x => x.Value.ToString().Contains("SIMULTANEOUS CHANGES TO BOOKING FILE")).FirstOrDefault().Value;

                                            if (!string.IsNullOrEmpty(tval))
                                            {
                                                {
                                                    DateTime tm = DateTime.Now.AddSeconds(6);
                                                    while (DateTime.Now < tm)
                                                    {
                                                        //------Do nothing-------- 
                                                    }
                                                }
                                                //*** Wait for 5 sec*************
                                                //***Retrieve Pnr*****
                                                rtReq = objFQ.RetrivePnrReq(ADTPnr, strToken, false);
                                                rtRes = PostXml(url, rtReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                                //***End Retrieve
                                                //Queue the pnr in different PCC
                                                QReq = objFQ.QueuePnrReq(strToken, Qno, tktPCC);
                                                QRes = PostXml(url, QReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                                //

                                            }
                                        }
                                    }
                                    catch (Exception ex) {
                                        ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CreateGdsPnrGAL)", "Error_003", ex, "QueuePnrReq");
                                    }

                                }
                                else
                                {
                                    //Do nothing :ticketing with same id
                                }
                            }
                            else
                            { CHDPnr = GetPnrFromResponse(PNBF3Res, out AirCHDPnr); }

                        }
                        else if (CheckforGDSPNR(PNBF2Res))  // Code for new changes in GAL
                        {
                            if (i == 0)
                            {
                                ADTPnr = GetPnrFromResponse(PNBF2Res, out AirADTPnr);

                                if ((pcc != tktPCC) && (tktPCC != ""))
                                {
                                    //*** Wait for 5 sec*************

                                    {
                                        DateTime tm = DateTime.Now.AddSeconds(5);
                                        while (DateTime.Now < tm)
                                        {
                                            //------Do nothing-------- 
                                        }
                                    }
                                    //*** Wait for 5 sec*************
                                    //***Retrieve Pnr*****
                                    rtReq = objFQ.RetrivePnrReq(ADTPnr, strToken, false);
                                    rtRes = PostXml(url, rtReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                    //***End Retrieve
                                    //Queue the pnr in different PCC
                                    QReq = objFQ.QueuePnrReq(strToken, Qno, tktPCC);
                                    QRes = PostXml(url, QReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                    //
                                    var tval = "";

                                    try
                                    {
                                        string str = QRes.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                                                    .Replace("xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", "")
                                                    .Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "")
                                                    .Replace("xmlns=\"http://webservices.galileo.com\"", "");
                                        XDocument xmlDoc = XDocument.Parse(str);

                                        if (xmlDoc.Descendants("SubmitXmlOnSessionResult").Descendants("EndTransactMessage").Any())
                                        {

                                            tval = xmlDoc.Descendants("SubmitXmlOnSessionResult").Descendants("EndTransactMessage").Elements("Text").Where(x => x.Value.ToString().Contains("SIMULTANEOUS CHANGES TO BOOKING FILE")).FirstOrDefault().Value;

                                            if (!string.IsNullOrEmpty(tval))
                                            {
                                                {
                                                    DateTime tm = DateTime.Now.AddSeconds(6);
                                                    while (DateTime.Now < tm)
                                                    {
                                                        //------Do nothing-------- 
                                                    }
                                                }
                                                //*** Wait for 5 sec*************
                                                //***Retrieve Pnr*****
                                                rtReq = objFQ.RetrivePnrReq(ADTPnr, strToken, false);
                                                rtRes = PostXml(url, rtReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                                //***End Retrieve
                                                //Queue the pnr in different PCC
                                                QReq = objFQ.QueuePnrReq(strToken, Qno, tktPCC);
                                                QRes = PostXml(url, QReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                                                //

                                            }
                                        }
                                    }
                                    catch (Exception ex) {
                                        ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CreateGdsPnrGAL-CheckforGDSPNR)", "Error_003", ex, "CreateGdsPnrGAL");
                                    }

                                }
                                else
                                {
                                    //Do nothing :ticketing with same id
                                }
                            }

                        }
                        else
                        {
                            //create Failure Pnr
                            if (i == 0)
                            {
                                ADTPnr = Utility.GetRndm() + "-FQ";
                            }
                            else
                            { CHDPnr = Utility.GetRndm() + "-FQ"; }

                            #region write error log  for create Failure Pnr -1
                            try
                            {
                                // strVc,strTrip,crdType,SearchingId ,TicketingId ,BookingId ,PNBF1Res, SlcFltDs, i, WOCHD, ChkBal
                                //PNBF1Req, Userid, Pwd,  chkStatus
                                File.AppendAllText(ErrorLogPath + Convert.ToString(FltHdr.Tables[0].Rows[0]["OrderId"]) + "_" + ADTPnr + ".txt", System.DateTime.Now.ToString() +
                               Environment.NewLine + "OrderId: " + Convert.ToString(FltHdr.Tables[0].Rows[0]["OrderId"]) + Environment.NewLine + "ADTPnr: " + ADTPnr +
                               Environment.NewLine + "Trip: " + Convert.ToString(strTrip) + Environment.NewLine + "CHDPnr: " + Convert.ToString(CHDPnr) + Environment.NewLine + "Vc: " + Convert.ToString(strVc) +
                               Environment.NewLine + "Userid: " + Convert.ToString(Userid) + Environment.NewLine + "Pwd: " + Convert.ToString(Pwd) +
                               Environment.NewLine + "crdType: " + Convert.ToString(crdType) + Environment.NewLine + "chkStatus: " + Convert.ToString(chkStatus) + Environment.NewLine +
                               Environment.NewLine + "WOCHD: " + Convert.ToString(WOCHD) + Environment.NewLine + "ChkBal: " + Convert.ToString(ChkBal) + Environment.NewLine + Environment.NewLine + "1G_PNBF1Req:" + Environment.NewLine +
                               Environment.NewLine + PNBF1Req + Environment.NewLine + Environment.NewLine + Environment.NewLine + "PNBF1Res: " + Environment.NewLine + Environment.NewLine + Convert.ToString(PNBF1Res) + Environment.NewLine + Environment.NewLine + "PNBF2Req: " + Environment.NewLine + Environment.NewLine + Convert.ToString(PNBF2Req) + Environment.NewLine + Environment.NewLine + "PNBF2Res: " + Environment.NewLine + Environment.NewLine + Convert.ToString(PNBF2Res) + Environment.NewLine + Environment.NewLine);
                            }
                            catch (Exception ex)
                            {
                                ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CreateGdsPnrGAL)", "Error_003", ex, "CreateGdsPnrGAL");
                            }
                            #endregion
                        }
                    }
                    else
                    {
                        //create Failure Pnr
                        if (i == 0)
                        {
                            ADTPnr = Utility.GetRndm() + "-FQ";
                        }
                        else
                        { CHDPnr = Utility.GetRndm() + "-FQ"; }
                        #region write error log  for create Failure Pnr -2
                        try
                        {
                            // strVc,strTrip,crdType,SearchingId ,TicketingId ,BookingId ,PNBF1Res, SlcFltDs, i, WOCHD, ChkBal
                            //PNBF1Req, Userid, Pwd,  chkStatus
                            File.AppendAllText(ErrorLogPath + Convert.ToString(FltHdr.Tables[0].Rows[0]["OrderId"]) + "_" + ADTPnr + ".txt", System.DateTime.Now.ToString() +
                                Environment.NewLine + "OrderId: " + Convert.ToString(FltHdr.Tables[0].Rows[0]["OrderId"]) + Environment.NewLine + "ADTPnr: " + ADTPnr +
                                Environment.NewLine + "Trip: " + Convert.ToString(strTrip) + Environment.NewLine + "CHDPnr: " + Convert.ToString(CHDPnr) + Environment.NewLine + "Vc: " + Convert.ToString(strVc) +
                                Environment.NewLine + "Userid: " + Convert.ToString(Userid) + Environment.NewLine + "Pwd: " + Convert.ToString(Pwd) +
                                Environment.NewLine + "crdType: " + Convert.ToString(crdType) + Environment.NewLine + "chkStatus: " + Convert.ToString(chkStatus) + Environment.NewLine +
                                Environment.NewLine + "WOCHD: " + Convert.ToString(WOCHD) + Environment.NewLine + "ChkBal: " + Convert.ToString(ChkBal) + Environment.NewLine + Environment.NewLine + "1G_PNBF1Req:" + Environment.NewLine +
                                Environment.NewLine + PNBF1Req + Environment.NewLine + Environment.NewLine + Environment.NewLine + "PNBF1Res: " + Environment.NewLine + Environment.NewLine + Convert.ToString(PNBF1Res) + Environment.NewLine + Environment.NewLine);
                        }
                        catch (Exception ex)
                        {
                            ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CreateGdsPnrGAL)", "Error_003", ex, "CreateGdsPnrGAL");
                        }
                        #endregion
                    }
                    #endregion
                    SEReq = objFQ.EndSessionReq(strToken);
                    SERes = PostXml(url, SEReq, Userid, Pwd, "http://webservices.galileo.com/EndSession");
                    PnrHT.Add("ADTPNR", ADTPnr.ToString().Replace("'", ""));
                    PnrHT.Add("CHDPNR", CHDPnr.ToString().Replace("'", ""));
                    PnrHT.Add("ADTAIRPNR", AirADTPnr.ToString().Replace("'", ""));
                    PnrHT.Add("CHDAIRPNR", AirCHDPnr.ToString().Replace("'", ""));
                    PnrHT.Add("SSReq", SSReq.ToString().Replace("'", ""));
                    PnrHT.Add("SSRes", SSRes.ToString().Replace("'", ""));
                    PnrHT.Add("PNBF1Req", PNBF1Req.ToString().Replace("'", ""));
                    PnrHT.Add("PNBF1Res", PNBF1Res.ToString().Replace("'", ""));
                    PnrHT.Add("PNBF2Req", PNBF2Req.ToString().Replace("'", ""));
                    PnrHT.Add("PNBF2Res", PNBF2Res.ToString().Replace("'", ""));
                    PnrHT.Add("PNBF3Req", PNBF3Req.ToString().Replace("'", ""));
                    PnrHT.Add("PNBF3Res", PNBF3Res.ToString().Replace("'", ""));
                    PnrHT.Add("SEReq", SEReq.ToString().Replace("'", ""));
                    PnrHT.Add("SERes", SERes.ToString().Replace("'", ""));
                    PnrHT.Add("Other", rtReq.ToString().Replace("'", "") + rtRes.ToString().Replace("'", "") + QReq.ToString().Replace("'", "") + QRes.ToString().Replace("'", ""));
                }
            }
            catch (Exception ex)
            {
                if (ADTPnr.ToString().Replace("'", "").Trim() == "")
                    ADTPnr = Utility.GetRndm() + "-FQ";
                PnrHT.Add("ADTPNR", ADTPnr.ToString().Replace("'", ""));
                PnrHT.Add("CHDPNR", CHDPnr.ToString().Replace("'", ""));
                PnrHT.Add("ADTAIRPNR", AirADTPnr.ToString().Replace("'", ""));
                PnrHT.Add("CHDAIRPNR", AirCHDPnr.ToString().Replace("'", ""));
                PnrHT.Add("SSReq", SSReq.ToString().Replace("'", ""));
                PnrHT.Add("SSRes", SSRes.ToString().Replace("'", ""));
                PnrHT.Add("PNBF1Req", PNBF1Req.ToString().Replace("'", ""));
                PnrHT.Add("PNBF1Res", PNBF1Res.ToString().Replace("'", ""));
                PnrHT.Add("PNBF2Req", PNBF2Req.ToString().Replace("'", ""));
                PnrHT.Add("PNBF2Res", PNBF2Res.ToString().Replace("'", ""));
                PnrHT.Add("PNBF3Req", PNBF3Req.ToString().Replace("'", ""));
                PnrHT.Add("PNBF3Res", PNBF3Res.ToString().Replace("'", ""));
                PnrHT.Add("SEReq", SEReq.ToString().Replace("'", ""));
                PnrHT.Add("SERes", SERes.ToString().Replace("'", ""));
                PnrHT.Add("Other", ex.Message.ToString().Replace("'", "") + "...." + ex.StackTrace.ToString().Replace("'", ""));
            }

            return PnrHT;
        }

        private string GetTokenFromSessionStratResponse(string strRes)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strRes);
            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
            return xdo.Root.Value;
        }

        private bool CheckforGDSPNR(string response)
        {
            bool status = false;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(response);
                XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
                mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");
                XmlNode temp = xd.SelectSingleNode("//GenPNRInfo/RecLoc", mgr);
                if (temp != null)
                {
                    if (temp.InnerText.Trim().Length > 3)
                        status = true;
                    else
                        status = false;
                }
                else { status = false; }
            }
            catch (Exception ex)
            { status = false;
                ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CheckforGDSPNR)", "Error_003", ex, "CheckforGDSPNR");
            }



            return status;

        }
        private bool CheckPnrCreationStatus(string strRes, DataSet SlcFltDs, int Cnt, bool WOCHD, bool ChkBal)
        {
            string ErrorLogPath = ConfigurationManager.AppSettings["TBOSaveUrl"].ToString();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(strRes);
                XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);

                mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");

                XmlNode temp = xd.SelectSingleNode("//Status", mgr);
                XmlNode temp1 = xd.SelectSingleNode("//FareFiledOKInd", mgr);
                XmlNodeList temp2 = xd.SelectNodes("//GenQuoteDetails/TotAmt", mgr);
                XmlNodeList temp3 = xd.SelectNodes("//GenQuoteDetails/UniqueKey", mgr);
                XmlNodeList temp4 = xd.SelectNodes("//PsgrTypes/PICReq", mgr);
                string st = "", FiledInd = "";
                double TF = 0, TotFare = 0;
                st = temp.InnerText;
                FiledInd = temp1.InnerText;
                if (SlcFltDs.Tables[0].Rows[0]["Searchvalue"].ToString().Trim() == "PROMOFARE")
                {
                    for (int Q = 0; Q <= temp2.Count - 1; Q++)
                    {
                        if (Q == 0)
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
                        else if ((Q == 1) && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                        else if ((Q == 1) && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                        else if (Q == 2)
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                    }
                }
                else
                {
                    for (int Q = 0; Q <= temp2.Count - 1; Q++)
                    {
                        if ((temp3[Q].InnerText == "1") && (temp4[Q].InnerText == "ADT"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "1") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "INF"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "1") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "CNN"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "2") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "CNN"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "2") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "INF"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                        else if (temp3[Q].InnerText == "3" && (temp4[Q].InnerText == "INF"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));

                    }
                }
                //SMS charge calc
                double SMSChg = 0;
                try
                {
                    SMSChg = double.Parse(SlcFltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                    SMSChg = SMSChg + (double.Parse(SlcFltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                }
                catch (Exception ex)
                {
                    try
                    {
                        File.AppendAllText(ErrorLogPath+"EX1_" + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "OrderId: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + Environment.NewLine + "st: " + st + Environment.NewLine + "FiledInd: " + FiledInd + Environment.NewLine + "TF: " + Convert.ToString(TF) + Environment.NewLine + "TotFare: " + Convert.ToString(TotFare) + Environment.NewLine + "ChkBal: " + Convert.ToString(ChkBal) + Environment.NewLine + "PROMOFARE: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]) + Environment.NewLine + "Cnt: " + Convert.ToString(Cnt) + Environment.NewLine + "WOCHD: " + Convert.ToString(WOCHD) + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + strRes + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    }
                    catch (Exception ex1)
                    {
                    }
                    ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CheckPnrCreationStatus_SMSChg)", "Error_003", ex, "CheckPnrCreationStatus");
                }

                TF = TF + SMSChg;
                //SMS charge calc end

                if (WOCHD == false)
                {
                    TotFare = double.Parse(SlcFltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                    TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                    TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
                }
                else
                {
                    if (Cnt == 0)
                    {
                        TotFare = double.Parse(SlcFltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                        // TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim());
                        TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
                    }
                    else
                    { TotFare = double.Parse(SlcFltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()); }

                }

                if (((st.Trim() == "SS") || (st.Trim() == "HS")) && (FiledInd.Trim() == "Y") && ((TF <= TotFare) || ChkBal == false))
                {
                    #region Write Log  CheckPnrCreationStatus
                    //try
                    //{
                    //    File.AppendAllText(ErrorLogPath + "GDSPNR_TRUE_" + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "OrderId: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + Environment.NewLine + "st: " + st + Environment.NewLine + "FiledInd: " + FiledInd + Environment.NewLine + "TF: " + Convert.ToString(TF) + Environment.NewLine + "TotFare: " + Convert.ToString(TotFare) + Environment.NewLine + "ChkBal: " + Convert.ToString(ChkBal) + Environment.NewLine + "PROMOFARE: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]) + Environment.NewLine + "Cnt: " + Convert.ToString(Cnt) + Environment.NewLine + "WOCHD: " + Convert.ToString(WOCHD) + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + strRes + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    //}
                    //catch (Exception ex)
                    //{
                    //    ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CheckPnrCreationStatus)", "Error_003", ex, "CheckPnrCreationStatus");
                    //}
                    #endregion
                    return true;
                }
                else {
                    try
                    {
                        File.AppendAllText(ErrorLogPath + "GDSPNR_Status_" + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "OrderId: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + Environment.NewLine + Environment.NewLine + "st: " + st + Environment.NewLine + "FiledInd: " + FiledInd + Environment.NewLine + "TF: " + Convert.ToString(TF) + Environment.NewLine + "TotFare: " + Convert.ToString(TotFare) + Environment.NewLine + "ChkBal: " + Convert.ToString(ChkBal) + Environment.NewLine + "PROMOFARE: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]) + Environment.NewLine + "Cnt: " + Convert.ToString(Cnt) + Environment.NewLine + "WOCHD: " + Convert.ToString(WOCHD) + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + strRes + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                    }
                    catch (Exception ex)
                    {
                        ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CheckPnrCreationStatus)", "Error_003", ex, "CheckPnrCreationStatus");
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                try
                {
                    File.AppendAllText(ErrorLogPath + "EX2_" + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "OrderId: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Track_id"]) + Environment.NewLine + "ChkBal: " + Convert.ToString(ChkBal) + Environment.NewLine + "PROMOFARE: " + Convert.ToString(SlcFltDs.Tables[0].Rows[0]["Searchvalue"]) + Environment.NewLine + "Cnt: " + Convert.ToString(Cnt) + Environment.NewLine + "WOCHD: " + Convert.ToString(WOCHD) + Environment.NewLine + "1G_XML_RESPONSE:" + Environment.NewLine + strRes + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception exx)
                {
                }
                ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CheckPnrCreationStatus)", "Error_003", ex, "CheckPnrCreationStatus");
                return false;
            }
        }

        private bool CheckForFareGuarantee(string strRes)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(strRes);
                XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
                mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");
                XmlNode temp = xd.SelectSingleNode("//EndTransaction/EndTransactMessage/Text", mgr);
                if (temp != null)
                {
                    if (temp.InnerText.IndexOf("TO ENSURE FARE GUARANTEE - TICKET NOW") >= 0)
                        return true;
                    else
                        return false;
                }
                else { return false; }
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(CheckForFareGuarantee)", "Error_003", ex, "CheckForFareGuarantee");
                return false;                
            }
        }

        private string GetPnrFromResponse(string strRes, out string strAirPnr)
        {
            strAirPnr = "";
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(strRes);
                XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
                mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");
                XmlNode temp = xd.SelectSingleNode("//GenPNRInfo/RecLoc", mgr);
                XmlNode temp1 = xd.SelectSingleNode("//VndRecLocs/RecLocInfoAry/RecLocInfo/RecLoc", mgr);
                if (temp1 != null)
                    strAirPnr = temp1.InnerText;
                if (temp != null)
                {
                    return temp.InnerText;
                }
                else { return ""; }

            }
            catch (Exception ex)
            { return ""; }
        }
        /// <summary>
        /// 
        /// For Cross Check Fare
        /// </summary>
        /// <param name="FltDs"></param>
        /// <returns></returns>
        #region old FairAvailablity
        //public bool FairAvailablity(DataSet FltDs)
        //{
        //    try
        //    {
        //        List<FlightSearchResults> Fltlist = GetFltResultListFromDS(FltDs);


        //        double TF = 0, TotFare = 0, MrkUp = 0;
        //        if (FltDs.Tables[0].Rows[0]["Provider"].ToString() == "1G")
        //        {
        //            strVc = FltDs.Tables[0].Rows[0]["ValidatingCarrier"].ToString().Trim();
        //            strTrip = FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim();
        //            strProvider = "1G";
        //            GetCredentials();
        //            string strRequest = "";
        //            string strResponse = "";
        //            FltRequest objFQ = new FltRequest(searchInputs, CrdList);
        //            //------------------------------start
        //            int adt = 0, chd = 0, inf = 0, NoOfPnr = 0;
        //            bool WOCHD = false, InfWIDiffFBC = false, chkStatus = false;
        //            string AdtFBC = "", ChdFBC = "", InfFBC = "";
        //            adt = int.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //            chd = int.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
        //            inf = int.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
        //            AdtFBC = FltDs.Tables[0].Rows[0]["AdtRbd"].ToString().Trim();//AdtFarebasis
        //            ChdFBC = FltDs.Tables[0].Rows[0]["ChdRbd"].ToString().Trim();//ChdFarebasis
        //            InfFBC = FltDs.Tables[0].Rows[0]["InfRbd"].ToString().Trim();//InfFarebasis

        //            #region Check pnr count with ADT,CHD,INF
        //            if (chd > 0 || inf > 0)
        //            {
        //                if (chd > 0)
        //                {
        //                    #region pnr with CHD,INF
        //                    if (inf > 0)
        //                    {
        //                        if (AdtFBC == ChdFBC && AdtFBC == InfFBC)
        //                        {
        //                            //Single Pnr
        //                        }
        //                        else if ((AdtFBC != ChdFBC) && (AdtFBC == InfFBC))
        //                        {
        //                            WOCHD = true;
        //                            NoOfPnr = 1;
        //                            //Multiple Pnrs
        //                        }
        //                        else if ((AdtFBC != ChdFBC) && (AdtFBC != InfFBC))
        //                        {
        //                            WOCHD = true;
        //                            InfWIDiffFBC = true;
        //                            NoOfPnr = 1;
        //                            //multiple pnr with DiffFBC in case on Inf
        //                        }
        //                        else if ((AdtFBC == ChdFBC) && (AdtFBC != InfFBC))
        //                        {
        //                            InfWIDiffFBC = true;
        //                            //single pnr with DiffFBC in case on Inf
        //                        }
        //                    }
        //                    #endregion
        //                    #region pnr with CHD
        //                    else
        //                    {
        //                        if (AdtFBC == ChdFBC)
        //                        {
        //                            //Single Pnr 
        //                        }
        //                        else if ((AdtFBC != ChdFBC))
        //                        {
        //                            WOCHD = true;
        //                            //Multiple Pnrs
        //                        }
        //                    }
        //                    #endregion
        //                }
        //                else
        //                {
        //                    #region pnr with INF
        //                    if (inf > 0)
        //                    {
        //                        if (AdtFBC != InfFBC)
        //                        { InfWIDiffFBC = true; }
        //                        else if (AdtFBC == InfFBC)
        //                        { }
        //                    }
        //                    #endregion
        //                }
        //            }
        //            #endregion
        //            for (int i = 0; i <= NoOfPnr; i++)
        //            {
        //                if (WOCHD)
        //                {
        //                    if (i == 0)
        //                    {
        //                        strRequest = objFQ.GDS1G_PNRBFMgmtFareCrossCheck(FltDs, WOCHD, false, InfWIDiffFBC);
        //                    }
        //                    else
        //                    {
        //                        strRequest = objFQ.GDS1G_PNRBFMgmtFareCrossCheck(FltDs, WOCHD, true, InfWIDiffFBC);
        //                    }
        //                }
        //                else
        //                {
        //                    strRequest = objFQ.GDS1G_PNRBFMgmtFareCrossCheck(FltDs, WOCHD, false, InfWIDiffFBC);
        //                }
        //                FlightCommonBAL objFCB = new FlightCommonBAL();
        //                strResponse = objFCB.PostXml(url, strRequest, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");
        //                XmlDocument xd = new XmlDocument();
        //                xd.LoadXml(strResponse);
        //                XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
        //                XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
        //                mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");

        //                if (FltDs.Tables[0].Rows[0]["Searchvalue"].ToString() == "PROMOFARE")
        //                {
        //                    XmlNodeList temp2 = xd.SelectNodes("//TotAmt", mgr);
        //                    for (int Q = 0; Q <= temp2.Count - 1; Q++)
        //                    {
        //                        if (Q == 0)
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
        //                        else if ((Q == 1) && (double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //                        else if ((Q == 1) && (double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                        else if (Q == 2)
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                    }
        //                }
        //                else
        //                {
        //                    XmlNodeList temp2 = xd.SelectNodes("//TotAmt", mgr);
        //                    XmlNodeList temp3 = xd.SelectNodes("//GenQuoteDetails/UniqueKey", mgr);
        //                    XmlNodeList temp4 = xd.SelectNodes("//PsgrTypes/PICReq", mgr);

        //                    for (int Q = 0; Q <= temp2.Count - 1; Q++)
        //                    {
        //                        if ((temp3[Q].InnerText == "1") && (temp4[Q].InnerText == "ADT"))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
        //                        else if ((temp3[Q].InnerText == "1") && (double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "INF"))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                        else if ((temp3[Q].InnerText == "1") && (double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "CNN"))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //                        //else if (temp3[Q].InnerText == "2")
        //                        //    TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //                        else if ((temp3[Q].InnerText == "2") && (double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "CNN"))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //                        else if ((temp3[Q].InnerText == "2") && (double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "INF"))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                        else if ((temp3[Q].InnerText == "3") && (temp4[Q].InnerText == "INF"))
        //                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                    }
        //                }
        //            }

        //            //------------------------------------------end


        //            //SMS charge calc
        //            double SMSChg = 0;
        //            try
        //            {
        //                SMSChg = double.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                SMSChg = SMSChg + (double.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //            }
        //            catch
        //            { }
        //            TF = TF + SMSChg;
        //            //SMS charge calc end

        //            if (bool.Parse(FltDs.Tables[0].Rows[0]["IsCorp"].ToString().Trim()))
        //            {
        //                TotFare = double.Parse(FltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
        //                MrkUp = double.Parse(FltDs.Tables[0].Rows[0]["ADTAdminMrk"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                MrkUp += double.Parse(FltDs.Tables[0].Rows[0]["CHDAdminMrk"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
        //                TotFare = TotFare - MrkUp;
        //            }
        //            else
        //            {
        //                TotFare = double.Parse(FltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
        //            }

        //            if (TF == TotFare)
        //            { return true; }
        //            else { return false; }
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}
        #endregion

        public bool FairAvailablity(DataSet FltDs)
        {

            bool result = false;
            if (FltDs.Tables[0].Rows[0]["Provider"].ToString() == "1G")
            {
                strVc = FltDs.Tables[0].Rows[0]["ValidatingCarrier"].ToString().Trim();
                strTrip = FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim();
                strProvider = "1G";
                crdType = Convert.ToString(FltDs.Tables[0].Rows[0]["RESULTTYPE"]).Trim();
                GetCredentials();
                GetGalTicketingCrd();

                #region Check Id for Auto ticketing  with reprising in case of private fare in different ID
                List<CredentialList> BkgCrdList = null;
                if (TktCrdList.Count > 0 && Convert.ToString(FltDs.Tables[0].Rows[0]["SearchId"]).ToUpper() == TktCrdList[0].UserID.ToUpper() && TktCrdList[0].UserID.ToUpper() != CrdList[0].UserID.ToUpper())
                {
                    url = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                    Userid = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                    Pwd = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
                    pcc = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc;
                    BkgCrdList = TktCrdList;
                }
                else
                {
                    BkgCrdList = CrdList;
                }
                #endregion
               // File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "userid-" + Userid + Environment.NewLine);

                FlightCommonBAL objFCB = new FlightCommonBAL();
                FltRequest objFQ = new FltRequest(searchInputs, BkgCrdList);

                
                // FltRequest objFQ = new FltRequest(searchInputs, CrdList);

                //string strRequest = objFQ.FQFSSingleRequest(FltDs, "");
                string strRequest = objFQ.FQFSSingleRequestNew(FltDs, "");
                string strResponse = objFCB.PostXml(url, strRequest, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");

              //  File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "request-" + strRequest + Environment.NewLine);
              //  File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "respone-" + strResponse + Environment.NewLine);

                string sResponse = strResponse.Replace("xmlns=\"http://webservices.galileo.com\"", "").Replace("xmlns=\"http://ns.galileo.com\"", "");

                ///Insert Checkfarexml file
                try
                {
                    InsertXML(strRequest, strResponse, url, Userid, Pwd);
                }
                catch(Exception ex)
                { }


                XDocument xmlDoc = XDocument.Parse(sResponse);

                IEnumerable<XElement> FareInfoLIst = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("GenQuoteDetails")
                                                     select item;


                IEnumerable<XElement> PsgrTypeslist = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("PsgrTypes")
                                                      select item;

                // FareInfoLIst.ToList()[i].Element("PsgrTypes").Element("PICReq");

                decimal TotalAmt = 0;
                for (int i = 0; i < FareInfoLIst.Count(); i++)
                {
                    XElement FareInfo = FareInfoLIst.ElementAt(i);// FareInfoLIst.ElementAt(i).Element("GenQuoteDetails");

                    XElement PsgrTypes = PsgrTypeslist.ElementAt(i).Element("PICReq");


                    int TotDecPos = Convert.ToInt16(FareInfo.Element("TotDecPos").Value);
                    decimal totamt = TotDecPos != 0 ? Convert.ToDecimal(FareInfo.Element("TotAmt").Value) / (10 ^ TotDecPos) : Convert.ToDecimal(FareInfo.Element("TotAmt").Value);

                    if (PsgrTypes.Value.ToString().Trim().ToUpper() == "ADT")
                    {
                        totamt = totamt * decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                       

                    }
                    else if (PsgrTypes.Value.ToString().Trim().ToUpper() == "CNN")
                    {
                        totamt = totamt * decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                       

                    }
                    else if (PsgrTypes.Value.ToString().Trim().ToUpper() == "INF")
                    {
                        totamt = totamt * decimal.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());

                    }

                    TotalAmt += totamt;
                }


                decimal TotFare = decimal.Parse(FltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                TotFare += decimal.Parse(FltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                TotFare += decimal.Parse(FltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * decimal.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());

                decimal TT = 0;
                if( Convert.ToInt16(FltDs.Tables[0].Rows[0]["Child"]) > 0)
                {
                    TT = (decimal.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) /2) * (decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()) + decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                
                    //TT = decimal.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * (decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
                }
                else
                {
                   TT = decimal.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * (decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()) + decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                
                }
               
              

                //File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "TotFare/TT/TotalAmt" + TotFare + "/" + TT + "/" + TotalAmt + Environment.NewLine);

                if (TotalAmt == TotFare - TT)
                {
                    result = true;
                }
                else
                { result = false; }

            }
            else if (FltDs.Tables[0].Rows[0]["Provider"].ToString() == "YA")
            {
                Dictionary<string, string> Log = new Dictionary<string, string>();
                string exep = "";

                int adultCount = int.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                int childCount = int.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                int infantCount = int.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());

                DataRow[] FltO = FltDs.Tables[0].Select("TripType='O'", "counter ASC");
                DataRow[] FltR = FltDs.Tables[0].Select("TripType='R'", "counter ASC");
                string ODR = "";
                if (Convert.ToString(FltDs.Tables[0].Rows[FltDs.Tables[0].Rows.Count - 1]["TripType"]).Trim().ToUpper() == "R" && Convert.ToString(FltDs.Tables[0].Rows[0]["Trip"]).ToUpper().Trim() == "D")
                {
                    ODR = FltR[0]["Searchvalue"].ToString();
                }

                YAAirPrice objAirPrice = new YAAirPrice();
                string airPriceResp = objAirPrice.AirPrice(FltO[0]["Searchvalue"].ToString(), ODR, adultCount, childCount, infantCount, STD.BAL.MedthodSVCUrl.SvcURL, STD.BAL.MedthodSVCUrl.AirPriceMTHD, ref Log, ref exep, Convert.ToString(FltDs.Tables[0].Rows[0]["Track_Id"]), "", Convert.ToString(FltDs.Tables[0].Rows[0]["Trip"]).ToUpper().Trim());

                decimal TotPrice = Math.Floor(objAirPrice.ParseAirPrice(airPriceResp));

                decimal totfare = Math.Floor(Convert.ToDecimal(FltDs.Tables[0].Rows[0]["OriginalTF"]));

                if (totfare > 0 && Convert.ToDecimal(TotPrice) > 0 && totfare >= Convert.ToDecimal(TotPrice))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }


            }
            else
            {
                result = true;
            }

            return result;

        }

        public bool FairAvailablity1G(DataSet FltDs, ref string ResValue)
        {

            bool result = false;
            if (FltDs.Tables[0].Rows[0]["Provider"].ToString() == "1G")
            {
                strVc = FltDs.Tables[0].Rows[0]["ValidatingCarrier"].ToString().Trim();
                strTrip = FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim();
                strProvider = "1G";
                crdType = Convert.ToString(FltDs.Tables[0].Rows[0]["RESULTTYPE"]).Trim();
                GetCredentials();
                GetGalTicketingCrd();

                #region Check Id for Auto ticketing  with reprising in case of private fare in different ID
                List<CredentialList> BkgCrdList = null;
                if (TktCrdList.Count > 0 && Convert.ToString(FltDs.Tables[0].Rows[0]["SearchId"]).ToUpper() == TktCrdList[0].UserID.ToUpper() && TktCrdList[0].UserID.ToUpper() != CrdList[0].UserID.ToUpper())
                {
                    url = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                    Userid = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                    Pwd = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
                    pcc = ((from crd in TktCrdList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc;
                    BkgCrdList = TktCrdList;
                }
                else
                {
                    BkgCrdList = CrdList;
                }
                #endregion
                // File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "userid-" + Userid + Environment.NewLine);

                FlightCommonBAL objFCB = new FlightCommonBAL();
                FltRequest objFQ = new FltRequest(searchInputs, BkgCrdList);


                // FltRequest objFQ = new FltRequest(searchInputs, CrdList);

                string strRequest = objFQ.FQFSSingleRequest(FltDs, "");
                string strResponse = objFCB.PostXml(url, strRequest, Userid, Pwd, "http://webservices.galileo.com/SubmitXml");

                //  File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "request-" + strRequest + Environment.NewLine);
                //  File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "respone-" + strResponse + Environment.NewLine);

                string sResponse = strResponse.Replace("xmlns=\"http://webservices.galileo.com\"", "").Replace("xmlns=\"http://ns.galileo.com\"", "");
                ResValue = sResponse;
                XDocument xmlDoc = XDocument.Parse(sResponse);

                IEnumerable<XElement> FareInfoLIst = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("GenQuoteDetails")
                                                     select item;


                IEnumerable<XElement> PsgrTypeslist = from item in xmlDoc.Descendants("FareQuoteFlightSpecific_23").Descendants("FareInfo").Descendants("PsgrTypes")
                                                      select item;

                // FareInfoLIst.ToList()[i].Element("PsgrTypes").Element("PICReq");

                decimal TotalAmt = 0;
                for (int i = 0; i < FareInfoLIst.Count(); i++)
                {
                    XElement FareInfo = FareInfoLIst.ElementAt(i);// FareInfoLIst.ElementAt(i).Element("GenQuoteDetails");

                    XElement PsgrTypes = PsgrTypeslist.ElementAt(i).Element("PICReq");


                    int TotDecPos = Convert.ToInt16(FareInfo.Element("TotDecPos").Value);
                    decimal totamt = TotDecPos != 0 ? Convert.ToDecimal(FareInfo.Element("TotAmt").Value) / (10 ^ TotDecPos) : Convert.ToDecimal(FareInfo.Element("TotAmt").Value);

                    if (PsgrTypes.Value.ToString().Trim().ToUpper() == "ADT")
                    {
                        totamt = totamt * decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());


                    }
                    else if (PsgrTypes.Value.ToString().Trim().ToUpper() == "CNN")
                    {
                        totamt = totamt * decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());


                    }
                    else if (PsgrTypes.Value.ToString().Trim().ToUpper() == "INF")
                    {
                        totamt = totamt * decimal.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());

                    }

                    TotalAmt += totamt;
                }


                decimal TotFare = decimal.Parse(FltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                TotFare += decimal.Parse(FltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                TotFare += decimal.Parse(FltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * decimal.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());

                decimal TT = 0;
                if (Convert.ToInt16(FltDs.Tables[0].Rows[0]["Child"]) > 0)
                {
                    TT = (decimal.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) / 2) * (decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()) + decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));

                    //TT = decimal.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * (decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
                }
                else
                {
                    TT = decimal.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * (decimal.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()) + decimal.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));

                }



                //File.AppendAllText("C:\\\\CPN_SP\\\\IndiGoSpecialAsyncOpenSkies3" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", "TotFare/TT/TotalAmt" + TotFare + "/" + TT + "/" + TotalAmt + Environment.NewLine);

                if (TotalAmt == TotFare - TT)
                {
                    result = true;
                }
                else
                { result = false; }

            }
            else if (FltDs.Tables[0].Rows[0]["Provider"].ToString() == "YA")
            {
                Dictionary<string, string> Log = new Dictionary<string, string>();
                string exep = "";

                int adultCount = int.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                int childCount = int.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                int infantCount = int.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());

                DataRow[] FltO = FltDs.Tables[0].Select("TripType='O'", "counter ASC");
                DataRow[] FltR = FltDs.Tables[0].Select("TripType='R'", "counter ASC");
                string ODR = "";
                if (Convert.ToString(FltDs.Tables[0].Rows[FltDs.Tables[0].Rows.Count - 1]["TripType"]).Trim().ToUpper() == "R" && Convert.ToString(FltDs.Tables[0].Rows[0]["Trip"]).ToUpper().Trim() == "D")
                {
                    ODR = FltR[0]["Searchvalue"].ToString();
                }

                YAAirPrice objAirPrice = new YAAirPrice();
                string airPriceResp = objAirPrice.AirPrice(FltO[0]["Searchvalue"].ToString(), ODR, adultCount, childCount, infantCount, STD.BAL.MedthodSVCUrl.SvcURL, STD.BAL.MedthodSVCUrl.AirPriceMTHD, ref Log, ref exep, Convert.ToString(FltDs.Tables[0].Rows[0]["Track_Id"]), "", Convert.ToString(FltDs.Tables[0].Rows[0]["Trip"]).ToUpper().Trim());

                decimal TotPrice = Math.Floor(objAirPrice.ParseAirPrice(airPriceResp));

                decimal totfare = Math.Floor(Convert.ToDecimal(FltDs.Tables[0].Rows[0]["OriginalTF"]));

                if (totfare > 0 && Convert.ToDecimal(TotPrice) > 0 && totfare >= Convert.ToDecimal(TotPrice))
                {
                    result = true;
                }
                else
                {
                    result = false;
                }


            }
            else
            {
                result = true;
            }

            return result;

        }


        public List<FlightSearchResults> GetFltResultListFromDS(DataSet FltDs)
        {
            List<FlightSearchResults> resultList = new List<FlightSearchResults>();

            for (int i = 0; i < FltDs.Tables[0].Rows.Count; i++)
            {
                FlightSearchResults fsr = new FlightSearchResults();
                fsr.Leg = Convert.ToInt32(FltDs.Tables[0].Rows[i]["Leg"]);
                fsr.LineNumber = Convert.ToInt32(FltDs.Tables[0].Rows[i]["LineItemNumber"]);

                fsr.Flight = Convert.ToString(FltDs.Tables[0].Rows[i]["Flight"]); ;
                fsr.Stops = Convert.ToString(FltDs.Tables[0].Rows[i]["Stops"]);

                fsr.Adult = Convert.ToInt32(FltDs.Tables[0].Rows[i]["Adult"]);
                fsr.Child = Convert.ToInt32(FltDs.Tables[0].Rows[i]["Child"]);
                fsr.Infant = Convert.ToInt32(FltDs.Tables[0].Rows[i]["Infant"]);


                fsr.depdatelcc = Convert.ToString(FltDs.Tables[0].Rows[i]["depdatelcc"]);
                fsr.arrdatelcc = Convert.ToString(FltDs.Tables[0].Rows[i]["arrdatelcc"]);
                fsr.Departure_Date = Convert.ToString(FltDs.Tables[0].Rows[i]["Departure_Date"]);
                fsr.Arrival_Date = Convert.ToString(FltDs.Tables[0].Rows[i]["Arrival_Date"]);
                fsr.DepartureDate = Convert.ToString(FltDs.Tables[0].Rows[i]["DepartureDate"]);
                fsr.ArrivalDate = Convert.ToString(FltDs.Tables[0].Rows[i]["ArrivalDate"]);


                fsr.FlightIdentification = Convert.ToString(FltDs.Tables[0].Rows[i]["FlightIdentification"]);
                fsr.AirLineName = Convert.ToString(FltDs.Tables[0].Rows[i]["AirLineName"]);

                fsr.ValiDatingCarrier = Convert.ToString(FltDs.Tables[0].Rows[i]["ValiDatingCarrier"]);
                fsr.OperatingCarrier = Convert.ToString(FltDs.Tables[0].Rows[i]["OperatingCarrier"]);
                fsr.MarketingCarrier = Convert.ToString(FltDs.Tables[0].Rows[i]["MarketingCarrier"]);
                fsr.fareBasis = Convert.ToString(FltDs.Tables[0].Rows[i]["FareBasis"]);


                fsr.DepartureLocation = Convert.ToString(FltDs.Tables[0].Rows[i]["DepartureLocation"]);
                fsr.DepartureCityName = Convert.ToString(FltDs.Tables[0].Rows[i]["DepartureCityName"]);
                fsr.DepartureTime = Convert.ToString(FltDs.Tables[0].Rows[i]["DepartureTime"]);
                fsr.DepartureAirportName = Convert.ToString(FltDs.Tables[0].Rows[i]["DepAirportCode"]);
                fsr.DepartureTerminal = Convert.ToString(FltDs.Tables[0].Rows[i]["DepartureTerminal"]);
                fsr.DepAirportCode = Convert.ToString(FltDs.Tables[0].Rows[i]["DepAirportCode"]);

                fsr.ArrivalLocation = Convert.ToString(FltDs.Tables[0].Rows[i]["ArrivalLocation"]);
                fsr.ArrivalCityName = Convert.ToString(FltDs.Tables[0].Rows[i]["ArrivalCityName"]);
                fsr.ArrivalTime = Convert.ToString(FltDs.Tables[0].Rows[i]["ArrivalTime"]);
                fsr.ArrivalAirportName = Convert.ToString(FltDs.Tables[0].Rows[i]["ArrAirportCode"]);
                fsr.ArrivalTerminal = Convert.ToString(FltDs.Tables[0].Rows[i]["ArrivalTerminal"]);
                fsr.ArrAirportCode = Convert.ToString(FltDs.Tables[0].Rows[i]["ArrAirportCode"]);
                fsr.Trip = Convert.ToString(FltDs.Tables[0].Rows[i]["Trip"]);

                fsr.TotDur = Convert.ToString(FltDs.Tables[0].Rows[i]["Tot_Dur"]);


                fsr.AdtBfare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtBfare"]);
                fsr.AdtCabin = Convert.ToString(FltDs.Tables[0].Rows[i]["AdtCabin"]); ;
                fsr.AdtFSur = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtFSur"]);
                fsr.AdtTax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtTax"]);
                fsr.AdtOT = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtOT"]);
                fsr.AdtRbd = Convert.ToString(FltDs.Tables[0].Rows[i]["AdtRbd"]);
                fsr.AdtFarebasis = Convert.ToString(FltDs.Tables[0].Rows[i]["AdtFarebasis"]);
                fsr.AdtFareType = Convert.ToString(FltDs.Tables[0].Rows[i]["AdtFareType"]);
                // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                fsr.AdtFare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtFare"]);
                fsr.sno = Convert.ToString(FltDs.Tables[0].Rows[i]["sno"]);


                fsr.ChdBFare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdBfare"]);
                fsr.ChdCabin = Convert.ToString(FltDs.Tables[0].Rows[i]["ChdCabin"]); ;
                fsr.ChdFSur = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdFSur"]);
                fsr.ChdTax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdTax"]);
                fsr.ChdOT = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdOT"]);
                fsr.ChdRbd = Convert.ToString(FltDs.Tables[0].Rows[i]["ChdRbd"]);
                fsr.ChdFarebasis = Convert.ToString(FltDs.Tables[0].Rows[i]["ChdFarebasis"]);
                fsr.ChdFare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdFare"]);



                fsr.InfBfare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfBfare"]);
                fsr.InfCabin = Convert.ToString(FltDs.Tables[0].Rows[i]["InfCabin"]); ;
                fsr.InfFSur = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfFSur"]);
                fsr.InfTax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfTax"]);
                fsr.InfOT = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfOT"]);
                fsr.InfRbd = Convert.ToString(FltDs.Tables[0].Rows[i]["InfRbd"]);
                fsr.InfFarebasis = Convert.ToString(FltDs.Tables[0].Rows[i]["InfFarebasis"]);
                fsr.InfFare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfFare"]);



                fsr.Searchvalue = Convert.ToString(FltDs.Tables[0].Rows[i]["Searchvalue"]);
                fsr.OriginalTF = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["OriginalTF"]);
                fsr.Provider = Convert.ToString(FltDs.Tables[0].Rows[i]["Provider"]); ;

                fsr.RBD = Convert.ToString(FltDs.Tables[0].Rows[i]["RBD"]);
                fsr.TotalFare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["TotFare"]);
                fsr.TotalFuelSur = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["TotalFuelSur"]);
                fsr.TotalTax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["TotalTax"]);
                fsr.OriginalTT = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["OriginalTT"]);
                fsr.TotBfare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["TotalBfare"]);
                fsr.AvailableSeats = Convert.ToString(FltDs.Tables[0].Rows[i]["AvailableSeats"]);


                fsr.OrgDestFrom = Convert.ToString(FltDs.Tables[0].Rows[i]["OrgDestFrom"]);
                fsr.OrgDestTo = Convert.ToString(FltDs.Tables[0].Rows[i]["OrgDestTo"]);
                fsr.TripType = Convert.ToString(FltDs.Tables[0].Rows[i]["TripType"]);


                fsr.NetFare = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["NetFare"]);
                fsr.STax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["STax"]);
                fsr.TFee = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["TFee"]);
                fsr.AdtDiscount = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["DisCount"]);
                fsr.EQ = Convert.ToString(FltDs.Tables[0].Rows[i]["EQ"]);
                fsr.Sector = Convert.ToString(FltDs.Tables[0].Rows[i]["Sector"]);
                fsr.TripCnt = Convert.ToString(FltDs.Tables[0].Rows[i]["TripCnt"]);
                // fsr.Currency = Convert.ToString(FltDs.Tables[0].Rows[i]["Currency"]);
                fsr.ADTAdminMrk = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ADTAdminMrk"]);
                fsr.ADTAgentMrk = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ADTAgentMrk"]);
                fsr.CHDAdminMrk = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["CHDAdminMrk"]);
                fsr.CHDAgentMrk = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["CHDAgentMrk"]);
                fsr.InfAdminMrk = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfAdminMrk"]);
                fsr.InfAgentMrk = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfAgentMrk"]);

                fsr.IATAComm = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["IATAComm"]);
                fsr.ChdfareType = Convert.ToString(FltDs.Tables[0].Rows[i]["ChdFareType"]);
                fsr.InfFareType = Convert.ToString(FltDs.Tables[0].Rows[i]["InfFareType"]);
                fsr.FBPaxType = Convert.ToString(FltDs.Tables[0].Rows[i]["FBPaxType"]);
                // fsr.FlightStatus = Convert.ToString(FltDs.Tables[0].Rows[i]["FlightStatus"]);
                //fsr.Adt_Tax = Convert.ToString(FltDs.Tables[0].Rows[i]["Adt_Tax"]);
                fsr.AdtSrvTax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtSrvTax"]);
                // fsr.Chd_Tax = Convert.ToString(FltDs.Tables[0].Rows[i]["Chd_Tax"]);
                fsr.ChdSrvTax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdSrvTax"]);
                fsr.InfSrvTax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfSrvTax"]);
                fsr.STax = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["SrvTax"]);
                //fsr.TF = Convert.ToString(FltDs.Tables[0].Rows[i]["TF"]);
                //fsr.TC = Convert.ToString(FltDs.Tables[0].Rows[i]["TC"]);
                fsr.AdtTds = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtTds"]);


                fsr.ChdTds = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdTds"]);
                fsr.InfTds = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfTds"]);
                //fsr.AdtComm = Convert.ToString(FltDs.Tables[0].Rows[i]["AdtComm"]);
                //fsr.ChdComm = Convert.ToString(FltDs.Tables[0].Rows[i]["ChdComm"]);
                //fsr.InfComm = Convert.ToString(FltDs.Tables[0].Rows[i]["InfComm"]);
                fsr.AdtCB = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtCB"]);
                fsr.ChdCB = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdCB"]);
                fsr.InfCB = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfCB"]);
                fsr.ElectronicTicketing = Convert.ToString(FltDs.Tables[0].Rows[i]["ElectronicTicketing"]);
                fsr.AdtMgtFee = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtMgtFee"]);
                fsr.ChdMgtFee = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdMgtFee"]);

                fsr.InfMgtFee = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["InfMgtFee"]);
                fsr.TotMgtFee = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["TotMgtFee"]);
                fsr.IsCorp = Convert.ToBoolean(FltDs.Tables[0].Rows[i]["IsCorp"]);
                //fsr.AdtComm1 = Convert.ToString(FltDs.Tables[0].Rows[i]["AdtComm1"]);
                //fsr.ChdComm1 = Convert.ToString(FltDs.Tables[0].Rows[i]["ChdComm1"]);
                fsr.AdtSrvTax1 = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["AdtSrvTax1"]);
                fsr.ChdSrvTax1 = (float)Convert.ToDouble(FltDs.Tables[0].Rows[i]["ChdSrvTax1"]);

                resultList.Add(fsr);





            }


            return resultList;
        }

        //public bool FairAvailablity(DataSet FltDs)
        //{
        //    try
        //    {
        //        double TF = 0, TotFare = 0, MrkUp = 0;
        //        if (FltDs.Tables[0].Rows[0]["Provider"].ToString() == "1G")
        //        {
        //            strVc = FltDs.Tables[0].Rows[0]["ValidatingCarrier"].ToString().Trim();
        //            strTrip = FltDs.Tables[0].Rows[0]["Trip"].ToString().Trim();
        //            strProvider = "1G";
        //            GetCredentials();
        //            string strResponse = "";
        //            FltRequest objFQ = new FltRequest(searchInputs, CrdList);
        //            FlightCommonBAL objFCB = new FlightCommonBAL();
        //            strResponse = objFCB.PostXml(url, objFQ.GDS1G_PNRBFMgmtFareCrossCheck(FltDs), Userid, Pwd, "http://webservices.galileo.com/SubmitXml");

        //            XmlDocument xd = new XmlDocument();
        //            xd.LoadXml(strResponse);
        //            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
        //            XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);
        //            mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");

        //            if (FltDs.Tables[0].Rows[0]["Searchvalue"].ToString() == "PROMOFARE")
        //            {
        //                XmlNodeList temp2 = xd.SelectNodes("//TotAmt", mgr);
        //                for (int Q = 0; Q <= temp2.Count - 1; Q++)
        //                {
        //                    if (Q == 0)
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
        //                    else if ((Q == 1) && (double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0))
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //                    else if ((Q == 1) && (double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0))
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                    else if (Q == 2)
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                }
        //            }
        //            else
        //            {
        //                XmlNodeList temp2 = xd.SelectNodes("//TotAmt", mgr);
        //                XmlNodeList temp3 = xd.SelectNodes("//GenQuoteDetails/UniqueKey", mgr);

        //                for (int Q = 0; Q <= temp2.Count - 1; Q++)
        //                {
        //                    if (temp3[Q].InnerText == "1")
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
        //                    //else if (temp3[Q].InnerText == "2")
        //                    //    TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //                    else if ((temp3[Q].InnerText == "2") && (double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0))
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //                    else if ((temp3[Q].InnerText == "2") && (double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0))
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
        //                    else if (temp3[Q].InnerText == "3")
        //                        TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));

        //                }
        //            }
        //            //SMS charge calc
        //            double SMSChg = 0;
        //            try
        //            {
        //                SMSChg = double.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                SMSChg = SMSChg + (double.Parse(FltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
        //            }
        //            catch
        //            { }
        //            TF = TF + SMSChg;
        //            //SMS charge calc end

        //            if (bool.Parse(FltDs.Tables[0].Rows[0]["IsCorp"].ToString().Trim()))
        //            {
        //                TotFare = double.Parse(FltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
        //                MrkUp = double.Parse(FltDs.Tables[0].Rows[0]["ADTAdminMrk"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                MrkUp += double.Parse(FltDs.Tables[0].Rows[0]["CHDAdminMrk"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
        //                TotFare = TotFare - MrkUp;
        //            }
        //            else
        //            {
        //                TotFare = double.Parse(FltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
        //                TotFare += double.Parse(FltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(FltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
        //            }

        //            if (TF == TotFare)
        //            { return true; }
        //            else { return false; }
        //        }
        //        else
        //        {
        //            return true;
        //        }
        //    }
        //    catch
        //    {
        //        return false;
        //    }
        //}

        /// <summary>
        /// Online Ticketing
        /// </summary>
        /// <param name="Pnr"></param>
        /// <param name="CrdDs"></param>
        ///
        public void InsertXML(string Request,string Response,string url,string userID,string Pass)
        {
            SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            string Value = "";
            try
            {
                SqlCommand cmd = new SqlCommand("Sp_InsertCrossfarexml", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Request",Request);
                cmd.Parameters.AddWithValue("@Response",Response);
                cmd.Parameters.AddWithValue("@url",url);
                cmd.Parameters.AddWithValue("@userID",userID);
                cmd.Parameters.AddWithValue("@Pass",Pass);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch (Exception ex)
            {
             
            }
        }

        public Hashtable GetTicketNumberUsingOLTKT(string Pnr, DataSet CrdDs, string Orderid, DataSet SlcFltDs, DataSet FltHdr, DataSet FltPaxDs)
        {
            string SSReq = "", SSRes = "", strToken, AirPnr = "";
            string PNBF1Req = "", PNBF2Req = "", TktReq = "", SEReq = "";
            string PNBF1Res = "", PNBF2Res = "", TktRes = "", SERes = "";



            string PNRBF1Req = "", PNRBF2Req = "", PNRBF3Req = "", PNRBF4Req = "";
            string PNRBF1Res = "", PNRBF2Res = "", PNRBF3Res = "", PNRBF4Res = "";
            strVc = FltHdr.Tables[0].Rows[0]["VC"].ToString().Trim();
            strTrip = FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim();
            strProvider = "1G";
            crdType = SlcFltDs.Tables[0].Rows[0]["RESULTTYPE"].ToString().Trim();
            connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString;
            GetCredentials();

            bool PNRReprice = false;
            try
            {
                string SearchingId = (Convert.ToString(SlcFltDs.Tables[0].Rows[0]["SearchId"]).ToUpper()).Trim();
                string BookingId = (((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID).ToUpper().Trim();
                string TicketingId = (Convert.ToString(CrdDs.Tables[0].Rows[0]["USERID"]).ToUpper()).Trim();
                string BkgPcc = (((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].CarrierAcc).ToUpper().Trim();
                if (SearchingId == TicketingId && TicketingId != BookingId && crdType == "CRP")
                {
                    PNRReprice = true;
                }
            }
            catch (Exception ex1)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(GetTicketNumberUsingOLTKT)", "Error_004", ex1, "GetTicketNumberUsingOLTKT");
            }

            string STTReq = "", STTRes = "";
            ArrayList TktNoArray = new ArrayList();
            CredentialList objCrd = new CredentialList();
            Hashtable TKTHT = new Hashtable();
            bool WOCHD = false, InfWIDiffFBC = false, chkStatus = false;
            string strCode = "";
            int DocProdCnt = 1, PrcCnt = 0;
            try
            {
                objCrd.UserID = Convert.ToString(CrdDs.Tables[0].Rows[0]["USERID"]).Trim();
                objCrd.CorporateID = Convert.ToString(CrdDs.Tables[0].Rows[0]["HAP"]).Trim();
                objCrd.Password = Convert.ToString(CrdDs.Tables[0].Rows[0]["PWD"]).Trim();
                objCrd.AvailabilityURL = Convert.ToString(CrdDs.Tables[0].Rows[0]["URL"]).Trim();
                objCrd.BookingURL = Convert.ToString(CrdDs.Tables[0].Rows[0]["URL"]).Trim();
                objCrd.CarrierAcc = Convert.ToString(CrdDs.Tables[0].Rows[0]["PCC"]).Trim();
                objCrd.Provider = "1G";
                CrdList = new List<CredentialList>();
                CrdList.Add(objCrd);
                url = Convert.ToString(CrdDs.Tables[0].Rows[0]["URL"]).Trim();
                Userid = Convert.ToString(CrdDs.Tables[0].Rows[0]["USERID"]).Trim();
                Pwd = Convert.ToString(CrdDs.Tables[0].Rows[0]["PWD"]).Trim();
                pcc = Convert.ToString(CrdDs.Tables[0].Rows[0]["PCC"]).Trim();
                //strCode = Convert.ToString(CrdDs.Tables[0].Rows[0]["strCode"]).Trim();



                STD.DAL.FlightCommonDAL objFltCommonDal = new STD.DAL.FlightCommonDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                try
                {
                    strCode = objFltCommonDal.GetDealCodeFromDB(Orderid);
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(GetTicketNumberUsingOLTKT)", "Error_004", ex, "GetDealCodeFromDB");
                }
                try
                {
                    DocProdCnt = int.Parse(objFltCommonDal.GetTicketingInfoFromDB(Orderid));
                }
                catch (Exception ex)
                {
                    ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(GetTicketNumberUsingOLTKT)", "Error_004", ex, "GetTicketingInfoFromDB");
                }
                bool tktst;

                FltRequest objFQ = new FltRequest(searchInputs, CrdList);
            lbl2:
                SSReq = objFQ.BeginSessionReq();
                SSRes = PostXml(url, SSReq, Userid, Pwd, "http://webservices.galileo.com/BeginSession");
                strToken = GetTokenFromSessionStratResponse(SSRes);
                PNBF1Req = objFQ.RetrivePnrReq(Pnr, strToken, true);
                PNBF1Res = PostXml(url, PNBF1Req, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                bool flag = false;
                bool ChkRepriceStatus = true;
                #region Repricing and ticketing in case of diffirent BkgId and TktId and corporate fare in ticketing   (16-11-2017)
                if (PNRReprice)
                {
                    ChkRepriceStatus = false;
                    PNRBF1Req = objFQ.FareQuoteCancelModsReq(strToken);
                    PNRBF1Res = PostXml(url, PNRBF1Req, Userid, Pwd, "http://webservices.galileo.com/BeginSession");

                    PNRBF2Req = objFQ.PNRBFManagementSavePnrReq("Agent", strToken);
                    PNRBF2Res = PostXml(url, PNRBF2Req, Userid, Pwd, "http://webservices.galileo.com/BeginSession");

                    //Check for Simaltaneous changes
                    if (PNRBF2Res.Contains("SIMULTANEOUS CHANGES") == false)
                    {
                        PNRBF3Req = objFQ.PNRBFManagementRepricePnrReq(SlcFltDs, FltHdr, FltPaxDs, CrdDs, WOCHD, false, InfWIDiffFBC, strCode, PNRReprice, strToken);
                        PNRBF3Res = PostXml(url, PNRBF3Req, Userid, Pwd, "http://webservices.galileo.com/BeginSession");

                        ChkRepriceStatus = CheckTktRepriceStatus(PNRBF3Res, SlcFltDs, 0, WOCHD, true);

                        PNRBF4Req = objFQ.PNRBFManagementSaveFareInPnrReq("Agent", strToken);
                        PNRBF4Res = PostXml(url, PNRBF4Req, Userid, Pwd, "http://webservices.galileo.com/BeginSession");
                        if (PNRBF4Res.Contains("SIMULTANEOUS CHANGES") == false)
                        {
                            PNBF1Req = objFQ.RetrivePnrReq(Pnr, strToken, true);
                            PNBF1Res = PostXml(url, PNBF1Req, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                        }
                        else { flag = true; PrcCnt++; System.Threading.Thread.Sleep(3000); goto lbl1; }
                    }
                    else { flag = true; PrcCnt++; System.Threading.Thread.Sleep(3000); goto lbl1; }
                }
                #endregion

                tktst = ChechTicketingStatus(PNBF1Res, out AirPnr);
                //if (tktst) 
                if (tktst == true && ChkRepriceStatus == true)
                {
                    //for (int tk = 1; tk <= DocProdCnt; tk++)
                    //{
                    //    TktReq = objFQ.DocProdFareManipulationReq(strToken, strCode, tk.ToString());
                    //    TktRes = PostXml(url, TktReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                    //}
                    if (DocProdCnt == 1)
                    {
                        TktReq = objFQ.DocProdFareManipulationReq(strToken, strCode, "1");
                        TktRes = PostXml(url, TktReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                    }
                    else
                    {
                        TktReq = objFQ.TerminalTransaction(strToken, "TKPDTD");
                        TktRes = PostXml(url, TktReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                    }
                    if (TktRes.IndexOf("ELECTRONIC TKT GENERATED") >= 0)
                    {                        
                        //if (pcc.ToString().ToUpper().Trim() != "57RS")
                        if (pcc.ToString().ToUpper().Trim() != Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["GALTRNSPCC"]))
                        {
                            STTReq = objFQ.TerminalTransaction(strToken, "Q/" + Convert.ToString(CrdDs.Tables[0].Rows[0]["QNO"]).Trim() + "+*RL-" + Pnr + "");
                            STTRes = PostXml(url, STTReq, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");

                            PNBF2Req = objFQ.RetriveCurrentPnrReq(strToken);
                            PNBF2Res = PostXml(url, PNBF2Req, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                        }
                        else
                        {
                            PNBF2Req = objFQ.RetrivePnrReq(Pnr, strToken, false);
                            PNBF2Res = PostXml(url, PNBF2Req, Userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                        }

                        #region Get Ticket number from xml with pax
                        try
                        {
                            XmlDocument xd = new XmlDocument();
                            xd.LoadXml(PNBF2Res);
                            XmlNode xxd = xd.SelectSingleNode("//PNRBFRetrieve");
                            XmlNodeList xt = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSRText");                            
                            XmlNodeList xt1 = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSR");           // Get Ticket No of Adult Child and Infant- Devesh(09-04-2018)
                           //XmlNodeList xt1 = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSR[starts-with(SSRCode,'TKNE')]");  // Comment line because not getting Infant Ticket No, Get Infant Name in TicketNo-Devesh(09-04-2018)

                            #region Add if condition:Getting Meal in Response
                            if (xt1.Count != xt.Count && xt1.Count > xt.Count)
                            {
                                xt1 = xd.SelectNodes("//PNRBFRetrieve/ProgramaticSSR[starts-with(SSRCode,'TKNE')]");
                            }
                            #endregion
                            int aa = xt.Count - 1;
                            for (int ii = 0; ii <= xt1.Count - 1; ii++)
                            {
                                XmlNode xt2 = xt1[ii];
                                xt1[ii].AppendChild(xt[ii]);
                                xxd.ReplaceChild(xt2, xt1[ii]);
                            }
                            int i = 0;
                            XDocument xdo = XDocument.Load(new XmlNodeReader(xxd));

                            var tkt = from t in xdo.Descendants("PNRBFRetrieve").Descendants("ProgramaticSSR")
                                      from p in xdo.Descendants("PNRBFRetrieve").Descendants("FNameInfo")
                                      from l in xdo.Descendants("PNRBFRetrieve").Descendants("LNameInfo")
                                      where p.Element("AbsNameNum").Value == t.Element("AppliesToAry").Element("AppliesTo").Element("AbsNameNum").Value &&
                                      l.Element("LNameNum").Value == t.Element("AppliesToAry").Element("AppliesTo").Element("AbsNameNum").Value &&
                                      t.Element("Status").Value == "HK"
                                      select new
                                      {
                                          indx = i,
                                          fn = p.Element("FName").Value,
                                          ln = l.Element("LName").Value,
                                          tk = t.Element("ProgramaticSSRText").Element("Text").Value,
                                          abs = t.Element("AppliesToAry").Element("AppliesTo").Element("AbsNameNum").Value

                                      };

                            foreach (var a in tkt)
                            {
                                i++;
                                string[] tkt1 = Utility.Split(a.fn, " ");
                                string nm = "", tkno = "";
                                for (int n = tkt1.Length - 1; n >= 0; n--)
                                {
                                    nm = nm + tkt1[n].ToString();
                                }
                                tkno = Utility.Left(Utility.Left(a.tk, 13), 3) + "-" + Utility.Right(Utility.Left(a.tk, 13), 10);
                               // TktNoArray.Add(nm + a.ln + "/" + tkno);
                                TktNoArray.Add(nm + a.ln + "/" + tkno + "/" + (a.fn + a.ln).Replace(" ", ""));
                            }
                        }
                        catch (Exception ex1)
                        {
                            ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(GetTicketNumberUsingOLTKT)", "Error_004", ex1, "GetTicketNumberUsingOLTKT");
                            TktNoArray.Add("AirLine is not able to make ETicket");
                        }
                        #endregion
                    }
                    else
                    {
                        TktNoArray.Add("AirLine is not able to make ETicket");
                    }
                }
                else
                {
                    TktNoArray.Add("AirLine is not able to make ETicket");
                }
            lbl1:
                SEReq = objFQ.EndSessionReq(strToken);
                SERes = PostXml(url, SEReq, Userid, Pwd, "http://webservices.galileo.com/EndSession");
                if (flag == true && PrcCnt <= 2) goto lbl2;
                if (ChkRepriceStatus == false && TktNoArray.Count == 0) TktNoArray.Add("AirLine is not able to make ETicket");
                TKTHT.Add("SSReq", SSReq);
                TKTHT.Add("SSRes", SSRes);
                TKTHT.Add("PNRRTReq", PNBF1Req);
                TKTHT.Add("PNRRTRes", PNBF1Res);
                TKTHT.Add("DOCPRDReq", TktReq);
                TKTHT.Add("DOCPRDRes", TktRes);
                TKTHT.Add("PNRRT2Req", STTReq + "<br/>" + PNBF2Req);
                TKTHT.Add("PNRRT2Res", STTRes + "<br/>" + PNBF2Res);
                TKTHT.Add("SEReq", SEReq);
                TKTHT.Add("SERes", SERes);
                TKTHT.Add("TktNoArray", TktNoArray);
                TKTHT.Add("AirPnr", AirPnr);

                #region Reprice Request Response Log
                TKTHT.Add("PNRBF1Req", PNRBF1Req);
                TKTHT.Add("PNRBF1Res", PNRBF1Res);
                TKTHT.Add("PNRBF2Req", PNRBF2Req);
                TKTHT.Add("PNRBF2Res", PNRBF2Res);
                TKTHT.Add("PNRBF3Req", PNRBF3Req);
                TKTHT.Add("PNRBF3Res", PNRBF3Res);
                TKTHT.Add("PNRBF4Req", PNRBF4Req);
                TKTHT.Add("PNRBF4Res", PNRBF4Res);
                #endregion Reprice Log
            }
            catch (Exception ex)
            {
                ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(GetTicketNumberUsingOLTKT)", "Error_004", ex, "GetTicketNumberUsingOLTKT");
                TktNoArray.Add("AirLine is not able to make ETicket");
                TKTHT.Add("SSReq", SSReq);
                TKTHT.Add("SSRes", SSRes);
                TKTHT.Add("PNRRTReq", PNBF1Req);
                TKTHT.Add("PNRRTRes", PNBF1Res);
                TKTHT.Add("DOCPRDReq", TktReq);
                TKTHT.Add("DOCPRDRes", TktRes);
                TKTHT.Add("PNRRT2Req", PNBF2Req);
                TKTHT.Add("PNRRT2Res", PNBF2Res);
                TKTHT.Add("SEReq", SEReq);
                TKTHT.Add("SERes", ex.Message.ToString().Replace("'", ""));
                TKTHT.Add("TktNoArray", TktNoArray);
                TKTHT.Add("AirPnr", AirPnr);

                #region Reprice Request Response Log
                TKTHT.Add("PNRBF1Req", PNRBF1Req);
                TKTHT.Add("PNRBF1Res", PNRBF1Res);
                TKTHT.Add("PNRBF2Req", PNRBF2Req);
                TKTHT.Add("PNRBF2Res", PNRBF2Res);
                TKTHT.Add("PNRBF3Req", PNRBF3Req);
                TKTHT.Add("PNRBF3Res", PNRBF3Res);
                TKTHT.Add("PNRBF4Req", PNRBF4Req);
                TKTHT.Add("PNRBF4Res", PNRBF4Res);
                #endregion Reprice Log
            }
            return TKTHT;
        }
        private bool ChechTicketingStatus(string strRes, out string AirPnr)
        {
            bool st = false, QType = false;
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strRes);
            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
            XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);

            mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");
            AirPnr = "";
            try
            {
                XmlNodeList temp = xd.SelectNodes("//AirSeg/Status", mgr);
                XmlNode temp1 = xd.SelectSingleNode("//VndRecLocs/RecLocInfoAry/RecLocInfo/RecLoc", mgr);
                XmlNodeList temp2 = xd.SelectNodes("//GenQuoteDetails/QuoteType", mgr);
                for (int i = 0; i <= temp.Count - 1; i++)
                {
                    if (temp[i].InnerText.Trim() == "HK")
                        st = true;
                    else
                        st = false;
                }

                for (int i = 0; i <= temp2.Count - 1; i++)
                {
                    if (temp2[i].InnerText.Trim() == "A" || temp2[i].InnerText.Trim() == "G" || temp2[i].InnerText.Trim() == "P")
                        QType = true;
                    else
                        QType = false;
                }
                AirPnr = temp1.InnerText;

                if (st == true && QType == true && AirPnr != "")
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        /// <summary>
        /// Http post method for comunicating with web service using SOAP.
        /// </summary>
        /// <param name="url">service url</param>
        /// <param name="xml">request xml</param>
        /// <param name="Userid">User id</param>
        /// <param name="Pwd">Password</param>
        /// <param name="SOAPAction">SOAP Action</param>
        /// <returns>response xml</returns>
        public string PostXml(string url, string xml, string Userid, string Pwd, string SOAPAction)
        {
            StringBuilder sbResult = new StringBuilder();
            try
            {
                HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(url);
                if (!string.IsNullOrEmpty(xml))
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip");
                    Http.Method = "POST";
                    byte[] lbPostBuffer = Encoding.UTF8.GetBytes(xml);
                    Http.ContentLength = lbPostBuffer.Length;
                    Http.ContentType = @"text/xml";
                    Http.KeepAlive = false;
                    // Http.Credentials = new NetworkCredential(Userid, Pwd);
                    string auth = Userid + ":" + Pwd;
                    auth = Convert.ToBase64String(Encoding.Default.GetBytes(auth));
                    Http.Headers["Authorization"] = "Basic " + auth;
                    Http.Headers.Add(String.Format("SOAPAction: \"{0}\"", SOAPAction));
                    // Http.PreAuthenticate = true;
                    Http.AllowAutoRedirect = false;

                   
                    //Http.KeepAlive = true;
                    //Http.Credentials = new NetworkCredential(Userid, Pwd);
                    //Http.Headers.Add(String.Format("SOAPAction: \"{0}\"", SOAPAction));
                    //Http.PreAuthenticate = true;

                    //using (Stream PostStream = new GZipStream(Http.GetRequestStream(), CompressionMode.Compress, true))


                    using (Stream PostStream = Http.GetRequestStream())
                    {
                        PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                    }
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        //else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        //{
                        //    responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        //}
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        responseStream.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                string ErrorLogPath = ConfigurationManager.AppSettings["TBOSaveUrl"].ToString();
                try
                {
                   // string url, string xml, string Userid, string Pwd, string SOAPAction                    
                    File.AppendAllText(ErrorLogPath + "GALTran_PostXml_Exception_" + System.DateTime.Now.Date.ToString("ddMMyyyyhhhmmsstt") + ".txt", System.DateTime.Now.ToString() + Environment.NewLine + "url: " + Convert.ToString(url) + Environment.NewLine + "Userid: " + Userid + Environment.NewLine + "Pwd: " + Pwd + Environment.NewLine + "SOAPAction: " + Convert.ToString(SOAPAction) + Environment.NewLine + "ExceptionMsg: " + Convert.ToString(ex.StackTrace.ToString() + ex.Message) + Environment.NewLine + Environment.NewLine + "xml: " + Environment.NewLine + Convert.ToString(xml) + Environment.NewLine  + Environment.NewLine + Environment.NewLine + Environment.NewLine);
                }
                catch (Exception ex1)
                {
                }
                ITZERRORLOG.ExecptionLogger.FileHandling("GALTransactions(PostXml)", "Error_003", ex, "PostXml");

                sbResult.Append("");

            }
            return sbResult.ToString();
        }
        private bool CheckTktRepriceStatus(string strRes, DataSet SlcFltDs, int Cnt, bool WOCHD, bool ChkBal)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(strRes);
                XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
                XmlNamespaceManager mgr = new XmlNamespaceManager(xd.NameTable);

                mgr.AddNamespace("SubmitXmlOnSessionResponse", "http://webservices.galileo.com");

                //XmlNode temp = xd.SelectSingleNode("//Status", mgr);
                XmlNode temp1 = xd.SelectSingleNode("//FareFiledOKInd", mgr);
                XmlNodeList temp2 = xd.SelectNodes("//GenQuoteDetails/TotAmt", mgr);
                XmlNodeList temp3 = xd.SelectNodes("//GenQuoteDetails/UniqueKey", mgr);
                XmlNodeList temp4 = xd.SelectNodes("//PsgrTypes/PICReq", mgr);
                //string st = "";
                string FiledInd = "";
                double TF = 0, TotFare = 0;
                //st = temp.InnerText;
                FiledInd = temp1.InnerText;
                if (SlcFltDs.Tables[0].Rows[0]["Searchvalue"].ToString().Trim() == "PROMOFARE")
                {
                    for (int Q = 0; Q <= temp2.Count - 1; Q++)
                    {
                        if (Q == 0)
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
                        else if ((Q == 1) && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                        else if ((Q == 1) && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                        else if (Q == 2)
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                    }
                }
                else
                {
                    for (int Q = 0; Q <= temp2.Count - 1; Q++)
                    {
                        if ((temp3[Q].InnerText == "1") && (temp4[Q].InnerText == "ADT"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "1") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "INF"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "1") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "CNN"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "2") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "CNN"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                        else if ((temp3[Q].InnerText == "2") && (double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()) > 0) && (temp4[Q].InnerText == "INF"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));
                        else if (temp3[Q].InnerText == "3" && (temp4[Q].InnerText == "INF"))
                            TF = TF + (double.Parse(temp2[Q].InnerText) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim()));

                    }
                }
                //SMS charge calc
                double SMSChg = 0;
                try
                {
                    SMSChg = double.Parse(SlcFltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                    SMSChg = SMSChg + (double.Parse(SlcFltDs.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()));
                }
                catch
                { }

                TF = TF + SMSChg;
                //SMS charge calc end

                if (WOCHD == false)
                {
                    TotFare = double.Parse(SlcFltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                    TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim());
                    TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
                }
                else
                {
                    if (Cnt == 0)
                    {
                        TotFare = double.Parse(SlcFltDs.Tables[0].Rows[0]["AdtFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Adult"].ToString().Trim());
                        // TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim());
                        TotFare += double.Parse(SlcFltDs.Tables[0].Rows[0]["InfFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Infant"].ToString().Trim());
                    }
                    else
                    { TotFare = double.Parse(SlcFltDs.Tables[0].Rows[0]["ChdFare"].ToString().Trim()) * double.Parse(SlcFltDs.Tables[0].Rows[0]["Child"].ToString().Trim()); }

                }
                // if (((st.Trim() == "SS") || (st.Trim() == "HS")) && (FiledInd.Trim() == "Y") && ((TF <= TotFare) || ChkBal == false)) // old line
                if ((FiledInd.Trim() == "Y") && (TF <= TotFare))
                { return true; }
                else { return false; }
            }
            catch
            {
                return false;
            }
        }

    }

}
namespace STD.Shared
{
    public class FareRule
    {
        public int index { get; set; }
        public string header { get; set; }
        public string message { get; set; }
    }
}

