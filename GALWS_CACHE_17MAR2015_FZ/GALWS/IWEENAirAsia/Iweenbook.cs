﻿using STD.BAL;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GALWS.IWEENAirAsia
{
    public class Iweenbook
    {
        string UserName;
        string Password;
        string ServiceUrl;
        string TargetCode;
        string SecurityGUID;


        public Iweenbook()
        {
            //UserName = username;
            //Password = password;
            //ServiceUrl = serviceUrl;
            //TargetCode = targetCode;
            //SecurityGUID = securityGUID;
        }
        public string BookingRequest(DataTable FltDT, DataSet PaxDs, string VC, DataSet Crd, DataSet FltHdrs, ref ArrayList TktNoArray, string Constr, ref string bookingID, ref string airlinpnr)
        {
            string strRes = "";
            Dictionary<string, string> Log = new Dictionary<string, string>();
            string PNR = "";
            string Status = "";
            string exep = "";
            string Bkgid = "";
            string trip = "";


            try
            {
                DataSet objDs = new DataSet();
                DataTable dt = new DataTable();
                dt = FltDT.Copy();
                dt.TableName = "New Name";
                objDs.Tables.Add(dt);

                DataRow[] ADTPax = PaxDs.Tables[0].Select("PaxType='ADT'", "PaxId ASC");
                DataRow[] CHDPax = PaxDs.Tables[0].Select("PaxType='CHD'", "PaxId ASC");
                DataRow[] INFPax = PaxDs.Tables[0].Select("PaxType='INF'", "PaxId ASC");
                string[] sno = Convert.ToString(FltDT.Rows[0]["sno"]).Split(':');
                #region FareQuotel
                IweenPricing objAirPrice = new IweenPricing();



                #endregion
                #region FareQuote
                //DataSet dsCrd = Crd;
                trip = Convert.ToString(FltHdrs.Tables[0].Rows[0]["Trip"]).ToUpper();
                int totalpax = ADTPax.Count() + CHDPax.Count() + INFPax.Count();
                ArrayList airPriceResp = new ArrayList();
                ArrayList FinalairPriceResp = new ArrayList();

                string SellReferenceId = "";
                string statusmessage = "";
                string statuscode = "";
                airPriceResp = objAirPrice.GetFareQuote(Crd, objDs, ref Log, ref exep, Convert.ToString(FltDT.Rows[0]["SearchId"]), ref statusmessage);
                statuscode = statusmessage.Split(':')[0];
                //float TotPrice = (float)Math.Ceiling((Convert.ToDecimal(airPriceResp[0])));

                float totfare = (float)Math.Ceiling((Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"])));
                //decimal TotPrice = Math.Ceiling(Convert.ToDecimal(airPriceResp[0]));
                //float TotPrice = float.Parse(Convert.ToString(airPriceResp[0]));


                //decimal totfare = Convert.ToDecimal(FltDT.Rows[0]["OriginalTF"]);
                //decimal totfare = Convert.ToDecimal(FltDT.Compute("SUM(OriginalTF)", string.Empty));

                if (totfare > 0 && statuscode != "200")
                {
                    goto FareMisMatch;
                }
                else
                {
                    #region
                    StringBuilder LowFareSearchReq = new StringBuilder();
                    LowFareSearchReq.Append("<?xml version='1.0' encoding='UTF-8'?>");
                    LowFareSearchReq.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mer='http://mercuryws.nextra.com/'>");
                    LowFareSearchReq.Append("<soapenv:Header/>");
                    LowFareSearchReq.Append("<soapenv:Body>");
                    LowFareSearchReq.Append("<mer:BookFlight>");
                    LowFareSearchReq.Append("<bookingrequest>");
                    LowFareSearchReq.Append("<taxInfoStr>" + sno[0] + "</taxInfoStr>");
                    LowFareSearchReq.Append("<promocode />");
                    LowFareSearchReq.Append("<promovalue />");
                    LowFareSearchReq.Append("<leademail>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgEmail"]) + "</leademail>");
                    LowFareSearchReq.Append("<leadmobile>" + Convert.ToString(FltHdrs.Tables[0].Rows[0]["PgMobile"]) + "</leadmobile>");
                    LowFareSearchReq.Append("<leadcountry>IN</leadcountry>");
                    LowFareSearchReq.Append("<leadcity>ghaziabad</leadcity>");
                    LowFareSearchReq.Append("<leadstate>MZ</leadstate>");
                    LowFareSearchReq.Append("<paymentmode>cashcard</paymentmode>");
                    LowFareSearchReq.Append("<totalinvoice />");
                    if (trip == "D")
                        LowFareSearchReq.Append("<domint>domestic</domint>");
                    else
                        LowFareSearchReq.Append("<domint>international</domint>");
                    LowFareSearchReq.Append("<credentials>");
                    //LowFareSearchReq.Append("<agentid>DM24614</agentid>");
                    //LowFareSearchReq.Append("<officeid>DM24614</officeid>");
                    //LowFareSearchReq.Append("<password>Test@321</password>");
                    //LowFareSearchReq.Append("<username>testcustomer</username>");
                    LowFareSearchReq.Append("<agentid>AAA16445</agentid>");
                    LowFareSearchReq.Append("<officeid>AAA16445</officeid>");
                    LowFareSearchReq.Append("<username>RICHA</username>");
                    LowFareSearchReq.Append("<password>1-TRVL!9102!~</password>");
                    LowFareSearchReq.Append("<timestamp>" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "</timestamp>");//2019-04-18 10:35:00.688
                    LowFareSearchReq.Append("</credentials>");
                    LowFareSearchReq.Append("<numadults>" + ADTPax.Count() + "</numadults>");
                    LowFareSearchReq.Append("<numchildren>" + CHDPax.Count() + "</numchildren>");
                    LowFareSearchReq.Append("<numinfants>" + INFPax.Count() + "</numinfants>");
                    

                    #region Adult
                    if (ADTPax.Count() > 0)
                    {
                        for (int kd = 0; kd < ADTPax.Count(); kd++)
                        {
                            LowFareSearchReq.Append("<passengerList>");
                            //LowFareSearchReq.Append("<PassengerRecord>");
                            LowFareSearchReq.Append("<title>" + Convert.ToString(ADTPax[kd]["Title"]) + "</title>");
                            LowFareSearchReq.Append("<first_name>" + Convert.ToString(ADTPax[kd]["FName"]) + "</first_name>");
                            LowFareSearchReq.Append("<last_name>" + Convert.ToString(ADTPax[kd]["LName"]) + "</last_name>");
                            LowFareSearchReq.Append("<dob>" + Convert.ToString(ADTPax[kd]["DOB"].ToString().Split('/')[2] + "-" + ADTPax[kd]["DOB"].ToString().Split('/')[1] + "-" + ADTPax[kd]["DOB"].ToString().Split('/')[0]) + "</dob>");
                            LowFareSearchReq.Append("<type>Adult</type>");
                            LowFareSearchReq.Append("<tour_code />");
                            LowFareSearchReq.Append("<deal_code />");
                            LowFareSearchReq.Append("<frequent_flyer_number />");
                            LowFareSearchReq.Append("<visa />");
                            LowFareSearchReq.Append("<agentid>AAA16445</agentid>");
                            LowFareSearchReq.Append("<meal_preference />");
                            LowFareSearchReq.Append("<seat_preference />");
                            LowFareSearchReq.Append("<additional_segmentinfo />");
                            LowFareSearchReq.Append("<paxssrinfo />");
                            LowFareSearchReq.Append("<profileid />");
                            //LowFareSearchReq.Append("</PassengerRecord>");
                            LowFareSearchReq.Append("</passengerList>");
                        }
                    }
                    #endregion
                    #region Child
                    if (CHDPax.Count() > 0)
                    {
                        for (int cd = 0; cd < CHDPax.Count(); cd++)
                        {
                            LowFareSearchReq.Append("<passengerList>");
                            //LowFareSearchReq.Append("<PassengerRecord>");
                            LowFareSearchReq.Append("<title>" + Convert.ToString(CHDPax[cd]["Title"]) + "</title>");
                            LowFareSearchReq.Append("<first_name>" + Convert.ToString(CHDPax[cd]["FName"]) + "</first_name>");
                            LowFareSearchReq.Append("<last_name>" + Convert.ToString(CHDPax[cd]["LName"]) + "</last_name>");
                            LowFareSearchReq.Append("<dob>" + Convert.ToString(CHDPax[cd]["DOB"].ToString().Split('/')[2] + "-" + CHDPax[cd]["DOB"].ToString().Split('/')[1] + "-" + CHDPax[cd]["DOB"].ToString().Split('/')[0]) + "</dob>");
                            LowFareSearchReq.Append("<type>Child</type>");
                            LowFareSearchReq.Append("<tour_code />");
                            LowFareSearchReq.Append("<deal_code />");
                            LowFareSearchReq.Append("<frequent_flyer_number />");
                            LowFareSearchReq.Append("<visa />");
                            LowFareSearchReq.Append("<agentid>AAA16445</agentid>");
                            LowFareSearchReq.Append("<meal_preference />");
                            LowFareSearchReq.Append("<seat_preference />");
                            LowFareSearchReq.Append("<additional_segmentinfo />");
                            LowFareSearchReq.Append("<paxssrinfo />");
                            LowFareSearchReq.Append("<profileid />");
                            //LowFareSearchReq.Append("</PassengerRecord>");
                            LowFareSearchReq.Append("</passengerList>");
                        }
                    }
                    #endregion
                    #region Infant
                    if (INFPax.Count() > 0)
                    {
                        for (int ifd = 0; ifd < INFPax.Count(); ifd++)
                        {
                            LowFareSearchReq.Append("<passengerList>");
                            //LowFareSearchReq.Append("<PassengerRecord>");
                            LowFareSearchReq.Append("<title>" + Convert.ToString(INFPax[ifd]["Title"]) + "</title>");
                            LowFareSearchReq.Append("<first_name>" + Convert.ToString(INFPax[ifd]["FName"]) + "</first_name>");
                            LowFareSearchReq.Append("<last_name>" + Convert.ToString(INFPax[ifd]["LName"]) + "</last_name>");
                            LowFareSearchReq.Append("<dob>" + Convert.ToString(INFPax[ifd]["DOB"].ToString().Split('/')[2] + "-" + INFPax[ifd]["DOB"].ToString().Split('/')[1] + "-" + INFPax[ifd]["DOB"].ToString().Split('/')[0]) + "</dob>");
                            LowFareSearchReq.Append("<type>Infant</type>");
                            LowFareSearchReq.Append("<tour_code />");
                            LowFareSearchReq.Append("<deal_code />");
                            LowFareSearchReq.Append("<frequent_flyer_number />");
                            LowFareSearchReq.Append("<visa />");
                            LowFareSearchReq.Append("<agentid>AAA16445</agentid>");
                            LowFareSearchReq.Append("<meal_preference />");
                            LowFareSearchReq.Append("<seat_preference />");
                            LowFareSearchReq.Append("<additional_segmentinfo />");
                            LowFareSearchReq.Append("<paxssrinfo />");
                            LowFareSearchReq.Append("<profileid />");
                            //LowFareSearchReq.Append("</PassengerRecord>");
                            LowFareSearchReq.Append("</passengerList>");
                        }
                    }
                        #endregion
                    
                    
                    LowFareSearchReq.Append("<tripid />");
                    LowFareSearchReq.Append("<wsmode>LIVE</wsmode>");
                    LowFareSearchReq.Append("</bookingrequest>");
                    LowFareSearchReq.Append("</mer:BookFlight>");
                    LowFareSearchReq.Append("</soapenv:Body>");
                    LowFareSearchReq.Append("</soapenv:Envelope>");

                    Log.Add("BookReq", LowFareSearchReq.ToString());

                    Log.Add("BookResp", strRes);
                    string randomkey = Utility.GetRndm();
                    //strRes = IweenAirAsiaAuth.GetResponse("http://modemov2.iweensoft.com:80/flights/flights?wsdl", LowFareSearchReq.ToString());
                    IweenAirAsiaAuth.SaveXml(LowFareSearchReq.ToString(), SecurityGUID, "Book_Req" + randomkey);
                    strRes = IweenAirAsiaAuth.GetResponse("http://www.airasiaapi.com:8080/flights/flights?wsdl", LowFareSearchReq.ToString());
                    IweenAirAsiaAuth.SaveXml(strRes, SecurityGUID, "Book_Res" + randomkey);
                    XDocument xd2 = XDocument.Parse(strRes);
                    string statuscodeval = "";
                    string statusmessagecode = "";
                    string txid = "";
                    xd2.Descendants("getitineraryrequest").ToList().ForEach(Each =>
                    {
                        statuscodeval = Each.Element("statuscode").Value;
                        statusmessagecode = Each.Element("statusmessage").Value;
                        txid = Each.Element("txid").Value;
                    });
                    if (statuscodeval == "200")
                    {
                        StringBuilder itinerary = new StringBuilder();
                        itinerary.Append("<?xml version='1.0' encoding='UTF-8'?>");
                        itinerary.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:mer='http://mercuryws.nextra.com/'>");
                        itinerary.Append("<soapenv:Header/>");
                        itinerary.Append("<soapenv:Body>");
                        //itinerary.Append("<mer:ValidateFlightPrice>");
                        itinerary.Append("<getitineraryrequest>");
                        itinerary.Append("<credentials>");
                        //itinerary.Append("<officeid>DM24614</officeid>");
                        //itinerary.Append("<password>Test@321</password>");
                        //itinerary.Append("<username>testcustomer</username>");
                        LowFareSearchReq.Append("<officeid>AAA16445</officeid>");
                        LowFareSearchReq.Append("<username>RICHA</username>");
                        LowFareSearchReq.Append("<password>1-TRVL!9102!~</password>");
                        itinerary.Append("</credentials>");
                        itinerary.Append("<txid>" + txid + "</txid>");
                        itinerary.Append("<wsmode>LIVE</wsmode>");
                        itinerary.Append("</getitineraryrequest>");
                        //itinerary.Append("</mer:ValidateFlightPrice>");
                        itinerary.Append("</soapenv:Body>");
                        itinerary.Append("</soapenv:Envelope>");
                        string randomkey2 = Utility.GetRndm();
                        IweenAirAsiaAuth.SaveXml(itinerary.ToString(), SecurityGUID, "Getitationary_Req" + randomkey2);
                        //strRes = IweenAirAsiaAuth.GetResponse("http://modemov2.iweensoft.com:80/flights/flights?wsdl", itinerary.ToString());
                        strRes = IweenAirAsiaAuth.GetResponse("http://www.airasiaapi.com:8080/flights/flights?wsdl", itinerary.ToString());
                        IweenAirAsiaAuth.SaveXml(strRes, SecurityGUID, "Getitationary__Res" + randomkey2);

                        strRes = RemoveAllNamespaces(strRes);
                        strRes = strRes.Replace("<![CDATA[", "").Replace("]]]]>>", "").Replace("]]>", "").Replace("S:", "").Replace("ns2:", "");
                        XDocument xd = XDocument.Parse(strRes);
                        //XDocument xd2 = XDocument.Parse(responseXml);
                        //int flight = 1;

                        foreach (var AvlR in xd.Descendants("return").Descendants("FlightInvoice"))
                        {
                            foreach (var Avlflight in AvlR.Descendants("FlightRecord"))
                            {
                                PNR = Avlflight.Element("GdsPnr").Value.ToString();
                                airlinpnr = Avlflight.Element("AirlinePnr").Value.ToString();
                            }
                        }
                    }
                    #endregion
                }



            #endregion
            FareMisMatch:
                exep = exep + "Fare Amount Diffrence new fare:" + statuscode + " old Fare:" + totfare.ToString();

            }
            catch (Exception ex)
            {
                exep = exep + ex.Message + ex.StackTrace.ToString();
                PNR = PNR + "-FQ";
                ITZERRORLOG.ExecptionLogger.FileHandling("IWEENBOOK(IWEENBooking)", "Error_001", ex, "BookingRequest");
            }
            finally
            {
                FlightCommonBAL objCommonBAL = new FlightCommonBAL(Constr);

                //objCommonBAL.InsertMULTIBookingLog(Convert.ToString(FltDT.Rows[0]["ValiDatingCarrier"]), Convert.ToString(FltDT.Rows[0]["Track_id"]), PNR
                //    , Log.ContainsKey("BookReq") == true ? Log["BookReq"] : "", Log.ContainsKey("BookResp") == true ? Log["BookResp"] : ""
                //    , Log.ContainsKey("RepriceReq") == true ? Log["RepriceReq"] : ""
                //    , Log.ContainsKey("RepriceResp") == true ? Log["RepriceResp"] : "", exep);
            }
            return PNR;

        }

        public static string RemoveAllNamespaces(string xmlDocument)
        {
            XElement xmlDocumentWithoutNs = RemoveAllNamespaces(XElement.Parse(xmlDocument));

            return xmlDocumentWithoutNs.ToString();
        }

        //Core recursion function
        private static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;

                foreach (XAttribute attribute in xmlDocument.Attributes())
                    xElement.Add(attribute);

                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }
    }
}
