﻿using Newtonsoft.Json;
using STD.BAL;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace GALWS.IWEENAirAsia
{

    public class IweenSearch
    {
        //public string LoginID { get; set; }
        //public string LoginPass { get; set; }
        //public string UserID { get; set; }
        //public string UserPass { get; set; }
        public string SecurityGUID { get; set; }
        //public string IP { get; set; }
        //public string ServiceUrl { get; set; }
        //public string TargetCode { get; set; }

        public IweenSearch()
        {
            //LoginID = loginId;
            //LoginPass = loginPass;
            //UserID = userId;
            //UserPass = userPass;
            //IP = ip;
            //ServiceUrl = serviceUrl;
            //TargetCode = targetCode;
        }
        public List<FlightSearchResults> GetFlightAvailability(FlightSearch f, bool RTFS, string fareType, bool isLCC, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, string constr, List<FlightCityList> CityList, CredentialList CrdList, HttpContext contx, List<MISCCharges> MiscList)
        {
            List<FlightSearchResults> resultList = new List<FlightSearchResults>();
            string exep = "";
            string TTrip = fareType;
            string TTripType ="";
            bool faretypedr=true;
            try
            {
                SecurityGUID = "Iween-" + f.SessionId;
                IweenRequest objFareQ = new IweenRequest();
                string resp = objFareQ.GetLowFareSearch(f, ref exep, TTrip, TTripType, RTFS, faretypedr, CrdList);
                //TravarenaRequest objFareQ = new TravarenaRequest(LoginID, LoginPass, ServiceUrl, TargetCode, SecurityGUID);
                resultList = ParseFareQoute(resp, f, CityList,SrvchargeList, MarkupDs, RTFS,constr, MiscList,fareType, isLCC, contx);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return resultList;
        }
        private List<FlightSearchResults> ParseFareQoute(string responseXml, FlightSearch searchInput, List<FlightCityList> CityList, List<FltSrvChargeList> SrvchargeList, DataSet MarkupDs, bool RTFS, string constr, List<MISCCharges> MiscList, string fareType, bool isLCC, HttpContext context)
        {
            List<FlightSearchResults> FinalList = new List<FlightSearchResults>();
            FlightCommonBAL objFltComm = new FlightCommonBAL();
            try
            {
                

                List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
                List<FlightSearchResults> RfsrList = new List<FlightSearchResults>();
                FlightCommonBAL searchInputComm = new FlightCommonBAL(constr);
                float srvChargeAdt = 0;
                float srvChargeChd = 0;
                float srvChargeINF = 0;
                float srvCharge = 0;
                DataTable CommDt = new DataTable();
                Hashtable STTFTDS = new Hashtable();
             
                int adt = searchInput.Adult, chd = searchInput.Child, inf = searchInput.Infant;
                if (responseXml.Contains("Error") == false)
                {
                    XDocument xd2 = XDocument.Parse(responseXml);
                    int flight = 1;
                    foreach (var AvlR in xd2.Descendants("return").Descendants("flightjourneys"))
                    {
                        int lineNum = 1;
                        foreach (var Avlflight in AvlR.Descendants("flightoptions"))
                        {
                            int leg = 1;
                            int stopcount = Avlflight.Descendants("recommendedflight").Elements("flightlegs").Count();
                            foreach (var Items in Avlflight.Descendants("recommendedflight").Descendants("flightlegs"))
                            {

                                FlightSearchResults fsr = new FlightSearchResults();
                                fsr.Leg = leg;
                                fsr.LineNumber = lineNum;
                                fsr.Flight = flight.ToString();
                                fsr.Stops = (stopcount - 1).ToString() + "-Stop";

                                fsr.Adult = searchInput.Adult;
                                fsr.Child = searchInput.Child;
                                fsr.Infant = searchInput.Infant;

                                fsr.depdatelcc = Items.Element("depdate").Value.ToString();
                                fsr.arrdatelcc = Items.Element("arrdate").Value.ToString();
                                fsr.Departure_Date = Convert.ToDateTime(Items.Element("depdate").Value.Trim()).ToString("dd MMM"); // legdetailsM.DepartureDate[8].ToString() + legdetailsM.DepartureDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.DepartureDate[5].ToString() + legdetailsM.DepartureDate[6].ToString()));
                                fsr.Arrival_Date = Convert.ToDateTime(Items.Element("arrdate").Value.Trim()).ToString("dd MMM");// legdetailsM.ArrivalDate[8].ToString() + legdetailsM.ArrivalDate[9].ToString() + " " + GetMonthName(Convert.ToInt16(legdetailsM.ArrivalDate[5].ToString() + legdetailsM.ArrivalDate[6].ToString()));
                                fsr.DepartureDate = Convert.ToDateTime(Items.Element("depdate").Value.Trim()).ToString("ddMMyy");
                                fsr.ArrivalDate = Convert.ToDateTime(Items.Element("arrdate").Value.Trim()).ToString("ddMMyy");


                                fsr.FlightIdentification = Items.Element("flightnumber").Value.ToString();
                                fsr.AirLineName = Items.Element("carriername").Value.ToString();

                                fsr.ValiDatingCarrier = Items.Element("carrier").Value.ToString();
                                fsr.OperatingCarrier = Items.Element("carrier").Value.ToString();
                                fsr.MarketingCarrier = Items.Element("carrier").Value.ToString();
                                fsr.fareBasis = "";


                                fsr.DepartureLocation = Items.Element("origin").Value.ToString();
                                fsr.DepartureCityName = Items.Element("origin_name").Value.ToString();
                                fsr.DepartureTime = Items.Element("deptime").Value.Trim().ToString();
                                fsr.DepartureAirportName = Items.Element("origin_name").Value.ToString();
                                fsr.DepartureTerminal = Items.Element("depterminal").Value.ToString();
                                fsr.DepAirportCode = Items.Element("origin").Value.ToString();

                                fsr.ArrivalLocation = Items.Element("destination").Value.ToString();
                                fsr.ArrivalCityName = Items.Element("destination_name").Value.ToString();
                                fsr.ArrivalTime = Items.Element("arrtime").Value.Trim().ToString();
                                fsr.ArrivalAirportName = Items.Element("destination").Value.ToString();
                                fsr.ArrivalTerminal = Items.Element("arrterminal").Value.ToString();
                                fsr.ArrAirportCode = Items.Element("destination").Value.ToString();
                                //fsr.Trip = searchInput.Trip.ToString();
                                fsr.Sector = fsr.DepartureLocation + ":" + fsr.ArrivalLocation;
                                fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt32(Items.Element("journeyduration").Value));
                                fsr.TripCnt = GetTimeInHrsAndMin(Convert.ToInt32(Items.Element("journeyduration").Value));
                                string OfferedFare = "";
                                foreach (var ADTFARELIST in Avlflight.Descendants("recommendedflight").Descendants("flightfare"))
                                {

                                    #region ADT

                                    if (adt != 0)
                                    {
                                        fsr.AdtBfare = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("adultbasefare").Value.ToString()));
                                        fsr.AdtCabin = Items.Element("cabinclass").Value.ToString();
                                        fsr.AdtFSur = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("adultyq").Value.ToString()));
                                        fsr.AdtTax = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("adulttax").Value.ToString()));
                                        fsr.AdtTax = (float)Math.Ceiling((fsr.AdtTax) + srvCharge);
                                        fsr.AdtOT = (float)Math.Ceiling(fsr.AdtTax - fsr.AdtFSur);// + paxFare.AdditionalTxnFeePub + float.Parse((ChargeBU / paxFare.PassengerCount).ToString()) + srvCharge);
                                        fsr.AdtRbd = Items.Element("bookingclass").Value.ToString();
                                        fsr.AdtFarebasis = "";
                                        fsr.AdtFareType = ADTFARELIST.Element("refundableinfo").Value.ToString() == "Y" ? "Refundable" : "Non Refundable";// fareInfo.FareTypeID.Trim();
                                        // fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                        fsr.AdtFare = (float)Math.Ceiling(float.Parse((fsr.AdtTax + fsr.AdtBfare).ToString())); //  + srvCharge  nextraflightkey
                                        var nextracustomstr = Avlflight.Descendants("recommendedflight").First().Element("nextracustomstr").Value.ToString();
                                        var nextraflightkey = Avlflight.Descendants("recommendedflight").First().Element("nextraflightkey").Value.ToString();
                                        fsr.sno = nextracustomstr + ":" + nextraflightkey + ":" + Convert.ToString(isLCC);
                                        fsr.fareBasis = fsr.AdtFarebasis;
                                        fsr.FBPaxType = "ADT";

                                        fsr.AdtFar = ADTFARELIST.Element("faretype").Value.ToString();
                                        if (fsr.AdtFar == "normal")
                                            fsr.AdtFar = "NRM";
                                        fsr.BagInfo = fsr.AdtFar;
                                        if (fsr.AdtFar.Trim().Contains("special non commissionable fare"))
                                        {
                                            fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier + "S", fsr.AdtFare, searchInput.Trip.ToString());
                                            fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + "S", fsr.AdtFare, searchInput.Trip.ToString());

                                            fsr.AdtDiscount1 = 0;  //-AdtComm  
                                            fsr.AdtCB = 0;
                                            fsr.AdtSrvTax1 = 0;// added TO TABLE
                                            fsr.AdtDiscount = 0;
                                            fsr.AdtEduCess = 0;
                                            fsr.AdtHighEduCess = 0;
                                            fsr.AdtTF = 0;
                                            fsr.AdtTds = 0;
                                        }
                                        else
                                        {
                                            fsr.ADTAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());
                                            fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());

                                            CommDt.Clear();
                                            CommDt = searchInputComm.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.fareBasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                            fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());  //-AdtComm  
                                            fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                            STTFTDS.Clear();
                                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, searchInput.TDS);
                                            fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE
                                            fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                            fsr.AdtEduCess = 0;
                                            fsr.AdtHighEduCess = 0;
                                            fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                            fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                        }

                                        if (searchInput.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = searchInputComm.clac_MgtFee(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), searchInput.Trip.ToString(), decimal.Parse(fsr.AdtFare.ToString()));
                                                fsr.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                fsr.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }


                                    }
                                    #endregion
                                    #region CHD
                                    if (chd != 0)
                                    {
                                        fsr.ChdBFare = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("childbasefare").Value.ToString()));
                                        fsr.ChdCabin = Items.Element("cabinclass").Value.ToString();
                                        fsr.ChdFSur = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("childyq").Value.ToString()));
                                        fsr.ChdTax = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("childtax").Value.ToString()));
                                        fsr.ChdTax = (float)Math.Ceiling(float.Parse((fsr.ChdTax + srvCharge).ToString()));
                                        fsr.ChdOT = (float)Math.Ceiling(fsr.ChdTax - fsr.ChdFSur);// + paxFare.AdditionalTxnFeePub + srvCharge);
                                        fsr.ChdRbd = Items.Element("bookingclass").Value.ToString();
                                        fsr.ChdFarebasis = "";
                                        fsr.ChdFare = (float)Math.Ceiling(float.Parse((fsr.ChdTax + fsr.ChdBFare).ToString())); //  + srvCharge

                                        //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());



                                        if (fsr.AdtFar.Trim().Contains("special non commissionable fare"))
                                        {
                                            fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier + "S", fsr.ChdFare, fsr.Trip.ToString());
                                            fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + "S", fsr.ChdFare, searchInput.Trip.ToString());
                                            fsr.ChdDiscount1 = 0;  //-AdtComm  
                                            fsr.ChdCB = 0;
                                            fsr.ChdSrvTax1 = 0;// added TO TABLE
                                            fsr.ChdDiscount = 0;
                                            fsr.ChdEduCess = 0;
                                            fsr.ChdHighEduCess = 0;
                                            fsr.ChdTF = 0;
                                            fsr.ChdTds = 0;
                                        }
                                        else
                                        {
                                            fsr.CHDAdminMrk = CalcMarkup(MarkupDs.Tables["AdminMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, fsr.Trip.ToString());
                                            fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());
                                            CommDt.Clear();
                                            CommDt = searchInputComm.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.ChdFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[0].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, "NRM", "");
                                            fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());//-ChdComm
                                            fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                            STTFTDS.Clear();
                                            STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, searchInput.TDS);
                                            fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString()); // added TO TABLE 
                                            fsr.ChdDiscount = fsr.ChdDiscount1 - fsr.ChdSrvTax1;
                                            fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                            fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                            fsr.ChdEduCess = 0;
                                            fsr.ChdHighEduCess = 0;
                                        }
                                        if (searchInput.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = searchInputComm.clac_MgtFee(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), searchInput.Trip.ToString(), decimal.Parse(fsr.ChdFare.ToString()));
                                                fsr.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                fsr.ChdSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }
                                    }
                                    #endregion
                                    #region INF
                                    if (inf != 0)
                                    {
                                        fsr.InfBfare = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("infantbasefare").Value.ToString()));
                                        fsr.InfCabin = "";
                                        fsr.InfFSur = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("infantyq").Value.ToString()));
                                        fsr.InfTax = (float)Math.Ceiling(float.Parse(ADTFARELIST.Element("infanttax").Value.ToString()));
                                        fsr.InfOT = (float)Math.Ceiling(fsr.InfTax - fsr.InfFSur);//+ paxFare.AdditionalTxnFeePub);
                                        fsr.InfRbd = Items.Element("bookingclass").Value.ToString();
                                        fsr.InfFarebasis = "";
                                        fsr.InfFare = (float)Math.Ceiling(float.Parse((fsr.InfTax + fsr.InfBfare).ToString()));


                                        if (searchInput.IsCorp == true)
                                        {
                                            try
                                            {
                                                DataTable MGDT = new DataTable();
                                                MGDT = searchInputComm.clac_MgtFee(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.InfBfare.ToString()), decimal.Parse(fsr.InfFSur.ToString()), searchInput.Trip.ToString(), decimal.Parse(fsr.InfFare.ToString()));
                                                fsr.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                                                fsr.InfSrvTax1 = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                                            }
                                            catch { }
                                        }
                                    }
                                    #endregion

                                    OfferedFare = ADTFARELIST.Element("totalnet").Value.ToString();
                                }

                                fsr.Searchvalue = OfferedFare + "Jocose3591";
                                fsr.OriginalTF = float.Parse(OfferedFare);
                                fsr.OriginalTT = srvCharge;
                                fsr.Provider = "IW";

                                fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                                //fsr.TotalFare =(float)Math.Ceiling( float.Parse(flt.Fare.PublishedFare.ToString()));
                                //fsr.TotalFuelSur = (float)Math.Ceiling(float.Parse(flt.Fare.YQTax.ToString()));//(fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                                //fsr.TotalTax =(float)Math.Ceiling( float.Parse(flt.Fare.Tax.ToString()) + float.Parse(flt.Fare.OtherCharges.ToString()) + float.Parse(flt.Fare.AdditionalTxnFeePub.ToString()));// (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                                //fsr.OriginalTT = fsr.TotalTax;
                                //fsr.TotBfare =(float)Math.Ceiling( float.Parse(flt.Fare.BaseFare.ToString()));// (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);


                                fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                                fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(objlist.InfTF * objlist.Infant);
                                fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);
                                fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);
                                fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +objFS.InfTds;
                                fsr.TotMgtFee = (fsr.AdtMgtFee * fsr.Adult) + (fsr.ChdMgtFee * fsr.Child) + (fsr.InfMgtFee * fsr.Infant);



                                fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                                fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                                fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                                fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                                fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                                fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                                fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                                fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));


                                if (searchInput.RTF == true || searchInput.GDSRTF == true || RTFS == true)
                                {
                                    fsr.Sector = searchInput.HidTxtDepCity.Split(',')[0] + ":" + searchInput.HidTxtArrCity.Split(',')[0] + ":" + searchInput.HidTxtDepCity.Split(',')[0];
                                    fsr.FType = "RTF";
                                }
                                else
                                {
                                    if (fareType == "IB")
                                        fsr.Sector = searchInput.HidTxtArrCity.Split(',')[0] + ":" + searchInput.HidTxtDepCity.Split(',')[0];
                                    else
                                        fsr.Sector = searchInput.HidTxtDepCity.Split(',')[0] + ":" + searchInput.HidTxtArrCity.Split(',')[0];
                                }
                                if (fareType == "IB")
                                {
                                    fsr.OrgDestFrom = searchInput.HidTxtArrCity.Split(',')[0];
                                    fsr.OrgDestTo = searchInput.HidTxtDepCity.Split(',')[0];
                                }
                                else
                                {
                                    fsr.OrgDestFrom = searchInput.HidTxtDepCity.Split(',')[0];
                                    fsr.OrgDestTo = searchInput.HidTxtArrCity.Split(',')[0];
                                }

                                fsr.TripType = searchInput.TripType.ToString();
                                fsr.Trip = searchInput.Trip.ToString();

                                if (searchInput.TripType == TripType.RoundTrip && searchInput.Trip == STD.Shared.Trip.D && RTFS == true)
                                {
                                    if (Convert.ToInt32(fsr.Flight) == 1)
                                        fsrList.Add(fsr);
                                    else
                                        RfsrList.Add(fsr);
                                }
                                else if (searchInput.TripType == TripType.RoundTrip && searchInput.Trip == STD.Shared.Trip.I)
                                {
                                    if (Convert.ToInt32(fsr.Flight) == 1)
                                        fsrList.Add(fsr);
                                    else
                                        RfsrList.Add(fsr);
                                }
                                else
                                {
                                    fsrList.Add(fsr);
                                }


                                leg++;

                            }
                            lineNum++;
                        }
                        
                    }
                    flight = flight + 1;

                    if (RTFS == true && searchInput.Trip == Trip.D && fsrList.Count > 0 && RfsrList.Count > 0)  //RTF True
                        FinalList = RoundTripFare(fsrList, RfsrList, srvCharge);
                    else if (searchInput.TripType == TripType.RoundTrip && searchInput.Trip == Trip.I && fsrList.Count > 0 && RfsrList.Count > 0)  //RTF True
                        FinalList = RoundTripFare(fsrList, RfsrList, srvCharge);
                    else
                    {
                        //fsrList = fsrList.Where((x => ((x.AdtFar == "NRM")))).ToList();
                        if (searchInput.NStop == true)
                        {
                            fsrList = fsrList.Where(s => s.Stops == "0-Stop").ToList();
                            FinalList = fsrList;
                        }
                        FinalList = fsrList;
                        if (RfsrList.Count != 0)
                        {
                            //  RfsrList = RfsrList.Where((x => ((x.AdtFar == "NRM")))).ToList();
                            FinalList = RfsrList;
                        }
                    }
                }

            }
            catch (Exception ex)
            {

            }
            return objFltComm.AddFlightKey(FinalList, searchInput.GDSRTF);
            

        }
        //private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        //{
        //    decimal STaxP = 0;
        //    decimal TFeeP = 0;
        //    int IATAComm = 0;
        //    //decimal originalDis = 0;
        //    Hashtable STHT = new Hashtable();
        //    if (string.IsNullOrEmpty(TDS))
        //    {
        //        TDS = "0";
        //    }

        //    try
        //    {
        //        List<FltSrvChargeList> StNew = (from st in SrvchargeList where st.AirlineCode == VC select st).ToList();
        //        if (StNew.Count > 0)
        //        {
        //            STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
        //            TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
        //            IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
        //        }
        //        STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
        //        STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
        //        //STHT.Add("Tds", Math.Round((double.Parse(Dis.ToString()) * double.Parse(TDS)) / 100, 0));
        //        STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
        //        STHT.Add("IATAComm", IATAComm);
        //    }
        //    catch
        //    {
        //        STHT.Add("STax", 0);
        //        STHT.Add("TFee", 0);
        //        STHT.Add("Tds", 0);
        //        STHT.Add("IATAComm", 0);
        //    }
        //    return STHT;
        //}
        private List<FlightSearchResults> RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR, float srvCharge)
        {

            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            ArrayList Comb = new ArrayList();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            for (int k = 1; k <= LnOb; k++)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                for (int l = 1; l <= LnIb; l++)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    //List<FlightSearchResults> st = new List<FlightSearchResults>();

                    if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            // st = Merge(OB, IB, ln);
                            Final.AddRange(Merge(OB, IB, ln, srvCharge));
                            ln++;///Increment Total Ln
                        }
                    }
                    else
                    {
                        Final.AddRange(Merge(OB, IB, ln, srvCharge));
                        ln++;
                    }

                }

            }
            //Comb.Add(Final);
            return Final;
        }
        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln, float srvCharge)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
            //ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
            //CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0;//,
            //InfSrvTax = 0, InfTF = 0;
            float ADTAgentMrk = 0, CHDAgentMrk = 0;
            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT + srvCharge;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare + srvCharge;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax + srvCharge;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;
            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT + srvCharge;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare + srvCharge;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax + srvCharge;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;

            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = srvCharge;// item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child);
            float NetFare = TotalFare - ((ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                y.ADTAgentMrk = ADTAgentMrk;
                #endregion

                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                y.CHDAgentMrk = CHDAgentMrk;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = srvCharge;//OriginalTT;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }
        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].City;
            }

            return name;
        }
        private string getAirlineName(string airl, List<AirlineList> AirList)
        {
            string airl1 = "";
            try
            {
                airl1 = ((from ar in AirList where ar.AirlineCode == airl select ar).ToList())[0].AlilineName;
            }
            catch { airl1 = airl; }
            return airl1;
        }
        private string GetMonthName(int monthNo)
        {

            Dictionary<int, string> month = new Dictionary<int, string>();

            month.Add(01, "JAN");
            month.Add(02, "FEB");
            month.Add(03, "MAR");
            month.Add(04, "APR");
            month.Add(05, "MAY");
            month.Add(06, "JUN");
            month.Add(07, "JUL");
            month.Add(08, "AUG");
            month.Add(09, "SEP");
            month.Add(10, "OCT");
            month.Add(11, "NOV");
            month.Add(12, "DEC");

            return month[monthNo];

        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }
                if (airMrkArray.Length > 0)
                {
                    if (Trip == "I")
                    {
                        if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                        {
                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                        }
                        else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                        {
                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                        }
                    }
                    else
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }
        //public List<FlightSearchResults> PaxFareDetails(string xml, string mainxml, string SegRefKey, string PaxType, string VCarrier)
        //{
        //    List<FlightSearchResults> PFI = new List<FlightSearchResults>();
        //    FlightSearchResults PXFARE = new FlightSearchResults();
        //    XNamespace Pns = "http://www.travelport.com/schema/air_v42_0";
        //    XNamespace Common = "http://www.travelport.com/schema/common_v42_0";
        //    XDocument xd = XDocument.Parse(xml);

        //    XDocument xd1 = XDocument.Parse(mainxml);
        //    var fare = xd1.Descendants(Pns + "LowFareSearchRsp").Descendants(Pns + "FareInfoList");
        //    var hosttokwn = xd1.Descendants(Pns + "LowFareSearchRsp").Descendants(Pns + "HostTokenList");
        //    var prcinfo = xd.Root;
        //    //var prcs = xd.Descendants(Pns + "LowFareSearchRsp").Descendants(Pns + "AirPricingSolution");
        //    //var prcinfo1 = prcinfo; 
        //    string prckey = ""; double TotalPrice = 0; double BasePrice = 0; double Taxes = 0; string FareType = ""; string Carrier = "";
        //    //string key = xd.Root.Attributes("Key").First().Value;
        //    prckey = xd.Root.Attributes("Key").First().Value;
        //    TotalPrice = Convert.ToDouble(xd.Root.Attributes("TotalPrice").First().Value.Remove(0, 3));
        //    if (TravarenaUtility.Left(xd.Root.Attributes("TotalPrice").First().Value.ToUpper().Trim(), 3) == "INR")
        //        TotalPrice = Convert.ToDouble(xd.Root.Attributes("TotalPrice").First().Value.Remove(0, 3));
        //    else
        //        TotalPrice = xd.Root.Attributes("ApproximateTotalPrice").Any() == true ? Convert.ToDouble(xd.Root.Attributes("ApproximateTotalPrice").First().Value.Remove(0, 3)) : Convert.ToDouble(xd.Root.Attributes("EquivalentTotalPrice").First().Value.Remove(0, 3));

        //    if (TravarenaUtility.Left(xd.Root.Attributes("BasePrice").First().Value.Substring(0, 3).ToUpper().Trim(), 3) == "INR")
        //        BasePrice = Convert.ToDouble(xd.Root.Attributes("BasePrice").First().Value.Remove(0, 3));
        //    else
        //        BasePrice = xd.Root.Attributes("EquivalentBasePrice").Any() == true ? Convert.ToDouble(xd.Root.Attributes("EquivalentBasePrice").First().Value.Remove(0, 3)) : Convert.ToDouble(xd.Root.Attributes("ApproximateBasePrice").First().Value.Remove(0, 3));
        //    Taxes = xd.Root.Attributes("Taxes").Any() == true ? Convert.ToDouble(xd.Root.Attributes("Taxes").First().Value.Remove(0, 3)) : 0;
        //    if (PaxType == "ADT")
        //    {
        //        Carrier = xd.Root.Attributes("PlatingCarrier").Any() == true ? xd.Root.Attributes("PlatingCarrier").First().Value : VCarrier;
        //        PXFARE.ValiDatingCarrier = Carrier;
        //        PXFARE.OperatingCarrier = Carrier;

        //        //try
        //        //{
        //        //    PXFARE.PricingMethod = xd.Root.Attributes("PricingMethod").Any() == true ? xd.Root.Attributes("PricingMethod").First().Value : "";
        //        //}
        //        //catch (Exception ex)
        //        //{ PXFARE.PricingMethod = ""; }
        //    }
        //    try
        //    {
        //        FareType = "";// xd.Descendants(Pns + "FareRulesFilter").Any() == true ? xd.Descendants(Pns + "FareRulesFilter").Descendants(Pns + "Refundability").Attributes("Value").First().Value : "Refundable";
        //    }
        //    catch (Exception ex) { FareType = "NonRefundable"; }
        //    //FareType = xd.Root.Attribute("Refundable") == null ? "NonRefundable" : Convert.ToBoolean(xd.Root.Attributes("Refundable").First().Value) == true ? "Refundable" : "Non Refundable";
        //    //== true ?
        //    var bookinginfo = xd.Root.Elements(Pns + "BookingInfo");


        //    var bkInfo = from bk in bookinginfo.Where(x => x.Attribute("SegmentRef").Value == SegRefKey)
        //                 select new
        //                 {
        //                     CabinClass = bk.Attributes("CabinClass").First().Value,
        //                     FareInfoRef = bk.Attributes("FareInfoRef").First().Value,
        //                     RBD = bk.Attributes("BookingCode").First().Value,
        //                     SegmentRef = bk.Attributes("SegmentRef").First().Value,
        //                     HostTokenList = bk.Attributes("HostTokenRef").Any() == false ? "" : bk.Attributes("HostTokenRef").First().Value

        //                 };
        //    var TaxBreakUp = from tx in xd.Root.Elements(Pns + "TaxInfo")//.Where(x => x.Attribute("SegmentRef").Value == SegRefKey)
        //                     select new
        //                     {
        //                         Category = tx.Attributes("Category").First().Value,
        //                         Amount = tx.Attributes("Amount").First().Value.Remove(0, 3),
        //                     };

        //    var fareinfo = from fr in fare.Descendants(Pns + "FareInfo").Where(x => x.Attribute("Key").Value == bkInfo.First().FareInfoRef.ToString())//&& x.Attribute("PassengerTypeCode").Value == PaxType
        //                   select new
        //                   {
        //                       FareBasis = fr.Attributes("FareBasis").First().Value,
        //                       NotValidBefore = fr.Attributes("NotValidBefore").Any() == true ? fr.Attributes("NotValidBefore").First().Value : "",
        //                       NotValidAfter = fr.Attributes("NotValidAfter").Any() == true ? fr.Attributes("NotValidAfter").First().Value : "",
        //                       FareKey = fr.Attributes("Key").First().Value,
        //                       BaggageType = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "NumberOfPieces").Any() == true ? "UNIT" : "WEIGHT",
        //                       BaggageWeight = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Value").Any() == true ? fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Value").First().Value : "NA",
        //                       BaggageWeightType = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Unit").Any() == true ? fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "MaxWeight").Attributes("Unit").First().Value : "NA",
        //                       BaggagePieces = fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "NumberOfPieces").Any() == true ? fr.Descendants(Pns + "BaggageAllowance").Descendants(Pns + "NumberOfPieces").First().Value : "NA",
        //                       FareRuleKey = fr.Descendants(Pns + "FareRuleKey").First().Value,
        //                       AccCode = fr.Descendants(Common + "AccountCode").Any() == true ? fr.Descendants(Common + "AccountCode").Attributes("Code").First().Value : "",
        //                       PassengerTypeCode = fr.Attributes("PassengerTypeCode").Any() == true ? fr.Attributes("PassengerTypeCode").First().Value : "",
        //                       Origin = fr.Attributes("Origin").Any() == true ? fr.Attributes("Origin").First().Value : "",
        //                       Destination = fr.Attributes("Destination").Any() == true ? fr.Attributes("Destination").First().Value : "",
        //                       EffectiveDate = fr.Attributes("EffectiveDate").Any() == true ? fr.Attributes("EffectiveDate").First().Value : "",
        //                       DepartureDate = fr.Attributes("DepartureDate").Any() == true ? fr.Attributes("DepartureDate").First().Value : "",
        //                       Amount = fr.Attributes("Amount").Any() == true ? fr.Attributes("Amount").First().Value : "",
        //                       TaxAmount = fr.Attributes("TaxAmount").Any() == true ? fr.Attributes("TaxAmount").First().Value : "",
        //                       FareFamily = fr.Attributes("FareFamily").Any() == true ? fr.Attributes("FareFamily").First().Value : "",
        //                   };
        //    string k = "";
        //    string faretype = "";
        //    try
        //    {
        //        //  PXFARE.PricingMethod = xd.Root.Attributes("PricingMethod").Any() == true ? xd.Root.Attributes("PricingMethod").First().Value : "";

        //        if (xd.Root.Descendants(Pns + "BookingInfo") != null)
        //            if (xd.Root.Descendants(Pns + "BookingInfo").First().Attribute("BookingCode") != null)
        //                if (xd.Root.Descendants(Pns + "BookingInfo").First().Attribute("BookingCode").Value == "S")
        //                {
        //                    faretype = "CRP";
        //                }
        //                else
        //                {
        //                    faretype = "NRM";
        //                }

        //    }
        //    catch (Exception ex)
        //    { faretype = ""; }



        //    // PXFARE.FareCalc = xd.Descendants(Pns + "FareCalc").Any() == true ? xd.Descendants(Pns + "FareCalc").First().Value : "";
        //    // PXFARE.AccountCode = fareinfo.First().AccCode;

        //    PXFARE.TotalFare = Convert.ToSingle(TotalPrice);
        //    PXFARE.TotalTax = Convert.ToSingle(Taxes);




        //    if (PaxType == "ADT")
        //    {
        //        PXFARE.AdtBfare = Convert.ToSingle(BasePrice);
        //        PXFARE.AdtFar = faretype;// fareinfo.First().FareFamily;
        //        PXFARE.HegFareType = fareinfo.First().FareFamily;
        //        PXFARE.AdtFareType = FareType;
        //        PXFARE.AdtCabin = bkInfo.Any() == true ? bkInfo.First().CabinClass : "";
        //    }
        //    else if (PaxType == "CNN")
        //    {

        //        PXFARE.ChdBFare = Convert.ToSingle(BasePrice);
        //        PXFARE.ChdFar = faretype;//fareinfo.First().FareFamily;
        //        PXFARE.HegFareType = fareinfo.First().FareFamily;
        //        PXFARE.ChdfareType = FareType;
        //        PXFARE.ChdCabin = bkInfo.Any() == true ? bkInfo.First().CabinClass : "";
        //    }
        //    else if (PaxType == "INF")
        //    {
        //        PXFARE.InfBfare = Convert.ToSingle(BasePrice);
        //        PXFARE.InfFar = faretype;// fareinfo.First().FareFamily;
        //        PXFARE.HegFareType = fareinfo.First().FareFamily;
        //        PXFARE.InfFareType = FareType;
        //        PXFARE.InfCabin = bkInfo.Any() == true ? bkInfo.First().CabinClass : "";

        //    }

        //    string Searchvalue = bkInfo.Any() == true ? bkInfo.First().HostTokenList : "";


        //    var hosttokwns = from bk in hosttokwn.Elements(Common + "HostToken").Where(x => x.Attribute("Key").Value == Searchvalue)
        //                     select new
        //                     {
        //                         HostTokentext = bk.Value,


        //                     };
        //    string sss1 = Searchvalue;
        //    string sss2 = hosttokwns.Any() == true ? hosttokwns.First().HostTokentext : "";
        //    PXFARE.Searchvalue = sss1 + "~~~" + sss2;
        //    PXFARE.fareBasis = fareinfo.First().FareBasis;
        //    if (PaxType == "CNN")
        //    {
        //        PXFARE.ChdFarebasis = fareinfo.First().FareBasis;
        //    }
        //    else if (PaxType == "INF")
        //    {
        //        PXFARE.InfFarebasis = fareinfo.First().FareBasis;
        //    }

        //    PXFARE.RBD = bkInfo.Any() == true ? bkInfo.First().RBD : "";

        //    // PXFARE.FareInfoRef = bkInfo.First().FareInfoRef;
        //    //  PXFARE.SegmentRef = bkInfo.First().SegmentRef;
        //    // PXFARE.NotValidBefore = fareinfo.First().NotValidBefore;
        //    // PXFARE.NotValidAfter = fareinfo.First().NotValidAfter;
        //    if (fareinfo.First().BaggageType == "WEIGHT")
        //        PXFARE.BagInfo = fareinfo.First().BaggageWeight + " " + fareinfo.First().BaggageWeightType + " Baggage Included.";
        //    else
        //        PXFARE.BagInfo = fareinfo.First().BaggagePieces + " Piece(s) Baggage included.";

        //    foreach (var txb in TaxBreakUp)
        //    {
        //        if (txb.Category == "IN")
        //            PXFARE.AdtIN = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "JN")
        //        //    PXFARE.JN = Convert.ToDouble(txb.Amount);
        //        else if (txb.Category == "YQ")
        //            PXFARE.AdtFSur = Convert.ToSingle(txb.Amount);
        //        else if (txb.Category == "WO")
        //            PXFARE.AdtWO = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "YM")
        //        //    PXFARE.AdtYM = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "YR")
        //        //    #region GST
        //        //    PXFARE.YR = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "CGST")
        //        //    PXFARE.CGST = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "SGST")
        //        //    PXFARE.SGST = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "UTGST")
        //        //    PXFARE.UTGST = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "IGST")
        //        //    PXFARE.IGST = Convert.ToSingle(txb.Amount);
        //        //else if (txb.Category == "K3")
        //        //    PXFARE.K3 = Convert.ToSingle(txb.Amount);
        //        //    #endregion
        //    }
        //    #region Change and Cancel Panality
        //    try
        //    {
        //        if (xd.Descendants(Pns + "CancelPenalty").Any() == true)
        //        {
        //            //if (xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Amount").Any() == true)
        //            //{ PXFARE.CancelPenalty = xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Amount").First().Value; }
        //            //else if (xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Percentage").Any() == true)
        //            //{ PXFARE.CancelPenalty = xd.Descendants(Pns + "CancelPenalty").Descendants(Pns + "Percentage").First().Value + " Percentage"; }
        //            //else
        //            //{ PXFARE.CancelPenalty = "Not Available"; }
        //        }
        //        else
        //        {// PXFARE.CancelPenalty = "Not Available"; 
        //        }
        //    }
        //    catch (Exception exx)
        //    {
        //        // PXFARE.CancelPenalty = "Not Available";
        //    }

        //    try
        //    {
        //        if (xd.Descendants(Pns + "ChangePenalty").Any() == true)
        //        {
        //            if (xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Amount").Any() == true)
        //            {
        //                //PXFARE.ChangePenalty = xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Amount").First().Value;
        //            }
        //            else if (xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Percentage").Any() == true)
        //            {
        //                //PXFARE.ChangePenalty = xd.Descendants(Pns + "ChangePenalty").Descendants(Pns + "Percentage").First().Value + " Percentage"; 
        //            }
        //            else
        //            {
        //                // PXFARE.ChangePenalty = "Not Available"; 
        //            }
        //        }
        //        else
        //        {
        //            //  PXFARE.ChangePenalty = "Not Available";
        //        }
        //    }
        //    catch (Exception exx)
        //    {
        //        //PXFARE.ChangePenalty = "Not Available";
        //    }

        //    #endregion

        //    PXFARE.FareRule = xd.Descendants(Pns + "FareRulesFilter").Any() == true ? xd.Descendants(Pns + "FareRulesFilter").Descendants(Pns + "Refundability").Attributes("Value").First().Value : "Not Available";
        //    PFI.Add(PXFARE);

        //    return PFI;
        //}
        private Hashtable ServiceTax(string VC, double TotBFWI, double TotBFWOI, double FS, string Trip)
        {
            DataTable dtTax = new DataTable();
            Hashtable AirlineCharges = new Hashtable();
            SqlCommand sqlcom = new SqlCommand();

            SqlConnection conn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            sqlcom = new SqlCommand("ServiceCharge", conn);
            sqlcom.Parameters.Add("@vc", SqlDbType.VarChar).Value = VC;
            sqlcom.Parameters.Add("@trip", SqlDbType.VarChar).Value = Trip;
            sqlcom.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter da = new SqlDataAdapter(sqlcom);
            da.Fill(dtTax);
            try
            {
                if ((dtTax.Rows.Count > 0))
                {
                    AirlineCharges.Add("STax", Math.Round(((TotBFWI * Convert.ToDouble(dtTax.Rows[0]["SrvTax"])) / 100), 0));
                    // Math.Round(((TotBFWI * dtTax.Rows(0)("SrvTax")) / 100), 0)
                    AirlineCharges.Add("TF", Math.Round((((TotBFWOI + FS)
                                        * Convert.ToDouble(dtTax.Rows[0]["TranFee"]))
                                        / 100), 0));
                    AirlineCharges.Add("IATAComm", dtTax.Rows[0]["IATAComm"]);
                }
                else
                {
                    AirlineCharges.Add("STax", 0);
                    //  AirlineCharges.Add("STaxP", 0)
                    AirlineCharges.Add("TF", 0);
                    AirlineCharges.Add("IATAComm", 0);
                }

            }
            catch (Exception ex)
            {
                AirlineCharges.Add("STax", 0);
                // AirlineCharges.Add("STaxP", 0)
                AirlineCharges.Add("TF", 0);
                AirlineCharges.Add("IATAComm", 0);
            }

            return AirlineCharges;
        }

        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Clear();
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
    }
}
