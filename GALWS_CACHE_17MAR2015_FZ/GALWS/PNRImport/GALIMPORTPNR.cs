﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using STD.BAL;
//using System.Data.SqlClient;
//using System.Configuration;
//using STD.Shared;
//using System.Xml;
//using System.IO;
//using System.Xml.Linq;
//using System.Data;
//using System.Reflection;

//namespace GALIMPORT
//{

//    public class GALIMPORTPNR
//    {
//        STD.BAL.FltRequest objFltReq;
//        private List<CredentialList> CrdList;
//        private FlightSearch searchInputs;
//        private string GetTokenFromSessionStratResponse(string strRes)
//        {
//            XmlDocument xd = new XmlDocument();
//            xd.LoadXml(strRes);
//            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
//            return xdo.Root.Value;
//        }

//        public DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
//        {
//            DataTable dtReturn = new DataTable();

//            // column names 
//            System.Reflection.PropertyInfo[] oProps = null;

//            if (varlist == null) return dtReturn;

//            foreach (T rec in varlist)
//            {
//                // Use reflection to get property names, to create table, Only first time, others 
//                //will follow 
//                if (oProps == null)
//                {
//                    oProps = ((Type)rec.GetType()).GetProperties();
//                    foreach (PropertyInfo pi in oProps)
//                    {
//                        Type colType = pi.PropertyType;

//                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
//                        == typeof(Nullable<>)))
//                        {
//                            colType = colType.GetGenericArguments()[0];
//                        }

//                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
//                    }
//                }

//                DataRow dr = dtReturn.NewRow();

//                foreach (PropertyInfo pi in oProps)
//                {
//                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
//                    (rec, null);
//                }

//                dtReturn.Rows.Add(dr);
//            }
//            return dtReturn;
//        }
//        public DataSet PNRDetailsGAL(string PNR,string Trip)
//        {
//            DataSet dspnr = new DataSet();
//            DataTable dt1 = new DataTable("CancelStatus");
//            try
//            {
//                STD.BAL.FltRequest FltRq = new STD.BAL.FltRequest();
//                STD.BAL.FlightCommonBAL FCBAL = new STD.BAL.FlightCommonBAL();
//                string Req = "";
//                CrdList = STD.BAL.Data.GetProviderCrd(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
//                objFltReq = new STD.BAL.FltRequest(searchInputs, CrdList);
//                string url = "", userid = "", Pwd = "";
//                url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
//                userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
//                Pwd = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
//                string strpnr = ""; string Token = ""; string STTReq = ""; //string PNR = "2BK96Q";
//                Req = FCBAL.PostXml(url, objFltReq.BeginSessionReq(), userid, Pwd, "http://webservices.galileo.com/BeginSession");
//                Token = GetTokenFromSessionStratResponse(Req);
//                if (Trip == "D")
//                {
//                    STTReq = objFltReq.TerminalTransaction(Token, "Q/50+*RL-" + PNR);
//                }
//                else
//                {
//                    STTReq = objFltReq.TerminalTransaction(Token, "Q/45+*RL-" + PNR);
//                }

//                string Res = FCBAL.PostXml(url, STTReq, userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
//                strpnr = FCBAL.PostXml(url, objFltReq.RetriveCurrentPnrReq(Token), userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
//                FCBAL.PostXml(url, objFltReq.EndSessionReq(Token), userid, Pwd, "http://webservices.galileo.com/EndSession");
//                XNamespace ns = "http://webservices.galileo.com";
//                XDocument xdoc = XDocument.Parse(strpnr);
//                var FLIGHTSEGMENT = from IMP in xdoc.Descendants(ns + "SubmitXmlOnSessionResult").Elements("PNRBFManagement_13").Elements("PNRBFRetrieve").Elements("AirSeg")
//                                    select new
//                                    {

//                                        loc1 = IMP.Element("StartAirp").Value,
//                                        loc2 = IMP.Element("EndAirp").Value,
//                                        Depdate = IMP.Element("Dt").Value.ToString().Substring(6, 2) + IMP.Element("Dt").Value.ToString().Substring(4, 2) + IMP.Element("Dt").Value.ToString().Substring(2, 2),//.Substring(5,6),
//                                        Deptime = IMP.Element("StartTm").Value,
//                                        ArrDate = (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(3, 2) + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(0, 2) + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(8, 2),
//                                        Arrtime = IMP.Element("EndTm").Value,
//                                        Airline = IMP.Element("AirV").Value,
//                                        FlightNumber = IMP.Element("FltNum").Value,
//                                        RBD = IMP.Element("BIC").Value,
//                                        Status = IMP.Element("Status").Value
//                                    };
//                var PAXNAME = from p in xdoc.Descendants("PNRBFRetrieve").Descendants("FNameInfo")
//                              from l in xdoc.Descendants("PNRBFRetrieve").Descendants("LNameInfo")
//                              where p.Element("AbsNameNum").Value == l.Element("LNameNum").Value //&&//k.Element("AppliesToAry").Element("AppliesTo").Element("AbsNameNum").Value &&
//                              select new
//                              {
//                                  indx = p.Element("AbsNameNum").Value,
//                                  FName = p.Element("FName").Value,
//                                  LName = l.Element("LName").Value
//                              };

//                DataTable Segment = new DataTable("SegmentDetails");
//                  Segment=  LINQToDataTable(FLIGHTSEGMENT);
//                  DataTable PaxName = new DataTable("TktDetails");
//                  PaxName=  LINQToDataTable(PAXNAME);


//                if (Segment.Rows.Count > 0)
//                {
//                    if (Segment.Rows[0]["Status"].ToString().Contains("HK") || Segment.Rows[0]["Status"].ToString().Contains("KL") || Segment.Rows[0]["Status"].ToString().Contains("KK") || Segment.Rows[0]["Status"].ToString().Contains("TK"))
//                    {
//                        dspnr.Tables.Add(Segment);
//                        dspnr.Tables.Add(PaxName);
//                    }
//                    else
//                    {

//                        dt1.Columns.Add("CancelStatus", typeof(String));
//                        dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
//                        dspnr.Tables.Add(dt1);
//                    }
//                }
//                else
//                {
//                    dt1.Columns.Add("CancelStatus", typeof(String));
//                    dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
//                    dspnr.Tables.Add(dt1);  

//                }
//            }
//            catch (Exception ex)
//            {
//                dt1.Columns.Add("CancelStatus", typeof(String));
//                dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
//                dspnr.Tables.Add(dt1);
//            }


//            return dspnr;
//        }
//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using STD.BAL;
using System.Data.SqlClient;
using System.Configuration;
using STD.Shared;
using System.Xml;
using System.IO;
using System.Xml.Linq;
using System.Data;
using System.Reflection;
using System.Collections;
using System.ComponentModel;
using System.Text;
using System.Text.RegularExpressions;

namespace PNRImport
{

    public class GALIMPORTPNR
    {
        STD.BAL.FltRequest objFltReq;
        private List<CredentialList> CrdList;
        private FlightSearch searchInputs;
        List<FlightCityList> CityList;
        List<AirlineList> Airlist;
       
        private string GetTokenFromSessionStratResponse(string strRes)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(strRes);
            XDocument xdo = XDocument.Load(new XmlNodeReader(xd));
            return xdo.Root.Value;
        }

        public DataTable LINQToDataTable<T>(IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            // column names 
            System.Reflection.PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            foreach (T rec in varlist)
            {
                // Use reflection to get property names, to create table, Only first time, others 
                //will follow 
                if (oProps == null)
                {
                    oProps = ((Type)rec.GetType()).GetProperties();
                    foreach (PropertyInfo pi in oProps)
                    {
                        Type colType = pi.PropertyType;

                        if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            colType = colType.GetGenericArguments()[0];
                        }

                        dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                    }
                }

                DataRow dr = dtReturn.NewRow();

                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                }

                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }

        //public DataSet PNRDetailsGAL(string PNR, string Trip)
        //{
        //    DataSet dspnr = new DataSet();
        //    DataTable dt1 = new DataTable("CancelStatus");
        //    try
        //    {
        //        //STD.BAL.FltRequest FltRq = new STD.BAL.FltRequest();
        //        STD.BAL.FlightCommonBAL FCBAL = new STD.BAL.FlightCommonBAL();
        //        string Req = "";
        //        CrdList = STD.BAL.Data.GetProviderCrd(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        //        objFltReq = new STD.BAL.FltRequest(searchInputs, CrdList);
        //        string url = "", userid = "", Pwd = "";
        //        url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
        //        userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
        //        Pwd = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
        //        string strpnr = ""; string Token = ""; string STTReq = ""; //string PNR = "2BK96Q";
        //        Req = FCBAL.PostXml(url, objFltReq.BeginSessionReq(), userid, Pwd, "http://webservices.galileo.com/BeginSession");
        //        Token = GetTokenFromSessionStratResponse(Req);
        //        if (Trip == "D")
        //        {
        //            STTReq = objFltReq.TerminalTransaction(Token, "Q/50+*RL-" + PNR);
        //        }
        //        else
        //        {
        //            STTReq = objFltReq.TerminalTransaction(Token, "Q/45+*RL-" + PNR);
        //        }

        //        string Res = FCBAL.PostXml(url, STTReq, userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
        //        strpnr = FCBAL.PostXml(url, objFltReq.RetriveCurrentPnrReq(Token), userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");                
        //        FCBAL.PostXml(url, objFltReq.EndSessionReq(Token), userid, Pwd, "http://webservices.galileo.com/EndSession");
        //        XNamespace ns = "http://webservices.galileo.com";
        //        XDocument xdoc = XDocument.Parse(strpnr);
        //        var FLIGHTSEGMENT = from IMP in xdoc.Descendants(ns + "SubmitXmlOnSessionResult").Elements("PNRBFManagement_13").Elements("PNRBFRetrieve").Elements("AirSeg")
        //                            select new
        //                            {

        //                                loc1 = IMP.Element("StartAirp").Value,
        //                                loc2 = IMP.Element("EndAirp").Value,
        //                                Depdate = IMP.Element("Dt").Value.ToString().Substring(6, 2) + IMP.Element("Dt").Value.ToString().Substring(4, 2) + IMP.Element("Dt").Value.ToString().Substring(2, 2),//.Substring(5,6),
        //                                Deptime = IMP.Element("StartTm").Value,
        //                                ArrDate = (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(3, 2) + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(0, 2) + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(8, 2),
        //                                Arrtime = IMP.Element("EndTm").Value,
        //                                Airline = IMP.Element("AirV").Value,
        //                                FlightNumber = IMP.Element("FltNum").Value,
        //                                RBD = IMP.Element("BIC").Value,
        //                                Status = IMP.Element("Status").Value
        //                            };
        //        var PAXNAME = from l in xdoc.Descendants("PNRBFRetrieve").Descendants("LNameInfo")
        //                      select new
        //                      {
        //                          LName = l.Element("LName").Value,
        //                          LNameNo = l.Element("LNameNum").Value,
        //                          NoOfPax = int.Parse(l.Element("NumPsgrs").Value)
        //                      };
        //        int i = 0;
        //        ArrayList a = new ArrayList();
        //        foreach (var ln in PAXNAME)
        //        {
        //            int j;
        //            for (j = 1; j <= ln.NoOfPax; j++)
        //            {
        //                i = i + 1;
        //                var fn = (from p in xdoc.Descendants("PNRBFRetrieve").Descendants("FNameInfo")
        //                          where p.Element("AbsNameNum").Value == i.ToString()
        //                          select p).ToList();
        //                a.Add(
        //                    new st
        //                    {
        //                        indx = i,
        //                        FName = fn[0].Element("FName").Value,
        //                        LName = ln.LName
        //                    });
        //            }
        //        }
        //        var dfdk = from st sss in a
        //                   select sss;
        //        DataTable Segment = new DataTable("SegmentDetails");
        //        Segment = LINQToDataTable(FLIGHTSEGMENT);
        //        DataTable PaxName = new DataTable("TktDetails");
        //        PaxName = LINQToDataTable(dfdk);
        //        if (Segment.Rows.Count > 0)
        //        {
        //            if (Segment.Rows[0]["Status"].ToString().Contains("HK") || Segment.Rows[0]["Status"].ToString().Contains("KL") || Segment.Rows[0]["Status"].ToString().Contains("KK") || Segment.Rows[0]["Status"].ToString().Contains("TK"))
        //            {
        //                dspnr.Tables.Add(Segment);
        //                dspnr.Tables.Add(PaxName);
        //            }
        //            else
        //            {

        //                dt1.Columns.Add("CancelStatus", typeof(String));
        //                dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
        //                dspnr.Tables.Add(dt1);
        //            }
        //        }
        //        else
        //        {
        //            dt1.Columns.Add("CancelStatus", typeof(String));
        //            dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
        //            dspnr.Tables.Add(dt1);

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        dt1.Columns.Add("CancelStatus", typeof(String));
        //        dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
        //        dspnr.Tables.Add(dt1);
        //    }


        //    return dspnr;
        //}
        public DataSet PNRDetailsGAL(string PNR, string Trip, string triptype)
        {
            gotochk:
            RandomKeyGenerator rk = new RandomKeyGenerator();
            string trackid = rk.Generate();
            trackid = trackid.Trim();
            string ckord = "";
            ckord =  CheckOrderIdExtOrNot(trackid);
            if (ckord == "1")
            {
                goto gotochk; 
            }
            // Dim RK As New RandomKeyGenerator()
            //Dim FareBreak_Row As New Hashtable()
            //TRANSID = RK.Generate()
            FlightSearchResults objFS = new FlightSearchResults();

            STD.BAL.FlightCommonBAL FCBAL1 = new STD.BAL.FlightCommonBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
            List<STD.Shared.UserFlightSearch> FCSHARED = new List<STD.Shared.UserFlightSearch>();
            FCSHARED = FCBAL1.GetUserFlightSearch(HttpContext.Current.Session["UID"].ToString(), HttpContext.Current.Session["User_Type"].ToString());

            DataSet dspnr = new DataSet();
            DataTable dt1 = new DataTable("CancelStatus");
        
            try
            {
                //STD.BAL.FltRequest FltRq = new STD.BAL.FltRequest();
                STD.BAL.FlightCommonBAL FCBAL = new STD.BAL.FlightCommonBAL();
                string Req = "";
                CrdList = STD.BAL.Data.GetProviderCrd(ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                objFltReq = new STD.BAL.FltRequest(searchInputs, CrdList);
                string url = "", userid = "", Pwd = "";
                url = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].AvailabilityURL;
                userid = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].UserID;
                Pwd = ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].Password;
                string strpnr = ""; string Token = ""; string STTReq = ""; //string PNR = "2BK96Q";
                Req = FCBAL.PostXml(url, BeginSessionReq(), userid, Pwd, "http://webservices.galileo.com/BeginSession");
                Token = GetTokenFromSessionStratResponse(Req);
                if (Trip == "D")
                {
                    STTReq = objFltReq.TerminalTransaction(Token, "Q/50+*RL-" + PNR);
                }
                else
                {
                    STTReq = objFltReq.TerminalTransaction(Token, "Q/50+*RL-" + PNR);
                }

                string Res = FCBAL.PostXml(url, STTReq, userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
                strpnr = FCBAL.PostXml(url, objFltReq.RetriveCurrentPnrReq(Token,"imp"), userid, Pwd, "http://webservices.galileo.com/SubmitXmlOnSession");
      
               // string strPath = System.Web.HttpContext.Current.Server.MapPath("\\CHECK.xml");
               // XDocument xdoc = XDocument.Load(strPath);
                Resimportpnr(trackid, strpnr, Req + "zzz_z" + STTReq);
                FCBAL.PostXml(url, objFltReq.EndSessionReq(Token), userid, Pwd, "http://webservices.galileo.com/EndSession");
                XNamespace ns = "http://webservices.galileo.com";
                XNamespace soapenv = "http://schemas.xmlsoap.org/soap/envelope/";
                XDocument xdoc = XDocument.Parse(strpnr.Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty));
                //Replace("xmlns=\"http://webservices.galileo.com\"", String.Empty)
                DataTable fltheader = new DataTable();
                DataTable fltpaxDetails = new DataTable();
                DataTable FltFareDetails = new DataTable();
                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString()))
                {
                    conn.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT TOP 0 * FROM FltHeader", conn))
                    {
                        adapter.Fill(fltheader);
                    };
                    using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT TOP 0 * FROM FltPaxDetails", conn))
                    {
                        adapter.Fill(fltpaxDetails);
                    };
                    using (SqlDataAdapter adapter = new SqlDataAdapter("SELECT TOP 0 * FROM FltFareDetails", conn))
                    {
                        adapter.Fill(FltFareDetails);
                    };


                    DataColumn GSTColumn = new DataColumn("GST", typeof(decimal));
                    /* You created a DataColumn here which is the first thing to do when you want to add a column to a DataTable */
                    FltFareDetails.Columns.Add(GSTColumn);

                    conn.Close();
                };
                XElement PNRBFManagement_43 = xdoc.Element(soapenv + "Envelope").Element(soapenv + "Body").Element("SubmitXmlOnSessionResponse").Element("SubmitXmlOnSessionResult").Element("PNRBFManagement_43");
                XElement PNRBFRetrieve = PNRBFManagement_43.Element("PNRBFRetrieve");
                XElement DocProdDisplayStoredQuote = PNRBFManagement_43.Element("PNRBFRetrieve");

                string AIRLINE_PNR ="";
                string VCRR = "";
                try
                {
                    AIRLINE_PNR = PNRBFRetrieve.Element("VndRecLocs").Element("RecLocInfoAry").Element("RecLocInfo").Element("RecLoc").Value;
                    VCRR = PNRBFRetrieve.Element("VndRecLocs").Element("RecLocInfoAry").Element("RecLocInfo").Element("Vnd").Value;
                }
                catch(Exception ex)
                {
                    AIRLINE_PNR = "NULL"; VCRR = "NULL";
                }

                //string Mobile = PNRBFRetrieve.Element("OSI").Element("OSIMsg").Value;
                //string Email = PNRBFRetrieve.Element("Email").Element("Data").Value;
                int INF_No = 0;
                int CHD_NO = 0;
                IEnumerable<XElement> INFCOUNK = xdoc.Descendants("DocProdDisplayStoredQuote").Descendants("AgntEnteredPsgrDescInfo").Where(x => x.Element("AgntEnteredPsgrDesc").Value == "INF");

               foreach(var inf in INFCOUNK)
                {
                    INF_No = 1;
                
                }

               IEnumerable<XElement> CHIDCOUNK = xdoc.Descendants("DocProdDisplayStoredQuote").Descendants("AgntEnteredPsgrDescInfo").Where(x => x.Element("AgntEnteredPsgrDesc").Value.ToUpper().Contains("C"));

               foreach (var CHDT in CHIDCOUNK)
               {
                   CHD_NO = 1;

               }

               string chekinfchd = "0";



                if(CHD_NO == 1 && INF_No ==1)
                {
                    chekinfchd = "3";

                }
                if (CHD_NO == 0 && INF_No == 1)
                {
                    chekinfchd = "2";

                }
                



                 IEnumerable<XElement> ficlist = xdoc.Descendants("DocProdDisplayStoredQuote").Descendants("SegRelatedInfo");

                var FLIGHTSEGMENT = from IMP in xdoc.Descendants("SubmitXmlOnSessionResult").Elements("PNRBFManagement_43").Elements("PNRBFRetrieve").Elements("AirSeg")
                                    select new
                                    {
                                        Segnum  = IMP.Element("SegNum").Value,
                                        loc1 = IMP.Element("StartAirp").Value,
                                        loc2 = IMP.Element("EndAirp").Value,
                                        Depdate = IMP.Element("Dt").Value.ToString().Substring(6, 2) + IMP.Element("Dt").Value.ToString().Substring(4, 2) + IMP.Element("Dt").Value.ToString().Substring(2, 2),//.Substring(5,6),
                                        Deptime = IMP.Element("StartTm").Value.Length == 3 ? "0" + IMP.Element("StartTm").Value : IMP.Element("StartTm").Value.Length == 2 ? "00" + IMP.Element("StartTm").Value : IMP.Element("StartTm").Value.Length == 1 ? "000" + IMP.Element("StartTm").Value : IMP.Element("StartTm").Value,
                                        //ArrDateee = (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(3, 2) + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(0, 2) + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value))).ToString().Substring(8, 2),
                                        ADate = (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value)).Day).ToString() + "/" + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value)).Month).ToString() + "/" + (Convert.ToDateTime(IMP.Element("Dt").Value.ToString().Substring(0, 4) + "-" + IMP.Element("Dt").Value.ToString().Substring(4, 2) + "-" + IMP.Element("Dt").Value.ToString().Substring(6, 2)).AddDays(Convert.ToInt32(IMP.Element("DayChg").Value)).Year).ToString(),
                                        DateChange = IMP.Element("DayChg").Value,
                                        Arrtime = IMP.Element("EndTm").Value.Length == 3 ? "0" + IMP.Element("EndTm").Value : IMP.Element("EndTm").Value.Length == 2 ? "00" + IMP.Element("EndTm").Value : IMP.Element("EndTm").Value.Length == 1 ? "000" + IMP.Element("EndTm").Value : IMP.Element("EndTm").Value,
                                        Airline = IMP.Element("AirV").Value,
                                        FlightNumber = IMP.Element("FltNum").Value,
                                        RBD = IMP.Element("BIC").Value,
                                        Status = IMP.Element("Status").Value,
                                        AdtFareBasis = ficlist.Where(x => x.Element("UniqueKey").Value == "1" && x.Element("RelSegNum").Value ==IMP.Element("SegNum").Value).ElementAt(0).Element("FIC").Value,
                                        ChdFareBasis = CHD_NO > 0 ? ficlist.Where(x => x.Element("UniqueKey").Value == "2" && x.Element("RelSegNum").Value ==IMP.Element("SegNum").Value).ElementAt(0).Element("FIC").Value : "",
                                        InfFareBasis = INF_No > 0 ? ficlist.Where(x => x.Element("UniqueKey").Value == chekinfchd && x.Element("RelSegNum").Value == IMP.Element("SegNum").Value).ElementAt(0).Element("FIC").Value : "",
                                    };

                //XElement xelement = XElement.Load("..\\..\\Employees.xml");
                //var stCnt = from address in xelement.Elements("Employee")
                //            where (string)address.Element("Address").Element("State") == "CA"
                //            select address;


                var PAXNAME = from l in xdoc.Descendants("PNRBFRetrieve").Descendants("LNameInfo")
                              select new
                              {
                                  LName = l.Element("LName").Value,
                                  LNameNo = l.Element("LNameNum").Value,
                                  NoOfPax = int.Parse(l.Element("NumPsgrs").Value)
                              };
                int i = 0;
                ArrayList a = new ArrayList();
                foreach (var ln in PAXNAME)
                {
                    int j;
                    for (j = 1; j <= ln.NoOfPax; j++)
                    {
                        i = i + 1;
                        var fn = (from p in xdoc.Descendants("PNRBFRetrieve").Descendants("FNameInfo")
                                  where p.Element("AbsNameNum").Value == i.ToString()
                                  select p).ToList();
                        IEnumerable<XElement> ADTpxt = xdoc.Descendants("DocProdDisplayStoredQuote").Descendants("AgntEnteredPsgrDescInfo"); //.Where(x => x.Element("AgntEnteredPsgrDesc").Value == "ADT");
                        string paxnumberyTPE = "";
                        int unikey = 0;
                        //foreach( var paxidsdf in ADTpxt.Where( x => x.Elements("ApplesToAry").Element("AbsNameNum").Value ==i.ToString()))
                        foreach (var paxidsdf in ADTpxt)
                        {
                            foreach (var paxids in paxidsdf.Element("ApplesToAry").Elements("AppliesTo").Where(x => x.Element("AbsNameNum").Value == i.ToString()))
                            {
                                paxnumberyTPE = paxidsdf.Element("AgntEnteredPsgrDesc").Value;
                                unikey = Convert.ToInt32(paxidsdf.Element("UniqueKey").Value);

                                if (paxnumberyTPE.ToUpper().Contains("CNN") || paxnumberyTPE.ToUpper().Contains("C"))
                                { 
                                  paxnumberyTPE = "CNN";
                                }
                            }
                        }
                        string dob_ = "";
                        IEnumerable<XElement> DOB = PNRBFRetrieve.Descendants("NameRmkInfo");

                        if(DOB.Count() > 0)
                        {
                           foreach(var GETDOB in DOB.Where(x => x.Element("AbsNameNum").Value == i.ToString()))
                           {
                               dob_ = GETDOB.Element("NameRmk").Value;

                           }

                        }



                        a.Add(
                           new st
                           {
                               indx = i,
                               FName = fn[0].Element("FName").Value,
                               paxtype = paxnumberyTPE,
                               unqkey = unikey,
                               LName = ln.LName,
                               DOB = dob_
                           });
                    }
                }
                var dfdk = from st sss in a
                           select sss;
                DataTable Segment = new DataTable("SegmentDetails");
                Segment = LINQToDataTable(FLIGHTSEGMENT);
                DataTable PaxName = new DataTable("TktDetails");
                PaxName = LINQToDataTable(dfdk);
                if (Segment.Rows.Count > 0)
                {
                    if (Segment.Rows[0]["Status"].ToString().Contains("HK") || Segment.Rows[0]["Status"].ToString().Contains("KL") || Segment.Rows[0]["Status"].ToString().Contains("KK") || Segment.Rows[0]["Status"].ToString().Contains("TK"))
                    {
                        Segment.Columns.Add("ArrDate", typeof(System.String));
                        Segment.Columns.Add("ArrDate_", typeof(System.String));
                        Segment.Columns.Add("DepDate_", typeof(System.String));
                        Segment.Columns.Add("ArrTime_", typeof(System.String));
                        Segment.Columns.Add("depTime_", typeof(System.String));
                        foreach (DataRow dr in Segment.Rows)
                        {
                            string[] al = dr["ADate"].ToString().Split('/');
                            string date = al[0]; string mon = al[1];
                            if (al[0].Length == 1)
                                date = "0" + al[0];
                            if (al[1].Length == 1)
                                mon = "0" + al[1];
                            string year = al[2].Substring(2, 2);
                            string ArrivalDate = date + mon + year;
                            dr["ArrDate"] = ArrivalDate;


                            if(dr["AdtFareBasis"].ToString().Contains("/"))
                            dr["AdtFareBasis"] = dr["AdtFareBasis"].ToString().Split('/')[0];


                            if (CHD_NO > 0)
                            {
                                if (dr["ChdFareBasis"].ToString().Contains("/"))
                                    dr["ChdFareBasis"] = dr["ChdFareBasis"].ToString().Split('/')[0];
                            }


                            if (INF_No > 0)
                            {
                                if (dr["InfFareBasis"].ToString().Contains("/"))
                                    dr["InfFareBasis"] = dr["InfFareBasis"].ToString().Split('/')[0];
                            }


                            dr["ArrTime_"] = dr["Arrtime"].ToString().Substring(0, 2) + ":" + dr["Arrtime"].ToString().Substring(2, 2);
                            dr["depTime_"] = dr["Deptime"].ToString().Substring(0, 2) + ":" + dr["Deptime"].ToString().Substring(2, 2);

                            dr["ArrDate_"] = ArrivalDate.ToString().Substring(0, 2) + "-" + ArrivalDate.ToString().Substring(2, 2) + "-" +ArrivalDate.ToString().Substring(4, 2);
                            dr["DepDate_"] = dr["Depdate"].ToString().Substring(0, 2) + "-" + dr["Depdate"].ToString().Substring(2, 2) + "-" + dr["Depdate"].ToString().Substring(4, 2);
  
                        }

                       dspnr.Tables.Add(Segment);
                       dspnr.Tables.Add(PaxName);
                    }
                    else
                    {

                        dt1.Columns.Add("CancelStatus", typeof(String));
                        dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
                        dspnr.Tables.Add(dt1);
                    }
                }
                else
                {
                    dt1.Columns.Add("CancelStatus", typeof(String));
                    dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
                    dspnr.Tables.Add(dt1);

                }


                string sector = "";
                bool sec = false;
                //if (Trip == "I" && triptype =="O")
                //{
                //  sector=  Segment.Rows[0]["loc1"].ToString() + ":" + Segment.Rows[Segment.Rows.Count - 1]["loc2"].ToString();
                //}
                //if (Trip == "I" && triptype == "R")
                //{
                //    sector = Segment.Rows[0]["loc1"].ToString() + ":" + Segment.Rows[Segment.Rows.Count - 1]["loc2"].ToString() + ":" + Segment.Rows[0]["loc1"].ToString();
                //}

                //if (Trip == "D" && triptype == "O")
                //{
                //    sector = Segment.Rows[0]["loc1"].ToString() + ":" + Segment.Rows[Segment.Rows.Count - 1]["loc2"].ToString();
                //}
                //if (Trip == "D" && triptype == "R")
                //{
                //    sector = Segment.Rows[0]["loc1"].ToString() + ":" + Segment.Rows[Segment.Rows.Count - 1]["loc2"].ToString() + ":" + Segment.Rows[0]["loc1"].ToString();
                //}
                //else
                //{
                //    foreach (var sem in FLIGHTSEGMENT)
                //    {
                //        if (FLIGHTSEGMENT.Count() == 1)
                //        {

                //            sector = sem.loc1 + ":" + sem.loc2;
                //        }
                //        else
                //        {
                //            if (sec == false)
                //            {
                //                sector = sem.loc1;
                //                sec = true;
                //            }

                //            else
                //            {
                //                sector += ":" + sem.loc2;
                //            }
                //        }
                //    }

                //}
                //add fltfareDetalis

                string pty = "paxtype";
                DataTable paxnamecopy = PaxName.Copy();
                DataTable fltpax = RemoveDuplicateRows(paxnamecopy, pty);
                int ADTCOUNT = (PaxName.Select().Where(s => s["paxtype"].ToString().ToUpper() == "ADT").Count() != 0 ? PaxName.Select().Where(s => s["paxtype"].ToString().ToUpper() == "ADT").Count() : 0);
                int CHDCOUNT = (PaxName.Select().Where(s => s["paxtype"].ToString().ToUpper() == "CNN").Count() != 0 ? PaxName.Select().Where(s => s["paxtype"].ToString().ToUpper() == "CNN").Count() : 0);
                int INFCOUNT = (PaxName.Select().Where(s => s["paxtype"].ToString().ToUpper() == "INF").Count() != 0 ? PaxName.Select().Where(s => s["paxtype"].ToString().ToUpper() == "INF").Count() : 0);

                int k = 0;
                for (int ifre = 0; fltpax.Rows.Count > ifre; ifre++)
                {
                    IEnumerable<XElement> pfare = xdoc.Descendants("DocProdDisplayStoredQuote").Descendants("GenQuoteDetails");
                    foreach (var pxfr in pfare.Where(X => X.Element("UniqueKey").Value == fltpax.Rows[ifre]["unqkey"].ToString()))
                    {
                       
                        DataRow addfareDetails = FltFareDetails.NewRow();
                        addfareDetails["FareId"] = k;
                        addfareDetails["OrderId"] = trackid.Trim();
                        k= k + 1;
                        if (pxfr.Elements("BaseFareCurrency").First().Value == "INR")
                            addfareDetails["BaseFare"] = float.Parse(pxfr.Elements("BaseFareAmt").First().Value);
                        else
                            addfareDetails["BaseFare"] = float.Parse(pxfr.Elements("EquivAmt").First().Value);

                        addfareDetails["TotalFare"] = float.Parse(pxfr.Elements("TotAmt").First().Value);

                        var adt = from item in pxfr.Elements("TaxDataAry").Elements("TaxData")
                                  select new
                                  {
                                      adtAmt = item.Element("Amt").Value,
                                      adtCon = item.Element("Country").Value
                                  };

                        addfareDetails["YQ"] = 0;
                        addfareDetails["WO"] = 0;
                        addfareDetails["GST"] = 0;
                        addfareDetails["YR"] = 0;
                        float OT = 0;
                        float TotalTax = 0;
                        foreach (var adtd in adt)
                        {
                            if (adtd.adtCon == "YQ")
                                addfareDetails["YQ"] = float.Parse(adtd.adtAmt);
                            else if (adtd.adtCon == "WO")
                                addfareDetails["WO"] = float.Parse(adtd.adtAmt);
                            else if (adtd.adtCon == "K3")
                                addfareDetails["GST"] = float.Parse(adtd.adtAmt);
                            else if (adtd.adtCon == "YR")
                                addfareDetails["YR"] = float.Parse(adtd.adtAmt);
                            else
                                OT = OT + float.Parse(adtd.adtAmt);
                            //if (adtd.adtCon != "YQ")

                            TotalTax = TotalTax + float.Parse(adtd.adtAmt);


                            addfareDetails["Qtax"] = 0; //asking

                        }

                        addfareDetails["OT"] = OT;    //add servicecharge
                        addfareDetails["TotalTax"] = TotalTax;
                       
                        addfareDetails["ServiceTax"] = 0;
                        addfareDetails["TranFee"] = 0;
                        addfareDetails["AdminMrk"] = 0;
                        addfareDetails["AgentMrk"] = 0;
                        addfareDetails["DistrMrk"] = 0;
                        addfareDetails["TotalDiscount"] = 0;
                        addfareDetails["PLb"] = 0;
                        addfareDetails["Discount"] = 0;
                        addfareDetails["DistrMrk"] = 0;
                        addfareDetails["TotalDiscount"] = 0;
                        addfareDetails["CashBack"] = 0;
                        addfareDetails["Tds"] = 0;
                        addfareDetails["TdsOn"] = 0;
                        addfareDetails["TotalAfterDis"] = 0;
                        addfareDetails["AvlBalance"] = 0;
                        addfareDetails["PaxType"] = fltpax.Rows[ifre]["paxtype"].ToString();

                        addfareDetails["CreateDate"] = DateTime.Today;
                        addfareDetails["UpdateDate"] = DateTime.MinValue.AddHours(9);
                        addfareDetails["TdsOn"] = 0;
                        addfareDetails["YFLAG"] = false;
                        addfareDetails["YCRN"] = false;
                        addfareDetails["Y_CAN_FARE"] = false;
                        addfareDetails["MgtFee"] = 0;
                        addfareDetails["ServiceTax1"] = 0;
                        addfareDetails["Discount1"] = 0;
                        addfareDetails["FareType"] = "";
                        FltFareDetails.Rows.Add(addfareDetails);
                    }
                }

                //end fltfareddetails

                //add fltPaxdetails
                for (int ipx = 0; PaxName.Rows.Count > ipx; ipx++)
                {
                    string firstname = "";
                    string Gender = "";
                    string title = "";

                    if (PaxName.Rows[ipx]["paxtype"].ToString() == "ADT")
                    {
                        if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MRS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 3, 3);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 3, 3);
                            Gender = "F";
                        }
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MR"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 2, 2);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 2, 2);
                            Gender = "M";
                        }
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 2, 2);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 2, 2);
                            Gender = "F";
                        }
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MISS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 4, 4);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 4, 4);
                            Gender = "F";
                        }  
                        else
                        {
                            title = "";
                            firstname = PaxName.Rows[ipx]["FName"].ToString();
                            Gender = "";
                        }
                    
                    }
                    else if (PaxName.Rows[ipx]["paxtype"].ToString() == "CNN")
                    {
                       if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MRS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 3, 3);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 3, 3);
                            Gender = "F";
                        }

                       else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MR"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 2, 2);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 2, 2);
                            Gender = "M";
                        }
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MSTR"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 4, 4);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 4, 4);
                            Gender = "M";
                        }
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 2, 2);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 2, 2);
                            Gender = "F";
                        }
                      
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MISS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 4, 4);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 4, 4);
                            Gender = "F";
                        }  
                        else
                        {
                            title = "";
                            firstname = PaxName.Rows[ipx]["FName"].ToString();
                            Gender = "";
                           
                        }                  
                    }
                    else
                    {
                         if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MRS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 3, 3);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 3, 3);
                            Gender = "F";
                        }
                         else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MR"))
                         {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 2, 2);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 2, 2);
                            Gender = "M";
                         }
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MSTR"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 4, 4);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 4, 4);
                            Gender = "M";
                        }
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 2, 2);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 2, 2);
                            Gender = "F";
                        }
                     
                        else if (PaxName.Rows[ipx]["FName"].ToString().ToUpper().Contains("MISS"))
                        {
                            title = PaxName.Rows[ipx]["FName"].ToString().ToUpper().Substring(PaxName.Rows[ipx]["FName"].ToString().ToUpper().Length - 4, 4);
                            firstname = PaxName.Rows[ipx]["FName"].ToString().Remove(PaxName.Rows[ipx]["FName"].ToString().Length - 4, 4);
                            Gender = "F";
                        }
                     
                        else
                        {
                            title = "";
                            firstname = PaxName.Rows[ipx]["FName"].ToString();
                            Gender = "";

                        }                  
                    }

                    DataRow addpaxdetails = fltpaxDetails.NewRow();
                    addpaxdetails["PaxId"] = PaxName.Rows[ipx]["indx"].ToString();
                    addpaxdetails["OrderId"] = trackid.Trim();
                    addpaxdetails["TransactionId"] = "";
                    addpaxdetails["Title"] = title.Trim();
                    addpaxdetails["FName"] = firstname.Trim();
                    addpaxdetails["MName"] = "";
                    addpaxdetails["LName"] = PaxName.Rows[ipx]["LName"].ToString();
                    addpaxdetails["PaxType"] = PaxName.Rows[ipx]["paxtype"].ToString();
                    addpaxdetails["TicketNumber"] = "";
                    addpaxdetails["DOB"] = PaxName.Rows[ipx]["DOB"].ToString();
                    addpaxdetails["FFNumber"] = "";
                    addpaxdetails["FFAirline"] = "";
                    addpaxdetails["MealType"] = "";
                    addpaxdetails["SeatType"] = "";
                    addpaxdetails["IsPrimary"] = false;
                    addpaxdetails["InfAssociatePaxName"] = "";
                    addpaxdetails["MordifyStatus"] = "";
                    addpaxdetails["CreateDate"] = DateTime.Today;
                    addpaxdetails["UpdateDate"] = DateTime.MinValue.AddHours(9);
                    addpaxdetails["YFLAG"] = false;
                    addpaxdetails["YCRN"] = false;
                    addpaxdetails["Gender"] = Gender.Trim();
                    addpaxdetails["PassportExpireDate"] = "";
                    addpaxdetails["PassportNo"] = "";
                    addpaxdetails["IssueCountryCode"] = "";
                    addpaxdetails["NationalityCode"] = "";
                    fltpaxDetails.Rows.Add(addpaxdetails);
                }

                ///ADD FLTHEARDER TABLE
                ///

                DataTable Primarypaxresults = fltpaxDetails.Select("PaxId = 1").CopyToDataTable();
                DataRow addrow = fltheader.NewRow();
                addrow["HeaderId"] = "001";
                addrow["OrderId"] = trackid.Trim();
                addrow["sector"] = sector.Trim();
                addrow["Status"] = "Booked";
                addrow["MordifyStatus"] = "Booked";
                addrow["GdsPnr"] = PNR.Trim();
                addrow["AirlinePnr"] = AIRLINE_PNR;
                addrow["VC"] = VCRR;
                addrow["Duration"] = "";
                addrow["TripType"] = "";
                addrow["Trip"] = Trip.Trim();
                addrow["TourCode"] = "";
                addrow["TotalBookingCost"] = 0.00;
                addrow["TotalAfterDis"] = 0.00;
                addrow["SFDis"] = 0.00;
                addrow["AdditionalMarkup"] = 0.00;
                addrow["Adult"] = ADTCOUNT;
                addrow["Child"] = CHDCOUNT;
                addrow["Infant"] = INFCOUNT;
                addrow["AgentId"] = "";
                addrow["AgencyName"] = "";
                addrow["DistrId"] = "";
                addrow["ExecutiveId"] = "";
                addrow["PaymentType"] = "";
                addrow["PgTitle"] = Primarypaxresults.Rows[0]["Title"].ToString();
                addrow["PgFName"] = Primarypaxresults.Rows[0]["FName"].ToString();
                addrow["PgLName"] = Primarypaxresults.Rows[0]["LName"].ToString();
                addrow["PgMobile"] = "__"; //Mobile.Split('-')[1].ToString();
                addrow["PgEmail"] = "__";
                addrow["Remark"] = "";
                addrow["RejectedRemark"] = "";
                addrow["CreateDate"] = DateTime.Today;
                addrow["UpdateDate"] = DateTime.MinValue.AddHours(9);
             
                fltheader.Rows.Add(addrow);

               
                ////addd table to dataset
                dspnr.Tables.Add(fltpaxDetails);
                dspnr.Tables.Add(FltFareDetails);
                dspnr.Tables.Add(fltheader);
            }
            catch (Exception ex)
            {

                Resimportpnr(trackid,ex.Message.ToString(),"");
                dt1.Columns.Add("CancelStatus", typeof(String));
                dt1.Rows.Add("Either The PNR Has Been Canceled Or Some Problem In The PNR");
                dspnr.Tables.Add(dt1);
            }
          

            return dspnr;
        }
        private string SoapStart(string opt, string Token)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
            XmlBuilder.Append("<soap:Body>");

            if (opt == "BS")
            {
                XmlBuilder.Append("<BeginSession xmlns='http://webservices.galileo.com'>");
                XmlBuilder.Append("<Profile>" + ((from crd in CrdList where crd.Provider == "1G" select crd).ToList())[0].CorporateID + "</Profile>");
                XmlBuilder.Append("<LDVOverride />");
            }


            return XmlBuilder.ToString();
        }
        private string SoapEnd(string opt)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            if (opt == "SS")
            {
                XmlBuilder.Append("</SubmitXml>");
            }
            else if (opt == "MS")
            {
                XmlBuilder.Append("</Requests>");
                XmlBuilder.Append("</MultiSubmitXml>");
            }
            else if (opt == "BS")
            {
                XmlBuilder.Append("</BeginSession>");
            }
            else if (opt == "ES")
            {
                XmlBuilder.Append("</EndSession>");
            }
            else if (opt == "SONS")
            {
                XmlBuilder.Append("</SubmitXmlOnSession>");
            }
            else if (opt == "STT")
            {
                XmlBuilder.Append("</SubmitTerminalTransaction>");
            }
            XmlBuilder.Append("</soap:Body>");
            XmlBuilder.Append("</soap:Envelope>");
            return XmlBuilder.ToString();
        }
        public string BeginSessionReq()
        {
            StringBuilder XmlBuilder = new StringBuilder();
            XmlBuilder.Append(SoapStart("BS", ""));
            XmlBuilder.Append(SoapEnd("BS"));
            return XmlBuilder.ToString();
        }

        public DataTable ToDataTable<T>(IList<T> data)// T is any generic type
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));

            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        public DataTable RemoveDuplicateRows(DataTable table, string DistinctColumn)
        {
            try
            {
                ArrayList UniqueRecords = new ArrayList();
                ArrayList DuplicateRecords = new ArrayList();

                // Check if records is already added to UniqueRecords otherwise,
                // Add the records to DuplicateRecords
                foreach (DataRow dRow in table.Rows)
                {
                    if (UniqueRecords.Contains(dRow[DistinctColumn]))
                        DuplicateRecords.Add(dRow);
                    else
                        UniqueRecords.Add(dRow[DistinctColumn]);
                }

                // Remove duplicate rows from DataTable added to DuplicateRecords
                foreach (DataRow dRow in DuplicateRecords)
                {
                    table.Rows.Remove(dRow);
                }

                // Return the clean DataTable which contains unique records.
                return table;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public class st
        {
            public string FName { get; set; }
            public string LName { get; set; }
            public int indx { get; set; }
            public int unqkey { get; set; }
            public string paxtype { get; set; }
            public string DOB { get; set; }
        }
        public class FlightSearchResults : ICloneable
        {
            #region FlightDetails
            public string OrgDestFrom { get; set; }
            public string OrgDestTo { get; set; }
            public string DepartureLocation { get; set; }
            public string DepartureCityName { get; set; }
            public string DepAirportCode { get; set; }//added TO TABLE
            public string DepartureAirportName { get; set; }
            public string DepartureTerminal { get; set; }//added TO TABLE
            public string ArrivalLocation { get; set; }
            public string ArrivalCityName { get; set; }
            public string ArrAirportCode { get; set; }//added TO TABLE
            public string ArrivalAirportName { get; set; }
            public string ArrivalTerminal { get; set; }// added  TO TABLE
            public string DepartureDate { get; set; }
            public string Departure_Date { get; set; }
            public string DepartureTime { get; set; }
            public string ArrivalDate { get; set; }
            public string Arrival_Date { get; set; }
            public string ArrivalTime { get; set; }
            public int Adult { get; set; }
            public int Child { get; set; }
            public int Infant { get; set; }
            public int TotPax { get; set; }
            public string MarketingCarrier { get; set; }
            public string OperatingCarrier { get; set; }//added TO TABLE
            public string FlightIdentification { get; set; }
            public string ValiDatingCarrier { get; set; }
            public string AirLineName { get; set; }
            public string AvailableSeats { get; set; }
            public string AvailableSeats1 { get; set; }
            public string ElectronicTicketing { get; set; }// added TO TABLE
            public string ProductDetailQualifier { get; set; }////
            public string getVia { get; set; }
            public string BagInfo { get; set; }

            // public string AdtCabin { get; set; }//added TO TABLE
            string _AdtCabin;
            public string AdtCabin
            {
                get { return _AdtCabin ?? ""; }
                set { _AdtCabin = value; }
            }

            public string ChdCabin { get; set; }//added TO TABLE
            public string InfCabin { get; set; }//added TO TABLE
            public string AdtRbd { get; set; }//added TO TABLE
            public string ChdRbd { get; set; }//added TO TABLE
            public string InfRbd { get; set; }//added TO TABLE
            public string GSTAllowed { get; set; }
            public string AdtFareType { get; set; }//added TO TABLE
            public string AdtFarebasis { get; set; }//added TO TABLE
            public string ChdfareType { get; set; }//added TO TABLE
            public string ChdFarebasis { get; set; }//added TO TABLE
            public string InfFareType { get; set; }//added TO TABLE
            public string InfFarebasis { get; set; }//added TO TABLE -farebasis changed-to- Farebasis
            public string InfFar { get; set; }////
            public string ChdFar { get; set; }////
            public string AdtFar { get; set; }/////
            public string AdtBreakPoint { get; set; }
            public string AdtAvlStatus { get; set; }
            public string ChdBreakPoint { get; set; }
            public string ChdAvlStatus { get; set; }
            public string InfBreakPoint { get; set; }
            public string InfAvlStatus { get; set; }
            public string fareBasis { get; set; }
            public string FBPaxType { get; set; }
            public string RBD { get; set; }
            public string FareRule { get; set; }
            public string FareDet { get; set; }
            #endregion

            #region AdtFareDetails
            public float AdtFare { get; set; }
            public float AdtBfare { get; set; }
            public float AdtTax { get; set; }
            public float AdtFSur { get; set; }//add TO Adt_Tax LIKE :-YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#
            public float AdtYR { get; set; }
            public float AdtWO { get; set; }
            public float AdtIN { get; set; }
            [DefaultValue(0)]
            public float AdtQ { get; set; }
            public float AdtJN { get; set; }
            public float AdtOT { get; set; }//// added TO TABLE
            [DefaultValue(0)]
            public float AdtSrvTax { get; set; }   // added TO TABLE
            [DefaultValue(0)]
            public float AdtSrvTax1 { get; set; }   // added TO TABLE
            public float AdtEduCess { get; set; }
            public float AdtHighEduCess { get; set; }
            public float AdtTF { get; set; }
            public float ADTAdminMrk { get; set; }
            public float ADTAgentMrk { get; set; }
            public float ADTDistMrk { get; set; }
            [DefaultValue(0)]
            public float AdtDiscount { get; set; }  //-AdtComm
            [DefaultValue(0)]
            public float AdtDiscount1 { get; set; }
            public float AdtCB { get; set; }
            public float AdtTds { get; set; }
            [DefaultValue(0)]
            public float AdtMgtFee { get; set; }
            #endregion

            #region CHDFareDetails
            [DefaultValue(0)]
            public float ChdFare { get; set; }
            [DefaultValue(0)]
            public float ChdBFare { get; set; }
            [DefaultValue(0)]
            public float ChdTax { get; set; }
            [DefaultValue(0)]
            public float ChdFSur { get; set; }
            [DefaultValue(0)]
            public float ChdYR { get; set; }
            [DefaultValue(0)]
            public float ChdWO { get; set; }
            [DefaultValue(0)]
            public float ChdIN { get; set; }
            [DefaultValue(0)]
            public float ChdQ { get; set; }
            [DefaultValue(0)]
            public float ChdJN { get; set; }
            [DefaultValue(0)]
            public float ChdOT { get; set; }// added TO TABLE
            [DefaultValue(0)]
            public float ChdSrvTax { get; set; }// added TO TABLE 
            [DefaultValue(0)]
            public float ChdSrvTax1 { get; set; }// added TO TABLE 
            [DefaultValue(0)]
            public float ChdEduCess { get; set; }
            [DefaultValue(0)]
            public float ChdHighEduCess { get; set; }
            [DefaultValue(0)]
            public float ChdTF { get; set; }
            [DefaultValue(0)]
            public float CHDAdminMrk { get; set; }
            [DefaultValue(0)]
            public float CHDAgentMrk { get; set; }
            [DefaultValue(0)]
            public float CHDDistMrk { get; set; }
            [DefaultValue(0)]
            public float ChdTds { get; set; }
            [DefaultValue(0)]
            public float ChdDiscount { get; set; } //-ChdComm
            [DefaultValue(0)]
            public float ChdDiscount1 { get; set; } //-ChdComm
            [DefaultValue(0)]
            public float ChdCB { get; set; }
            [DefaultValue(0)]
            public float ChdMgtFee { get; set; }
            #endregion

            #region INFFareDetails
            [DefaultValue(0)]
            public float InfFare { get; set; }
            [DefaultValue(0)]
            public float InfBfare { get; set; }
            [DefaultValue(0)]
            public float InfTax { get; set; }
            [DefaultValue(0)]
            public float InfFSur { get; set; }//YQ:1#YR:1#WO:1#IN:1#Q:1#JN:1#OT:1#              
            [DefaultValue(0)]
            public float InfYR { get; set; }
            [DefaultValue(0)]
            public float InfWO { get; set; }
            [DefaultValue(0)]
            public float InfIN { get; set; }
            [DefaultValue(0)]
            public float InfQ { get; set; }
            [DefaultValue(0)]
            public float InfJN { get; set; }
            [DefaultValue(0)]
            public float InfOT { get; set; }// added TO TABLE         
            [DefaultValue(0)]
            public float InfSrvTax { get; set; }// added TO TABLE
            [DefaultValue(0)]
            public float InfSrvTax1 { get; set; }// added TO TABLE
            [DefaultValue(0)]
            public float InfEduCess { get; set; }////
            [DefaultValue(0)]
            public float InfHighEduCess { get; set; }////
            [DefaultValue(0)]
            public float InfTF { get; set; }
            [DefaultValue(0)]
            public float InfAdminMrk { get; set; }// added TO TABLE
            [DefaultValue(0)]
            public float InfAgentMrk { get; set; }// added TO TABLE
            [DefaultValue(0)]
            public float InfDistMrk { get; set; }
            [DefaultValue(0)]
            public float InfTds { get; set; }
            [DefaultValue(0)]
            public float InfDiscount { get; set; }//InfComm       
            [DefaultValue(0)]
            public float InfDiscount1 { get; set; }//InfComm 
            [DefaultValue(0)]
            public float InfCB { get; set; }// added TO TABLE
            [DefaultValue(0)]
            public float InfMgtFee { get; set; }
            #endregion

            #region TotFareDetails
            [DefaultValue(0)]
            public float TotBfare { get; set; }
            [DefaultValue(0)]
            public float TotalFare { get; set; }
            [DefaultValue(0)]
            public float TotalTax { get; set; }
            [DefaultValue(0)]
            public float TotalFuelSur { get; set; }
            [DefaultValue(0)]
            public float NetFare { get; set; }
            [DefaultValue(0)]
            public float TotMrkUp { get; set; }
            [DefaultValue(0)]
            public float STax { get; set; }
            [DefaultValue(0)]
            public float TFee { get; set; }
            [DefaultValue(0)]
            public float TotDis { get; set; }
            [DefaultValue(0)]
            public float TotCB { get; set; }
            [DefaultValue(0)]
            public float TotTds { get; set; }
            [DefaultValue(0)]
            public float TotMgtFee { get; set; }
            #endregion

            #region OtherDetails
            [DefaultValue(0)]
            public float IATAComm { get; set; }
            public string Searchvalue { get; set; }
            public int LineNumber { get; set; }
            public int Leg { get; set; }
            public string Flight { get; set; }
            public string TotDur { get; set; }
            public string TripType { get; set; }
            public string EQ { get; set; }
            public string Stops { get; set; }
            public string Trip { get; set; }
            public string Sector { get; set; }
            public string TripCnt { get; set; }
            //Not in List But in Table
            //public string Currency { get; set; }       
            public string sno { get; set; }
            public string depdatelcc { get; set; }
            public string arrdatelcc { get; set; }
            public float OriginalTF { get; set; }
            public float OriginalTT { get; set; }
            public string Track_id { get; set; }
            public string FType { get; set; }//FlightStatus-Ftype
            //Not in List But in Table
            //Adt_Tax- ,Chd_Tax,Inf_Tax-Need to be Calculated        
            public bool IsCorp { get; set; }
            public bool IsLTC { get; set; }
            public string Provider { get; set; }
            public string SubKey { get; set; }
            public string MainKey { get; set; }


            #endregion

            #region ICloneable Members

            public object Clone()
            {
                return this.MemberwiseClone();
            }


            #endregion
        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");


                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }
                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
     
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

        private float GetQTax(string fareConstruct)
        {
            string[] adtQtax;
            float Qt = 0, Roe = 0;
            float QTax = 0;
            try
            {
                adtQtax = Utility.Split(fareConstruct, " ");
                //foreach (var t in adtQtax)
                for (int q = 0; q <= adtQtax.Length - 1; q++)
                {
                    if (adtQtax[q].ToString().StartsWith("Q"))
                    {
                        if (Utility.IsNumeric(adtQtax[q].ToString().Replace("Q", "")))
                        {
                            Qt += float.Parse((adtQtax[q].ToString().Replace("Q", "")));
                        }
                        else
                        {
                            if (adtQtax[q].ToString().Contains("Q"))
                            {
                                if (adtQtax[q].ToString().Length == 1)
                                {
                                    MatchCollection matches = Regex.Matches(adtQtax[q + 1].ToString(), @"[+-]?\d+(\.\d+)?");
                                    if (matches.Count > 0)
                                    {
                                        if (Utility.IsNumeric(matches[0].Value))
                                        {
                                            Qt += float.Parse(matches[0].Value);
                                            //q += 1;
                                        }
                                    }
                                }
                                else
                                {
                                    string[] q1 = Utility.Split(adtQtax[q], "Q");
                                    int i;
                                    for (i = 0; i <= q1.Length - 1; i++)
                                    {
                                        if (Utility.IsNumeric(q1[i].Replace("Q", "")))
                                        {
                                            Qt += float.Parse((q1[i].Replace("Q", "")));
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else if (adtQtax[q].ToString().EndsWith("Q"))
                    {
                        MatchCollection matches = Regex.Matches(adtQtax[q + 1].ToString(), @"[+-]?\d+(\.\d+)?");
                        if (matches.Count > 0)
                        {
                            if (Utility.IsNumeric(matches[0].Value))
                            {
                                Qt += float.Parse(matches[0].Value);
                                //q += 1;
                            }
                        }
                    }
                    else if (adtQtax[q].ToString().StartsWith("ROE"))
                    {
                        if (Utility.IsNumeric(adtQtax[q].ToString().Replace("ROE", "")))
                        {
                            Roe = float.Parse((adtQtax[q].ToString().Replace("ROE", "")));
                        }
                    }
                }
                QTax = Qt * Roe;

            }
            catch (Exception ex)
            {
                QTax = 0;
            }
            return float.Parse(Math.Round(Convert.ToDouble(QTax.ToString()), 0).ToString());
        }

        public void Resimportpnr(string Trackid, string Res, string req)
        { 
            try
            {
            SqlConnection con_pnr = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString());

            if (con_pnr.State == ConnectionState.Closed)
            {
                con_pnr.Open();
            }
            SqlCommand cmd =  new SqlCommand();
            cmd.Connection = con_pnr;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "sp_log_ImportPnr";
            cmd.Parameters.AddWithValue("@OrderID", Trackid.Trim());
            cmd.Parameters.AddWithValue("@Request", req.Trim());
            cmd.Parameters.AddWithValue("@Response", Res.Trim());
            cmd.ExecuteNonQuery();
            con_pnr.Close();
            }
            catch(Exception ex)
            {
            
            }        
        }

        public string CheckOrderIdExtOrNot(string Trackid)
        {
            string CheckOrdID = "";
            try
            {
            SqlConnection con_pnr1 = new SqlConnection(ConfigurationManager.ConnectionStrings["myAmdDB"].ToString());
            if (con_pnr1.State == ConnectionState.Closed)
            {
                con_pnr1.Open();
            }
            SqlCommand cmd =  new SqlCommand();
           
            cmd.Connection = con_pnr1;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "OrderIDSamOrNot";
            cmd.Parameters.AddWithValue("@OrderID", Trackid.Trim());
            SqlDataReader dr =   cmd.ExecuteReader();
             while (dr.Read())
             {
                 CheckOrdID = (string)dr["OrderNo"];
             }
             con_pnr1.Close();
            }         
            catch(Exception ex)
            {
                CheckOrdID = "2";
            }

            return CheckOrdID;
        }

      
    }

}
