﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

/// <summary>
/// Summary description for PNRBFMgmt_Fare_CrossCheck
/// </summary>
[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
[System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
public partial class PNRBFManagement_25
{
  public PNRBFManagement_25PNRBFPrimaryBldChgMods PNRBFPrimaryBldChgMods { get; set; }
    
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgMods
{
    public PNRBFManagement_25PNRBFPrimaryBldChgModsItemAry ItemAry { get; set; }
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgModsItemAry
{
    public PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItem Item { get; set; }
}


[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItem
{
    public string DataBlkInd { get; set; }
    public PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQual NameQual { get; set; }
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQual
{
    public string EditTypeInd { get; set; }
    public string EditTypeIndAppliesTo { get; set; }
    public PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQual AddChgNameRmkQual { get; set; }
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQual
{
    public string NameType { get; set; }
    public string LNameID { get; set; }
    public string LName { get; set; }
    public string LNameRmk { get; set; }
    public PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQualNameTypeQual NameTypeQual { get; set; }
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQualNameTypeQual
{
    public PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQualNameTypeQualFNameAry FNameAry { get; set; }
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQualNameTypeQualFNameAry
{
    public PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQualNameTypeQualFNameAryFNameItem FNameItem { get; set; }
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25PNRBFPrimaryBldChgModsItemAryItemNameQualAddChgNameRmkQualNameTypeQualFNameAryFNameItem
{
    public string PsgrNum { get; set; }
    public string AbsNameNum { get; set; }
    public string FName { get; set; }
    public string FNameRmk { get; set; }
}


[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25AirSegSellMods
{

}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25AirSegSellModsItemAry
{

}

[System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "2.0.50727.1432")]
[System.SerializableAttribute()]
[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.ComponentModel.DesignerCategoryAttribute("code")]
[System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
public partial class PNRBFManagement_25StorePriceMods
{

}



