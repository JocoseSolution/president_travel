﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace STD.Shared
{
    public class G8ServiceQuote
    {
        public string LogicalFlightID { get; set; }
        public string DepartureDate { get; set; }
        public string AirportCode { get; set; }
        public string ServiceCode { get; set; }
        public string Cabin { get; set; }
        public string Category { get; set; }
        public string FareClass { get; set; }
        public string FareBasisCode { get; set; }
        public string DestinationAirportCode { get; set; }

    }
    public class G8ServiceQuoteResponse
    {
        public string CodeType { get; set; }
        public string SSRCategory { get; set; }
        public string DepartureDate { get; set; }
        public string Amount { get; set; }
        public string AmountWithTax { get; set; }
        public string Description { get; set; }
        public string LogicalFlightID { get; set; }
        public int PersonOrgID { get; set; }
        public string ServiceID { get; set; }
        public ServiceType ServiceType { get; set; }
    }

    public enum ServiceType
    {
        Meal,
        Seat,
        Baggage
    }

    public class G8BookFlightRequest
    {

        public string Address { get; set; }///Line 1 of the main passenger’s address
        public string Address2 { get; set; }///Optional. Line 2 of the main passenger’s address
        public string City { get; set; }///The main passenger’s city
        public string State { get; set; }///The main passenger’s state
        public string Postal { get; set; }///The main passenger’s zip code
        public string Country { get; set; }///The main passenger’s country
        public string CountryCode { get; set; }
        public string AreaCode { get; set; }
        public string ContactValue { get; set; }///The contact information for the passenger, usually phone number
        public string CarrierCurrency { get; set; }///The currency used by the carrier
        public string DisplayCurrency { get; set; }//The currency used in this dataset
        public string Email { get; set; }///The main passenger’s E-mail address
        public string Fax { get; set; }//Optional. The fax number for the main passenger
        public string Mobile { get; set; }///Optional. The mobile number for the main passenger
        public string IATANum { get; set; }///The IATANum of the Agent booking the reservation (Agent ID)
        public string WebBookingID { get; set; }///Security token GUID as returned from a previous call to GetSecurityGUID
        public string PromoCode { get; set; }///The promotional code for the booking, if one is used
        public Int64 ProfileID { get; set; }///The profile ID assigned to this reservation
        public G8Payment paymentDetails { get; set; }///Payment 
        public List<G8Segment> segment { get; set; }///segment
        public List<G8Person> CustomerList { get; set; }///customer list
        public string SecurityGUID { get; set; }


    }



    public class G8Payment
    {
        public int PaymentNum { get; set; }///Sequential payment number (typically 1)
        public string CompanyName { get; set; }///Optional. Name of the company on the credit card
        public string FirstName { get; set; }//First Name of Card Holder
        public string LastName { get; set; }///Last Name of Card Holder
        //public string CardType { get; set; }///The card type used: Ex: “VISA”
        public string PaymentCurrency { get; set; } ///The currency of the payment: Ex: “USD”
        public int ISOCurrency { get; set; }/// OPTIONAL: The ISO numeric value for the currency
        public decimal PaymentAmount { get; set; }///The amount of this payment. Web bookings must be the entire amount of the reseration.
        //public string CardNum { get; set; }///The Credit Card Number
        //public string CVCode { get; set; }///Three digit CV Code from the credit card
        //  public DateTime ExpirationDate { get; set; } ///Expiration date of the card. Expriation date of 05/05 would be expressed as “05/01/2005”`
        // public string IsTaCreditCard  { get; set; }
        public string VoucherNum { get; set; }
        public string OriginalCurrency { get; set; }///Original currency code of the payment
        public decimal OriginalAmount { get; set; }///Original amount of the payment
        public decimal ExchangeRate { get; set; }///Exchange rate used for conversion
        public DateTime ExchangeRateDate { get; set; }///Exchange rate date used for the conversion
        public string PaymentComment { get; set; }
        public DateTime PaymentDate { get; set; }


    }

    public class G8Segment
    {
        public int FareInformationID { get; set; }///The FareInformationID from the RAAvailabilityResponse for which you want to book.
        public string MarketingCode { get; set; }///Marketing code used for availability
        public List<G8ServiceQuoteResponse> SpecialServices { get; set; } ///
    }


    public class G8Person
    {

        public string LastName { get; set; }///Last Name of passenger
        public string FirstName { get; set; }///First Name of passenger
        public string MiddleName { get; set; } ///MiddleName
        public string Title { get; set; }///Title of passenger Ex: “MR”
        public string DOB { get; set; }///Age of passenger
        public string Gender { get; set; }
        public int PTCID { get; set; }///PTC ID of passenger: 1 = Adult, 6 = Child, 5 = Infant
        public int ContactType { get; set; }///Contact type for passenger: 0 = Home Phone, 1 = Work Phone, 2 = Mobile, 3 = Pager, 4 = E-Mail, 5 = Fax
        public string ContactNum { get; set; }///Actual number of contact type
        public Int64 ProfileID { get; set; }///optional The profile ID assigned to this passenger
        public int PersonOrgID { get; set; }
        public int TravelWithPersonOrgID { get; set; }  ///

    }
}
