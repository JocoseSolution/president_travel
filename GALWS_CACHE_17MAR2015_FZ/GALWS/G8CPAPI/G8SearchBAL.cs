﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections;
using STD.Shared;
using System.Data;
using System.Configuration;

namespace STD.BAL
{
    public class G8SearchBAL
    {
        public string ConStr { get; set; }
        public string LoginID { get; set; }
        public string LoginPass { get; set; }
        public string Iatanumber { get; set; }
        public string UserID { get; set; }
        public string UserPass { get; set; }
        public string SecurityGUID { get; set; }
        public string IP { get; set; }

        public G8SearchBAL(string ConnectionString, string loginId, string loginPass, string iatanumber, string userId, string userPass, string ip)
        {
            ConStr = ConnectionString;
            LoginID = loginId;
            LoginPass = loginPass;
            Iatanumber = iatanumber;
            UserID = userId;
            UserPass = userPass;
            IP = ip;

        }

        public ArrayList GetFlightAvailability(FlightSearch f, List<FlightCityList> CityList, List<FltSrvChargeList> SrvChargeList, float srvCharge, DataSet MarkupDs, bool IsNRTP, string CrdType)
        {
            ArrayList resultList = new ArrayList();
            string exep = "";
            try
            {
                G8SecurityToken objSecToken = new G8SecurityToken(LoginID, LoginPass);
                G8SvcAndMethodUrls SMUrl = new G8SvcAndMethodUrls();

                SecurityGUID = objSecToken.GetSecurityToken(SMUrl.RetrieveSecurityTokenUrl, SMUrl.SecurityTokenSvcUrl, ref exep);
                if (!string.IsNullOrEmpty(SecurityGUID))
                {
                    G8TravelAgent objTa = new G8TravelAgent(SecurityGUID, Iatanumber, UserID, UserPass, IP);

                    if (objTa.LoginTA(SMUrl.LoginTAUrl, SMUrl.TASvcUrl, ref exep))
                    {
                        decimal transfee = objTa.TranFeeTA(SMUrl.TransFeeTAUrl, SMUrl.TASvcUrl, ref exep);

                        if (transfee >= 0)
                        {
                            G8FareQuote objFareQ = new G8FareQuote(SecurityGUID, Iatanumber, UserID, UserPass);
                            resultList = ParseFareQoute(objFareQ.GetFareQuote(SMUrl.RetrieveFareQuotePRUrl, SMUrl.PricingSvcUrl, f, IsNRTP, ref exep), f, CityList, srvCharge, MarkupDs, SrvChargeList, CrdType);

                        }

                    }


                }



            }
            catch (Exception ex)
            {
                throw ex;
            }



            return resultList;
        }

        public ArrayList GetIXFlightAvailability(FlightSearch f, List<FlightCityList> CityList, List<FltSrvChargeList> SrvChargeList, float srvCharge, DataSet MarkupDs, bool IsNRTP, string CrdType)
        {
            ArrayList resultList = new ArrayList();
            string exep = "";
            try
            {
                G8SecurityToken objSecToken = new G8SecurityToken(LoginID, LoginPass);
                IXSvcAndMethodUrls SMUrl = new IXSvcAndMethodUrls();
                SecurityGUID = objSecToken.GetIXSecurityToken(SMUrl.RetrieveSecurityTokenUrl, SMUrl.SecurityTokenSvcUrl, ref exep);
                if (!string.IsNullOrEmpty(SecurityGUID))
                {
                    G8TravelAgent objTa = new G8TravelAgent(SecurityGUID, Iatanumber, UserID, UserPass, IP);
                    if (objTa.LoginIXTA(SMUrl.LoginTAUrl, SMUrl.TASvcUrl, ref exep))
                    {
                        decimal transfee = objTa.TranIXFeeTA(SMUrl.TransFeeTAUrl, SMUrl.TASvcUrl, ref exep);

                        if (transfee >= 0)
                        {
                            G8FareQuote objFareQ = new G8FareQuote(SecurityGUID, Iatanumber, UserID, UserPass);
                            resultList = ParseIXFareQoute(objFareQ.GetIXFareQuote(SMUrl.RetrieveFareQuotePRUrl, SMUrl.PricingSvcUrl, f, IsNRTP, ref exep), f, CityList, srvCharge, MarkupDs, SrvChargeList, CrdType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }



            return resultList;
        }

        private ArrayList ParseIXFareQoute(string responseXml, FlightSearch searchInput, List<FlightCityList> CityList, float srvCharge, DataSet MarkupDs, List<FltSrvChargeList> SrvChargeList, string CrdType)
        {
            ArrayList FinalList = new ArrayList();
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL FCBAL = new FlightCommonBAL(ConStr);

            List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> fsrListR = new List<FlightSearchResults>();

            float OtherTF = 0;
            OtherTF = Convert.ToInt32(ConfigurationManager.AppSettings["DOMIXTF"]);
            // srvCharge = srvCharge + OtherTF;
            // XDocument xmlDocq = XDocument.Load(@"E:\bhupinder 1.11.2014\d drive\Bhupender\FlyDubai\TestWeb\TestWeb\fareQOneWay.xml");//Parse(responseXml);
            //XNamespace ns = "http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Response";
            string str = responseXml.Replace("xmlns:a=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Response\"", "").Replace("a:", "").Replace("xmlns=\"http://tempuri.org/\"", "");

            XDocument xmlDoc = XDocument.Parse(str);

            IEnumerable<XElement> xlFSegOnword = null;
            IEnumerable<XElement> xlFSegReturn = null;


            if (searchInput.TripType == TripType.RoundTrip)
            {
                xlFSegOnword = from el in xmlDoc.Descendants("FlightSegments").Descendants("FlightSegment")
                               where el.Element("LegCount").Value == "1"
                               select el;

                xlFSegReturn = from el in xmlDoc.Descendants("FlightSegments").Descendants("FlightSegment")
                               where el.Element("LegCount").Value == "2"
                               select el;
            }
            else
            {
                xlFSegOnword = from el in xmlDoc.Descendants("FlightSegments").Descendants("FlightSegment")
                               select el;
            }

            IEnumerable<XElement> xlLegDetails = from el in xmlDoc.Descendants("LegDetails").Descendants("LegDetail")
                                                 select el;
            IEnumerable<XElement> xlSegmentDetails = from el in xmlDoc.Descendants("SegmentDetails").Descendants("SegmentDetail")
                                                     select el;
            IEnumerable<XElement> xlTaxDetails = from el in xmlDoc.Descendants("TaxDetails").Descendants("TaxDetail")
                                                 select el;
            if ((searchInput.TripType == TripType.RoundTrip && searchInput.RTF == true) || (searchInput.TripType == TripType.RoundTrip && searchInput.Trip == Trip.I))
            {
                #region OutBound Parsing

                int lineNum = 1;
                #region flight segment
                for (int fs = 0; fs < xlFSegOnword.Count(); fs++)
                {
                    XElement objFs = xlFSegOnword.ElementAt(fs);
                    IEnumerable<XElement> objFareTypes;

                    //Enumerable<XElement> objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                    // select fts;
                    if (CrdType == "CRP" || CrdType == "CPN")
                    {
                        // for GoSpecial fare
                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       where fts.Element("FareTypeName").Value.Trim().ToLower() == "gospecial"
                                       select fts;
                    }
                    else
                    {

                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       select fts;
                    }



                    #region fareType
                    for (int ft = 0; ft < objFareTypes.Count(); ft++)
                    {
                        XElement objFT = objFareTypes.ElementAt(ft);



                        IEnumerable<XElement> objLegDetails = from lds in objFs.Descendants("FlightLegDetails").Descendants("FlightLegDetail")
                                                              select lds;

                        #region legDetais
                        int legcount = 1;
                        for (int ld = 0; ld < objLegDetails.Count(); ld++)
                        {
                            FlightSearchResults fsr = new FlightSearchResults();
                            XElement LegDetail = objLegDetails.ElementAt(ld);

                            XElement objLegDetail = xlLegDetails.Where(x => x.Element("PFID").Value == LegDetail.Element("PFID").Value).FirstOrDefault();
                            XElement segDetails = xlSegmentDetails.Where(x => x.Element("LFID").Value == objFs.Element("LFID").Value).FirstOrDefault();

                            fsr.OrgDestFrom = segDetails.Element("Origin").Value;
                            fsr.OrgDestTo = segDetails.Element("Destination").Value;
                            fsr.Stops = segDetails.Element("Stops").Value + "-Stop";
                            fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(segDetails.Element("FlightTime").Value));
                            fsr.EQ = segDetails.Element("AircraftType").Value.Trim();
                            fsr.MarketingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.OperatingCarrier = segDetails.Element("OperatingCarrier").Value.Trim();
                            fsr.ValiDatingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.Searchvalue = objFs.Element("LFID").Value;


                            fsr.depdatelcc = objLegDetail.Element("DepartureDate").Value;
                            fsr.arrdatelcc = objLegDetail.Element("ArrivalDate").Value;
                            fsr.Departure_Date = objLegDetail.Element("DepartureDate").Value[8].ToString() + objLegDetail.Element("DepartureDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("DepartureDate").Value[5].ToString() + objLegDetail.Element("DepartureDate").Value[6].ToString()));
                            fsr.Arrival_Date = objLegDetail.Element("ArrivalDate").Value[8].ToString() + objLegDetail.Element("ArrivalDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("ArrivalDate").Value[5].ToString() + objLegDetail.Element("ArrivalDate").Value[6].ToString()));
                            fsr.FlightIdentification = objLegDetail.Element("FlightNum").Value;
                            fsr.DepartureDate = Convert.ToDateTime(objLegDetail.Element("DepartureDate").Value.Trim()).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(objLegDetail.Element("ArrivalDate").Value.Trim()).ToString("ddMMyy");
                            fsr.Sector = fsr.OrgDestFrom + ":" + fsr.OrgDestTo + ":" + fsr.OrgDestFrom;


                            fsr.DepartureLocation = objLegDetail.Element("Origin").Value;
                            fsr.ArrivalLocation = objLegDetail.Element("Destination").Value;
                            fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Destination").Value.Trim());
                            fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Origin").Value);
                            fsr.DepartureTime = objLegDetail.Element("DepartureDate").Value.Remove(0, objLegDetail.Element("DepartureDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.ArrivalTime = objLegDetail.Element("ArrivalDate").Value.Remove(0, objLegDetail.Element("ArrivalDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Origin").Value.Trim());
                            fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Destination").Value.Trim());
                            fsr.ArrAirportCode = objLegDetail.Element("Destination").Value.Trim();
                            fsr.DepAirportCode = objLegDetail.Element("Origin").Value.Trim();

                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.TripType = TripType.R.ToString();
                            fsr.LineNumber = lineNum;
                            fsr.Leg = legcount;
                            fsr.Flight = "1";
                            fsr.AirLineName = "Air-India Express";
                            fsr.Adult = searchInput.Adult;
                            fsr.Child = searchInput.Child;
                            fsr.Infant = searchInput.Infant;
                            fsr.TotPax = searchInput.Adult + searchInput.Child;
                            fsr.Provider = "LCC";


                            IEnumerable<XElement> FareInfos = from fInf in objFT.Descendants("FareInfos").Descendants("FareInfo")
                                                              select fInf;

                            float AdtFare = 0, ChdFare = 0, InfFare = 0;
                            float AdtTax = 0, ChdTax = 0, InfTax = 0;

                            #region fareInfo

                            for (int fi = 0; fi < FareInfos.Count(); fi++)
                            {
                                XElement objFI = FareInfos.ElementAt(fi);

                                // decimal totalTax = 0;
                                //totalTax = Convert.ToDecimal(objFI.Element("DisplayTaxSum").Value); //- Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value);

                                IEnumerable<XElement> TaxDetails = from taxD in objFI.Descendants("ApplicableTaxDetails").Descendants("ApplicableTaxDetail")
                                                                   select taxD;

                                #region Adult Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 1)
                                {
                                    fsr.AdtAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.AdtCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.AdtFSur = 0;
                                    AdtTax = float.Parse(objFI.Element("DisplayTaxSum").Value);// +OtherTF;
                                    fsr.AdtTax = (float)Math.Ceiling((decimal)AdtTax);

                                    fsr.AdtRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.AdtFarebasis = objFI.Element("FBCode").Value.Trim();
                                    fsr.AdtFareType = "Refundable";

                                    AdtFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);  // float.Parse(objFI.Element("FareAmtInclTax").Value);
                                    //(float)Math.Ceiling(Convert.ToDecimal(objFI.Element("FareAmtInclTax").Value));//fareInfo.BaseFareAmtInclTax);
                                    string provider = "IX";// searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                                    fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";


                                    fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + ":" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.AdtFSur = fsr.AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                        else
                                        {
                                            fsr.AdtOT = fsr.AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                    }

                                    fsr.AdtOT += OtherTF;

                                    float alltax = fsr.AdtWO + fsr.AdtJN + fsr.AdtIN + fsr.AdtOT + fsr.AdtFSur;
                                    if (fsr.AdtTax <= alltax)
                                    {
                                        fsr.AdtTax = alltax;
                                    }
                                    fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;
                                    #region Calculation
                                    fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    fsr.AdtTax = fsr.AdtTax + srvCharge;
                                    fsr.AdtFare = fsr.AdtFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.AdtFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, searchInput.TDS);
                                    fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    // fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());



                                }

                                #endregion

                                #region Child Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 6)
                                {

                                    fsr.ChdAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.ChdCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.ChdFSur = 0;
                                    ChdTax = float.Parse(objFI.Element("DisplayTaxSum").Value);// +OtherTF;
                                    fsr.ChdTax = (float)Math.Ceiling((decimal)ChdTax);

                                    fsr.ChdRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.ChdFarebasis = objFI.Element("FBCode").Value.Trim();
                                    //fsr.AdtFareType = "Refundable";// fareInfo.FareTypeID.Trim();
                                    //fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                    ChdFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);

                                    fsr.BagInfo = fsr.BagInfo + "(Child)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + "chd-" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;

                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.ChdFSur = fsr.ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.ChdOT = fsr.ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }



                                    }
                                    fsr.ChdOT += OtherTF;
                                    float alltax = fsr.ChdWO + fsr.ChdJN + fsr.ChdIN + fsr.ChdOT + fsr.ChdFSur;
                                    if (fsr.ChdTax <= alltax)
                                    {
                                        fsr.ChdTax = alltax;
                                    }
                                    fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;
                                    #region Calculation
                                    fsr.ChdOT = fsr.ChdOT + srvCharge;
                                    fsr.ChdTax = fsr.ChdTax + srvCharge;
                                    fsr.ChdFare = fsr.ChdFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.CHDDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.ChdFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, searchInput.TDS);
                                    fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.ChdDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());


                                }
                                #endregion

                                #region Infant Fare


                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 5)
                                {
                                    fsr.InfAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.InfCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.InfFSur = 0;// decimal.Parse(objFI.Element("PTCID").Value);
                                    InfTax = float.Parse(objFI.Element("DisplayTaxSum").Value);// +OtherTF;
                                    fsr.InfTax = (float)Math.Ceiling((decimal)InfTax);// decimal.Parse(objFI.Element("PTCID").Value);

                                    fsr.InfRbd = objFI.Element("FCCode").Value.Trim();// fareInfo.FCCode.Trim();
                                    fsr.InfFarebasis = objFI.Element("FBCode").Value.Trim(); //fareInfo.FBCode.Trim();

                                    InfFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);




                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.InfFSur = fsr.InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.InfOT = fsr.InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }


                                    }
                                    fsr.InfOT +=  OtherTF;
                                    float alltax = fsr.InfWO + fsr.InfJN + fsr.InfIN + fsr.InfOT + fsr.InfFSur;
                                    if (fsr.InfTax <= alltax)
                                    {
                                        fsr.InfTax = alltax;
                                    }
                                    fsr.InfFare = fsr.InfBfare + fsr.InfTax;
                                }

                                #endregion

                            }


                            #endregion



                            //fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            //fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare); ;

                            //fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            //fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            //fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            //fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            //fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            //fsr.Trip = searchInput.Trip.ToString();

                            //fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            //fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            //fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * fsr.InfFare) + (OtherTF * (fsr.Adult + fsr.Child + fsr.Infant));
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats1 = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.AvailableSeats = CrdType;
                            fsr.AdtFar = CrdType;
                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(fsr.InfTF * fsr.Infant);
                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);// +(fsr.InfDiscount * fsr.Infant);
                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);// +(fsr.InfCB * fsr.Infant);
                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +(fsr.InfTds * fsr.Infant);
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);// +(fsr.InfAdminMrk * fsr.Infant) + (fsr.InfAgentMrk * fsr.Infant);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));//+ (fsr.InfAgentMrk * fsr.Infant));
                            fsr.OriginalTT = srvCharge + OtherTF;
                            fsrList.Add(fsr);

                            legcount++;

                        }
                        #endregion


                        lineNum++;


                    }
                    #endregion





                }
                #endregion


                #endregion
                #region INBound Parsing

                int lineNum1 = 1;
                #region flight segment
                for (int fs = 0; fs < xlFSegReturn.Count(); fs++)
                {
                    XElement objFs = xlFSegReturn.ElementAt(fs);
                    IEnumerable<XElement> objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                                         select fts;

                    #region fareType
                    for (int ft = 0; ft < objFareTypes.Count(); ft++)
                    {
                        XElement objFT = objFareTypes.ElementAt(ft);



                        IEnumerable<XElement> objLegDetails = from lds in objFs.Descendants("FlightLegDetails").Descendants("FlightLegDetail")
                                                              select lds;

                        #region legDetais
                        int legcount = 1;
                        for (int ld = 0; ld < objLegDetails.Count(); ld++)
                        {
                            FlightSearchResults fsr = new FlightSearchResults();
                            XElement LegDetail = objLegDetails.ElementAt(ld);

                            XElement objLegDetail = xlLegDetails.Where(x => x.Element("PFID").Value == LegDetail.Element("PFID").Value).FirstOrDefault();
                            XElement segDetails = xlSegmentDetails.Where(x => x.Element("LFID").Value == objFs.Element("LFID").Value).FirstOrDefault();

                            fsr.OrgDestFrom = segDetails.Element("Destination").Value;
                            fsr.OrgDestTo = segDetails.Element("Origin").Value;
                            fsr.Stops = segDetails.Element("Stops").Value + "-Stop";
                            fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(segDetails.Element("FlightTime").Value));
                            fsr.EQ = segDetails.Element("AircraftType").Value.Trim();
                            fsr.MarketingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.OperatingCarrier = segDetails.Element("OperatingCarrier").Value.Trim();
                            fsr.ValiDatingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.Searchvalue = objFs.Element("LFID").Value;


                            fsr.depdatelcc = objLegDetail.Element("DepartureDate").Value;
                            fsr.arrdatelcc = objLegDetail.Element("ArrivalDate").Value;
                            fsr.Departure_Date = objLegDetail.Element("DepartureDate").Value[8].ToString() + objLegDetail.Element("DepartureDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("DepartureDate").Value[5].ToString() + objLegDetail.Element("DepartureDate").Value[6].ToString()));
                            fsr.Arrival_Date = objLegDetail.Element("ArrivalDate").Value[8].ToString() + objLegDetail.Element("ArrivalDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("ArrivalDate").Value[5].ToString() + objLegDetail.Element("ArrivalDate").Value[6].ToString()));
                            fsr.FlightIdentification = objLegDetail.Element("FlightNum").Value;
                            fsr.DepartureDate = Convert.ToDateTime(objLegDetail.Element("DepartureDate").Value.Trim()).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(objLegDetail.Element("ArrivalDate").Value.Trim()).ToString("ddMMyy");
                            fsr.Sector = fsr.OrgDestFrom + ":" + fsr.OrgDestTo + ":" + fsr.OrgDestFrom;


                            fsr.DepartureLocation = objLegDetail.Element("Origin").Value;
                            fsr.ArrivalLocation = objLegDetail.Element("Destination").Value;
                            fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Destination").Value.Trim());
                            fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Origin").Value);
                            fsr.DepartureTime = objLegDetail.Element("DepartureDate").Value.Remove(0, objLegDetail.Element("DepartureDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.ArrivalTime = objLegDetail.Element("ArrivalDate").Value.Remove(0, objLegDetail.Element("ArrivalDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Origin").Value.Trim());
                            fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Destination").Value.Trim());
                            fsr.ArrAirportCode = objLegDetail.Element("Destination").Value.Trim();
                            fsr.DepAirportCode = objLegDetail.Element("Origin").Value.Trim();

                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.TripType = TripType.R.ToString(); ;
                            fsr.LineNumber = lineNum1;
                            fsr.Leg = legcount;
                            fsr.Flight = "2";
                            fsr.AirLineName = "Air-India Express";
                            fsr.Adult = searchInput.Adult;
                            fsr.Child = searchInput.Child;
                            fsr.Infant = searchInput.Infant;
                            fsr.TotPax = searchInput.Adult + searchInput.Child;
                            fsr.Provider = "LCC";


                            IEnumerable<XElement> FareInfos = from fInf in objFT.Descendants("FareInfos").Descendants("FareInfo")
                                                              select fInf;

                            float AdtFare = 0, ChdFare = 0, InfFare = 0;
                            float AdtTax = 0, ChdTax = 0, InfTax = 0;

                            #region fareInfo

                            for (int fi = 0; fi < FareInfos.Count(); fi++)
                            {
                                XElement objFI = FareInfos.ElementAt(fi);

                                // decimal totalTax = 0;
                                //totalTax = Convert.ToDecimal(objFI.Element("DisplayTaxSum").Value); //- Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value);

                                IEnumerable<XElement> TaxDetails = from taxD in objFI.Descendants("ApplicableTaxDetails").Descendants("ApplicableTaxDetail")
                                                                   select taxD;

                                #region Adult Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 1)
                                {
                                    fsr.AdtAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.AdtCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.AdtFSur = 0;
                                    AdtTax = float.Parse(objFI.Element("DisplayTaxSum").Value);//+ OtherTF;
                                    fsr.AdtTax = (float)Math.Ceiling((decimal)AdtTax);

                                    fsr.AdtRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.AdtFarebasis = objFI.Element("FBCode").Value.Trim();
                                    fsr.AdtFareType = "Refundable";

                                    AdtFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);  // float.Parse(objFI.Element("FareAmtInclTax").Value);
                                    //(float)Math.Ceiling(Convert.ToDecimal(objFI.Element("FareAmtInclTax").Value));//fareInfo.BaseFareAmtInclTax);
                                    string provider = "IX"; //searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "G8" : "G8";
                                    fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";


                                    fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + ":" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.AdtFSur = fsr.AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.AdtOT = fsr.AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                    }
                                    fsr.AdtOT += OtherTF;

                                    float alltax = fsr.AdtWO + fsr.AdtJN + fsr.AdtIN + fsr.AdtOT + fsr.AdtFSur;
                                    if (fsr.AdtTax <= alltax)
                                    {
                                        fsr.AdtTax = alltax;
                                    }
                                    fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;
                                    #region Calculation
                                    fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    fsr.AdtTax = fsr.AdtTax + srvCharge;
                                    fsr.AdtFare = fsr.AdtFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.AdtFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, searchInput.TDS);
                                    fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    // fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());


                                }

                                #endregion

                                #region Child Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 6)
                                {

                                    fsr.ChdAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.ChdCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.ChdFSur = 0;
                                    ChdTax = float.Parse(objFI.Element("DisplayTaxSum").Value);// +OtherTF;
                                    fsr.ChdTax = (float)Math.Ceiling((decimal)ChdTax);

                                    fsr.ChdRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.ChdFarebasis = objFI.Element("FBCode").Value.Trim();
                                    //fsr.AdtFareType = "Refundable";// fareInfo.FareTypeID.Trim();
                                    //fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                    ChdFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);
                                    fsr.BagInfo = fsr.BagInfo + "(Child)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");
                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + "chd-" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.ChdFSur = fsr.ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.ChdOT = fsr.ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }



                                    }
                                    fsr.ChdOT += OtherTF;
                                    float alltax = fsr.ChdWO + fsr.ChdJN + fsr.ChdIN + fsr.ChdOT + fsr.ChdFSur;
                                    if (fsr.ChdTax <= alltax)
                                    {
                                        fsr.ChdTax = alltax;
                                    }
                                    fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;
                                    #region Calculation
                                    fsr.ChdOT = fsr.ChdOT + srvCharge;
                                    fsr.ChdTax = fsr.ChdTax + srvCharge;
                                    fsr.ChdFare = fsr.ChdFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.CHDDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.ChdFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, searchInput.TDS);
                                    fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.ChdDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());

                                }
                                #endregion

                                #region Infant Fare


                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 5)
                                {
                                    fsr.InfAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.InfCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.InfFSur = 0;// decimal.Parse(objFI.Element("PTCID").Value);
                                    InfTax = float.Parse(objFI.Element("DisplayTaxSum").Value);// +OtherTF;
                                    fsr.InfTax = (float)Math.Ceiling((decimal)InfTax);// decimal.Parse(objFI.Element("PTCID").Value);

                                    fsr.InfRbd = objFI.Element("FCCode").Value.Trim();// fareInfo.FCCode.Trim();
                                    fsr.InfFarebasis = objFI.Element("FBCode").Value.Trim(); //fareInfo.FBCode.Trim();

                                    InfFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);
                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.InfFSur = fsr.InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.InfOT = fsr.InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                    }                                   
                                    fsr.InfOT +=  OtherTF;
                                    float alltax = fsr.InfWO + fsr.InfJN + fsr.InfIN + fsr.InfOT + fsr.InfFSur;
                                    if (fsr.InfTax <= alltax)
                                    {
                                        fsr.InfTax = alltax;
                                    }
                                    fsr.InfFare = fsr.InfBfare + fsr.InfTax;
                                }
                                #endregion
                            }
                            #endregion
                            //fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            //fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare); ;

                            //fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            //fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            //fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            //fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            //fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            //fsr.Trip = searchInput.Trip.ToString();
                            //fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            //fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            //fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * fsr.AdtFare) - (fsr.Adult * srvCharge) + (fsr.Child * fsr.ChdFare) - (fsr.Child * srvCharge) + (fsr.Infant * fsr.InfFare); ;
                            fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare) + OtherTF *(fsr.Adult+fsr.Child+fsr.Infant);
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats1 = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.AvailableSeats = CrdType;
                            fsr.AdtFar = CrdType;
                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(fsr.InfTF * fsr.Infant);
                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);// +(fsr.InfDiscount * fsr.Infant);
                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);// +(fsr.InfCB * fsr.Infant);
                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +(fsr.InfTds * fsr.Infant);
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);// +(fsr.InfAdminMrk * fsr.Infant) + (fsr.InfAgentMrk * fsr.Infant);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));//+ (fsr.InfAgentMrk * fsr.Infant));
                            //fsr.OriginalTT = srvCharge;

                            fsr.OriginalTT = srvCharge + OtherTF;;
                            fsrListR.Add(fsr);

                            legcount++;

                        }
                        #endregion


                        lineNum1++;


                    }
                    #endregion





                }
                #endregion

                //result = fsrList;

                #endregion

                FinalList = RoundTripFare(fsrList, fsrListR, srvCharge);

            }
            else
            {
                #region oneWay Parsing

                int lineNum = 1;
                #region flight segment
                for (int fs = 0; fs < xlFSegOnword.Count(); fs++)
                {
                    XElement objFs = xlFSegOnword.ElementAt(fs);
                    IEnumerable<XElement> objFareTypes;
                    if (CrdType == "CRP" || CrdType == "CPN")
                    {
                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       where fts.Element("FareTypeName").Value.Trim().ToLower() == "ixspecial"
                                       select fts;
                    }
                    else
                    {
                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       select fts;
                    }
                    #region fareType
                    for (int ft = 0; ft < objFareTypes.Count(); ft++)
                    {
                        XElement objFT = objFareTypes.ElementAt(ft);



                        IEnumerable<XElement> objLegDetails = from lds in objFs.Descendants("FlightLegDetails").Descendants("FlightLegDetail")
                                                              select lds;

                        #region legDetais
                        int legcount = 1;
                        for (int ld = 0; ld < objLegDetails.Count(); ld++)
                        {
                            FlightSearchResults fsr = new FlightSearchResults();
                            XElement LegDetail = objLegDetails.ElementAt(ld);

                            XElement objLegDetail = xlLegDetails.Where(x => x.Element("PFID").Value == LegDetail.Element("PFID").Value).FirstOrDefault();
                            XElement segDetails = xlSegmentDetails.Where(x => x.Element("LFID").Value == objFs.Element("LFID").Value).FirstOrDefault();

                            fsr.OrgDestFrom = segDetails.Element("Origin").Value;
                            fsr.OrgDestTo = segDetails.Element("Destination").Value;
                            fsr.Stops = segDetails.Element("Stops").Value + "-Stop";
                            fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(segDetails.Element("FlightTime").Value));
                            fsr.EQ = segDetails.Element("AircraftType").Value.Trim();
                            fsr.MarketingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.OperatingCarrier = segDetails.Element("OperatingCarrier").Value.Trim();
                            fsr.ValiDatingCarrier = segDetails.Element("CarrierCode").Value.Trim();

                            fsr.Searchvalue = objFs.Element("LFID").Value;

                            fsr.depdatelcc = objLegDetail.Element("DepartureDate").Value;
                            fsr.arrdatelcc = objLegDetail.Element("ArrivalDate").Value;
                            fsr.Departure_Date = objLegDetail.Element("DepartureDate").Value[8].ToString() + objLegDetail.Element("DepartureDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("DepartureDate").Value[5].ToString() + objLegDetail.Element("DepartureDate").Value[6].ToString()));
                            fsr.Arrival_Date = objLegDetail.Element("ArrivalDate").Value[8].ToString() + objLegDetail.Element("ArrivalDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("ArrivalDate").Value[5].ToString() + objLegDetail.Element("ArrivalDate").Value[6].ToString()));
                            fsr.FlightIdentification = objLegDetail.Element("FlightNum").Value;
                            fsr.DepartureDate = Convert.ToDateTime(objLegDetail.Element("DepartureDate").Value.Trim()).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(objLegDetail.Element("ArrivalDate").Value.Trim()).ToString("ddMMyy");
                            fsr.Sector = fsr.OrgDestFrom + ":" + fsr.OrgDestTo;


                            fsr.DepartureLocation = objLegDetail.Element("Origin").Value;
                            fsr.ArrivalLocation = objLegDetail.Element("Destination").Value;
                            fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Destination").Value.Trim());
                            fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Origin").Value);
                            fsr.DepartureTime = objLegDetail.Element("DepartureDate").Value.Remove(0, objLegDetail.Element("DepartureDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.ArrivalTime = objLegDetail.Element("ArrivalDate").Value.Remove(0, objLegDetail.Element("ArrivalDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Origin").Value.Trim());
                            fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Destination").Value.Trim());
                            fsr.ArrAirportCode = objLegDetail.Element("Destination").Value.Trim();
                            fsr.DepAirportCode = objLegDetail.Element("Origin").Value.Trim();

                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.TripType = searchInput.TripType.ToString();
                            fsr.LineNumber = lineNum;
                            fsr.Leg = legcount;
                            fsr.Flight = "1";
                            fsr.AirLineName = "Air-India Express";
                            fsr.Adult = searchInput.Adult;
                            fsr.Child = searchInput.Child;
                            fsr.Infant = searchInput.Infant;
                            fsr.TotPax = searchInput.Adult + searchInput.Child;
                            fsr.Provider = "LCC";


                            IEnumerable<XElement> FareInfos = from fInf in objFT.Descendants("FareInfos").Descendants("FareInfo")
                                                              select fInf;


                            float AdtFare = 0, ChdFare = 0, InfFare = 0;
                            float AdtTax = 0, ChdTax = 0, InfTax = 0;

                            #region fareInfo

                            for (int fi = 0; fi < FareInfos.Count(); fi++)
                            {
                                XElement objFI = FareInfos.ElementAt(fi);

                                // decimal totalTax = 0;
                                //totalTax = Convert.ToDecimal(objFI.Element("DisplayTaxSum").Value); //- Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value);

                                IEnumerable<XElement> TaxDetails = from taxD in objFI.Descendants("ApplicableTaxDetails").Descendants("ApplicableTaxDetail")
                                                                   select taxD;

                                #region Adult Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 1)
                                {
                                    fsr.AdtAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.AdtCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.AdtFSur = 0;
                                    AdtTax = float.Parse(objFI.Element("DisplayTaxSum").Value);// +OtherTF;
                                    fsr.AdtTax = (float)Math.Ceiling((decimal)AdtTax);

                                    fsr.AdtRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.AdtFarebasis = objFI.Element("FBCode").Value.Trim();
                                    fsr.AdtFareType = "Refundable";

                                    AdtFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);  // float.Parse(objFI.Element("FareAmtInclTax").Value);
                                    //(float)Math.Ceiling(Convert.ToDecimal(objFI.Element("FareAmtInclTax").Value));//fareInfo.BaseFareAmtInclTax);
                                    string provider = "IX"; //searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                                    fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";


                                    fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;

                                            fsr.Searchvalue = fsr.Searchvalue + ":" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.AdtFSur = fsr.AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.AdtOT = fsr.AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                    }
                                    fsr.AdtOT += OtherTF;
                                    //fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    float alltax = fsr.AdtWO + fsr.AdtJN + fsr.AdtIN + fsr.AdtOT + fsr.AdtFSur;
                                    if (fsr.AdtTax <= alltax)
                                    {
                                        fsr.AdtTax = alltax;
                                    }
                                    else
                                    {
                                        fsr.AdtTax = fsr.AdtTax;// +srvCharge;
                                    }
                                    fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;
                                    #region Calculation
                                    fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    fsr.AdtTax = fsr.AdtTax + srvCharge;
                                    fsr.AdtFare = fsr.AdtFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.AdtFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, searchInput.TDS);
                                    fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //  fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());

                                }

                                #endregion

                                #region Child Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 6)
                                {

                                    fsr.ChdAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.ChdCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.ChdFSur = 0;
                                    ChdTax = float.Parse(objFI.Element("DisplayTaxSum").Value);  //+ OtherTF;
                                    fsr.ChdTax = (float)Math.Ceiling((decimal)ChdTax);

                                    fsr.ChdRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.ChdFarebasis = objFI.Element("FBCode").Value.Trim();
                                    //fsr.AdtFareType = "Refundable";// fareInfo.FareTypeID.Trim();
                                    //fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                    ChdFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);

                                    fsr.BagInfo = fsr.BagInfo + "(Child)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");
                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + "chd-" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.ChdFSur = fsr.ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.ChdOT = fsr.ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }



                                    }

                                    fsr.ChdOT += OtherTF;
                                    fsr.ChdOT = fsr.ChdOT; // +srvCharge;
                                    float alltax = fsr.ChdWO + fsr.ChdJN + fsr.ChdIN + fsr.ChdOT + fsr.ChdFSur;
                                    if (fsr.ChdTax <= alltax)
                                    {
                                        fsr.ChdTax = alltax;
                                    }
                                    else
                                    {
                                        fsr.ChdTax = fsr.ChdTax;// +srvCharge;
                                    }
                                    fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;
                                    #region Calculation
                                    fsr.ChdOT = fsr.ChdOT + srvCharge;
                                    fsr.ChdTax = fsr.ChdTax + srvCharge;
                                    fsr.ChdFare = fsr.ChdFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.CHDDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.ChdFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, searchInput.TDS);
                                    fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.ChdDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());

                                }
                                #endregion

                                #region Infant Fare


                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 5)
                                {
                                    fsr.InfAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.InfCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.InfFSur = 0;// decimal.Parse(objFI.Element("PTCID").Value);
                                    InfTax = float.Parse(objFI.Element("DisplayTaxSum").Value);// + OtherTF;
                                    fsr.InfTax = (float)Math.Ceiling((decimal)InfTax);// decimal.Parse(objFI.Element("PTCID").Value);

                                    fsr.InfRbd = objFI.Element("FCCode").Value.Trim();// fareInfo.FCCode.Trim();
                                    fsr.InfFarebasis = objFI.Element("FBCode").Value.Trim(); //fareInfo.FBCode.Trim();

                                    InfFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);
                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));

                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.InfFSur = fsr.InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.InfOT = fsr.InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                    }                                   
                                    fsr.InfOT +=  OtherTF;
                                    float alltax = fsr.InfWO + fsr.InfJN + fsr.InfIN + fsr.InfOT + fsr.InfFSur;
                                    if (fsr.InfTax <= alltax)
                                    {
                                        fsr.InfTax = alltax;
                                    }
                                    fsr.InfFare = fsr.InfBfare + fsr.InfTax;
                                }
                                #endregion
                            }
                            #endregion
                            //fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            //fsr.OriginalTT = srvCharge; //(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare); ;

                            //fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            //fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            //fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            //fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            //fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            //fsr.Trip = searchInput.Trip.ToString();

                            //fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            //fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            //fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            // fsr.OriginalTF = (fsr.Adult * fsr.AdtFare) - (fsr.Adult * srvCharge) + (fsr.Child * fsr.ChdFare) - (fsr.Child * srvCharge) + (fsr.Infant * fsr.InfFare); ;
                            fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare) + OtherTF *(fsr.Adult+fsr.Child+fsr.Infant);
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats1 = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.AvailableSeats = CrdType;
                            fsr.AdtFar = CrdType;
                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(fsr.InfTF * fsr.Infant);
                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);// +(fsr.InfDiscount * fsr.Infant);
                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);// +(fsr.InfCB * fsr.Infant);
                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +(fsr.InfTds * fsr.Infant);
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);// +(fsr.InfAdminMrk * fsr.Infant) + (fsr.InfAgentMrk * fsr.Infant);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));//+ (fsr.InfAgentMrk * fsr.Infant));
                            // fsr.OriginalTT = srvCharge;

                            fsr.OriginalTT = srvCharge + OtherTF;
                            fsrList.Add(fsr);

                            legcount++;

                        }
                        #endregion


                        lineNum++;


                    }
                    #endregion





                }
                #endregion

                FinalList.Add(fsrList);

                #endregion

            }
            //FlightCommonBAL objFltComm = new FlightCommonBAL();
            FCBAL.AddFlightKey((List<FlightSearchResults>)FinalList[0], searchInput.RTF);

            return FinalList;

        }
        private ArrayList ParseFareQoute(string responseXml, FlightSearch searchInput, List<FlightCityList> CityList, float srvCharge, DataSet MarkupDs, List<FltSrvChargeList> SrvChargeList, string CrdType)
        {
            ArrayList FinalList = new ArrayList();
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            FlightCommonBAL FCBAL = new FlightCommonBAL(ConStr);

            List<FlightSearchResults> fsrList = new List<FlightSearchResults>();
            List<FlightSearchResults> fsrListR = new List<FlightSearchResults>();
            // XDocument xmlDocq = XDocument.Load(@"E:\bhupinder 1.11.2014\d drive\Bhupender\FlyDubai\TestWeb\TestWeb\fareQOneWay.xml");//Parse(responseXml);
            //XNamespace ns = "http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Response";
            string str = responseXml.Replace("xmlns:a=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Response\"", "").Replace("a:", "").Replace("xmlns=\"http://tempuri.org/\"", "");

            XDocument xmlDoc = XDocument.Parse(str);

            IEnumerable<XElement> xlFSegOnword = null;
            IEnumerable<XElement> xlFSegReturn = null;


            if (searchInput.TripType == TripType.RoundTrip)
            {
                xlFSegOnword = from el in xmlDoc.Descendants("FlightSegments").Descendants("FlightSegment")
                               where el.Element("LegCount").Value == "1"
                               select el;

                xlFSegReturn = from el in xmlDoc.Descendants("FlightSegments").Descendants("FlightSegment")
                               where el.Element("LegCount").Value == "2"
                               select el;
            }
            else
            {
                xlFSegOnword = from el in xmlDoc.Descendants("FlightSegments").Descendants("FlightSegment")
                               select el;
            }

            IEnumerable<XElement> xlLegDetails = from el in xmlDoc.Descendants("LegDetails").Descendants("LegDetail")
                                                 select el;
            IEnumerable<XElement> xlSegmentDetails = from el in xmlDoc.Descendants("SegmentDetails").Descendants("SegmentDetail")
                                                     select el;
            IEnumerable<XElement> xlTaxDetails = from el in xmlDoc.Descendants("TaxDetails").Descendants("TaxDetail")
                                                 select el;






            if (searchInput.TripType == TripType.RoundTrip && searchInput.RTF == true)
            {
                #region OutBound Parsing

                int lineNum = 1;
                #region flight segment
                for (int fs = 0; fs < xlFSegOnword.Count(); fs++)
                {
                    XElement objFs = xlFSegOnword.ElementAt(fs);
                    IEnumerable<XElement> objFareTypes;

                    //Enumerable<XElement> objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                    // select fts;
                    if (CrdType == "CRP" || CrdType == "CPN")
                    {
                        // for GoSpecial fare
                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       where fts.Element("FareTypeName").Value.Trim().ToLower() == "gospecial"
                                       select fts;
                    }
                    else
                    {

                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       select fts;
                    }



                    #region fareType
                    for (int ft = 0; ft < objFareTypes.Count(); ft++)
                    {
                        XElement objFT = objFareTypes.ElementAt(ft);



                        IEnumerable<XElement> objLegDetails = from lds in objFs.Descendants("FlightLegDetails").Descendants("FlightLegDetail")
                                                              select lds;

                        #region legDetais
                        int legcount = 1;
                        for (int ld = 0; ld < objLegDetails.Count(); ld++)
                        {
                            FlightSearchResults fsr = new FlightSearchResults();
                            XElement LegDetail = objLegDetails.ElementAt(ld);

                            XElement objLegDetail = xlLegDetails.Where(x => x.Element("PFID").Value == LegDetail.Element("PFID").Value).FirstOrDefault();
                            XElement segDetails = xlSegmentDetails.Where(x => x.Element("LFID").Value == objFs.Element("LFID").Value).FirstOrDefault();

                            fsr.OrgDestFrom = segDetails.Element("Origin").Value;
                            fsr.OrgDestTo = segDetails.Element("Destination").Value;
                            fsr.Stops = segDetails.Element("Stops").Value + "-Stop";
                            fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(segDetails.Element("FlightTime").Value));
                            fsr.EQ = segDetails.Element("AircraftType").Value.Trim();
                            fsr.MarketingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.OperatingCarrier = segDetails.Element("OperatingCarrier").Value.Trim();
                            fsr.ValiDatingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.Searchvalue = objFs.Element("LFID").Value;


                            fsr.depdatelcc = objLegDetail.Element("DepartureDate").Value;
                            fsr.arrdatelcc = objLegDetail.Element("ArrivalDate").Value;
                            fsr.Departure_Date = objLegDetail.Element("DepartureDate").Value[8].ToString() + objLegDetail.Element("DepartureDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("DepartureDate").Value[5].ToString() + objLegDetail.Element("DepartureDate").Value[6].ToString()));
                            fsr.Arrival_Date = objLegDetail.Element("ArrivalDate").Value[8].ToString() + objLegDetail.Element("ArrivalDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("ArrivalDate").Value[5].ToString() + objLegDetail.Element("ArrivalDate").Value[6].ToString()));
                            fsr.FlightIdentification = objLegDetail.Element("FlightNum").Value;
                            fsr.DepartureDate = Convert.ToDateTime(objLegDetail.Element("DepartureDate").Value.Trim()).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(objLegDetail.Element("ArrivalDate").Value.Trim()).ToString("ddMMyy");
                            fsr.Sector = fsr.OrgDestFrom + ":" + fsr.OrgDestTo + ":" + fsr.OrgDestFrom;


                            fsr.DepartureLocation = objLegDetail.Element("Origin").Value;
                            fsr.ArrivalLocation = objLegDetail.Element("Destination").Value;
                            fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Destination").Value.Trim());
                            fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Origin").Value);
                            fsr.DepartureTime = objLegDetail.Element("DepartureDate").Value.Remove(0, objLegDetail.Element("DepartureDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.ArrivalTime = objLegDetail.Element("ArrivalDate").Value.Remove(0, objLegDetail.Element("ArrivalDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Origin").Value.Trim());
                            fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Destination").Value.Trim());
                            fsr.ArrAirportCode = objLegDetail.Element("Destination").Value.Trim();
                            fsr.DepAirportCode = objLegDetail.Element("Origin").Value.Trim();

                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.TripType = TripType.R.ToString();
                            fsr.LineNumber = lineNum;
                            fsr.Leg = legcount;
                            fsr.Flight = "1";
                            fsr.AirLineName = "GoAir";
                            fsr.Adult = searchInput.Adult;
                            fsr.Child = searchInput.Child;
                            fsr.Infant = searchInput.Infant;
                            fsr.TotPax = searchInput.Adult + searchInput.Child;
                            fsr.Provider = "LCC";


                            IEnumerable<XElement> FareInfos = from fInf in objFT.Descendants("FareInfos").Descendants("FareInfo")
                                                              select fInf;

                            float AdtFare = 0, ChdFare = 0, InfFare = 0;
                            float AdtTax = 0, ChdTax = 0, InfTax = 0;

                            #region fareInfo

                            for (int fi = 0; fi < FareInfos.Count(); fi++)
                            {
                                XElement objFI = FareInfos.ElementAt(fi);

                                // decimal totalTax = 0;
                                //totalTax = Convert.ToDecimal(objFI.Element("DisplayTaxSum").Value); //- Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value);

                                IEnumerable<XElement> TaxDetails = from taxD in objFI.Descendants("ApplicableTaxDetails").Descendants("ApplicableTaxDetail")
                                                                   select taxD;

                                #region Adult Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 1)
                                {
                                    fsr.AdtAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.AdtCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.AdtFSur = 0;
                                    AdtTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.AdtTax = (float)Math.Ceiling((decimal)AdtTax);

                                    fsr.AdtRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.AdtFarebasis = objFI.Element("FBCode").Value.Trim();
                                    fsr.AdtFareType = "Refundable";

                                    AdtFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);  // float.Parse(objFI.Element("FareAmtInclTax").Value);
                                    //(float)Math.Ceiling(Convert.ToDecimal(objFI.Element("FareAmtInclTax").Value));//fareInfo.BaseFareAmtInclTax);
                                    string provider = "G8";// searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                                    fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";


                                    fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + ":" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.AdtFSur = fsr.AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                        else
                                        {
                                            fsr.AdtOT = fsr.AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                    }


                                    float alltax = fsr.AdtWO + fsr.AdtJN + fsr.AdtIN + fsr.AdtOT + fsr.AdtFSur;
                                    if (fsr.AdtTax <= alltax)
                                    {
                                        fsr.AdtTax = alltax;
                                    }
                                    fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;
                                    #region Calculation
                                    fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    fsr.AdtTax = fsr.AdtTax + srvCharge;
                                    fsr.AdtFare = fsr.AdtFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + CrdType, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier + CrdType, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.AdtFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, searchInput.TDS);
                                    fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    // fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());



                                }

                                #endregion

                                #region Child Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 6)
                                {

                                    fsr.ChdAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.ChdCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.ChdFSur = 0;
                                    ChdTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.ChdTax = (float)Math.Ceiling((decimal)ChdTax);

                                    fsr.ChdRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.ChdFarebasis = objFI.Element("FBCode").Value.Trim();
                                    //fsr.AdtFareType = "Refundable";// fareInfo.FareTypeID.Trim();
                                    //fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                    ChdFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);

                                    fsr.BagInfo = fsr.BagInfo + "(Child)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + "chd-" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;

                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.ChdFSur = fsr.ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.ChdOT = fsr.ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }



                                    }

                                    float alltax = fsr.ChdWO + fsr.ChdJN + fsr.ChdIN + fsr.ChdOT + fsr.ChdFSur;
                                    if (fsr.ChdTax <= alltax)
                                    {
                                        fsr.ChdTax = alltax;
                                    }
                                    fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;
                                    #region Calculation
                                    fsr.ChdOT = fsr.ChdOT + srvCharge;
                                    fsr.ChdTax = fsr.ChdTax + srvCharge;
                                    fsr.ChdFare = fsr.ChdFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier + CrdType, fsr.ChdFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.CHDDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier + CrdType, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.ChdFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, searchInput.TDS);
                                    fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.ChdDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());


                                }
                                #endregion

                                #region Infant Fare


                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 5)
                                {
                                    fsr.InfAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.InfCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.InfFSur = 0;// decimal.Parse(objFI.Element("PTCID").Value);
                                    InfTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.InfTax = (float)Math.Ceiling((decimal)InfTax);// decimal.Parse(objFI.Element("PTCID").Value);

                                    fsr.InfRbd = objFI.Element("FCCode").Value.Trim();// fareInfo.FCCode.Trim();
                                    fsr.InfFarebasis = objFI.Element("FBCode").Value.Trim(); //fareInfo.FBCode.Trim();

                                    InfFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);




                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.InfFSur = fsr.InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.InfOT = fsr.InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }


                                    }

                                    float alltax = fsr.InfWO + fsr.InfJN + fsr.InfIN + fsr.InfOT + fsr.InfFSur;
                                    if (fsr.InfTax <= alltax)
                                    {
                                        fsr.InfTax = alltax;
                                    }
                                    fsr.InfFare = fsr.InfBfare + fsr.InfTax;


                                }

                                #endregion





                            }


                            #endregion



                            //fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            //fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare); ;

                            //fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            //fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            //fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            //fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            //fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            //fsr.Trip = searchInput.Trip.ToString();

                            //fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            //fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            //fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats1 = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.AvailableSeats = CrdType;
                            fsr.AdtFar = CrdType;
                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(fsr.InfTF * fsr.Infant);
                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);// +(fsr.InfDiscount * fsr.Infant);
                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);// +(fsr.InfCB * fsr.Infant);
                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +(fsr.InfTds * fsr.Infant);
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);// +(fsr.InfAdminMrk * fsr.Infant) + (fsr.InfAgentMrk * fsr.Infant);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));//+ (fsr.InfAgentMrk * fsr.Infant));
                            fsr.OriginalTT = srvCharge;



                            fsrList.Add(fsr);

                            legcount++;

                        }
                        #endregion


                        lineNum++;


                    }
                    #endregion





                }
                #endregion


                #endregion
                #region INBound Parsing

                int lineNum1 = 1;
                #region flight segment
                for (int fs = 0; fs < xlFSegReturn.Count(); fs++)
                {
                    XElement objFs = xlFSegReturn.ElementAt(fs);
                    IEnumerable<XElement> objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                                         select fts;

                    #region fareType
                    for (int ft = 0; ft < objFareTypes.Count(); ft++)
                    {
                        XElement objFT = objFareTypes.ElementAt(ft);



                        IEnumerable<XElement> objLegDetails = from lds in objFs.Descendants("FlightLegDetails").Descendants("FlightLegDetail")
                                                              select lds;

                        #region legDetais
                        int legcount = 1;
                        for (int ld = 0; ld < objLegDetails.Count(); ld++)
                        {
                            FlightSearchResults fsr = new FlightSearchResults();
                            XElement LegDetail = objLegDetails.ElementAt(ld);

                            XElement objLegDetail = xlLegDetails.Where(x => x.Element("PFID").Value == LegDetail.Element("PFID").Value).FirstOrDefault();
                            XElement segDetails = xlSegmentDetails.Where(x => x.Element("LFID").Value == objFs.Element("LFID").Value).FirstOrDefault();

                            fsr.OrgDestFrom = segDetails.Element("Destination").Value;
                            fsr.OrgDestTo = segDetails.Element("Origin").Value;
                            fsr.Stops = segDetails.Element("Stops").Value + "-Stop";
                            fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(segDetails.Element("FlightTime").Value));
                            fsr.EQ = segDetails.Element("AircraftType").Value.Trim();
                            fsr.MarketingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.OperatingCarrier = segDetails.Element("OperatingCarrier").Value.Trim();
                            fsr.ValiDatingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.Searchvalue = objFs.Element("LFID").Value;


                            fsr.depdatelcc = objLegDetail.Element("DepartureDate").Value;
                            fsr.arrdatelcc = objLegDetail.Element("ArrivalDate").Value;
                            fsr.Departure_Date = objLegDetail.Element("DepartureDate").Value[8].ToString() + objLegDetail.Element("DepartureDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("DepartureDate").Value[5].ToString() + objLegDetail.Element("DepartureDate").Value[6].ToString()));
                            fsr.Arrival_Date = objLegDetail.Element("ArrivalDate").Value[8].ToString() + objLegDetail.Element("ArrivalDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("ArrivalDate").Value[5].ToString() + objLegDetail.Element("ArrivalDate").Value[6].ToString()));
                            fsr.FlightIdentification = objLegDetail.Element("FlightNum").Value;
                            fsr.DepartureDate = Convert.ToDateTime(objLegDetail.Element("DepartureDate").Value.Trim()).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(objLegDetail.Element("ArrivalDate").Value.Trim()).ToString("ddMMyy");
                            fsr.Sector = fsr.OrgDestFrom + ":" + fsr.OrgDestTo + ":" + fsr.OrgDestFrom;


                            fsr.DepartureLocation = objLegDetail.Element("Origin").Value;
                            fsr.ArrivalLocation = objLegDetail.Element("Destination").Value;
                            fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Destination").Value.Trim());
                            fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Origin").Value);
                            fsr.DepartureTime = objLegDetail.Element("DepartureDate").Value.Remove(0, objLegDetail.Element("DepartureDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.ArrivalTime = objLegDetail.Element("ArrivalDate").Value.Remove(0, objLegDetail.Element("ArrivalDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Origin").Value.Trim());
                            fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Destination").Value.Trim());
                            fsr.ArrAirportCode = objLegDetail.Element("Destination").Value.Trim();
                            fsr.DepAirportCode = objLegDetail.Element("Origin").Value.Trim();

                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.TripType = TripType.R.ToString(); ;
                            fsr.LineNumber = lineNum1;
                            fsr.Leg = legcount;
                            fsr.Flight = "2";
                            fsr.AirLineName = "GoAir";
                            fsr.Adult = searchInput.Adult;
                            fsr.Child = searchInput.Child;
                            fsr.Infant = searchInput.Infant;
                            fsr.TotPax = searchInput.Adult + searchInput.Child;
                            fsr.Provider = "LCC";


                            IEnumerable<XElement> FareInfos = from fInf in objFT.Descendants("FareInfos").Descendants("FareInfo")
                                                              select fInf;

                            float AdtFare = 0, ChdFare = 0, InfFare = 0;
                            float AdtTax = 0, ChdTax = 0, InfTax = 0;

                            #region fareInfo

                            for (int fi = 0; fi < FareInfos.Count(); fi++)
                            {
                                XElement objFI = FareInfos.ElementAt(fi);

                                // decimal totalTax = 0;
                                //totalTax = Convert.ToDecimal(objFI.Element("DisplayTaxSum").Value); //- Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value);

                                IEnumerable<XElement> TaxDetails = from taxD in objFI.Descendants("ApplicableTaxDetails").Descendants("ApplicableTaxDetail")
                                                                   select taxD;

                                #region Adult Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 1)
                                {
                                    fsr.AdtAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.AdtCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.AdtFSur = 0;
                                    AdtTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.AdtTax = (float)Math.Ceiling((decimal)AdtTax);

                                    fsr.AdtRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.AdtFarebasis = objFI.Element("FBCode").Value.Trim();
                                    fsr.AdtFareType = "Refundable";

                                    AdtFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);  // float.Parse(objFI.Element("FareAmtInclTax").Value);
                                    //(float)Math.Ceiling(Convert.ToDecimal(objFI.Element("FareAmtInclTax").Value));//fareInfo.BaseFareAmtInclTax);
                                    string provider = "G8"; //searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "G8" : "G8";
                                    fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";


                                    fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + ":" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.AdtFSur = fsr.AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.AdtOT = fsr.AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                    }


                                    float alltax = fsr.AdtWO + fsr.AdtJN + fsr.AdtIN + fsr.AdtOT + fsr.AdtFSur;
                                    if (fsr.AdtTax <= alltax)
                                    {
                                        fsr.AdtTax = alltax;
                                    }
                                    fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;
                                    #region Calculation
                                    fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    fsr.AdtTax = fsr.AdtTax + srvCharge;
                                    fsr.AdtFare = fsr.AdtFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.AdtFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, searchInput.TDS);
                                    fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    // fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());


                                }

                                #endregion

                                #region Child Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 6)
                                {

                                    fsr.ChdAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.ChdCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.ChdFSur = 0;
                                    ChdTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.ChdTax = (float)Math.Ceiling((decimal)ChdTax);

                                    fsr.ChdRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.ChdFarebasis = objFI.Element("FBCode").Value.Trim();
                                    //fsr.AdtFareType = "Refundable";// fareInfo.FareTypeID.Trim();
                                    //fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                    ChdFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);
                                    fsr.BagInfo = fsr.BagInfo + "(Child)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");
                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + "chd-" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.ChdFSur = fsr.ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.ChdOT = fsr.ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }



                                    }

                                    float alltax = fsr.ChdWO + fsr.ChdJN + fsr.ChdIN + fsr.ChdOT + fsr.ChdFSur;
                                    if (fsr.ChdTax <= alltax)
                                    {
                                        fsr.ChdTax = alltax;
                                    }
                                    fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;
                                    #region Calculation
                                    fsr.ChdOT = fsr.ChdOT + srvCharge;
                                    fsr.ChdTax = fsr.ChdTax + srvCharge;
                                    fsr.ChdFare = fsr.ChdFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.CHDDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.ChdFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, searchInput.TDS);
                                    fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.ChdDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());

                                }
                                #endregion

                                #region Infant Fare


                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 5)
                                {
                                    fsr.InfAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.InfCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.InfFSur = 0;// decimal.Parse(objFI.Element("PTCID").Value);
                                    InfTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.InfTax = (float)Math.Ceiling((decimal)InfTax);// decimal.Parse(objFI.Element("PTCID").Value);

                                    fsr.InfRbd = objFI.Element("FCCode").Value.Trim();// fareInfo.FCCode.Trim();
                                    fsr.InfFarebasis = objFI.Element("FBCode").Value.Trim(); //fareInfo.FBCode.Trim();

                                    InfFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);




                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.InfFSur = fsr.InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.InfOT = fsr.InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }


                                    }

                                    float alltax = fsr.InfWO + fsr.InfJN + fsr.InfIN + fsr.InfOT + fsr.InfFSur;
                                    if (fsr.InfTax <= alltax)
                                    {
                                        fsr.InfTax = alltax;
                                    }
                                    fsr.InfFare = fsr.InfBfare + fsr.InfTax;


                                }

                                #endregion





                            }


                            #endregion



                            //fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            //fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare); ;

                            //fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            //fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            //fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            //fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            //fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            //fsr.Trip = searchInput.Trip.ToString();


                            //fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            //fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            //fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * fsr.AdtFare) - (fsr.Adult * srvCharge) + (fsr.Child * fsr.ChdFare) - (fsr.Child * srvCharge) + (fsr.Infant * fsr.InfFare); ;
                            fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare);
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats1 = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.AvailableSeats = CrdType;
                            fsr.AdtFar = CrdType;
                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(fsr.InfTF * fsr.Infant);
                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);// +(fsr.InfDiscount * fsr.Infant);
                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);// +(fsr.InfCB * fsr.Infant);
                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +(fsr.InfTds * fsr.Infant);
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);// +(fsr.InfAdminMrk * fsr.Infant) + (fsr.InfAgentMrk * fsr.Infant);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));//+ (fsr.InfAgentMrk * fsr.Infant));
                            fsr.OriginalTT = srvCharge;


                            fsrListR.Add(fsr);

                            legcount++;

                        }
                        #endregion


                        lineNum1++;


                    }
                    #endregion





                }
                #endregion

                //result = fsrList;

                #endregion

                FinalList = RoundTripFare(fsrList, fsrListR, srvCharge);

            }
            else
            {
                #region oneWay Parsing

                int lineNum = 1;
                #region flight segment
                for (int fs = 0; fs < xlFSegOnword.Count(); fs++)
                {
                    XElement objFs = xlFSegOnword.ElementAt(fs);
                    IEnumerable<XElement> objFareTypes;
                    if (CrdType == "CRP" || CrdType == "CPN")
                    {
                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       where fts.Element("FareTypeName").Value.Trim().ToLower() == "gospecial"
                                       select fts;
                    }
                    else
                    {
                        objFareTypes = from fts in objFs.Descendants("FareTypes").Descendants("FareType")
                                       select fts;
                    }
                    #region fareType
                    for (int ft = 0; ft < objFareTypes.Count(); ft++)
                    {
                        XElement objFT = objFareTypes.ElementAt(ft);



                        IEnumerable<XElement> objLegDetails = from lds in objFs.Descendants("FlightLegDetails").Descendants("FlightLegDetail")
                                                              select lds;

                        #region legDetais
                        int legcount = 1;
                        for (int ld = 0; ld < objLegDetails.Count(); ld++)
                        {
                            FlightSearchResults fsr = new FlightSearchResults();
                            XElement LegDetail = objLegDetails.ElementAt(ld);

                            XElement objLegDetail = xlLegDetails.Where(x => x.Element("PFID").Value == LegDetail.Element("PFID").Value).FirstOrDefault();
                            XElement segDetails = xlSegmentDetails.Where(x => x.Element("LFID").Value == objFs.Element("LFID").Value).FirstOrDefault();

                            fsr.OrgDestFrom = segDetails.Element("Origin").Value;
                            fsr.OrgDestTo = segDetails.Element("Destination").Value;
                            fsr.Stops = segDetails.Element("Stops").Value + "-Stop";
                            fsr.TotDur = GetTimeInHrsAndMin(Convert.ToInt16(segDetails.Element("FlightTime").Value));
                            fsr.EQ = segDetails.Element("AircraftType").Value.Trim();
                            fsr.MarketingCarrier = segDetails.Element("CarrierCode").Value.Trim();
                            fsr.OperatingCarrier = segDetails.Element("OperatingCarrier").Value.Trim();
                            fsr.ValiDatingCarrier = segDetails.Element("CarrierCode").Value.Trim();

                            fsr.Searchvalue = objFs.Element("LFID").Value;

                            fsr.depdatelcc = objLegDetail.Element("DepartureDate").Value;
                            fsr.arrdatelcc = objLegDetail.Element("ArrivalDate").Value;
                            fsr.Departure_Date = objLegDetail.Element("DepartureDate").Value[8].ToString() + objLegDetail.Element("DepartureDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("DepartureDate").Value[5].ToString() + objLegDetail.Element("DepartureDate").Value[6].ToString()));
                            fsr.Arrival_Date = objLegDetail.Element("ArrivalDate").Value[8].ToString() + objLegDetail.Element("ArrivalDate").Value[9].ToString() + " " + GetMonthName(Convert.ToInt16(objLegDetail.Element("ArrivalDate").Value[5].ToString() + objLegDetail.Element("ArrivalDate").Value[6].ToString()));
                            fsr.FlightIdentification = objLegDetail.Element("FlightNum").Value;
                            fsr.DepartureDate = Convert.ToDateTime(objLegDetail.Element("DepartureDate").Value.Trim()).ToString("ddMMyy");
                            fsr.ArrivalDate = Convert.ToDateTime(objLegDetail.Element("ArrivalDate").Value.Trim()).ToString("ddMMyy");
                            fsr.Sector = fsr.OrgDestFrom + ":" + fsr.OrgDestTo;


                            fsr.DepartureLocation = objLegDetail.Element("Origin").Value;
                            fsr.ArrivalLocation = objLegDetail.Element("Destination").Value;
                            fsr.ArrivalCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Destination").Value.Trim());
                            fsr.DepartureCityName = GetAirPortAndLocationName(CityList, 2, objLegDetail.Element("Origin").Value);
                            fsr.DepartureTime = objLegDetail.Element("DepartureDate").Value.Remove(0, objLegDetail.Element("DepartureDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.ArrivalTime = objLegDetail.Element("ArrivalDate").Value.Remove(0, objLegDetail.Element("ArrivalDate").Value.IndexOf("T") + 1).Replace(":", "").Remove(4);
                            fsr.DepartureAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Origin").Value.Trim());
                            fsr.ArrivalAirportName = GetAirPortAndLocationName(CityList, 1, objLegDetail.Element("Destination").Value.Trim());
                            fsr.ArrAirportCode = objLegDetail.Element("Destination").Value.Trim();
                            fsr.DepAirportCode = objLegDetail.Element("Origin").Value.Trim();

                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.TripType = searchInput.TripType.ToString();
                            fsr.LineNumber = lineNum;
                            fsr.Leg = legcount;
                            fsr.Flight = "1";
                            fsr.AirLineName = "GoAir";
                            fsr.Adult = searchInput.Adult;
                            fsr.Child = searchInput.Child;
                            fsr.Infant = searchInput.Infant;
                            fsr.TotPax = searchInput.Adult + searchInput.Child;
                            fsr.Provider = "LCC";


                            IEnumerable<XElement> FareInfos = from fInf in objFT.Descendants("FareInfos").Descendants("FareInfo")
                                                              select fInf;


                            float AdtFare = 0, ChdFare = 0, InfFare = 0;
                            float AdtTax = 0, ChdTax = 0, InfTax = 0;

                            #region fareInfo

                            for (int fi = 0; fi < FareInfos.Count(); fi++)
                            {
                                XElement objFI = FareInfos.ElementAt(fi);

                                // decimal totalTax = 0;
                                //totalTax = Convert.ToDecimal(objFI.Element("DisplayTaxSum").Value); //- Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value);

                                IEnumerable<XElement> TaxDetails = from taxD in objFI.Descendants("ApplicableTaxDetails").Descendants("ApplicableTaxDetail")
                                                                   select taxD;

                                #region Adult Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 1)
                                {
                                    fsr.AdtAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.AdtBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.AdtCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.AdtFSur = 0;
                                    AdtTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.AdtTax = (float)Math.Ceiling((decimal)AdtTax);

                                    fsr.AdtRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.AdtFarebasis = objFI.Element("FBCode").Value.Trim();
                                    fsr.AdtFareType = "Refundable";

                                    AdtFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);  // float.Parse(objFI.Element("FareAmtInclTax").Value);
                                    //(float)Math.Ceiling(Convert.ToDecimal(objFI.Element("FareAmtInclTax").Value));//fareInfo.BaseFareAmtInclTax);
                                    string provider = "G8"; //searchInput.HidTxtDepCity.Split(',')[1].ToUpper().Trim() == "IN" ? "FZ" : "FZINT";
                                    fsr.sno = objFI.Element("FareID").Value.Trim() + ":" + SecurityGUID + ":" + objFT.Element("FareTypeName").Value.Trim() + ":" + segDetails.Element("DepartureDate").Value + ":" + provider;
                                    fsr.fareBasis = fsr.AdtFarebasis;
                                    fsr.FBPaxType = "ADT";


                                    fsr.BagInfo = "(Adult)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");

                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo = fsr.BagInfo + " + " + taxel.Element("TaxDesc").Value;

                                            fsr.Searchvalue = fsr.Searchvalue + ":" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.AdtWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.AdtJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.AdtIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.AdtFSur = fsr.AdtFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.AdtOT = fsr.AdtOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }

                                    }

                                    //fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    float alltax = fsr.AdtWO + fsr.AdtJN + fsr.AdtIN + fsr.AdtOT + fsr.AdtFSur;
                                    if (fsr.AdtTax <= alltax)
                                    {
                                        fsr.AdtTax = alltax;
                                    }
                                    else
                                    {
                                        fsr.AdtTax = fsr.AdtTax;// +srvCharge;
                                    }
                                    fsr.AdtFare = fsr.AdtBfare + fsr.AdtTax;
                                    #region Calculation
                                    fsr.AdtOT = fsr.AdtOT + srvCharge;
                                    fsr.AdtTax = fsr.AdtTax + srvCharge;
                                    fsr.AdtFare = fsr.AdtFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.ADTAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.ADTDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.AdtBfare.ToString()), decimal.Parse(fsr.AdtFSur.ToString()), 1, fsr.AdtRbd, fsr.AdtCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.AdtFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.AdtDiscount1, fsr.AdtBfare, fsr.AdtFSur, searchInput.TDS);
                                    fsr.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.AdtDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //  fsr.ADTAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());

                                }

                                #endregion

                                #region Child Fare

                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 6)
                                {

                                    fsr.ChdAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.ChdBFare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.ChdCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.ChdFSur = 0;
                                    ChdTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.ChdTax = (float)Math.Ceiling((decimal)ChdTax);

                                    fsr.ChdRbd = objFI.Element("FCCode").Value.Trim();
                                    fsr.ChdFarebasis = objFI.Element("FBCode").Value.Trim();
                                    //fsr.AdtFareType = "Refundable";// fareInfo.FareTypeID.Trim();
                                    //fsr.AdtFareTypeName = fareInfo.FareTypeName.Trim();
                                    ChdFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);

                                    fsr.BagInfo = fsr.BagInfo + "(Child)" + (objFT.Element("FareTypeName").Value.Trim().ToUpper() == "GOBUSINESS" ? "35 kg baggage allowance" : "15 kg baggage allowance");
                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                            fsr.Searchvalue = fsr.Searchvalue + "chd-" + taxel.Element("TaxCode").Value + ":" + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.ChdWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.ChdJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.ChdIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.ChdFSur = fsr.ChdFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.ChdOT = fsr.ChdOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }



                                    }
                                    fsr.ChdOT = fsr.ChdOT; // +srvCharge;
                                    float alltax = fsr.ChdWO + fsr.ChdJN + fsr.ChdIN + fsr.ChdOT + fsr.ChdFSur;
                                    if (fsr.ChdTax <= alltax)
                                    {
                                        fsr.ChdTax = alltax;
                                    }
                                    else
                                    {
                                        fsr.ChdTax = fsr.ChdTax;// +srvCharge;
                                    }
                                    fsr.ChdFare = fsr.ChdBFare + fsr.ChdTax;
                                    #region Calculation
                                    fsr.ChdOT = fsr.ChdOT + srvCharge;
                                    fsr.ChdTax = fsr.ChdTax + srvCharge;
                                    fsr.ChdFare = fsr.ChdFare + srvCharge;
                                    CommDt.Clear();
                                    STTFTDS.Clear();
                                    fsr.CHDAdminMrk = 0;// CalcMarkup(MarkupDs.Tables["AdminMarkUp"], objlist.ValiDatingCarrier, objlist.AdtFare, searchInputs.Trip.ToString()); // float.Parse(Markup["Admin_Markup"].ToString());
                                    fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.AdtFare, searchInput.Trip.ToString());  //float.Parse(Markup["Dist_Markup"].ToString());
                                    fsr.CHDDistMrk = 0; //float.Parse(Markup["Agnt_Markup"].ToString());                                                       
                                    CommDt = FCBAL.GetFltComm_Gal(searchInput.AgentType, fsr.ValiDatingCarrier, decimal.Parse(fsr.ChdBFare.ToString()), decimal.Parse(fsr.ChdFSur.ToString()), 1, fsr.ChdRbd, fsr.ChdCabin, searchInput.DepDate, fsr.OrgDestFrom + "-" + fsr.OrgDestTo, searchInput.RetDate, fsr.ChdFarebasis, searchInput.HidTxtDepCity.Split(',')[1].ToString().Trim(), searchInput.HidTxtArrCity.Split(',')[1].ToString().Trim(), fsr.FlightIdentification, fsr.OperatingCarrier, fsr.MarketingCarrier, CrdType, "");
                                    fsr.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                                    fsr.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                                    STTFTDS = CalcSrvTaxTFeeTds(SrvChargeList, fsr.ValiDatingCarrier, fsr.ChdDiscount1, fsr.ChdBFare, fsr.ChdFSur, searchInput.TDS);
                                    fsr.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                                    fsr.ChdDiscount = fsr.AdtDiscount1 - fsr.AdtSrvTax1;
                                    fsr.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                                    fsr.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                                    #endregion
                                    //fsr.CHDAgentMrk = CalcMarkup(MarkupDs.Tables["AgentMarkUp"], fsr.ValiDatingCarrier, fsr.ChdFare, searchInput.Trip.ToString());

                                }
                                #endregion

                                #region Infant Fare


                                if (Convert.ToInt16(objFI.Element("PTCID").Value) == 5)
                                {
                                    fsr.InfAvlStatus = objFI.Element("SeatsAvailable").Value.Trim();
                                    fsr.InfBfare = (float)Math.Ceiling(Convert.ToDecimal(objFI.Element("DisplayFareAmt").Value.Trim()));
                                    fsr.InfCabin = objFI.Element("Cabin").Value.Trim();
                                    fsr.InfFSur = 0;// decimal.Parse(objFI.Element("PTCID").Value);
                                    InfTax = float.Parse(objFI.Element("DisplayTaxSum").Value);
                                    fsr.InfTax = (float)Math.Ceiling((decimal)InfTax);// decimal.Parse(objFI.Element("PTCID").Value);

                                    fsr.InfRbd = objFI.Element("FCCode").Value.Trim();// fareInfo.FCCode.Trim();
                                    fsr.InfFarebasis = objFI.Element("FBCode").Value.Trim(); //fareInfo.FBCode.Trim();

                                    InfFare = float.Parse(objFI.Element("BaseFareAmtInclTax").Value);




                                    for (int td = 0; td < TaxDetails.Count(); td++)
                                    {
                                        XElement taxseg = TaxDetails.ElementAt(td);
                                        XElement taxel = xlTaxDetails.Where(x => x.Element("TaxID").Value.Trim() == taxseg.Element("TaxID").Value.Trim()).First();

                                        if (taxel.Element("TaxCode").Value.Contains("BAG"))
                                        {
                                            fsr.BagInfo += " + " + taxel.Element("TaxDesc").Value;
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "WO")
                                        {
                                            fsr.InfWO = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "JN")
                                        {
                                            fsr.InfJN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim() == "IN")
                                        {
                                            fsr.InfIN = (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));

                                        }
                                        else if (taxel.Element("TaxCode").Value.Trim().Contains("FUEL"))
                                        {
                                            fsr.InfFSur = fsr.InfFSur + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }
                                        else
                                        {
                                            fsr.InfOT = fsr.InfOT + (float)Math.Ceiling(Convert.ToDecimal(taxseg.Element("Amt").Value));
                                        }


                                    }

                                    float alltax = fsr.InfWO + fsr.InfJN + fsr.InfIN + fsr.InfOT + fsr.InfFSur;
                                    if (fsr.InfTax <= alltax)
                                    {
                                        fsr.InfTax = alltax;
                                    }
                                    fsr.InfFare = fsr.InfBfare + fsr.InfTax;


                                }

                                #endregion





                            }


                            #endregion



                            //fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            //fsr.OriginalTT = srvCharge; //(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            //fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare); ;

                            //fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            //fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            //fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            //fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            //fsr.AvailableSeats = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            //fsr.Trip = searchInput.Trip.ToString();

                            //fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);
                            //fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee + fsr.TotMgtFee;
                            //fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));
                            fsr.RBD = fsr.AdtRbd + ":" + fsr.ChdRbd + ":" + fsr.InfRbd;
                            fsr.OriginalTT = 0;//(fsr.Adult * AdtTax) + (fsr.Child * ChdTax) + (fsr.Infant * InfTax);
                            // fsr.OriginalTF = (fsr.Adult * fsr.AdtFare) - (fsr.Adult * srvCharge) + (fsr.Child * fsr.ChdFare) - (fsr.Child * srvCharge) + (fsr.Infant * fsr.InfFare); ;
                            fsr.OriginalTF = (fsr.Adult * AdtFare) + (fsr.Child * ChdFare) + (fsr.Infant * InfFare);
                            fsr.TotalFare = (fsr.Adult * fsr.AdtFare) + (fsr.Child * fsr.ChdFare) + (fsr.Infant * fsr.InfFare);
                            fsr.TotalFuelSur = (fsr.Adult * fsr.AdtFSur) + (fsr.Child * fsr.ChdFSur) + (fsr.Infant * fsr.InfFSur);
                            fsr.TotalTax = (fsr.Adult * fsr.AdtTax) + (fsr.Child * fsr.ChdTax) + (fsr.Infant * fsr.InfTax);
                            fsr.TotBfare = (fsr.Adult * fsr.AdtBfare) + (fsr.Child * fsr.ChdBFare) + (fsr.Infant * fsr.InfBfare);
                            fsr.AvailableSeats1 = fsr.AdtAvlStatus + ":" + fsr.ChdAvlStatus + ":" + fsr.InfAvlStatus;
                            fsr.AvailableSeats = CrdType;
                            fsr.AdtFar = CrdType;
                            fsr.Trip = searchInput.Trip.ToString();
                            fsr.STax = (fsr.AdtSrvTax * fsr.Adult) + (fsr.ChdSrvTax * fsr.Child) + (fsr.InfSrvTax * fsr.Infant);
                            fsr.TFee = (fsr.AdtTF * fsr.Adult) + (fsr.ChdTF * fsr.Child);// +(fsr.InfTF * fsr.Infant);
                            fsr.TotDis = (fsr.AdtDiscount * fsr.Adult) + (fsr.ChdDiscount * fsr.Child);// +(fsr.InfDiscount * fsr.Infant);
                            fsr.TotCB = (fsr.AdtCB * fsr.Adult) + (fsr.ChdCB * fsr.Child);// +(fsr.InfCB * fsr.Infant);
                            fsr.TotTds = (fsr.AdtTds * fsr.Adult) + (fsr.ChdTds * fsr.Child);// +(fsr.InfTds * fsr.Infant);
                            fsr.TotMrkUp = (fsr.ADTAdminMrk * fsr.Adult) + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAdminMrk * fsr.Child) + (fsr.CHDAgentMrk * fsr.Child);// +(fsr.InfAdminMrk * fsr.Infant) + (fsr.InfAgentMrk * fsr.Infant);
                            fsr.TotalFare = fsr.TotalFare + fsr.TotMrkUp + fsr.STax + fsr.TFee;
                            fsr.NetFare = (fsr.TotalFare + fsr.TotTds) - (fsr.TotDis + fsr.TotCB + (fsr.ADTAgentMrk * fsr.Adult) + (fsr.CHDAgentMrk * fsr.Child));//+ (fsr.InfAgentMrk * fsr.Infant));
                            fsr.OriginalTT = srvCharge;


                            fsrList.Add(fsr);

                            legcount++;

                        }
                        #endregion


                        lineNum++;


                    }
                    #endregion





                }
                #endregion

                FinalList.Add(fsrList);

                #endregion

            }
            //FlightCommonBAL objFltComm = new FlightCommonBAL();
            FCBAL.AddFlightKey((List<FlightSearchResults>)FinalList[0], searchInput.RTF);

            return FinalList;

        }

        private ArrayList RoundTripFare(List<FlightSearchResults> objO, List<FlightSearchResults> objR, float srvCharge)
        {

            int ln = 1;//For Total Line No.         
            int LnOb = objO[objO.Count - 1].LineNumber;
            int LnIb = objR[objR.Count - 1].LineNumber;
            ArrayList Comb = new ArrayList();
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            for (int k = 1; k <= LnOb; k++)
            {
                var OB = (from ct in objO where ct.LineNumber == k select ct).ToList();

                for (int l = 1; l <= LnIb; l++)
                {
                    var IB = (from c in objR where c.LineNumber == l select c).ToList();
                    //List<FlightSearchResults> st = new List<FlightSearchResults>();

                    if (OB[OB.Count - 1].DepartureDate.Split('T')[0] == IB[0].DepartureDate.Split('T')[0])
                    {
                        int obtmin = Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(OB[OB.Count - 1].DepartureTime.Substring(2, 2));
                        int ibtmin = Convert.ToInt16(IB[0].DepartureTime.Substring(0, 2)) * 60 + Convert.ToInt16(IB[0].DepartureTime.Substring(2, 2));

                        if ((obtmin + 120) <= ibtmin)
                        {
                            // st = Merge(OB, IB, ln);
                            Final.AddRange(Merge(OB, IB, ln, srvCharge));
                            ln++;///Increment Total Ln
                        }
                    }
                    else
                    {
                        Final.AddRange(Merge(OB, IB, ln, srvCharge));
                        ln++;
                    }

                }

            }
            Comb.Add(Final);
            return Comb;
        }

        private List<FlightSearchResults> Merge(List<FlightSearchResults> OB, List<FlightSearchResults> IB, int Ln, float srvCharge)
        {
            List<FlightSearchResults> Final = new List<FlightSearchResults>();

            float AdtFSur = 0, AdtWO = 0, AdtIN = 0, AdtJN = 0, AdtYR = 0, AdtBfare = 0, AdtOT = 0, AdtFare = 0, AdtTax = 0;//,
            float ADTAdminMrk = 0, ADTAgentMrk = 0, AdtDiscount = 0, AdtCB = 0, AdtSrvTax = 0, AdtTF = 0, AdtTds = 0, IATAComm = 0;
            float ChdFSur = 0, ChdWO = 0, ChdIN = 0, ChdJN = 0, ChdYR = 0, ChdBFare = 0, ChdOT = 0, ChdFare = 0, ChdTax = 0;//,
            float CHDAdminMrk = 0, CHDAgentMrk = 0, ChdDiscount = 0, ChdCB = 0, ChdSrvTax = 0, ChdTF = 0, ChdTds = 0;
            float InfFSur = 0, InfIN = 0, InfJN = 0, InfOT = 0, InfQ = 0, InfFare = 0, InfBfare = 0, InfTax = 0,
            InfSrvTax = 0, InfTF = 0;
            //float ADTAgentMrk = 0, CHDAgentMrk = 0;
            var ObF = OB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var IbF = IB.Select(x => (FlightSearchResults)x.Clone()).ToList();
            var item = (FlightSearchResults)OB[0].Clone();
            var itemib = (FlightSearchResults)IB[0].Clone();
            #region ADULT
            int Adult = item.Adult;
            AdtFSur = AdtFSur + item.AdtFSur + itemib.AdtFSur;
            AdtWO = AdtWO + item.AdtWO + itemib.AdtWO;
            AdtIN = AdtIN + item.AdtIN + itemib.AdtIN;
            AdtJN = AdtJN + item.AdtJN + itemib.AdtJN;
            AdtYR = AdtYR + item.AdtYR + itemib.AdtYR;
            AdtBfare = AdtBfare + item.AdtBfare + itemib.AdtBfare;
            AdtOT = AdtOT + item.AdtOT + itemib.AdtOT + srvCharge;
            AdtFare = AdtFare + item.AdtFare + itemib.AdtFare + srvCharge;
            AdtTax = AdtTax + item.AdtTax + itemib.AdtTax + srvCharge;
            ADTAgentMrk = ADTAgentMrk + item.ADTAgentMrk + itemib.ADTAgentMrk;

            ADTAdminMrk = ADTAdminMrk + item.ADTAdminMrk + itemib.ADTAdminMrk;
            AdtDiscount = AdtDiscount + item.AdtDiscount + itemib.AdtDiscount;
            AdtCB = AdtCB + item.AdtCB + itemib.AdtCB;
            AdtSrvTax = AdtSrvTax + item.AdtSrvTax + itemib.AdtSrvTax;
            AdtTF = AdtTF + item.AdtTF + itemib.AdtTF;
            AdtTds = AdtTds + item.AdtTds + itemib.AdtTds;
            IATAComm = IATAComm + item.IATAComm + itemib.IATAComm;
            #endregion

            #region CHILD
            int Child = item.Child;
            ChdFSur = ChdFSur + item.ChdFSur + itemib.ChdFSur;
            ChdWO = ChdWO + item.ChdWO + itemib.ChdWO;
            ChdIN = ChdIN + item.ChdIN + itemib.ChdIN;
            ChdJN = ChdJN + item.ChdJN + itemib.ChdJN;
            ChdYR = ChdYR + item.ChdYR + itemib.ChdYR;
            ChdBFare = ChdBFare + item.ChdBFare + itemib.ChdBFare;
            ChdOT = ChdOT + item.ChdOT + itemib.ChdOT + srvCharge;
            ChdFare = ChdFare + item.ChdFare + itemib.ChdFare + srvCharge;
            ChdTax = ChdTax + item.ChdTax + itemib.ChdTax + srvCharge;
            // CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;
            CHDAdminMrk = CHDAdminMrk + item.CHDAdminMrk + itemib.CHDAdminMrk;
            CHDAgentMrk = CHDAgentMrk + item.CHDAgentMrk + itemib.CHDAgentMrk;
            ChdDiscount = ChdDiscount + item.ChdDiscount + itemib.ChdDiscount;
            ChdCB = ChdCB + item.ChdCB + itemib.ChdCB;
            ChdSrvTax = ChdSrvTax + item.ChdSrvTax + itemib.ChdSrvTax;
            ChdTF = ChdTF + item.ChdTF + itemib.ChdTF;
            ChdTds = ChdTds + item.ChdTds + itemib.ChdTds;

            #endregion

            #region INFANT
            int Infant = item.Infant;
            InfFare = InfFare + item.InfFare + itemib.InfFare;
            InfBfare = InfBfare + item.InfBfare + itemib.InfBfare;
            InfFSur = InfFSur + item.InfFSur + itemib.InfFSur;
            InfIN = InfIN + item.InfIN + itemib.InfIN;
            InfJN = InfJN + item.InfJN + itemib.InfJN;
            InfOT = InfOT + item.InfOT + itemib.InfOT;
            InfQ = InfQ + item.InfQ + itemib.InfQ;
            InfTax = InfTax + item.InfTax + itemib.InfTax;
            #endregion

            #region TOTAL
            float OriginalTF = item.OriginalTF + itemib.OriginalTF;
            float OriginalTT = item.OriginalTT + itemib.OriginalTT;
            float TotBfare = (AdtBfare * Adult) + (ChdBFare * Child) + (InfBfare * Infant);
            float TotalTax = (AdtTax * Adult) + (ChdTax * Child) + (InfTax * Infant);
            float TotalFuelSur = (AdtFSur * Adult) + (ChdFSur * Child);
            float TotalFare = (float)Math.Ceiling(Convert.ToDecimal((AdtFare * Adult) + (ChdFare * Child) + (InfFare * Infant)));
            //float TotMrkUp = (ADTAgentMrk * Adult) + (CHDAgentMrk * Child);
            //float NetFare = TotalFare - ((ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            float STax = (AdtSrvTax * Adult) + (ChdSrvTax * Child) + (InfSrvTax * Infant);
            float TFee = (AdtTF * Adult) + (ChdTF * Child) + (InfTF * Infant);
            float TotDis = (AdtDiscount * Adult) + (ChdDiscount * Child);
            float TotCB = (AdtCB * Adult) + (ChdCB * Child);
            float TotTds = (AdtTds * Adult) + (ChdTds * Child);// +InfTds;
            float TotMrkUp = (ADTAdminMrk * Adult) + (ADTAgentMrk * Adult) + (CHDAdminMrk * Child) + (CHDAgentMrk * Child);
            TotalFare = TotalFare + TotMrkUp + STax + TFee;
            float NetFare = (TotalFare + TotTds) - (TotDis + TotCB + (ADTAgentMrk * Adult) + (CHDAgentMrk * Child));
            #endregion


            ObF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                // y.ADTAgentMrk = ADTAgentMrk;
                y.ADTAdminMrk = ADTAdminMrk;
                y.ADTAgentMrk = ADTAgentMrk;
                y.AdtDiscount = AdtDiscount;
                y.AdtCB = AdtCB;
                y.AdtSrvTax = AdtSrvTax;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                #endregion


                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                //y.CHDAgentMrk = CHDAgentMrk;
                y.CHDAdminMrk = CHDAdminMrk;
                y.CHDAgentMrk = CHDAgentMrk;
                y.ChdDiscount = ChdDiscount;
                y.ChdCB = ChdCB;
                y.ChdSrvTax = ChdSrvTax;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;
                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = OriginalTT;
                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotTds = TotTds;
                y.TotMrkUp = TotMrkUp;
                y.NetFare = NetFare;
                #endregion
            });
            Final.AddRange(ObF);



            IbF.ForEach(y =>
            {
                #region Adult
                y.LineNumber = Ln;
                y.AdtFSur = AdtFSur;
                y.AdtIN = AdtIN;
                y.AdtJN = AdtJN;
                y.AdtYR = AdtYR;
                y.AdtBfare = AdtBfare;
                y.AdtOT = AdtOT;
                y.AdtFare = AdtFare;
                y.AdtTax = AdtTax;
                // y.ADTAgentMrk = ADTAgentMrk;

                y.ADTAdminMrk = ADTAdminMrk;
                y.ADTAgentMrk = ADTAgentMrk;
                y.AdtDiscount = AdtDiscount;
                y.AdtCB = AdtCB;
                y.AdtSrvTax = AdtSrvTax;
                y.AdtTF = AdtTF;
                y.AdtTds = AdtTds;
                #endregion


                #region Child
                y.ChdFSur = ChdFSur;
                y.ChdWO = ChdWO;
                y.ChdIN = ChdIN;
                y.ChdJN = ChdJN;
                y.ChdYR = ChdYR;
                y.ChdBFare = ChdBFare;
                y.ChdOT = ChdOT;
                y.ChdFare = ChdFare;
                y.ChdTax = ChdTax;
                //y.CHDAgentMrk = CHDAgentMrk;

                y.CHDAdminMrk = CHDAdminMrk;
                y.CHDAgentMrk = CHDAgentMrk;
                y.ChdDiscount = ChdDiscount;
                y.ChdCB = ChdCB;
                y.ChdSrvTax = ChdSrvTax;
                y.ChdTF = ChdTF;
                y.ChdTds = ChdTds;

                #endregion

                #region Infant
                y.InfFare = InfFare;
                y.InfBfare = InfBfare;
                y.InfFSur = InfFSur;
                y.InfIN = InfIN;
                y.InfJN = InfJN;
                y.InfOT = InfOT;
                y.InfQ = InfQ;
                y.InfTax = InfTax;
                #endregion

                #region Total
                y.TotBfare = TotBfare;
                y.TotalTax = TotalTax;
                y.TotalFuelSur = TotalFuelSur;
                y.TotalFare = TotalFare;
                y.OriginalTF = OriginalTF;
                y.OriginalTT = srvCharge;//OriginalTT;
                y.NetFare = NetFare;


                y.STax = STax;
                y.TFee = TFee;
                y.TotDis = TotDis;
                y.TotTds = TotTds;
                y.TotMrkUp = TotMrkUp;
                #endregion
            });
            Final.AddRange(IbF);


            return Final;
        }

        private string GetAirPortAndLocationName(List<FlightCityList> CityList, int i, string depLocCode)
        { // i=1 for airport, i=2 for location name
            string name = "";
            if (i == 1)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].AirportName;
            }
            if (i == 2)
            {
                name = ((from ct in CityList where ct.AirportCode.Trim() == depLocCode.Trim() select ct).ToList())[0].City;
            }

            return name;
        }
        private string GetMonthName(int monthNo)
        {

            Dictionary<int, string> month = new Dictionary<int, string>();

            month.Add(01, "JAN");
            month.Add(02, "FEB");
            month.Add(03, "MAR");
            month.Add(04, "APR");
            month.Add(05, "MAY");
            month.Add(06, "JUN");
            month.Add(07, "JUL");
            month.Add(08, "AUG");
            month.Add(09, "SEP");
            month.Add(10, "OCT");
            month.Add(11, "NOV");
            month.Add(12, "DEC");

            return month[monthNo];

        }

        private string GetTimeInHrsAndMin(int min)
        {
            string rslt;
            if (min < 60)
            {
                rslt = "00:" + min.ToString("00");
            }
            else
            {
                int hrs = min / 60;
                int rmin = min % 60;

                rslt = hrs.ToString("00") + ":" + rmin.ToString("00");
            }

            return rslt;

        }
        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");

                if (!(airMrkArray != null && airMrkArray.Length > 0))
                {
                    airMrkArray = Mrkdt.Select("AirlineCode='ALL'", "");

                }

                if (airMrkArray.Length > 0)
                {

                    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    {
                        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    }
                    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    }

                    //if (Trip == "I")
                    //{
                    //    if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                    //    {
                    //        mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                    //    }
                    //    else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                    //    {
                    //        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                    //    }
                    //}
                    //else
                    //{
                    //    mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    //}
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            //int IATAComm = 0;
            decimal originalDis = 0;
            Hashtable STHT = new Hashtable();
            if (string.IsNullOrEmpty(TDS))
            {
                TDS = "0";
            }

            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                originalDis = decimal.Parse(Dis.ToString()) - decimal.Parse(STHT["TFee"].ToString());
                STHT.Add("STax", Math.Round(((originalDis * STaxP) / 100), 0));
                STHT.Add("Tds", Math.Round((((originalDis - decimal.Parse(STHT["STax"].ToString())) * decimal.Parse(TDS)) / 100), 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }

    }
}
