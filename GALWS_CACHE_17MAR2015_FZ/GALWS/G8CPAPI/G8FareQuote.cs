﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Collections;
using STD.Shared;

namespace STD.BAL
{
    public class G8FareQuote
    {
        string SecurityToken;
        string IATANumber;
        string UserName;
        string Password;


        public G8FareQuote(string securityToken, string Iatanumber, string username, string password)
        {
            SecurityToken = securityToken;
            IATANumber = Iatanumber;
            UserName = username;
            Password = password;

        }

        public string GetFareQuote(string methodUrl, string serviceUrl, FlightSearch searchInput,bool IsNRTP, ref string exep)
        {
            string res = "";

            string[] depD = searchInput.DepDate.Split('/');
            string[] arrD = searchInput.RetDate.Split('/');
            string departureDate = "";
            string arrivalDate = "";
            if (searchInput.DepDate.Trim() == searchInput.RetDate.Trim() && searchInput.DepDate.Replace('/', '-') == DateTime.Now.ToString("dd/MM/yyyy"))
            {

                //string [] ndate = searchInput.DepDate.Split('/');

                DateTime newDate = new DateTime(Convert.ToInt16(depD[2]), Convert.ToInt16(depD[1]), Convert.ToInt16(depD[0]), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second).AddHours(2);

                departureDate = depD.Length > 1 ? (newDate.Year.ToString() + "-" + (newDate.Month.ToString().Trim().Length > 1 ? newDate.Month.ToString().Trim() : "0" + newDate.Month.ToString().Trim()) + "-" + (newDate.Day.ToString().Trim().Length > 1 ? newDate.Day.ToString().Trim() : "0" + newDate.Day.ToString().Trim())) : "";
                arrivalDate = depD.Length > 1 ? (newDate.Year.ToString() + "-" + (newDate.Month.ToString().Trim().Length > 1 ? newDate.Month.ToString().Trim() : "0" + newDate.Month.ToString().Trim()) + "-" + (newDate.Day.ToString().Trim().Length > 1 ? newDate.Day.ToString().Trim() : "0" + newDate.Day.ToString().Trim())) : "";
            }           
            else
            {
                departureDate = depD.Length > 1 ? (depD[2] + "-" + (depD[1].Trim().Length > 1 ? depD[1].Trim() : "0" + depD[1].Trim()) + "-" + (depD[0].Trim().Length > 1 ? depD[0].Trim() : "0" + depD[0].Trim())) : "";
                arrivalDate = arrD.Length > 1 ? (arrD[2] + "-" + (arrD[1].Trim().Length > 1 ? arrD[1].Trim() : "0" + arrD[1].Trim()) + "-" + (arrD[0].Trim().Length > 1 ? arrD[0].Trim() : "0" + arrD[0].Trim())) : "";
            }

           


            StringBuilder FareQuoteReq = new StringBuilder();

            FareQuoteReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Request.FareQuote\">");
            FareQuoteReq.Append("<soapenv:Header/><soapenv:Body><tem:RetrieveFareQuote><!--Optional:--><tem:RetrieveFareQuoteRequest>");
            FareQuoteReq.Append(" <rad:SecurityGUID>" + SecurityToken + "</rad:SecurityGUID>");
            FareQuoteReq.Append("<rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode>");
            FareQuoteReq.Append("<rad:AccessibleCarrierCode>G8</rad:AccessibleCarrierCode>");
            FareQuoteReq.Append("</rad:CarrierCode></rad:CarrierCodes>");
            FareQuoteReq.Append("<rad:HistoricUserName>" + UserName + "</rad:HistoricUserName>");
            FareQuoteReq.Append("<rad1:CurrencyOfFareQuote>INR</rad1:CurrencyOfFareQuote>");
            //FareQuoteReq.Append("<rad1:PromotionalCode>TESTPRO3</rad1:PromotionalCode>");
            FareQuoteReq.Append("<rad1:PromotionalCode></rad1:PromotionalCode>");
            FareQuoteReq.Append("<rad1:IataNumberOfRequestor>" + IATANumber + "</rad1:IataNumberOfRequestor>");
            FareQuoteReq.Append("<rad1:CorporationID>-214</rad1:CorporationID>");
            FareQuoteReq.Append("<rad1:FareFilterMethod>NoCombinabilityRoundtripLowestFarePerFareType</rad1:FareFilterMethod>");
            FareQuoteReq.Append("<rad1:FareGroupMethod>WebFareTypes</rad1:FareGroupMethod>");
            FareQuoteReq.Append("<rad1:InventoryFilterMethod>Available</rad1:InventoryFilterMethod>");
            #region FareQuoteDetails
            FareQuoteReq.Append("<rad1:FareQuoteDetails>");
            #region oneWay
            FareQuoteReq.Append("<rad1:FareQuoteDetail>");
            if (IsNRTP)
            {
                FareQuoteReq.Append("<rad1:Origin>" + searchInput.HidTxtArrCity.Split(',')[0] + "</rad1:Origin>");
                FareQuoteReq.Append("<rad1:Destination>" + searchInput.HidTxtDepCity.Split(',')[0] + "</rad1:Destination>");
                FareQuoteReq.Append("<rad1:UseAirportsNotMetroGroups>false</rad1:UseAirportsNotMetroGroups>");
                FareQuoteReq.Append("<rad1:DateOfDeparture>" + arrivalDate + "</rad1:DateOfDeparture>");
            }
            else
            {
                FareQuoteReq.Append("<rad1:Origin>" + searchInput.HidTxtDepCity.Split(',')[0] + "</rad1:Origin>");
                FareQuoteReq.Append("<rad1:Destination>" + searchInput.HidTxtArrCity.Split(',')[0] + "</rad1:Destination>");
                FareQuoteReq.Append("<rad1:UseAirportsNotMetroGroups>false</rad1:UseAirportsNotMetroGroups>");
                FareQuoteReq.Append("<rad1:DateOfDeparture>" + departureDate + "</rad1:DateOfDeparture>");
            }
           
            FareQuoteReq.Append("<rad1:FareTypeCategory>1</rad1:FareTypeCategory>");
            FareQuoteReq.Append("<rad1:FareClass></rad1:FareClass>");
            FareQuoteReq.Append("<rad1:FareBasisCode></rad1:FareBasisCode>");
            FareQuoteReq.Append("<rad1:Cabin>" + searchInput.Cabin + "</rad1:Cabin>");
            FareQuoteReq.Append("<rad1:LFID>-214</rad1:LFID>");
            FareQuoteReq.Append("<rad1:OperatingCarrierCode>G8</rad1:OperatingCarrierCode>");
            FareQuoteReq.Append("<rad1:MarketingCarrierCode>G8</rad1:MarketingCarrierCode>");
            FareQuoteReq.Append("<rad1:NumberOfDaysBefore>0</rad1:NumberOfDaysBefore>");
            FareQuoteReq.Append("<rad1:NumberOfDaysAfter>0</rad1:NumberOfDaysAfter>");
            FareQuoteReq.Append("<rad1:LanguageCode>en</rad1:LanguageCode>");
            FareQuoteReq.Append("<rad1:TicketPackageID>1</rad1:TicketPackageID>");

            #region pax details
            FareQuoteReq.Append("<rad1:FareQuoteRequestInfos>");
            if (searchInput.Adult > 0)
            {
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                FareQuoteReq.Append("<rad1:PassengerTypeID>1</rad1:PassengerTypeID>");
                FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Adult + "</rad1:TotalSeatsRequired>");
                FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
            }
            if (searchInput.Child > 0)
            {
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                FareQuoteReq.Append("<rad1:PassengerTypeID>6</rad1:PassengerTypeID>");
                FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Child + "</rad1:TotalSeatsRequired>");
                FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
            }

            if (searchInput.Infant > 0)
            {
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                FareQuoteReq.Append("<rad1:PassengerTypeID>5</rad1:PassengerTypeID>");
                FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Infant + "</rad1:TotalSeatsRequired>");
                FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
            }

            FareQuoteReq.Append("</rad1:FareQuoteRequestInfos>");



            #endregion
            FareQuoteReq.Append("</rad1:FareQuoteDetail>");
            #endregion

            if (searchInput.TripType == TripType.RoundTrip && searchInput.RTF==true)
            {

                #region RoundTrip
                FareQuoteReq.Append("<rad1:FareQuoteDetail>");
                FareQuoteReq.Append("<rad1:Origin>" + searchInput.HidTxtArrCity.Split(',')[0] + "</rad1:Origin>");
                FareQuoteReq.Append("<rad1:Destination>" + searchInput.HidTxtDepCity.Split(',')[0] + "</rad1:Destination>");
                FareQuoteReq.Append("<rad1:UseAirportsNotMetroGroups>false</rad1:UseAirportsNotMetroGroups>");
                FareQuoteReq.Append("<rad1:DateOfDeparture>" + arrivalDate + "</rad1:DateOfDeparture>");
                FareQuoteReq.Append("<rad1:FareTypeCategory>1</rad1:FareTypeCategory>");
                FareQuoteReq.Append("<rad1:FareClass></rad1:FareClass>");
                FareQuoteReq.Append("<rad1:FareBasisCode></rad1:FareBasisCode>");
                FareQuoteReq.Append("<rad1:Cabin>" + searchInput.Cabin + "</rad1:Cabin>");
                FareQuoteReq.Append("<rad1:LFID>-214</rad1:LFID>");
                FareQuoteReq.Append("<rad1:OperatingCarrierCode>G8</rad1:OperatingCarrierCode>");
                FareQuoteReq.Append("<rad1:MarketingCarrierCode>G8</rad1:MarketingCarrierCode>");
                FareQuoteReq.Append("<rad1:NumberOfDaysBefore>0</rad1:NumberOfDaysBefore>");
                FareQuoteReq.Append("<rad1:NumberOfDaysAfter>0</rad1:NumberOfDaysAfter>");
                FareQuoteReq.Append("<rad1:LanguageCode>en</rad1:LanguageCode>");
                FareQuoteReq.Append("<rad1:TicketPackageID>1</rad1:TicketPackageID>");

                #region pax details
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfos>");
                if (searchInput.Adult > 0)
                {
                    FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                    FareQuoteReq.Append("<rad1:PassengerTypeID>1</rad1:PassengerTypeID>");
                    FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Adult + "</rad1:TotalSeatsRequired>");
                    FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
                }
                if (searchInput.Child > 0)
                {
                    FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                    FareQuoteReq.Append("<rad1:PassengerTypeID>6</rad1:PassengerTypeID>");
                    FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Child + "</rad1:TotalSeatsRequired>");
                    FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
                }

                if (searchInput.Infant > 0)
                {
                    FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                    FareQuoteReq.Append("<rad1:PassengerTypeID>5</rad1:PassengerTypeID>");
                    FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Infant + "</rad1:TotalSeatsRequired>");
                    FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
                }

                FareQuoteReq.Append("</rad1:FareQuoteRequestInfos>");



                #endregion
                FareQuoteReq.Append("</rad1:FareQuoteDetail>");
                #endregion

            }

            FareQuoteReq.Append(" </rad1:FareQuoteDetails>");



            #endregion


            FareQuoteReq.Append("</tem:RetrieveFareQuoteRequest></tem:RetrieveFareQuote>");
            FareQuoteReq.Append("</soapenv:Body></soapenv:Envelope>");

            res = G8Utility.PostXml(FareQuoteReq.ToString(), methodUrl, serviceUrl, ref exep);

            //G8Utility.SaveXml(FareQuoteReq.ToString(), SecurityToken, "FareQuote_Req");
            //G8Utility.SaveXml(res, SecurityToken, "FareQuote_Res");
            return res;
        }

        public string GetIXFareQuote(string methodUrl, string serviceUrl, FlightSearch searchInput, bool IsNRTP, ref string exep)
        {
            string res = "";

            string[] depD = searchInput.DepDate.Split('/');
            string[] arrD = searchInput.RetDate.Split('/');
            string departureDate = "";
            string arrivalDate = "";
            if (searchInput.DepDate.Trim() == searchInput.RetDate.Trim() && searchInput.DepDate.Replace('/', '-') == DateTime.Now.ToString("dd/MM/yyyy"))
            {

                //string [] ndate = searchInput.DepDate.Split('/');

                DateTime newDate = new DateTime(Convert.ToInt16(depD[2]), Convert.ToInt16(depD[1]), Convert.ToInt16(depD[0]), DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second).AddHours(2);

                departureDate = depD.Length > 1 ? (newDate.Year.ToString() + "-" + (newDate.Month.ToString().Trim().Length > 1 ? newDate.Month.ToString().Trim() : "0" + newDate.Month.ToString().Trim()) + "-" + (newDate.Day.ToString().Trim().Length > 1 ? newDate.Day.ToString().Trim() : "0" + newDate.Day.ToString().Trim())) : "";
                arrivalDate = depD.Length > 1 ? (newDate.Year.ToString() + "-" + (newDate.Month.ToString().Trim().Length > 1 ? newDate.Month.ToString().Trim() : "0" + newDate.Month.ToString().Trim()) + "-" + (newDate.Day.ToString().Trim().Length > 1 ? newDate.Day.ToString().Trim() : "0" + newDate.Day.ToString().Trim())) : "";
            }
            else if (depD.Length == 1 || arrD.Length == 1)
            {
                depD = searchInput.DepDate.Split('-');
                arrD = searchInput.RetDate.Split('-');
                departureDate = depD.Length > 1 ? (depD[2] + "-" + (depD[1].Trim().Length > 1 ? depD[1].Trim() : "0" + depD[1].Trim()) + "-" + (depD[0].Trim().Length > 1 ? depD[0].Trim() : "0" + depD[0].Trim())) : "";
                arrivalDate = arrD.Length > 1 ? (arrD[2] + "-" + (arrD[1].Trim().Length > 1 ? arrD[1].Trim() : "0" + arrD[1].Trim()) + "-" + (arrD[0].Trim().Length > 1 ? arrD[0].Trim() : "0" + arrD[0].Trim())) : "";
            }
            else
            {
                departureDate = depD.Length > 1 ? (depD[2] + "-" + (depD[1].Trim().Length > 1 ? depD[1].Trim() : "0" + depD[1].Trim()) + "-" + (depD[0].Trim().Length > 1 ? depD[0].Trim() : "0" + depD[0].Trim())) : "";
                arrivalDate = arrD.Length > 1 ? (arrD[2] + "-" + (arrD[1].Trim().Length > 1 ? arrD[1].Trim() : "0" + arrD[1].Trim()) + "-" + (arrD[0].Trim().Length > 1 ? arrD[0].Trim() : "0" + arrD[0].Trim())) : "";
            }




            StringBuilder FareQuoteReq = new StringBuilder();

            FareQuoteReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Request.FareQuote\">");
            FareQuoteReq.Append("<soapenv:Header/><soapenv:Body><tem:RetrieveFareQuote><!--Optional:--><tem:RetrieveFareQuoteRequest>");
            FareQuoteReq.Append(" <rad:SecurityGUID>" + SecurityToken + "</rad:SecurityGUID>");
            FareQuoteReq.Append("<rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode>");
            FareQuoteReq.Append("<rad:AccessibleCarrierCode>IX</rad:AccessibleCarrierCode>");
            FareQuoteReq.Append("</rad:CarrierCode></rad:CarrierCodes>");
            FareQuoteReq.Append("<rad:HistoricUserName>" + UserName + "</rad:HistoricUserName>");
            FareQuoteReq.Append("<rad1:CurrencyOfFareQuote>INR</rad1:CurrencyOfFareQuote>");
            //FareQuoteReq.Append("<rad1:PromotionalCode>TESTPRO3</rad1:PromotionalCode>");
            FareQuoteReq.Append("<rad1:PromotionalCode></rad1:PromotionalCode>");
            FareQuoteReq.Append("<rad1:IataNumberOfRequestor>" + IATANumber + "</rad1:IataNumberOfRequestor>");
            FareQuoteReq.Append("<rad1:CorporationID>-2147483648</rad1:CorporationID>");
            FareQuoteReq.Append("<rad1:FareFilterMethod>NoCombinabilityRoundtripLowestFarePerFareType</rad1:FareFilterMethod>");
            FareQuoteReq.Append("<rad1:FareGroupMethod>WebFareTypes</rad1:FareGroupMethod>");
            FareQuoteReq.Append("<rad1:InventoryFilterMethod>Available</rad1:InventoryFilterMethod>");
            #region FareQuoteDetails
            FareQuoteReq.Append("<rad1:FareQuoteDetails>");
            #region oneWay
            FareQuoteReq.Append("<rad1:FareQuoteDetail>");
            if (IsNRTP)
            {
                FareQuoteReq.Append("<rad1:Origin>" + searchInput.HidTxtArrCity.Split(',')[0] + "</rad1:Origin>");
                FareQuoteReq.Append("<rad1:Destination>" + searchInput.HidTxtDepCity.Split(',')[0] + "</rad1:Destination>");
                FareQuoteReq.Append("<rad1:UseAirportsNotMetroGroups>false</rad1:UseAirportsNotMetroGroups>");
                FareQuoteReq.Append("<rad1:DateOfDeparture>" + arrivalDate + "</rad1:DateOfDeparture>");
            }
            else
            {
                FareQuoteReq.Append("<rad1:Origin>" + searchInput.HidTxtDepCity.Split(',')[0] + "</rad1:Origin>");
                FareQuoteReq.Append("<rad1:Destination>" + searchInput.HidTxtArrCity.Split(',')[0] + "</rad1:Destination>");
                FareQuoteReq.Append("<rad1:UseAirportsNotMetroGroups>false</rad1:UseAirportsNotMetroGroups>");
                FareQuoteReq.Append("<rad1:DateOfDeparture>" + departureDate + "</rad1:DateOfDeparture>");
            }

            FareQuoteReq.Append("<rad1:FareTypeCategory>1</rad1:FareTypeCategory>");
            FareQuoteReq.Append("<rad1:FareClass></rad1:FareClass>");
            FareQuoteReq.Append("<rad1:FareBasisCode></rad1:FareBasisCode>");
            FareQuoteReq.Append("<rad1:Cabin>" + searchInput.Cabin + "</rad1:Cabin>");
            FareQuoteReq.Append("<rad1:LFID>-214</rad1:LFID>");
            FareQuoteReq.Append("<rad1:OperatingCarrierCode>IX</rad1:OperatingCarrierCode>");
            FareQuoteReq.Append("<rad1:MarketingCarrierCode>IX</rad1:MarketingCarrierCode>");
            FareQuoteReq.Append("<rad1:NumberOfDaysBefore>0</rad1:NumberOfDaysBefore>");
            FareQuoteReq.Append("<rad1:NumberOfDaysAfter>0</rad1:NumberOfDaysAfter>");
            FareQuoteReq.Append("<rad1:LanguageCode>en</rad1:LanguageCode>");
            FareQuoteReq.Append("<rad1:TicketPackageID>1</rad1:TicketPackageID>");

            #region pax details
            FareQuoteReq.Append("<rad1:FareQuoteRequestInfos>");
            if (searchInput.Adult > 0)
            {
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                FareQuoteReq.Append("<rad1:PassengerTypeID>1</rad1:PassengerTypeID>");
                FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Adult + "</rad1:TotalSeatsRequired>");
                FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
            }
            if (searchInput.Child > 0)
            {
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                FareQuoteReq.Append("<rad1:PassengerTypeID>6</rad1:PassengerTypeID>");
                FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Child + "</rad1:TotalSeatsRequired>");
                FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
            }

            if (searchInput.Infant > 0)
            {
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                FareQuoteReq.Append("<rad1:PassengerTypeID>5</rad1:PassengerTypeID>");
                FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Infant + "</rad1:TotalSeatsRequired>");
                FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
            }

            FareQuoteReq.Append("</rad1:FareQuoteRequestInfos>");



            #endregion
            FareQuoteReq.Append("</rad1:FareQuoteDetail>");
            #endregion

            if ((searchInput.TripType == TripType.RoundTrip && searchInput.RTF == true) || (searchInput.TripType == TripType.RoundTrip && searchInput.Trip==Trip.I))
            {

                #region RoundTrip
                FareQuoteReq.Append("<rad1:FareQuoteDetail>");
                FareQuoteReq.Append("<rad1:Origin>" + searchInput.HidTxtArrCity.Split(',')[0] + "</rad1:Origin>");
                FareQuoteReq.Append("<rad1:Destination>" + searchInput.HidTxtDepCity.Split(',')[0] + "</rad1:Destination>");
                FareQuoteReq.Append("<rad1:UseAirportsNotMetroGroups>false</rad1:UseAirportsNotMetroGroups>");
                FareQuoteReq.Append("<rad1:DateOfDeparture>" + arrivalDate + "</rad1:DateOfDeparture>");
                FareQuoteReq.Append("<rad1:FareTypeCategory>1</rad1:FareTypeCategory>");
                FareQuoteReq.Append("<rad1:FareClass></rad1:FareClass>");
                FareQuoteReq.Append("<rad1:FareBasisCode></rad1:FareBasisCode>");
                FareQuoteReq.Append("<rad1:Cabin>" + searchInput.Cabin + "</rad1:Cabin>");
                FareQuoteReq.Append("<rad1:LFID>-214</rad1:LFID>");
                FareQuoteReq.Append("<rad1:OperatingCarrierCode>IX</rad1:OperatingCarrierCode>");
                FareQuoteReq.Append("<rad1:MarketingCarrierCode>IX</rad1:MarketingCarrierCode>");
                FareQuoteReq.Append("<rad1:NumberOfDaysBefore>0</rad1:NumberOfDaysBefore>");
                FareQuoteReq.Append("<rad1:NumberOfDaysAfter>0</rad1:NumberOfDaysAfter>");
                FareQuoteReq.Append("<rad1:LanguageCode>en</rad1:LanguageCode>");
                FareQuoteReq.Append("<rad1:TicketPackageID>1</rad1:TicketPackageID>");

                #region pax details
                FareQuoteReq.Append("<rad1:FareQuoteRequestInfos>");
                if (searchInput.Adult > 0)
                {
                    FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                    FareQuoteReq.Append("<rad1:PassengerTypeID>1</rad1:PassengerTypeID>");
                    FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Adult + "</rad1:TotalSeatsRequired>");
                    FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
                }
                if (searchInput.Child > 0)
                {
                    FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                    FareQuoteReq.Append("<rad1:PassengerTypeID>6</rad1:PassengerTypeID>");
                    FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Child + "</rad1:TotalSeatsRequired>");
                    FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
                }

                if (searchInput.Infant > 0)
                {
                    FareQuoteReq.Append("<rad1:FareQuoteRequestInfo>");
                    FareQuoteReq.Append("<rad1:PassengerTypeID>5</rad1:PassengerTypeID>");
                    FareQuoteReq.Append("<rad1:TotalSeatsRequired>" + searchInput.Infant + "</rad1:TotalSeatsRequired>");
                    FareQuoteReq.Append("</rad1:FareQuoteRequestInfo>");
                }

                FareQuoteReq.Append("</rad1:FareQuoteRequestInfos>");



                #endregion
                FareQuoteReq.Append("</rad1:FareQuoteDetail>");
                #endregion

            }

            FareQuoteReq.Append(" </rad1:FareQuoteDetails>");



            #endregion


            FareQuoteReq.Append("</tem:RetrieveFareQuoteRequest></tem:RetrieveFareQuote>");
            FareQuoteReq.Append("</soapenv:Body></soapenv:Envelope>");

            res = G8Utility.PostXml(FareQuoteReq.ToString(), methodUrl, serviceUrl, ref exep);

            G8Utility.SaveXml(FareQuoteReq.ToString(), SecurityToken, "FareQuote_Req");
            G8Utility.SaveXml(res, SecurityToken, "FareQuote_Res");
            return res;
        }

        public List<G8ServiceQuoteResponse> GetIXServiceQuote(string methodUrl, string serviceUrl, List<G8ServiceQuote> srvQuoteList, int TotPax, ref string exep)
        {
            string response = "";
            StringBuilder ServiceQuoteReq = new StringBuilder();

            ServiceQuoteReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Request.Service\">");
            ServiceQuoteReq.Append("<soapenv:Header/><soapenv:Body><tem:RetrieveServiceQuotes><!--Optional:--><tem:RetrieveServiceQuotesRequest>");
            ServiceQuoteReq.Append("<rad:SecurityGUID>" + SecurityToken + "</rad:SecurityGUID>");
            ServiceQuoteReq.Append("<rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode>");
            ServiceQuoteReq.Append("<rad:AccessibleCarrierCode>IX</rad:AccessibleCarrierCode>");
            ServiceQuoteReq.Append("</rad:CarrierCode></rad:CarrierCodes>");
            ServiceQuoteReq.Append("<rad1:RetrieveServiceQuotes>");

            for (int i = 0; i < srvQuoteList.Count; i++)
            {
                #region ServiceQoute

                ServiceQuoteReq.Append("<rad1:ServiceQuote>");
                ServiceQuoteReq.Append("<rad1:LogicalFlightID>" + srvQuoteList[i].LogicalFlightID + "</rad1:LogicalFlightID>");
                ServiceQuoteReq.Append("<rad1:DepartureDate>" + srvQuoteList[i].DepartureDate + "</rad1:DepartureDate>");
                ServiceQuoteReq.Append("<rad1:AirportCode>" + srvQuoteList[i].AirportCode + "</rad1:AirportCode>");
                ServiceQuoteReq.Append("<rad1:ServiceCode>" + srvQuoteList[i].ServiceCode + "</rad1:ServiceCode>");
                ServiceQuoteReq.Append("<rad1:Cabin>" + srvQuoteList[i].Cabin + "</rad1:Cabin>");
                ServiceQuoteReq.Append("<rad1:Category>" + srvQuoteList[i].Category + "</rad1:Category>");
                ServiceQuoteReq.Append("<rad1:Currency>INR</rad1:Currency>");
                ServiceQuoteReq.Append("<rad1:UTCOffset>0</rad1:UTCOffset>");
                ServiceQuoteReq.Append("<rad1:OperatingCarrierCode>IX</rad1:OperatingCarrierCode>");
                ServiceQuoteReq.Append("<rad1:MarketingCarrierCode>IX</rad1:MarketingCarrierCode>");
                ServiceQuoteReq.Append("<rad1:FareClass>" + srvQuoteList[i].FareClass + "</rad1:FareClass>");
                ServiceQuoteReq.Append("<rad1:FareBasisCode>" + srvQuoteList[i].FareBasisCode + "</rad1:FareBasisCode>");
                ServiceQuoteReq.Append("<rad1:ReservationChannel>TPAPI</rad1:ReservationChannel>");
                ServiceQuoteReq.Append("<rad1:DestinationAirportCode>" + srvQuoteList[i].DestinationAirportCode + "</rad1:DestinationAirportCode>");
                ServiceQuoteReq.Append("</rad1:ServiceQuote>");

                #endregion
            }
            ServiceQuoteReq.Append("</rad1:RetrieveServiceQuotes>");
            ServiceQuoteReq.Append("</tem:RetrieveServiceQuotesRequest></tem:RetrieveServiceQuotes>");
            ServiceQuoteReq.Append("</soapenv:Body></soapenv:Envelope>");

            response = G8Utility.PostXml(ServiceQuoteReq.ToString(), methodUrl, serviceUrl, ref exep);

            G8Utility.SaveXml(ServiceQuoteReq.ToString(), SecurityToken, "ServiceQuoteReq_Req");
            G8Utility.SaveXml(response, SecurityToken, "ServiceQuoteReq_Res");

            return ParseServiceQoute(response, TotPax);
        }

        public List<G8ServiceQuoteResponse> GetServiceQuote(string methodUrl, string serviceUrl, List<G8ServiceQuote> srvQuoteList, int TotPax, ref string exep)
        {
            string response = "";
            StringBuilder ServiceQuoteReq = new StringBuilder();

            ServiceQuoteReq.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:rad=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request\" xmlns:rad1=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Request.Service\">");
            ServiceQuoteReq.Append("<soapenv:Header/><soapenv:Body><tem:RetrieveServiceQuotes><!--Optional:--><tem:RetrieveServiceQuotesRequest>");
            ServiceQuoteReq.Append("<rad:SecurityGUID>" + SecurityToken + "</rad:SecurityGUID>");
            ServiceQuoteReq.Append("<rad:CarrierCodes><!--Zero or more repetitions:--><rad:CarrierCode>");
            ServiceQuoteReq.Append("<rad:AccessibleCarrierCode>G8</rad:AccessibleCarrierCode>");
            ServiceQuoteReq.Append("</rad:CarrierCode></rad:CarrierCodes>");
            ServiceQuoteReq.Append("<rad1:RetrieveServiceQuotes>");

            for (int i = 0; i < srvQuoteList.Count; i++)
            {
                #region ServiceQoute

                ServiceQuoteReq.Append("<rad1:ServiceQuote>");
                ServiceQuoteReq.Append("<rad1:LogicalFlightID>" + srvQuoteList[i].LogicalFlightID + "</rad1:LogicalFlightID>");
                ServiceQuoteReq.Append("<rad1:DepartureDate>" + srvQuoteList[i].DepartureDate + "</rad1:DepartureDate>");
                ServiceQuoteReq.Append("<rad1:AirportCode>" + srvQuoteList[i].AirportCode + "</rad1:AirportCode>");
                ServiceQuoteReq.Append("<rad1:ServiceCode>" + srvQuoteList[i].ServiceCode + "</rad1:ServiceCode>");
                ServiceQuoteReq.Append("<rad1:Cabin>" + srvQuoteList[i].Cabin + "</rad1:Cabin>");
                ServiceQuoteReq.Append("<rad1:Category>" + srvQuoteList[i].Category + "</rad1:Category>");
                ServiceQuoteReq.Append("<rad1:Currency>INR</rad1:Currency>");
                ServiceQuoteReq.Append("<rad1:UTCOffset>0</rad1:UTCOffset>");
                ServiceQuoteReq.Append("<rad1:OperatingCarrierCode>G8</rad1:OperatingCarrierCode>");
                ServiceQuoteReq.Append("<rad1:MarketingCarrierCode>G8</rad1:MarketingCarrierCode>");
                ServiceQuoteReq.Append("<rad1:FareClass>" + srvQuoteList[i].FareClass + "</rad1:FareClass>");
                ServiceQuoteReq.Append("<rad1:FareBasisCode>" + srvQuoteList[i].FareBasisCode + "</rad1:FareBasisCode>");
                ServiceQuoteReq.Append("<rad1:ReservationChannel>TPAPI</rad1:ReservationChannel>");
                ServiceQuoteReq.Append("<rad1:DestinationAirportCode>" + srvQuoteList[i].DestinationAirportCode + "</rad1:DestinationAirportCode>");
                ServiceQuoteReq.Append("</rad1:ServiceQuote>");

                #endregion
            }
            ServiceQuoteReq.Append("</rad1:RetrieveServiceQuotes>");
            ServiceQuoteReq.Append("</tem:RetrieveServiceQuotesRequest></tem:RetrieveServiceQuotes>");
            ServiceQuoteReq.Append("</soapenv:Body></soapenv:Envelope>");

            response = G8Utility.PostXml(ServiceQuoteReq.ToString(), methodUrl, serviceUrl, ref exep);

            //G8Utility.SaveXml(ServiceQuoteReq.ToString(), SecurityToken, "ServiceQuoteReq_Req");
            //G8Utility.SaveXml(response, SecurityToken, "ServiceQuoteReq_Res");

            return ParseServiceQoute(response, TotPax);
        }

        public List<G8ServiceQuoteResponse> ParseServiceQoute(string respXml, int TotPax)
        {
            List<G8ServiceQuoteResponse> srvQList = new List<G8ServiceQuoteResponse>();

            // XDocument xmlDocq = XDocument.Load(@"E:\bhupinder 1.11.2014\d drive\Bhupender\FlyDubai\TestWeb\TestWeb\SrvQuoteResp.xml");

            string str = respXml.ToString().Replace("xmlns:a=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Response\"", "").Replace("a:", "").Replace("xmlns=\"http://tempuri.org/\"", "");

            XDocument xmlDoc = XDocument.Parse(str);
            IEnumerable<XElement> SrvBag = null;

            SrvBag = from el in xmlDoc.Descendants("ServiceQuotes").Descendants("ViewServiceQuote")
                     where
                    (Convert.ToInt32(el.Element("QuantityAvailable").Value) >= TotPax &&
                     Convert.ToDecimal(el.Element("Amount").Value) > 0) || (Convert.ToInt32(el.Element("CategoryID").Value) == 8 &&
                     Convert.ToDecimal(el.Element("Amount").Value) > 0)
                     select el;


            for (int i = 0; i < SrvBag.Count(); i++)
            {
                decimal amount = 0;
                IEnumerable<XElement> SrvBagTax = null;
                XElement objSrvBag = SrvBag.ElementAt(i);
                G8ServiceQuoteResponse objSresp = new G8ServiceQuoteResponse();
                objSresp.CodeType = objSrvBag.Element("CodeType").Value.Trim();
                objSresp.DepartureDate = objSrvBag.Element("DepartureDate").Value.Trim();

                amount = Convert.ToDecimal(objSrvBag.Element("Amount").Value.Trim());
                SrvBagTax = from el in objSrvBag.Descendants("ApplicableTaxes").Descendants("ApplicableTax")
                            select el;

                for (int bt = 0; bt < SrvBagTax.Count(); bt++)
                {
                    XElement objSrvTax = SrvBagTax.ElementAt(bt);
                    amount = amount + Convert.ToDecimal(objSrvTax.Element("TaxAmount").Value.Trim());
                }

                objSresp.AmountWithTax = amount.ToString().Trim();
                objSresp.Amount = objSrvBag.Element("Amount").Value.Trim();
                objSresp.SSRCategory = objSrvBag.Element("CategoryID").Value.Trim();
                objSresp.Description = objSrvBag.Element("Description").Value.Trim();
                objSresp.ServiceID = objSrvBag.Element("ServiceID").Value.Trim();
                objSresp.LogicalFlightID = objSrvBag.Element("LogicalFlightID").Value.Trim();

                if (Convert.ToInt16(objSresp.SSRCategory) == 6)
                {
                    objSresp.ServiceType = ServiceType.Meal;
                }
                else if (Convert.ToInt16(objSresp.SSRCategory) == 81 || objSresp.CodeType.Trim().StartsWith("X"))
                {
                    objSresp.ServiceType = ServiceType.Baggage;
                }
                else if (objSresp.CodeType.Trim().StartsWith("NS"))
                {
                    objSresp.ServiceType = ServiceType.Seat;
                } 


                srvQList.Add(objSresp);

            }


            return srvQList; //GetBagList(srvQList,searchvalue,sno);


        }


        public List<G8ServiceQuoteResponse> GetBagList(List<G8ServiceQuoteResponse> ServiceQouteList, string searchvalue, string sno, string LFID)
        {
            List<G8ServiceQuoteResponse> BagList = new List<G8ServiceQuoteResponse>();

            if (!sno.ToLower().Contains(":basic"))
            {
                string bagQuery = searchvalue.Contains("BAG") ? "BUP" : "BAG";
                if (bagQuery == "BAG")
                {
                    BagList = ServiceQouteList.Where(x => x.CodeType.Contains("BAG") && x.LogicalFlightID.Trim() == LFID.Trim()).ToList();
                }
                else
                {
                    if (searchvalue.ToUpper().Contains(":BAGB:"))
                    {
                        BagList = ServiceQouteList.Where(x => x.CodeType.Contains("BUP") && x.CodeType.Trim().ToUpper() != "BUPZ" && x.LogicalFlightID.Trim() == LFID.Trim()).ToList();
                    }
                    else if (searchvalue.ToUpper().Contains(":BAGL:"))
                    {
                        BagList = ServiceQouteList.Where(x => x.CodeType.Trim().ToUpper() == "BUPZ" && x.LogicalFlightID.Trim() == LFID.Trim()).ToList();
                    }

                }
            }


            return BagList;

        }

        public List<G8ServiceQuoteResponse> GetIXBagList(List<G8ServiceQuoteResponse> ServiceQouteList, string searchvalue, string sno, string LFID,string Stype)
        {
            List<G8ServiceQuoteResponse> BagList = new List<G8ServiceQuoteResponse>();

            if (!sno.ToLower().Contains(":basic"))
            {
                BagList = ServiceQouteList.Where(x => Convert.ToString(x.ServiceType).Contains(Stype) && x.LogicalFlightID.Trim() == LFID.Trim()).ToList();


                //string bagQuery = searchvalue.Contains("BAG") ? "BUP" : "BAG";
                //if (bagQuery == "BAG")
                //{
                //    BagList = ServiceQouteList.Where(x => Convert.ToString(x.ServiceType).Contains("Baggage") && x.LogicalFlightID.Trim() == LFID.Trim()).ToList();
                //}
                //else
                //{
                //    if (searchvalue.ToUpper().Contains(":BAGB:"))
                //    {
                //        BagList = ServiceQouteList.Where(x => x.CodeType.Contains("BUP") && x.CodeType.Trim().ToUpper() != "BUPZ" && x.LogicalFlightID.Trim() == LFID.Trim()).ToList();
                //    }
                //    else if (searchvalue.ToUpper().Contains(":BAGL:"))
                //    {
                //        BagList = ServiceQouteList.Where(x => x.CodeType.Trim().ToUpper() == "BUPZ" && x.LogicalFlightID.Trim() == LFID.Trim()).ToList();
                //    }

                //}
            }


            return BagList;

        }

        public List<G8ServiceQuoteResponse> GetSSRListByLFID(List<G8ServiceQuoteResponse> ServiceQouteList, string LFID)
        {
            List<G8ServiceQuoteResponse> resultList = new List<G8ServiceQuoteResponse>();

            resultList = ServiceQouteList.Where(x => x.LogicalFlightID.Trim() == LFID.Trim()).ToList();
            return resultList;

        }
        
        public List<G8ServiceQuoteResponse> GetSSRListServiceTypeWise(List<G8ServiceQuoteResponse> ServiceQouteList, ServiceType serviceType)
        {
            List<G8ServiceQuoteResponse> resultList = new List<G8ServiceQuoteResponse>();
            resultList = ServiceQouteList.Where(x => x.ServiceType == serviceType).ToList();
            return resultList;
        }






    }
}
