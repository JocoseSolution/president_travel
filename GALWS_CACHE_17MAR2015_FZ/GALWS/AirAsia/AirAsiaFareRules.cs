﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STD.Shared;
using STD.DAL;
using ITZERRORLOG;
using System.Xml.Linq;
using System.Collections;
using System.Configuration;
using System.IO;
namespace GALWS.AirAsia
{
    public class AirAsiaFareRules
    {
        public string GetFareRule( string constr, string sno, string FareBasisCode, string ClassOfService, string sessionids)
        {
            string FareRulestr = "";
            try
            {
                string[] alist = sno.Split(':');
                if (alist.Length > 2)
                {
                    Credentials objCrd = new Credentials(constr);
                    List<CredentialList> CrdList = objCrd.GetServiceCredentials("");
                    CrdList = CrdList.Where(x => x.Provider == "AK").ToList();

                    string FareRulesRequest = SetFareRulesRequest(sessionids, FareBasisCode, ClassOfService, alist[1].Trim(), alist[0].Trim());
                    AirAsiaUtility.SaveFile(FareRulesRequest, "FareRuleReq_" + FareBasisCode);

                    string FareRulesResponse = AirAsiaUtility.AirAsiaPostXML(CrdList[0].LoginPWD, "http://tempuri.org/IFareService/GetFareRule", FareRulesRequest);
                    AirAsiaUtility.SaveFile(FareRulesResponse, "FareRuleRes_" + FareBasisCode);

                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; //XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities";

                    XDocument xmlDoc = XDocument.Parse(FareRulesResponse.Replace("xmlns=\"http://tempuri.org/\"", string.Empty));
                    XElement GetFareRuleResult = xmlDoc.Element(s + "Envelope").Element(s + "Body").Element("GetFareRuleResponse").Element("GetFareRuleResult");
                    if (GetFareRuleResult.Element("ExceptionMessage") == null)
                    {
                        if (GetFareRuleResult.Element("Data") != null)
                        {
                            FareRulestr = "<div id='Finalfarerule'>" + GetFareRuleResult.Element("Data").Value + "</div>";
                        }
                    }
                    else 
                    {
                        if (GetFareRuleResult.Element("ExceptionMessage").Value.Trim().Contains("Invalid Login.") || GetFareRuleResult.Element("ExceptionMessage").Value.Trim().Contains("Session token authentication failure. : No such session:"))
                        {
                            AirAsiaLogon objlogindtl = new AirAsiaLogon();
                            sessionids = objlogindtl.GetSessionID(CrdList[0].UserID, CrdList[0].Password, CrdList[0].LoginID);
                            string FareRulesRequest2 = SetFareRulesRequest(sessionids, FareBasisCode, ClassOfService, alist[1].Trim(), alist[0].Trim());
                            AirAsiaUtility.SaveFile(FareRulesRequest2, "FareRuleReq2_" + FareBasisCode);
                             string FareRulesResponse2 = AirAsiaUtility.AirAsiaPostXML(CrdList[0].LoginPWD, "http://tempuri.org/IFareService/GetFareRule", FareRulesRequest2);
                            AirAsiaUtility.SaveFile(FareRulesResponse, "FareRuleRes2_" + FareBasisCode);
                            XDocument xmlDoc2 = XDocument.Parse(FareRulesResponse2.Replace("xmlns=\"http://tempuri.org/\"", string.Empty));
                            XElement GetFareRuleResult2 = xmlDoc2.Element(s + "Envelope").Element(s + "Body").Element("GetFareRuleResponse").Element("GetFareRuleResult");
                            if (GetFareRuleResult2.Element("ExceptionMessage") == null)
                            {
                                if (GetFareRuleResult2.Element("Data") != null)
                                {
                                    FareRulestr =  GetFareRuleResult2.Element("Data").Value ;
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            { ExecptionLogger.FileHandling("GetFareRule", "Error_001", ex, "AirAsiaFlight");}
            return FareRulestr;
        }

        public string SetFareRulesRequest(string Sessionids, string FareBasisCode, string ClassOfService , string CarrierCode,string RuleNumber)
        {
            StringBuilder objreqXml = new StringBuilder();
            objreqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\">");
            objreqXml.Append("<soapenv:Header/><soapenv:Body><tem:GetFareRule>");
            objreqXml.Append("<tem:strSessionID>" + Sessionids + "</tem:strSessionID>");
            objreqXml.Append("<tem:objFareRuleRequestData><ace:FareBasisCode>" + FareBasisCode + "</ace:FareBasisCode><ace:ClassOfService>" + ClassOfService + "</ace:ClassOfService>");
            objreqXml.Append("<ace:CarrierCode>" + CarrierCode + "</ace:CarrierCode><ace:RuleNumber>" + RuleNumber + "</ace:RuleNumber>");
            objreqXml.Append("<ace:RuleTariff/><ace:CultureCode>en-GB</ace:CultureCode></tem:objFareRuleRequestData>");
            objreqXml.Append("</tem:GetFareRule></soapenv:Body></soapenv:Envelope>");

            return objreqXml.ToString();
        }
        
    }
}
