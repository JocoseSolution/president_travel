﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml;

namespace GALWS.AirAsia
{
   public class AirAsiaLogon
    {
       public string GetSessionID(string loginID, string loginPass, string URL)
       {
           string token = "";
           try
           {
               StringBuilder ReqXml=new StringBuilder(); 

                ReqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07/ACE.Entities\">");
                ReqXml.Append(" <soapenv:Header/> <soapenv:Body> <tem:Logon><tem:logonRequest>");
                ReqXml.Append("<ace:Username>" + loginID + "</ace:Username> <ace:Password>" + loginPass + "</ace:Password> </tem:logonRequest></tem:Logon> </soapenv:Body> </soapenv:Envelope>");

                string resXml = AirAsiaUtility.AirAsiaPostXML(URL, "http://tempuri.org/ISessionService/Logon", ReqXml.ToString());

                if (!resXml.Contains("<Errors>"))
                {
                    XNamespace s = "http://schemas.xmlsoap.org/soap/envelope/"; // XNamespace a = "http://schemas.datacontract.org/2004/07/ACE.Entities"; XNamespace i = "http://www.w3.org/2001/XMLSchema-instance";

                    XDocument xmlDoc = XDocument.Parse(resXml.Replace("xmlns=\"http://tempuri.org/\"", string.Empty));
                    XElement LogonResult = xmlDoc.Element(s + "Envelope").Element(s + "Body").Element("LogonResponse").Element("LogonResult");
                    //if (LogonResult.Element(a + "SessionID").Value == "")
                        token = LogonResult.Element("SessionID").Value;
                }
           }
           catch(Exception ex)
           { }
           return token;
       }
       public string LogoutSession(string loginID, string loginPass, string URL)
       {
           string token = "";
           try
           {
               StringBuilder ReqXml = new StringBuilder();

               ReqXml.Append("<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\" xmlns:ace=\"http://schemas.datacontract.org/2004/07ACE.Entities\">");
               ReqXml.Append(" <soapenv:Header/> <soapenv:Body> <tem:Logon><tem:logonRequest>");
               ReqXml.Append("<ace:Username>" + loginID + "</ace:Username> <ace:Password>" + loginPass + "</ace:Password> </tem:logonRequest></tem:Logon> </soapenv:Body> </soapenv:Envelope>");

               string resXml = AirAsiaUtility.AirAsiaPostXML(URL, "http://tempuri.org/ISessionService/Logon", ReqXml.ToString());



           }
           catch (Exception ex)
           { }
           return token;
       }
    }
}
