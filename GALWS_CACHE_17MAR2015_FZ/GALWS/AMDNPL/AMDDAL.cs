﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using STD.Shared;
using System.Data.Common;
using System.Collections;

namespace STD.DAL
{
    public class AmdDAL
    {
        //SqlConnection con;
        //SqlCommand cmd;
        //SqlDataAdapter adap;
        private SqlDatabase DBHelper;
        public AmdDAL(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }
        public int InsertGDSBookingLogs(string ORDERID, string PNR, string SELL_REQ, string SELL_RES, string ADDMULTI_REQ, string ADDMULTI_RES, string PRICE_REQ, string PRICE_RES, string TST_REQ, string TST_RES, string PNR_REQ,
                     string PNR_RES, string OTHER)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_INSERTAMDGDSBOOKING";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@ORDERID", DbType.String, ORDERID);
            DBHelper.AddInParameter(cmd, "@PNR", DbType.String, PNR);
            DBHelper.AddInParameter(cmd, "@SELL_REQ", DbType.String, SELL_REQ);
            DBHelper.AddInParameter(cmd, "@SELL_RES", DbType.String, SELL_RES);
            DBHelper.AddInParameter(cmd, "@ADDMULTI_REQ", DbType.String, ADDMULTI_REQ);
            DBHelper.AddInParameter(cmd, "@ADDMULTI_RES", DbType.String, ADDMULTI_RES);
            DBHelper.AddInParameter(cmd, "@PRICE_REQ", DbType.String, PRICE_REQ);
            DBHelper.AddInParameter(cmd, "@PRICE_RES", DbType.String, PRICE_RES);
            DBHelper.AddInParameter(cmd, "@TST_REQ", DbType.String, TST_REQ);
            DBHelper.AddInParameter(cmd, "@TST_RES", DbType.String, TST_RES);
            DBHelper.AddInParameter(cmd, "@PNR_REQ", DbType.String, PNR_REQ);
            DBHelper.AddInParameter(cmd, "@PNR_RES", DbType.String, PNR_RES);
            DBHelper.AddInParameter(cmd, "@OTHER", DbType.String, OTHER);
            int i = DBHelper.ExecuteNonQuery(cmd);
            return i;
        }
        public List<SoapActionUrl> GetSoapActionUrl(string PROVIDER)
        {

            var SoapList = new List<SoapActionUrl>();
            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "SP_GETSOAPACTIONURL";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@PROVIDER", DbType.String, PROVIDER);
                DataTable dt = new DataTable();
                dt.Load(DBHelper.ExecuteReader(Dbcmd));
                DataRow[] dr;
                dr = dt.Select("PROVIDER='1A'");
                // dr = dt.Select("SOAPACTIONMETHOD IN ('ADDPAX','AIRLINEPNR','GDSPNR','IGNORE','PRICING','SELL','SIGNIN','SIGNOUT','TST')");//"SOAPACTIONMETHOD IN ('ADDPAX','AIRLINEPNR','GDSPNR','IGNORE','PRICING','SELL','SIGNIN','SIGNOUT','TST')"
                SoapList.Add(new SoapActionUrl
                {
                    ADDPAX = dr[0]["SOAPACTIONURL"].ToString(),
                    AIRLINEPNR = dr[1]["SOAPACTIONURL"].ToString(),
                    CRYPTIC = dr[2]["SOAPACTIONURL"].ToString(),
                    DOCISSUANCE = dr[3]["SOAPACTIONURL"].ToString(),
                    GDSPNR = dr[4]["SOAPACTIONURL"].ToString(),
                    IGNORE = dr[5]["SOAPACTIONURL"].ToString(),
                    PRICING = dr[6]["SOAPACTIONURL"].ToString(),
                    SELL = dr[7]["SOAPACTIONURL"].ToString(),
                    SIGNIN = dr[8]["SOAPACTIONURL"].ToString(),
                    SIGNOUT = dr[9]["SOAPACTIONURL"].ToString(),
                    TST = dr[10]["SOAPACTIONURL"].ToString()

                });
            }

            catch (Exception ex)
            {

            }
            return SoapList;

        }
        public DataTable GetTicketingCrd(string VC)
        {
            DataTable dt = new DataTable();
            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "GetTicketingCrd";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@VC", DbType.String, VC);
                dt.Load(DBHelper.ExecuteReader(Dbcmd));

            }

            catch (Exception ex)
            {

            }
            return dt;
        }
        public int InsertGDSTicketingLogs(string ORDERID, string SIGNIN_REQ, string SIGNIN_RES, string RETRIVEPNR_REQ, string RETRIVEPNR_RES, string CRIPTIC_REQ, string CRIPTIC_RES, string SIGNOUT_REQ, string SIGNOUT_RES, string OTHER, string RETRIVEPNRTKT_REQ, string RETRIVEPNRTKT_RES, string REPRICE_REQ, string REPRICE_RES, string RETST_REQ, string RETST_RES, string REPNRRETRIVE_REQ, string REPNRRETRIVE_RES)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_INSERTGDSTICKETINGLOGS";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@ORDERID", DbType.String, ORDERID);
            DBHelper.AddInParameter(cmd, "@SIGNIN_REQ", DbType.String, SIGNIN_REQ);
            DBHelper.AddInParameter(cmd, "@SIGNIN_RES", DbType.String, SIGNIN_RES);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNR_REQ", DbType.String, RETRIVEPNR_REQ);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNR_RES", DbType.String, RETRIVEPNR_RES);
            DBHelper.AddInParameter(cmd, "@CRIPTIC_REQ", DbType.String, CRIPTIC_REQ);
            DBHelper.AddInParameter(cmd, "@CRIPTIC_RES", DbType.String, CRIPTIC_RES);
            DBHelper.AddInParameter(cmd, "@SIGNOUT_REQ", DbType.String, SIGNOUT_REQ);
            DBHelper.AddInParameter(cmd, "@SIGNOUT_RES", DbType.String, SIGNOUT_RES);
            DBHelper.AddInParameter(cmd, "@OTHER", DbType.String, OTHER);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNRTKT_REQ", DbType.String, RETRIVEPNRTKT_REQ);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNRTKT_RES", DbType.String, RETRIVEPNRTKT_RES);
            DBHelper.AddInParameter(cmd, "@REPRICE_REQ", DbType.String, REPRICE_REQ);
            DBHelper.AddInParameter(cmd, "@REPRICE_RES", DbType.String, REPRICE_RES);
            DBHelper.AddInParameter(cmd, "@RETST_REQ", DbType.String, RETST_REQ);
            DBHelper.AddInParameter(cmd, "@RETST_RES", DbType.String, RETST_RES);
            DBHelper.AddInParameter(cmd, "@REPNRRETRIVE_REQ", DbType.String, REPNRRETRIVE_REQ);
            DBHelper.AddInParameter(cmd, "@REPNRRETRIVE_RES", DbType.String, REPNRRETRIVE_RES);
            int i = DBHelper.ExecuteNonQuery(cmd);
            return i;
        }
        public DataSet GetGDSXmlLogsAndFlight(string OrderId)
        {
            DataSet ds = new DataSet();

            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "SP_GETGDSXMLLOGSANDFLIGHT";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@ORDERID", DbType.String, OrderId);
                ds = DBHelper.ExecuteDataSet(Dbcmd);

            }

            catch (Exception ex)
            {

            }
            return ds;
        }
    }
}
