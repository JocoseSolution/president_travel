﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml.Linq;
using System.Xml;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;
using GALWS.AMDNPL;

namespace STD.BAL
{
    public class PNRCreation
    {
        AmdBAL BAL_IA2 = new AmdBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        string Constr = "", Trip = "", VC = "";
        public PNRCreation()
        {
        }

       
        public PNRCreation(string constr, string trip, string vc)
        {
            Constr = constr;
            Trip = trip;
            VC = vc;
         //   BAL_IA = new GALWS.AMDNPL.AmdBusiness(Constr);
        }
        Hashtable HSREQRES = new Hashtable();
        Hashtable HSPNR = new Hashtable();
        List<Shared.SoapActionUrl> ListSAU = new List<STD.Shared.SoapActionUrl>();

        string GDSPNR = ""; string AirlinePNR = ""; string sessionid20 = ""; string SessionIdTST = ""; string SessionIdAME = "";
        #region PNRCreation
        AmdBAL BAL_IA = new AmdBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        public Hashtable PNRCreate(DataSet FltDsGAL, DataSet FltHdr, DataSet PaxDS, string VC, DataSet Crd, ref ArrayList TktNoArray, string Constr, ref string bookingID)
        {
            //variable Declaration
            //  System.DateTime.Now.ToString("ddMMyyyyHHmmss");

            ListSAU = BAL_IA.GetSoapActionUrl("1A");
            //string h = ListSAU[0].ADDPAX;
            string StatusXml = ""; string AddMultiElementsXML = ""; string PriceRes = ""; string TSTFromPricingXML = ""; string AddMEleXML = "";
            try
            {
                AmdSession objSsnHdr = new AmdSession();
                int Adult = Convert.ToInt32(FltHdr.Tables[0].Rows[0]["Adult"].ToString());
                int Child = Convert.ToInt32(FltHdr.Tables[0].Rows[0]["Child"].ToString());
                int Infant = Convert.ToInt32(FltHdr.Tables[0].Rows[0]["Infant"].ToString());
                HSREQRES.Add("ORDERID", FltHdr.Tables[0].Rows[0]["OrderId"].ToString());
                //Begin Step1 authenticate and generate sessionid
                //string SessionSignIn = SignIn(ListSAU[0].SIGNIN); //SignIn("http://webservices.amadeus.com/1ASIWMASSPT/VLSSLQ_06_1_1A");
                //End Step1 authenticate and generate sessionid
                //Begin Step2 Air_SellFromRecommendation
                //if (SessionSignIn != "")
                //{
                string SessionId = "";//GetIncrementedSessionId(SessionSignIn);
                StatusXml = Air_SellFromRecommendation(SessionId, ListSAU[0].SELL, Adult, Child, Infant, FltDsGAL, FltHdr, PaxDS, "WSP4APRE", "KTMVS3270", "AMADEUS100", ServiceUrls.SvcURL, IncludeSessionType.Start); //Air_SellFromRecommendation(SessionId, "http://webservices.amadeus.com/1ASIWMASSPT/ITAREQ_05_2_IA", Adult, Child, Infant, FltDsGAL, FltHdr, PaxDS);
                                                                                                                                                                                                                        //string StatusCode = sell_recom_chk(StatusXml);
                                                                                                                                                                                                                        //Begin Step2 Air_SellFromRecommendation
                if (HSREQRES["SELL_STATUS"].ToString() == "TRUE")
                {
                    //Begin Step3 PNR_AddMultiElements
                    // SessionIdAME = GetIncrementedSessionId(SessionId);
                    SessionIdAME = objSsnHdr.GetSessionInseries(StatusXml);
                    SessionId = SessionIdAME;
                    AddMultiElementsXML = AddMultiElements(SessionIdAME, ListSAU[0].ADDPAX, Adult, Child, Infant, FltDsGAL, FltHdr, PaxDS, "WSP4APRE", "KTMVS3270", "AMADEUS100", ServiceUrls.SvcURL, IncludeSessionType.InSeries);//AddMultiElements(SessionIdAME, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A", Adult, Child, Infant, FltDsGAL, FltHdr, PaxDS);
                                                                                                                                                                                                                                   //End Step3 PNR_AddMultiElements
                    if (HSREQRES["ADDMULTI_STATUS"].ToString() == "TRUE")
                    {
                        //Begin Step4 Fare_PricePNRWithBookingClass
                        //string SessionIdPrice = GetIncrementedSessionId(SessionIdAME);
                        string SessionIdPrice = objSsnHdr.GetSessionInseries(AddMultiElementsXML);
                        SessionId = SessionIdPrice;
                        PriceRes = Fare_PricePNRWithBookingClass(SessionIdPrice, ListSAU[0].PRICING, FltHdr.Tables[0].Rows[0]["VC"].ToString(), FltDsGAL, AddMultiElementsXML, "WSP4APRE", "KTMVS3270", "AMADEUS100", ServiceUrls.SvcURL, IncludeSessionType.InSeries);//Fare_PricePNRWithBookingClass(SessionIdPrice, "http://webservices.amadeus.com/1ASIWMASSPT/TPCBRQ_07_3_1A", FltHdr.Tables[0].Rows[0]["VC"].ToString(), FltDsGAL, AddMultiElementsXML);
                        XDocument PXDoc = XDocument.Parse(PriceRes);
                        XNamespace Pns = "http://xml.amadeus.com/TPCBRR_07_3_1A";
                        //End Step4 Fare_PricePNRWithBookingClass 
                        if (HSREQRES["PRICE_STATUS"].ToString() == "TRUE")
                        {

                            var OTF = PXDoc.Descendants(Pns + "fareList").Descendants(Pns + "fareDataInformation").Descendants(Pns + "fareDataSupInformation")
                                  .Where(m => m.Elements(Pns + "fareDataQualifier").First().Value == "712")
                                  .Select(m => m.Elements(Pns + "fareAmount").First().Value);
                            double OriginalTF = 0;
                            foreach (var fare in OTF)
                            {
                                OriginalTF = OriginalTF + Convert.ToDouble(fare.ToString());

                            }
                            //changes by 
                            //SMS charge calc
                            double SMSChg = 0;
                            try
                            {
                                //SMSChg = double.Parse(FltDsGAL.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDsGAL.Tables[0].Rows[0]["Adult"].ToString().Trim());
                                //SMSChg = SMSChg + (double.Parse(FltDsGAL.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDsGAL.Tables[0].Rows[0]["Child"].ToString().Trim()));
                                SMSChg = double.Parse(FltDsGAL.Tables[0].Rows[0]["OriginalTT"].ToString().Trim());
                            }
                            catch
                            { }

                            OriginalTF = OriginalTF + SMSChg;
                            if (Child > 0)
                            { OriginalTF = OriginalTF + SMSChg; }

                            //SMS charge calc end

                            double TotFare = double.Parse(FltDsGAL.Tables[0].Rows[0]["AdtFare"].ToString().Trim());
                            TotFare += double.Parse(FltDsGAL.Tables[0].Rows[0]["ChdFare"].ToString().Trim());
                            TotFare += double.Parse(FltDsGAL.Tables[0].Rows[0]["InfFare"].ToString().Trim());
                            //changes by  end
                            //bool FareStatus=false;
                            double farediff = OriginalTF - TotFare;
                            //if (Convert.ToDouble(OriginalTF) * (Adult + Child + Infant) == Convert.ToDouble(FltDsGAL.Tables[0].Rows[0]["OriginalTF"].ToString()))
                            //if (OriginalTF == TotFare)//condition changed by 
                            if (farediff < 3)
                            {
                                // SessionIdTST = GetIncrementedSessionId(SessionIdAME);
                                string SessionIdTST = objSsnHdr.GetSessionInseries(PriceRes);
                                SessionId = SessionIdTST;
                                TSTFromPricingXML = Ticket_CreateTSTFromPricing(SessionIdTST, ListSAU[0].TST, Adult, Child, Infant, FltDsGAL.Tables[0].Rows[0]["AdtFare"].ToString().Trim(), FltDsGAL.Tables[0].Rows[0]["ChdFare"].ToString().Trim(), FltDsGAL.Tables[0].Rows[0]["InfFare"].ToString().Trim(), PriceRes.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_11_1_1A\"", ""), AddMultiElementsXML.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_11_1_1A\"", ""), FltHdr.Tables[0].Rows[0]["VC"].ToString().Trim(), FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim(), SMSChg, "WSP4APRE", "KTMVS3270", "AMADEUS100", ServiceUrls.SvcURL, IncludeSessionType.InSeries, FltHdr.Tables[0].Rows[0]["OrderID"].ToString());//Ticket_CreateTSTFromPricing(SessionIdTST, "http://webservices.amadeus.com/1ASIWMASSPT/TAUTCQ_04_1_1A", Adult, Child, Infant, FltDsGAL.Tables[0].Rows[0]["AdtFare"].ToString().Trim(), FltDsGAL.Tables[0].Rows[0]["ChdFare"].ToString().Trim(), FltDsGAL.Tables[0].Rows[0]["InfFare"].ToString().Trim(), PriceRes.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_08_2_1A\"", ""), AddMultiElementsXML.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_08_2_1A\"", ""), FltHdr.Tables[0].Rows[0]["VC"].ToString().Trim(), FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim());
                                if (HSREQRES["TST_STATUS"].ToString() == "TRUE")
                                {
                                    //Begin Step5 PNR_AddMultiElements with option code 11
                                    string SessionIdADM = objSsnHdr.GetSessionInseries(TSTFromPricingXML);
                                    SessionId = SessionIdADM;
                                    AddMEleXML = PNR_AddMultiElements11(SessionIdADM, ListSAU[0].GDSPNR, "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL);//PNR_AddMultiElements11(SessionIdADM, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                                    if (HSREQRES["PNR_STATUS"].ToString() == "TRUE")
                                    {
                                        //string PNR = "";
                                        //Parse XML and return PNR
                                        XDocument docpnr = XDocument.Parse(AddMEleXML.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_17_1_1A\"", ""));
                                        if (docpnr.Descendants("PNR_Reply").Descendants("pnrHeader").Descendants("reservationInfo").Descendants("reservation").Elements("controlNumber").Any() == true)
                                        {
                                            GDSPNR = docpnr.Descendants("PNR_Reply").Descendants("pnrHeader").Descendants("reservationInfo").Descendants("reservation").Elements("controlNumber").First().Value;
                                            HSPNR.Add("GDSPNR", GDSPNR);
                                        }
                                        else
                                        {
                                            GDSPNR = Utility.GetRndm() + "-FQ";
                                            HSPNR.Add("GDSPNR", GDSPNR);
                                        }
                                        BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), GDSPNR, HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["TST_RES"].ToString(), HSREQRES["PNR_REQ"].ToString(), HSREQRES["PNR_RES"].ToString(), farediff.ToString());
                                        try
                                        {
                                            if (docpnr.Descendants("PNR_Reply").Descendants("originDestinationDetails").Descendants("itineraryInfo").Descendants("itineraryReservationInfo").Any() == true)
                                            {
                                                AirlinePNR = docpnr.Descendants("PNR_Reply").Descendants("originDestinationDetails").Descendants("itineraryInfo").Descendants("itineraryReservationInfo").Descendants("reservation").Elements("controlNumber").First().Value;

                                            }
                                            string AddMEleXML21 = "";

                                            if (!string.IsNullOrEmpty(GDSPNR) && !GDSPNR.Contains("-FQ"))
                                            {

                                                SessionId = objSsnHdr.GetSessionInseries(AddMEleXML);
                                                OnlineTicketing objtkt = new OnlineTicketing();
                                                TktNoArray = objtkt.GDSOnlineTicketing(SessionId, GDSPNR, Crd, FltHdr.Tables[0].Rows[0]["OrderId"].ToString(), "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL);
                                                HSPNR.Add("TktNoArray", TktNoArray);

                                            }

                                        }
                                        catch (Exception ex) { }

                                        //SessionOut                                           
                                        HSPNR.Add("AirlinePNR", AirlinePNR);
                                        SignOut(ListSAU[0].SIGNOUT, SessionId);//SignOut("http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A", SessionId);
                                    }
                                    else
                                    {
                                        GDSPNR = Utility.GetRndm() + "-FQ";
                                        //AIRLINEPNR = "";
                                        HSPNR.Add("GDSPNR", GDSPNR);
                                        HSPNR.Add("AirlinePNR", AirlinePNR);
                                        TktNoArray.Add("AIRLINE");
                                        HSPNR.Add("TktNoArray", TktNoArray);
                                        SignOut(ListSAU[0].SIGNOUT, SessionId); //SignOut("http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A", SessionId);
                                    }


                                }
                                else
                                {
                                    GDSPNR = Utility.GetRndm() + "-FQ";
                                    HSPNR.Add("GDSPNR", GDSPNR);
                                    HSPNR.Add("AirlinePNR", AirlinePNR);
                                    TktNoArray.Add("AIRLINE");
                                    HSPNR.Add("TktNoArray", TktNoArray);
                                    SignOut(ListSAU[0].SIGNOUT, SessionId);//SignOut("http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A", SessionId);
                                                                           //AIRLINEPNR = "";
                                }
                            }

                            else
                            {
                                BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), GDSPNR, HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), "", "", "", "", farediff.ToString());
                                GDSPNR = Utility.GetRndm() + "-FQ";
                                HSPNR.Add("GDSPNR", GDSPNR);
                                HSPNR.Add("AirlinePNR", AirlinePNR);
                                SessionIdTST = GetIncrementedSessionId(SessionIdAME);
                                PNR_AddMultiElements20(SessionIdTST, ListSAU[0].IGNORE); //PNR_AddMultiElements20(SessionIdTST, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                                SignOut(ListSAU[0].SIGNOUT, SessionId);//SignOut("http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A", SessionId);
                                                                       //AIRLINEPNR = "";
                            }


                        }
                        else
                        {
                            GDSPNR = Utility.GetRndm() + "-FQ";
                            HSPNR.Add("GDSPNR", GDSPNR);
                            HSPNR.Add("AirlinePNR", AirlinePNR);
                            TktNoArray.Add("AIRLINE");
                            HSPNR.Add("TktNoArray", TktNoArray);
                            SignOut(ListSAU[0].SIGNOUT, SessionId);//SignOut("http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A", SessionId);
                                                                   //   AIRLINEPNR = "";
                        }

                    }
                    else
                    {

                        GDSPNR = Utility.GetRndm() + "-FQ";
                        HSPNR.Add("GDSPNR", GDSPNR);
                        HSPNR.Add("AirlinePNR", AirlinePNR);
                        TktNoArray.Add("AIRLINE");
                        HSPNR.Add("TktNoArray", TktNoArray);
                        SignOut(ListSAU[0].SIGNOUT, SessionId);//SignOut("http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A", SessionId);
                                                               //AIRLINEPNR = "";
                    }


                }
                else
                {
                    GDSPNR = Utility.GetRndm() + "-FQ";
                    HSPNR.Add("GDSPNR", GDSPNR);
                    HSPNR.Add("AirlinePNR", AirlinePNR);
                    TktNoArray.Add("AIRLINE");
                    HSPNR.Add("TktNoArray", TktNoArray);
                    SignOut(ListSAU[0].SIGNOUT, SessionId);// SignOut("http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A", SessionId);
                                                           //AIRLINEPNR = "";
                }

                //}
                //else
                //{
                //    GDSPNR = Utility.GetRndm() + "-FQ";
                //    HSPNR.Add("GDSPNR", GDSPNR);
                //    HSPNR.Add("AirlinePNR", AirlinePNR);
                //    //AIRLINEPNR = "";
                //}
            }
            catch (Exception ex)
            {
                GDSPNR = Utility.GetRndm() + "-FQ";
                HSPNR.Add("GDSPNR", GDSPNR);
                HSPNR.Add("AirlinePNR", AirlinePNR);
                TktNoArray.Add("AIRLINE");
                HSPNR.Add("TktNoArray", TktNoArray);


            }
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();

                foreach (DictionaryEntry M in HSREQRES)
                {
                    XmlBuilder.Append("<LOG>");
                    XmlBuilder.Append("<NAME>" + Convert.ToString(M.Key) + "</NAME>");
                    XmlBuilder.Append("<RESPONSE>" + Convert.ToString(M.Value) + "</RESPONSE>");
                    XmlBuilder.Append("<DATETIME>" + System.DateTime.Now.ToString() + "</DATETIME>");
                    XmlBuilder.Append("</LOG>");
                }
                string folder = "MPTB/" + FltHdr.Tables[0].Rows[0]["OrderId"].ToString();

                AmdUtility.SaveFile(XmlBuilder.ToString(), "AMDLOG_" + FltHdr.Tables[0].Rows[0]["OrderId"].ToString(), folder);

            }
            catch { }


            return HSPNR;

        }
        #endregion PnrCreation
        #region SignIn
        public string SignIn(string SoapAction)
        {
            // SoapAction = "http://webservices.amadeus.com/VLSSLQ_06_1_1A";
            SoapAction = "http://webservices.amadeus.com/VLSSOQ_04_1_1A";
            SHA1 sha1 = SHA1.Create();
            string timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss") + "Z";//"2016-10-13T6:37:48Z";


            string nonsce = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15);

            // var bytes = Encoding.UTF8.GetBytes(nonsce);


            byte[] bytesnonse = Encoding.UTF8.GetBytes(nonsce);
            var base64Nonce = Convert.ToBase64String(bytesnonse);

            byte[] bytedate = Encoding.UTF8.GetBytes(timestamp);
            byte[] bytepass = sha1.ComputeHash(Encoding.Default.GetBytes("AMADEUS100"));//("AMADEUS"));

            GALWS.AMDNPL.AmdUtility objUtiity = new GALWS.AMDNPL.AmdUtility();
            var result = objUtiity.ConcatArrays(objUtiity.ConcatArrays(bytesnonse, bytedate), bytepass);

            var bytesPassDigest = sha1.ComputeHash(result);

            var base64passDigest = Convert.ToBase64String(bytesPassDigest);

            string pwd = "AMADEUS100";
            byte[] bytesnonsepwd = Encoding.UTF8.GetBytes(pwd);
            var base64Noncepwd = Convert.ToBase64String(bytesnonsepwd);
            string session = "";
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                XmlBuilder.Append("<soap:Body>");
                XmlBuilder.Append("<Security_Authenticate>");
                XmlBuilder.Append("<userIdentifier>");
                XmlBuilder.Append("<originIdentification>");
                XmlBuilder.Append("<sourceOffice>KTMVS3270</sourceOffice>");
                XmlBuilder.Append("</originIdentification>");
                XmlBuilder.Append("<originatorTypeCode>U</originatorTypeCode>");
                XmlBuilder.Append("<originator>WSP4APRE</originator>");
                XmlBuilder.Append("</userIdentifier>");
                XmlBuilder.Append("<dutyCode>");
                XmlBuilder.Append("<dutyCodeDetails>");
                XmlBuilder.Append("<referenceQualifier>DUT</referenceQualifier>");
                XmlBuilder.Append("<referenceIdentifier>SU</referenceIdentifier>");
                XmlBuilder.Append("</dutyCodeDetails>");
                XmlBuilder.Append("</dutyCode>");
                XmlBuilder.Append("<systemDetails>");
                XmlBuilder.Append("<organizationDetails>");
                XmlBuilder.Append("<organizationId>NMC-INDIA</organizationId>");
                XmlBuilder.Append("</organizationDetails>");
                XmlBuilder.Append("</systemDetails>");
                XmlBuilder.Append("<passwordInfo>");
                XmlBuilder.Append("<dataLength>10</dataLength>");
                XmlBuilder.Append("<dataType>E</dataType>");
                XmlBuilder.Append("<binaryData>QU1BREVVUzEwMA==</binaryData>");
                XmlBuilder.Append("</passwordInfo>");
                XmlBuilder.Append("</Security_Authenticate>");
                XmlBuilder.Append("</soap:Body>");
                XmlBuilder.Append("</soap:Envelope>");
                string strsessionid = "";
                strsessionid = PostXml(XmlBuilder.ToString(), SoapAction);
                XNamespace ns = "http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd";// "http://webservices.amadeus.com/definitions";
                XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
                XDocument xdoc = XDocument.Parse(strsessionid);
                session = xdoc.Descendants(ns + "SessionId").First().Value + "|" + xdoc.Descendants(ns + "SequenceNumber").First().Value + "|" + xdoc.Descendants(ns + "SecurityToken").First().Value;//(xdoc.Descendants(soap + "Header")).Descendants(ns + "Session").Descendants("SessionId").First().Value;

            }
            catch (Exception ex)
            {
                session = "";

            }

            return session;
        }
        #endregion End SignIn
        private string GetSessionHeader(string SH)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            string[] ss = Utility.Split(SH, "|");
            XmlBuilder.Append("<wbs:Session>");
            XmlBuilder.Append("<wbs:SessionId>" + ss[0].ToString() + "</wbs:SessionId>");
            XmlBuilder.Append("<wbs:SequenceNumber>" + ss[1].ToString() + "</wbs:SequenceNumber>");
            XmlBuilder.Append("<wbs:SecurityToken>" + ss[2].ToString() + "</wbs:SecurityToken>");
            XmlBuilder.Append("</wbs:Session>");
            return XmlBuilder.ToString();
        }
        #region Air_SellFromRecommendation
        public string Air_SellFromRecommendation(string sessionid, string SoapAction, int Adult, int Child, int Infant, DataSet FltDsGAL, DataSet FltHdr, DataSet PaxDs,
            string username, string officeID, string pass, string svcUrl, IncludeSessionType includeSessionType)
        {
            SoapAction = "http://webservices.amadeus.com/ITAREQ_05_2_IA";

            string SELRes = "";
            AmdSession objSessuonHeader = new AmdSession();
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:itar='http://xml.amadeus.com/ITAREQ_05_2_IA'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");
                XmlBuilder.Append("<Air_SellFromRecommendation>");
                XmlBuilder.Append("<messageActionDetails>");
                XmlBuilder.Append("<messageFunctionDetails>");
                XmlBuilder.Append("<messageFunction>183</messageFunction>");
                XmlBuilder.Append("<additionalMessageFunction>M1</additionalMessageFunction>");
                XmlBuilder.Append("</messageFunctionDetails>");
                XmlBuilder.Append("</messageActionDetails>");
                DataRow[] droneway;
                DataRow[] drround = new DataRow[0];

                if (FltHdr.Tables[0].Rows[0]["TripType"].ToString().ToUpper() == "O")
                {
                    droneway = FltDsGAL.Tables[0].Select("flight=1", "counter asc");

                }
                else
                {
                    droneway = FltDsGAL.Tables[0].Select("flight=1", "counter asc");
                    drround = FltDsGAL.Tables[0].Select("flight=2", "counter asc");
                }
                if (droneway.Length > 0 || drround.Length > 0)
                {
                    XmlBuilder.Append("<itineraryDetails>");
                    XmlBuilder.Append("<originDestinationDetails>");
                    XmlBuilder.Append("<origin>" + droneway[0]["OrgDestFrom"].ToString() + "</origin>");//need
                    XmlBuilder.Append("<destination>" + droneway[0]["OrgDestTo"].ToString() + "</destination>");//need
                    XmlBuilder.Append("</originDestinationDetails>");//need
                    XmlBuilder.Append("<message>");
                    XmlBuilder.Append("<messageFunctionDetails>");
                    XmlBuilder.Append("<messageFunction>183</messageFunction>");
                    XmlBuilder.Append("</messageFunctionDetails>");
                    XmlBuilder.Append("</message>");

                    for (int i = 0; i < droneway.Length; i++)
                    {
                        XmlBuilder.Append("<segmentInformation>");
                        XmlBuilder.Append("<travelProductInformation>");
                        XmlBuilder.Append("<flightDate>");
                        string DepDate = Convert.ToString(droneway[i]["DepartureDate"]);
                        // string DepDate = STD.BAL.Utility.Right(droneway[i]["DepartureDate"].ToString(), 2) + droneway[i]["DepartureDate"].ToString().Substring(4, 2) + droneway[i]["DepartureDate"].ToString().Substring(2, 2);
                        XmlBuilder.Append("<departureDate>" + DepDate + "</departureDate>");
                        XmlBuilder.Append("</flightDate>");
                        XmlBuilder.Append("<boardPointDetails>");
                        XmlBuilder.Append("<trueLocationId>" + droneway[i]["DepartureLocation"].ToString() + "</trueLocationId>");//need
                        XmlBuilder.Append("</boardPointDetails>");
                        XmlBuilder.Append("<offpointDetails>");
                        XmlBuilder.Append("<trueLocationId>" + droneway[i]["ArrivalLocation"].ToString() + "</trueLocationId>");//need
                        XmlBuilder.Append("</offpointDetails>");
                        XmlBuilder.Append("<companyDetails>");
                        XmlBuilder.Append("<marketingCompany>" + droneway[i]["MarketingCarrier"].ToString() + "</marketingCompany>");//need
                        XmlBuilder.Append("</companyDetails>");
                        XmlBuilder.Append("<flightIdentification>");
                        XmlBuilder.Append("<flightNumber>" + droneway[i]["FlightIdentification"].ToString() + "</flightNumber>");//need
                        XmlBuilder.Append("<bookingClass>" + droneway[i]["RBD"].ToString() + "</bookingClass>");//need
                        XmlBuilder.Append("</flightIdentification>");
                        XmlBuilder.Append("</travelProductInformation>");
                        XmlBuilder.Append("<relatedproductInformation>");
                        XmlBuilder.Append(" <quantity>" + (Adult + Child) + "</quantity>");//need
                        XmlBuilder.Append("<statusCode>NN</statusCode>");//need                       
                        XmlBuilder.Append("</relatedproductInformation>");
                        XmlBuilder.Append("</segmentInformation>");

                    }


                    XmlBuilder.Append("</itineraryDetails>");
                    #region itenary 2
                    if (drround.Length > 0)
                    {
                        XmlBuilder.Append("<itineraryDetails>");

                        XmlBuilder.Append("<originDestinationDetails>");
                        XmlBuilder.Append("<origin>" + drround[0]["OrgDestTo"].ToString() + "</origin>");//need
                        XmlBuilder.Append("<destination>" + drround[0]["OrgDestFrom"].ToString() + "</destination>");//need
                        XmlBuilder.Append("</originDestinationDetails>");//need
                        XmlBuilder.Append("<message>");
                        XmlBuilder.Append("<messageFunctionDetails>");
                        XmlBuilder.Append("<messageFunction>183</messageFunction>");
                        XmlBuilder.Append("</messageFunctionDetails>");
                        XmlBuilder.Append("</message>");

                        for (int i = 0; i < drround.Length; i++)
                        {
                            XmlBuilder.Append("<segmentInformation>");
                            XmlBuilder.Append("<travelProductInformation>");
                            XmlBuilder.Append("<flightDate>");
                            string DepDate = Convert.ToString(drround[i]["DepartureDate"]); //STD.BAL.Utility.Right(drround[i]["DepartureDate"].ToString(), 2) + drround[i]["DepartureDate"].ToString().Substring(4, 2) + drround[i]["DepartureDate"].ToString().Substring(2, 2);
                            // XmlBuilder.Append("<departureDate>" + drround[i]["DepartureDate"].ToString() + "</departureDate>");//MPTB
                            XmlBuilder.Append("<departureDate>" + DepDate + "</departureDate>");
                            XmlBuilder.Append("</flightDate>");
                            XmlBuilder.Append("<boardPointDetails>");
                            XmlBuilder.Append("<trueLocationId>" + drround[i]["DepartureLocation"].ToString() + "</trueLocationId>");//need
                            XmlBuilder.Append("</boardPointDetails>");
                            XmlBuilder.Append("<offpointDetails>");
                            XmlBuilder.Append("<trueLocationId>" + drround[i]["ArrivalLocation"].ToString() + "</trueLocationId>");//need
                            XmlBuilder.Append("</offpointDetails>");
                            XmlBuilder.Append("<companyDetails>");
                            XmlBuilder.Append("<marketingCompany>" + drround[i]["MarketingCarrier"].ToString() + "</marketingCompany>");//need
                            XmlBuilder.Append("</companyDetails>");
                            XmlBuilder.Append("<flightIdentification>");
                            XmlBuilder.Append("<flightNumber>" + drround[i]["FlightIdentification"].ToString() + "</flightNumber>");//need
                            XmlBuilder.Append("<bookingClass>" + drround[i]["RBD"].ToString() + "</bookingClass>");//need
                            XmlBuilder.Append("</flightIdentification>");
                            XmlBuilder.Append("</travelProductInformation>");
                            XmlBuilder.Append("<relatedproductInformation>");
                            XmlBuilder.Append(" <quantity>" + (Adult + Child) + "</quantity>");//need
                            XmlBuilder.Append("<statusCode>NN</statusCode>");//need
                            XmlBuilder.Append("</relatedproductInformation>");
                            XmlBuilder.Append("</segmentInformation>");
                        }
                        XmlBuilder.Append("</itineraryDetails>");
                    }
                    #endregion
                }
                XmlBuilder.Append("</Air_SellFromRecommendation>");
                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                HSREQRES.Add("SELL_REQ", XmlBuilder.ToString());
                string folder = "MPTB/" + FltHdr.Tables[0].Rows[0]["OrderId"].ToString() + DateTime.Now.ToString("hh_mm_ss");
                SELRes = AmdUtility.PostXml(XmlBuilder.ToString(), ServiceUrls.Air_SellFromRecommendation, svcUrl, "Air_SellFromRecommendation", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");
                                                                                                                                                         //   SELRes = PostXml(XmlBuilder.ToString(), SoapAction);
                HSREQRES.Add("SELL_RES", SELRes);

                if (SELRes == "")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), "", "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("SELL_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE);//PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (SELRes.Contains("<errorText>") == true)
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("SELL_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE);//PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (SELRes.Substring(0, 5) == "Error")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), "", "", "", "", "", "", "", "", "", HSREQRES["SELL_RES"].ToString());
                    HSREQRES.Add("SELL_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else
                {
                    string StatusCode = sell_recom_chk(SELRes);
                    if (StatusCode == "True")
                    {
                        HSREQRES.Add("SELL_STATUS", "TRUE");
                    }
                    else
                    {
                        HSREQRES.Add("SELL_STATUS", "FALSE");
                        BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), "", "", "", "", "", "", "", "", "");
                        sessionid20 = GetIncrementedSessionId(sessionid);
                        PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                    }
                }
            }
            catch (Exception ex)
            {

                SELRes = "";
                HSREQRES.Add("SELL_STATUS", "FALSE");
                BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), "", "", "", "", "", "", "", "", "", ex.ToString());
                sessionid20 = GetIncrementedSessionId(sessionid);
                PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
            }

            return SELRes;
        }
        #endregion End Air_SellFromRecommendation
        #region AddMultiElements
        public string AddMultiElements(string sessionid, string SoapAction, int Adult, int Child, int Infant, DataSet FltDsGAL, DataSet FltHdr, DataSet PaxDs, string username, string officeID, string pass, string svcUrl, IncludeSessionType includeSessionType)
        {
            SoapAction = "http://webservices.amadeus.com/PNRADD_17_1_1A";
            int j = 0;
            int k = 0;
            string admultielement = "";
            string ADDMELEXML = "";
            int AdtSeat = 0;
            int ChdSeat = 0;

            int AdtMeal = 0;
            int ChdMeal = 0;
            AmdSession objSessuonHeader = new AmdSession();
            try
            {

                //StringBuilder XmlBuilder = new StringBuilder();
                //XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                ////XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
                //XmlBuilder.Append("<soapenv:Header>");
                //XmlBuilder.Append(GetSessionHeader(sessionid));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                //XmlBuilder.Append("</soapenv:Header>");
                //XmlBuilder.Append("<soapenv:Body>");
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));//GetSessionHeader(sessionid));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");
                XmlBuilder.Append("<PNR_AddMultiElements>");
                XmlBuilder.Append("<pnrActions>");
                XmlBuilder.Append("<optionCode>0</optionCode>");
                XmlBuilder.Append("</pnrActions>");

                DataRow[] DrAdult = new DataRow[0];
                DataRow[] DrChild = new DataRow[0];
                DataRow[] DrInfant = new DataRow[0];
                DrAdult = PaxDs.Tables[0].Select("PaxType='ADT'");
                DrChild = PaxDs.Tables[0].Select("PaxType='CHD'");
                DrInfant = PaxDs.Tables[0].Select("PaxType='INF'");
                int TotPax = Adult + Child;// +Infant;

                for (j = 1; j <= TotPax; j++)
                {
                    if (Adult == Infant)
                    {
                        //Val(Adult)
                        for (k = 1; k <= Adult; k++)
                        {
                            XmlBuilder.Append("<travellerInfo>");
                            XmlBuilder.Append("<elementManagementPassenger>");
                            XmlBuilder.Append("<reference>");
                            XmlBuilder.Append("<qualifier>PR</qualifier>");
                            XmlBuilder.Append("<number>" + j + "</number>");
                            XmlBuilder.Append("</reference>");
                            XmlBuilder.Append("<segmentName>NM</segmentName>");
                            XmlBuilder.Append("</elementManagementPassenger>");
                            XmlBuilder.Append("<passengerData>");
                            XmlBuilder.Append("<travellerInformation>");
                            XmlBuilder.Append("<traveller>");
                            XmlBuilder.Append("<surname>" + DrAdult[k - 1]["LName"].ToString() + "</surname>");
                            XmlBuilder.Append("<quantity>2</quantity>");
                            XmlBuilder.Append("</traveller>");
                            XmlBuilder.Append("<passenger>");
                            if (DrAdult[k - 1]["MName"].ToString() == "")
                            {
                                XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                            }
                            else
                            {
                                XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["MName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                            }
                            XmlBuilder.Append("<type>ADT</type>");
                            XmlBuilder.Append("<infantIndicator>2</infantIndicator>");
                            XmlBuilder.Append("</passenger>");
                            XmlBuilder.Append("<passenger>");
                            XmlBuilder.Append("<firstName>" + DrInfant[k - 1]["FName"].ToString() + "</firstName>");
                            XmlBuilder.Append("<type>INF</type>");
                            XmlBuilder.Append("</passenger>");
                            XmlBuilder.Append("</travellerInformation>");
                            XmlBuilder.Append("<dateOfBirth>");
                            XmlBuilder.Append("<dateAndTimeDetails>");
                            XmlBuilder.Append("<qualifier>706</qualifier>");
                            // XmlBuilder.Append("<date>" + "</date>");
                            XmlBuilder.Append("<date>" + STD.BAL.Utility.Left(DrInfant[k - 1]["DOB"].ToString(), 2) + "" + datecon(STD.BAL.Utility.Mid(DrInfant[k - 1]["DOB"].ToString(), 3, 2)) + "" + STD.BAL.Utility.Right(DrInfant[k - 1]["DOB"].ToString(), 2) + "</date>");
                            XmlBuilder.Append("</dateAndTimeDetails>");
                            XmlBuilder.Append("</dateOfBirth>");
                            XmlBuilder.Append("</passengerData>");
                            XmlBuilder.Append("</travellerInfo>");
                            //XmlBuilder.Append("</passengerData>");
                            //XmlBuilder.Append("</travellerInfo>");
                            ADDMELEXML = ADDMELEXML + XmlBuilder.ToString();
                            XmlBuilder = new StringBuilder();
                            j = j + 1;
                        }
                    }
                    else if (Adult > Infant && Infant > 0)
                    {
                        //Val(Adult)
                        for (k = 1; k <= Adult; k++)
                        {
                            if (Infant > 0)
                            {

                                XmlBuilder.Append("<travellerInfo>");
                                XmlBuilder.Append("<elementManagementPassenger>");
                                XmlBuilder.Append("<reference>");
                                XmlBuilder.Append("<qualifier>PR</qualifier>");
                                XmlBuilder.Append("<number>" + j + "</number>");
                                XmlBuilder.Append("</reference>");
                                XmlBuilder.Append("<segmentName>NM</segmentName>");
                                XmlBuilder.Append("</elementManagementPassenger>");
                                XmlBuilder.Append("<passengerData>");
                                XmlBuilder.Append("<travellerInformation>");
                                XmlBuilder.Append("<traveller>");
                                XmlBuilder.Append("<surname>" + DrAdult[k - 1]["LName"].ToString() + "</surname>");
                                XmlBuilder.Append("<quantity>2</quantity>");
                                XmlBuilder.Append("</traveller>");
                                XmlBuilder.Append("<passenger>");
                                if (DrAdult[k - 1]["MName"].ToString() == "")
                                {
                                    XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                                }
                                else
                                {
                                    XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["MName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                                }
                                XmlBuilder.Append("<type>ADT</type>");
                                XmlBuilder.Append("<infantIndicator>2</infantIndicator>");
                                XmlBuilder.Append("</passenger>");
                                XmlBuilder.Append("<passenger>");
                                //XmlBuilder.Append("<firstName>" + DrInfant[k - 1]["FName"].ToString() + " " + DrInfant[k - 1]["Title"].ToString() + " </firstName>");
                                XmlBuilder.Append("<firstName>" + DrInfant[k - 1]["FName"].ToString() + "</firstName>");
                                XmlBuilder.Append("<type>INF</type>");
                                XmlBuilder.Append("</passenger>");
                                XmlBuilder.Append("</travellerInformation>");
                                XmlBuilder.Append("<dateOfBirth>");
                                XmlBuilder.Append("<dateAndTimeDetails>");
                                XmlBuilder.Append("<qualifier>706</qualifier>");
                                XmlBuilder.Append("<date>" + STD.BAL.Utility.Left(DrInfant[k - 1]["DOB"].ToString(), 2) + "" + datecon(STD.BAL.Utility.Mid(DrInfant[k - 1]["DOB"].ToString(), 3, 2)) + "" + STD.BAL.Utility.Right(DrInfant[k - 1]["DOB"].ToString(), 2) + "</date>");
                                XmlBuilder.Append("</dateAndTimeDetails>");
                                XmlBuilder.Append("</dateOfBirth>");
                                XmlBuilder.Append("</passengerData>");
                                XmlBuilder.Append("</travellerInfo>");
                            }
                            else
                            {
                                XmlBuilder.Append("<travellerInfo>");
                                XmlBuilder.Append("<elementManagementPassenger>");
                                XmlBuilder.Append("<reference>");
                                XmlBuilder.Append("<qualifier>PR</qualifier>");
                                XmlBuilder.Append("<number>" + j + "</number>");
                                XmlBuilder.Append("</reference>");
                                XmlBuilder.Append("<segmentName>NM</segmentName>");
                                XmlBuilder.Append("</elementManagementPassenger>");
                                XmlBuilder.Append("<passengerData>");
                                XmlBuilder.Append("<travellerInformation>");
                                XmlBuilder.Append("<traveller>");
                                XmlBuilder.Append("<surname>" + DrAdult[k - 1]["LName"].ToString() + "</surname>");
                                XmlBuilder.Append("<quantity>1</quantity>");
                                XmlBuilder.Append("</traveller>");
                                XmlBuilder.Append("<passenger>");
                                if (DrAdult[k - 1]["MName"].ToString() == "")
                                {
                                    XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                                }
                                else
                                {
                                    XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["MName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                                }
                                XmlBuilder.Append("<type>ADT</type>");
                                XmlBuilder.Append("</passenger>");
                                XmlBuilder.Append("</travellerInformation>");
                                XmlBuilder.Append("</passengerData>");
                                XmlBuilder.Append("</travellerInfo>");

                            }
                            ADDMELEXML = ADDMELEXML + XmlBuilder.ToString();
                            XmlBuilder = new StringBuilder();
                            Infant = Infant - 1;
                            j = j + 1;
                            //k = k + 1;
                        }
                    }
                    else
                    {
                        for (k = 1; k <= Adult; k++)
                        {
                            XmlBuilder.Append("<travellerInfo>");
                            XmlBuilder.Append("<elementManagementPassenger>");
                            XmlBuilder.Append("<reference>");
                            XmlBuilder.Append("<qualifier>PR</qualifier>");
                            XmlBuilder.Append("<number>" + j + "</number>");
                            XmlBuilder.Append("</reference>");
                            XmlBuilder.Append("<segmentName>NM</segmentName>");
                            XmlBuilder.Append("</elementManagementPassenger>");
                            XmlBuilder.Append("<passengerData>");
                            XmlBuilder.Append("<travellerInformation>");
                            XmlBuilder.Append("<traveller>");
                            XmlBuilder.Append("<surname>" + DrAdult[k - 1]["LName"].ToString() + "</surname>");
                            XmlBuilder.Append("<quantity>1</quantity>");
                            XmlBuilder.Append("</traveller>");
                            XmlBuilder.Append("<passenger>");
                            if (DrAdult[k - 1]["MName"].ToString() == "")
                            {
                                XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                            }
                            else
                            {
                                XmlBuilder.Append("<firstName>" + DrAdult[k - 1]["FName"].ToString() + " " + DrAdult[k - 1]["MName"].ToString() + " " + DrAdult[k - 1]["Title"].ToString() + " </firstName>");
                            }
                            XmlBuilder.Append("<type>ADT</type>");
                            XmlBuilder.Append("</passenger>");
                            XmlBuilder.Append("</travellerInformation>");
                            XmlBuilder.Append("</passengerData>");
                            XmlBuilder.Append("</travellerInfo>");
                            ADDMELEXML = ADDMELEXML + XmlBuilder.ToString();
                            XmlBuilder = new StringBuilder();
                            j = j + 1;
                        }
                    }
                    if ((Child > 0))
                    {
                        for (k = 1; k <= Child; k++)
                        {

                            XmlBuilder.Append("<travellerInfo>");
                            XmlBuilder.Append("<elementManagementPassenger>");
                            XmlBuilder.Append("<reference>");
                            XmlBuilder.Append("<qualifier>PR</qualifier>");
                            XmlBuilder.Append("<number>" + j + "</number>");
                            XmlBuilder.Append("</reference>");
                            XmlBuilder.Append("<segmentName>NM</segmentName>");
                            XmlBuilder.Append("</elementManagementPassenger>");
                            XmlBuilder.Append("<passengerData>");
                            XmlBuilder.Append("<travellerInformation>");
                            XmlBuilder.Append("<traveller>");
                            XmlBuilder.Append("<surname>" + DrChild[k - 1]["LName"].ToString() + "</surname>");
                            XmlBuilder.Append("<quantity>1</quantity>");
                            XmlBuilder.Append("</traveller>");
                            XmlBuilder.Append("<passenger>");
                            if (DrAdult[k - 1]["MName"].ToString() == "")
                            {
                                XmlBuilder.Append("<firstName>" + DrChild[k - 1]["FName"].ToString() + " " + DrChild[k - 1]["Title"].ToString() + " </firstName>");
                            }
                            else
                            {
                                XmlBuilder.Append("<firstName>" + DrChild[k - 1]["FName"].ToString() + " " + DrChild[k - 1]["MName"].ToString() + " " + DrChild[k - 1]["Title"].ToString() + " </firstName>");
                            }
                            XmlBuilder.Append("<type>CHD</type>");
                            XmlBuilder.Append("</passenger>");
                            XmlBuilder.Append("</travellerInformation>");
                            XmlBuilder.Append("<dateOfBirth>");
                            XmlBuilder.Append("<dateAndTimeDetails>");
                            XmlBuilder.Append("<qualifier>706</qualifier>");
                            XmlBuilder.Append("<date>" + STD.BAL.Utility.Left(DrChild[k - 1]["DOB"].ToString(), 2) + "" + datecon(STD.BAL.Utility.Mid(DrChild[k - 1]["DOB"].ToString(), 3, 2)) + "" + STD.BAL.Utility.Right(DrChild[k - 1]["DOB"].ToString(), 2) + "</date>");
                            XmlBuilder.Append("</dateAndTimeDetails>");
                            XmlBuilder.Append("</dateOfBirth>");
                            XmlBuilder.Append("</passengerData>");
                            XmlBuilder.Append("</travellerInfo>");
                            //XmlBuilder.Append("</passengerData>");
                            //XmlBuilder.Append("</travellerInfo>");
                            ADDMELEXML = ADDMELEXML + XmlBuilder.ToString();
                            XmlBuilder = new StringBuilder();
                            j = j + 1;
                            // Child = Child - 1;
                        }
                    }
                }
                XmlBuilder.Append("<dataElementsMaster>");
                XmlBuilder.Append("<marker1/>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<reference>");
                XmlBuilder.Append("<qualifier>OT</qualifier>");
                XmlBuilder.Append("<number>1</number>");
                XmlBuilder.Append("</reference>");
                XmlBuilder.Append("<segmentName>RF</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<freetextData>");
                XmlBuilder.Append("<freetextDetail>");
                XmlBuilder.Append("<subjectQualifier>3</subjectQualifier>");
                XmlBuilder.Append("<type>P22</type>");
                XmlBuilder.Append("</freetextDetail>");
                XmlBuilder.Append("<longFreetext>Online Booking</longFreetext>");
                XmlBuilder.Append("</freetextData>");
                XmlBuilder.Append("</dataElementsIndiv>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<reference>");
                XmlBuilder.Append("<qualifier>OT</qualifier>");
                XmlBuilder.Append("<number>2</number>");
                XmlBuilder.Append("</reference>");
                XmlBuilder.Append("<segmentName>AP</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<freetextData>");
                XmlBuilder.Append("<freetextDetail>");
                XmlBuilder.Append("<subjectQualifier>3</subjectQualifier>");
                XmlBuilder.Append("<type>6</type>");
                XmlBuilder.Append("</freetextDetail>");
                XmlBuilder.Append("<longFreetext>011-40878787</longFreetext>");
                XmlBuilder.Append("</freetextData>");
                XmlBuilder.Append("</dataElementsIndiv>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<reference>");
                XmlBuilder.Append("<qualifier>OT</qualifier>");
                XmlBuilder.Append("<number>3</number>");
                XmlBuilder.Append("</reference>");
                XmlBuilder.Append("<segmentName>RM</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<miscellaneousRemark>");
                XmlBuilder.Append("<remarks>");
                XmlBuilder.Append("<type>RM</type>");
                XmlBuilder.Append("<freetext>" + FltHdr.Tables[0].Rows[0]["PgEmail"].ToString() + "</freetext>");
                XmlBuilder.Append("</remarks>");
                XmlBuilder.Append("</miscellaneousRemark>");
                XmlBuilder.Append("</dataElementsIndiv>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<reference>");
                XmlBuilder.Append("<qualifier>OT</qualifier>");
                XmlBuilder.Append("<number>3</number>");
                XmlBuilder.Append("</reference>");
                XmlBuilder.Append("<segmentName>RM</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<miscellaneousRemark>");
                XmlBuilder.Append("<remarks>");
                XmlBuilder.Append("<type>RM</type>");
                XmlBuilder.Append("<freetext>Online Booking By " + FltHdr.Tables[0].Rows[0]["AgentId"].ToString() + "</freetext>");
                XmlBuilder.Append("</remarks>");
                XmlBuilder.Append("</miscellaneousRemark>");
                XmlBuilder.Append("</dataElementsIndiv>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<reference>");
                XmlBuilder.Append("<qualifier>OT</qualifier>");
                XmlBuilder.Append("<number>2</number>");
                XmlBuilder.Append("</reference>");
                XmlBuilder.Append("<segmentName>FM</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<commission>");
                XmlBuilder.Append("<passengerType>PAX</passengerType>");
                XmlBuilder.Append("<indicator>FM</indicator>");
                XmlBuilder.Append("<commissionInfo>");
                XmlBuilder.Append("<percentage>" + FltDsGAL.Tables[0].Rows[0]["IATAComm"].ToString() + "</percentage>");//will come from database
                XmlBuilder.Append("</commissionInfo>");
                XmlBuilder.Append("</commission>");
                XmlBuilder.Append("</dataElementsIndiv>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<segmentName>OS</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<freetextData>");
                XmlBuilder.Append("<freetextDetail>");
                XmlBuilder.Append("<subjectQualifier>3</subjectQualifier>");
                XmlBuilder.Append("<companyId>YY</companyId>");
                XmlBuilder.Append("</freetextDetail>");
                XmlBuilder.Append("<longFreetext>Mobile-" + FltHdr.Tables[0].Rows[0]["PgMobile"].ToString() + "</longFreetext>");
                XmlBuilder.Append("</freetextData>");
                XmlBuilder.Append("</dataElementsIndiv>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<reference>");
                XmlBuilder.Append("<qualifier>OT</qualifier>");
                XmlBuilder.Append("<number>1</number>");
                XmlBuilder.Append("</reference>");
                XmlBuilder.Append("<segmentName>FV</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<ticketingCarrier>");
                XmlBuilder.Append("<carrier>");
                XmlBuilder.Append("<airlineCode>" + FltHdr.Tables[0].Rows[0]["VC"].ToString() + "</airlineCode>");
                XmlBuilder.Append("</carrier>");
                XmlBuilder.Append("</ticketingCarrier>");
                XmlBuilder.Append("</dataElementsIndiv>");
                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<reference>");
                XmlBuilder.Append("<qualifier>OT</qualifier>");
                XmlBuilder.Append("<number>5</number>");
                XmlBuilder.Append("</reference>");
                XmlBuilder.Append("<segmentName>TK</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<ticketElement>");
                XmlBuilder.Append("<passengerType>PAX</passengerType>");
                XmlBuilder.Append("<ticket>");
                XmlBuilder.Append("<indicator>OK</indicator>");
                XmlBuilder.Append("</ticket>");
                XmlBuilder.Append("</ticketElement>");
                XmlBuilder.Append("</dataElementsIndiv>");
                //Meal,Seat,FFAirline Req
                try
                {
                    for (int ff = 0; ff <= PaxDs.Tables[0].Rows.Count - 1; ff++)
                    {
                        if (!string.IsNullOrEmpty(PaxDs.Tables[0].Rows[ff]["FFNumber"].ToString().Trim()))
                        {
                            XmlBuilder.Append("<dataElementsIndiv><elementManagementData><reference><qualifier>OT</qualifier><number>2</number></reference><segmentName>SSR</segmentName></elementManagementData><serviceRequest><ssr> <type>FQTV</type><companyId>" + PaxDs.Tables[0].Rows[ff]["FFAirline"].ToString().Trim() + "</companyId><indicator>P01</indicator></ssr></serviceRequest><frequentTravellerData><frequentTraveller><companyId>" + PaxDs.Tables[0].Rows[ff]["FFAirline"].ToString().Trim() + "</companyId><membershipNumber>" + PaxDs.Tables[0].Rows[ff]["FFAirline"].ToString().Trim() + PaxDs.Tables[0].Rows[ff]["FFNumber"].ToString().Trim() + "</membershipNumber></frequentTraveller></frequentTravellerData><referenceForDataElement><reference><qualifier>PR</qualifier><number>" + ff + 1 + "</number></reference></referenceForDataElement></dataElementsIndiv>");
                        }
                        if (PaxDs.Tables[0].Rows[ff]["PaxType"].ToString().Trim() == "ADT" && PaxDs.Tables[0].Rows[ff]["SeatType"].ToString().Trim() != "")
                        {
                            XmlBuilder.Append("<dataElementsIndiv><elementManagementData><segmentName>STR</segmentName> </elementManagementData><seatGroup><seatRequest><special><seatType>" + PaxDs.Tables[0].Rows[AdtSeat]["SeatType"].ToString().Trim() + "</seatType></special></seatRequest></seatGroup><referenceForDataElement><reference><qualifier>PR</qualifier> <number>" + AdtSeat + 1 + "</number></reference><reference><qualifier>ST</qualifier> <number>1</number> </reference></referenceForDataElement></dataElementsIndiv>");
                            AdtSeat = AdtSeat + 1;
                        }
                        if (PaxDs.Tables[0].Rows[ff]["PaxType"].ToString().Trim() == "CHD" && PaxDs.Tables[0].Rows[ff]["SeatType"].ToString().Trim() != "")
                        {
                            XmlBuilder.Append("<dataElementsIndiv><elementManagementData><segmentName>STR</segmentName> </elementManagementData><seatGroup><seatRequest><special><seatType>" + PaxDs.Tables[0].Rows[AdtSeat]["SeatType"].ToString().Trim() + "</seatType></special></seatRequest></seatGroup><referenceForDataElement><reference><qualifier>PR</qualifier> <number>" + ChdSeat + 1 + "</number></reference><reference><qualifier>ST</qualifier> <number>1</number> </reference></referenceForDataElement></dataElementsIndiv>");
                            ChdSeat = ChdSeat + 1;
                        }
                        if (PaxDs.Tables[0].Rows[ff]["PaxType"].ToString().Trim() == "ADT" && PaxDs.Tables[0].Rows[ff]["MealType"].ToString().Trim() != "")
                        {
                            XmlBuilder.Append("<dataElementsIndiv><elementManagementData><reference><qualifier>OT</qualifier><number>280</number></reference><segmentName>SSR</segmentName></elementManagementData><serviceRequest><ssr><type>" + PaxDs.Tables[0].Rows[AdtMeal]["MealType"].ToString().Trim() + "</type><status>NN</status><quantity>1</quantity><companyId>YY</companyId><indicator>P01</indicator></ssr></serviceRequest><referenceForDataElement><reference><qualifier>PR</qualifier><number>" + AdtMeal + 1 + "</number></reference></referenceForDataElement></dataElementsIndiv>");
                            AdtMeal = AdtMeal + 1;
                        }
                        if (PaxDs.Tables[0].Rows[ff]["PaxType"].ToString().Trim() == "CHD" && PaxDs.Tables[0].Rows[ff]["MealType"].ToString().Trim() != "")
                        {
                            XmlBuilder.Append("<dataElementsIndiv><elementManagementData><reference><qualifier>OT</qualifier><number>280</number></reference><segmentName>SSR</segmentName></elementManagementData><serviceRequest><ssr><type>" + PaxDs.Tables[0].Rows[ChdMeal]["SeatType"].ToString().Trim() + "</type><status>NN</status><quantity>1</quantity><companyId>YY</companyId><indicator>P01</indicator></ssr></serviceRequest><referenceForDataElement><reference><qualifier>PR</qualifier><number>" + ChdMeal + 1 + "</number></reference></referenceForDataElement></dataElementsIndiv>");
                            ChdMeal = ChdMeal + 1;
                        }

                    }

                }
                catch (Exception ex)
                {

                }

                //try
                //{
                //    if (FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                //    {
                //        DataTable dt = new DataTable();
                //        dt = BAL_IA.GetTicketingCrd(FltHdr.Tables[0].Rows[0]["VC"].ToString());
                //        bool OT = false;
                //        if (dt.Rows.Count > 0)
                //            OT = Convert.ToBoolean(dt.Rows[0]["OnlineTkt"].ToString());
                //        if (OT == true && dt.Rows[0]["Office_ID"].ToString().Trim().ToUpper() != "DELVS32VB")
                //        {
                //            string TodayDate = DateTime.Now.ToString("MMddyy");
                //            XmlBuilder.Append("<dataElementsIndiv><elementManagementData><segmentName>ES</segmentName></elementManagementData><pnrSecurity><security><identification>" + dt.Rows[0]["Office_ID"].ToString() + "</identification><accessMode>B</accessMode></security><securityInfo><creationDate>" + TodayDate + "</creationDate><agentCode>AASU</agentCode></securityInfo></pnrSecurity></dataElementsIndiv>");

                //        }
                //    }
                //}
                //catch (Exception ex)
                //{

                //}

                XmlBuilder.Append("<dataElementsIndiv>");
                XmlBuilder.Append("<elementManagementData>");
                XmlBuilder.Append("<segmentName>FP</segmentName>");
                XmlBuilder.Append("</elementManagementData>");
                XmlBuilder.Append("<formOfPayment>");
                try
                {
                    DataTable FOPDt = new DataTable();
                    FOPDt = Data.GetFOP(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString, FltHdr.Tables[0].Rows[0]["VC"].ToString().Trim(), FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim(), "1A");
                    StringBuilder XmlBuilder1 = new StringBuilder();
                    if (FOPDt.Rows[0]["FOP"].ToString().Trim().ToUpper() == "CA")
                    {
                        XmlBuilder1.Append("<fop>");
                        XmlBuilder1.Append("<identification>CA</identification>");
                        XmlBuilder1.Append("</fop>");
                    }
                    else if (FOPDt.Rows[0]["FOP"].ToString().Trim().ToUpper() == "CC")
                    {
                        XmlBuilder1.Append("<fop>");
                        XmlBuilder1.Append("<identification>" + FOPDt.Rows[0]["FOP"].ToString().Trim().ToUpper() + "</identification>");
                        XmlBuilder1.Append("<creditCardCode>" + FOPDt.Rows[0]["CardType"].ToString().Trim().ToUpper() + "</creditCardCode>");
                        XmlBuilder1.Append("<accountNumber>" + FOPDt.Rows[0]["CardNumber"].ToString().Trim().ToUpper() + "</accountNumber>");
                        XmlBuilder1.Append("<expiryDate>" + FOPDt.Rows[0]["CardExpDate"].ToString().Trim().ToUpper() + "</expiryDate>");
                        XmlBuilder1.Append("<currencyCode>INR</currencyCode>");
                        XmlBuilder1.Append("</fop>");
                    }
                    XmlBuilder.Append(XmlBuilder1.ToString());
                }
                catch (Exception ex)
                {
                    XmlBuilder.Append("<fop>");
                    XmlBuilder.Append("<identification>CA</identification>");
                    XmlBuilder.Append("</fop>");
                }
                XmlBuilder.Append("</formOfPayment>");
                XmlBuilder.Append("</dataElementsIndiv>");
                if (FltHdr.Tables[0].Rows[0]["Trip"].ToString().Trim().ToUpper() == "D")
                {
                    string strCode = "";
                    STD.DAL.FlightCommonDAL objFltCommonDal = new STD.DAL.FlightCommonDAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
                    try
                    {
                        strCode = objFltCommonDal.GetDealCodeFromDB(FltHdr.Tables[0].Rows[0]["OrderId"].ToString());
                    }
                    catch
                    { }
                    if (strCode.Contains("TKT") && (strCode.Contains("DEALCODE") || strCode.Contains("TOURCODE")))
                    {

                        XmlBuilder.Append("<dataElementsIndiv><elementManagementData><segmentName>FT</segmentName></elementManagementData><tourCode><netRemit><indicator>N</indicator><freetext>" + Utility.Split(strCode, "/")[2].ToString().Trim() + "</freetext></netRemit></tourCode></dataElementsIndiv>");
                    }
                }
                XmlBuilder.Append("</dataElementsMaster>");
                XmlBuilder.Append("</PNR_AddMultiElements>");
                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                ADDMELEXML = ADDMELEXML + XmlBuilder.ToString();
                HSREQRES.Add("ADDMULTI_REQ", ADDMELEXML);
                admultielement = PostXml(ADDMELEXML, SoapAction);
                HSREQRES.Add("ADDMULTI_RES", admultielement);
                if (admultielement == "")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), "", "", "", "", "", "", "", "");
                    HSREQRES.Add("ADDMULTI_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (admultielement.Contains("<errorText>") == true)
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), "", "", "", "", "", "", "");
                    HSREQRES.Add("ADDMULTI_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (admultielement.Substring(0, 5) == "Error")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), "", "", "", "", "", "", "", HSREQRES["ADDMULTI_RES"].ToString());
                    HSREQRES.Add("ADDMULTI_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else
                {
                    HSREQRES.Add("ADDMULTI_STATUS", "TRUE");
                }
            }
            catch (Exception ex)
            {

                admultielement = "";
                HSREQRES.Add("ADDMULTI_STATUS", "FALSE");
                BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), "", "", "", "", "", "", "", ex.ToString());
                sessionid20 = GetIncrementedSessionId(sessionid);
                PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
            }
            //}
            //catch (Exception ex)
            //{
            //    admultielement = "Error: " + ex;
            //}
            return admultielement;
        }
        #endregion AddMultiElements
        #region Fare_PricePNRWithBookingClass
        public string Fare_PricePNRWithBookingClass(string sessionid, string SoapAction, string AirlineCode, DataSet FltDsGAL, string Pax_Res, string username, string officeID, string pass, string svcUrl, IncludeSessionType includeSessionType)
        {
            SoapAction = "http://webservices.amadeus.com/TPCBRQ_18_1_1A";
            string PriceRes = "";
            AmdSession objSessuonHeader = new AmdSession();
            try
            {
                if (FltDsGAL.Tables[0].Rows[0]["Trip"].ToString().ToUpper() == "S")
                {
                    //StringBuilder XmlBuilder = new StringBuilder();
                    //XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                    ////XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                    //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:tpc='http://xml.amadeus.com/TPCBRQ_07_3_1A'>");
                    //XmlBuilder.Append("<soapenv:Header>");
                    //XmlBuilder.Append(GetSessionHeader(sessionid)); //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                    //XmlBuilder.Append("</soapenv:Header>");
                    //XmlBuilder.Append("<soapenv:Body>");
                    StringBuilder XmlBuilder = new StringBuilder();
                    XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                    //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                    XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:tpc='http://xml.amadeus.com/TPCBRQ_07_3_1A'>");
                    XmlBuilder.Append("<soapenv:Header>");
                    XmlBuilder.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));//GetSessionHeader(sessionid)); //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                    XmlBuilder.Append("</soapenv:Header>");
                    XmlBuilder.Append("<soapenv:Body>");
                    XmlBuilder.Append("<Fare_PricePNRWithBookingClass>");
                    XmlBuilder.Append("<overrideInformation>");
                    XmlBuilder.Append("<attributeDetails>");
                    XmlBuilder.Append("<attributeType>RP</attributeType>");
                    XmlBuilder.Append("</attributeDetails>");
                    XmlBuilder.Append("<attributeDetails>");
                    XmlBuilder.Append("<attributeType>RU</attributeType>");
                    XmlBuilder.Append("</attributeDetails>");
                    XmlBuilder.Append("<attributeDetails>");
                    XmlBuilder.Append("<attributeType>RLO</attributeType>");
                    XmlBuilder.Append("</attributeDetails>");
                    XmlBuilder.Append("</overrideInformation>");
                    XmlBuilder.Append("<validatingCarrier>");
                    XmlBuilder.Append("<carrierInformation>");
                    XmlBuilder.Append("<carrierCode>" + AirlineCode + "</carrierCode>");
                    XmlBuilder.Append("</carrierInformation>");
                    XmlBuilder.Append("</validatingCarrier>");
                    XmlBuilder.Append("</Fare_PricePNRWithBookingClass>");
                    XmlBuilder.Append("</soapenv:Body>");
                    XmlBuilder.Append("</soapenv:Envelope>");
                    HSREQRES.Add("PRICE_REQ", XmlBuilder.ToString());
                    PriceRes = PostXml(XmlBuilder.ToString(), SoapAction);
                    HSREQRES.Add("PRICE_RES", PriceRes.ToString());
                }
                else
                {
                    Hashtable HSPrice = new Hashtable();
                    PricingWithFBA GAP = new PricingWithFBA();
                    HSPrice = GAP.Pricing_FBA(sessionid, SoapAction, AirlineCode, FltDsGAL, Pax_Res, username, officeID, pass, svcUrl, includeSessionType);
                    PriceRes = HSPrice["fare_res"].ToString();
                    HSREQRES.Add("PRICE_REQ", HSPrice["fare_req"].ToString());
                    HSREQRES.Add("PRICE_RES", HSPrice["fare_res"].ToString());
                }
                if (PriceRes == "")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), "", "", "", "", "", "");
                    HSREQRES.Add("PRICE_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE);// PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (PriceRes.Contains("<errorText>") == true)
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), "", "", "", "", "");
                    HSREQRES.Add("PRICE_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (PriceRes.Substring(0, 5) == "Error")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), "", "", "", "", "", HSREQRES["PRICE_RES"].ToString());
                    HSREQRES.Add("PRICE_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (PriceRes.Contains("UNABLE TO FARE") == true)
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), "", "", "", "", "", HSREQRES["PRICE_RES"].ToString());
                    HSREQRES.Add("PRICE_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else
                {
                    HSREQRES.Add("PRICE_STATUS", "TRUE");
                }
            }
            catch (Exception ex)
            {

                PriceRes = "";
                HSREQRES.Add("PRICE_STATUS", "FALSE");
                BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), "", "", "", "", "", ex.ToString());
                sessionid20 = GetIncrementedSessionId(sessionid);
                PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
            }
            //}
            //catch (Exception ex)
            //{
            //    PriceRes = "Error: " + ex;
            //}

            return PriceRes;
        }
        #endregion Fare_PricePNRWithBookingClass
        #region PNR_AddMultiElements11
        public string PNR_AddMultiElements11(string sessionid, string SoapAction, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl)
        {
            string ResAME11 = "";
            AmdSession objSessuonHeader = new AmdSession();
            SoapAction = "http://webservices.amadeus.com/PNRADD_17_1_1A";
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));//GetSessionHeader(sessionid));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
                //XmlBuilder.Append("<soapenv:Header>");
                //XmlBuilder.Append(GetSessionHeader(sessionid));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                //XmlBuilder.Append("</soapenv:Header>");
                //XmlBuilder.Append("<soapenv:Body>");

                XmlBuilder.Append("<PNR_AddMultiElements>");
                XmlBuilder.Append("<pnrActions>");
                XmlBuilder.Append("<optionCode>11</optionCode>");
                XmlBuilder.Append("</pnrActions>");
                XmlBuilder.Append("</PNR_AddMultiElements>");

                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                HSREQRES.Add("PNR_REQ", XmlBuilder.ToString());
                ResAME11 = PostXml(XmlBuilder.ToString(), SoapAction);
                HSREQRES.Add("PNR_RES", ResAME11);
                if (ResAME11 == "")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["TST_RES"].ToString(), HSREQRES["PNR_REQ"].ToString(), "", "");
                    HSREQRES.Add("PNR_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (ResAME11.Contains("<errorText>") == true)
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["TST_RES"].ToString(), HSREQRES["PNR_REQ"].ToString(), HSREQRES["PNR_RES"].ToString(), "");
                    HSREQRES.Add("PNR_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (ResAME11.Substring(0, 5) == "Error")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["TST_RES"].ToString(), HSREQRES["PNR_REQ"].ToString(), "", HSREQRES["PNR_RES"].ToString());
                    HSREQRES.Add("PNR_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else
                {
                    HSREQRES.Add("PNR_STATUS", "TRUE");
                }
            }
            catch (Exception ex)
            {

                ResAME11 = "";
                HSREQRES.Add("PNR_STATUS", "FALSE");
                BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["PNR_REQ"].ToString(), "", ex.ToString());
                sessionid20 = GetIncrementedSessionId(sessionid);
                PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
            }

            //}
            //catch (Exception ex)
            //{
            //    ResAME11 = "Error: " + ex;
            //}
            return ResAME11;


        }
        #endregion PNR_AddMultiElements11
        #region PNR_AddMultiElements11




        #endregion PNR_AddMultiElements11
        #region Ticket_CreateTSTFromPricing
        public string Ticket_CreateTSTFromPricing(string sessionid, string SoapAction, int Adult, int Child, int Infant, string AdtFare, string ChdFare, string InfFare, string PriceRes, string addmultiRes, string airline, string Trip, double SmsCharge, string username, string officeID, string pass, string svcUrl, IncludeSessionType includeSessionType, string orderid)
        {
            string ResTST = "";
            AmdSession objssnHdr = new AmdSession();
            SoapAction = "http://webservices.amadeus.com/TAUTCQ_04_1_1A";
            try
            {

                if (Trip == "D")
                {
                    string exep = "";
                    string folder = "MPTB/" + orderid + DateTime.Now.ToString("hh_mm_ss");
                    STD.BAL.TSTReq objTST = new TSTReq();
                    string ReqTST = objTST.getTstRequest(sessionid, SoapAction, Adult, Child, Infant, AdtFare, ChdFare, InfFare, PriceRes, addmultiRes, airline, SmsCharge, "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL, ref exep, folder);
                    if (ReqTST != "" && ReqTST != "INVALIDFARE")
                    {
                        HSREQRES.Add("TST_REQ", ReqTST);
                        ResTST = PostXml(ReqTST, SoapAction);
                        HSREQRES.Add("TST_RES", ResTST);
                    }
                    else
                    {
                        HSREQRES.Add("TST_REQ", ReqTST);
                        HSREQRES.Add("TST_RES", ResTST);
                    }
                }
                else
                {

                    StringBuilder XmlBuilder = new StringBuilder();
                    XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                    XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:fmp='http://xml.amadeus.com/TAUTCQ_04_1_1A'>");
                    XmlBuilder.Append("<soapenv:Header>");
                    XmlBuilder.Append(objssnHdr.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, IncludeSessionType.InSeries)); //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                    XmlBuilder.Append("</soapenv:Header>");
                    XmlBuilder.Append("<soapenv:Body>");
                    //StringBuilder XmlBuilder = new StringBuilder();
                    //XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                    //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:fmp='http://xml.amadeus.com/TAUTCQ_04_1_1A'>");
                    //XmlBuilder.Append("<soapenv:Header>");
                    //XmlBuilder.Append(GetSessionHeader(sessionid)); //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                    //XmlBuilder.Append("</soapenv:Header>");
                    //XmlBuilder.Append("<soapenv:Body>");
                    if ((Adult > 0) && (Child == 0) && (Infant == 0))
                    {
                        XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>1</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");
                    }
                    else if ((Adult > 0) && (Child > 0) && (Infant == 0))
                    {
                        XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>1</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>2</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");

                    }
                    else if (Adult > 0 && Child > 0 && Infant > 0)
                    {
                        XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>1</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>2</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>3</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");

                    }
                    else if (Adult > 0 && Child == 0 && Infant > 0)
                    {
                        XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>1</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");
                        XmlBuilder.Append("<psaList>");
                        XmlBuilder.Append("<itemReference>");
                        XmlBuilder.Append("<referenceType>TST</referenceType>");
                        XmlBuilder.Append("<uniqueReference>2</uniqueReference>");
                        XmlBuilder.Append("</itemReference>");
                        XmlBuilder.Append("</psaList>");


                    }
                    XmlBuilder.Append("</Ticket_CreateTSTFromPricing>");
                    XmlBuilder.Append("</soapenv:Body>");
                    XmlBuilder.Append("</soapenv:Envelope>");

                    HSREQRES.Add("TST_REQ", XmlBuilder.ToString());
                    ResTST = PostXml(XmlBuilder.ToString(), SoapAction);
                    HSREQRES.Add("TST_RES", ResTST);
                }
                if (ResTST == "")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), "", "", "", "");
                    HSREQRES.Add("TST_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (ResTST.Contains("<errorText>") == true)
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["TST_RES"].ToString(), "", "", "");
                    HSREQRES.Add("TST_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (ResTST.Substring(0, 5) == "Error")
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), "", "", "", HSREQRES["TST_RES"].ToString());
                    HSREQRES.Add("TST_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else if (ResTST.Contains("INVALID FARE") == true)
                {
                    BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), "", "", "", HSREQRES["TST_RES"].ToString());
                    HSREQRES.Add("TST_STATUS", "FALSE");
                    sessionid20 = GetIncrementedSessionId(sessionid);
                    PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");
                }
                else
                {
                    HSREQRES.Add("TST_STATUS", "TRUE");
                }
            }
            catch (Exception ex)
            {

                ResTST = "";
                HSREQRES.Add("TST_STATUS", "FALSE");
                BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), "", "", "", ex.ToString());
                sessionid20 = GetIncrementedSessionId(sessionid);
                PNR_AddMultiElements20(sessionid20, ListSAU[0].IGNORE); //PNR_AddMultiElements20(sessionid20, "http://webservices.amadeus.com/1ASIWMASSPT/PNRADD_08_2_1A");

            }

            return ResTST;
        }
        #endregion Ticket_CreateTSTFromPricing
        #region PNR_AddMultiElements21
        public string PNR_AddMultiElements21(string sessionid, string SoapAction)
        {
            string ResAME21 = "";
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                //XmlBuilder.Append("<soapenv:Header>");
                //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                //XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(GetSessionHeader(sessionid));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");

                XmlBuilder.Append("<PNR_AddMultiElements>");
                XmlBuilder.Append("<pnrActions>");
                XmlBuilder.Append("<optionCode>21</optionCode>");
                XmlBuilder.Append("</pnrActions>");
                XmlBuilder.Append("</PNR_AddMultiElements>");

                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                ResAME21 = PostXml(XmlBuilder.ToString(), SoapAction);

            }
            catch (Exception ex)
            {
                ResAME21 = "Error: " + ex;
            }
            return ResAME21;


        }
        #endregion PNR_AddMultiElements21
        #region PNR_AddMultiElements20
        public void PNR_AddMultiElements20(string sessionid, string SoapAction)
        {
            SoapAction = "http://webservices.amadeus.com/PNRADD_17_1_1A";
            string ResAME20 = "";
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                //XmlBuilder.Append("<soapenv:Header>");
                //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                //XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_17_1_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(GetSessionHeader(sessionid));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");

                XmlBuilder.Append("<PNR_AddMultiElements>");
                XmlBuilder.Append("<pnrActions>");
                XmlBuilder.Append("<optionCode>20</optionCode>");
                XmlBuilder.Append("</pnrActions>");
                XmlBuilder.Append("</PNR_AddMultiElements>");
                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                ResAME20 = PostXml(XmlBuilder.ToString(), SoapAction);

            }
            catch (Exception ex)
            {

                ResAME20 = "";
                //HSREQRES.Add("PNR_STATUS", "FALSE");
                //BAL_IA.InsertGDSBookingLogs(HSREQRES["ORDERID"].ToString(), "", HSREQRES["SELL_REQ"].ToString(), HSREQRES["SELL_RES"].ToString(), HSREQRES["ADDMULTI_REQ"].ToString(), HSREQRES["ADDMULTI_RES"].ToString(), HSREQRES["PRICE_REQ"].ToString(), HSREQRES["PRICE_RES"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["TST_REQ"].ToString(), HSREQRES["PNR_REQ"].ToString(), "", ex.ToString());

            }

            //}
            //catch (Exception ex)
            //{
            //    ResAME11 = "Error: " + ex;
            //}
            //return ResAME11;


        }
        #endregion PNR_AddMultiElements20
        #region SignOut
        public string SignOut(string SoapAction, string sessionid)
        {
            SoapAction = "http://webservices.amadeus.com/VLSSOQ_04_1_1A";
            string session = "";
            try
            {
                StringBuilder SOUT = new StringBuilder();
                SOUT.Append("<?xml version='1.0' encoding='utf-8'?>");
                SOUT.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:vls='http://xml.amadeus.com/VLSSOQ_04_1_1A'>");
                //SOUT.Append("<soap:Header xmlns='http://webservices.amadeus.com/definitions'>");
                //SOUT.Append("<SessionId>" + sessionid + "</SessionId>");
                //SOUT.Append("</soap:Header>");
                SOUT.Append("<soapenv:Header>");
                SOUT.Append(GetSessionHeader(sessionid));//XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                SOUT.Append("</soapenv:Header>");
                SOUT.Append("<soapenv:Body>");
                SOUT.Append("<Security_SignOut/>");
                SOUT.Append("</soapenv:Body>");
                SOUT.Append("</soapenv:Envelope>");
                PostXml(SOUT.ToString(), SoapAction);// "http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A"    

                //HSREQRES.Add("SIGNIN_REQ", SOUT.ToString());
                //HSREQRES.Add("SIGNIN_RES", strRes);
            }
            catch (Exception ex)
            {
                session = "";

            }

            return session;
        }
        #endregion End SignOut
        #region PostXML
        public static string PostXml(String XMLIn, String Module_Version)
        {
            string rawResponse = string.Empty;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(XMLIn);
                //  HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://production.webservices.amadeus.com");               
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://nodeD1.test.webservices.amadeus.com/1ASIWPREP4A");
                request.Headers.Add(String.Format("SOAPAction: \"{0}\"", Module_Version));
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = @"text/xml;charset=utf-8";
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Timeout = 3000000;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    rawResponse = GetWebResponseString(response);
                    return rawResponse;
                }
            }
            catch (WebException wex)
            {
                // We issued an HttpWebRequest, so the response will be an HttpWebResponse.
                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;

                //// Grab the response stream.           
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        // Grab the body of the response as a string for parsing.
                        rawResponse = GetWebResponseString(httpResponse);
                        //IBE.Shared.LogUtility.LogInfo(rawResponse, "SoapFaults", IBE.Shared.Utilities.GetCurrentTimestamp());[commented HS 11-10-2011]
                        //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, wex, "698", rawResponse, string.Empty, "");
                        //throw wex;
                    }
                }
            }

            catch (Exception ex)
            {
                //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, ex, "699", rawResponse, string.Empty, "");
                rawResponse = "Error: " + ex;
            }
            return rawResponse;
        }
        #endregion PostXML
        #region GetWebResponseString
        private static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            Stream streamResponse;
            using (streamResponse = myHttpWebResponse.GetResponseStream())
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                using (StreamReader streamRead = new StreamReader(streamResponse))
                {
                    Char[] readBuffer = new Char[256];
                    int count = streamRead.Read(readBuffer, 0, 256);

                    while (count > 0)
                    {
                        String resultData = new String(readBuffer, 0, count);
                        rawResponse.Append(resultData);
                        count = streamRead.Read(readBuffer, 0, 256);
                    }
                }
            }
            return rawResponse.ToString();
        }
        #endregion GetWebResponseString
        #region GetIncrementedSessionId
        private string GetIncrementedSessionId(string SId)
        {
            string[] SID = SId.Split('|');
            string SessionId = SID[0] + "|" + Convert.ToInt32(Convert.ToInt32(SID[1]) + 1) + "|" + SID[2];
            return SessionId;

        }
        #endregion GetIncrementedSessionId
        #region sell_recom_chk

        public string sell_recom_chk(string Sell_Res)
        {
            string flg = "True";
            string x = null;
            string result1 = null;
            int tot = 0;
            try
            {
                System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
                xmldoc.LoadXml(Sell_Res);

                StringWriter sw = new StringWriter();
                XmlTextWriter xw = new XmlTextWriter(sw);
                xmldoc.WriteTo(xw);
                x = sw.ToString();
                result1 = x;
                result1 = result1.Replace("<", "");
                result1 = result1.Replace(">", "");
                result1 = result1.Replace("/", "");

                string[] seg_info = Utility.Split(result1.ToString(), "segmentInformation");
                tot = seg_info.Length;
                int i = 0;
                string[] status = null;
                ArrayList code = new ArrayList();
                for (i = 1; i <= tot - 1; i += 2)
                {
                    status = Utility.Split(seg_info[i], "statusCode");
                    if (status[1].ToString() != "OK")
                    {
                        flg = "False";
                    }
                }
            }
            catch (Exception ex)
            {
                return flg = "False";
            }
            return flg;
        }
        #endregion sell_recom_chk
        #region MonthConverter
        public string datecon(string MM)
        {
            string mm_str = "";
            switch (MM)
            {
                case "01":
                    mm_str = "JAN";
                    break;
                case "02":
                    mm_str = "FEB";
                    break;
                case "03":
                    mm_str = "MAR";
                    break;
                case "04":
                    mm_str = "APR";
                    break;
                case "05":
                    mm_str = "MAY";
                    break;
                case "06":
                    mm_str = "JUN";
                    break;
                case "07":
                    mm_str = "JUL";
                    break;
                case "08":
                    mm_str = "AUG";
                    break;
                case "09":
                    mm_str = "SEP";
                    break;
                case "10":
                    mm_str = "OCT";
                    break;
                case "11":
                    mm_str = "NOV";
                    break;
                case "12":
                    mm_str = "DEC";
                    break;
                default:

                    break;
            }

            return mm_str;

        }
        #endregion MonthConverter
    }

}
