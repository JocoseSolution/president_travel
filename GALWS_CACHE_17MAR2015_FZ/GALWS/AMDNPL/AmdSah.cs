﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.Shared
{
   //public  class AmdSah
   // {

   // }
    public class SoapActionUrl
    {
        public string SIGNIN { get; set; }
        public string SELL { get; set; }
        public string ADDPAX { get; set; }
        public string PRICING { get; set; }
        public string TST { get; set; }
        public string GDSPNR { get; set; }
        public string AIRLINEPNR { get; set; }
        public string IGNORE { get; set; }
        public string SIGNOUT { get; set; }
        public string CRYPTIC { get; set; }
        public string DOCISSUANCE { get; set; }
    }
    public class TicketingCrd
    {
        public string Corporate_ID { get; set; }
        public string Office_ID { get; set; }
        public string Password { get; set; }
        public bool OnlineTkt { get; set; }
        public string SigninXML { get; set; }
    }
    public class Flights_1A
    {
        public string Seg_Ref { get; set; }
        public string Flt_Schdule { get; set; }
        public string Flt_ref { get; set; }
        public string Eft_Ref { get; set; }
        public string Mcx_Ref { get; set; }
        public string DateOfDeparture { get; set; }
        public string TimeOfDeparture { get; set; }
        public string DateOfArrival { get; set; }
        public string TimeOfArrival { get; set; }
        public string LocationId1 { get; set; }
        public string LocationId1T { get; set; }
        public string LocationId2 { get; set; }
        public string LocationId2T { get; set; }
        public string MarketingCarrier { get; set; }
        public string OperatingCarrier { get; set; }
        public string FlightNumber { get; set; }
        public string EquipmentType { get; set; }
        public string ElectronicTicketing { get; set; }
        public string ProductDetailQualifier { get; set; }
    }

    public class PaxFare_1A
    {
        public string Adult { get; set; }
        public string Child { get; set; }
        public string Inf { get; set; }
        public string AdultFare { get; set; }
        public string ChildFare { get; set; }
        public string InfantFare { get; set; }
        public string TimeOfDeparture { get; set; }
        public string DateOfArrival { get; set; }
        public string TimeOfArrival { get; set; }
        public string LocationId1 { get; set; }
        public string LocationId1T { get; set; }
        public string LocationId2 { get; set; }
        public string LocationId2T { get; set; }
        public string MarketingCarrier { get; set; }
        public string OperatingCarrier { get; set; }
        public string FlightNumber { get; set; }
        public string EquipmentType { get; set; }
        public string ElectronicTicketing { get; set; }
        public string ProductDetailQualifier { get; set; }
    }

    //Fare Details Node- For Each Segment
    public class LegFareDtls_1A
    {
        public int RecomNo { get; set; }
        public int SEGREF { get; set; }
        public string ADT_RBD { get; set; }
        public string CHD_RBD { get; set; }
        public string INF_RBD { get; set; }
        public string ADT_CABIN { get; set; }
        public string CHD_CABIN { get; set; }
        public string INF_CABIN { get; set; }
        public string ADT_AVLSTATUS { get; set; }
        public string CHD_AVLSTATUS { get; set; }
        public string INF_AVLSTATUS { get; set; }
        public string ADT_FAREBASIS { get; set; }
        public string CHD_FAREBASIS { get; set; }
        public string INF_FAREBASIS { get; set; }
        public string ADT_FARETYPE { get; set; }
        public string CHD_FARETYPE { get; set; }
        public string INF_FARETYPE { get; set; }
        //Common
        public string BREAKPOINT { get; set; }
        public string ADT_CNT { get; set; }
        public string CHD_CNT { get; set; }
        public string INF_CNT { get; set; }

    }
}
