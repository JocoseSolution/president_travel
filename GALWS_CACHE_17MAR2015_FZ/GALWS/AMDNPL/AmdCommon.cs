﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;
using System.Collections;
using System.Data;
using System.Threading.Tasks;
using STD.BAL;
using STD.DAL;
using ITZERRORLOG;
using STD.Shared;

namespace GALWS.AMDNPL
{
    public class AmdCommon
    {
        public string UserName { get; set; }
        public string OfficeID { get; set; }
        public string Pass { get; set; }
        public string ConnStr { get; set; }
        public AmdCommon(string username, string officeId, string pass, string coonectionString)
        {
            UserName = username;
            OfficeID = officeId;
            Pass = pass;
            ConnStr = coonectionString;
        }

        #region MasterPricerRequest
        public string MasterPricerRequest(FlightSearch obj, string SessionId, int NFlt, bool isInbound, string svcUrl, bool IsPrivateFare, DataTable PFCodeDt)
        {
            #region variables          

            string Dep = Utility.Left(obj.HidTxtDepCity, 3);
            string Arr = Utility.Left(obj.HidTxtArrCity, 3);

            //if (isInbound)
            //{
            //    Dep = Utility.Left(obj.HidTxtArrCity, 3);
            //    Arr = Utility.Left(obj.HidTxtDepCity, 3);
            //}

            string DepDate = Utility.Left(obj.DepDate, 2) + Utility.Mid(obj.DepDate, 3, 2) + Utility.Right(obj.DepDate, 2);
            string ArrDate = string.IsNullOrEmpty(obj.RetDate) == false ? Utility.Left(obj.RetDate, 2) + Utility.Mid(obj.RetDate, 3, 2) + Utility.Right(obj.RetDate, 2) : "";
            int segcount = 1;
            if ((obj.TripType == TripType.RoundTrip && obj.RTF == true && obj.Trip == Trip.D) || (obj.TripType == TripType.RoundTrip && obj.Trip == Trip.I))
                segcount = 2;
            StringBuilder Req = new StringBuilder();
            string Response = "";


            AmdSession objHeader = new AmdSession();
            #endregion
            try
            {
                Req.Append("<?xml version='1.0' encoding='utf-8'?>");
                Req.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:sec='http://xml.amadeus.com/2010/06/Security_v1' xmlns:typ='http://xml.amadeus.com/2010/06/Types_v1' xmlns:iat='http://www.iata.org/IATA/2007/00/IATA2010.1' xmlns:app='http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3' xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1' xmlns:ses='http://xml.amadeus.com/2010/06/Session_v3' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_18_1_1A'>");//http://xml.amadeus.com/FMPTBQ_17_3_1A
                Req.Append("<soapenv:Header>");
                Req.Append(objHeader.GetSessionHeader(SessionId, UserName, OfficeID, Pass,ServiceUrls.ActionMasterPricer, svcUrl, IncludeSessionType.NO));
                Req.Append("</soapenv:Header>");
                #region Body
                Req.Append("<soapenv:Body>");
                Req.Append("<Fare_MasterPricerTravelBoardSearch xmlns='" +ServiceUrls.ActionMasterPricer + "' >");//
                Req.Append("<numberOfUnit>");
                Req.Append("<unitNumberDetail>");
                Req.Append("<numberOfUnits>" + (obj.Adult + obj.Child) + "</numberOfUnits>");
                Req.Append("<typeOfUnit>PX</typeOfUnit>");
                Req.Append("</unitNumberDetail>");
                Req.Append("<unitNumberDetail>");
                Req.Append("<numberOfUnits>" + NFlt + "</numberOfUnits>");//No. of Flights
                Req.Append("<typeOfUnit>RC</typeOfUnit>");
                Req.Append("</unitNumberDetail>");
                Req.Append("</numberOfUnit>");
                #region Pax
                if (obj.Adult >= 1)
                {
                    Req.Append("<paxReference>");
                    Req.Append("<ptc>ADT</ptc>");
                    for (int i = 1; i <= obj.Adult; i++)
                    {
                        Req.Append("<traveller>");
                        Req.Append("<ref>" + i + "</ref>");
                        Req.Append("</traveller>");
                    }
                    Req.Append("</paxReference>");
                }
                if (obj.Child >= 1)
                {
                    Req.Append("<paxReference>");
                    Req.Append("<ptc>CH</ptc>");
                    for (int i = 1; i <= obj.Child; i++)
                    {
                        Req.Append("<traveller>");
                        Req.Append("<ref>" + (obj.Adult + i) + "</ref>");
                        Req.Append("</traveller>");
                    }
                    Req.Append("</paxReference>");
                }
                if (obj.Infant >= 1)
                {
                    Req.Append("<paxReference>");
                    Req.Append("<ptc>INF</ptc>");
                    for (int i = 1; i <= obj.Infant; i++)
                    {
                        Req.Append("<traveller>");
                        Req.Append("<ref>" + i + "</ref>");
                        Req.Append("<infantIndicator>" + i + "</infantIndicator>");
                        Req.Append("</traveller>");
                    }
                    Req.Append("</paxReference>");
                }
                #endregion
                #region FareOptions
                Req.Append("<fareOptions>");
                Req.Append("<pricingTickInfo>");
                Req.Append("<pricingTicketing>");
                if (IsPrivateFare)
                { Req.Append("<priceType>RW</priceType>"); }
                else
                {
                    Req.Append("<priceType>RP</priceType>");
                    Req.Append("<priceType>RU</priceType>");
                }
                Req.Append("<priceType>TAC</priceType>");
                Req.Append("<priceType>ET</priceType>");
            //    Req.Append("<priceType>MNR</priceType>");

                Req.Append("</pricingTicketing>");
                Req.Append("</pricingTickInfo>");



                if (IsPrivateFare)
                {

                    DataTable DtVC = new DataTable();
                    if (String.IsNullOrEmpty(obj.HidTxtAirLine.ToUpper().Trim()) == false)
                    {
                        try
                        {
                            FltDeal objDeal = new FltDeal(ConnStr);
                            // FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
                            DtVC = objDeal.GetCodeforPForDCFromDB("PF", obj.Trip.ToString(), obj.AgentType, obj.UID, obj.AirLine, "", obj.HidTxtDepCity.Split(',')[0], obj.HidTxtArrCity.Split(',')[0], obj.HidTxtDepCity.Split(',')[1], obj.HidTxtArrCity.Split(',')[1]);
                          //  DtVC = objFCDAL.GetCodeforPForDCFromDB_VC(obj.TCCode, obj.Trip.ToString(), "PA", obj.HidTxtAirLine.ToUpper().Trim());
                        }
                        catch (Exception ex)
                        {
                            ExecptionLogger.FileHandling("FlightBAL(1A_MPTB_Req)", "Error_001", ex, "FlightRequest");
                        }
                    }
                    if (DtVC.Rows.Count > 0)
                    {
                        Req.Append("<corporate>");
                        for (int i = 0; i < DtVC.Rows.Count; i++)
                        {
                            if (DtVC.Rows[i]["PCatagoryCode"].ToString().Contains("BSP"))
                            {

                                Req.Append("<corporateId>");
                                Req.Append("<corporateQualifier>RW</corporateQualifier>");
                                Req.Append("<identity>" + DtVC.Rows[i]["D_T_Code"].ToString() + "</identity>");
                                Req.Append(" </corporateId>");


                            }

                        }
                        Req.Append("</corporate>");

                    }
                    else
                    {
                        if (PFCodeDt.Rows.Count > 0)
                        {
                            Req.Append("<corporate>");
                            for (int i = 0; i < PFCodeDt.Rows.Count; i++)
                            {
                                if (PFCodeDt.Rows[i]["PCatagoryCode"].ToString().Contains("BSP"))
                                {

                                    Req.Append("<corporateId>");
                                    Req.Append("<corporateQualifier>RW</corporateQualifier>");
                                    Req.Append("<identity>" + PFCodeDt.Rows[i]["D_T_Code"].ToString() + "</identity>");
                                    Req.Append(" </corporateId>");


                                }
                            }

                            Req.Append("</corporate>");

                        }

                    }
                }





                Req.Append("</fareOptions>");
                #endregion
                Req.Append("<travelFlightInfo>");
                ;

                if (obj.Cabin != "")
                {
                    Req.Append("<cabinId>");
                    Req.Append("<cabin>" + (obj.Cabin == "B" ? "C" : obj.Cabin) + "</cabin>");
                    Req.Append("</cabinId>");
                }
                else
                {
                    Req.Append("<cabinId>");
                    Req.Append("<cabin>Y</cabin>");
                    Req.Append("</cabinId>");
                }
                if (obj.HidTxtAirLine.Length > 1)
                {
                    Req.Append("<companyIdentity>");
                    Req.Append("<carrierQualifier>M</carrierQualifier>");
                    Req.Append("<carrierId>" + Utility.Right(obj.HidTxtAirLine, 2) + "</carrierId>");
                    Req.Append("</companyIdentity>");
                }
                else
                {
                    Req.Append("<companyIdentity>");
                    Req.Append("<carrierQualifier>X</carrierQualifier>");
                    Req.Append("<carrierId>H1</carrierId>");
                    Req.Append("</companyIdentity>");
                }


                Req.Append("</travelFlightInfo>");
                #region itinerary
                int j = 1;
                while (j <= segcount)
                {
                    string x = ""; //For reversing
                    if (j == 2 || isInbound == true)
                    {
                        x = Dep;
                        Dep = Arr;
                        Arr = x;
                        DepDate = ArrDate;
                    }
                    Req.Append("<itinerary>");
                    Req.Append("<requestedSegmentRef>");
                    Req.Append("<segRef>" + j + "</segRef>");
                    Req.Append("</requestedSegmentRef>");
                    Req.Append("<departureLocalization>");
                    Req.Append("<departurePoint>");
                    Req.Append("<locationId>" + Dep + "</locationId>");
                    Req.Append("</departurePoint>");
                    Req.Append("</departureLocalization>");
                    Req.Append("<arrivalLocalization>");
                    Req.Append("<arrivalPointDetails>");
                    Req.Append("<locationId>" + Arr + "</locationId>");
                    Req.Append("</arrivalPointDetails>");
                    Req.Append("</arrivalLocalization>");
                    Req.Append("<timeDetails>");
                    Req.Append("<firstDateTimeDetail>");
                    Req.Append("<timeQualifier>TD</timeQualifier>");
                    Req.Append("<date>" + DepDate + "</date>"); //251013
                    Req.Append("<time>1200</time>");
                    Req.Append("<timeWindow>12</timeWindow>");
                    Req.Append("</firstDateTimeDetail>");
                    Req.Append("</timeDetails>");
                    Req.Append("</itinerary>");
                    j++;
                }
                #endregion
                Req.Append("</Fare_MasterPricerTravelBoardSearch>");
                Req.Append("</soapenv:Body>");
                #endregion
                Req.Append("</soapenv:Envelope>");

                string folder = "MPTB/" + obj.Adult + obj.Child + obj.Infant + "_" + obj.HidTxtAirLine + "_" + obj.HidTxtDepCity + obj.HidTxtArrCity + obj.DepDate.Replace("/", "-") + "_" + obj.RetDate.Replace("/", "") + "_" + obj.Trip + "_" + obj.TripType + "_" + DateTime.Now.ToString("hh_mm_ss");

                Response = AmdUtility.PostXml(Req.ToString(),ServiceUrls.ActionMasterPricer, svcUrl, "MasterPricerRequest", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");
            }
            catch (Exception ex)
            {
            }
            //finally
            //{
            //    //AmdUtility.SaveFile(Req.ToString(), "MasterPricerRequest");
            //    //AmdUtility.SaveFile(Response, "MasterPricerRespose");

            //}
            return Response;
        }
        #endregion



        #region ParseFlightDetails
        public ArrayList ParseFltDetails(string Flt_Res)
        {
            ArrayList FltIndex = new ArrayList();
            XDocument main = XDocument.Load(new StringReader(Flt_Res));
            XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
            XNamespace ns1 = XNamespace.Get("http://xml.amadeus.com/FMPTBR_17_3_1A");
            var root = main.Descendants(soap + "Body").Descendants(ns1 + "flightIndex");
            #region
            if (root.Count() > 0) //Response doesn't Contain Error
            {
                try
                {
                    foreach (var r in root)
                    {
                        List<Flights_1A> Flt = new List<Flights_1A>();
                        string Seg_Ref = r.Element(ns1 + "requestedSegmentRef").Element(ns1 + "segRef").Value;
                        string Flt_ref = null;
                        string Eft_Ref = null;
                        string Mcx_Ref = null;
                        var Flts = r.Descendants(ns1 + "groupOfFlights");
                        foreach (var f in Flts)
                        {
                            #region propFlightGrDetail
                            Flt_ref = "S" + f.Element(ns1 + "propFlightGrDetail").Elements(ns1 + "flightProposal").ElementAt(0).Element(ns1 + "ref").Value;
                            Eft_Ref = f.Element(ns1 + "propFlightGrDetail").Elements(ns1 + "flightProposal").ElementAt(1).Element(ns1 + "ref").Value;
                            Mcx_Ref = f.Element(ns1 + "propFlightGrDetail").Elements(ns1 + "flightProposal").ElementAt(2).Element(ns1 + "ref").Value;
                            #endregion
                            #region flight Details
                            var legs = f.Descendants(ns1 + "flightDetails");
                            foreach (var l in legs)
                            {
                                Flights_1A obj = new Flights_1A();
                                obj.DateOfDeparture = l.Element(ns1 + "flightInformation").Element(ns1 + "productDateTime").Element(ns1 + "dateOfDeparture").Value;
                                obj.TimeOfDeparture = l.Element(ns1 + "flightInformation").Element(ns1 + "productDateTime").Element(ns1 + "timeOfDeparture").Value;
                                obj.DateOfArrival = l.Element(ns1 + "flightInformation").Element(ns1 + "productDateTime").Element(ns1 + "dateOfArrival").Value;
                                obj.TimeOfArrival = l.Element(ns1 + "flightInformation").Element(ns1 + "productDateTime").Element(ns1 + "timeOfArrival").Value;
                                obj.LocationId1 = l.Element(ns1 + "flightInformation").Elements(ns1 + "location").ElementAt(0).Element(ns1 + "locationId").Value;
                                if (l.Element(ns1 + "flightInformation").Elements(ns1 + "location").ElementAt(0).Element(ns1 + "terminal") != null)
                                {
                                    obj.LocationId1T = l.Element(ns1 + "flightInformation").Elements(ns1 + "location").ElementAt(0).Element(ns1 + "terminal").Value;
                                }
                                if (l.Element(ns1 + "flightInformation").Elements(ns1 + "location").ElementAt(1).Element(ns1 + "locationId") != null)
                                {
                                    obj.LocationId2 = l.Element(ns1 + "flightInformation").Elements(ns1 + "location").ElementAt(1).Element(ns1 + "locationId").Value;
                                }

                                obj.MarketingCarrier = l.Element(ns1 + "flightInformation").Element(ns1 + "companyId").Element(ns1 + "marketingCarrier").Value;
                                try
                                {
                                    if (l.Element(ns1 + "flightInformation").Element(ns1 + "companyId").Element(ns1 + "operatingCarrier") != null)
                                    {
                                        obj.OperatingCarrier = l.Element(ns1 + "flightInformation").Element(ns1 + "companyId").Element(ns1 + "operatingCarrier").Value;
                                    }
                                    else { obj.OperatingCarrier = ""; }
                                }
                                catch { obj.OperatingCarrier = ""; }
                                obj.FlightNumber = l.Element(ns1 + "flightInformation").Element(ns1 + "flightOrtrainNumber").Value;
                                obj.EquipmentType = l.Element(ns1 + "flightInformation").Element(ns1 + "productDetail").Element(ns1 + "equipmentType").Value;
                                obj.ElectronicTicketing = l.Element(ns1 + "flightInformation").Element(ns1 + "addProductDetail").Element(ns1 + "electronicTicketing").Value;
                                try
                                {
                                    obj.ProductDetailQualifier = l.Element(ns1 + "flightInformation").Element(ns1 + "addProductDetail").Element(ns1 + "productDetailQualifier").Value;

                                }
                                catch { obj.ProductDetailQualifier = ""; }
                                obj.Flt_ref = Flt_ref;
                                obj.Eft_Ref = Eft_Ref;
                                obj.Mcx_Ref = Mcx_Ref;
                                obj.Seg_Ref = Seg_Ref;
                                obj.ValidatingCarrier = Mcx_Ref;
                                Flt.Add(obj);
                            }

                            #endregion
                        }
                        FltIndex.Add(Flt);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion
            return FltIndex;
        }
        #endregion
        #region ParsePriceDetails
        public async Task<List<FlightSearchResults>> ParsePriceDetails(string Flt_Res, string JourneyType1, ArrayList Flights, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MarkupDs, FlightSearch searchInputs, List<MISCCharges> MiscList, bool inbound, bool isPrivateFare, CORP_ENTY_CONFIG CpOrEtConfig, int FltSecNum = 1)
        {
            XDocument main = XDocument.Load(new StringReader(Flt_Res));
            XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
            XNamespace ns1 = XNamespace.Get("http://xml.amadeus.com/FMPTBR_18_1_1A");//http://xml.amadeus.com/FMPTBR_17_3_1A
            var root = main.Descendants(soap + "Body").Descendants(ns1 + "recommendation");
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            #region Variables
            string FareRul = "";
            List<List<LegFareDtls_1A>> AllLeg = new List<List<LegFareDtls_1A>>();
            int StartLineNO = 1;
            bool farecnt = false;
            string MNR_XML = "";
            try
            {
                MNR_XML = XDocument.Parse(AmdUtility.RemoveAllNamespaces(main.Descendants(soap + "Body").Elements().FirstOrDefault()).ToString()).Element("Fare_MasterPricerTravelBoardSearchReply").Element("mnrGrp").ToString();
            }
            catch (Exception es) { }


            #endregion
            try
            {
                //Loop for each recomendation
                foreach (var r in root)
                {

                    #region Variables For Each Recommendation
                    int Rec_no = 0;
                    // string RefQ_NO1 = "";
                    int SegRefCount = 0;
                    int legcountOB = 0, legCountIB = 0;
                    ArrayList gpfare = new ArrayList();

                    string PtcTyp = "";
                    int PtcAdt = 0;
                    int PtcChd = 0;
                    int PtcInf = 0;
                    //Fare Variables
                    Hashtable Adt_FR = new Hashtable();
                    Hashtable Chd_FR = new Hashtable();
                    Hashtable Inf_FR = new Hashtable();
                    string Tot_Fare = "";
                    string Tot_Tax = "";
                    string Tot_YQ = "";
                    string Tot_YR = "";
                    string Tot_WO = "";
                    string Tot_IN = "";
                    string Tot_JN = "";
                    string Tot_Q = "";
                    string Pax_Tq = ""; //Transport Qualifier
                    string Pax_Ref = "";
                    string Pax_Yq = "";
                    string Pax_Yr = "";
                    string Pax_Q = "";
                    string Pax_WO = "";
                    string Pax_IN = "";
                    string Pax_JN = "";
                    string FareType = "";
                    string FareCode = "";

                    string BRefQualifierOne = "";
                    string BRefQualifierRound = "";
                    #endregion
                    List<LegFareDtls_1A> Seg1List = new List<LegFareDtls_1A>();
                    XElement s = r;
                    XDocument a = XDocument.Parse(AmdUtility.RemoveAllNamespaces(r).ToString());
                    Rec_no = int.Parse(a.Element("recommendation").Element("itemNumber").Element("itemNumberId").Element("number").Value);
                    #region recPriceInfo
                    var md = a.Element("recommendation").Element("recPriceInfo").Descendants("monetaryDetail");
                    decimal amt1 = 0, amt2 = 0;
                    string sliceAndDiceXML = "";

                    //if (a.Element("recommendation").Descendants("specificRecDetails").Any())
                    //{
                    //    a.Element("recommendation").Descendants("specificRecDetails").ToList().ForEach(x => { sliceAndDiceXML = sliceAndDiceXML + x.ToString(); });
                    //}
                    //else
                    //{
                    //    sliceAndDiceXML = "";
                    //}

                    foreach (var m in md)
                    {
                        string v = "";
                        if (m.Elements("amountType").Any() == true)// check if AmountType node exsists
                        {
                            v = m.Element("amountType").Value.Trim();
                            if (v == "YQ")
                                Tot_YQ = m.Element("amount").Value.Trim();
                            else if (v == "YR")
                                Tot_YR = m.Element("amount").Value.Trim();
                            else if (v == "WO")
                                Tot_WO = m.Element("amount").Value.Trim();
                            else if (v == "IN")
                                Tot_IN = m.Element("amount").Value.Trim();
                            else if (v == "JN")
                                Tot_JN = m.Element("amount").Value.Trim();
                            else if (v == "Q")
                                Tot_Q = m.Element("amount").Value.Trim();
                        }
                        else if (amt1 == 0)
                        {
                            amt1 = decimal.Parse(m.Element("amount").Value);
                        }
                        else
                        {
                            amt2 = decimal.Parse(m.Element("amount").Value);
                        }
                    }
                    if (amt1 > amt2)
                        Tot_Fare = amt1.ToString();
                    else Tot_Fare = amt2.ToString();
                    #endregion
                    #region segmentFlightRef  - Call it in Last to Filter Details and Prepare List
                    var SegFRef = a.Element("recommendation").Descendants("segmentFlightRef");




                    List<string> SegFRef_OB = new List<string>(); List<string> SegFRef_IB = new List<string>();

                    List<string> SegRef_SD = new List<string>();
                    SegRefCount = SegFRef.Count();
                    foreach (var sref in SegFRef)
                    {

                        for (int j = 0; j < sref.Elements("referencingDetail").Count(); j++) //Round Trip
                        {
                            if (j == 0)
                            {

                                SegFRef_OB.Add(sref.Elements("referencingDetail").ElementAt(j).Element("refQualifier").Value + sref.Elements("referencingDetail").ElementAt(j).Element("refNumber").Value);
                                if (sref.Elements("referencingDetail").Descendants("refQualifier").Where(x => x.Value == "A").Any())
                                {

                                    SegRef_SD.Add(sref.Elements("referencingDetail").Where(x => x.Element("refQualifier").Value == "A").FirstOrDefault().Element("refNumber").Value);
                                }
                                else
                                {
                                    SegRef_SD.Add("");
                                }
                            }
                            else if (j == 1) { SegFRef_IB.Add(sref.Elements("referencingDetail").ElementAt(j).Element("refQualifier").Value + sref.Elements("referencingDetail").ElementAt(j).Element("refNumber").Value); }
                        }
                    }
                    #endregion
                    #region PaxFareProduct  - Find Fare Per Pax
                    var pxfare = a.Element("recommendation").Descendants("paxFareProduct");
                    StringBuilder GroupFare = new StringBuilder();// For Extracting group Fare
                    GroupFare.Append("<?xml version='1.0' encoding='utf-16'?>").Append("<gp>");
                    foreach (var px in pxfare)
                    {
                        #region Evaluate Fare Per Pax
                        string paxcnt = px.Element("paxReference").Elements("traveller").Count().ToString();
                        #region paxFareDetail -- Find fare
                        Pax_Ref = px.Element("paxFareDetail").Element("paxFareNum").Value;
                        PtcTyp = px.Element("paxReference").Element("ptc").Value;
                        Pax_Tq = px.Element("paxFareDetail").Element("codeShareDetails").Element("transportStageQualifier").Value;
                        Tot_Fare = px.Element("paxFareDetail").Element("totalFareAmount").Value;
                        Tot_Tax = px.Element("paxFareDetail").Element("totalTaxAmount").Value;
                        try
                        { Pax_Yq = px.Element("paxFareDetail").Descendants("monetaryDetails").First(p => p.Element("amountType").Value == "YQ").Element("amount").Value; }
                        catch { Pax_Yq = "0"; }
                        try { Pax_Yr = px.Element("paxFareDetail").Descendants("monetaryDetails").First(p => p.Element("amountType").Value == "YR").Element("amount").Value; }
                        catch { Pax_Yr = "0"; }
                        try { Pax_Q = px.Element("paxFareDetail").Descendants("monetaryDetails").First(p => p.Element("amountType").Value == "Q").Element("amount").Value; }
                        catch { Pax_Q = "0"; }
                        try { Pax_WO = px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "WO").Element("rate").Value; }
                        catch { Pax_WO = "0"; }
                        try { Pax_IN = px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "IN").Element("rate").Value; }
                        catch { Pax_IN = "0"; }
                        try { Pax_JN = px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "K3").Element("rate").Value; }
                        catch { Pax_JN = "0"; }
                        try { Pax_IN = (Convert.ToInt16(Pax_IN) + Convert.ToInt16(px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "YM").Element("rate").Value.ToString())).ToString(); }
                        catch { }

                        FareType = px.Element("fare").Elements("pricingMessage").ElementAt(0).Element("description").Value;
                        FareCode = px.Element("fare").Elements("pricingMessage").Elements("freeTextQualification").ElementAt(0).Element("informationType").Value;
                        #endregion
                        if (PtcTyp == "ADT")
                        {
                            PtcAdt = px.Element("paxReference").Elements("traveller").Count();
                            Adt_FR.Add("Type", PtcTyp);
                            Adt_FR.Add("Pax_Tq", Pax_Tq);
                            Adt_FR.Add("Tot_Fare", Tot_Fare);
                            Adt_FR.Add("Tot_Tax", Tot_Tax);
                            Adt_FR.Add("Pax_Yq", Pax_Yq);
                            Adt_FR.Add("Pax_Yr", Pax_Yr);
                            Adt_FR.Add("Pax_Q", Pax_Q);
                            Adt_FR.Add("Pax_WO", Pax_WO);
                            Adt_FR.Add("Pax_IN", Pax_IN);
                            Adt_FR.Add("Pax_JN", Pax_JN);
                            Adt_FR.Add("Fare_Type", FareType);
                            Adt_FR.Add("Fare_Code", FareCode);
                        }
                        else if (PtcTyp == "CH")
                        {
                            PtcChd = px.Element("paxReference").Elements("traveller").Count();
                            Chd_FR.Add("Type", PtcTyp);
                            Chd_FR.Add("Pax_Tq", Pax_Tq);
                            Chd_FR.Add("Tot_Fare", Tot_Fare);
                            Chd_FR.Add("Tot_Tax", Tot_Tax);
                            Chd_FR.Add("Pax_Yq", Pax_Yq);
                            Chd_FR.Add("Pax_Yr", Pax_Yr);
                            Chd_FR.Add("Pax_Q", Pax_Q);
                            Chd_FR.Add("Pax_WO", Pax_WO);
                            Chd_FR.Add("Pax_IN", Pax_IN);
                            Chd_FR.Add("Pax_JN", Pax_JN);
                        }
                        else if (PtcTyp == "INF")
                        {
                            PtcInf = px.Element("paxReference").Elements("traveller").Count();
                            Inf_FR.Add("Type", PtcTyp);
                            Inf_FR.Add("Pax_Tq", Pax_Tq);
                            Inf_FR.Add("Tot_Fare", Tot_Fare);
                            Inf_FR.Add("Tot_Tax", Tot_Tax);
                            Inf_FR.Add("Pax_Yq", Pax_Yq);
                            Inf_FR.Add("Pax_Yr", Pax_Yr);
                            Inf_FR.Add("Pax_Q", Pax_Q);
                            Inf_FR.Add("Pax_WO", Pax_WO);
                            Inf_FR.Add("Pax_IN", Pax_IN);
                            Inf_FR.Add("Pax_JN", Pax_JN);
                        }
                        //FareRul = "-" + PtcTyp + "l-";
                        #region fare
                        //if (px.Elements("fare").Count() >= 2)
                        //{
                        //    farecnt = true;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("textSubjectQualifier").Value;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("informationType").Value;
                        //    FareRul = FareRul + "/" + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("description").Value + "-";
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(1).Element("pricingMessage").Element("freeTextQualification").Element("textSubjectQualifier").Value;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(1).Element("pricingMessage").Element("freeTextQualification").Element("informationType").Value;
                        //    FareRul = FareRul + "/" + px.Elements("fare").ElementAt(1).Element("pricingMessage").Element("description").Value + "-";
                        //}
                        //else
                        //{
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("textSubjectQualifier").Value;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("informationType").Value;
                        //    FareRul = FareRul + "/" + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("description").Value + "-";
                        //}
                        #endregion
                        #region fareDetails - Find rbd,etc - For Each Leg(<groupOfFares>)
                        int seg = 1;
                        if (JourneyType1 == "O")
                        {
                            GroupFare.Append("<Segment id='" + seg + "'>");
                            GroupFare.Append(System.String.Concat(px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").ToList()));
                            GroupFare.Append("</Segment>");
                            legcountOB = px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").Count(); //px.Element("fareDetails").Descendants("groupOfFares").Count();
                        }
                        else if (JourneyType1 == "R")
                        {

                            var fd = px.Descendants("fareDetails");
                            foreach (var f in fd)
                            {
                                GroupFare.Append("<Segment id='" + seg + "'>");
                                GroupFare = GroupFare.Append(string.Concat(px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").ToList()));
                                GroupFare.Append("</Segment>");
                                if (seg == 1) { legcountOB = px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").Count(); }
                                if (seg == 2) { legCountIB = px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").Count(); }
                                seg++;
                            }
                        }
                        #endregion
                        #endregion
                    }
                    GroupFare.Append("</gp>");
                    #endregion
                    #region Find Leg Details
                    List<LegFareDtls_1A> LegOB = null; List<LegFareDtls_1A> LegIB = null;
                    XDocument gn = XDocument.Parse(GroupFare.ToString());
                    if (JourneyType1 == "O")
                    {
                        IEnumerable<XElement> OB = from s1 in gn.Element("gp").Elements("Segment").Where(s1 => (string)s1.Attribute("id") == "1") select s1;
                        LegOB = GetLegDetails(OB, legcountOB, PtcAdt, PtcChd, PtcInf, Rec_no);

                    }
                    else if (JourneyType1 == "R")
                    {
                        IEnumerable<XElement> OB = from s1 in gn.Element("gp").Elements("Segment").Where(s1 => (string)s1.Attribute("id") == "1") select s1;
                        LegOB = GetLegDetails(OB, legcountOB, PtcAdt, PtcChd, PtcInf, Rec_no);
                        IEnumerable<XElement> IB = from s1 in gn.Element("gp").Elements("Segment").Where(s1 => (string)s1.Attribute("id") == "2") select s1;
                        LegIB = GetLegDetails(IB, legCountIB, PtcAdt, PtcChd, PtcInf, Rec_no);
                    }
                    #endregion
                    #region List Variables
                    Hashtable Markup = new Hashtable();
                    Hashtable CommGal = new Hashtable();//For Comm
                    Hashtable STXTFee = new Hashtable(); //For Srv. Tax
                    Hashtable TDS = new Hashtable(); //For TDS
                    string arrCity = "", depCity = "";
                    int schd = 0;
                    if (schd == 0)
                    {
                        depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                        arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                    }
                    else
                    {
                        depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                        arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                    }
                    List<ResListItem> FlightList = new List<ResListItem>();
                    #endregion
                    #region OneWay List
                    if (JourneyType1 == "O")
                    {
                        try
                        {
                            #region Map to Flights
                            //Find No of flights for this Fare
                            List<Flights_1A> FltOB = (List<Flights_1A>)Flights[0];
                            List<Flights_1A> flt = (from Flights_1A f in FltOB //(List<Flights_1A>)FltOB.Select(lgk => lgk.Flt_ref == seg).ToList();
                                                    from segv1 in SegFRef_OB
                                                    where f.Flt_ref == segv1
                                                    select f).ToList();
                            List<FlightSearchResults> Ob = new List<FlightSearchResults>();
                            Ob = AddLegstoFlt(LegOB, flt, null, null, SrvchargeList, CityList, AirList, MarkupDs, searchInputs, depCity, arrCity, Adt_FR, Chd_FR, Inf_FR, Rec_no, StartLineNO, MiscList, a.Element("recommendation").ToString(), Flt_Res, SegRef_SD, inbound, SegFRef_OB, SegFRef_IB, MNR_XML, isPrivateFare, CpOrEtConfig, FltSecNum);
                            foreach (FlightSearchResults f in Ob)
                            {

                                Final.Add(f);
                            }

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    #endregion
                    #region RoundTrip List
                    else if (JourneyType1 == "R")
                    {
                        #region Map to Flights
                        List<Flights_1A> FltOB = (List<Flights_1A>)Flights[0];
                        List<Flights_1A> flt1 =
                        (from Flights_1A f in FltOB
                         from segv1 in SegFRef_OB
                         where f.Flt_ref == segv1
                         select f).ToList();
                        #endregion
                        #region Map to Flights
                        List<Flights_1A> FltIB = (List<Flights_1A>)Flights[1];
                        List<Flights_1A> flt2 = (from Flights_1A f in FltIB
                                                 from segv2 in SegFRef_IB
                                                 where f.Flt_ref == segv2
                                                 select f).ToList();
                        #endregion
                        List<FlightSearchResults> FltList = new List<FlightSearchResults>();
                        FltList = AddLegstoFlt(LegOB, flt1, LegIB, flt2, SrvchargeList, CityList, AirList, MarkupDs, searchInputs, depCity, arrCity, Adt_FR, Chd_FR, Inf_FR, Rec_no, StartLineNO, MiscList, a.Element("recommendation").ToString(), Flt_Res, SegRef_SD, inbound, SegFRef_OB, SegFRef_IB, MNR_XML, isPrivateFare, CpOrEtConfig, FltSecNum);//.Where(p=>p.TripType=="R").ToList()
                        foreach (FlightSearchResults f in FltList)
                        {

                            Final.Add(f);
                        }
                    }
                    #endregion
                    StartLineNO = StartLineNO + SegRefCount;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            await Task.Delay(0);
            AmdUtility.SaveFile(SerializeAnObject(Final), System.DateTime.Now.ToString("ddMMyyyyHHmmss") + "_Req", "LogfileReq");
            FlightCommonBAL objFltComm = new FlightCommonBAL();
            return objFltComm.AddFlightKey(Final, searchInputs.GDSRTF); //Final;
        }
        #endregion
        #region Utility


        private float CalcMarkup(DataTable Mrkdt, string VC, double fare, string Trip)
        {
            DataRow[] airMrkArray;
            double mrkamt = 0;
            try
            {
                airMrkArray = Mrkdt.Select("AirlineCode='" + VC + "'", "");
                if (airMrkArray.Length > 0)
                {
                    if (Trip == "I")
                    {
                        if ((airMrkArray[0]["MarkupType"].ToString()) == "P")
                        {
                            mrkamt = Math.Round((fare * Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString())) / 100, 0);
                        }
                        else if ((airMrkArray[0]["MarkupType"].ToString()) == "F")
                        {
                            mrkamt = Convert.ToDouble(airMrkArray[0]["MarkupValue"].ToString());
                        }
                    }
                    else
                    {
                        mrkamt = Convert.ToDouble(airMrkArray[0]["MarkUp"].ToString());
                    }
                }
                else
                {
                    mrkamt = 0;
                }
            }
            catch (Exception ex)
            {
                mrkamt = 0;
            }
            return float.Parse(mrkamt.ToString());
        }

        private string getCityName(string city, List<FlightCityList> CityList)
        {
            string city1 = "";
            try
            {
                city1 = ((from ct in CityList where ct.AirportCode == city select ct).ToList())[0].City;
            }
            catch { city1 = city; }
            return city1;
        }

        private string getAirlineName(string airl, List<AirlineList> AirList)
        {
            string airl1 = "";
            try
            {
                airl1 = ((from ar in AirList where ar.AirlineCode == airl select ar).ToList())[0].AlilineName;
            }
            catch { airl1 = airl; }
            return airl1;
        }
        private Hashtable CalcSrvTaxTFeeTds(List<FltSrvChargeList> SrvchargeList, string VC, float Dis, float Basic, float YQ, string TDS)
        {
            decimal STaxP = 0;
            decimal TFeeP = 0;
            decimal IATAComm = 0;
            Hashtable STHT = new Hashtable();
            try
            {
                STaxP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).SrviceTax;
                TFeeP = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).TransactionFee;
                IATAComm = ((from st in SrvchargeList where st.AirlineCode == VC select st).ToList()[0]).IATACommissiom;
                STHT.Add("STax", Math.Round(((decimal.Parse(Dis.ToString()) * STaxP) / 100), 0));
                STHT.Add("TFee", Math.Round(((decimal.Parse((Basic + YQ).ToString()) * TFeeP) / 100), 0));
                STHT.Add("Tds", Math.Round(((double.Parse(Dis.ToString()) - double.Parse(STHT["STax"].ToString())) * double.Parse(TDS)) / 100, 0));
                STHT.Add("IATAComm", IATAComm);
            }
            catch
            {
                STHT.Add("STax", 0);
                STHT.Add("TFee", 0);
                STHT.Add("Tds", 0);
                STHT.Add("IATAComm", 0);
            }
            return STHT;
        }
        public void SaveXml(string Res, string name)
        {
            //string newFileName = Res + DateTime.Now.ToString("hh:mm:ss");
            //string newFileName = Res + "- " + Departure + "- " + Arrival + "-" + AgentID + "-" + DateTime.Now.ToString().Trim().Replace("/", "-");

            string activeDir = @"D:\Req_Res_1A\" + DateTime.Now.ToString("dd-MMMM-yyyy") + "/"; //verbatim string
            string path = activeDir + name;
            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }

            try
            {
                File.WriteAllText(path, Res);
            }
            catch (Exception ex)
            {
                //throw ex;
            }
        }
        #endregion
        #region LegDetails
        public List<LegFareDtls_1A> GetLegDetails(IEnumerable<XElement> OB, int legOB, int Adt, int Chd, int Inf, int recom)
        {
            List<LegFareDtls_1A> OB_leg = new List<LegFareDtls_1A>();
            //Per pax find legs
            try
            {
                #region Loop for Leg
                for (int i = 0; i < legOB; i++)
                {
                    LegFareDtls_1A leg = new LegFareDtls_1A();
                    leg.RecomNo = recom;
                    leg.SEGREF = int.Parse(OB.ElementAt(0).Attribute("id").Value);
                    leg.ADT_CNT = Adt.ToString();
                    leg.CHD_CNT = Chd.ToString();
                    leg.INF_CNT = Inf.ToString();
                    try
                    {
                        leg.Corporate_Code = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("corporateId").Value;
                    }
                    catch (Exception ex) { }
                    #region Adult
                    if (Adt > 0)
                    {
                        leg.ADT_RBD = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("rbd").Value;
                        leg.ADT_CABIN = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("cabin").Value;
                        leg.ADT_AVLSTATUS = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("avlStatus").Value;
                        leg.ADT_FAREBASIS = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareBasis").Value;
                        leg.ADT_FARETYPE = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareType").Value;
                        //Extra
                        leg.BREAKPOINT = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("breakPoint").Value;
                    }
                    #endregion
                    #region Child
                    if (Chd > 0 && Adt > 0)
                    {
                        leg.CHD_RBD = OB.ElementAt(1).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("rbd").Value;
                        leg.CHD_CABIN = OB.ElementAt(1).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("cabin").Value;
                        leg.CHD_AVLSTATUS = OB.ElementAt(1).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("avlStatus").Value;
                        leg.CHD_FAREBASIS = OB.ElementAt(1).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareBasis").Value;
                        leg.CHD_FARETYPE = OB.ElementAt(1).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareType").Value;
                        leg.CHD_BREAKPOINT = OB.ElementAt(1).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("breakPoint").Value;

                    }
                    else if (Chd > 0 && Adt == 0)
                    {
                        leg.CHD_RBD = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("rbd").Value;
                        leg.CHD_CABIN = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("cabin").Value;
                        leg.CHD_AVLSTATUS = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("avlStatus").Value;
                        leg.CHD_FAREBASIS = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareBasis").Value;
                        leg.CHD_FARETYPE = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareType").Value;
                        leg.CHD_BREAKPOINT = OB.ElementAt(0).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("breakPoint").Value;

                    }
                    else
                    {
                        leg.CHD_RBD = "";
                        leg.CHD_CABIN = "";
                        leg.CHD_AVLSTATUS = "";
                        leg.CHD_FAREBASIS = "";
                        leg.CHD_FARETYPE = "";
                        leg.CHD_BREAKPOINT = "";
                    }
                    #endregion
                    #region Infant
                    if (Inf > 0)
                    {
                        int index = Chd > 0 ? 2 : 1;

                        leg.INF_RBD = OB.ElementAt(index).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("rbd").Value;
                        leg.INF_CABIN = OB.ElementAt(index).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("cabin").Value;
                        leg.INF_AVLSTATUS = OB.ElementAt(index).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("cabinProduct").Element("avlStatus").Value;
                        leg.INF_FAREBASIS = OB.ElementAt(index).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareBasis").Value;
                        leg.INF_FARETYPE = OB.ElementAt(index).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("fareProductDetail").Element("fareType").Value;

                        leg.INF_BREAKPOINT = OB.ElementAt(index).Descendants("groupOfFares").ElementAt(i).Element("productInformation").Element("breakPoint").Value;
                    }
                    else
                    {
                        leg.INF_RBD = "";
                        leg.INF_CABIN = "";
                        leg.INF_AVLSTATUS = "";
                        leg.INF_FAREBASIS = "";
                        leg.INF_FARETYPE = "";
                        leg.INF_BREAKPOINT = "";
                    }
                    #endregion

                    OB_leg.Add(leg);
                }
                #endregion
            }
            catch (Exception ex)
            {
            }
            return OB_leg;
        }
        #endregion
        #region AddToLeg
        public List<FlightSearchResults> AddLegstoFlt(List<LegFareDtls_1A> LegOB, List<Flights_1A> fltOB, List<LegFareDtls_1A> LegIB, List<Flights_1A> fltIB, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, string depCity, string arrCity, Hashtable Adt_FR, Hashtable Chd_FR, Hashtable Inf_FR, int recno, int StartLNO, List<MISCCharges> MiscList, string BRefQualifierXML, string MXML, List<string> SliceAndDiceList, bool inbound, List<string> segREfO, List<string> segRefR, string MNRXML, bool isPrivateFare, CORP_ENTY_CONFIG CpOrEtConfig, int FltSecNum)
        {
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();//For Final List
            int OB_Flt = fltOB.Count() / LegOB.Count();
            int IB_Flt = 0;
            if (fltIB != null && LegIB != null)
            {
                IB_Flt = fltIB.Count() / LegIB.Count();
            }
            try
            {
                int i = 1; //For Incrementing OB_Flt Count
                int j = 0; //For incrementing Flight count OB
                int k = 0; //For incrementing Flight count IB
                // while (i <= OB_Flt)
                while (i <= segREfO.Count()) //Prepare Each Flight
                {
                    #region Add Each OB Flight Legs
                    string PrevRef = segREfO[j];//fltOB[j].Flt_ref;
                    List<FlightSearchResults> Ob = new List<FlightSearchResults>();
                    List<Flights_1A> Perflt = fltOB.Where(x => x.Flt_ref == PrevRef).ToList().Distinct().ToList();
                    #region Baggage Info One Way

                    XDocument mainx = XDocument.Load(new StringReader(BRefQualifierXML));
                    for (int ii = 0; ii <= Perflt.Count - 1; ii++)
                    {
                        try
                        {
                            var segmentFlightRef = from f in mainx.Descendants("segmentFlightRef")
                                                   where (f.Descendants("referencingDetail").Elements("refNumber").First().Value == Perflt[ii].Flt_ref.ToString().Remove(0, 1))// || f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumber").Elements(ns1 + "number").First().Value == SegFRef_OB[1]
                                                   select new
                                                   {
                                                       faslist = f.Descendants("referencingDetail").First(p => p.Element("refQualifier").Value == "B").Element("refNumber").Value//.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail")//.ElementAt(0).Element(ns1 + "refNumber").Value, //.Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value //.Elements(ns1 + "number").First().Value  //.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value,

                                                   };
                            Perflt[ii].Baggage = BaggageInformation(MXML, segmentFlightRef.ToList()[0].faslist.ToString(), "O");// Bag[0].Baggage.ToString();
                        }
                        catch (Exception ex) { Perflt[ii].Baggage = ""; }

                        #region Refund and Reissue Penalty


                        try
                        {
                            var segmentFlightRefForPen = from f in mainx.Descendants("segmentFlightRef")
                                                         where (f.Descendants("referencingDetail").Elements("refNumber").First().Value == Perflt[ii].Flt_ref.ToString().Remove(0, 1))// || f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumber").Elements(ns1 + "number").First().Value == SegFRef_OB[1]
                                                         select new
                                                         {
                                                             faslist = f.Descendants("referencingDetail").First(p => p.Element("refQualifier").Value == "M").Element("refNumber").Value//.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail")//.ElementAt(0).Element(ns1 + "refNumber").Value, //.Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value //.Elements(ns1 + "number").First().Value  //.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value,

                                                         };
                            Perflt[ii].Penalty = PenaltyInformation(MNRXML, segmentFlightRefForPen.ToList()[0].faslist.ToString(), "O");// Bag[0].Baggage.ToString();
                        }
                        catch (Exception ex) { Perflt[ii].Penalty = ""; }
                        #endregion


                    }
                    #endregion
                    #region slice And Dice XML
                    string sdREf = "";
                    try
                    {
                        sdREf = SliceAndDiceList[j].ToString().Trim();
                    }
                    catch { sdREf = ""; }
                    List<string> segrefSDO = new List<string>(); ;
                    string SDXML = "";
                    XElement specificRecDetailsO = null;
                    XElement specificRecDetailsR = null;
                    if (!string.IsNullOrEmpty(sdREf))
                    {
                        if (mainx.Element("recommendation").Descendants("specificRecDetails").Any())
                        {
                            foreach (XElement obj in mainx.Element("recommendation").Descendants("specificRecDetails"))
                            {
                                if (obj.Descendants("specificRecItem").Where(x => x.Element("refNumber").Value == sdREf).Any())
                                {
                                    obj.DescendantNodesAndSelf().ToList().ForEach(x => SDXML = SDXML + x.ToString());
                                    int iseg = 0;
                                    foreach (XElement obj12 in obj.Descendants("specificProductDetails").Descendants("fareContextDetails").Descendants("requestedSegmentInfo").Descendants("segRef"))
                                    {
                                        if (obj12.Value == "1")
                                        {
                                            if (specificRecDetailsO == null)
                                            {
                                                specificRecDetailsO = obj.Descendants("specificProductDetails").ElementAt(iseg);
                                            }
                                        }
                                        else if (obj12.Value == "2")
                                        {
                                            if (specificRecDetailsR == null)
                                            {

                                                specificRecDetailsR = obj.Descendants("specificProductDetails").ElementAt(iseg);
                                            }
                                        }

                                        iseg++;

                                    }
                                }

                            }
                        }


                    }

                    //mainx.Descendants("")


                    #endregion

                    List<string> specificRecDetailsOList = new List<string>();
                    List<string> specificRecDetailsRList = new List<string>();
                    if (specificRecDetailsO != null)
                    {
                        var jjq = specificRecDetailsO.Descendants("fareContextDetails").Descendants("cnxContextDetails").Descendants("fareCnxInfo").Descendants("contextDetails").Descendants("availabilityCnxType").ToList();
                        foreach (XElement ssp in jjq)
                        {
                            specificRecDetailsOList.Add(ssp.Value);
                        }
                    }



                    Ob = GetFinalFlightList(LegOB, Perflt, SrvchargeList, CityList, AirList, MrkupDS, searchInputs, depCity, arrCity, Adt_FR, Chd_FR, Inf_FR, recno, StartLNO, 0, MiscList, inbound, isPrivateFare, CpOrEtConfig, FltSecNum);


                    int qq = 0;
                    foreach (FlightSearchResults f in Ob)
                    {
                        if (specificRecDetailsOList.Count() > 0 && Ob.Count() > qq && specificRecDetailsOList.Count() > qq)
                        {

                            f.SliceAndDiceXML = specificRecDetailsOList[qq].Trim();


                        }
                        FlightList.Add(f);
                        qq++;
                    }

                    // j = j + Perflt.Count();

                    j = j + 1;


                    #endregion

                    #region Add Each IB Flight Legs
                    if (fltIB != null)
                    {
                        string PrevRefIB = segRefR[k];//fltIB[k].Flt_ref;
                        List<FlightSearchResults> Ib = new List<FlightSearchResults>();
                        List<Flights_1A> PerfltIB = fltIB.Where(x => x.Flt_ref == PrevRefIB).ToList().Distinct().ToList();
                        #region Baggage Info Round Trip
                        for (int ii = 0; ii <= PerfltIB.Count - 1; ii++)
                        {
                            try
                            {
                                var segmentFlightRef = from f in mainx.Descendants("segmentFlightRef")
                                                       where (f.Descendants("referencingDetail").ElementAt(1).Elements("refNumber").First().Value == PerfltIB[ii].Flt_ref.ToString().Remove(0, 1))// || f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumber").Elements(ns1 + "number").First().Value == SegFRef_OB[1]
                                                       select new
                                                       {
                                                           faslist = f.Descendants("referencingDetail").First(p => p.Element("refQualifier").Value == "B").Element("refNumber").Value//.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail")//.ElementAt(0).Element(ns1 + "refNumber").Value, //.Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value //.Elements(ns1 + "number").First().Value  //.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value,

                                                       };

                                PerfltIB[ii].Baggage = BaggageInformation(MXML, segmentFlightRef.ToList()[0].faslist.ToString(), "R");// Bag[0].Baggage.ToString();
                            }
                            catch (Exception ex) { PerfltIB[ii].Baggage = ""; }


                            try
                            {
                                var segmentFlightRefForPen = from f in mainx.Descendants("segmentFlightRef")
                                                             where (f.Descendants("referencingDetail").Elements("refNumber").First().Value == Perflt[ii].Flt_ref.ToString().Remove(0, 1))// || f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumber").Elements(ns1 + "number").First().Value == SegFRef_OB[1]
                                                             select new
                                                             {
                                                                 faslist = f.Descendants("referencingDetail").First(p => p.Element("refQualifier").Value == "M").Element("refNumber").Value//.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail")//.ElementAt(0).Element(ns1 + "refNumber").Value, //.Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value //.Elements(ns1 + "number").First().Value  //.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail").Elements(ns1 + "refNumber").First().Value,

                                                             };
                                PerfltIB[ii].Penalty = PenaltyInformation(MNRXML, segmentFlightRefForPen.ToList()[0].faslist.ToString(), "R");// Bag[0].Baggage.ToString();
                            }
                            catch (Exception ex) { if (Perflt.Count > ii) { Perflt[ii].Penalty = ""; } }
                        }
                        #endregion
                        Ib = GetFinalFlightList(LegIB, PerfltIB, SrvchargeList, CityList, AirList, MrkupDS, searchInputs, depCity, arrCity, Adt_FR, Chd_FR, Inf_FR, recno, StartLNO, 1, MiscList, true, isPrivateFare, CpOrEtConfig, FltSecNum);



                        qq = 0;


                        if (specificRecDetailsR != null)
                        {
                            var jjq = specificRecDetailsR.Descendants("fareContextDetails").Descendants("cnxContextDetails").Descendants("fareCnxInfo").Descendants("contextDetails").Descendants("availabilityCnxType").ToList();
                            foreach (XElement ssp in jjq)
                            {
                                specificRecDetailsRList.Add(ssp.Value);
                            }
                        }
                        foreach (FlightSearchResults f in Ib)
                        {
                            if (specificRecDetailsRList.Count() > 0 && Ib.Count() > qq && specificRecDetailsRList.Count() > qq)
                            {
                                f.SliceAndDiceXML = specificRecDetailsRList[qq].Trim();

                            }
                            FlightList.Add(f);
                        }


                        // k = k + PerfltIB.Count();
                        k = k + 1;
                    }
                    #endregion

                    i++;
                    StartLNO++;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return FlightList;
        }
        #endregion
        #region FinalList
        public List<FlightSearchResults> GetFinalFlightList(List<LegFareDtls_1A> Leg, List<Flights_1A> flt, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MrkupDS, FlightSearch searchInputs, string depCity, string arrCity, Hashtable Adt_FR, Hashtable Chd_FR, Hashtable Inf_FR, int recno, int StartLNO, int schd, List<MISCCharges> MiscList, bool inbound, bool isPrivateFare, CORP_ENTY_CONFIG CpOrEtConfig, int FltSecNum)
        {
            FlightCommonBAL objFltComm = new FlightCommonBAL(ConnStr);
            DataTable CommDt = new DataTable();
            Hashtable STTFTDS = new Hashtable();
            List<FlightSearchResults> FlightList = new List<FlightSearchResults>();
            try
            {
                //Now Flt and Leg Would be Equal
                for (int i = 0; i < flt.Count(); i++) //For Each Flight 
                {
                    #region List
                    FlightSearchResults objFS = new FlightSearchResults(); //ResListItem
                    //decimal AdtTF = 0, ChdTF = 0, InfTF = 0;//For Total Fare Per Pax Type

                    #region Flight Details

                    if (inbound)
                    {
                        objFS.OrgDestFrom = arrCity;
                        objFS.OrgDestTo = depCity;
                    }
                    else
                    {
                        objFS.OrgDestFrom = depCity;
                        objFS.OrgDestTo = arrCity;
                    }

                    objFS.DepartureLocation = flt[i].LocationId1;
                    objFS.DepAirportCode = flt[i].LocationId1;
                    objFS.DepartureTerminal = string.IsNullOrEmpty(flt[i].LocationId1T) == true ? "1" : flt[i].LocationId1T;
                    objFS.DepartureCityName = getCityName(objFS.DepartureLocation, CityList);
                    objFS.ArrivalLocation = flt[i].LocationId2;
                    objFS.ArrAirportCode = flt[i].LocationId2;
                    objFS.ArrivalTerminal = string.IsNullOrEmpty(flt[i].LocationId2T) == true ? "1" : flt[i].LocationId1T;
                    objFS.ArrivalCityName = getCityName(objFS.ArrivalLocation, CityList);
                    objFS.BagInfo = flt[i].Baggage;
                  //  objFS.AdtFar = flt[i].Penalty;

                    if (isPrivateFare==false)
                        objFS.AdtFar = "NRM";
                    else                       
                        objFS.AdtFar = "CRP";
                    try
                    {
                        objFS.DepartureAirportName = ((from ct in CityList where ct.AirportCode == objFS.DepartureLocation select ct).ToList())[0].AirportName;
                        objFS.ArrivalAirportName = ((from ct in CityList where ct.AirportCode == objFS.ArrivalLocation select ct).ToList())[0].AirportName;
                    }
                    catch (Exception ex)
                    {
                        objFS.DepartureAirportName = flt[i].LocationId1;
                        objFS.ArrivalAirportName = flt[i].LocationId2;
                    }
                    objFS.DepartureDate = flt[i].DateOfDeparture;
                    objFS.Departure_Date = Utility.Left(flt[i].DateOfDeparture, 2) + " " + Utility.datecon(Utility.Mid(flt[i].DateOfDeparture, 2, 2));
                    objFS.DepartureTime = flt[i].TimeOfDeparture;

                    objFS.depdatelcc = Convert.ToDateTime("20" + Utility.Right(flt[i].DateOfDeparture, 2) + "-" + Utility.Mid(flt[i].DateOfDeparture, 2, 2) + "-" + Utility.Left(flt[i].DateOfDeparture, 2) + "T" + Utility.Left(flt[i].TimeOfDeparture, 2) + ":" + Utility.Right(flt[i].TimeOfDeparture, 2)).ToString("yyyy-MM-ddTHH:mm:ss");

                    objFS.ArrivalDate = flt[i].DateOfArrival;
                    objFS.Arrival_Date = Utility.Left(flt[i].DateOfArrival, 2) + " " + Utility.datecon(Utility.Mid(flt[i].DateOfArrival, 2, 2));
                    objFS.ArrivalTime = flt[i].TimeOfArrival;

                    objFS.arrdatelcc = Convert.ToDateTime("20" + Utility.Right(flt[i].DateOfArrival, 2) + "-" + Utility.Mid(flt[i].DateOfArrival, 2, 2) + "-" + Utility.Left(flt[i].DateOfArrival, 2) + "T" + Utility.Left(flt[i].TimeOfArrival, 2) + ":" + Utility.Right(flt[i].TimeOfArrival, 2)).ToString("yyyy-MM-ddTHH:mm:ss");

                    objFS.Adult = searchInputs.Adult;
                    objFS.Child = searchInputs.Child;
                    objFS.Infant = searchInputs.Infant;
                    objFS.TotPax = searchInputs.Adult + searchInputs.Child;
                    objFS.MarketingCarrier = flt[i].MarketingCarrier.Trim();
                    try
                    {
                        objFS.OperatingCarrier = flt[i].OperatingCarrier.Trim();
                    }
                    catch { }

                    objFS.FlightIdentification = flt[i].FlightNumber.Trim();
                    objFS.TotDur = Utility.Left(flt[i].Eft_Ref.Trim(), 2) + ":" + Utility.Right(flt[i].Eft_Ref.Trim(), 2);//flt[i].Eft_Ref.Trim();//
                    objFS.ValiDatingCarrier = flt[i].Mcx_Ref.Trim();
                    //objFS.AirLineName = getAirlineName(flt[i].Mcx_Ref.Trim(), AirList);
                    objFS.AirLineName = getAirlineName(flt[i].MarketingCarrier.Trim(), AirList);
                    //objFS.AvailableSeats = "";
                    //objFS.ArrDateLcc = flt[i]."STA"];
                    //objFS.DepDateLcc = flt[i]."STD"];

                    if (searchInputs.Trip == Trip.I)
                    {
                        if (schd == 0)
                        {
                            if (searchInputs.TripType == TripType.OneWay)
                                objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                            else
                                objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                        }
                        else
                        {
                            if (searchInputs.TripType == TripType.RoundTrip)
                                objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                            else
                                objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                        }
                    }
                    else
                    {
                        if (schd == 0)
                        {
                            if (searchInputs.GDSRTF == true)
                            { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo + ":" + objFS.OrgDestFrom; }
                            else
                            { objFS.Sector = objFS.OrgDestFrom + ":" + objFS.OrgDestTo; }
                        }
                        else
                        {
                            if (searchInputs.GDSRTF == true)
                                objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom + ":" + objFS.OrgDestTo;
                            else
                                objFS.Sector = objFS.OrgDestTo + ":" + objFS.OrgDestFrom;
                        }
                    }

                    //objFS.EQ = flt[i]."EQType"];
                    objFS.EQ = flt[i].EquipmentType.Trim();
                    objFS.LineNumber = StartLNO; //recno;
                    objFS.Leg = (i + 1);
                    objFS.Stops = (Leg.Count() - 1) + "-Stop";

                    objFS.Trip = searchInputs.Trip.ToString();
                    //objFS.FareBasis = t.FBC;
                    //objFS.RBD = t.FCS;
                    //objFS.FareRule = t.FSK; //Fare Sell Key
                    //objFS.Searchvalue = t.FSK; //Fare Sell Key
                    //objFS.SNO = t.JSK; //Journey Sell Key
                    objFS.Trip = searchInputs.Trip.ToString();


                    if ((searchInputs.GDSRTF == true) || (searchInputs.Trip == Trip.I))
                    {
                        if (schd == 0)
                        {
                            objFS.Flight = "1";
                            objFS.FType = "OutBound";
                            objFS.TripType = "O";
                        }
                        else
                        {
                            objFS.Flight = "2";
                            objFS.FType = "InBound";
                            objFS.TripType = "R";
                        }
                    }
                    else
                    {
                        objFS.Flight = "1";
                        objFS.FType = "OutBound";
                        objFS.TripType = "O";
                    }
                    #endregion


                    #region Adult
                    float srvCharge = 0;
                    if (searchInputs.Adult > 0)
                    {
                        string adtCabin = "Economy";
                        if (Leg[i].ADT_CABIN.Trim().ToUpper() == "M" || Leg[i].ADT_CABIN.Trim().ToUpper() == "Y")
                        { adtCabin = "Economy"; }
                        else if (Leg[i].ADT_CABIN.Trim().ToUpper() == "W")
                        { adtCabin = "PremiumEconomy"; }
                        if (Leg[i].ADT_CABIN.Trim().ToUpper() == "C" || Leg[i].ADT_CABIN.Trim().ToUpper() == "F")
                        { adtCabin = "Business"; }


                        objFS.AdtCabin = adtCabin;
                        objFS.AdtFarebasis = Leg[i].ADT_FAREBASIS;
                        objFS.AdtRbd = Leg[i].ADT_RBD;
                        objFS.AvailableSeats1 = Leg[i].ADT_AVLSTATUS;
                        objFS.AdtBreakPoint = Leg[i].BREAKPOINT;
                        objFS.Searchvalue = Leg[i].Corporate_Code;
                        objFS.FareDet = Leg[i].Corporate_Code;
                        //objFS.FareRule=

                        // if (searchInputs.Trip.ToString() == "I")
                        ///Block by PAT

                        /// srvCharge = objFltComm.MISCServiceFee(MiscList, objFS.ValiDatingCarrier);
                        ////Adt Fare Details 


                        objFS.AdtBfare = float.Parse((decimal.Parse(Adt_FR["Tot_Fare"].ToString()) - decimal.Parse(Adt_FR["Tot_Tax"].ToString())).ToString());
                        objFS.AdtFare = float.Parse(Adt_FR["Tot_Fare"].ToString());
                        objFS.AdtTax = float.Parse(Adt_FR["Tot_Tax"].ToString());
                        objFS.AdtFSur = float.Parse(Adt_FR["Pax_Yq"].ToString());
                        objFS.AdtYR = float.Parse(Adt_FR["Pax_Yr"].ToString());
                        objFS.AdtQ = float.Parse(Adt_FR["Pax_Q"].ToString());

                        objFS.AdtIN = float.Parse(Adt_FR["Pax_IN"].ToString());
                        objFS.AdtJN = float.Parse(Adt_FR["Pax_JN"].ToString());
                        objFS.AdtWO = float.Parse(Adt_FR["Pax_WO"].ToString());
                        objFS.AdtOT = float.Parse((decimal.Parse(objFS.AdtTax.ToString()) - decimal.Parse((objFS.AdtFSur + objFS.AdtYR + objFS.AdtIN + objFS.AdtJN + objFS.AdtWO).ToString())).ToString());


                        //SMS charge add 
                        objFS.AdtOT = objFS.AdtOT + srvCharge;
                        objFS.AdtTax = objFS.AdtTax + srvCharge;
                        objFS.AdtFare = objFS.AdtFare + srvCharge;
                        //SMS charge add end

                        objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                        objFS.ADTAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                        if (Adt_FR["Fare_Type"].ToString().Contains("NON-REFUNDABLE") == true)
                            objFS.AdtFareType = "Non Refundable";
                        else if (Adt_FR["Fare_Code"].ToString() == "70" || Adt_FR["Fare_Code"].ToString() == "71" || Adt_FR["Fare_Code"].ToString() == "72" || Adt_FR["Fare_Code"].ToString() == "")
                            objFS.AdtFareType = "Non Refundable";
                        else
                            objFS.AdtFareType = "Refundable";

                        if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                        {
                            objFS.ADTAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.AdtFare, searchInputs.Trip.ToString());
                            objFS.AdtBfare = objFS.AdtBfare + objFS.ADTAdminMrk;
                            objFS.AdtFare = objFS.AdtFare + objFS.ADTAdminMrk;
                        }
                        CommDt.Clear();
                        STTFTDS.Clear();
                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                        {
                            ///change by PAT

                            /// CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), 1, objFS.AdtRbd, objFS.AdtCabin);
                            // objFS.AdtDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            // //objFS.AdtDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            // objFS.AdtCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            // //STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                            //// STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.AdtDiscount1, objFS.AdtBfare, objFS.AdtFSur, searchInputs.TDS);
                            // //objFS.AdtSrvTax = float.Parse(STTFTDS["STax"].ToString());
                            // objFS.AdtSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            // objFS.AdtDiscount = objFS.AdtDiscount1 - objFS.AdtSrvTax1;
                            // objFS.AdtTF = float.Parse(STTFTDS["TFee"].ToString());
                            // objFS.AdtTds = float.Parse(STTFTDS["Tds"].ToString());
                            // objFS.IATAComm = int.Parse(STTFTDS["IATAComm"].ToString());
                        }
                        else
                        {
                            objFS.AdtDiscount = 0;
                            objFS.AdtCB = 0;
                            objFS.AdtSrvTax = 0;
                            objFS.AdtTF = 0;
                            objFS.AdtTds = 0;
                            objFS.IATAComm = 0;
                        }
                    }
                    if (searchInputs.IsCorp == true)
                    {
                        try
                        {
                            DataTable MGDT = new DataTable();
                            //  MGDT=objFltComm.GetServiceTax()
                            // MGDT = objFltComm.clac_MgtFee(searchInputs.TPCode, objFS.ValiDatingCarrier, decimal.Parse(objFS.AdtBfare.ToString()), decimal.Parse(objFS.AdtFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.AdtFare.ToString()));
                            objFS.AdtMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                            objFS.AdtSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        }
                        catch { }
                    }
                    objFS.AdtEduCess = 0;
                    objFS.AdtHighEduCess = 0;
                    #endregion

                    #region child

                    string chdCabin = "Economy";
                    if (Leg[i].CHD_CABIN.Trim().ToUpper() == "M" || Leg[i].CHD_CABIN.Trim().ToUpper() == "Y")
                    { chdCabin = "Economy"; }
                    else if (Leg[i].CHD_CABIN.Trim().ToUpper() == "W")
                    { chdCabin = "PremiumEconomy"; }
                    if (Leg[i].CHD_CABIN.Trim().ToUpper() == "C" || Leg[i].CHD_CABIN.Trim().ToUpper() == "F")
                    { chdCabin = "Business"; }


                    objFS.ChdCabin = chdCabin;
                    objFS.ChdFarebasis = Leg[i].CHD_FAREBASIS;
                    objFS.ChdRbd = Leg[i].CHD_RBD;
                    objFS.ChdBreakPoint = Leg[i].CHD_BREAKPOINT;
                    ////objFS.ChdfareType
                    //objFS.ChdTax = 0;
                    if (searchInputs.Child > 0)
                    {

                        objFS.ChdBFare = float.Parse((decimal.Parse(Chd_FR["Tot_Fare"].ToString()) - decimal.Parse(Chd_FR["Tot_Tax"].ToString())).ToString());
                        objFS.ChdFare = float.Parse(Chd_FR["Tot_Fare"].ToString());
                        objFS.ChdTax = float.Parse(Chd_FR["Tot_Tax"].ToString());
                        objFS.ChdFSur = float.Parse(Chd_FR["Pax_Yq"].ToString());
                        objFS.ChdYR = float.Parse(Chd_FR["Pax_Yr"].ToString());
                        objFS.ChdQ = float.Parse(Chd_FR["Pax_Q"].ToString());

                        //objFS.ChdIN = 0;
                        //objFS.ChdJN = 0;
                        //objFS.ChdWO = 0;
                        objFS.ChdIN = float.Parse(Chd_FR["Pax_IN"].ToString());
                        objFS.ChdJN = float.Parse(Chd_FR["Pax_JN"].ToString());
                        objFS.ChdWO = float.Parse(Chd_FR["Pax_WO"].ToString());
                        objFS.ChdOT = float.Parse((decimal.Parse(objFS.ChdTax.ToString()) - decimal.Parse((objFS.ChdFSur + objFS.ChdYR + objFS.ChdIN + objFS.ChdJN + objFS.ChdWO).ToString())).ToString());

                        objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                        objFS.CHDAgentMrk = CalcMarkup(MrkupDS.Tables["AgentMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                        CommDt.Clear();
                        STTFTDS.Clear();
                        if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                        {
                            objFS.CHDAdminMrk = CalcMarkup(MrkupDS.Tables["AdminMarkUp"], objFS.ValiDatingCarrier, objFS.ChdFare, searchInputs.Trip.ToString());
                            objFS.ChdBFare = objFS.ChdBFare + objFS.CHDAdminMrk;
                            objFS.ChdFare = objFS.ChdFare + objFS.CHDAdminMrk;
                        }
                        if (searchInputs.Trip.ToString() == JourneyType.D.ToString())
                        {
                            ///Block by PAT
                            ///CommDt = objFltComm.GetFltComm_Gal(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), 1, objFS.ChdRbd, objFS.ChdCabin);
                            ////// objFS.ChdDiscount = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            ////objFS.ChdDiscount1 = float.Parse(CommDt.Rows[0]["Dis"].ToString());
                            ////objFS.ChdCB = float.Parse(CommDt.Rows[0]["CB"].ToString());
                            ///////Block by PAT
                            //////STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                            ///////STTFTDS = CalcSrvTaxTFeeTds(SrvchargeList, objFS.ValiDatingCarrier, objFS.ChdDiscount1, objFS.ChdBFare, objFS.ChdFSur, searchInputs.TDS);
                            //////objFS.ChdSrvTax = float.Parse(STTFTDS["STax"].ToString());
                            ////objFS.ChdSrvTax1 = float.Parse(STTFTDS["STax"].ToString());
                            ////objFS.ChdDiscount = objFS.ChdDiscount1 - objFS.ChdSrvTax1;
                            ////objFS.ChdTF = float.Parse(STTFTDS["TFee"].ToString());
                            ////objFS.ChdTds = float.Parse(STTFTDS["Tds"].ToString());
                        }
                        else
                        {
                            objFS.ChdDiscount = 0;
                            objFS.ChdCB = 0;
                            objFS.ChdSrvTax = 0;
                            objFS.ChdTF = 0;
                            objFS.ChdTds = 0;
                        }

                        ///block by PAT

                        //if (searchInputs.IsCorp == true)
                        //{
                        //    try
                        //    {
                        //        DataTable MGDT = new DataTable();
                        //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.ChdBFare.ToString()), decimal.Parse(objFS.ChdFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.ChdFare.ToString()));
                        //        objFS.ChdMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        //        objFS.ChdSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        //    }
                        //    catch { }
                        //}
                        //SMS Charge add
                        objFS.ChdOT = objFS.ChdOT + srvCharge;
                        objFS.ChdTax = objFS.ChdTax + srvCharge;
                        objFS.ChdFare = objFS.ChdFare + srvCharge;
                        //SMS charge add end

                        objFS.ChdEduCess = 0;
                        objFS.ChdHighEduCess = 0;
                        //objFS.IATAComm = int.Parse(STTFTDS["IATAComm"].ToString());
                    }
                    else
                    {
                        objFS.ChdBFare = 0;
                        objFS.ChdFare = 0;
                        objFS.ChdFSur = 0;
                        objFS.ChdWO = 0;
                        objFS.ChdIN = 0;
                        objFS.ChdJN = 0;
                        objFS.ChdYR = 0;
                        objFS.ChdOT = 0;
                        objFS.ChdTax = 0;
                        objFS.ChdDiscount = 0;
                        objFS.ChdCB = 0;
                        objFS.ChdSrvTax = 0;
                        objFS.ChdTF = 0;
                        objFS.ChdTds = 0;
                        objFS.ChdEduCess = 0;
                        objFS.ChdHighEduCess = 0;

                    }

                    #endregion

                    #region Infant
                    if (searchInputs.Infant > 0)
                    {
                        string infCabin = "Economy";
                        if (Leg[i].INF_CABIN.Trim().ToUpper() == "M" || Leg[i].INF_CABIN.Trim().ToUpper() == "Y")
                        { infCabin = "Economy"; }
                        else if (Leg[i].INF_CABIN.Trim().ToUpper() == "W")
                        { infCabin = "PremiumEconomy"; }
                        if (Leg[i].INF_CABIN.Trim().ToUpper() == "C" || Leg[i].INF_CABIN.Trim().ToUpper() == "F")
                        { infCabin = "Business"; }

                        objFS.InfCabin = infCabin;
                        objFS.InfFarebasis = Leg[i].INF_FAREBASIS;
                        objFS.InfRbd = Leg[i].INF_RBD;
                        objFS.InfBreakPoint = Leg[i].INF_BREAKPOINT;

                        if (Inf_FR.Count > 0)
                        {

                            objFS.InfBfare = float.Parse((decimal.Parse(Inf_FR["Tot_Fare"].ToString()) - decimal.Parse(Inf_FR["Tot_Tax"].ToString())).ToString());
                            objFS.InfFare = float.Parse(Inf_FR["Tot_Fare"].ToString());
                            objFS.InfTax = float.Parse(Inf_FR["Tot_Tax"].ToString());
                            objFS.InfFSur = float.Parse(Inf_FR["Pax_Yq"].ToString());
                            objFS.InfYR = float.Parse(Inf_FR["Pax_Yr"].ToString());
                            objFS.InfQ = float.Parse(Inf_FR["Pax_Q"].ToString());
                            objFS.InfOT = float.Parse((decimal.Parse(objFS.InfTax.ToString()) - decimal.Parse((objFS.InfFSur + objFS.InfYR).ToString())).ToString());

                        }
                        else
                        {
                            objFS.InfFare = 0;
                            objFS.InfBfare = 0;
                            objFS.InfTax = 0;
                            objFS.InfOT = 0;
                            objFS.InfQ = 0;
                        }
                        objFS.InfIN = 0;
                        objFS.InfJN = 0;
                        objFS.InfWO = 0;

                        ///block for amadeus integration PAT

                        //if (searchInputs.IsCorp == true)
                        //{
                        //    try
                        //    {
                        //        DataTable MGDT = new DataTable();
                        //        MGDT = objFltComm.clac_MgtFee(searchInputs.AgentType, objFS.ValiDatingCarrier, decimal.Parse(objFS.InfBfare.ToString()), decimal.Parse(objFS.InfFSur.ToString()), searchInputs.Trip.ToString(), decimal.Parse(objFS.InfFare.ToString()));
                        //        objFS.InfMgtFee = float.Parse(MGDT.Rows[0]["MGTFEE"].ToString());
                        //        objFS.InfSrvTax = float.Parse(MGDT.Rows[0]["MGTSRVTAX"].ToString());
                        //    }
                        //    catch { }
                        //}
                    }
                    else
                    {
                        objFS.InfFare = 0;
                        objFS.InfBfare = 0;
                        objFS.InfFSur = 0;
                        objFS.InfIN = 0;
                        objFS.InfJN = 0;
                        objFS.InfOT = 0;
                        objFS.InfQ = 0;
                    }
                    #endregion
                    // objFS.AdtFar = "";
                    objFS.OriginalTF = float.Parse((objFS.AdtFare * objFS.Adult + objFS.ChdFare * objFS.Child).ToString());//Without Infant Fare
                    objFS.TotBfare = (objFS.AdtBfare * objFS.Adult) + (objFS.ChdBFare * objFS.Child) + (objFS.InfBfare * objFS.Infant);
                    objFS.TotalTax = (objFS.AdtTax * objFS.Adult) + (objFS.ChdTax * objFS.Child) + (objFS.InfTax * objFS.Infant);
                    objFS.TotalFuelSur = (objFS.AdtFSur * objFS.Adult) + (objFS.ChdFSur * objFS.Child);
                    objFS.TotalFare = (objFS.AdtFare * objFS.Adult) + (objFS.ChdFare * objFS.Child) + (objFS.InfFare * objFS.Infant);
                    objFS.STax = (objFS.AdtSrvTax * objFS.Adult) + (objFS.ChdSrvTax * objFS.Child) + (objFS.InfSrvTax * objFS.Infant);
                    objFS.TFee = (objFS.AdtTF * objFS.Adult) + (objFS.ChdTF * objFS.Child) + (objFS.InfTF * objFS.Infant);
                    objFS.TotDis = (objFS.AdtDiscount * objFS.Adult) + (objFS.ChdDiscount * objFS.Child);
                    objFS.TotCB = (objFS.AdtCB * objFS.Adult) + (objFS.ChdCB * objFS.Child);
                    objFS.TotTds = (objFS.AdtTds * objFS.Adult) + (objFS.ChdTds * objFS.Child);// +objFS.InfTds;
                    objFS.TotMrkUp = (objFS.ADTAdminMrk * objFS.Adult) + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child) + (objFS.CHDAgentMrk * objFS.Child);
                    objFS.TotMgtFee = (objFS.AdtMgtFee * objFS.Adult) + (objFS.ChdMgtFee * objFS.Child) + (objFS.InfMgtFee * objFS.Infant);
                    //if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                    //    objFS.TotalFare = objFS.TotalFare + objFS.STax + objFS.TFee + objFS.TotMgtFee + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child);
                    //else
                    //    objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                    //objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));
                    if ((searchInputs.Trip.ToString() == JourneyType.I.ToString()) && (searchInputs.IsCorp == true))
                        objFS.TotalFare = (objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee) - ((objFS.ADTAdminMrk * objFS.Adult) + (objFS.CHDAdminMrk * objFS.Child));
                    else
                        objFS.TotalFare = objFS.TotalFare + objFS.TotMrkUp + objFS.STax + objFS.TFee + objFS.TotMgtFee;
                    objFS.NetFare = (objFS.TotalFare + objFS.TotTds) - (objFS.TotDis + objFS.TotCB + (objFS.ADTAgentMrk * objFS.Adult) + (objFS.CHDAgentMrk * objFS.Child));

                    objFS.IsCorp = searchInputs.IsCorp;
                    objFS.Provider = "1A";
                    objFS.OriginalTT = srvCharge;
                    objFS.RBD = objFS.AdtRbd;
                    objFS.TripCnt = isPrivateFare == true ? "C" : "R";
                    objFS.SubSLineNumber = FltSecNum.ToString();

                    //objFS.GrpCode = CpOrEtConfig.GrpCode;
                    //objFS.TPCode = searchInputs.TPCode;
                    //objFS.Code = CpOrEtConfig.Code;
                    //objFS.TDCode = searchInputs.TDCode;
                    //objFS.ST = CpOrEtConfig.ST;

                 //   objFS.TFOrgDest = searchInputs.OFFLINE_TF == true ? "Y" : "N";
                   // objFS.TFOrgDest = objFS.TFOrgDest + ":" + Utility.Split(searchInputs.HidTxtDepCity.Trim(), ",")[0] + ":" + Utility.Split(searchInputs.HidTxtArrCity.Trim(), ",")[0];



                    FlightList.Add(objFS);


                    #endregion
                }
            }
            catch (Exception ex)
            {
            }
            return FlightList;
            // FltResult f;
            //ArrayList a = new ArrayList();
            //string s = f.GetEntityXml;
        }
        #endregion
        #region IncrementSession
        #region GetIncrementedSessionId
        private string GetIncrementedSessionId(string SId)
        {
            string[] SID = SId.Split('|');
            string SessionId = SID[0] + "|" + Convert.ToInt32(Convert.ToInt32(SID[1]) + 1) + "|" + SID[2];
            return SessionId;

        }
        #endregion GetIncrementedSessionId
        #endregion

        #region BaggageInfo
        private string BaggageInformation(string RES, string SegRef, string Trip)
        {
            string Baggage = "";
            try
            {
                XDocument main = XDocument.Load(new StringReader(RES));
                XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                XNamespace ns1 = XNamespace.Get("http://xml.amadeus.com/FMPTBR_17_3_1A");
                var fltoneway = from f in main.Descendants(soap + "Body").Descendants(ns1 + "serviceCoverageInfoGrp")
                                where (f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumber").Elements(ns1 + "number").First().Value == SegRef)
                                select new
                                {
                                    faslist = f.Descendants(ns1 + "serviceCovInfoGrp").Descendants(ns1 + "refInfo").Descendants(ns1 + "referencingDetail")

                                };
                string onewayref = ""; string roundref = "";
                if (fltoneway.ToList()[0].faslist.Count() == 1)
                {
                    onewayref = fltoneway.ToList()[0].faslist.ElementAt(0).Element(ns1 + "refNumber").Value;
                    roundref = onewayref;
                }
                else
                {
                    onewayref = fltoneway.ToList()[0].faslist.ElementAt(0).Element(ns1 + "refNumber").Value;
                    roundref = fltoneway.ToList()[0].faslist.ElementAt(1).Element(ns1 + "refNumber").Value;
                }
                BaggList BaggageList = new BaggList();

                if (Trip == "O")
                {
                    var flt1 = from f in main.Descendants(soap + "Body").Descendants(ns1 + "freeBagAllowanceGrp")

                               where (f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumberDetails").Elements(ns1 + "number").First().Value == onewayref)// || f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumber").Elements(ns1 + "number").First().Value == SegFRef_OB[1]
                               select new
                               {
                                   freeAllowance = f.Descendants(ns1 + "freeBagAllownceInfo").Descendants(ns1 + "baggageDetails").Elements(ns1 + "freeAllowance").First().Value,
                                   quantityCode = f.Descendants(ns1 + "freeBagAllownceInfo").Descendants(ns1 + "baggageDetails").Elements(ns1 + "quantityCode").First().Value,
                                   //unitQualifier = f.Descendants(ns1 + "freeBagAllownceInfo").Descendants(ns1 + "baggageDetails").Elements(ns1 + "unitQualifier").First().Value,


                               };
                    if (flt1.ToList()[0].quantityCode.ToUpper() == "W")
                    {
                        Baggage = flt1.ToList()[0].freeAllowance + " KG " + " Baggage included";//flt1.ToList()[0].unitQualifier +


                    }
                    else
                    {
                        Baggage = flt1.ToList()[0].freeAllowance + " Piece(s) Baggage included";

                    }
                }
                if (Trip == "R")
                {
                    BaggageList = new BaggList();
                    var flt1rt = from f in main.Descendants(soap + "Body").Descendants(ns1 + "freeBagAllowanceGrp")
                                 where (f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumberDetails").Elements(ns1 + "number").First().Value == roundref)// || f.Descendants(ns1 + "itemNumberInfo").Descendants(ns1 + "itemNumber").Elements(ns1 + "number").First().Value == SegFRef_OB[1]
                                 select new
                                 {
                                     freeAllowance = f.Descendants(ns1 + "freeBagAllownceInfo").Descendants(ns1 + "baggageDetails").Elements(ns1 + "freeAllowance").First().Value,
                                     quantityCode = f.Descendants(ns1 + "freeBagAllownceInfo").Descendants(ns1 + "baggageDetails").Elements(ns1 + "quantityCode").First().Value,
                                     // unitQualifier = f.Descendants(ns1 + "freeBagAllownceInfo").Descendants(ns1 + "baggageDetails").Elements(ns1 + "unitQualifier").First().Value,
                                 };
                    if (flt1rt.ToList()[0].quantityCode.ToUpper() == "W")
                    {
                        Baggage = flt1rt.ToList()[0].freeAllowance + " KG " + " Baggage included";//flt1rt.ToList()[0].unitQualifier +
                    }
                    else
                    {
                        BaggageList.Baggage = flt1rt.ToList()[0].freeAllowance + " Piece(s) Baggage included";
                    }
                }
            }
            catch (Exception ex) { }
            return Baggage;


        }
        #endregion


        #region PenaltyInformation
        private string PenaltyInformation(string MNR_RES, string SegRef, string Trip)
        {
            string Penalty = "";
            try
            {
                XDocument main = XDocument.Load(new StringReader(MNR_RES));
                XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
                XNamespace ns1 = XNamespace.Get("http://xml.amadeus.com/FMPTBR_14_3_1A");
                var RIRUList = from f in main.Descendants("mnrDetails")
                               where (f.Descendants("mnrRef").Descendants("itemNumberDetails").Elements("number").First().Value == SegRef)
                               select new
                               {
                                   RIRU = f.Descendants("catGrp")

                               };

                string fareInfo = "";
                string refund = "";
                string reissue = "";

                //"FareCondition - " + ADTFARELIST[0].FareRulesFilter.ToString() + "/CancelPenalty - " + ADTFARELIST[0].CancelPenalty.ToString() + "/ChangePenalty - " + ADTFARELIST[0].ChangePenalty.ToString();

                foreach (var a in RIRUList)
                {
                    foreach (var pen in a.RIRU)
                    {
                        // Reissue (Category 31)
                        if (pen.Descendants("catInfo").Descendants("descriptionInfo").Elements("number").First().Value == "31")
                        {
                            int RVJ = 0;
                            int BDJ = 0;

                            foreach (var b in pen.Descendants("statusInfo").Descendants("statusInformation"))
                            {
                                if (b.Element("indicator").Value == "RVJ")
                                {
                                    RVJ = Convert.ToInt32(b.Element("action").Value);
                                }
                                if (b.Element("indicator").Value == "BDJ")
                                {
                                    BDJ = Convert.ToInt32(b.Element("action").Value);
                                }
                            }

                            if (BDJ == 1) //RVJ == 1 ||
                            {
                                if (pen.Descendants("monInfo").Descendants("otherMonetaryDetails").Elements("typeQualifier").FirstOrDefault().Value.Trim().ToUpper() == "BDT")
                                {
                                    reissue = pen.Descendants("monInfo").Descendants("otherMonetaryDetails").Elements("amount").FirstOrDefault().Value.Trim();
                                }
                            }
                            else
                            {
                                fareInfo += "| Reissue Not Allowed";
                            }

                        }


                        //Refund (Category 33)
                        if (pen.Descendants("catInfo").Descendants("descriptionInfo").Elements("number").First().Value.Trim().ToUpper() == "33")
                        {
                            int RVJ = 0;
                            int BDJ = 0;

                            foreach (var b in pen.Descendants("statusInfo").Descendants("statusInformation"))
                            {
                                if (b.Element("indicator").Value == "RVJ")
                                {
                                    RVJ = Convert.ToInt32(b.Element("action").Value);
                                }
                                if (b.Element("indicator").Value == "BDJ")
                                {
                                    BDJ = Convert.ToInt32(b.Element("action").Value);
                                }
                            }

                            if (BDJ == 1) //RVJ == 1 ||
                            {
                                if (pen.Descendants("monInfo").Descendants("monetaryDetails").Elements("typeQualifier").FirstOrDefault().Value.Trim().ToUpper() == "BDT")
                                {
                                    refund = pen.Descendants("monInfo").Descendants("monetaryDetails").Elements("amount").FirstOrDefault().Value.Trim();
                                }
                            }
                            else
                            {
                                fareInfo += "| Refund Not Allowed.";
                            }

                        }


                    }
                }

                Penalty = "FareCondition - " + fareInfo + "/CancelPenalty - " + refund + "/ChangePenalty - " + reissue;
            }
            catch (Exception ex) { }
            return Penalty;


        }
        #endregion
        #region Reissue
        public string MasterPricerRequestATC(FlightSearch obj, string SessionId, int NFlt, bool isInbound, string svcUrl, bool IsPrivateFare, DataTable PFCodeDt, string RID, string RefNo)
        {
          //  LNBCORPDAL.ReissuePnrDAL objDal = new LNBCORPDAL.ReissuePnrDAL(ConnStr.Trim());
          //  List<SegmentDetailReissue> objSD = objDal.GetReissueTrvlTktDetails(RID, RefNo, "GETTKT");
            #region variables


            string Dep = Utility.Left(obj.HidTxtDepCity, 3);
            string Arr = Utility.Left(obj.HidTxtArrCity, 3);
            string DepDate = Utility.Left(obj.DepDate, 2) + Utility.Mid(obj.DepDate, 3, 2) + Utility.Right(obj.DepDate, 2);
            string ArrDate = string.IsNullOrEmpty(obj.RetDate) == false ? Utility.Left(obj.RetDate, 2) + Utility.Mid(obj.RetDate, 3, 2) + Utility.Right(obj.RetDate, 2) : "";
            int segcount = 1;
            if ((obj.TripType == TripType.RoundTrip && obj.RTF == true && obj.Trip == Trip.D) || (obj.TripType == TripType.RoundTrip && obj.Trip == Trip.I))
                segcount = 2;
            StringBuilder Req = new StringBuilder();
            string Response = "";


            AmdSession objHeader = new AmdSession();
            #endregion
            try
            {
                Req.Append("<?xml version='1.0' encoding='utf-8'?>");
                Req.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:sec='http://xml.amadeus.com/2010/06/Security_v1' xmlns:typ='http://xml.amadeus.com/2010/06/Types_v1' xmlns:iat='http://www.iata.org/IATA/2007/00/IATA2010.1' xmlns:app='http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3' xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1' xmlns:ses='http://xml.amadeus.com/2010/06/Session_v3' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_18_1_1A'>");
                Req.Append("<soapenv:Header>");
                Req.Append(objHeader.GetSessionHeader(SessionId, UserName, OfficeID, Pass, ServiceUrls.ATCActionMasterPricer, svcUrl, IncludeSessionType.NO));
                Req.Append("</soapenv:Header>");
                #region Body
                Req.Append("<soapenv:Body>");
                Req.Append("<Ticket_ATCShopperMasterPricerTravelBoardSearch  xmlns='" + ServiceUrls.ATCActionMasterPricer + "' >");//
                Req.Append("<numberOfUnit>");
                Req.Append("<unitNumberDetail>");
                Req.Append("<numberOfUnits>" + (obj.Adult + obj.Child) + "</numberOfUnits>");
                Req.Append("<typeOfUnit>PX</typeOfUnit>");
                Req.Append("</unitNumberDetail>");
                Req.Append("<unitNumberDetail>");
                Req.Append("<numberOfUnits>" + NFlt + "</numberOfUnits>");//No. of Flights
                Req.Append("<typeOfUnit>RC</typeOfUnit>");
                Req.Append("</unitNumberDetail>");
                Req.Append("</numberOfUnit>");
                #region Pax
                if (obj.Adult >= 1)
                {
                    Req.Append("<paxReference>");
                    Req.Append("<ptc>ADT</ptc>");
                    for (int i = 1; i <= obj.Adult; i++)
                    {
                        Req.Append("<traveller>");
                        Req.Append("<ref>" + i + "</ref>");
                        Req.Append("</traveller>");
                    }
                    Req.Append("</paxReference>");
                }
                if (obj.Child >= 1)
                {
                    Req.Append("<paxReference>");
                    Req.Append("<ptc>CH</ptc>");
                    for (int i = 1; i <= obj.Child; i++)
                    {
                        Req.Append("<traveller>");
                        Req.Append("<ref>" + (obj.Adult + i) + "</ref>");
                        Req.Append("</traveller>");
                    }
                    Req.Append("</paxReference>");
                }
                if (obj.Infant >= 1)
                {
                    Req.Append("<paxReference>");
                    Req.Append("<ptc>INF</ptc>");
                    for (int i = 1; i <= obj.Infant; i++)
                    {
                        Req.Append("<traveller>");
                        Req.Append("<ref>" + i + "</ref>");
                        Req.Append("<infantIndicator>" + i + "</infantIndicator>");
                        Req.Append("</traveller>");
                    }
                    Req.Append("</paxReference>");
                }
                #endregion
                #region FareOptions
                Req.Append("<fareOptions>");
                Req.Append("<pricingTickInfo>");
                Req.Append("<pricingTicketing>");

                //Req.Append("<priceType>TAC</priceType>");
                //Req.Append("<priceType>ET</priceType>");
                //Req.Append("<priceType>MNR</priceType>");
                if (IsPrivateFare)
                { Req.Append("<priceType>RW</priceType>"); }
                else
                {
                    Req.Append("<priceType>RP</priceType>");
                    Req.Append("<priceType>RU</priceType>");
                }
                Req.Append("</pricingTicketing>");
                Req.Append("</pricingTickInfo>");



                if (IsPrivateFare)
                {

                    DataTable DtVC = new DataTable();
                    if (String.IsNullOrEmpty(obj.HidTxtAirLine.ToUpper().Trim()) == false)
                    {
                        try
                        {
                            //FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
                            //DtVC = objFCDAL.GetCodeforPForDCFromDB_VC(obj.TCCode, obj.Trip.ToString(), "PA", obj.HidTxtAirLine.ToUpper().Trim());
                            FltDeal objDeal = new FltDeal(ConnStr);
                            // FlightCommonDAL objFCDAL = new FlightCommonDAL(ConnStr);
                            DtVC = objDeal.GetCodeforPForDCFromDB("PA", obj.Trip.ToString(), obj.AgentType, obj.UID, obj.AirLine, "", obj.HidTxtDepCity.Split(',')[0], obj.HidTxtArrCity.Split(',')[0], obj.HidTxtDepCity.Split(',')[1], obj.HidTxtArrCity.Split(',')[1]);
                            //  DtVC = objFCDAL.GetCodeforPForDCFromDB_VC(obj.TCCode, obj.Trip.ToString(), "PA", obj.HidTxtAirLine.ToUpper().Trim());
                        }
                        catch (Exception ex)
                        {
                            ExecptionLogger.FileHandling("FlightBAL(1A_MPTB_Req)", "Error_001", ex, "FlightRequest");
                        }
                    }
                    if (DtVC.Rows.Count > 0)
                    {
                        Req.Append("<corporate>");
                        for (int i = 0; i < DtVC.Rows.Count; i++)
                        {
                            if (DtVC.Rows[i]["PCatagoryCode"].ToString().Contains("BSP"))
                            {

                                Req.Append("<corporateId>");
                                Req.Append("<corporateQualifier>RW</corporateQualifier>");
                                Req.Append("<identity>" + DtVC.Rows[i]["D_T_Code"].ToString() + "</identity>");
                                Req.Append(" </corporateId>");


                            }

                        }
                        Req.Append("</corporate>");

                    }
                    else
                    {
                        if (PFCodeDt.Rows.Count > 0)
                        {
                            Req.Append("<corporate>");
                            for (int i = 0; i < PFCodeDt.Rows.Count; i++)
                            {
                                if (PFCodeDt.Rows[i]["PCatagoryCode"].ToString().Contains("BSP"))
                                {

                                    Req.Append("<corporateId>");
                                    Req.Append("<corporateQualifier>RW</corporateQualifier>");
                                    Req.Append("<identity>" + PFCodeDt.Rows[i]["D_T_Code"].ToString() + "</identity>");
                                    Req.Append(" </corporateId>");


                                }
                            }

                            Req.Append("</corporate>");

                        }

                    }
                }





                Req.Append("</fareOptions>");
                #endregion
                Req.Append("<travelFlightInfo>");
                if (obj.Cabin != "")
                {
                    Req.Append("<cabinId>");
                    Req.Append("<cabin>" + obj.Cabin + "</cabin>");
                    Req.Append("</cabinId>");
                }
                else
                {
                    Req.Append("<cabinId>");
                    Req.Append("<cabin>Y</cabin>");
                    Req.Append("</cabinId>");
                }
                if (obj.HidTxtAirLine.Length > 1)
                {
                    Req.Append("<companyIdentity>");
                    Req.Append("<carrierQualifier>M</carrierQualifier>");
                    Req.Append("<carrierId>" + Utility.Right(obj.HidTxtAirLine, 2) + "</carrierId>");
                    Req.Append("</companyIdentity>");
                }
                Req.Append("</travelFlightInfo>");
                #region itinerary
                int j = 1;
                while (j <= segcount)
                {
                    string x = ""; //For reversing
                    if (j == 2 || isInbound == true)
                    {
                        x = Dep;
                        Dep = Arr;
                        Arr = x;
                        DepDate = ArrDate;
                    }
                    Req.Append("<itinerary>");
                    Req.Append("<requestedSegmentRef>");
                    Req.Append("<segRef>" + j + "</segRef>");
                    Req.Append("</requestedSegmentRef>");
                    Req.Append("<departureLocalization>");
                    Req.Append("<departurePoint>");
                    Req.Append("<locationId>" + Dep + "</locationId>");
                    Req.Append("</departurePoint>");
                    Req.Append("</departureLocalization>");
                    Req.Append("<arrivalLocalization>");
                    Req.Append("<arrivalPointDetails>");
                    Req.Append("<locationId>" + Arr + "</locationId>");
                    Req.Append("</arrivalPointDetails>");
                    Req.Append("</arrivalLocalization>");
                    Req.Append("<timeDetails>");
                    Req.Append("<firstDateTimeDetail>");
                    Req.Append("<timeQualifier>TD</timeQualifier>");
                    Req.Append("<date>" + DepDate + "</date>"); //251013
                    Req.Append("<time>1200</time>");
                    Req.Append("<timeWindow>12</timeWindow>");
                    Req.Append("</firstDateTimeDetail>");
                    Req.Append("</timeDetails>");
                    Req.Append("</itinerary>");
                    j++;
                }
                #endregion
                #region ticketChangeInfo
                Req.Append("<ticketChangeInfo>");
                //Req.Append("<ticketNumberDetails>");
                //foreach (var p in objSD)
                //{
                //    Req.Append("<documentDetails>");
                //    Req.Append("<number>" + p.TicketNumber + "</number>");
                //    Req.Append("</documentDetails>");
                //}
                //Req.Append("</ticketNumberDetails>");

                //#region ATCitinerary
                //j = 1;
                //int ResSegCount = objSD[0].TripType.ToUpper() == "O" ? 1 : 2;
                //string Sector = objSD[0].Sector;//
                //if (segcount == 2)
                //{
                //    Sector = objSD[0].Sector.Split(',')[0];
                //    for (int s = 1; s < objSD[0].Sector.Split(',').Count() / 2; s++)
                //    {
                //        Sector += ":" + objSD[0].Sector.Split(',')[s];
                //    }
                //}
                ////: .Split(',')[objSD[0].Sector.Split(',').Count() / segcount];
                //string BookedDep = Sector.Split(',')[0].Split(':')[0];
                //string BookedArr = Sector.Split(',')[Sector.Split(',').Count() - 1].Split(':')[1];
                //List<string> ViaDep = new List<string>();
                //for (int s = 1; s < Sector.Split(',').Count(); s++)
                //{
                //    ViaDep.Add(Sector.Split(',')[s].Split(':')[0]);
                //}
                //while (j <= segcount)
                //{
                //    string x = ""; //For reversing
                //    if (j == 2 || isInbound == true)
                //    {
                //        x = BookedDep;
                //        BookedDep = BookedArr;
                //        BookedArr = x;
                //        //DepDate = ArrDate;
                //    }
                //    Req.Append("<ticketRequestedSegments>");
                //    Req.Append("<actionIdentification>");
                //    Req.Append("<actionRequestCode>C</actionRequestCode>");
                //    Req.Append("</actionIdentification>");
                //    Req.Append("<connectPointDetails>");
                //    Req.Append("<connectionDetails>");
                //    Req.Append("<location>" + BookedDep + "</location>");
                //    Req.Append("</connectionDetails>");
                //    foreach (var via in ViaDep)
                //    {
                //        Req.Append("<connectionDetails>");
                //        Req.Append("<location>" + via + "</location>");
                //        Req.Append("</connectionDetails>");
                //    }
                //    Req.Append("<connectionDetails>");
                //    Req.Append("<location>" + BookedArr + "</location>");
                //    Req.Append("</connectionDetails>");
                //    Req.Append("</connectPointDetails>");
                //    Req.Append("</ticketRequestedSegments>");
                //    j++;
                //}
                //#endregion


                Req.Append("</ticketChangeInfo>");
                #endregion
                Req.Append("</Ticket_ATCShopperMasterPricerTravelBoardSearch>");
                Req.Append("</soapenv:Body>");
                #endregion
                Req.Append("</soapenv:Envelope>");

                string folder = "MPTB/" + obj.Adult + obj.Child + obj.Infant + "_" + obj.HidTxtAirLine + "_" + obj.HidTxtDepCity + obj.HidTxtArrCity + obj.DepDate.Replace("/", "-") + "_" + obj.RetDate.Replace("/", "") + "_" + obj.Trip + "_" + obj.TripType + "_" + DateTime.Now.ToString("hh_mm_ss");

                Response = AmdUtility.PostXml(Req.ToString(), ServiceUrls.ATCActionMasterPricer, svcUrl, "ATCMasterPricerRequest", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");
            }
            catch (Exception ex)
            {
            }
            //finally
            //{
            //    //AmdUtility.SaveFile(Req.ToString(), "MasterPricerRequest");
            //    //AmdUtility.SaveFile(Response, "MasterPricerRespose");

            //}
            return Response;
        }
        public ArrayList ParseFltDetailsATC(string Flt_Res)
        {
            ArrayList FltIndex = new ArrayList();
            XDocument main = XDocument.Load(new StringReader(Flt_Res.Replace("xmlns=\"http://xml.amadeus.com/FMTCTR_13_1_1A\"", "")));
            XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
            var root = main.Descendants(soap + "Body").Descendants("Ticket_ATCShopperMasterPricerTravelBoardSearchReply").Descendants("flightIndex");
            #region
            if (root.Count() > 0) //Response doesn't Contain Error
            {
                try
                {
                    foreach (var r in root)
                    {
                        List<Flights_1A> Flt = new List<Flights_1A>();
                        string Seg_Ref = r.Element("requestedSegmentRef").Element("segRef").Value;
                        string Flt_ref = null;
                        string Eft_Ref = null;
                        string Mcx_Ref = null;
                        var Flts = r.Descendants("groupOfFlights");
                        foreach (var f in Flts)
                        {
                            #region propFlightGrDetail
                            Flt_ref = "S" + f.Element("propFlightGrDetail").Elements("flightProposal").ElementAt(0).Element("ref").Value;
                            Eft_Ref = f.Element("propFlightGrDetail").Elements("flightProposal").ElementAt(1).Element("ref").Value;
                            Mcx_Ref = f.Element("propFlightGrDetail").Elements("flightProposal").ElementAt(2).Element("ref").Value;
                            #endregion
                            #region flight Details
                            var legs = f.Descendants("flightDetails");
                            foreach (var l in legs)
                            {
                                Flights_1A obj = new Flights_1A();
                                obj.DateOfDeparture = l.Element("flightInformation").Element("productDateTime").Element("dateOfDeparture").Value;
                                obj.TimeOfDeparture = l.Element("flightInformation").Element("productDateTime").Element("timeOfDeparture").Value;
                                obj.DateOfArrival = l.Element("flightInformation").Element("productDateTime").Element("dateOfArrival").Value;
                                obj.TimeOfArrival = l.Element("flightInformation").Element("productDateTime").Element("timeOfArrival").Value;
                                obj.LocationId1 = l.Element("flightInformation").Elements("location").ElementAt(0).Element("locationId").Value;
                                if (l.Element("flightInformation").Elements("location").ElementAt(0).Element("terminal") != null)
                                {
                                    obj.LocationId1T = l.Element("flightInformation").Elements("location").ElementAt(0).Element("terminal").Value;
                                }
                                if (l.Element("flightInformation").Elements("location").ElementAt(1).Element("locationId") != null)
                                {
                                    obj.LocationId2 = l.Element("flightInformation").Elements("location").ElementAt(1).Element("locationId").Value;
                                }

                                obj.MarketingCarrier = l.Element("flightInformation").Element("companyId").Element("marketingCarrier").Value;
                                try
                                {
                                    if (l.Element("flightInformation").Element("companyId").Element("operatingCarrier") != null)
                                    {
                                        obj.OperatingCarrier = l.Element("flightInformation").Element("companyId").Element("operatingCarrier").Value;
                                    }
                                    else { obj.OperatingCarrier = ""; }
                                }
                                catch { obj.OperatingCarrier = ""; }
                                obj.FlightNumber = l.Element("flightInformation").Element("flightOrtrainNumber").Value;
                                obj.EquipmentType = l.Element("flightInformation").Element("productDetail").Element("equipmentType").Value;
                                obj.ElectronicTicketing = l.Element("flightInformation").Element("addProductDetail").Element("electronicTicketing").Value;
                                try
                                {
                                    obj.ProductDetailQualifier = l.Element("flightInformation").Element("addProductDetail").Element("productDetailQualifier").Value;

                                }
                                catch { obj.ProductDetailQualifier = ""; }
                                obj.Flt_ref = Flt_ref;
                                obj.Eft_Ref = Eft_Ref;
                                obj.Mcx_Ref = Mcx_Ref;
                                obj.Seg_Ref = Seg_Ref;
                                obj.ValidatingCarrier = Mcx_Ref;
                                Flt.Add(obj);
                            }

                            #endregion
                        }
                        FltIndex.Add(Flt);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion
            return FltIndex;
        }
        public async Task<List<FlightSearchResults>> ParsePriceDetailsATC(string Flt_Res, string JourneyType1, ArrayList Flights, List<FltSrvChargeList> SrvchargeList, List<FlightCityList> CityList, List<AirlineList> AirList, DataSet MarkupDs, FlightSearch searchInputs, List<MISCCharges> MiscList, bool inbound, bool isPrivateFare, CORP_ENTY_CONFIG CpOrEtConfig, int FltSecNum = 1)
        {
            XDocument main = XDocument.Load(new StringReader(Flt_Res.Replace("xmlns=\"http://xml.amadeus.com/FMTCTR_13_1_1A\"", "")));
            XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
            var root = main.Descendants(soap + "Body").Descendants("Ticket_ATCShopperMasterPricerTravelBoardSearchReply").Descendants("recommendation");
            List<FlightSearchResults> Final = new List<FlightSearchResults>();
            #region Variables
            string FareRul = "";
            List<List<LegFareDtls_1A>> AllLeg = new List<List<LegFareDtls_1A>>();
            int StartLineNO = 1;
            bool farecnt = false;
            string MNR_XML = "";
            try
            {
                MNR_XML = XDocument.Parse(AmdUtility.RemoveAllNamespaces(main.Descendants(soap + "Body").Elements().FirstOrDefault()).ToString()).Element("Ticket_ATCShopperMasterPricerTravelBoardSearchReply").Element("mnrGrp").ToString();
            }
            catch (Exception es) { }


            #endregion
            try
            {
                //Loop for each recomendation
                foreach (var r in root)
                {

                    #region Variables For Each Recommendation
                    int Rec_no = 0;
                    // string RefQ_NO1 = "";
                    int SegRefCount = 0;
                    int legcountOB = 0, legCountIB = 0;
                    ArrayList gpfare = new ArrayList();

                    string PtcTyp = "";
                    int PtcAdt = 0;
                    int PtcChd = 0;
                    int PtcInf = 0;
                    //Fare Variables
                    Hashtable Adt_FR = new Hashtable();
                    Hashtable Chd_FR = new Hashtable();
                    Hashtable Inf_FR = new Hashtable();
                    string Tot_Fare = "";
                    string Tot_Tax = "";
                    string Tot_YQ = "";
                    string Tot_YR = "";
                    string Tot_WO = "";
                    string Tot_IN = "";
                    string Tot_JN = "";
                    string Tot_Q = "";
                    string Pax_Tq = ""; //Transport Qualifier
                    string Pax_Ref = "";
                    string Pax_Yq = "";
                    string Pax_Yr = "";
                    string Pax_Q = "";
                    string Pax_WO = "";
                    string Pax_IN = "";
                    string Pax_JN = "";
                    string FareType = "";
                    string FareCode = "";

                    string BRefQualifierOne = "";
                    string BRefQualifierRound = "";
                    #endregion
                    List<LegFareDtls_1A> Seg1List = new List<LegFareDtls_1A>();
                    XElement s = r;
                    XDocument a = XDocument.Parse(AmdUtility.RemoveAllNamespaces(r).ToString());
                    Rec_no = int.Parse(a.Element("recommendation").Element("itemNumber").Element("itemNumberId").Element("number").Value);
                    #region recPriceInfo
                    var md = a.Element("recommendation").Element("recPriceInfo").Descendants("monetaryDetail");
                    decimal amt1 = 0, amt2 = 0;
                    string sliceAndDiceXML = "";

                    //if (a.Element("recommendation").Descendants("specificRecDetails").Any())
                    //{
                    //    a.Element("recommendation").Descendants("specificRecDetails").ToList().ForEach(x => { sliceAndDiceXML = sliceAndDiceXML + x.ToString(); });
                    //}
                    //else
                    //{
                    //    sliceAndDiceXML = "";
                    //}

                    foreach (var m in md)
                    {
                        string v = "";
                        if (m.Elements("amountType").Any() == true)// check if AmountType node exsists
                        {
                            v = m.Element("amountType").Value.Trim();
                            if (v == "YQ")
                                Tot_YQ = m.Element("amount").Value.Trim();
                            else if (v == "YR")
                                Tot_YR = m.Element("amount").Value.Trim();
                            else if (v == "WO")
                                Tot_WO = m.Element("amount").Value.Trim();
                            else if (v == "IN")
                                Tot_IN = m.Element("amount").Value.Trim();
                            else if (v == "JN")
                                Tot_JN = m.Element("amount").Value.Trim();
                            else if (v == "Q")
                                Tot_Q = m.Element("amount").Value.Trim();
                        }
                        else if (amt1 == 0)
                        {
                            amt1 = decimal.Parse(m.Element("amount").Value);
                        }
                        else
                        {
                            amt2 = decimal.Parse(m.Element("amount").Value);
                        }
                    }
                    if (amt1 > amt2)
                        Tot_Fare = amt1.ToString();
                    else Tot_Fare = amt2.ToString();
                    #endregion
                    #region segmentFlightRef  - Call it in Last to Filter Details and Prepare List
                    var SegFRef = a.Element("recommendation").Descendants("segmentFlightRef");




                    List<string> SegFRef_OB = new List<string>(); List<string> SegFRef_IB = new List<string>();

                    List<string> SegRef_SD = new List<string>();
                    SegRefCount = SegFRef.Count();
                    foreach (var sref in SegFRef)
                    {

                        for (int j = 0; j < sref.Elements("referencingDetail").Count(); j++) //Round Trip
                        {
                            if (j == 0)
                            {

                                SegFRef_OB.Add(sref.Elements("referencingDetail").ElementAt(j).Element("refQualifier").Value + sref.Elements("referencingDetail").ElementAt(j).Element("refNumber").Value);
                                if (sref.Elements("referencingDetail").Descendants("refQualifier").Where(x => x.Value == "A").Any())
                                {

                                    SegRef_SD.Add(sref.Elements("referencingDetail").Where(x => x.Element("refQualifier").Value == "A").FirstOrDefault().Element("refNumber").Value);
                                }
                                else
                                {
                                    SegRef_SD.Add("");
                                }
                            }
                            else if (j == 1) { SegFRef_IB.Add(sref.Elements("referencingDetail").ElementAt(j).Element("refQualifier").Value + sref.Elements("referencingDetail").ElementAt(j).Element("refNumber").Value); }
                        }
                    }
                    #endregion
                    #region PaxFareProduct  - Find Fare Per Pax
                    var pxfare = a.Element("recommendation").Descendants("paxFareProduct");
                    StringBuilder GroupFare = new StringBuilder();// For Extracting group Fare
                    GroupFare.Append("<?xml version='1.0' encoding='utf-16'?>").Append("<gp>");
                    foreach (var px in pxfare)
                    {
                        #region Evaluate Fare Per Pax
                        string paxcnt = px.Element("paxReference").Elements("traveller").Count().ToString();
                        #region paxFareDetail -- Find fare
                        Pax_Ref = px.Element("paxFareDetail").Element("paxFareNum").Value;
                        PtcTyp = px.Element("paxReference").Element("ptc").Value;
                        Pax_Tq = px.Element("paxFareDetail").Element("codeShareDetails").Element("transportStageQualifier").Value;
                        Tot_Fare = px.Element("paxFareDetail").Element("totalFareAmount").Value;
                        Tot_Tax = px.Element("paxFareDetail").Element("totalTaxAmount").Value;
                        try
                        { Pax_Yq = px.Element("paxFareDetail").Descendants("monetaryDetails").First(p => p.Element("amountType").Value == "YQ").Element("amount").Value; }
                        catch { Pax_Yq = "0"; }
                        try { Pax_Yr = px.Element("paxFareDetail").Descendants("monetaryDetails").First(p => p.Element("amountType").Value == "YR").Element("amount").Value; }
                        catch { Pax_Yr = "0"; }
                        try { Pax_Q = px.Element("paxFareDetail").Descendants("monetaryDetails").First(p => p.Element("amountType").Value == "Q").Element("amount").Value; }
                        catch { Pax_Q = "0"; }
                        try { Pax_WO = px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "WO").Element("rate").Value; }
                        catch { Pax_WO = "0"; }
                        try { Pax_IN = px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "IN").Element("rate").Value; }
                        catch { Pax_IN = "0"; }
                        try { Pax_JN = px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "K3").Element("rate").Value; }
                        catch { Pax_JN = "0"; }
                        try { Pax_IN = (Convert.ToInt16(Pax_IN) + Convert.ToInt16(px.Element("passengerTaxDetails").Descendants("taxDetails").First(p => p.Element("type").Value == "YM").Element("rate").Value.ToString())).ToString(); }
                        catch { }

                        FareType = px.Element("fare").Elements("pricingMessage").ElementAt(0).Element("description").Value;
                        FareCode = px.Element("fare").Elements("pricingMessage").Elements("freeTextQualification").ElementAt(0).Element("informationType").Value;
                        #endregion
                        if (PtcTyp == "ADT")
                        {
                            PtcAdt = px.Element("paxReference").Elements("traveller").Count();
                            Adt_FR.Add("Type", PtcTyp);
                            Adt_FR.Add("Pax_Tq", Pax_Tq);
                            Adt_FR.Add("Tot_Fare", Tot_Fare);
                            Adt_FR.Add("Tot_Tax", Tot_Tax);
                            Adt_FR.Add("Pax_Yq", Pax_Yq);
                            Adt_FR.Add("Pax_Yr", Pax_Yr);
                            Adt_FR.Add("Pax_Q", Pax_Q);
                            Adt_FR.Add("Pax_WO", Pax_WO);
                            Adt_FR.Add("Pax_IN", Pax_IN);
                            Adt_FR.Add("Pax_JN", Pax_JN);
                            Adt_FR.Add("Fare_Type", FareType);
                            Adt_FR.Add("Fare_Code", FareCode);
                        }
                        else if (PtcTyp == "CH")
                        {
                            PtcChd = px.Element("paxReference").Elements("traveller").Count();
                            Chd_FR.Add("Type", PtcTyp);
                            Chd_FR.Add("Pax_Tq", Pax_Tq);
                            Chd_FR.Add("Tot_Fare", Tot_Fare);
                            Chd_FR.Add("Tot_Tax", Tot_Tax);
                            Chd_FR.Add("Pax_Yq", Pax_Yq);
                            Chd_FR.Add("Pax_Yr", Pax_Yr);
                            Chd_FR.Add("Pax_Q", Pax_Q);
                            Chd_FR.Add("Pax_WO", Pax_WO);
                            Chd_FR.Add("Pax_IN", Pax_IN);
                            Chd_FR.Add("Pax_JN", Pax_JN);
                        }
                        else if (PtcTyp == "INF")
                        {
                            PtcInf = px.Element("paxReference").Elements("traveller").Count();
                            Inf_FR.Add("Type", PtcTyp);
                            Inf_FR.Add("Pax_Tq", Pax_Tq);
                            Inf_FR.Add("Tot_Fare", Tot_Fare);
                            Inf_FR.Add("Tot_Tax", Tot_Tax);
                            Inf_FR.Add("Pax_Yq", Pax_Yq);
                            Inf_FR.Add("Pax_Yr", Pax_Yr);
                            Inf_FR.Add("Pax_Q", Pax_Q);
                            Inf_FR.Add("Pax_WO", Pax_WO);
                            Inf_FR.Add("Pax_IN", Pax_IN);
                            Inf_FR.Add("Pax_JN", Pax_JN);
                        }
                        //FareRul = "-" + PtcTyp + "l-";
                        #region fare
                        //if (px.Elements("fare").Count() >= 2)
                        //{
                        //    farecnt = true;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("textSubjectQualifier").Value;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("informationType").Value;
                        //    FareRul = FareRul + "/" + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("description").Value + "-";
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(1).Element("pricingMessage").Element("freeTextQualification").Element("textSubjectQualifier").Value;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(1).Element("pricingMessage").Element("freeTextQualification").Element("informationType").Value;
                        //    FareRul = FareRul + "/" + px.Elements("fare").ElementAt(1).Element("pricingMessage").Element("description").Value + "-";
                        //}
                        //else
                        //{
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("textSubjectQualifier").Value;
                        //    FareRul = FareRul + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("freeTextQualification").Element("informationType").Value;
                        //    FareRul = FareRul + "/" + px.Elements("fare").ElementAt(0).Element("pricingMessage").Element("description").Value + "-";
                        //}
                        #endregion
                        #region fareDetails - Find rbd,etc - For Each Leg(<groupOfFares>)
                        int seg = 1;
                        if (JourneyType1 == "O")
                        {
                            GroupFare.Append("<Segment id='" + seg + "'>");
                            GroupFare.Append(System.String.Concat(px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").ToList()));
                            GroupFare.Append("</Segment>");
                            legcountOB = px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").Count(); //px.Element("fareDetails").Descendants("groupOfFares").Count();
                        }
                        else if (JourneyType1 == "R")
                        {

                            var fd = px.Descendants("fareDetails");
                            foreach (var f in fd)
                            {
                                GroupFare.Append("<Segment id='" + seg + "'>");
                                GroupFare = GroupFare.Append(string.Concat(px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").ToList()));
                                GroupFare.Append("</Segment>");
                                if (seg == 1) { legcountOB = px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").Count(); }
                                if (seg == 2) { legCountIB = px.Elements("fareDetails").ElementAt(seg - 1).Descendants("groupOfFares").Count(); }
                                seg++;
                            }
                        }
                        #endregion
                        #endregion
                    }
                    GroupFare.Append("</gp>");
                    #endregion
                    #region Find Leg Details
                    List<LegFareDtls_1A> LegOB = null; List<LegFareDtls_1A> LegIB = null;
                    XDocument gn = XDocument.Parse(GroupFare.ToString());
                    if (JourneyType1 == "O")
                    {
                        IEnumerable<XElement> OB = from s1 in gn.Element("gp").Elements("Segment").Where(s1 => (string)s1.Attribute("id") == "1") select s1;
                        LegOB = GetLegDetails(OB, legcountOB, PtcAdt, PtcChd, PtcInf, Rec_no);

                    }
                    else if (JourneyType1 == "R")
                    {
                        IEnumerable<XElement> OB = from s1 in gn.Element("gp").Elements("Segment").Where(s1 => (string)s1.Attribute("id") == "1") select s1;
                        LegOB = GetLegDetails(OB, legcountOB, PtcAdt, PtcChd, PtcInf, Rec_no);
                        IEnumerable<XElement> IB = from s1 in gn.Element("gp").Elements("Segment").Where(s1 => (string)s1.Attribute("id") == "2") select s1;
                        LegIB = GetLegDetails(IB, legCountIB, PtcAdt, PtcChd, PtcInf, Rec_no);
                    }
                    #endregion
                    #region List Variables
                    Hashtable Markup = new Hashtable();
                    Hashtable CommGal = new Hashtable();//For Comm
                    Hashtable STXTFee = new Hashtable(); //For Srv. Tax
                    Hashtable TDS = new Hashtable(); //For TDS
                    string arrCity = "", depCity = "";
                    int schd = 0;
                    if (schd == 0)
                    {
                        depCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                        arrCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                    }
                    else
                    {
                        depCity = Utility.Left(searchInputs.HidTxtArrCity, 3);
                        arrCity = Utility.Left(searchInputs.HidTxtDepCity, 3);
                    }
                    List<ResListItem> FlightList = new List<ResListItem>();
                    #endregion
                    #region OneWay List
                    if (JourneyType1 == "O")
                    {
                        try
                        {
                            #region Map to Flights
                            //Find No of flights for this Fare
                            List<Flights_1A> FltOB = (List<Flights_1A>)Flights[0];
                            List<Flights_1A> flt = (from Flights_1A f in FltOB //(List<Flights_1A>)FltOB.Select(lgk => lgk.Flt_ref == seg).ToList();
                                                    from segv1 in SegFRef_OB
                                                    where f.Flt_ref == segv1
                                                    select f).ToList();
                            List<FlightSearchResults> Ob = new List<FlightSearchResults>();
                            Ob = AddLegstoFlt(LegOB, flt, null, null, SrvchargeList, CityList, AirList, MarkupDs, searchInputs, depCity, arrCity, Adt_FR, Chd_FR, Inf_FR, Rec_no, StartLineNO, MiscList, a.Element("recommendation").ToString(), Flt_Res, SegRef_SD, inbound, SegFRef_OB, SegFRef_IB, MNR_XML, isPrivateFare, CpOrEtConfig, FltSecNum);
                            foreach (FlightSearchResults f in Ob)
                            {

                                Final.Add(f);
                            }

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    #endregion
                    #region RoundTrip List
                    else if (JourneyType1 == "R")
                    {
                        #region Map to Flights
                        List<Flights_1A> FltOB = (List<Flights_1A>)Flights[0];
                        List<Flights_1A> flt1 =
                        (from Flights_1A f in FltOB
                         from segv1 in SegFRef_OB
                         where f.Flt_ref == segv1
                         select f).ToList();
                        #endregion
                        #region Map to Flights
                        List<Flights_1A> FltIB = (List<Flights_1A>)Flights[1];
                        List<Flights_1A> flt2 = (from Flights_1A f in FltIB
                                                 from segv2 in SegFRef_IB
                                                 where f.Flt_ref == segv2
                                                 select f).ToList();
                        #endregion
                        List<FlightSearchResults> FltList = new List<FlightSearchResults>();
                        FltList = AddLegstoFlt(LegOB, flt1, LegIB, flt2, SrvchargeList, CityList, AirList, MarkupDs, searchInputs, depCity, arrCity, Adt_FR, Chd_FR, Inf_FR, Rec_no, StartLineNO, MiscList, a.Element("recommendation").ToString(), Flt_Res, SegRef_SD, inbound, SegFRef_OB, SegFRef_IB, MNR_XML, isPrivateFare, CpOrEtConfig, FltSecNum);//.Where(p=>p.TripType=="R").ToList()
                        foreach (FlightSearchResults f in FltList)
                        {

                            Final.Add(f);
                        }
                    }
                    #endregion
                    StartLineNO = StartLineNO + SegRefCount;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            await Task.Delay(0);
            return Final;
        }

        public ArrayList ParseFltDetailsATCNew(string Flt_Res)
        {
            ArrayList FltIndex = new ArrayList();
            XDocument main = XDocument.Load(new StringReader(Flt_Res.Replace("xmlns=\"http://xml.amadeus.com/FMPTBR_18_1_1A\"", "")));
            XNamespace soap = XNamespace.Get("http://schemas.xmlsoap.org/soap/envelope/");
            var root = main.Descendants(soap + "Body").Descendants("Fare_MasterPricerTravelBoardSearchReply").Descendants("flightIndex");
            #region
            if (root.Count() > 0) //Response doesn't Contain Error
            {
                try
                {
                    foreach (var r in root)
                    {
                        List<Flights_1A> Flt = new List<Flights_1A>();
                        string Seg_Ref = r.Element("requestedSegmentRef").Element("segRef").Value;
                        string Flt_ref = null;
                        string Eft_Ref = null;
                        string Mcx_Ref = null;
                        var Flts = r.Descendants("groupOfFlights");
                        foreach (var f in Flts)
                        {
                            #region propFlightGrDetail
                            Flt_ref = "S" + f.Element("propFlightGrDetail").Elements("flightProposal").ElementAt(0).Element("ref").Value;
                            Eft_Ref = f.Element("propFlightGrDetail").Elements("flightProposal").ElementAt(1).Element("ref").Value;
                            Mcx_Ref = f.Element("propFlightGrDetail").Elements("flightProposal").ElementAt(2).Element("ref").Value;
                            #endregion
                            #region flight Details
                            var legs = f.Descendants("flightDetails");
                            foreach (var l in legs)
                            {
                                Flights_1A obj = new Flights_1A();
                                string deptime = ""; string arrtime = "";
                                deptime = l.Element("flightInformation").Element("productDateTime").Element("timeOfDeparture").Value;
                                arrtime = l.Element("flightInformation").Element("productDateTime").Element("timeOfArrival").Value;
                                obj.DateOfDeparture = l.Element("flightInformation").Element("productDateTime").Element("dateOfDeparture").Value;
                              
                                obj.TimeOfDeparture = deptime.Substring(0, 2) +":"+ deptime.Substring(2, 2);
                                obj.DateOfArrival = l.Element("flightInformation").Element("productDateTime").Element("dateOfArrival").Value;
                                obj.TimeOfArrival = l.Element("flightInformation").Element("productDateTime").Element("timeOfArrival").Value;
                                obj.TimeOfArrival = arrtime.Substring(0, 2) + ":" + arrtime.Substring(2, 2);
                                obj.LocationId1 = l.Element("flightInformation").Elements("location").ElementAt(0).Element("locationId").Value;
                                if (l.Element("flightInformation").Elements("location").ElementAt(0).Element("terminal") != null)
                                {
                                    obj.LocationId1T = l.Element("flightInformation").Elements("location").ElementAt(0).Element("terminal").Value;
                                }
                                if (l.Element("flightInformation").Elements("location").ElementAt(1).Element("locationId") != null)
                                {
                                    obj.LocationId2 = l.Element("flightInformation").Elements("location").ElementAt(1).Element("locationId").Value;
                                }

                                obj.MarketingCarrier = l.Element("flightInformation").Element("companyId").Element("marketingCarrier").Value;
                                try
                                {
                                    if (l.Element("flightInformation").Element("companyId").Element("operatingCarrier") != null)
                                    {
                                        obj.OperatingCarrier = l.Element("flightInformation").Element("companyId").Element("operatingCarrier").Value;
                                    }
                                    else { obj.OperatingCarrier = ""; }
                                }
                                catch { obj.OperatingCarrier = ""; }
                                obj.FlightNumber = l.Element("flightInformation").Element("flightOrtrainNumber").Value;
                                obj.EquipmentType = l.Element("flightInformation").Element("productDetail").Element("equipmentType").Value;
                                obj.ElectronicTicketing = l.Element("flightInformation").Element("addProductDetail").Element("electronicTicketing").Value;
                                try
                                {
                                    obj.ProductDetailQualifier = l.Element("flightInformation").Element("addProductDetail").Element("productDetailQualifier").Value;

                                }
                                catch { obj.ProductDetailQualifier = ""; }
                                obj.Flt_ref = Flt_ref;
                                obj.Eft_Ref = Eft_Ref;
                                obj.Mcx_Ref = Mcx_Ref;
                                obj.Seg_Ref = Seg_Ref;
                                obj.ValidatingCarrier = Mcx_Ref;
                                Flt.Add(obj);
                            }

                            #endregion
                        }
                        FltIndex.Add(Flt);
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            #endregion
            return FltIndex;
        }
        #endregion
        public string SerializeAnObject(object obj)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            try
            {
                serializer.Serialize(stream, obj);
                stream.Position = 0;
                doc.Load(stream);
                return doc.InnerXml;
            }
            catch
            {
                throw;
            }
            finally
            {
                stream.Close();
                stream.Dispose();
            }
        }
    }
}
