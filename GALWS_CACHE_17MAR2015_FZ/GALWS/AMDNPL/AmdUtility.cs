﻿using System;
using System.Linq;
using System.Text;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Linq;


namespace GALWS.AMDNPL
{
    public class AmdUtility
    {
        public byte[] ConcatArrays(byte[] array1, byte[] array2)
        {
            byte[] newArray = new byte[array1.Length + array2.Length];

            Array.Copy(array1, 0, newArray, 0, array1.Length);

            Array.Copy(array2, 0, newArray, array1.Length, array2.Length);

            return newArray;
        }



        public static byte[] Compress(byte[] data)
        {
            MemoryStream ms = new MemoryStream();
            DeflateStream ds = new DeflateStream(ms, CompressionMode.Compress);
            ds.Write(data, 0, data.Length);
            ds.Flush();
            ds.Close();
            return ms.ToArray();
        }
        public static string PostXml(String XMLIn, String Module_Version, string SvcUrl, string name, string folder)
        {
            string rawResponse = string.Empty;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(XMLIn);
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(SvcUrl);
                request.Proxy = null;
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://production.webservices.amadeus.com");
                request.Headers.Add(String.Format("SOAPAction: \"{0}\"", Module_Version));
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                //request.Headers.Add(HttpRequestHeader.AcceptEncoding, "deflate");
                //request.Headers.Add(HttpRequestHeader.AcceptEncoding, "br");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                request.Method = "POST";
                request.KeepAlive = true;
                request.ContentLength = bytes.Length;
                request.ContentType = @"text/xml";
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Timeout = 3000000;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    rawResponse = GetWebResponseString(response);
                    return rawResponse;
                }
            }
            catch (WebException wex)
            {
                // We issued an HttpWebRequest, so the response will be an HttpWebResponse.
                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;

                //// Grab the response stream.           
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        // Grab the body of the response as a string for parsing.
                        rawResponse = GetWebResponseString(httpResponse);
                        //IBE.Shared.LogUtility.LogInfo(rawResponse, "SoapFaults", IBE.Shared.Utilities.GetCurrentTimestamp());[commented HS 11-10-2011]
                        //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, wex, "698", rawResponse, string.Empty, "");
                        //throw wex;
                    }
                }
            }

            catch (Exception ex)
            {
                //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, ex, "699", rawResponse, string.Empty, "");
                throw ex;
            }
            finally
            {
                SaveFile(XMLIn, name + "_Req", folder);
                SaveFile(rawResponse, name + "_Res", folder);
            }


            return rawResponse;
        }

        public static string GetUrlNew(string Url, string PostData, bool GZip)
        {
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            if (GZip)
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

            if (!string.IsNullOrEmpty(PostData))
            {
                Http.Method = "POST";
                byte[] lbPostBuffer = Encoding.Default.GetBytes(PostData);

                Http.ContentLength = lbPostBuffer.Length;

                Stream PostStream = Http.GetRequestStream();
                PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                PostStream.Close();
            }

            HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse();

            Stream responseStream = responseStream = WebResponse.GetResponseStream();
            if (WebResponse.ContentEncoding.ToLower().Contains("gzip"))
                responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
            else if (WebResponse.ContentEncoding.ToLower().Contains("deflate"))
                responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);

            StreamReader Reader = new StreamReader(responseStream, Encoding.Default);

            string Html = Reader.ReadToEnd();

            WebResponse.Close();
            responseStream.Close();

            return Html;
        }

        public static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            Stream streamResponse;
            string aa = "";
            //System.IO.StreamReader sr = new System.IO.StreamReader(myHttpWebResponse.GetResponseStream());

            //string aa = sr.ReadToEnd().Trim();
            using (streamResponse = myHttpWebResponse.GetResponseStream())
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip") || myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                {

                    if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                        streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                    else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                        streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);


                    using (StreamReader streamRead = new StreamReader(streamResponse))
                    {
                        Char[] readBuffer = new Char[myHttpWebResponse.ContentLength];
                        int count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength));

                        while (count > 0)
                        {
                            String resultData = new String(readBuffer, 0, count);
                            rawResponse.Append(resultData);
                            count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength));
                        }
                    }

                    aa = rawResponse.ToString();
                }
                else
                {
                    System.IO.StreamReader sr = new System.IO.StreamReader(streamResponse);
                    aa = sr.ReadToEnd().Trim();
                }
            }
            return aa;
        }

        public static XElement RemoveAllNamespaces(XElement xmlDocument)
        {
            if (!xmlDocument.HasElements)
            {
                XElement xElement = new XElement(xmlDocument.Name.LocalName);
                xElement.Value = xmlDocument.Value;
                return xElement;
            }
            return new XElement(xmlDocument.Name.LocalName, xmlDocument.Elements().Select(el => RemoveAllNamespaces(el)));
        }

        public static string StripXmlWhitespace(string Xml, string Folder, string filename)
        {
            //string activeDir = Folder;
            //filename = activeDir + "/" + filename;
            //DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            //if (!Directory.Exists(objDirectoryInfo.FullName))
            //{
            //    Directory.CreateDirectory(activeDir);
            //}

            //var Files = Directory.GetFiles(activeDir);
            //var fl = Files[0];//from f in Files.Select(f=>f.ToString() == filename)
            if (Xml != "")
            {
                Regex Parser = new Regex(@">\s*<");
                Xml = Parser.Replace(Xml, "><");
            }
            else
            {
            }
            return Xml.Trim();
        }
        public static void SaveFile(string Res, string module, string folder)
        {
            string newFileName = module + DateTime.Now.ToString("_HH_mm_ss");

            string[] folderarry = folder.Split('/');
            string activeDir = "";

            try
            {
                if (folderarry.Length > 1)
                {
                    activeDir = ConfigurationManager.AppSettings["AMDSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy");
                    for (int i = 0; i < folderarry.Length; i++)
                    {
                        if (folderarry[i] != "")
                        {
                            activeDir += @"\" + folderarry[i] + @"\";
                        }

                    }
                    // activeDir = ConfigurationManager.AppSettings["RWSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\" + folderarry[0] + @"\" + folderarry[1] + @"\";// +securityToken + @"\";
                }
                else
                {
                    activeDir = ConfigurationManager.AppSettings["AMDSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\" + folderarry[0] + @"\";// +securityToken + @"\";
                }
            }
            catch
            {
                activeDir = ConfigurationManager.AppSettings["AMDSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\" + folder + @"\";// +securityToken + @"\";
            }

            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }
            try
            {
                //XDocument xmlDoc = XDocument.Parse(Res);

                //xmlDoc.Save(activeDir + @"\" + filename + ".xml");

                string path = activeDir + newFileName + ".xml";//@"c:\temp\MyTest.txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {

                        sw.Write(Res);

                    }
                }



            }
            catch (Exception ex)
            {
            }
        }
    }
    public enum IncludeSessionType
    {
        NO,
        Start,
        InSeries,
        End
    }
}
