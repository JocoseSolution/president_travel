﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.AMDNPL
{
    public class FareRulesFlight
    {
        public string PaxType { get; set; }
        public string DepartureType { get; set; }
        public string Rule { get; set; }
        public string FeeCurrency { get; set; }
        public string FeeAmount { get; set; }
        public string FareBasis { get; set; }
        public bool IsRefundable { get; set; }
        public string Remarks { get; set; }
        public string Cabin { get; set; }
        public string Segment { get; set; }    
    }

    public class UAPIAirFR  
    {
        public string IdType { get; set; }
        public string Segment { get; set; }
    }
}
