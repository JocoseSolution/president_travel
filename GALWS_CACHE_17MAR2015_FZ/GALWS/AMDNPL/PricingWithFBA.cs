﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Xml.Linq;
using System.Net;
using System.IO;
using System.IO.Compression;
using GALWS.AMDNPL;

namespace STD.BAL
{
    public class PricingWithFBA
    {
        public Hashtable Pricing_FBA(string SessionId, string SoapAction, string VC, DataSet FltDsGAL, string Pax_Res, string username, string officeID, string pass, string svcUrl, IncludeSessionType includeSessionType)
        {
            string fare_req = "";
            string fare_res = "";
            int icnt = 0;
            try
            {
                ArrayList refq = new ArrayList();
                try
                {
                    for (int i = 0; i <= FltDsGAL.Tables[0].Rows.Count - 1; i++)
                    {
                        if (i == 0)
                        {
                            refq.Add("S");
                        }
                        else
                        {
                            refq.Add("S");
                        }
                    }
                }
                catch (Exception ex)
                {
                }
                Hashtable stRef = new Hashtable();
                Pax_Res = Pax_Res.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_11_1_1A\"", "");
                XDocument XPRes = XDocument.Parse(Pax_Res);
                int cntr = 0;
                var reff = from REF in XPRes.Descendants("PNR_Reply").Descendants("originDestinationDetails").Descendants("itineraryInfo")
                           select new
                  {

                      STNumber = REF.Descendants("elementManagementItinerary").Descendants("reference").Elements("number").First().Value,
                  };
                foreach (var reffno in reff)
                {

                    stRef.Add(cntr, reffno.STNumber);
                    cntr = cntr + 1;

                }
            PRC_FBA:
                fare_req = Price_PNR_New_FBA(SessionId,SoapAction, VC, FltDsGAL, stRef, refq,  username,  officeID,  pass,  svcUrl,  includeSessionType);
                fare_res = PostXml(fare_req, SoapAction);
                if (fare_res.Contains("<errorText>") == true)
                {
                    if (refq.Count == 1)
                    {
                        goto PRC_FBA1;
                    }
                    else if (refq.Count == 2)
                    {
                        if (icnt > 1)
                            goto PRC_FBA1;
                        if (refq[1].ToString() == "S")
                            refq[1] = "X";
                        else
                            refq[1] = "S";
                        icnt++;
                    }
                    else if (refq.Count == 3)
                    {
                        goto PRC_FBA1;
                        //for (int i = 0; i <= refq.Count - 1; i++)
                        //{
                        //    if (refq[1].ToString() == "S" && refq[2].ToString() == "S")
                        //    {
                        //        refq[1] = "X";
                        //        refq[2] = "S";
                        //    }
                        //    else if (refq[1].ToString() == "X" && refq[2].ToString() == "S")
                        //    {
                        //        refq[1] = "S";
                        //        refq[2] = "X";
                        //    }
                        //    else if (refq[1].ToString() == "S" && refq[2].ToString() == "X")
                        //    {
                        //        refq[1] = "X";
                        //        refq[2] = "X";
                        //    }
                        //}
                    }
                    //goto PRC_FBA;
                }




            }
            catch (Exception ex)
            {
            }
        PRC_FBA1:
            Hashtable Prc = new Hashtable();
            Prc.Add("fare_req", fare_req);
            Prc.Add("fare_res", fare_res);
            return Prc;
        }
        private string GetSessionHeader(string SH)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            string[] ss = Utility.Split(SH, "|");
            XmlBuilder.Append("<wbs:Session>");
            XmlBuilder.Append("<wbs:SessionId>" + ss[0].ToString() + "</wbs:SessionId>");
            XmlBuilder.Append("<wbs:SequenceNumber>" + ss[1].ToString() + "</wbs:SequenceNumber>");
            XmlBuilder.Append("<wbs:SecurityToken>" + ss[2].ToString() + "</wbs:SecurityToken>");
            XmlBuilder.Append("</wbs:Session>");
            return XmlBuilder.ToString();
        }
        public string Price_PNR_New_FBA(string sessionid,string SoapAction, string VC, DataSet FltDsGAL, Hashtable stRef, ArrayList refQ, string username, string officeID, string pass, string svcUrl, IncludeSessionType includeSessionType)
        {
            
            AmdSession objSessuonHeader = new AmdSession();
            StringBuilder fare = new StringBuilder();
            StringBuilder fare1 = new StringBuilder();
            //fare.Append("<?xml version='1.0' encoding='utf-8'?>");
            ////fare.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
            //fare.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:tpc='http://xml.amadeus.com/TPCBRQ_07_3_1A'>");
            //fare.Append("<soapenv:Header>");
            //fare.Append(GetSessionHeader(sessionid)); //fare.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
            //fare.Append("</soapenv:Header>");
            //fare.Append("<soapenv:Body>");
            // StringBuilder XmlBuilder = new StringBuilder();

            fare.Append("<?xml version='1.0' encoding='utf-8'?>");
            //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
            fare.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:tpc='http://xml.amadeus.com/TPCBRQ_07_3_1A'>");
            fare.Append("<soapenv:Header>");
            fare.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));//GetSessionHeader(sessionid)); //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
            fare.Append("</soapenv:Header>");
            fare.Append("<soapenv:Body>");
            fare.Append("<Fare_PricePNRWithBookingClass> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>RP</pricingOptionKey> </pricingOptionKey> </pricingOptionGroup> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>RU</pricingOptionKey> </pricingOptionKey> </pricingOptionGroup> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>RLO</pricingOptionKey> </pricingOptionKey> </pricingOptionGroup> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>VC</pricingOptionKey> </pricingOptionKey> <carrierInformation> <companyIdentification> <otherCompany>" + VC + "</otherCompany> </companyIdentification> </carrierInformation> </pricingOptionGroup> </Fare_PricePNRWithBookingClass>");
            //fare.Append("<Fare_PricePNRWithBookingClass>");

            //fare.Append("<pricingOptionGroup>");
            //for (int i = 0; i <= stRef.Count - 1; i++)
            //{
            //    fare.Append("<pricingOptionKey>");
            //    //fare.Append("<refQualifier>S</refQualifier>");
            //    fare.Append("<pricingOptionKey>" + stRef[i].ToString() + "</pricingOptionKey>");
            //    fare.Append("</pricingOptionKey>");
            //    fare1.Append("<pricingFareBase>");
            //    fare1.Append("<fareBasisOptions>");
            //    fare1.Append("<fareBasisDetails>");
            //    if (FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString().Length <= 3)
            //    {
            //        fare1.Append("<primaryCode>" + FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString() + "</primaryCode>");
            //        fare1.Append("<fareBasisCode></fareBasisCode>");
            //    }
            //    else
            //    {
            //        fare1.Append("<primaryCode>" + STD.BAL.Utility.Left(FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString(), 3) + "</primaryCode>");
            //        fare1.Append("<fareBasisCode>" + FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString().Substring(3, (FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString().Length - 3)) + "</fareBasisCode>");
            //    }
            //    fare1.Append("</fareBasisDetails>");
            //    fare1.Append("</fareBasisOptions>");
            //    fare1.Append("<fareBasisSegReference>");
            //    fare1.Append("<refDetails>");
            //    fare1.Append("<refQualifier>" + refQ[i].ToString() + "</refQualifier>");
            //    fare1.Append("<refNumber>" + stRef[i].ToString() + "</refNumber>");
            //    fare1.Append("</refDetails>");
            //    fare1.Append("</fareBasisSegReference>");
            //    fare1.Append("</pricingFareBase>");
            //}
            //fare.Append("</paxSegReference>");
            //fare.Append("<overrideInformation>");
            //fare.Append("<pricingOptionKey><pricingOptionKey>RP</pricingOptionKey></pricingOptionKey>");
            //fare.Append("<attributeDetails><attributeType>RU</attributeType></attributeDetails>");
            //fare.Append("<attributeDetails><attributeType>FBA</attributeType></attributeDetails>");
            //fare.Append("<attributeDetails><attributeType>PTC</attributeType></attributeDetails>");
            //fare.Append("</overrideInformation>");
            //fare.Append("<validatingCarrier><carrierInformation><carrierCode>" + VC + "</carrierCode></carrierInformation></validatingCarrier>");

            //fare.Append("<paxSegReference>");
            //for (int i = 0; i <= stRef.Count - 1; i++)
            //{
            //    fare.Append("<refDetails>");
            //    fare.Append("<refQualifier>S</refQualifier>");
            //    fare.Append("<refNumber>" + stRef[i].ToString() + "</refNumber>");
            //    fare.Append("</refDetails>");
            //    fare1.Append("<pricingFareBase>");
            //    fare1.Append("<fareBasisOptions>");
            //    fare1.Append("<fareBasisDetails>");
            //    if (FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString().Length <= 3)
            //    {
            //        fare1.Append("<primaryCode>" + FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString() + "</primaryCode>");
            //        fare1.Append("<fareBasisCode></fareBasisCode>");
            //    }
            //    else
            //    {
            //        fare1.Append("<primaryCode>" + STD.BAL.Utility.Left(FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString(), 3) + "</primaryCode>");
            //        fare1.Append("<fareBasisCode>" + FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString().Substring(3, (FltDsGAL.Tables[0].Rows[i]["AdtFarebasis"].ToString().Length - 3)) + "</fareBasisCode>");
            //    }
            //    fare1.Append("</fareBasisDetails>");
            //    fare1.Append("</fareBasisOptions>");
            //    fare1.Append("<fareBasisSegReference>");
            //    fare1.Append("<refDetails>");
            //    fare1.Append("<refQualifier>" + refQ[i].ToString() + "</refQualifier>");
            //    fare1.Append("<refNumber>" + stRef[i].ToString() + "</refNumber>");
            //    fare1.Append("</refDetails>");
            //    fare1.Append("</fareBasisSegReference>");
            //    fare1.Append("</pricingFareBase>");
            //}
            //fare.Append("</paxSegReference>");
            //fare.Append("<overrideInformation>");
            //fare.Append("<attributeDetails><attributeType>RP</attributeType></attributeDetails>");
            //fare.Append("<attributeDetails><attributeType>RU</attributeType></attributeDetails>");
            //fare.Append("<attributeDetails><attributeType>FBA</attributeType></attributeDetails>");
            //fare.Append("<attributeDetails><attributeType>PTC</attributeType></attributeDetails>");
            //fare.Append("</overrideInformation>");
            //fare.Append("<validatingCarrier><carrierInformation><carrierCode>" + VC + "</carrierCode></carrierInformation></validatingCarrier>");
            //fare.Append(fare1.ToString());

          //  fare.Append("</Fare_PricePNRWithBookingClass>");
            fare.Append("</soapenv:Body>");
            fare.Append("</soapenv:Envelope>");
            return fare.ToString();
        }
        
        #region PostXML
        public static string PostXml(String XMLIn, String Module_Version)
        {
            string rawResponse = string.Empty;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(XMLIn);
                // HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://production.webservices.amadeus.com");
               HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://nodeD1.test.webservices.amadeus.com/1ASIWPREP4A");
                request.Headers.Add(String.Format("SOAPAction: \"{0}\"", Module_Version));
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = @"text/xml;charset=utf-8";
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ProtocolVersion = HttpVersion.Version11;
                request.Timeout = 3000000;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    rawResponse = GetWebResponseString(response);
                    return rawResponse;
                }
            }
            catch (WebException wex)
            {
               // rawResponse = "Error: " + wex;
                //We issued an HttpWebRequest, so the response will be an HttpWebResponse.
                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;

                // Grab the response stream.           
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        // Grab the body of the response as a string for parsing.
                        rawResponse = GetWebResponseString(httpResponse);
                        //IBE.Shared.LogUtility.LogInfo(rawResponse, "SoapFaults", IBE.Shared.Utilities.GetCurrentTimestamp());[commented HS 11-10-2011]
                        //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, wex, "698", rawResponse, string.Empty, "");
                        throw wex;
                    }
                }
            }

            catch (Exception ex)
            {
                //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, ex, "699", rawResponse, string.Empty, "");
                rawResponse = "Error: " + ex;
            }
            return rawResponse;
        }
        #endregion PostXML

        #region GetWebResponseString
        private static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            Stream streamResponse;
            using (streamResponse = myHttpWebResponse.GetResponseStream())
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                using (StreamReader streamRead = new StreamReader(streamResponse))
                {
                    Char[] readBuffer = new Char[256];
                    int count = streamRead.Read(readBuffer, 0, 256);

                    while (count > 0)
                    {
                        String resultData = new String(readBuffer, 0, count);
                        rawResponse.Append(resultData);
                        count = streamRead.Read(readBuffer, 0, 256);
                    }
                }
            }
            return rawResponse.ToString();
        }
        #endregion GetWebResponseString
    }
    
}
