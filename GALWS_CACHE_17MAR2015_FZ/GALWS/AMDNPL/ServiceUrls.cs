﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GALWS.AMDNPL
{
   public class ServiceUrls
    {
        public static string ActionFareCheckRule { get { return "http://xml.amadeus.com/FARQNQ_07_1_1A"; } }
        public static string ActionFareInformativePricingWithoutPNR { get { return "http://xml.amadeus.com/TIPNRQ_15_1_1A"; } }
        //public static string ActionFareInformativePricingWithoutPNR { get { return "http://xml.amadeus.com/TIPNRQ_15_1_1A"; } }//TIPNRQ_18_1_1A TIPNRQ_15_1_1A
        public static string SvcURL { get { return "https://nodeD1.test.webservices.amadeus.com/1ASIWPREP4A"; } }
        //public static string SvcURL { get { return "https://nodeD2.production.webservices.amadeus.com/1ASIWPATXGB"; } }
        
        public static string ActionMasterPricer { get { return "http://webservices.amadeus.com/FMPTBQ_18_1_1A"; } }//http://webservices.amadeus.com/FMPTBQ_17_3_1A
        public static string ActionPNR_Retrieve { get { return "http://webservices.amadeus.com/PNRRET_16_1_1A"; } }
        public static string ActionTicket_ProcessEDoc { get { return "http://webservices.amadeus.com/TATREQ_15_2_1A"; } }
        public static string ActionTicket_Cancle { get { return "http://webservices.amadeus.com/PNRXCL_16_1_1A"; } }
        public static string ActionPnr_AddMultiElements { get { return "http://webservices.amadeus.com/PNRADD_16_1_1A"; } }
        public static string ActionSecurity_newSignOut { get { return "http://webservices.amadeus.com/VLSSOQ_04_1_1A"; } }
        public static string ActionTicket_IgnoreRefund { get { return "http://webservices.amadeus.com/Ticket_IgnoreRefund_3.0"; } }
        public static string ATCActionMasterPricer { get { return "http://webservices.amadeus.com/FMTCTQ_13_1_1A"; } }
        public static string RebookAirSegment { get { return "http://xml.amadeus.com/ARBKUQ_14_1_1A"; } }
        public static string ActionTicket_InitRefund { get { return "http://webservices.amadeus.com/Ticket_InitRefund_3.0"; } }
        public static string ActionTicket_UpdateRefund { get { return "http://webservices.amadeus.com/Ticket_UpdateRefund_3.0"; } }
        public static string ActionTicket_ProcessRefund { get { return "http://webservices.amadeus.com/Ticket_ProcessRefund_3.0"; } }
        public static string Ticket_RepricePNRWithBookingClass { get { return "http://xml.amadeus.com/TARIPQ_14_3_1A"; } }
        public static string Ticket_ReissueConfirmedPricing { get { return "http://xml.amadeus.com/TARCPQ_13_2_1A"; } }
        public static string Profile_ReadProfile { get { return "http://webservices.amadeus.com/Profile_ReadProfile_12.2"; } }
        public static string PNR_Cancel { get { return "http://xml.amadeus.com/PNRXCL_16_1_1A"; } }
        public static string PNR_AddMultiElements { get { return "http://xml.amadeus.com/PNRADD_16_1_1A"; } }
        public static string signOut { get { return "http://webservices.amadeus.com/VLSSOQ_04_1_1A"; } }
        public static string Air_SellFromRecommendation { get { return "http://webservices.amadeus.com/ITAREQ_05_2_IA"; } }//http://xml.amadeus.com/ITAREQ_05_2_IA
        public static string PlacePnrinQueue { get { return "http://webservices.amadeus.com/QUQPCQ_03_1_1A"; } }
        public static string Ticket_CheckEligibility { get { return "http://xml.amadeus.com/FATCEQ_13_1_1A"; } }
    }
}
