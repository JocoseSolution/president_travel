﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Xml;
using GALWS.AMDNPL;

namespace STD.BAL
{
    public class TSTReq
    {
        public string getTstRequest(string SessionId, string SoapAction, int Adt, int Chd, int Inf, string AdtFare, string ChdFare, string InfFare, string PriceRes, string addmultiRes, string airline, double SmsCharge, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl, ref string exep, string folder)
        {
            Hashtable tkt = null;
            ArrayList fare = null;
            string adttstqulf = "";
            string chdtstqulf = "";
            string inftstqulf = "";
            string TSTReq = "";
            tkt = tktdetails(addmultiRes);
            fare = TST_Read(PriceRes, "");
            int totnoofpax = Adt + Chd;

            ArrayList tktno = new ArrayList();
            tktno = (ArrayList)tkt["TktNoArrayList"];
            try
            {
                for (int i = 0; i <= tktno.Count - 1; i++)
                {
                    string[] tktsplit = Utility.Split(tktno[i].ToString(), "::");
                    for (int ii = 0; ii <= fare.Count - 1; ii++)
                    {
                        string[] splitfare = Utility.Split(fare[ii].ToString(), "#");
                        if (Adt > 0)
                        {
                            if (splitfare[2].Split('/')[1] == tktsplit[0] && tktsplit[1] == "ADT" && splitfare[2].Split('/')[0] == "PA")
                            {
                                double adtfarediff = Convert.ToDouble(splitfare[1]) - ((Convert.ToDouble(AdtFare) - SmsCharge));
                                //if (Convert.ToDouble(splitfare[1]) == ((Convert.ToDouble(AdtFare) - SmsCharge)))// - Convert.ToDouble(perpax_mrk)))
                                if (adtfarediff < 3)
                                {
                                    adttstqulf = Utility.Left(fare[ii].ToString(), 1);
                                }
                                else
                                {
                                    //adttstqulf = "INVALIDFARE"
                                }
                            }
                        }
                        if (Chd > 0)
                        {
                            if (splitfare[2].Split('/')[1] == tktsplit[0] && tktsplit[1] == "CHD" && splitfare[2].Split('/')[0] == "PA")
                            {
                                double chdfarediff = Convert.ToDouble(splitfare[1]) - ((Convert.ToDouble(ChdFare) - SmsCharge));
                                //if (Convert.ToDouble(splitfare[1]) == ((Convert.ToDouble(ChdFare) - SmsCharge)))// - Convert.ToDouble(perpax_mrk)))
                                if (chdfarediff < 3)
                                {
                                    chdtstqulf = Utility.Left(fare[ii].ToString(), 1);
                                }
                                else
                                {
                                    //chdtstqulf = "INVALIDFARE"
                                }
                            }
                        }
                        if (Inf > 0)
                        {
                            if (splitfare[2].Split('/')[1] == tktsplit[0] && tktsplit[1] == "INF" && splitfare[2].Split('/')[0] == "PI")
                            {
                                double inffarediff = Convert.ToDouble(splitfare[1]) - (Convert.ToDouble(InfFare));
                                //if (Convert.ToDouble(splitfare[1]) == (Convert.ToDouble(InfFare)))
                                if (inffarediff < 3)
                                {
                                    inftstqulf = Utility.Left(fare[ii].ToString(), 1);
                                }
                                else
                                {
                                    inftstqulf = "INVALIDFARE";
                                }
                            }
                        }
                    }
                }
                bool chkfare = false;
                if (Adt > 0)
                    if (string.IsNullOrEmpty(adttstqulf))
                        chkfare = false;
                    else
                        chkfare = true;
                if (Chd > 0)
                    if (string.IsNullOrEmpty(chdtstqulf))
                        chkfare = false;
                    else
                        chkfare = true;

                // If adttstqulf = "" Or chdtstqulf = "" Then
                if (chkfare == false)
                {
                    TSTReq = "";
                }
                else
                {
                    if (inftstqulf == "INVALIDFARE")
                    {
                        TSTReq = inftstqulf;
                    }
                    else
                    {
                        TSTReq = PNR_TST(SessionId, SoapAction, Adt, Chd, Inf, adttstqulf, chdtstqulf, inftstqulf, username, officeID, pass, includeSessionType, svcUrl, ref exep, folder);
                    }
                }

            }
            catch (Exception ex)
            {
            }
            return TSTReq;
        }
        private string PNR_TST(string sessionid, string SoapAction, int Adult, int Child, int Infant, string adttstqlf, string chdtstqlf, string inftstqlf, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl, ref string exep, string folder)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            AmdSession objssnHdr = new AmdSession();
            XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
            XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:fmp='http://xml.amadeus.com/TAUTCQ_04_1_1A'>");
            XmlBuilder.Append("<soapenv:Header>");
            XmlBuilder.Append(objssnHdr.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));//GetSessionHeader(sessionid)); //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
            XmlBuilder.Append("</soapenv:Header>");
            XmlBuilder.Append("<soapenv:Body>");
            if ((Adult > 0 && Child == 0 && Infant == 0))
            {
                //tkt = "<PoweredTicket_CreateTSTFromPricing><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + adttstqlf + "</uniqueReference></itemReference></psaList></PoweredTicket_CreateTSTFromPricing>";
                XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + adttstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
            }
            else if (((Adult) > 0 && (Child) > 0) && (Infant) == 0)
            {
                //tkt = "<PoweredTicket_CreateTSTFromPricing><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + adttstqlf + "</uniqueReference></itemReference></psaList><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + chdtstqlf + "</uniqueReference></itemReference></psaList></PoweredTicket_CreateTSTFromPricing>";
                XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + adttstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + chdtstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
            }
            else if (((Adult) > 0 && (Child) > 0) && (Infant) > 0)
            {
                //tkt = "<PoweredTicket_CreateTSTFromPricing><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + adttstqlf + "</uniqueReference></itemReference></psaList><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + chdtstqlf + "</uniqueReference></itemReference></psaList><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + inftstqlf + "</uniqueReference></itemReference></psaList></PoweredTicket_CreateTSTFromPricing>";
                XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + adttstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + chdtstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + inftstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
            }
            else if (((Adult) > 0 && (Child) == 0) && (Infant) > 0)
            {
                //tkt = "<PoweredTicket_CreateTSTFromPricing><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + adttstqlf + "</uniqueReference></itemReference></psaList><psaList><itemReference><referenceType>TST</referenceType><uniqueReference>" + inftstqlf + "</uniqueReference></itemReference></psaList></PoweredTicket_CreateTSTFromPricing>";
                XmlBuilder.Append("<Ticket_CreateTSTFromPricing>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + adttstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + inftstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
            }
            if ((Adult == 0 && Child > 0 && Infant == 0))
            {

                XmlBuilder.Append("<Ticket_CreateTSTFromPricing  xmlns='" + SoapAction + "'>");
                XmlBuilder.Append("<psaList>");
                XmlBuilder.Append("<itemReference>");
                XmlBuilder.Append("<referenceType>TST</referenceType>");
                XmlBuilder.Append("<uniqueReference>" + chdtstqlf + "</uniqueReference>");
                XmlBuilder.Append("</itemReference>");
                XmlBuilder.Append("</psaList>");
            }
            XmlBuilder.Append("</Ticket_CreateTSTFromPricing>");
            XmlBuilder.Append("</soapenv:Body>");
            XmlBuilder.Append("</soapenv:Envelope>");
            return XmlBuilder.ToString();
        }

        private Hashtable tktdetails(string xmlstr)
        {

            XmlDocument xmldoc = new XmlDocument();

            string xmm = xmlstr.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                     .Replace("xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\"", "").Replace("awsse:", "")
                     .Replace("xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"", "").Replace("wsa:", "")
                     .Replace("xmlns=\"http://xml.amadeus.com/PNRACC_17_1_1A\"", "");

            xmldoc.LoadXml(xmm);
            XmlNodeList xnode = default(XmlNodeList);
            string[] tktno1 = null;
            string tktno = string.Empty;
            string ptno = string.Empty;
            ArrayList tktnoArraylist = new ArrayList();
            ArrayList tktnoArraylist1 = new ArrayList();
            bool FAElement = false;
            //XmlNamespaceManager nsm = new XmlNamespaceManager(xmldoc.NameTable);
            //nsm.AddNamespace("PN", "http://xml.amadeus.com/PNRACC_14_2_1A");//"http://xml.amadeus.com/PNRACC_08_2_1A");
            xnode = xmldoc.SelectNodes("//PNR_Reply/dataElementsMaster/dataElementsIndiv");//, nsm);
            for (int i = 0; i <= xnode.Count - 1; i++)
            {
                for (int ii = 0; ii <= xnode.Item(i).ChildNodes.Count - 1; ii++)
                {
                    if (xnode.Item(i).ChildNodes[ii].Name == "elementManagementData")
                    {
                        for (int fv = 0; fv <= xnode.Item(i).ChildNodes[ii].ChildNodes.Count - 1; fv++)
                        {
                            if (xnode.Item(i).ChildNodes[ii].ChildNodes[fv].Name == "segmentName")
                            {
                                if (xnode.Item(i).ChildNodes[ii].ChildNodes[fv].InnerText == "FA")
                                {
                                    FAElement = true;
                                }
                            }
                        }
                    }
                    if (FAElement == true)
                    {
                        if (xnode.Item(i).ChildNodes[ii].Name == "otherDataFreetext")
                        {
                            for (int tkt = 0; tkt <= xnode.Item(i).ChildNodes[ii].ChildNodes.Count - 1; tkt++)
                            {
                                if (xnode.Item(i).ChildNodes[ii].ChildNodes[tkt].Name == "longFreetext")
                                {
                                    tktno1 = xnode.Item(i).ChildNodes[ii].ChildNodes[tkt].InnerText.ToString().Split('/');
                                    tktno = tktno1[0].ToString();
                                }
                            }
                        }
                        else if (xnode.Item(i).ChildNodes[ii].Name == "referenceForDataElement")
                        {
                            for (int pt = 0; pt <= xnode.Item(i).ChildNodes[ii].ChildNodes.Count - 1; pt++)
                            {
                                if (xnode.Item(i).ChildNodes[ii].ChildNodes[pt].Name == "reference")
                                {
                                    for (int qulf = 0; qulf <= xnode.Item(i).ChildNodes[ii].ChildNodes[pt].ChildNodes.Count - 1; qulf++)
                                    {
                                        if (xnode.Item(i).ChildNodes[ii].ChildNodes[pt].ChildNodes[qulf].InnerText == "PT")
                                        {
                                            ptno = xnode.Item(i).ChildNodes[ii].ChildNodes[pt].ChildNodes[qulf].NextSibling.InnerText;
                                            FAElement = false;
                                            tktnoArraylist.Add(ptno + "::" + tktno);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //***************** Ticket No. with Passenger Details Start******************
            ptno = string.Empty;
            string srnm = string.Empty;
            string fstnm = string.Empty;

            XmlNodeList travinfo = xmldoc.SelectNodes("//travellerInfo");
            for (int i = 0; i <= travinfo.Count - 1; i++)
            {
                for (int ii = 0; ii <= travinfo.Item(i).ChildNodes.Count - 1; ii++)
                {
                    if (travinfo.Item(i).ChildNodes[ii].Name == "elementManagementPassenger")
                    {
                        for (int pt = 0; pt <= travinfo.Item(i).ChildNodes[ii].ChildNodes.Count - 1; pt++)
                        {
                            if (travinfo.Item(i).ChildNodes[ii].ChildNodes[pt].Name == "reference")
                            {
                                for (int qulf = 0; qulf <= travinfo.Item(i).ChildNodes[ii].ChildNodes[pt].ChildNodes.Count - 1; qulf++)
                                {
                                    if (travinfo.Item(i).ChildNodes[ii].ChildNodes[pt].ChildNodes[qulf].InnerText == "PT")
                                    {
                                        ptno = travinfo.Item(i).ChildNodes[ii].ChildNodes[pt].ChildNodes[qulf].NextSibling.InnerText;
                                        if (tktnoArraylist.Count > 0)
                                        {
                                            for (int t = 0; t <= tktnoArraylist.Count - 1; t++)
                                            {
                                                string typ = string.Empty;
                                                string infindc = string.Empty;
                                                string[] pttkt = Utility.Split(tktnoArraylist[t].ToString(), "::");
                                                if (pttkt[0].ToString().Trim() == ptno.ToString().Trim())
                                                {
                                                    XmlNode travnm = travinfo.Item(i).ChildNodes[ii].NextSibling;
                                                    for (int trvnm = 0; trvnm <= travnm.ChildNodes.Count - 1; trvnm++)
                                                    {
                                                        if (travnm.ChildNodes[trvnm].Name == "travellerInformation")
                                                        {
                                                            for (int trvnm1 = 0; trvnm1 <= travnm.ChildNodes[trvnm].ChildNodes.Count - 1; trvnm1++)
                                                            {
                                                                if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].Name == "traveller")
                                                                {
                                                                    for (int trvnm2 = 0; trvnm2 <= travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes.Count - 1; trvnm2++)
                                                                    {
                                                                        if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "surname")
                                                                        {
                                                                            srnm = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                        }
                                                                    }
                                                                }
                                                                else if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].Name == "passenger")
                                                                {
                                                                    for (int trvnm2 = 0; trvnm2 <= travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes.Count - 1; trvnm2++)
                                                                    {
                                                                        if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "firstName")
                                                                        {
                                                                            fstnm = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                        }
                                                                        else if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "type")
                                                                        {
                                                                            typ = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                        }
                                                                        else if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "infantIndicator")
                                                                        {
                                                                            infindc = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                        }
                                                                    }
                                                                    string[] tktarray = Utility.Split(pttkt[1], " ");
                                                                    if (typ == tktarray[0])
                                                                    {
                                                                        tktnoArraylist1.Add(pttkt[0] + "::" + typ + "::" + srnm + "::" + fstnm + "::" + tktarray[1]);
                                                                    }
                                                                    else if ((string.IsNullOrEmpty(typ) | typ == string.Empty) && !string.IsNullOrEmpty(infindc) && tktarray[0] != "INF")
                                                                    {
                                                                        tktnoArraylist1.Add(pttkt[0] + "::" + "ADT" + "::" + srnm + "::" + fstnm + "::" + tktarray[1]);
                                                                    }
                                                                    else if ((string.IsNullOrEmpty(typ) | typ == string.Empty) && string.IsNullOrEmpty(infindc) && tktarray[0] != "INF")
                                                                    {
                                                                        tktnoArraylist1.Add(pttkt[0] + "::" + "NA" + "::" + srnm + "::" + fstnm + "::" + tktarray[1]);
                                                                    }
                                                                    else if ((typ == "ADT" | typ == "CHD") && tktarray[0] == "PAX")
                                                                    {
                                                                        tktnoArraylist1.Add(pttkt[0] + "::" + typ + "::" + srnm + "::" + fstnm + "::" + tktarray[1]);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            string typ = string.Empty;
                                            string infindc = string.Empty;
                                            XmlNode travnm = travinfo.Item(i).ChildNodes[ii].NextSibling;
                                            for (int trvnm = 0; trvnm <= travnm.ChildNodes.Count - 1; trvnm++)
                                            {
                                                if (travnm.ChildNodes[trvnm].Name == "travellerInformation")
                                                {
                                                    for (int trvnm1 = 0; trvnm1 <= travnm.ChildNodes[trvnm].ChildNodes.Count - 1; trvnm1++)
                                                    {
                                                        if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].Name == "traveller")
                                                        {
                                                            for (int trvnm2 = 0; trvnm2 <= travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes.Count - 1; trvnm2++)
                                                            {
                                                                if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "surname")
                                                                {
                                                                    srnm = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                }
                                                            }
                                                        }
                                                        else if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].Name == "passenger")
                                                        {
                                                            for (int trvnm2 = 0; trvnm2 <= travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes.Count - 1; trvnm2++)
                                                            {
                                                                if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "firstName")
                                                                {
                                                                    fstnm = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                }
                                                                else if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "type")
                                                                {
                                                                    typ = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                }
                                                                else if (travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].Name == "infantIndicator")
                                                                {
                                                                    infindc = travnm.ChildNodes[trvnm].ChildNodes[trvnm1].ChildNodes[trvnm2].InnerText;
                                                                }
                                                            }
                                                            tktnoArraylist1.Add(ptno + "::" + typ + "::" + srnm + "::" + fstnm + "::" + "NOT TICKETED");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //***************** Ticket No. with Passenger Details End******************

            //***************** OriginDestinationDetails Details Start******************
            Hashtable originDestination = new Hashtable();
            XmlNodeList OrgDest = xmldoc.SelectNodes("//originDestinationDetails/itineraryInfo");
            for (int i = 0; i <= OrgDest.Count - 1; i++)
            {
                try
                {
                    Hashtable itineraryInfo = new Hashtable();
                    for (int ii = 0; ii <= OrgDest.Item(i).ChildNodes.Count - 1; ii++)
                    {
                        if (OrgDest.Item(i).ChildNodes[ii].Name == "travelProduct")
                        {
                            for (int tp = 0; tp <= OrgDest.Item(i).ChildNodes[ii].ChildNodes.Count - 1; tp++)
                            {
                                if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].Name == "product")
                                {
                                    for (int tp1 = 0; tp1 <= OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes.Count - 1; tp1++)
                                    {
                                        if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].Name == "depDate")
                                        {
                                            itineraryInfo.Add("depDate", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].InnerText);
                                        }
                                        else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].Name == "depTime")
                                        {
                                            itineraryInfo.Add("depTime", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].InnerText);
                                        }
                                        else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].Name == "arrDate")
                                        {
                                            itineraryInfo.Add("arrDate", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].InnerText);
                                        }
                                        else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].Name == "arrTime")
                                        {
                                            itineraryInfo.Add("arrTime", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].ChildNodes[tp1].InnerText);
                                        }
                                    }
                                }
                                else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].Name == "boardpointDetail")
                                {
                                    itineraryInfo.Add("boardpointDetail", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].InnerText);
                                }
                                else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].Name == "offpointDetail")
                                {
                                    itineraryInfo.Add("offpointDetail", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].InnerText);
                                }
                                else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].Name == "companyDetail")
                                {
                                    itineraryInfo.Add("companyDetail", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].InnerText);
                                }
                                else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].Name == "productDetails")
                                {
                                    itineraryInfo.Add("productDetails", OrgDest.Item(i).ChildNodes[ii].ChildNodes[tp].InnerText);
                                }
                            }
                        }
                        else if (OrgDest.Item(i).ChildNodes[ii].Name == "itineraryReservationInfo")
                        {
                            for (int ir = 0; ir <= OrgDest.Item(i).ChildNodes[ii].ChildNodes.Count - 1; ir++)
                            {
                                if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[ir].Name == "reservation")
                                {
                                    for (int ir1 = 0; ir1 <= OrgDest.Item(i).ChildNodes[ii].ChildNodes[ir].ChildNodes.Count - 1; ir1++)
                                    {
                                        if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[ir].ChildNodes[ir1].Name == "companyId")
                                        {
                                            itineraryInfo.Add("companyId", OrgDest.Item(i).ChildNodes[ii].ChildNodes[ir].ChildNodes[ir1].InnerText);
                                        }
                                        else if (OrgDest.Item(i).ChildNodes[ii].ChildNodes[ir].ChildNodes[ir1].Name == "controlNumber")
                                        {
                                            itineraryInfo.Add("controlNumber", OrgDest.Item(i).ChildNodes[ii].ChildNodes[ir].ChildNodes[ir1].InnerText);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    originDestination.Add(i, itineraryInfo);
                }
                catch (Exception ex)
                {
                }
            }
            //***************** OriginDestinationDetails Details End******************

            //***********All Details Start***********
            Hashtable Tkt_OrgDest_Details = new Hashtable();
            Tkt_OrgDest_Details.Add("originDestination", originDestination);
            Tkt_OrgDest_Details.Add("TktNoArrayList", tktnoArraylist1);
            //***********All Details End***********

            return Tkt_OrgDest_Details;
        }
        public ArrayList TST_Read(string Fare_Info, string ChdPt)
        {
            XmlDocument Fare_Reader = new XmlDocument();
            string FareList = null;
            string TaxList = null;
            string cpt = "";
            ArrayList FareArray = new ArrayList();
            ArrayList FareArray1 = new ArrayList();
            string paxref = "";
            string Pax = "";

            if (Fare_Info.Contains("<errorFreeText1>") == true)
            {
            }
            else
            {
                Fare_Reader.LoadXml(Fare_Info);
                XmlNodeList Fare_Node = Fare_Reader.GetElementsByTagName("fareList");
                for (int i = 0; i <= Fare_Node.Count - 1; i++)
                {
                    string uqlfr = "";
                    for (int ii = 0; ii <= Fare_Node[i].ChildNodes.Count - 1; ii++)
                    {
                        if (Fare_Node[i].ChildNodes[ii].Name == "fareReference")
                        {
                            for (int iii = 0; iii <= Fare_Node[i].ChildNodes[ii].ChildNodes.Count - 1; iii++)
                            {
                                if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].Name == "uniqueReference")
                                {
                                    uqlfr = Fare_Node[i].ChildNodes[ii].ChildNodes[iii].InnerText;
                                }
                            }
                        }
                        else if (Fare_Node[i].ChildNodes[ii].Name == "fareDataInformation")
                        {
                            for (int iii = 0; iii <= Fare_Node[i].ChildNodes[ii].ChildNodes.Count - 1; iii++)
                            {
                                if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].Name == "fareDataSupInformation")
                                {
                                    for (int FR = 0; FR <= Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes.Count - 1; FR++)
                                    {
                                        if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].Name == "fareDataQualifier")
                                        {
                                            if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].InnerText == "B")
                                            {
                                                // FareList = FareList && "BasicFare;"
                                            }
                                            else if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].InnerText == "712")
                                            {
                                                FareList = FareList + "TotalFare;";
                                            }
                                            else if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].InnerText == "TFT")
                                            {
                                                //FareList = FareList && "GrandTotalFare;"
                                            }
                                        }
                                        else if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].Name == "fareAmount")
                                        {
                                            if (FareList == "TotalFare;")
                                            {
                                                FareList = Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].InnerText;
                                                //& ";"

                                            }
                                        }
                                        else if (Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].Name == "fareCurrency")
                                        {
                                            //  FareList = FareList & Fare_Node[i].ChildNodes[ii].ChildNodes[iii].ChildNodes[FR].InnerText & "::"
                                        }
                                    }
                                }
                            }
                        }
                        else if (Fare_Node[i].ChildNodes[ii].Name == "paxSegReference")
                        {
                            paxref = "";
                            for (int pt = 0; pt <= Fare_Node[i].ChildNodes[ii].ChildNodes.Count - 1; pt++)
                            {
                                if (Fare_Node[i].ChildNodes[ii].ChildNodes[pt].Name == "refDetails")
                                {
                                    for (int rfc = 0; rfc <= Fare_Node[i].ChildNodes[ii].ChildNodes[pt].ChildNodes.Count - 1; rfc++)
                                    {
                                        if (Fare_Node[i].ChildNodes[ii].ChildNodes[pt].ChildNodes[rfc].Name == "refNumber")
                                        {
                                            cpt = Fare_Node[i].ChildNodes[ii].ChildNodes[pt].ChildNodes[rfc].InnerText;
                                            if (pt == Fare_Node[i].ChildNodes[ii].ChildNodes.Count - 1)
                                            {
                                                paxref = cpt;
                                                //paxref & "PaxRefNo" & cpt
                                            }
                                            else
                                            {
                                                paxref = cpt;
                                                //paxref & "PaxRefNo" & cpt & "/"
                                            }
                                        }
                                        else if (Fare_Node[i].ChildNodes[ii].ChildNodes[pt].ChildNodes[rfc].Name == "refQualifier")
                                        {
                                            Pax = Fare_Node[i].ChildNodes[ii].ChildNodes[pt].ChildNodes[rfc].InnerText;
                                        }
                                    }
                                    FareArray1.Add(Pax + "/" + paxref);
                                }
                            }
                        }
                    }
                    for (int ii = 0; ii <= FareArray1.Count - 1; ii++)
                    {
                        FareArray.Add(uqlfr + "Q" + "#" + FareList + "#" + FareArray1[ii] + "#" + i.ToString());
                    }
                    FareArray1.Clear();
                    FareList = "";
                }
            }
            return FareArray;
        }
        private string GetSessionHeader(string SH)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            string[] ss = Utility.Split(SH, "|");
            XmlBuilder.Append("<wbs:Session>");
            XmlBuilder.Append("<wbs:SessionId>" + ss[0].ToString() + "</wbs:SessionId>");
            XmlBuilder.Append("<wbs:SequenceNumber>" + ss[1].ToString() + "</wbs:SequenceNumber>");
            XmlBuilder.Append("<wbs:SecurityToken>" + ss[2].ToString() + "</wbs:SecurityToken>");
            XmlBuilder.Append("</wbs:Session>");
            return XmlBuilder.ToString();
        }

    }
}
