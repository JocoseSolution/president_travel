﻿using STD.BAL;
using STD.Shared;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace GALWS.AMDNPL
{
    public class AMDFareRule
    {

        public FlightSearch searchInputs { get; set; }
        public List<CredentialList> ProviderList;
        string pcc = "", url = "", Userid = "", Pwd = "", strCorporateID = "";
        //  string strVc = "", strTrip = "", strProvider = "";

        string constr = Convert.ToString(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        //public AMDFareRule(FlightSearch searchParam, List<CredentialList> CrsList)
        //{
        //    searchInputs = searchParam;
        //    ProviderList = CrsList;
        //}
        private void GetCredentials(string conn, string ProviderUserId)
        {
            //string UserId
            ProviderList = Data.GetProviderCrd(conn);
            //   "WSP4APRE", "KTMVS3270", "AMADEUS100"


            if (!string.IsNullOrEmpty(ProviderUserId))
            {
                url = ((from crd in ProviderList where crd.Provider == "1A" && crd.UserID == ProviderUserId select crd).ToList())[0].AvailabilityURL;
                Userid = ((from crd in ProviderList where crd.Provider == "1A" && crd.UserID == ProviderUserId select crd).ToList())[0].UserID;
                Pwd = ((from crd in ProviderList where crd.Provider == "1A" && crd.UserID == ProviderUserId select crd).ToList())[0].Password;
                pcc = ((from crd in ProviderList where crd.Provider == "1A" && crd.UserID == ProviderUserId select crd).ToList())[0].CarrierAcc;
                strCorporateID = ((from crd in ProviderList where crd.Provider == "1A" && crd.UserID == ProviderUserId select crd).ToList())[0].CorporateID;
            }
            else
            {

                url = ((from crd in ProviderList where crd.Provider == "1A" select crd).ToList())[0].AvailabilityURL;
                Userid = ((from crd in ProviderList where crd.Provider == "1A" select crd).ToList())[0].UserID;
                Pwd = ((from crd in ProviderList where crd.Provider == "1A" select crd).ToList())[0].Password;
                pcc = ((from crd in ProviderList where crd.Provider == "1A" select crd).ToList())[0].CarrierAcc;
                strCorporateID = ((from crd in ProviderList where crd.Provider == "1A" select crd).ToList())[0].CorporateID;
            }

        }


        public List<FareRulesFlight> GetFareRule1A(ArrayList JsonArr, string StrConn, UAPIAirFR objFR)
        {
            PNRCreation objPNR = new PNRCreation(constr, "D", "");
            List<FareRulesFlight> FareRuleList = new List<FareRulesFlight>();
            GetCredentials(StrConn, "");
            string folder = "MPTB";
            string farerulemain = "";
            string farerule = "";
            string tab = "";
            AmdSession objSsnHdr = new AmdSession();
            string exep = "";
            string SrcOffice = strCorporateID;
            string OrgId = Userid;
            string ClearPass = Pwd;
            string SessionId = "";
            try
            {
                //SessionId, ListSAU[0].SELL, Adult, Child, Infant, FltDsGAL, FltHdr, PaxDS, "WSP4APRE", "KTMVS3270", "AMADEUS100", ServiceUrls.SvcURL, IncludeSessionType.Start);
                string respInformPrice = GetInformPrice("", OrgId, SrcOffice, ClearPass, JsonArr, StrConn, folder);
                SessionId = objSsnHdr.GetSessionInseries(respInformPrice);

                string xmm = respInformPrice.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                         .Replace("xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\"", "").Replace("awsse:", "")
                         .Replace("xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"", "").Replace("wsa:", "")
                         .Replace("xmlns=\"http://xml.amadeus.com/TIPNRQ_18_1_1A\"", "");//TIPNRR_14_1_1A


                string fareruleXml = GetFareRule(SessionId, OrgId, SrcOffice, ClearPass, "712", false, "", folder);


                //string messageFunction = xmlDoc.Descendants("Fare_InformativePricingWithoutPNRReply").Descendants("messageFunctionDetails").Elements("messageFunction").FirstOrDefault().Value;
                //  string fareruleXml = GetMiniFareRule(SessionId, OrgId, SrcOffice, ClearPass, "712", false, "", folder);

                //FareRuleList = ParseMiniFareRule(fareruleXml, ref tab, objFR);
                //string fareruleXml = GetFareRule(SessionId, OrgId, SrcOffice, ClearPass, "712", false, "", folder);

                ////  if (FareRuleList != null && FareRuleList.Count < 1)
                ///
                if (fareruleXml.Contains("Error") == false)
                {

                    // SessionId = objSsnHdr.GetSessionInseries(fareruleXml);
                    // fareruleXml = GetFareRule(SessionId, OrgId, SrcOffice, ClearPass, "712", false, "", folder);

                    string xmm1 = fareruleXml.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                            .Replace("xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\"", "").Replace("awsse:", "")
                            .Replace("xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"", "").Replace("wsa:", "")
                            .Replace("xmlns=\"http://xml.amadeus.com/FARQNR_07_1_1A\"", "");//FARQNR_07_1_1A

                    XDocument xmlDoc = XDocument.Parse(xmm1);
                    string tabtxt = "";
                    if (!xmlDoc.Descendants("Fare_CheckRulesReply").Descendants("fareRuleText").Any() && xmlDoc.Descendants("Fare_CheckRulesReply").Descendants("travellerGrp").Descendants("travellerIdentRef").Descendants("referenceDetails").Descendants("type").Where(x => x.Value == "FC").Any())
                    {

                        StringBuilder Req = new StringBuilder();

                        XDocument xmlDoc1 = XDocument.Parse(xmm1);

                        IEnumerable<XElement> onjFlightDetails = xmlDoc.Descendants("Fare_CheckRulesReply").Descendants("flightDetails").Descendants("travellerGrp").Descendants("travellerIdentRef").Descendants("referenceDetails").Where(x => x.Element("type").Value == "FC");

                        foreach (XElement aa in onjFlightDetails)
                        {
                            tabtxt = "";
                            Req.Append(" <itemNumber>");
                            Req.Append("<itemNumberDetails>");
                            Req.Append("<number>1</number>");
                            Req.Append("</itemNumberDetails>");
                            Req.Append("<itemNumberDetails>");
                            Req.Append("<number>" + aa.Element("value").Value + "</number>");
                            Req.Append("<type>" + aa.Element("type").Value + "</type>");
                            Req.Append("</itemNumberDetails>");
                            Req.Append("</itemNumber>");
                            fareruleXml = GetFareRule(SessionId, OrgId, SrcOffice, ClearPass, "712", true, Req.ToString(), folder);

                            farerule += ParseFareRule(fareruleXml, ref tabtxt);
                            tab += tabtxt;

                            SessionId = objSsnHdr.GetSessionInseries(fareruleXml);
                            Req = new StringBuilder();

                        }



                    }
                    else
                    {

                        farerule += ParseFareRule(fareruleXml, ref tabtxt);
                        tab += tabtxt;
                    }

                    //farerulemain = "<ul class='tab'>" + tab + "</ul>"; ;
                    farerulemain += farerule;

                    FareRulesFlight objfr = new FareRulesFlight();
                    objfr.Remarks = farerulemain;
                    FareRuleList.Add(objfr);
                }
                else
                {
                    SessionId = objSsnHdr.GetSessionInseries(fareruleXml);
                }


                objPNR.SignOut("", SessionId);
            }
            catch (Exception ex)
            {
                objPNR.SignOut("", SessionId);
            }

            return FareRuleList;
        }

        public string ParseFareRule(string xml, ref string tab)
        {
            string bb = xml.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                  .Replace("xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\"", "").Replace("awsse:", "")
                  .Replace("xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"", "").Replace("wsa:", "")
                  .Replace("xmlns=\"http://xml.amadeus.com/FARQNR_07_1_1A\"", "");

            XDocument xmlDoc = XDocument.Parse(bb);
            StringBuilder Req = new StringBuilder();

            string sector = "";
            string html = "";
            string SectorTab = "";
            if (xmlDoc.Descendants("Fare_CheckRulesReply").Descendants("flightDetails").Descendants("odiGrp").Descendants("originDestination").Any())
            {
                IEnumerable<XElement> onjFlightDetails = xmlDoc.Descendants("Fare_CheckRulesReply").Descendants("flightDetails").Descendants("odiGrp").Descendants("originDestination");
                sector = onjFlightDetails.Elements("origin").FirstOrDefault().Value + "-" + onjFlightDetails.Elements("destination").FirstOrDefault().Value;
                //SectorTab = onjFlightDetails.Elements("origin").FirstOrDefault().Value + "<img src='" + System.Web.HttpContext.Current.Request.ApplicationPath + "/Images/air.png' />" + onjFlightDetails.Elements("destination").FirstOrDefault().Value;
            }
            string txt = "";
            IEnumerable<XElement> onjFlightDetails1 = xmlDoc.Descendants("Fare_CheckRulesReply").Descendants("tariffInfo");

            if (onjFlightDetails1 != null && onjFlightDetails1.Count() > 0)
            {
                foreach (XElement obj in onjFlightDetails1)
                {
                    IEnumerable<XElement> freetext = obj.Descendants("fareRuleText").Descendants("freeText");

                    foreach (XElement objtext in freetext)
                    {

                        txt = txt + objtext.Value;

                    }

                }
            }
            else
            {
                txt = "Fare rule not available.";

            }


            //  tab += "<li><a class='tablinks' title='Click for fare rules' onclick=\"SelectedSector(event,'" + sector + "')\">" + SectorTab + " </a></li>";

            // html += "<div id='" + sector + "' class='tabcontent'><h3>" + sector + "</h3><p>" + txt + "</p></div>";
            html += txt;


            return html;
        }

        public List<FareRulesFlight> ParseMiniFareRule(string xml, ref string tab, UAPIAirFR objFR)
        {
            string bb = xml.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                  .Replace("xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\"", "").Replace("awsse:", "")
                  .Replace("xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"", "").Replace("wsa:", "")
                  .Replace("xmlns=\"http://xml.amadeus.com/FMPTBQ_17_3_1A\"", "");// TMRCRR_11_1_1A

            XDocument xmlDoc = XDocument.Parse(bb);
            StringBuilder Req = new StringBuilder();
            List<FareRulesFlight> FareRuleList = new List<FareRulesFlight>();

            string sector = "";
            string html = "";
            string SectorTab = "";

            IEnumerable<XElement> fareComponent = xmlDoc.Descendants("MiniRule_GetFromPricingReply").Descendants("mnrByFareRecommendation").Descendants("fareComponentInfo");
            try
            {

                if (fareComponent != null && fareComponent.Count() > 0)
                {
                    foreach (XElement obj in fareComponent)
                    {

                        string fareBasis = obj.Descendants("fareQualifierDetails").Descendants("additionalFareDetails").Elements("rateClass").FirstOrDefault().Value.ToString();
                        sector = obj.Descendants("originAndDestination").Elements("origin").FirstOrDefault().Value + "-" + obj.Descendants("originAndDestination").Elements("destination").FirstOrDefault().Value;
                        IEnumerable<XElement> referenceDetails = obj.Descendants("fareComponentRef").Descendants("referenceDetails");
                        string FC_Val = "";

                        foreach (XElement referenceDetailsObj in referenceDetails)
                        {
                            if (referenceDetailsObj.Descendants("type").FirstOrDefault().Value == "FC")
                            {
                                FC_Val = referenceDetailsObj.Elements("value").FirstOrDefault().Value.Trim();
                                break;
                            }
                        }

                        IEnumerable<XElement> mnrRulesInfoGrp = xmlDoc.Descendants("MiniRule_GetFromPricingReply").Descendants("mnrByFareRecommendation").Descendants("mnrRulesInfoGrp");

                        foreach (XElement mnrgrp in mnrRulesInfoGrp)
                        {
                            string rulrno = mnrgrp.Descendants("mnrCatInfo").Descendants("descriptionInfo").Elements("number").FirstOrDefault().Value.Trim();
                            if (Convert.ToInt32(rulrno) == 31 || Convert.ToInt32(rulrno) == 33)
                            {
                                string proceessIndicator = mnrgrp.Descendants("mnrCatInfo").Elements("processIndicator").Any() ? mnrgrp.Descendants("mnrCatInfo").Elements("processIndicator").FirstOrDefault().Value.Trim() : "";

                                bool chkFC = false;
                                bool BfrDep = false;
                                bool Noshow = false;
                                bool AftrDep = false;
                                bool NoshowAftrDep = false;

                                decimal BfrDepAmount = 0;
                                decimal NoshowAmount = 0;
                                decimal AftrDepAmount = 0;
                                decimal NoshowAmountAftrDep = 0;


                                IEnumerable<XElement> statusInformation = mnrgrp.Descendants("mnrRestriAppInfoGrp").Descendants("mnrRestriAppInfo").Descendants("statusInformation");

                                foreach (XElement StInfo in statusInformation)
                                {
                                    if (StInfo.Element("indicator").Value.Trim().ToUpper() == "BDA")
                                    {
                                        BfrDep = Convert.ToInt32(StInfo.Element("action").Value.Trim().ToUpper()) == 1 ? true : false;
                                    }

                                    if (StInfo.Element("indicator").Value.Trim().ToUpper() == "BNA")
                                    {
                                        Noshow = Convert.ToInt32(StInfo.Element("action").Value.Trim().ToUpper()) == 1 ? true : false;
                                    }


                                    if (StInfo.Element("indicator").Value.Trim().ToUpper() == "ANA")
                                    {
                                        NoshowAftrDep = Convert.ToInt32(StInfo.Element("action").Value.Trim().ToUpper()) == 1 ? true : false;
                                    }

                                    if (StInfo.Element("indicator").Value.Trim().ToUpper() == "ADA")
                                    {
                                        AftrDep = Convert.ToInt32(StInfo.Element("action").Value.Trim().ToUpper()) == 1 ? true : false;
                                    }

                                }



                                foreach (XElement refdtl in mnrgrp.Descendants("mnrFCInfoGrp").Descendants("refInfo").Descendants("referenceDetails"))
                                {

                                    if (refdtl.Element("type").Value.Trim() == "FC")
                                    {
                                        int val = Convert.ToInt32(refdtl.Element("value").Value.Trim());
                                        if (Convert.ToInt32(FC_Val) == val)
                                        {
                                            chkFC = true;

                                        }
                                    }

                                    //// Refund
                                    if (Convert.ToInt32(rulrno) == 33 && chkFC == true)
                                    {
                                        // processIndicator: if codeset 'ASS' is returned then the category assumption applies. Assumption for category 33 is "Refundable without penalties".
                                        if (proceessIndicator.ToUpper() == "ASS")
                                        {
                                            // Refundable without panalties
                                            FareRulesFlight objfrFlt = new FareRulesFlight();
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "BEFORE DEPARTURE";
                                            objfrFlt.Rule = "CANCELLATION";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = "0";
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = true;
                                            objfrFlt.Remarks = "Refundable without panalties";
                                            objfrFlt.Cabin = objFR.IdType;
                                            objfrFlt.Segment = sector;
                                            FareRuleList.Add(objfrFlt);

                                            objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "AFTER DEPARTURE";
                                            objfrFlt.Rule = "CANCELLATION";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = "0";
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = true;
                                            objfrFlt.Remarks = "Refundable without panalties";
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);
                                            goto End;
                                        }
                                        else
                                        {
                                            foreach (XElement monetaryDetails in mnrgrp.Descendants("mnrMonInfoGrp").Descendants("monetaryInfo").Descendants("monetaryDetails"))
                                            {
                                                if (BfrDep == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "BDT")
                                                {
                                                    BfrDepAmount = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                                if (Noshow == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "BNT")
                                                {
                                                    NoshowAmount = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                                if (NoshowAftrDep == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "ANT")
                                                {
                                                    NoshowAmountAftrDep = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                                if (AftrDep == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "ADT")
                                                {
                                                    AftrDepAmount = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                            }

                                            FareRulesFlight objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "BEFORE DEPARTURE";
                                            objfrFlt.Rule = "CANCELLATION";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = BfrDepAmount > 0 ? BfrDepAmount.ToString() : BfrDep == false ? "NP" : "0";// BfrDepAmount.ToString();
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = BfrDep == false ? false : true;
                                            objfrFlt.Remarks = BfrDep == false ? "Not Allowed." : "";
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "AFTER DEPARTURE";
                                            objfrFlt.Rule = "CANCELLATION";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = AftrDepAmount > 0 ? AftrDepAmount.ToString() : AftrDep == false ? "NP" : "0";
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = AftrDep == false ? false : true;
                                            objfrFlt.Remarks = AftrDep == false ? "Not Allowed." : ""; ;
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "NO-SHOW";
                                            objfrFlt.Rule = "CANCELLATION";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = "Before Dep.:- " + (Noshow == false ? "Not Permitted" : (NoshowAmount >= 0 ? (NoshowAmount == 0 ? "Cancellation Permitted" : "INR " + NoshowAmount.ToString()) : ""));
                                            objfrFlt.FeeAmount += " /After Dep.: " + (NoshowAftrDep == false ? "Not Permitted" : (NoshowAmountAftrDep >= 0 ? (NoshowAmountAftrDep == 0 ? "Cancellation Permitted" : "INR " + NoshowAmountAftrDep.ToString()) : ""));// NoshowAmount.ToString();
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = Noshow == false ? false : true;
                                            objfrFlt.Remarks = Noshow == false ? "Not Allowed." : ""; ;
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            goto End;


                                        }


                                    }


                                    //// Reissue
                                    if (Convert.ToInt32(rulrno) == 31 && chkFC == true)
                                    {
                                        if (proceessIndicator.ToUpper() == "ASS")
                                        {
                                            // processIndicator: if codeset 'ASS' is returned then the category assumption applies = Not allowed


                                            FareRulesFlight objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "BEFORE DEPARTURE";
                                            objfrFlt.Rule = "CHANGE";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = BfrDepAmount.ToString();
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = BfrDep == false ? false : true;
                                            objfrFlt.Remarks = "Not Allowed.";
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "AFTER DEPARTURE";
                                            objfrFlt.Rule = "CHANGE";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = "0";
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = AftrDep == false ? false : true;
                                            objfrFlt.Remarks = "Not Allowed.";
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "NO-SHOW";
                                            objfrFlt.Rule = "CHANGE";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = NoshowAmount.ToString();
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = Noshow == false ? false : true;
                                            objfrFlt.Remarks = "Not Allowed.";
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            goto End;
                                        }
                                        else
                                        {
                                            foreach (XElement monetaryDetails in mnrgrp.Descendants("mnrMonInfoGrp").Descendants("monetaryInfo").Descendants("monetaryDetails"))
                                            {

                                                if (BfrDep == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "BDT")
                                                {
                                                    BfrDepAmount = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                                if (Noshow == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "BNT")
                                                {
                                                    NoshowAmount = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                                if (NoshowAftrDep == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "ANT")
                                                {
                                                    NoshowAmountAftrDep = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                                if (AftrDep == true && monetaryDetails.Element("typeQualifier").Value.Trim().ToUpper() == "ADT")
                                                {
                                                    AftrDepAmount = Convert.ToDecimal(monetaryDetails.Element("amount").Value.Trim().ToUpper());
                                                }

                                            }

                                            FareRulesFlight objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "BEFORE DEPARTURE";
                                            objfrFlt.Rule = "CHANGE";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = BfrDepAmount > 0 ? BfrDepAmount.ToString() : BfrDep == false ? "NP" : "0";// BfrDepAmount.ToString();
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = BfrDep == false ? false : true;
                                            objfrFlt.Remarks = BfrDep == false ? "Not Allowed." : "";
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "AFTER DEPARTURE";
                                            objfrFlt.Rule = "CHANGE";
                                            objfrFlt.FeeCurrency = "INR";
                                            objfrFlt.FeeAmount = AftrDepAmount > 0 ? AftrDepAmount.ToString() : AftrDep == false ? "NP" : "0";// AftrDepAmount.ToString();
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = AftrDep == false ? false : true;
                                            objfrFlt.Remarks = AftrDep == false ? "Not Allowed." : ""; ;
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            objfrFlt = new FareRulesFlight();
                                            objfrFlt.Segment = sector;
                                            objfrFlt.PaxType = "ADULT";
                                            objfrFlt.DepartureType = "NO-SHOW";
                                            objfrFlt.Rule = "CHANGE";
                                            objfrFlt.FeeCurrency = "INR";

                                            objfrFlt.FeeAmount = "Before Dep.:- " + (Noshow == false ? "Not Permitted" : (NoshowAmount >= 0 ? (NoshowAmount == 0 ? "Cancellation Permitted" : "INR " + NoshowAmount.ToString()) : ""));
                                            objfrFlt.FeeAmount += " /After Dep: " + (NoshowAftrDep == false ? "Not Permitted" : (NoshowAmountAftrDep >= 0 ? (NoshowAmountAftrDep == 0 ? "Cancellation Permitted" : "INR " + NoshowAmountAftrDep.ToString()) : ""));// NoshowAmount.ToString();
                                            objfrFlt.FareBasis = fareBasis;
                                            objfrFlt.IsRefundable = Noshow == false ? false : true;
                                            objfrFlt.Remarks = Noshow == false ? "Not Allowed." : ""; ;
                                            objfrFlt.Cabin = objFR.IdType;
                                            FareRuleList.Add(objfrFlt);

                                            goto End;


                                        }

                                    }



                                }



                            }
                        End:
                            string a = "";
                        }

                    }


                }
            }
            catch (Exception ex)
            {

            }

            return FareRuleList;
        }

        public string GetFareRule(string SessionId, string userName, string officeId, string pass, string messageFunction, bool isFCCall, string fareRuleXML, string folder)
        {
            #region variables

            StringBuilder Req = new StringBuilder();
            string Response = "";
            AmdSession objHeader = new AmdSession();
            #endregion
            try
            {
                Req.Append("<?xml version='1.0' encoding='utf-8'?>");
                Req.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:sec='http://xml.amadeus.com/2010/06/Security_v1' xmlns:typ='http://xml.amadeus.com/2010/06/Types_v1' xmlns:iat='http://www.iata.org/IATA/2007/00/IATA2010.1' xmlns:app='http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3' xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1' xmlns:ses='http://xml.amadeus.com/2010/06/Session_v3' xmlns:far='http://xml.amadeus.com/FARQNQ_07_1_1A'>"); //FARQNQ_07_1_1A
                Req.Append("<soapenv:Header>");
                Req.Append(objHeader.GetSessionHeader(SessionId, userName, officeId, pass, "http://webservices.amadeus.com/FARQNQ_07_1_1A", ServiceUrls.SvcURL, IncludeSessionType.InSeries));
                Req.Append("</soapenv:Header>");
                #region Body
                Req.Append("<soapenv:Body>");

                Req.Append("<Fare_CheckRules>");
                Req.Append("<msgType>");
                Req.Append("<messageFunctionDetails>");
                Req.Append(" <messageFunction>712</messageFunction>");
                Req.Append(" </messageFunctionDetails>");
                Req.Append(" </msgType>");


                if (isFCCall)
                {

                    Req.Append(fareRuleXML);
                    // string bb = fareRuleXML.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                    //.Replace("xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\"", "").Replace("awsse:", "")
                    //.Replace("xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"", "").Replace("wsa:", "")
                    //.Replace("xmlns=\"http://xml.amadeus.com/FARQNR_07_1_1A\"", "");

                    //XDocument xmlDoc = XDocument.Parse(fareRuleXML);

                    //IEnumerable<XElement> onjFlightDetails = xmlDoc.Descendants("Fare_CheckRulesReply").Descendants("flightDetails").Descendants("travellerGrp").Descendants("travellerIdentRef").Descendants("referenceDetails").Where(x => x.Element("type").Value == "FC");
                    //Req.Append(" <itemNumber>");
                    //foreach (XElement aa in onjFlightDetails)
                    //{


                    //    Req.Append("<itemNumberDetails>");
                    //    Req.Append("<number>" + aa.Element("value").Value + "</number>");
                    //    Req.Append("</itemNumberDetails>");
                    //    Req.Append("<itemNumberDetails>");
                    //    Req.Append("<number>" + aa.Element("value").Value + "</number>");
                    //    Req.Append("<type>" + aa.Element("type").Value + "</type>");
                    //    Req.Append("</itemNumberDetails>");


                    //}
                    //Req.Append("</itemNumber>");
                }
                else
                {
                    Req.Append("<itemNumber>");
                    Req.Append("<itemNumberDetails>");
                    Req.Append("<number>1</number>");
                    Req.Append("</itemNumberDetails>");
                    //Req.Append("</itemNumber>");

                    // Req.Append("<itemNumber>");
                    Req.Append("<itemNumberDetails>");
                    Req.Append("<number>1</number>");
                    Req.Append("<type>FC</type>");
                    Req.Append("</itemNumberDetails>");
                    Req.Append("</itemNumber>");
                }

                //Req.Append(" <fareRule>");
                //Req.Append("<tarifFareRule>");

                //Req.Append(" <ruleSectionId>PE</ruleSectionId>");
                //Req.Append(" <ruleSectionId>AP</ruleSectionId>");
                //Req.Append(" <ruleSectionId>SU</ruleSectionId>");
                //Req.Append(" <ruleSectionId>CO</ruleSectionId>");
                //Req.Append(" <ruleSectionId>MX</ruleSectionId>");
                //Req.Append(" <ruleSectionId>MN</ruleSectionId>");

                //Req.Append(" </tarifFareRule>");
                //Req.Append("</fareRule>");
                Req.Append("</Fare_CheckRules>");

                Req.Append("</soapenv:Body>");
                #endregion
                Req.Append("</soapenv:Envelope>");


                Response = AmdUtility.PostXml(Req.ToString(), "http://webservices.amadeus.com/FARQNQ_07_1_1A", ServiceUrls.SvcURL, "Fare_CheckRules", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //Utility1A.SaveFile(Req.ToString(), "FareRuleRequest");
                //Utility1A.SaveFile(Response, "FareRuleRespose");

            }
            return Response;




        }

        //Method Created by Bhupender 29-Mar-18
        public string GetMiniFareRule(string SessionId, string userName, string officeId, string pass, string messageFunction, bool isFCCall, string fareRuleXML, string folder)
        {
            #region variables

            StringBuilder Req = new StringBuilder();
            string Response = "";
            AmdSession objHeader = new AmdSession();
            #endregion
            try
            {
                Req.Append("<?xml version='1.0' encoding='utf-8'?>");
                Req.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:sec='http://xml.amadeus.com/2010/06/Security_v1' xmlns:typ='http://xml.amadeus.com/2010/06/Types_v1' xmlns:iat='http://www.iata.org/IATA/2007/00/IATA2010.1' xmlns:app='http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3' xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1' xmlns:ses='http://xml.amadeus.com/2010/06/Session_v3' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_17_3_1A'>");
                Req.Append("<soapenv:Header>");
                Req.Append(objHeader.GetSessionHeader(SessionId, userName, officeId, pass, "http://webservices.amadeus.com/TMRCRQ_11_1_1A", ServiceUrls.SvcURL, IncludeSessionType.InSeries));
                Req.Append("</soapenv:Header>");
                Req.Append("<soapenv:Body>");
                #region Body

                Req.Append("<MiniRule_GetFromPricing xmlns=\"http://xml.amadeus.com/TMRCRQ_11_1_1A\">");
                Req.Append("<fareRecommendationId>");
                Req.Append("<referenceType>FRN</referenceType>");
                Req.Append("<uniqueReference>ALL</uniqueReference>");
                Req.Append("</fareRecommendationId>");
                Req.Append("</MiniRule_GetFromPricing>");

                #endregion
                Req.Append("</soapenv:Body>");
                Req.Append("</soapenv:Envelope>");
                //TMRCRQ_11_1_1A

                Response = AmdUtility.PostXml(Req.ToString(), "http://webservices.amadeus.com/TMRCRQ_11_1_1A", ServiceUrls.SvcURL, "Fare_MiniRules", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //Utility1A.SaveFile(Req.ToString(), "FareRuleRequest");
                //Utility1A.SaveFile(Response, "FareRuleRespose");

            }
            return Response;




        }

        //public string GetMiniFareRule(string SessionId, string userName, string officeId, string pass, string messageFunction, bool isFCCall, string fareRuleXML, string folder)
        //{
        //    #region variables

        //    StringBuilder Req = new StringBuilder();
        //    string Response = "";
        //    SessionHeader objHeader = new SessionHeader();
        //    #endregion
        //    try
        //    {
        //        Req.Append("<?xml version='1.0' encoding='utf-8'?>");
        //        Req.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:sec='http://xml.amadeus.com/2010/06/Security_v1' xmlns:typ='http://xml.amadeus.com/2010/06/Types_v1' xmlns:iat='http://www.iata.org/IATA/2007/00/IATA2010.1' xmlns:app='http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3' xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1' xmlns:ses='http://xml.amadeus.com/2010/06/Session_v3' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_17_3_1A'>");
        //        Req.Append("<soapenv:Header>");
        //        Req.Append(objHeader.GetSessionHeader(SessionId, userName, officeId, pass, "http://webservices.amadeus.com/TMRCRQ_11_1_1A", MethodAndSVCUrl1A.SvcURL, IncludeSessionType.InSeries));
        //        Req.Append("</soapenv:Header>");
        //        Req.Append("<soapenv:Body>");
        //        #region Body

        //        Req.Append("<MiniRule_GetFromPricing xmlns=\"http://xml.amadeus.com/TMRCRQ_11_1_1A\">");
        //        Req.Append("<fareRecommendationId>");
        //        Req.Append("<referenceType>FRN</referenceType>");
        //        Req.Append("<uniqueReference>ALL</uniqueReference>");
        //        Req.Append("</fareRecommendationId>");
        //        Req.Append("</MiniRule_GetFromPricing>");

        //        #endregion
        //        Req.Append("</soapenv:Body>");
        //        Req.Append("</soapenv:Envelope>");


        //        Response = Utility1A.PostXml(Req.ToString(), "http://webservices.amadeus.com/TMRCRQ_11_1_1A", MethodAndSVCUrl1A.SvcURL, "Fare_MiniRules", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    finally
        //    {
        //        //Utility1A.SaveFile(Req.ToString(), "FareRuleRequest");
        //        //Utility1A.SaveFile(Response, "FareRuleRespose");

        //    }
        //    return Response;




        //}

        public string GetInformPrice2(string SessionId, string userName, string officeId, string pass, ArrayList JsonArr, string constring, string folder)
        {
            #region variables

            StringBuilder Req = new StringBuilder();
            string Response = "";
            AmdSession objHeader = new AmdSession();
            List<FlightSearchResults> ResultList = new List<FlightSearchResults>();

            #endregion
            try
            {
                int i = 0, j = 0, k = 0;

                object[] ListOW = null;
                ListOW = (object[])JsonArr[0];
                i = ListOW.Length;
                if (i > 0)
                {
                    for (j = 0; j < i; j++)
                    {
                        Dictionary<string, object> Rec = (Dictionary<string, object>)((object[])(ListOW))[j];
                        FlightSearchResults ResultList2 = new FlightSearchResults();
                        ResultList2 = SetVal(Rec);
                        ResultList.Add(ResultList2);
                    }
                    Req.Append("<?xml version='1.0' encoding='utf-8'?>");
                    Req.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:sec='http://xml.amadeus.com/2010/06/Security_v1' xmlns:typ='http://xml.amadeus.com/2010/06/Types_v1' xmlns:iat='http://www.iata.org/IATA/2007/00/IATA2010.1' xmlns:app='http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3' xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1' xmlns:ses='http://xml.amadeus.com/2010/06/Session_v3' xmlns:fmp='http://xml.amadeus.com/TIPNRQ_15_1_1A'>");//FMPTBQ_17_3_1A
                    Req.Append("<soapenv:Header>");
                    Req.Append(objHeader.GetSessionHeader(SessionId, userName, officeId, pass, "http://webservices.amadeus.com/TIPNRQ_15_1_1A", ServiceUrls.SvcURL, IncludeSessionType.Start));
                    Req.Append("</soapenv:Header>");
                    #region Body
                    Req.Append("<soapenv:Body>");




                    Req.Append("<Fare_InformativePricingWithoutPNR xmlns='http://xml.amadeus.com/TIPNRQ_15_1_1A'>");


                    if (ResultList[0].Adult > 0)
                    {
                        Req.Append("<passengersGroup>");
                        Req.Append("<segmentRepetitionControl>");
                        Req.Append("<segmentControlDetails>");
                        Req.Append(" <quantity>1</quantity>");
                        Req.Append("<numberOfUnits>" + ResultList[0].Adult + "</numberOfUnits>");
                        Req.Append("</segmentControlDetails>");
                        Req.Append("</segmentRepetitionControl>");


                        Req.Append(" <travellersID>");

                        for (int a = 1; a <= ResultList[0].Adult; a++)
                        {
                            Req.Append("<travellerDetails>");
                            Req.Append("<measurementValue>" + (a).ToString() + "</measurementValue>");
                            Req.Append("</travellerDetails>");
                        }

                        Req.Append("</travellersID>");
                        Req.Append("<discountPtc>");
                        Req.Append("<valueQualifier>ADT</valueQualifier>");
                        Req.Append("</discountPtc>");

                        Req.Append("</passengersGroup>");
                    }

                    if (ResultList[0].Infant > 0)
                    {
                        Req.Append("<passengersGroup>");
                        Req.Append("<segmentRepetitionControl>");
                        Req.Append("<segmentControlDetails>");
                        Req.Append(" <quantity>2</quantity>");
                        Req.Append("<numberOfUnits>" + ResultList[0].Infant + "</numberOfUnits>");
                        Req.Append("</segmentControlDetails>");
                        Req.Append("</segmentRepetitionControl>");
                        Req.Append("<travellersID>");

                        for (int inf = 1; inf <= ResultList[0].Infant; inf++)
                        {
                            Req.Append("<travellerDetails>");
                            Req.Append("<measurementValue>" + (inf).ToString() + "</measurementValue>");
                            Req.Append("</travellerDetails>");
                        }


                        Req.Append("</travellersID>");
                        Req.Append("<discountPtc>");
                        Req.Append("<valueQualifier>INF</valueQualifier>");
                        Req.Append("<fareDetails>");
                        Req.Append("<qualifier>766</qualifier>");
                        Req.Append("</fareDetails>");
                        Req.Append("</discountPtc>");
                        Req.Append("</passengersGroup>");

                    }

                    if (ResultList[0].Child > 0)
                    {
                        Req.Append(" <passengersGroup>");
                        Req.Append(" <segmentRepetitionControl>");
                        Req.Append(" <segmentControlDetails>");
                        Req.Append(" <quantity>" + (ResultList[0].Infant > 0 ? "3" : "2") + "</quantity>");
                        Req.Append("<numberOfUnits>" + ResultList[0].Child + "</numberOfUnits>");
                        Req.Append(" </segmentControlDetails>");
                        Req.Append("</segmentRepetitionControl>");
                        Req.Append("<travellersID>");
                        for (int c = 1; c <= ResultList[0].Child; c++)
                        {
                            Req.Append("<travellerDetails>");
                            Req.Append("<measurementValue>" + (ResultList[0].Adult + c).ToString() + "</measurementValue>");
                            Req.Append("</travellerDetails>");
                        }
                        //Req.Append("<travellerDetails>");
                        //Req.Append(" <measurementValue>3</measurementValue>");
                        //Req.Append("</travellerDetails>");

                        Req.Append(" </travellersID>");
                        Req.Append("<discountPtc>");
                        Req.Append(" <valueQualifier>CH</valueQualifier>");
                        Req.Append("</discountPtc>");
                        Req.Append("</passengersGroup>");
                    }

                    //Req.Append("<tripsGroup>");
                    //Req.Append("<originDestination>");
                    //Req.Append(" <origin>" + ResultList[0].OrgDestFrom + "</origin>");
                    //Req.Append("<destination>" + ResultList[0].OrgDestTo + "</destination>");
                    //Req.Append(" </originDestination>");

                    for (int n = 0; n < ResultList.Count; n++)
                    {
                        Req.Append("<segmentGroup>");
                        Req.Append("<segmentInformation>");
                        Req.Append(" <flightDate>");
                        Req.Append(" <departureDate>" + ResultList[n].DepartureDate + "</departureDate>");
                        Req.Append("</flightDate>");
                        Req.Append("<boardPointDetails>");
                        Req.Append(" <trueLocationId>" + ResultList[n].DepartureLocation + "</trueLocationId>");
                        Req.Append("</boardPointDetails>");
                        Req.Append("<offpointDetails>");
                        Req.Append(" <trueLocationId>" + ResultList[n].ArrivalLocation + "</trueLocationId>");
                        Req.Append(" </offpointDetails>");
                        Req.Append("<companyDetails>");
                        Req.Append(" <marketingCompany>" + ResultList[n].MarketingCarrier + "</marketingCompany>");
                        Req.Append("</companyDetails>");
                        Req.Append("<flightIdentification>");
                        Req.Append(" <flightNumber>" + ResultList[n].FlightIdentification + "</flightNumber>");
                        Req.Append("<bookingClass>" + (ResultList[0].Adult == 0 ? ResultList[n].ChdRbd : ResultList[n].AdtRbd) + "</bookingClass>");
                        Req.Append("</flightIdentification>");
                      //  Req.Append("<flightTypeDetails>");
                      //  Req.Append("<flightIndicator>" + ResultList[n].Flight + "</flightIndicator>");
                      //  Req.Append("</flightTypeDetails>");
                     //   Req.Append("<itemNumber>" + (n + 1).ToString() + "</itemNumber>");
                        Req.Append("</segmentInformation>");
                        Req.Append("</segmentGroup>");
                    }




                    //for corporate Fare
                    if (ResultList[0].TripCnt == "C" && !string.IsNullOrEmpty(ResultList[0].Searchvalue.Trim()))
                    {
                        Req.Append("<pricingOptionGroup>");
                        Req.Append("<pricingOptionKey>");
                        Req.Append("<pricingOptionKey>RW</pricingOptionKey>");
                        Req.Append("</pricingOptionKey>");
                        Req.Append("<optionDetail>");
                        Req.Append("<criteriaDetails>");
                        Req.Append(" <attributeType>" + ResultList[0].Searchvalue.Trim() + "</attributeType>");
                        Req.Append("</criteriaDetails>");
                        Req.Append("</optionDetail>");
                        Req.Append(" </pricingOptionGroup>");
                    }
                    else
                    {
                        //Req.Append("<pricingOptionGroup>");
                        //Req.Append("<pricingOptionKey>");
                        //Req.Append("<pricingOptionKey>RP</pricingOptionKey>");
                        //Req.Append(" </pricingOptionKey>");
                        //Req.Append("</pricingOptionGroup>");
                        //Req.Append("<pricingOptionGroup>");
                        //Req.Append("<pricingOptionKey>");
                        //Req.Append("<pricingOptionKey>RU</pricingOptionKey>");
                        //Req.Append("</pricingOptionKey>");
                        //Req.Append("</pricingOptionGroup>");
                    }

                    // validating Carrier
                    //Req.Append("<pricingOptionGroup>");
                    //Req.Append("<pricingOptionKey>");
                    //Req.Append("<pricingOptionKey>VC</pricingOptionKey>");
                    //Req.Append("</pricingOptionKey>");
                    //Req.Append("<carrierInformation>");
                    //Req.Append("<companyIdentification>");
                    //Req.Append("<otherCompany>" + ResultList[0].ValiDatingCarrier.Trim().ToUpper() + "</otherCompany>");
                    //Req.Append("</companyIdentification>");
                    //Req.Append("</carrierInformation>");
                    //Req.Append("</pricingOptionGroup>");

                    //Req.Append("</tripsGroup>");
                    Req.Append(" </Fare_InformativePricingWithoutPNR>");





                    Req.Append("</soapenv:Body>");
                    #endregion
                    Req.Append("</soapenv:Envelope>");


                    Response = AmdUtility.PostXml(Req.ToString(), "http://webservices.amadeus.com/TIPNRQ_15_1_1A", ServiceUrls.SvcURL, "Fare_InformativePricingWithoutPNR", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");

                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //Utility1A.SaveFile(Req.ToString(), "FareRuleRequest");
                //Utility1A.SaveFile(Response, "FareRuleRespose");

            }
            return Response;
        }


        public string GetInformPrice(string SessionId, string userName, string officeId, string pass, ArrayList JsonArr, string constring, string folder)
        {
            #region variables


            StringBuilder Req = new StringBuilder();
            string Response = "";
            AmdSession objHeader = new AmdSession();
            List<FlightSearchResults> ResultList = new List<FlightSearchResults>();





            #endregion
            try
            {
                int i = 0, j = 0, k = 0;

                object[] ListOW = null;
                ListOW = (object[])JsonArr[0];
                i = ListOW.Length;
                if (i > 0)
                {
                    for (j = 0; j < i; j++)
                    {
                        Dictionary<string, object> Rec = (Dictionary<string, object>)((object[])(ListOW))[j];
                        FlightSearchResults ResultList2 = new FlightSearchResults();
                        ResultList2 = SetVal(Rec);
                        ResultList.Add(ResultList2);
                    }
                    Req.Append("<?xml version='1.0' encoding='utf-8'?>");
                    Req.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:sec='http://xml.amadeus.com/2010/06/Security_v1' xmlns:typ='http://xml.amadeus.com/2010/06/Types_v1' xmlns:iat='http://www.iata.org/IATA/2007/00/IATA2010.1' xmlns:app='http://xml.amadeus.com/2010/06/AppMdw_CommonTypes_v3' xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1' xmlns:ses='http://xml.amadeus.com/2010/06/Session_v3' xmlns:fmp='http://xml.amadeus.com/TIPNRQ_18_1_1A'>");
                    Req.Append("<soapenv:Header>");
                    Req.Append(objHeader.GetSessionHeader(SessionId, userName, officeId, pass, "http://webservices.amadeus.com/TIPNRQ_18_1_1A", ServiceUrls.SvcURL, IncludeSessionType.Start));
                    Req.Append("</soapenv:Header>");
                    #region Body
                    Req.Append("<soapenv:Body>");




                    Req.Append(" <Fare_InformativePricingWithoutPNR xmlns='http://xml.amadeus.com/TIPNRQ_18_1_1A'>");
                    //Req.Append(" <messageDetails>");
                    //Req.Append("<messageFunctionDetails>");
                    //Req.Append("<businessFunction>1</businessFunction>");
                    //Req.Append("<messageFunction>741</messageFunction>");
                    //Req.Append("<responsibleAgency>1A</responsibleAgency>");
                    //Req.Append("</messageFunctionDetails>");
                    //Req.Append("</messageDetails>");

                    if (ResultList[0].Adult > 0)
                    {
                        Req.Append("<passengersGroup>");
                        Req.Append("<segmentRepetitionControl>");
                        Req.Append("<segmentControlDetails>");
                        Req.Append(" <quantity>1</quantity>");
                        Req.Append("<numberOfUnits>" + ResultList[0].Adult + "</numberOfUnits>");
                        Req.Append("</segmentControlDetails>");
                        Req.Append("</segmentRepetitionControl>");


                        Req.Append(" <travellersID>");

                        for (int a = 1; a <= ResultList[0].Adult; a++)
                        {
                            Req.Append("<travellerDetails>");
                            Req.Append("<measurementValue>" + (a).ToString() + "</measurementValue>");
                            Req.Append("</travellerDetails>");
                        }

                        Req.Append("</travellersID>");
                        Req.Append("<discountPtc>");
                        Req.Append("<valueQualifier>ADT</valueQualifier>");
                        Req.Append("</discountPtc>");

                        Req.Append("</passengersGroup>");
                    }

                    if (ResultList[0].Infant > 0)
                    {
                        Req.Append("<passengersGroup>");
                        Req.Append("<segmentRepetitionControl>");
                        Req.Append("<segmentControlDetails>");
                        Req.Append(" <quantity>2</quantity>");
                        Req.Append("<numberOfUnits>" + ResultList[0].Infant + "</numberOfUnits>");
                        Req.Append("</segmentControlDetails>");
                        Req.Append("</segmentRepetitionControl>");
                        Req.Append("<travellersID>");

                        for (int inf = 1; inf <= ResultList[0].Infant; inf++)
                        {
                            Req.Append("<travellerDetails>");
                            Req.Append("<measurementValue>" + (inf).ToString() + "</measurementValue>");
                            Req.Append("</travellerDetails>");
                        }


                        Req.Append(" </travellersID>");
                        Req.Append("<discountPtc>");
                        Req.Append("<valueQualifier>INF</valueQualifier>");
                        Req.Append("<fareDetails>");
                        Req.Append("  <qualifier>766</qualifier>");
                        Req.Append("</fareDetails>");
                        Req.Append("</discountPtc>");
                        Req.Append("</passengersGroup>");

                    }

                    if (ResultList[0].Child > 0)
                    {
                        Req.Append(" <passengersGroup>");
                        Req.Append(" <segmentRepetitionControl>");
                        Req.Append(" <segmentControlDetails>");
                        Req.Append(" <quantity>" + (ResultList[0].Infant > 0 ? "3" : "2") + "</quantity>");
                        Req.Append("<numberOfUnits>" + ResultList[0].Child + "</numberOfUnits>");
                        Req.Append(" </segmentControlDetails>");
                        Req.Append("</segmentRepetitionControl>");
                        Req.Append("<travellersID>");
                        for (int c = 1; c <= ResultList[0].Child; c++)
                        {
                            Req.Append("<travellerDetails>");
                            Req.Append("<measurementValue>" + (ResultList[0].Adult + c).ToString() + "</measurementValue>");
                            Req.Append("</travellerDetails>");
                        }
                        //Req.Append("<travellerDetails>");
                        //Req.Append(" <measurementValue>3</measurementValue>");
                        //Req.Append("</travellerDetails>");

                        Req.Append(" </travellersID>");
                        Req.Append("<discountPtc>");
                        Req.Append(" <valueQualifier>CH</valueQualifier>");
                        Req.Append("</discountPtc>");
                        Req.Append("</passengersGroup>");
                    }

                    //Req.Append("<tripsGroup>");
                    //Req.Append("<originDestination>");
                    //Req.Append(" <origin>" + ResultList[0].OrgDestFrom + "</origin>");
                    //Req.Append("<destination>" + ResultList[0].OrgDestTo + "</destination>");
                    //Req.Append(" </originDestination>");

                    for (int ig = 0; ig < ResultList.Count; ig++)
                    {
                        Req.Append("<segmentGroup>");
                        Req.Append("<segmentInformation>");
                        Req.Append(" <flightDate>");
                        Req.Append(" <departureDate>" + ResultList[ig].DepartureDate + "</departureDate>");
                        Req.Append("</flightDate>");
                        Req.Append("<boardPointDetails>");
                        Req.Append(" <trueLocationId>" + ResultList[ig].DepartureLocation + "</trueLocationId>");
                        Req.Append("</boardPointDetails>");
                        Req.Append("<offpointDetails>");
                        Req.Append(" <trueLocationId>" + ResultList[ig].ArrivalLocation + "</trueLocationId>");
                        Req.Append(" </offpointDetails>");
                        Req.Append("<companyDetails>");
                        Req.Append(" <marketingCompany>" + ResultList[ig].MarketingCarrier + "</marketingCompany>");
                        Req.Append("</companyDetails>");
                        Req.Append("<flightIdentification>");
                        Req.Append(" <flightNumber>" + ResultList[ig].FlightIdentification + "</flightNumber>");
                        Req.Append("<bookingClass>" + (ResultList[0].Adult == 0 ? ResultList[ig].ChdRbd : ResultList[ig].AdtRbd) + "</bookingClass>");
                        Req.Append("</flightIdentification>");
                      //  Req.Append("<flightTypeDetails>");
                     //   Req.Append("<flightIndicator>" + ResultList[ig].Flight + "</flightIndicator>");
                      //  Req.Append("</flightTypeDetails>");
                      //  Req.Append("<itemNumber>" + (ig + 1).ToString() + "</itemNumber>");
                        Req.Append("</segmentInformation>");
                        Req.Append("</segmentGroup>");
                    }




                    //for corporate Fare
                    if (ResultList[0].TripCnt == "C" && !string.IsNullOrEmpty(ResultList[0].Searchvalue.Trim()))
                    {
                        Req.Append("<pricingOptionGroup>");
                        Req.Append("<pricingOptionKey>");
                        Req.Append("<pricingOptionKey>RW</pricingOptionKey>");
                        Req.Append("</pricingOptionKey>");
                        Req.Append("<optionDetail>");
                        Req.Append("<criteriaDetails>");
                        Req.Append(" <attributeType>" + ResultList[0].Searchvalue.Trim() + "</attributeType>");
                        Req.Append("</criteriaDetails>");
                        Req.Append("</optionDetail>");
                        Req.Append(" </pricingOptionGroup>");
                    }
                    else
                    {
                        //Req.Append("<pricingOptionGroup>");
                        //Req.Append("<pricingOptionKey>");
                        //Req.Append("<pricingOptionKey>RP</pricingOptionKey>");
                        //Req.Append(" </pricingOptionKey>");
                        //Req.Append("</pricingOptionGroup>");
                        //Req.Append("<pricingOptionGroup>");
                        //Req.Append("<pricingOptionKey>");
                        //Req.Append("<pricingOptionKey>RU</pricingOptionKey>");
                        //Req.Append("</pricingOptionKey>");
                        //Req.Append("</pricingOptionGroup>");
                    }

                    //// validating Carrier
                    //Req.Append("<pricingOptionGroup>");
                    //Req.Append("<pricingOptionKey>");
                    //Req.Append("<pricingOptionKey>VC</pricingOptionKey>");
                    //Req.Append("</pricingOptionKey>");
                    //Req.Append("<carrierInformation>");
                    //Req.Append("<companyIdentification>");
                    //Req.Append("<otherCompany>" + ResultList[0].ValiDatingCarrier.Trim().ToUpper() + "</otherCompany>");
                    //Req.Append("</companyIdentification>");
                    //Req.Append("</carrierInformation>");
                    //Req.Append("</pricingOptionGroup>");

                    //Req.Append("</tripsGroup>");
                    Req.Append(" </Fare_InformativePricingWithoutPNR>");





                    Req.Append("</soapenv:Body>");
                    #endregion
                    Req.Append("</soapenv:Envelope>");


                    Response = AmdUtility.PostXml(Req.ToString(), "http://webservices.amadeus.com/TIPNRQ_18_1_1A", ServiceUrls.SvcURL, "Fare_InformativePricingWithoutPNR", folder);//"http://webservices.amadeus.com/1ASIWLNBLOK/FMPTBQ_13_1_1A");
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //Utility1A.SaveFile(Req.ToString(), "FareRuleRequest");
                //Utility1A.SaveFile(Response, "FareRuleRespose");

            }
            return Response;




        }


        public string GetBaggageVal(string baggageCode, string BaggageAllowance)
        {
            string Baggage = "";
            if (baggageCode == "W" || baggageCode == "700")
            {
                Baggage = BaggageAllowance + " KG " + " Baggage included";//flt1.ToList()[0].unitQualifier +


            }
            else if (baggageCode == "N")
            {
                Baggage = BaggageAllowance + " Piece(s) Baggage included";

            }
            else if (baggageCode == "701")
            {
                Baggage = BaggageAllowance + " 	Pounds Baggage included";

            }
            else if (baggageCode == "702")
            {
                Baggage = " Baggage not included";

            }

            return Baggage;
        }


        public static FlightSearchResults SetVal(Dictionary<string, object> Row)
        {
            FlightSearchResults obj = new FlightSearchResults();
            int length = Row.Count;
            int i = 0;
            Type t;
            //List<object> a = new List<object>();
            Dictionary<string, object> InsertRow = new Dictionary<string, object>();
            try
            {
                // Add Records to Final Dictionary  by Removing Null values.
                foreach (KeyValuePair<string, object> pair in Row)
                {
                    if (pair.Value == null)
                    {
                        InsertRow.Add(pair.Key, "");
                    }
                    else
                    {
                        InsertRow.Add(pair.Key, pair.Value);
                    }
                }
                //Set Values in the Object
                //Note :-Insert TrackId generated by Random Function.
                //obj.DISTRID = InsertRow["FltInfoID"].ToString();
                //obj.DISTRID = InsertRow["DISTRID"].ToString(); //From Session
                obj.OrgDestFrom = InsertRow["OrgDestFrom"].ToString();
                obj.OrgDestTo = InsertRow["OrgDestTo"].ToString();
                obj.DepartureLocation = InsertRow["DepartureLocation"].ToString();
                obj.DepartureCityName = InsertRow["DepartureCityName"].ToString();
                obj.DepAirportCode = InsertRow["DepAirportCode"].ToString();
                obj.DepartureAirportName = InsertRow["DepartureTerminal"].ToString();
                obj.ArrivalLocation = InsertRow["ArrivalLocation"].ToString();
                obj.ArrivalCityName = InsertRow["ArrivalCityName"].ToString();
                obj.ArrAirportCode = InsertRow["ArrAirportCode"].ToString();
                obj.ArrivalTerminal = InsertRow["ArrivalTerminal"].ToString();
                obj.DepartureDate = InsertRow["DepartureDate"].ToString();
                obj.Departure_Date = InsertRow["Departure_Date"].ToString();
                obj.DepartureTime = InsertRow["DepartureTime"].ToString();
                obj.ArrivalDate = InsertRow["ArrivalDate"].ToString();
                obj.Arrival_Date = InsertRow["Arrival_Date"].ToString();
                obj.ArrivalTime = InsertRow["ArrivalTime"].ToString();
                obj.Adult = Convert.ToInt32(InsertRow["Adult"].ToString());
                obj.Child = Convert.ToInt32(InsertRow["Child"].ToString());
                obj.Infant = Convert.ToInt32(InsertRow["Infant"].ToString());
                obj.TotPax = obj.Adult + obj.Child + obj.Infant;
                obj.MarketingCarrier = InsertRow["MarketingCarrier"].ToString();
                obj.OperatingCarrier = InsertRow["OperatingCarrier"].ToString();
                obj.FlightIdentification = InsertRow["FlightIdentification"].ToString();
                obj.ValiDatingCarrier = InsertRow["ValiDatingCarrier"].ToString();
                obj.AirLineName = InsertRow["AirLineName"].ToString();
                obj.AvailableSeats = InsertRow["AvailableSeats"].ToString();
                obj.AdtCabin = InsertRow["AdtCabin"].ToString();
                obj.ChdCabin = InsertRow["ChdCabin"].ToString();
                obj.InfCabin = InsertRow["InfCabin"].ToString();
                obj.AdtRbd = InsertRow["AdtRbd"].ToString();
                obj.ChdRbd = InsertRow["ChdRbd"].ToString();
                obj.InfRbd = InsertRow["InfRbd"].ToString();
                obj.RBD = InsertRow["RBD"].ToString();
                obj.AdtFareType = InsertRow["AdtFare"].ToString();
                obj.AdtFarebasis = InsertRow["AdtBfare"].ToString();
                obj.AdtTax = float.Parse(InsertRow["AdtTax"].ToString());
                obj.ChdFare = float.Parse(InsertRow["ChdFare"].ToString());
                obj.ChdBFare = float.Parse(InsertRow["ChdBFare"].ToString());
                obj.ChdTax = float.Parse(InsertRow["ChdTax"].ToString());
                obj.InfFare = float.Parse(InsertRow["InfFare"].ToString());
                obj.InfBfare = float.Parse(InsertRow["InfBfare"].ToString());
                obj.InfTax = float.Parse(InsertRow["InfTax"].ToString());
                obj.TotBfare = float.Parse(InsertRow["TotBfare"].ToString());
                obj.TotalFare = float.Parse(InsertRow["TotalFare"].ToString()); //TotalFare
                obj.TotalTax = float.Parse(InsertRow["TotalTax"].ToString());
                obj.NetFare = float.Parse(InsertRow["NetFare"].ToString());
                obj.STax = float.Parse(InsertRow["STax"].ToString());
                obj.TFee = float.Parse(InsertRow["TFee"].ToString());
                obj.TotDis = float.Parse(InsertRow["TotDis"].ToString());
                obj.Searchvalue = InsertRow["Searchvalue"].ToString();
                // obj.LineNumber = Convert.ToInt32(InsertRow["LineNumber"].ToString());
                obj.Leg = Convert.ToInt32(InsertRow["Leg"].ToString());
                obj.Flight = InsertRow["Flight"].ToString();
                obj.Provider = InsertRow["Provider"].ToString();
                obj.TotDur = InsertRow["TotDur"].ToString();
                obj.TripType = InsertRow["TripType"].ToString();
                obj.EQ = InsertRow["EQ"].ToString();
                obj.Stops = InsertRow["Stops"].ToString();
                obj.Trip = InsertRow["Trip"].ToString();
                obj.Sector = InsertRow["Sector"].ToString();
                obj.TripCnt = InsertRow["TripCnt"].ToString();
                obj.ADTAdminMrk = float.Parse(InsertRow["ADTAdminMrk"].ToString());
                obj.ADTAgentMrk = float.Parse(InsertRow["ADTAgentMrk"].ToString());
                obj.CHDAdminMrk = float.Parse(InsertRow["CHDAdminMrk"].ToString());
                obj.CHDAgentMrk = float.Parse(InsertRow["CHDAgentMrk"].ToString());
                obj.InfAdminMrk = float.Parse(InsertRow["InfAdminMrk"].ToString());
                obj.InfAgentMrk = float.Parse(InsertRow["InfAgentMrk"].ToString());

                obj.IATAComm = float.Parse(InsertRow["IATAComm"].ToString());
                obj.AdtFareType = InsertRow["AdtFareType"].ToString();

                obj.AdtFareType = InsertRow["AdtFarebasis"].ToString();
                obj.ChdfareType = InsertRow["ChdfareType"].ToString();
                obj.ChdFarebasis = InsertRow["ChdFarebasis"].ToString();
                obj.InfFareType = InsertRow["InfFareType"].ToString();
                obj.InfFarebasis = InsertRow["InfFarebasis"].ToString();
                obj.fareBasis = InsertRow["fareBasis"].ToString();
                obj.FBPaxType = InsertRow["FBPaxType"].ToString();
                obj.AdtFSur = float.Parse(InsertRow["AdtFSur"].ToString());
                obj.ChdFSur = float.Parse(InsertRow["ChdFSur"].ToString());
                obj.InfFSur = float.Parse(InsertRow["InfFSur"].ToString());
                obj.TotalFuelSur = float.Parse(InsertRow["AdtFSur"].ToString()) + float.Parse(InsertRow["ChdFSur"].ToString()) + float.Parse(InsertRow["InfFSur"].ToString());
                obj.sno = InsertRow["sno"].ToString();
                obj.depdatelcc = InsertRow["depdatelcc"].ToString();
                obj.arrdatelcc = InsertRow["arrdatelcc"].ToString();
                obj.OriginalTF = float.Parse(InsertRow["OriginalTF"].ToString());
                obj.OriginalTT = float.Parse(InsertRow["OriginalTT"].ToString());
                //obj.TRACK_ID = TID;  //TrackId by Random Function //InsertRow["Track_id"].ToString();
                //Fare BreakUp Fields 
                obj.FType = InsertRow["FType"].ToString();
                //obj.ADT_TAX = Append_Tax(InsertRow["AdtFSur"].ToString(), InsertRow["AdtYR"].ToString(), InsertRow["AdtWO"].ToString(), InsertRow["AdtIN"].ToString(), InsertRow["AdtQ"].ToString(), InsertRow["AdtJN"].ToString(), InsertRow["AdtOT"].ToString());
                obj.AdtOT = float.Parse(InsertRow["AdtOT"].ToString());
                obj.AdtSrvTax = float.Parse(InsertRow["AdtSrvTax"].ToString());
                obj.AdtSrvTax1 = float.Parse(InsertRow["AdtSrvTax1"].ToString());
                //obj.CHD_TAX = Append_Tax(InsertRow["ChdFSur"].ToString(), InsertRow["ChdYR"].ToString(), InsertRow["ChdWO"].ToString(), InsertRow["ChdIN"].ToString(), InsertRow["ChdQ"].ToString(), InsertRow["ChdJN"].ToString(), InsertRow["ChdOT"].ToString());
                obj.ChdOT = float.Parse(InsertRow["ChdOT"].ToString());
                obj.ChdSrvTax = float.Parse(InsertRow["ChdSrvTax"].ToString());
                obj.ChdSrvTax1 = float.Parse(InsertRow["ChdSrvTax1"].ToString());
                //obj.INF_TAX = Append_Tax(InsertRow["InfFSur"].ToString(), InsertRow["InfYR"].ToString(), InsertRow["InfWO"].ToString(), InsertRow["InfIN"].ToString(), InsertRow["InfQ"].ToString(), InsertRow["InfJN"].ToString(), InsertRow["InfOT"].ToString());
                obj.InfOT = float.Parse(InsertRow["InfOT"].ToString());
                obj.InfSrvTax = float.Parse(InsertRow["InfSrvTax"].ToString());
                obj.STax = float.Parse(InsertRow["STax"].ToString());//Convert.ToDecimal(obj.ADTSRVTAX) + Convert.ToDecimal(obj.CHDSRVTAX) + Convert.ToDecimal(obj.INFSRVTAX);
                // obj.TF = Convert.ToDecimal(InsertRow["TFee"].ToString());
                // obj.TC = Convert.ToDecimal(InsertRow["TotMrkUp"].ToString());//((Convert.ToDecimal(obj.ADTADMINMRK) + Convert.ToDecimal(obj.ADTAGENTMRK)) * obj.ADULT) + ((Convert.ToDecimal(obj.CHDADMINMRK) + Convert.ToDecimal(obj.CHDAGENTMRK)) * obj.CHILD) + ((Convert.ToDecimal(obj.INFADMINMRK) + Convert.ToDecimal(obj.INFAGENTMRK)) * obj.INFANT);
                obj.AdtTds = float.Parse(InsertRow["AdtTds"].ToString());
                obj.ChdTds = float.Parse(InsertRow["ChdTds"].ToString());
                obj.InfTds = float.Parse(InsertRow["InfTds"].ToString());
                obj.AdtDiscount = float.Parse(InsertRow["AdtDiscount"].ToString());
                obj.AdtDiscount1 = float.Parse(InsertRow["AdtDiscount1"].ToString());
                obj.ChdDiscount = float.Parse(InsertRow["ChdDiscount"].ToString());
                obj.ChdDiscount1 = float.Parse(InsertRow["ChdDiscount1"].ToString());
                obj.InfDiscount = float.Parse(InsertRow["InfDiscount"].ToString());
                obj.AdtCB = float.Parse(InsertRow["AdtCB"].ToString());
                obj.ChdCB = float.Parse(InsertRow["ChdCB"].ToString());
                obj.InfCB = float.Parse(InsertRow["InfCB"].ToString());
                obj.ElectronicTicketing = InsertRow["ElectronicTicketing"].ToString();
                obj.AdtMgtFee = float.Parse(InsertRow["AdtMgtFee"].ToString());
                obj.ChdMgtFee = float.Parse(InsertRow["ChdMgtFee"].ToString());
                obj.InfMgtFee = float.Parse(InsertRow["InfMgtFee"].ToString());
                obj.TotMgtFee = float.Parse(InsertRow["TotMgtFee"].ToString());
                //obj.User_id = InsertRow["User_id"].ToString();
                obj.IsCorp = Convert.ToBoolean(InsertRow["IsCorp"].ToString());
                obj.ProductDetailQualifier = InsertRow["ProductDetailQualifier"].ToString();
            }
            catch (Exception ex)
            {
                //ExceptionLogger.FileHandling("FlightSearchService", "Err_001", ex, "FlightSearch");
            }

            return obj;
        }

        public string Price_PNR_New_FBA(string sessionid, string SoapAction, string VC, string username, string officeID, string pass, string svcUrl, IncludeSessionType includeSessionType)
        {

            AmdSession objSessuonHeader = new AmdSession();
            StringBuilder fare = new StringBuilder();
            StringBuilder fare1 = new StringBuilder();

            fare.Append("<?xml version='1.0' encoding='utf-8'?>");
            //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
            fare.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:tpc='http://xml.amadeus.com/TPCBRQ_07_3_1A'>");
            fare.Append("<soapenv:Header>");
            fare.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));//GetSessionHeader(sessionid)); //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
            fare.Append("</soapenv:Header>");
            fare.Append("<soapenv:Body>");
            fare.Append("<Fare_PricePNRWithBookingClass> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>RP</pricingOptionKey> </pricingOptionKey> </pricingOptionGroup> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>RU</pricingOptionKey> </pricingOptionKey> </pricingOptionGroup> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>RLO</pricingOptionKey> </pricingOptionKey> </pricingOptionGroup> <pricingOptionGroup> <pricingOptionKey> <pricingOptionKey>VC</pricingOptionKey> </pricingOptionKey> <carrierInformation> <companyIdentification> <otherCompany>" + VC + "</otherCompany> </companyIdentification> </carrierInformation> </pricingOptionGroup> </Fare_PricePNRWithBookingClass>");

            fare.Append("</soapenv:Body>");
            fare.Append("</soapenv:Envelope>");
            return fare.ToString();
        }

    }
}
