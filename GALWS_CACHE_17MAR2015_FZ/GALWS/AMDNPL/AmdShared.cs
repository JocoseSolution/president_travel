﻿using System.Collections.Generic;

namespace GALWS.AMDNPL
{
    public class SoapActionUrl
    {
        public string SIGNIN { get; set; }
        public string SELL { get; set; }
        public string ADDPAX { get; set; }
        public string PRICING { get; set; }
        public string TST { get; set; }
        public string GDSPNR { get; set; }
        public string AIRLINEPNR { get; set; }
        public string IGNORE { get; set; }
        public string SIGNOUT { get; set; }
        public string CRYPTIC { get; set; }
        public string DOCISSUANCE { get; set; }
        public string PNRRET { get; set; }
    }
    public class TicketingCrd
    {
        public string Corporate_ID { get; set; }
        public string Office_ID { get; set; }
        public string Password { get; set; }
        public bool OnlineTkt { get; set; }
        public string SigninXML { get; set; }
    }
    public class Flights_1A
    {
        public string Seg_Ref { get; set; }
        public string Flt_Schdule { get; set; }
        public string Flt_ref { get; set; }
        public string Eft_Ref { get; set; }
        public string Mcx_Ref { get; set; }
        public string DateOfDeparture { get; set; }
        public string TimeOfDeparture { get; set; }
        public string DateOfArrival { get; set; }
        public string TimeOfArrival { get; set; }
        public string LocationId1 { get; set; }
        public string LocationId1T { get; set; }
        public string LocationId2 { get; set; }
        public string LocationId2T { get; set; }
        public string MarketingCarrier { get; set; }
        public string ValidatingCarrier { get; set; }
        public string OperatingCarrier { get; set; }
        public string FlightNumber { get; set; }
        public string EquipmentType { get; set; }
        public string ElectronicTicketing { get; set; }
        public string ProductDetailQualifier { get; set; }
        public string Baggage { get; set; }
        public string Penalty { get; set; }
    }

    public class PaxFare_1A
    {
        public string Adult { get; set; }
        public string Child { get; set; }
        public string Inf { get; set; }
        public string AdultFare { get; set; }
        public string ChildFare { get; set; }
        public string InfantFare { get; set; }
        public string TimeOfDeparture { get; set; }
        public string DateOfArrival { get; set; }
        public string TimeOfArrival { get; set; }
        public string LocationId1 { get; set; }
        public string LocationId1T { get; set; }
        public string LocationId2 { get; set; }
        public string LocationId2T { get; set; }
        public string MarketingCarrier { get; set; }
        public string OperatingCarrier { get; set; }
        public string FlightNumber { get; set; }
        public string EquipmentType { get; set; }
        public string ElectronicTicketing { get; set; }
        public string ProductDetailQualifier { get; set; }
    }

    //Fare Details Node- For Each Segment
    public class LegFareDtls_1A
    {
        public int RecomNo { get; set; }
        public int SEGREF { get; set; }
        public string ADT_RBD { get; set; }
        public string CHD_RBD { get; set; }
        public string INF_RBD { get; set; }
        public string ADT_CABIN { get; set; }
        public string CHD_CABIN { get; set; }
        public string INF_CABIN { get; set; }
        public string ADT_AVLSTATUS { get; set; }
        public string CHD_AVLSTATUS { get; set; }
        public string INF_AVLSTATUS { get; set; }
        public string ADT_FAREBASIS { get; set; }
        public string CHD_FAREBASIS { get; set; }
        public string INF_FAREBASIS { get; set; }
        public string ADT_FARETYPE { get; set; }
        public string CHD_FARETYPE { get; set; }
        public string INF_FARETYPE { get; set; }
        //Common
        public string BREAKPOINT { get; set; }
        public string ADT_CNT { get; set; }
        public string CHD_CNT { get; set; }
        public string INF_CNT { get; set; }
        public string CHD_BREAKPOINT { get; set; }
        public string INF_BREAKPOINT { get; set; }
        public string Corporate_Code { get; set; }


    }
    public class BlockAirline
    {

        public string Airline { get; set; }
        public float Provider { get; set; }
        public float Trip { get; set; }

    }
    public class BaggList
    {

        public string TripType { get; set; }
        public string Baggage { get; set; }




    }


    public class TICKETDTL
    {
        public string PaxType { get; set; }
        public string TKTNo { get; set; }
        public string PTNo { get; set; }
        public string STNo { get; set; }

    }
    public class SeatRequest
    {
        public string departureDate { get; set; }
        public string departureTime { get; set; }
        public string validatingCarrier { get; set; }
        public string trueLocationIdB { get; set; }
        public string trueLocationIdO { get; set; }
        public string Trip { get; set; }
        public string flightNumber { get; set; }
        public string bookingClass { get; set; }
        public string OrderID { get; set; }
        public string TCCode { get; set; }
        public string Provider { get; set; }
        public string FareBasis { get; set; }
    }
    public class FFDetails
    {
        public string Carrier { get; set; }
        public string Number { get; set; }
        public string TierLevel { get; set; }
    }
    public class CorporateId
    {
        public string CorporateQualifier { get; set; }
        public string identity { get; set; }
    }
    public class TicketDetails
    {
        public List<TICKETDTL> TicketDetail { get; set; }
        public List<string> PriceType { get; set; }
        public List<FFDetails> FFDetails { get; set; }
        public List<CorporateId> CorporateId { get; set; }
        public string SellLocationId { get; set; }
        public string TktLocationId { get; set; }
        public string Cabin { get; set; }
        public bool IsPrivateFare { get; set; }
        public string TCCode { get; set; }
        public string Trip { get; set; }
        public string AirLine { get; set; }
    }
    public class CORP_ENTY_CONFIG
    {
        public string TCCode { get; set; }
        public string TPCode { get; set; }

    }
}

