﻿using System;
using System.Linq;
using System.Text;
using System.Security.Cryptography;

using System.Xml.Linq;
using STD.BAL;

namespace GALWS.AMDNPL
{
   public class AmdSession
    {
        #region SessionHeader
        public string GetSessionHeader(string SH, string username, string agentCode, string pass, string action, string serviceUrl, IncludeSessionType includeSessionType)
        {


            StringBuilder XmlBuilder = new StringBuilder();




            if (includeSessionType == IncludeSessionType.NO || includeSessionType == IncludeSessionType.Start)
            {

                SHA1 sha1 = SHA1.Create();
                string timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss") + "Z";//"2016-10-13T6:37:48Z";


                string nonsce = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15);

                // var bytes = Encoding.UTF8.GetBytes(nonsce);


                byte[] bytesnonse = Encoding.UTF8.GetBytes(nonsce);
                var base64Nonce = Convert.ToBase64String(bytesnonse);

                byte[] bytedate = Encoding.UTF8.GetBytes(timestamp);
                byte[] bytepass = sha1.ComputeHash(Encoding.Default.GetBytes(pass));//("AMADEUS"));

                AmdUtility objUtiity = new AmdUtility();
                var result = objUtiity.ConcatArrays(objUtiity.ConcatArrays(bytesnonse, bytedate), bytepass);

                var bytesPassDigest = sha1.ComputeHash(result);

                var base64passDigest = Convert.ToBase64String(bytesPassDigest);


                XmlBuilder.Append("<add:MessageID xmlns:add='http://www.w3.org/2005/08/addressing'>" + Guid.NewGuid().ToString() + "</add:MessageID>");
                XmlBuilder.Append("<add:Action xmlns:add='http://www.w3.org/2005/08/addressing'>" + action + "</add:Action>");//http://webservices.amadeus.com/FMPTBQ_14_3_1A
                XmlBuilder.Append("<add:To xmlns:add='http://www.w3.org/2005/08/addressing'>" + serviceUrl + "</add:To>");//https://nodeD1.test.webservices.amadeus.com/1ASIWBKGWRA/
                XmlBuilder.Append("<link:TransactionFlowLink xmlns:link='http://wsdl.amadeus.com/2010/06/ws/Link_v1'/>");
                XmlBuilder.Append("<oas:Security xmlns:oas='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd'>");
                XmlBuilder.Append("<oas:UsernameToken oas1:Id='UsernameToken-1' xmlns:oas1='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd'>");
                XmlBuilder.Append("<oas:Username>" + username + "</oas:Username>");
                XmlBuilder.Append("<oas:Nonce EncodingType='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary'>" + base64Nonce + "</oas:Nonce>");
                XmlBuilder.Append("<oas:Password Type='http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordDigest'>" + base64passDigest + "</oas:Password>");
                XmlBuilder.Append("<oas1:Created>" + timestamp + "</oas1:Created>");
                XmlBuilder.Append("</oas:UsernameToken>");
                XmlBuilder.Append("</oas:Security>");
                XmlBuilder.Append("<AMA_SecurityHostedUser xmlns='http://xml.amadeus.com/2010/06/Security_v1'>");
                XmlBuilder.Append("<UserID AgentDutyCode='SU' POS_Type='1' PseudoCityCode='" + agentCode + "' RequestorType='U'/>");
                XmlBuilder.Append("</AMA_SecurityHostedUser>");

                if (includeSessionType == IncludeSessionType.Start)
                {
                    XmlBuilder.Append(" <awsse:Session TransactionStatusCode='Start' xmlns:awsse='http://xml.amadeus.com/2010/06/Session_v3'/>");

                }
            }

            else if (includeSessionType == IncludeSessionType.InSeries || includeSessionType == IncludeSessionType.End)
            {
                string[] ss = Utility.Split(SH, "|");

                XmlBuilder.Append("<awsse:Session TransactionStatusCode='" + includeSessionType.ToString() + "' xmlns:awsse='http://xml.amadeus.com/2010/06/Session_v3'>");
                XmlBuilder.Append(" <awsse:SessionId>" + ss[0].ToString() + "</awsse:SessionId>");
                XmlBuilder.Append(" <awsse:SequenceNumber>" + ss[1].ToString() + "</awsse:SequenceNumber>");
                XmlBuilder.Append(" <awsse:SecurityToken>" + ss[2].ToString() + "</awsse:SecurityToken>");
                XmlBuilder.Append(" </awsse:Session>");
                XmlBuilder.Append("<add:MessageID xmlns:add='http://www.w3.org/2005/08/addressing'>" + Guid.NewGuid().ToString() + "</add:MessageID>");
                XmlBuilder.Append("<add:Action xmlns:add='http://www.w3.org/2005/08/addressing'>" + action + "</add:Action>");
                XmlBuilder.Append("<add:To xmlns:add='http://www.w3.org/2005/08/addressing'>" + serviceUrl + "</add:To>");

                //XmlBuilder.Append("<wbs:Session >");
                //XmlBuilder.Append("<wbs:SessionId>" + ss[0].ToString() + "</wbs:SessionId>");
                //XmlBuilder.Append("<wbs:SequenceNumber>" + ss[1].ToString() + "</wbs:SequenceNumber>");
                //XmlBuilder.Append("<wbs:SecurityToken>" + ss[2].ToString() + "</wbs:SecurityToken>");
                //XmlBuilder.Append("</wbs:Session>");

            }
            return XmlBuilder.ToString();
        }
        #endregion


        public string GetSessionInseries(string responseXml)
        {
            string session = "";
            string xmm = responseXml.Replace("xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\"", "").Replace("soapenv:", "")
                        .Replace("xmlns:awsse=\"http://xml.amadeus.com/2010/06/Session_v3\"", "").Replace("awsse:", "")
                        .Replace("xmlns:wsa=\"http://www.w3.org/2005/08/addressing\"", "").Replace("wsa:", "");

            XDocument xmlDoc = XDocument.Parse(xmm);

            XElement cSession = xmlDoc.Descendants("Session").FirstOrDefault();

            string TransactionStatusCode = cSession.Attribute("TransactionStatusCode").Value;
            string sssnId = cSession.Element("SessionId").Value;
            string sssnSeqNo = cSession.Element("SequenceNumber").Value;
            string sssnSecToken = cSession.Element("SecurityToken").Value;

            if (TransactionStatusCode.ToLower().Trim() == "inseries")
            {
                session = sssnId + "|" + Convert.ToInt32(Convert.ToInt32(sssnSeqNo) + 1) + "|" + sssnSecToken;
            }


            return session;


        }
    }
}




