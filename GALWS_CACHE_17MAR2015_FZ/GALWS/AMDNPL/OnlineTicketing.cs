﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Xml.Linq;
using System.Xml;
using System.Collections;
using System.Data;
using System.Reflection;
using System.Data.SqlClient;
using System.Configuration;
using GALWS.AMDNPL;

namespace STD.BAL
{
    public class OnlineTicketing
    {
        AmdBAL BAL_IA = new AmdBAL(System.Configuration.ConfigurationManager.ConnectionStrings["myAmdDB"].ConnectionString);
        List<Shared.SoapActionUrl> ListSAU = new List<STD.Shared.SoapActionUrl>();
        Hashtable HSREQRES = new Hashtable();
        Hashtable HSPNR = new Hashtable();
        string SessionIdIgnore = ""; string SignOutReqRes = ""; string[] SignInOutArray = { };
        private string GetSessionHeader(string SH)
        {
            StringBuilder XmlBuilder = new StringBuilder();
            string[] ss = Utility.Split(SH, "|");
            XmlBuilder.Append("<wbs:Session>");
            XmlBuilder.Append("<wbs:SessionId>" + ss[0].ToString() + "</wbs:SessionId>");
            XmlBuilder.Append("<wbs:SequenceNumber>" + ss[1].ToString() + "</wbs:SequenceNumber>");
            XmlBuilder.Append("<wbs:SecurityToken>" + ss[2].ToString() + "</wbs:SecurityToken>");
            XmlBuilder.Append("</wbs:Session>");
            return XmlBuilder.ToString();
        }
        #region Ticketing
        public ArrayList GDSOnlineTicketing(string SessionId, string PNR, DataSet TktCrdDS, string OrderId, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl)
        {
            ArrayList TicketArrayList = new ArrayList();
            //Variables
            string CrypticRes = ""; string RetrivePNRRes = ""; string AirlinePNR = ""; string SessionIdRPNR = ""; string SessionCryptic = "";
            string result = ""; string SessionRtv = ""; string TicketRes = ""; string sessionIgnore = ""; string SessionSignIn = ""; string SessionRtv1 = "";
            //SignIn
            try
            {
                HSREQRES.Add("ORDERID", OrderId);
                ListSAU = BAL_IA.GetSoapActionUrl("1A");
                // SessionSignIn = SignIn(ListSAU[0].SIGNIN, TktCrdDS.Tables[0].Rows[0]["SigninXML"].ToString());
                //End SignIn
                //Wait
             //   System.Threading.Thread.Sleep(6000);
                //End Wait
                //if (HSREQRES["SIGNIN_STATUS"].ToString() == "TRUE")
                //{
                //Incremented SessionId
                SessionIdRPNR = SessionId;
                //SessionIdRPNR = GetIncrementedSessionId(SessionSignIn);
                //End Incremented SessionId
                //Retrive with option code 0
                string SoapAction = "http://webservices.amadeus.com/PNRRET_17_1_1A";
                RetrivePNRRes = RetrivePNR(SessionIdRPNR, SoapAction, PNR, "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL);
                string REPRICE_RESPONSE = "";
                if (HSREQRES["RETRIVEPNR_STATUS"].ToString() == "TRUE")
                {

                    //REPricing

                    ArrayList RETST = new ArrayList();
                    RETST = RePricing_TKT(SessionIdRPNR, OrderId, RetrivePNRRes, SoapAction, "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL);
                    string SessionIdReTST = ""; string Status = "FALSE";
                    try
                    {
                        if (RETST.Count > 1)
                        {
                            Status = RETST[0].ToString();
                            SessionIdReTST = RETST[1].ToString();
                        }
                        REPRICE_RESPONSE = Convert.ToString(HSREQRES["REPNRRETRIVE_RES"]);
                    }
                    catch
                    { }

                    if (Status == "TRUE")
                    {

                        XDocument docairlinepnr = XDocument.Parse(RetrivePNRRes.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_17_1_1A\"", ""));
                        if (docairlinepnr.Descendants("PNR_Reply").Descendants("originDestinationDetails").Descendants("itineraryInfo").Descendants("itineraryReservationInfo").Any() == true)
                            AirlinePNR = docairlinepnr.Descendants("PNR_Reply").Descendants("originDestinationDetails").Descendants("itineraryInfo").Descendants("itineraryReservationInfo").Descendants("reservation").Elements("controlNumber").First().Value;
                        //End Retrive with option code 0
                        if (AirlinePNR != "")
                        {
                            //Cryptic Command
                            //SessionCryptic = GetIncrementedSessionId(SessionIdRPNR);
                            //CrypticRes = CommandCryptic(SessionCryptic, ListSAU[0].CRYPTIC, "TTP/ET");//TTP/ET
                            //SessionCryptic = GetIncrementedSessionId(SessionIdReTST);
                            AmdSession objSsnHdr = new AmdSession();
                            SessionCryptic = "";
                            SessionCryptic = objSsnHdr.GetSessionInseries(REPRICE_RESPONSE);
                            System.Threading.Thread.Sleep(6000);
                            CrypticRes = DocIssuance(SessionCryptic, ListSAU[0].DOCISSUANCE, "ET", "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL); //CommandCryptic(SessionCryptic, ListSAU[0].CRYPTIC, "TTP/ET");//TTP/ET
                                                                                                                                                                                            //Dim tckt As New System.Xml.
                            if (HSREQRES["CRYPTIC_STATUS"].ToString() == "TRUE")
                            {
                                //XmlDocument tckt = new XmlDocument();
                                //StringWriter sw = new StringWriter();
                                //XmlTextWriter xw = new XmlTextWriter(sw);
                                //tckt.LoadXml(CrypticRes);
                                //tckt.WriteTo(xw);
                                //result = sw.ToString();
                                //string[] seg_info = STD.BAL.Utility.Split(result, "<Response>");
                                if (CrypticRes.Contains("OK ETICKET") == true)
                                {
                                    //  SessionRtv = GetIncrementedSessionId(SessionCryptic);

                                    SessionRtv = objSsnHdr.GetSessionInseries(CrypticRes);
                                    TicketRes = TicketNoRetrivePNR(SessionRtv, ListSAU[0].ADDPAX, PNR, "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL);
                                    if (HSREQRES["RETRIVEPNRTKT_STATUS"].ToString() == "TRUE")
                                    {

                                        string[] tkt_seg = STD.BAL.Utility.Split(TicketRes, "<segmentName>FA</segmentName>");
                                        if (tkt_seg.Length - 1 > 0)
                                        {
                                            TicketArrayList = TktNoWithPax(TicketRes);
                                        }
                                        else
                                        {

                                            for (int i = 1; i <= 2; i++)
                                            {
                                                System.Threading.Thread.Sleep(6000);
                                                //SessionRtv1 = GetIncrementedSessionId(SessionRtv);

                                                SessionRtv1 = objSsnHdr.GetSessionInseries(TicketRes);
                                                string TicketRes1 = TicketNoRetrivePNR(SessionRtv1, ListSAU[0].ADDPAX, PNR, "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL);
                                                if (HSREQRES["RETRIVEPNRTKT_STATUS"].ToString() == "TRUE")
                                                {
                                                    string[] tkt_seg1 = STD.BAL.Utility.Split(TicketRes1, "<segmentName>FA</segmentName>");
                                                    if (tkt_seg1.Length > 0)
                                                    {
                                                        TicketArrayList = TktNoWithPax(TicketRes);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }


                                }
                                if (TicketArrayList.Count > 0)
                                {
                                    if (SessionRtv1 == "") { SignInOutArray = SignoutLast(SessionRtv); }
                                    else { SignInOutArray = SignoutLast(SessionRtv); }
                                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"]==null?"": HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"]==null?"": HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", HSREQRES["RETRIVEPNRTKT_REQ"].ToString(), HSREQRES["RETRIVEPNRTKT_RES"].ToString(), HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), HSREQRES["REPNRRETRIVE_REQ"].ToString(), HSREQRES["REPNRRETRIVE_RES"].ToString());
                                }
                                else
                                {


                                    TicketArrayList.Add("AirLine is not able to make ETicket,Please Issue Ticket offline");
                                    SignInOutArray = Signoutandignore(SessionCryptic);
                                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "AirLine is not able to make ETicket,Please Issue Ticket offline", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), HSREQRES["REPNRRETRIVE_REQ"].ToString(), HSREQRES["REPNRRETRIVE_RES"].ToString());
                                    //sessionIgnore = GetIncrementedSessionId(SessionCryptic);
                                    //PNR_AddMultiElements20(sessionIgnore, ListSAU[0].IGNORE);
                                    //SignOut(ListSAU[0].SIGNOUT, sessionIgnore);
                                }
                                //End Cryptic Command
                            }
                            else
                            {
                                TicketArrayList.Add("Airline Cryptic Command Problem,Please Issue Ticket offline");
                            }

                        }
                        else
                        {
                            TicketArrayList.Add("Airline not able to fetch airline PNR,Please Issue Ticket offline");
                            SignInOutArray = Signoutandignore(sessionIgnore);
                            BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "Not able to fetch airline PNR,Please Issue Ticket offline", "", "", "", "", "", "", "", "");
                            //sessionIgnore = GetIncrementedSessionId(SessionIdRPNR);
                            //PNR_AddMultiElements20(sessionIgnore, ListSAU[0].IGNORE);
                            //SignOut(ListSAU[0].SIGNOUT, sessionIgnore);
                        }
                    }

                    else
                    {
                        TicketArrayList.Add("Airline not able to Repricing,Please Issue Ticket offline");
                    }//SomeText




                }
                else
                {
                    TicketArrayList.Add("Airline not able to retrive,Please Issue Ticket offline");
                }


                //}
                //else
                //{
                //    TicketArrayList.Add("Airline not able to signin,Please Issue Ticket offline");
                //}
            }
            catch (Exception ex)
            {
                TicketArrayList.Add("Airline error");
                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), "", "", "", "", "", "", "", "", ex.ToString(), "", "", "", "", "", "", "", "");
            }
            return TicketArrayList;
        }
        #endregion Ticketing
        #region SignIn
        public string SignIn(string SoapAction, string SigninXML)
        {
            string session = "";
            try
            {
                //List<Shared.TicketingCrd> ListTC = new List<STD.Shared.TicketingCrd>();
                //ListTC = BAL_IA.GetTicketingCrd(VC);
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append(SigninXML);
                string strsessionid = "";
                HSREQRES.Add("SIGNIN_REQ", XmlBuilder.ToString());
                strsessionid = PostXml(XmlBuilder.ToString(), SoapAction);
                XNamespace ns = "http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd";// "http://webservices.amadeus.com/definitions";
                XNamespace soap = "http://schemas.xmlsoap.org/soap/envelope/";
                XDocument xdoc = XDocument.Parse(strsessionid);
                session = xdoc.Descendants(ns + "SessionId").First().Value + "|" + xdoc.Descendants(ns + "SequenceNumber").First().Value + "|" + xdoc.Descendants(ns + "SecurityToken").First().Value;//(xdoc.Descendants(soap + "Header")).Descendants(ns + "Session").Descendants("SessionId").First().Value;
                HSREQRES.Add("SIGNIN_RES", strsessionid);
                if (strsessionid == "")
                {
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("SIGNIN_STATUS", "FALSE");
                }
                else if (strsessionid.Contains("<errorText>") == true)
                {
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("SIGNIN_STATUS", "FALSE");
                }
                else if (strsessionid.Substring(0, 5) == "Error")
                {
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("SIGNIN_STATUS", "FALSE");
                }
                else
                {

                    HSREQRES.Add("SIGNIN_STATUS", "TRUE");
                }

            }
            catch (Exception ex)
            {
                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), "", "", "", "", "", "", "", "", ex.ToString(), "", "", "", "", "", "", "", "");
                HSREQRES.Add("SIGNIN_STATUS", "FALSE");

            }

            return session;
        }
        #endregion End SignIn
        #region Retrive with option code 0
        public string RetrivePNR(string SessionId, string SoapAction, string PNR, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl)
        {
            string admultielement = "";
            AmdSession objSessuonHeader = new AmdSession();
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:itar='http://xml.amadeus.com/ITAREQ_05_2_IA'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(SessionId, username, officeID, pass, SoapAction, svcUrl, includeSessionType));
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");
                XmlBuilder.Append("<PNR_Retrieve>");
                XmlBuilder.Append("<retrievalFacts>");
                XmlBuilder.Append("<retrieve>");
                XmlBuilder.Append("<type>2</type>");
                XmlBuilder.Append("</retrieve>");
                XmlBuilder.Append("<reservationOrProfileIdentifier>");
                XmlBuilder.Append("<reservation>");
                XmlBuilder.Append("<controlNumber>" + PNR + "</controlNumber>");
                XmlBuilder.Append("</reservation>");
                XmlBuilder.Append("</reservationOrProfileIdentifier>");
                XmlBuilder.Append("</retrievalFacts>");
                XmlBuilder.Append("</PNR_Retrieve>");
                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                HSREQRES.Add("RETRIVEPNR_REQ", XmlBuilder.ToString());
                admultielement = PostXml(XmlBuilder.ToString(), SoapAction);
                HSREQRES.Add("RETRIVEPNR_RES", admultielement);
                if (admultielement == "")
                {
                    SignInOutArray = Signoutandignore(SessionId);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), "", "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("RETRIVEPNR_STATUS", "FALSE");

                }
                else if (admultielement.Contains("<errorText>") == true)
                {

                    SignInOutArray = Signoutandignore(SessionId);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("RETRIVEPNR_STATUS", "FALSE");
                }
                else if (admultielement.Substring(0, 5) == "Error")
                {
                    SignInOutArray = Signoutandignore(SessionId);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");
                    HSREQRES.Add("RETRIVEPNR_STATUS", "FALSE");
                }
                else
                {

                    HSREQRES.Add("RETRIVEPNR_STATUS", "TRUE");
                }


            }
            catch (Exception ex)
            {
                SignInOutArray = Signoutandignore(SessionId);
                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), "", "", "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), ex.ToString(), "", "", "", "", "", "", "", "");
                HSREQRES.Add("RETRIVEPNR_STATUS", "FALSE");
            }
            return admultielement;
        }
        #endregion Retrive with option code 0
        #region Command Cryptic
        public string CommandCryptic(string sessionid, string SoapAction, string command)
        {
            string CrypticRes = "";
            StringBuilder XmlBuilder = new StringBuilder();
            try
            {
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");
                //XmlBuilder.Append("<Cryptic_GetScreen_Query><Command>RT" + PNR + "</Command></Cryptic_GetScreen_Query>");
                XmlBuilder.Append("<Command_Cryptic>");
                XmlBuilder.Append("<messageAction>");
                XmlBuilder.Append("<messageFunctionDetails>");
                XmlBuilder.Append("<messageFunction>M</messageFunction>");
                XmlBuilder.Append("</messageFunctionDetails>");
                XmlBuilder.Append("</messageAction>");
                XmlBuilder.Append("<longTextString>");
                XmlBuilder.Append("<textStringDetails>" + command + "</textStringDetails>");
                XmlBuilder.Append("</longTextString>");
                XmlBuilder.Append("</Command_Cryptic>");
                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                HSREQRES.Add("CRYPTIC_REQ", XmlBuilder.ToString());
                CrypticRes = PostXml(XmlBuilder.ToString(), SoapAction);
                HSREQRES.Add("CRYPTIC_RES", CrypticRes);
                if (CrypticRes == "")
                {
                    HSREQRES.Add("CRYPTIC_STATUS", "FALSE");
                    SignInOutArray = Signoutandignore(sessionid);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");


                }
                else if (CrypticRes.Contains("<errorText>") == true)
                {
                    HSREQRES.Add("CRYPTIC_STATUS", "FALSE");
                    SignInOutArray = Signoutandignore(sessionid);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");

                }
                else if (CrypticRes.Substring(0, 5) == "Error")
                {
                    HSREQRES.Add("CRYPTIC_STATUS", "FALSE");
                    SignInOutArray = Signoutandignore(sessionid);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");

                }
                else
                {

                    HSREQRES.Add("CRYPTIC_STATUS", "TRUE");

                }
            }
            catch (Exception ex)
            {
                HSREQRES.Add("CRYPTIC_STATUS", "TRUE");
                SignInOutArray = Signoutandignore(sessionid);
                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), ex.ToString(), "", "", "", "", "", "", "", "");

            }


            return CrypticRes;
        }
        #endregion Command Cryptic
        #region DocIssuance_TKT
        public string DocIssuance(string sessionid, string SoapAction, string command, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl)
        {
            string CrypticRes = "";
            SoapAction = "http://webservices.amadeus.com/TTKTIQ_15_1_1A";
            AmdSession objSessuonHeader = new AmdSession();
            int i = 0;
            againTKT:
            StringBuilder XmlBuilder = new StringBuilder();
            try
            {
                if (i <= 2)
                {
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:itar='http://xml.amadeus.com/TTKTIQ_15_1_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");

                XmlBuilder.Append(" <DocIssuance_IssueTicket>");
                XmlBuilder.Append(" <otherCompoundOptions>");
                XmlBuilder.Append(" <attributeDetails>");
                XmlBuilder.Append(" <attributeType>ET</attributeType>");
                XmlBuilder.Append(" </attributeDetails>");
                XmlBuilder.Append(" </otherCompoundOptions>");
                XmlBuilder.Append(" </DocIssuance_IssueTicket>");

                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");

               

                if (HSREQRES.ContainsKey("CRYPTIC_REQ"))
                {
                    HSREQRES["CRYPTIC_REQ"] = HSREQRES["CRYPTIC_REQ"] + XmlBuilder.ToString();
                }
                else
                {
                    HSREQRES.Add("CRYPTIC_REQ", XmlBuilder.ToString());
                }
                    CrypticRes = PostXml(XmlBuilder.ToString(), SoapAction);
                  
                }

                if (HSREQRES.ContainsKey("CRYPTIC_RES"))
                {
                    HSREQRES["CRYPTIC_RES"] = HSREQRES["CRYPTIC_RES"] + CrypticRes;
                }
                else
                {
                    HSREQRES.Add("CRYPTIC_RES", CrypticRes);
                }

                if (CrypticRes == "")
                {
                  //  HSREQRES.Add("CRYPTIC_STATUS", "FALSE");

                    if (HSREQRES.ContainsKey("CRYPTIC_STATUS"))
                    {
                        HSREQRES["CRYPTIC_STATUS"] = HSREQRES["CRYPTIC_STATUS"] + "FALSE";
                    }
                    else
                    {
                        HSREQRES.Add("CRYPTIC_STATUS", "FALSE");
                    }
                    SignInOutArray = Signoutandignore(sessionid);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");
                }
                else if (CrypticRes.Contains("<errorText>") == true)
                {
                    if (HSREQRES.ContainsKey("CRYPTIC_STATUS"))
                    {
                        HSREQRES["CRYPTIC_STATUS"] = HSREQRES["CRYPTIC_STATUS"] + "FALSE";
                    }
                    else
                    {
                        HSREQRES.Add("CRYPTIC_STATUS", "FALSE");
                    }
                    SignInOutArray = Signoutandignore(sessionid);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");

                }
                else if (CrypticRes.Substring(0, 5) == "Error")
                {
                    if (HSREQRES.ContainsKey("CRYPTIC_STATUS"))
                    {
                        HSREQRES["CRYPTIC_STATUS"] = HSREQRES["CRYPTIC_STATUS"] + "FALSE";
                    }
                    else
                    {
                        HSREQRES.Add("CRYPTIC_STATUS", "FALSE");
                    }
                    SignInOutArray = Signoutandignore(sessionid);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", "", "", "", "", "", "");

                }
                else if (CrypticRes.Contains("SIMULTANEOUS CHANGES") == true && i<=2)
                {
                    AmdSession objSsnHdr = new AmdSession();
                    sessionid = "";
                    sessionid = objSsnHdr.GetSessionInseries(CrypticRes);
                     string AddMEleXML = PNR_AddMultiElements21(sessionid, ListSAU[0].GDSPNR, "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL);
                    sessionid = "";
                    sessionid = objSsnHdr.GetSessionInseries(AddMEleXML);
                    i++;
                    goto againTKT;
                }
                else
                {
                    if (HSREQRES.ContainsKey("CRYPTIC_STATUS"))
                    {
                        HSREQRES["CRYPTIC_STATUS"] = HSREQRES["CRYPTIC_STATUS"] + "TRUE";
                    }
                    else
                    {
                        HSREQRES.Add("CRYPTIC_STATUS", "TRUE");
                    }
                   // HSREQRES.Add("CRYPTIC_STATUS", "TRUE");

                }
            }
            catch (Exception ex)
            {
                HSREQRES.Add("CRYPTIC_STATUS", "TRUE");
                SignInOutArray = Signoutandignore(sessionid);
                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), ex.ToString(), "", "", "", "", "", "", "", "");

            }


            return CrypticRes;
        }
        #endregion DocIssuance_TKT
        #region Ignore
        public void PNR_AddMultiElements20(string sessionid, string SoapAction)
        {
            string ResAME20 = "";
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                //XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:def='http://webservices.amadeus.com/definitions' xmlns:fmp='http://xml.amadeus.com/FMPTBQ_08_2_1A'>");
                //XmlBuilder.Append("<soapenv:Header>");
                //XmlBuilder.Append("<def:SessionId>" + sessionid + "</def:SessionId>");
                //XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
                XmlBuilder.Append("<soap:Header xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
                XmlBuilder.Append(GetSessionHeader(sessionid));//XmlBuilder.Append("<SessionId>" + SessionId + "</SessionId>");
                XmlBuilder.Append("</soap:Header>");
                XmlBuilder.Append("<soapenv:Body>");

                XmlBuilder.Append("<PNR_AddMultiElements>");
                XmlBuilder.Append("<pnrActions>");
                XmlBuilder.Append("<optionCode>20</optionCode>");
                XmlBuilder.Append("</pnrActions>");
                XmlBuilder.Append("</PNR_AddMultiElements>");
                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");
                ResAME20 = PostXml(XmlBuilder.ToString(), SoapAction);
            }
            catch (Exception ex)
            {

                ResAME20 = "";
            }
        }
        #endregion Ignore
        public string PNR_AddMultiElements21(string sessionid, string SoapAction, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl)
        {
            string ResAME11 = "";
            AmdSession objSessuonHeader = new AmdSession();
            SoapAction = "http://webservices.amadeus.com/PNRADD_17_1_1A";
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_11_1_1A'>");
                XmlBuilder.Append("<soapenv:Header>");
                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(sessionid, username, officeID, pass, SoapAction, svcUrl, includeSessionType));
                XmlBuilder.Append("</soapenv:Header>");
                XmlBuilder.Append("<soapenv:Body>");

                XmlBuilder.Append("<PNR_AddMultiElements>");
                XmlBuilder.Append("<pnrActions>");
                XmlBuilder.Append("<optionCode>21</optionCode>");
                XmlBuilder.Append("</pnrActions>");
                XmlBuilder.Append("</PNR_AddMultiElements>");

                XmlBuilder.Append("</soapenv:Body>");
                XmlBuilder.Append("</soapenv:Envelope>");

                if (HSREQRES.ContainsKey("PNR_21_REQ"))
                {
                    HSREQRES["PNR_21_REQ"] = HSREQRES["PNR_21_REQ"] + XmlBuilder.ToString();
                }
                else
                {
                    HSREQRES.Add("PNR_21_REQ", XmlBuilder.ToString());
                }
                ResAME11 = PostXml(XmlBuilder.ToString(), SoapAction);                
                if (HSREQRES.ContainsKey("PNR_21_REQ"))
                {
                    HSREQRES["PNR_21_RES"] = HSREQRES["PNR_21_RES"] + ResAME11;
                }
                else
                {
                    HSREQRES.Add("PNR_21_RES", ResAME11);
                }
            }
            catch (Exception ex)
            {
                if (HSREQRES.ContainsKey("PNR_21_REQ"))
                {
                    HSREQRES["PNR_21_RES"] = HSREQRES["PNR_21_RES"] + ex.Message;
                }
                else
                {
                    HSREQRES.Add("PNR_21_RES", ex.Message);
                }
            }
            return ResAME11;


        }
        #region SignOut
        public string SignOut(string SoapAction, string sessionid)
        {
            SoapAction = "http://webservices.amadeus.com/VLSSOQ_04_1_1A";
            StringBuilder SOUT = new StringBuilder();
            string session = "";
            try
            {

                SOUT.Append("<?xml version='1.0' encoding='utf-8'?>");
                //SOUT.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' >");
                //SOUT.Append("<soap:Header xmlns='http://webservices.amadeus.com/definitions'>");
                //SOUT.Append("<SessionId>" + sessionid + "</SessionId>");
                //SOUT.Append("</soap:Header>");
                SOUT.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:vls='http://xml.amadeus.com/VLSSOQ_04_1_1A'>");
                SOUT.Append("<soapenv:Header>");
                SOUT.Append(GetSessionHeader(sessionid));//XmlBuilder.Append("<SessionId>" + SessionId + "</SessionId>");
                SOUT.Append("</soapenv:Header>");
                SOUT.Append("<soapenv:Body>");
                SOUT.Append("<Security_SignOut/>");
                SOUT.Append("</soapenv:Body>");
                SOUT.Append("</soapenv:Envelope>");
                //session = PostXml(SOUT.ToString(), SoapAction);// "http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A"    

                string strRes = PostXml(SOUT.ToString(), SoapAction);// "http://webservices.amadeus.com/1ASIWMASSPT/VLSSOQ_04_1_1A"    

                HSREQRES.Add("SIGNIN_REQ", SOUT.ToString());
                HSREQRES.Add("SIGNIN_RES", strRes);
            }
            catch (Exception ex)
            {
                session = "";

            }

            return SOUT.ToString() + "SPLITSIGNOUTSTRING" + session;
        }
        #endregion End SignOut
        #region PostXML
        public static string PostXml(String XMLIn, String Module_Version)
        {
            string rawResponse = string.Empty;
            try
            {
                byte[] bytes = Encoding.UTF8.GetBytes(XMLIn);
                // HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://production.webservices.amadeus.com");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://nodeD1.test.webservices.amadeus.com/1ASIWPREP4A");
                request.Headers.Add(String.Format("SOAPAction: \"{0}\"", Module_Version));
                request.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                request.Method = "POST";
                request.ContentLength = bytes.Length;
                request.ContentType = @"text/xml;charset=utf-8";
                request.Credentials = CredentialCache.DefaultCredentials;
                request.ProtocolVersion = HttpVersion.Version11;
                //request.Timeout = 3000000;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(bytes, 0, bytes.Length);
                }
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", response.StatusCode);
                        throw new ApplicationException(message);
                    }
                    rawResponse = GetWebResponseString(response);
                    return rawResponse;
                }
            }
            catch (WebException wex)
            {
                //rawResponse = "Error: " + wex;
                // We issued an HttpWebRequest, so the response will be an HttpWebResponse.
                HttpWebResponse httpResponse = wex.Response as HttpWebResponse;

                // Grab the response stream.           
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        // Grab the body of the response as a string for parsing.
                        rawResponse = GetWebResponseString(httpResponse);
                        //IBE.Shared.LogUtility.LogInfo(rawResponse, "SoapFaults", IBE.Shared.Utilities.GetCurrentTimestamp());[commented HS 11-10-2011]
                        //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, wex, "698", rawResponse, string.Empty, "");
                        //  throw wex;
                    }
                }
            }

            catch (Exception ex)
            {
                //ExceptionLogger objExceptionLogger = new ExceptionLogger(ExceptionLogger.ERROR_SOURCE.Services, ex, "699", rawResponse, string.Empty, "");
                rawResponse = "Error: " + ex;
            }
            return rawResponse;
        }
        #endregion PostXML
        #region GetWebResponseString
        private static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            Stream streamResponse;
            using (streamResponse = myHttpWebResponse.GetResponseStream())
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                using (StreamReader streamRead = new StreamReader(streamResponse))
                {
                    Char[] readBuffer = new Char[256];
                    int count = streamRead.Read(readBuffer, 0, 256);

                    while (count > 0)
                    {
                        String resultData = new String(readBuffer, 0, count);
                        rawResponse.Append(resultData);
                        count = streamRead.Read(readBuffer, 0, 256);
                    }
                }
            }
            return rawResponse.ToString();
        }
        #endregion GetWebResponseString
        #region GetIncrementedSessionId
        private string GetIncrementedSessionId(string SId)
        {
            string[] SID = SId.Split('|');
            string SessionId = SID[0] + "|" + Convert.ToInt32(Convert.ToInt32(SID[1]) + 1) + "|" + SID[2];
            return SessionId;

        }
        #endregion GetIncrementedSessionId
        #region GetTicketNo
        private ArrayList TktNoWithPax(string pnrreply)
        {
            ArrayList PAXLIST = new ArrayList();
            ArrayList TktNoArray = new ArrayList();
            var TKCKETLIST = new List<TICKETDTL>();
            try
            {
                pnrreply = pnrreply.Replace("xmlns=\"http://xml.amadeus.com/PNRACC_17_1_1A\"", "");
                XDocument doc = XDocument.Parse(pnrreply);
                var PAX = from IMP in doc.Descendants("travellerInfo")//.Descendants("passengerData").Descendants("travellerInformation")
                          select new
                          {
                              Traveller = IMP,
                          };
                var TKTNO = from IMP in doc.Descendants("dataElementsMaster").Descendants("dataElementsIndiv")
                            where (IMP.Elements("elementManagementData").Elements("segmentName").First().Value == "FA")
                            select new
                            {
                                TicketNo = IMP
                            };

                foreach (var tk in TKTNO)
                {

                    var PTNO = tk.TicketNo.Descendants("referenceForDataElement").Descendants("reference")//.Elements("number").First().Value;
                               .Where(m => m.Elements("qualifier").First().Value == "PT")
                                              .Select(m => m.Elements("number").First().Value);

                    string[] TKTDETAILS = tk.TicketNo.Descendants("otherDataFreetext").Elements("longFreetext").First().Value.Split(' ');
                    string[] TicketNo = TKTDETAILS[1].Split('/');
                    TKCKETLIST.Add(
                         new TICKETDTL
                         {
                             PaxType = TKTDETAILS[0].Trim(),
                             TKTNo = TicketNo[0].Trim(),
                             PTNo = PTNO.First(),

                         });

                }
                // int i = 0;
                foreach (var abc in PAX)
                {
                    string SirName = (abc.Traveller.Descendants("passengerData").Descendants("travellerInformation").Elements("traveller").Elements("surname").First().Value);
                    string PtNo = abc.Traveller.Descendants("elementManagementPassenger").Descendants("reference").Elements("number").First().Value;
                    foreach (var px in abc.Traveller.Descendants("passenger"))
                    {
                        // string[] FN = px.Element("firstName").Value.ToString().Split(' ');
                        string FirstName = "";
                        //if (FN.Length > 1)
                        //{
                        //    FirstName = FN[0].ToString().Trim() + FN[1].ToString().Trim();
                        //}
                        //else
                        //{
                        //    FirstName = FN[0].ToString();
                        //}
                        FirstName = px.Element("firstName").Value.ToString().Trim().Replace(" ", "");
                        string typ = px.Element("type").Value.ToString().Trim();
                        if (typ != "INF")
                            typ = "PAX";
                        var tktno = (from tk in TKCKETLIST where tk.PTNo == PtNo && tk.PaxType == typ select tk).ToList();
                        TktNoArray.Add(FirstName + SirName + "/" + tktno[0].TKTNo);
                        //if (TKCKETLIST[i].PTNo.ToString().ToUpper() == PtNo && TKCKETLIST[i].PaxType.ToString().ToUpper() == "INF")
                        //{
                        //    TktNoArray.Add(FirstName + SirName + "/" + TKCKETLIST[i].TKTNo);
                        //}
                        //else
                        //{
                        //    if (TKCKETLIST[i].PTNo.ToString().ToUpper() == PtNo)
                        //    {
                        //        TktNoArray.Add(FirstName + SirName + "/" + TKCKETLIST[i].TKTNo);
                        //    }
                        //}
                        //i = i + 1;

                    }
                }
            }
            catch (Exception ex)
            { }
            return TktNoArray;
        }
        #endregion GetTicketNo
        #region Retrive For Ticket No with option code 0
        public string TicketNoRetrivePNR(string SessionId, string SoapAction, string PNR, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl)
        {
            SoapAction = "http://webservices.amadeus.com/PNRADD_17_1_1A";
            AmdSession objSessuonHeader = new AmdSession();
            string tktrtvpnr = "";
            try
            {
                StringBuilder XmlBuilder = new StringBuilder();
                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                ////XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema'>");
                //XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
                //XmlBuilder.Append("<soap:Header xmlns='http://webservices.amadeus.com/definitions'>");
                //XmlBuilder.Append("<SessionId>" + SessionId + "</SessionId>");
                //XmlBuilder.Append("</soap:Header>");
                XmlBuilder.Append("<soap:Envelope xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>");
                XmlBuilder.Append("<soap:Header xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_17_1_1A'>");
                // XmlBuilder.Append(GetSessionHeader(SessionId));
                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(SessionId, username, officeID, pass, SoapAction, svcUrl, includeSessionType));
                XmlBuilder.Append("</soap:Header>");
                XmlBuilder.Append("<soap:Body>");
                XmlBuilder.Append("<PNR_AddMultiElements>");
                XmlBuilder.Append("<reservationInfo>");
                XmlBuilder.Append("<reservation>");
                XmlBuilder.Append("<controlNumber>" + PNR + "</controlNumber>");
                XmlBuilder.Append("</reservation>");
                XmlBuilder.Append("</reservationInfo>");
                XmlBuilder.Append("<pnrActions>");
                XmlBuilder.Append("<optionCode>0</optionCode>");
                XmlBuilder.Append(" </pnrActions>");
                XmlBuilder.Append("</PNR_AddMultiElements>");
                XmlBuilder.Append("</soap:Body>");
                XmlBuilder.Append("</soap:Envelope>");
                if (HSREQRES.ContainsKey("RETRIVEPNRTKT_REQ") == false)
                {
                    HSREQRES.Add("RETRIVEPNRTKT_REQ", XmlBuilder.ToString());
                }
                else
                {
                    HSREQRES["RETRIVEPNRTKT_REQ"] = XmlBuilder.ToString();
                }

                tktrtvpnr = PostXml(XmlBuilder.ToString(), SoapAction);
                if (HSREQRES.ContainsKey("RETRIVEPNRTKT_RES") == false)
                {
                    HSREQRES.Add("RETRIVEPNRTKT_RES", tktrtvpnr);
                }
                else
                {
                    HSREQRES["RETRIVEPNRTKT_RES"] = tktrtvpnr;
                }
                if (tktrtvpnr == "")
                {
                    SignInOutArray = Signoutandignore(SessionId);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", HSREQRES["RETRIVEPNRTKT_REQ"].ToString(), "", "", "", "", "", "", "");
                    if (HSREQRES.ContainsKey("RETRIVEPNRTKT_STATUS") == false)
                    {
                        HSREQRES.Add("RETRIVEPNRTKT_STATUS", "FALSE");
                    }
                    else
                    {
                        HSREQRES["RETRIVEPNRTKT_STATUS"] = "FALSE";
                    }

                }
                else if (tktrtvpnr.Contains("<errorText>") == true)
                {

                    SignInOutArray = Signoutandignore(SessionId);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", HSREQRES["RETRIVEPNRTKT_REQ"].ToString(), HSREQRES["RETRIVEPNRTKT_RES"].ToString(), "", "", "", "", "", "");
                    if (HSREQRES.ContainsKey("RETRIVEPNRTKT_STATUS") == false)
                    {
                        HSREQRES.Add("RETRIVEPNRTKT_STATUS", "FALSE");
                    }
                    else
                    {
                        HSREQRES["RETRIVEPNRTKT_STATUS"] = "FALSE";
                    }
                }
                else if (tktrtvpnr.Substring(0, 5) == "Error")
                {
                    SignInOutArray = Signoutandignore(SessionId);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", HSREQRES["RETRIVEPNRTKT_REQ"].ToString(), HSREQRES["RETRIVEPNRTKT_RES"].ToString(), "", "", "", "", "", "");
                    if (HSREQRES.ContainsKey("RETRIVEPNRTKT_STATUS") == false)
                    {
                        HSREQRES.Add("RETRIVEPNRTKT_STATUS", "FALSE");
                    }
                    else
                    {
                        HSREQRES["RETRIVEPNRTKT_STATUS"] = "FALSE";
                    }
                }
                else
                {

                    if (HSREQRES.ContainsKey("RETRIVEPNRTKT_STATUS") == false)
                    {
                        HSREQRES.Add("RETRIVEPNRTKT_STATUS", "TRUE");
                    }
                    else
                    {
                        HSREQRES["RETRIVEPNRTKT_STATUS"] = "TRUE";
                    }
                }


            }
            catch (Exception ex)
            {
                SignInOutArray = Signoutandignore(SessionId);
                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), ex.ToString(), "", "", "", "", "", "", "", "");
                if (HSREQRES.ContainsKey("RETRIVEPNRTKT_STATUS") == false)
                {
                    HSREQRES.Add("RETRIVEPNRTKT_STATUS", "FALSE");
                }
                else
                {
                    HSREQRES["RETRIVEPNRTKT_STATUS"] = "FALSE";
                }
            }
            return tktrtvpnr;
        }
        #endregion Retrive For Ticket No with option code 0
        public string[] Signoutandignore(string SessionId)
        {
            string[] SignOutArray = { };
            try
            {
                SessionIdIgnore = GetIncrementedSessionId(SessionId);
                PNR_AddMultiElements20(SessionIdIgnore, ListSAU[0].IGNORE);
                SignOutReqRes = SignOut(ListSAU[0].SIGNOUT, SessionIdIgnore);
                SignOutArray = STD.BAL.Utility.Split(SignOutReqRes, "SPLITSIGNOUTSTRING");
            }
            catch (Exception ex) { }
            // SignOutArray = STD.BAL.Utility.Split(SignOutReqRes, "SPLITSIGNOUTSTRING");
            return SignOutArray;
        }
        public string[] SignoutLast(string SessionId)
        {
            string[] SignOutArray = { };
            try
            {
                //SessionIdIgnore = GetIncrementedSessionId(SessionId);
                //PNR_AddMultiElements20(SessionId, ListSAU[0].IGNORE);
                SignOutReqRes = SignOut(ListSAU[0].SIGNOUT, SessionId);
                SignOutArray = STD.BAL.Utility.Split(SignOutReqRes, "SPLITSIGNOUTSTRING");
            }
            catch (Exception ex) { }
            // SignOutArray = STD.BAL.Utility.Split(SignOutReqRes, "SPLITSIGNOUTSTRING");
            return SignOutArray;
        }

        private ArrayList RePricing_TKT(string sessionid, string OrderId, string AddMulRes, string SoapAction, string username, string officeID, string pass, IncludeSessionType includeSessionType, string svcUrl)
        {
            SoapAction = "http://webservices.amadeus.com/TPCBRQ_18_1_1A";
            ArrayList TSTArray = new ArrayList();
            string Status = ""; string PriceRes = ""; string ReqTST = ""; string sessionidTST = "";
            DataSet ds = new DataSet(); double farediff = 0; string ResTST = "";
            string SessionIdRet = ""; string SessionIdPrice = "";
            ds = BAL_IA.GetGDSXmlLogsAndFlight(OrderId);
            try
            {
                AmdSession objSessuonHeader = new AmdSession();
                SessionIdRet = objSessuonHeader.GetSessionInseries(AddMulRes);
                SessionIdPrice = SessionIdRet;
                try
                {
                    string[] PRICEREQARRAY = Utility.Split(ds.Tables[0].Rows[0]["PRICE_REQ"].ToString(), "<soapenv:Body>");
                    StringBuilder PriceBuilder = new StringBuilder();
                    PriceBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                    //PriceBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:tpc='http://xml.amadeus.com/TPCBRQ_07_3_1A'>");
                    //PriceBuilder.Append("<soapenv:Header>");
                    //PriceBuilder.Append(GetSessionHeader(SessionIdRet));//PriceBuilder.Append(" <def:SessionId>" + SessionIdRet + "</def:SessionId>");
                    //PriceBuilder.Append("</soapenv:Header>");
                    //PriceBuilder.Append("<soapenv:Body>");
                    PriceBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:tpc='http://xml.amadeus.com/TPCBRQ_07_3_1A'>");
                    PriceBuilder.Append("<soapenv:Header>");
                    PriceBuilder.Append(objSessuonHeader.GetSessionHeader(SessionIdRet, username, officeID, pass, SoapAction, svcUrl, includeSessionType));
                    PriceBuilder.Append("</soapenv:Header>");
                    PriceBuilder.Append("<soapenv:Body>");
                    PriceBuilder.Append(PRICEREQARRAY[1].ToString());
                    HSREQRES.Add("REPRICE_REQ", PriceBuilder.ToString());
                    PriceRes = PostXml(PriceBuilder.ToString(), SoapAction);
                    HSREQRES.Add("REPRICE_RES", PriceRes);
                    if (PriceRes == "")
                    {
                        HSREQRES.Add("REPRICE_STATUS", "FALSE");
                        SignInOutArray = Signoutandignore(SessionIdRet);
                        BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), "", "", "", "", "");


                    }
                    else if (PriceRes.Contains("<errorText>") == true)
                    {
                        HSREQRES.Add("REPRICE_STATUS", "FALSE");
                        SignInOutArray = Signoutandignore(SessionIdRet);
                        BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), "", "", "", "");

                    }
                    else if (PriceRes.Substring(0, 5) == "Error")
                    {
                        HSREQRES.Add("REPRICE_STATUS", "FALSE");
                        SignInOutArray = Signoutandignore(SessionIdRet);
                        BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), "", "", "", "");

                    }
                    else
                    {

                        HSREQRES.Add("REPRICE_STATUS", "TRUE");

                    }
                }
                catch (Exception ex)
                {
                    HSREQRES.Add("REPRICE_STATUS", "FALSE");
                    SignInOutArray = Signoutandignore(SessionIdRet);
                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), HSREQRES["CRYPTIC_REQ"].ToString(), HSREQRES["CRYPTIC_RES"].ToString(), SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), ex.ToString(), "", "", "", "", "", "", "", "");
                };

                if (HSREQRES["REPRICE_STATUS"].ToString() == "TRUE")
                {
                    XDocument PXDoc = XDocument.Parse(PriceRes);
                    XNamespace Pns = "http://xml.amadeus.com/TPCBRR_18_1_1A";
                    var OTF = PXDoc.Descendants(Pns + "fareList").Descendants(Pns + "fareDataInformation").Descendants(Pns + "fareDataSupInformation")
                                          .Where(m => m.Elements(Pns + "fareDataQualifier").First().Value == "712")
                                          .Select(m => m.Elements(Pns + "fareAmount").First().Value);
                    double OriginalTF = 0;
                    foreach (var fare in OTF)
                    {
                        OriginalTF = OriginalTF + Convert.ToDouble(fare.ToString());

                    }
                    //changes by abhilash
                    //SMS charge calc
                    double SMSChg = 0;
                    try
                    {
                        //SMSChg = double.Parse(FltDsGAL.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDsGAL.Tables[0].Rows[0]["Adult"].ToString().Trim());
                        //SMSChg = SMSChg + (double.Parse(FltDsGAL.Tables[0].Rows[0]["OriginalTT"].ToString().Trim()) * double.Parse(FltDsGAL.Tables[0].Rows[0]["Child"].ToString().Trim()));
                        SMSChg = double.Parse(ds.Tables[1].Rows[0]["OriginalTT"].ToString().Trim());
                    }
                    catch
                    { }

                    OriginalTF = OriginalTF + SMSChg;
                    if (Convert.ToInt32(ds.Tables[1].Rows[0]["Child"].ToString()) > 0)
                    { OriginalTF = OriginalTF + SMSChg; }

                    //SMS charge calc end

                    double TotFare = double.Parse(ds.Tables[1].Rows[0]["AdtFare"].ToString().Trim());
                    TotFare += double.Parse(ds.Tables[1].Rows[0]["ChdFare"].ToString().Trim());
                    TotFare += double.Parse(ds.Tables[1].Rows[0]["InfFare"].ToString().Trim());
                    //bool FareStatus=false;
                    //changes by abhilash end
                    farediff = OriginalTF - TotFare;
                    if (farediff < 3)
                    {
                        //=========================TST





                        try
                        {
                            //SessionIdRet = "";
                            //SessionIdRet = GetIncrementedSessionId(SessionIdPrice);
                            //sessionidTST = SessionIdRet;

                            SessionIdRet = "";
                            SessionIdRet = objSessuonHeader.GetSessionInseries(PriceRes);
                            sessionidTST = SessionIdRet;
                            SoapAction = "http://webservices.amadeus.com/TAUTCQ_04_1_1A";
                            string exep = "";
                            string folder = "MPTB/" + OrderId + DateTime.Now.ToString("hh_mm_ss");
                            STD.BAL.TSTReq objTST = new TSTReq();
                            ReqTST = objTST.getTstRequest(SessionIdRet, SoapAction, Convert.ToInt32(ds.Tables[1].Rows[0]["Adult"].ToString()),
                                Convert.ToInt32(ds.Tables[1].Rows[0]["Child"].ToString()), Convert.ToInt32(ds.Tables[1].Rows[0]["Infant"].ToString()),
                                ds.Tables[1].Rows[0]["AdtFare"].ToString().Trim(), ds.Tables[1].Rows[0]["ChdFare"].ToString().Trim(),
                                ds.Tables[1].Rows[0]["InfFare"].ToString().Trim(), PriceRes.Replace("xmlns=\"http://xml.amadeus.com/TPCBRR_18_1_1A\"", ""), AddMulRes.Replace("xmlns=\"http://xml.amadeus.com/PNRRET_17_1_1A\"", ""), ds.Tables[1].Rows[0]["Trip"].ToString().Trim(), double.Parse(ds.Tables[1].Rows[0]["OriginalTT"].ToString().Trim()), "WSP4APRE", "KTMVS3270", "AMADEUS100", IncludeSessionType.InSeries, ServiceUrls.SvcURL, ref exep, folder);
                            if (ReqTST != "" && ReqTST != "INVALIDFARE")
                            {
                                HSREQRES.Add("RETST_REQ", ReqTST);
                                ResTST = PostXml(ReqTST, SoapAction);
                                HSREQRES.Add("RETST_RES", ResTST);
                            }
                            else
                            {
                                HSREQRES.Add("RETST_REQ", ReqTST);
                                HSREQRES.Add("RETST_RES", ResTST);
                            }
                            if (ResTST == "")
                            {
                                SignInOutArray = Signoutandignore(SessionIdRet);
                                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), "", "", "");
                                HSREQRES.Add("RETST_STATUS", "FALSE");
                            }
                            else if (ResTST.Contains("<errorText>") == true)
                            {
                                SignInOutArray = Signoutandignore(SessionIdRet);
                                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), "", "");
                                HSREQRES.Add("RETST_STATUS", "FALSE");
                            }
                            else if (ResTST.Substring(0, 5) == "Error")
                            {
                                SignInOutArray = Signoutandignore(SessionIdRet);
                                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), "", "");
                                HSREQRES.Add("RETST_STATUS", "FALSE");
                            }
                            else
                            {
                                HSREQRES.Add("RETST_STATUS", "TRUE");
                            }
                        }
                        catch (Exception ex)
                        {
                            ReqTST = "";
                            SignInOutArray = Signoutandignore(SessionIdRet);
                            BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), ex.ToString(), "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), "", "", "", "");
                            HSREQRES.Add("RETST_STATUS", "FALSE");
                        }
                        if (HSREQRES["RETST_STATUS"].ToString() == "TRUE")
                        {
                            SoapAction = "http://webservices.amadeus.com/PNRADD_17_1_1A";
                            SessionIdRet = "";
                            SessionIdRet = objSessuonHeader.GetSessionInseries(ResTST);
                            SessionIdRet = SessionIdRet;
                            //SessionIdRet = "";
                            //SessionIdRet = GetIncrementedSessionId(sessionidTST);
                            string ResAME11 = "";
                            try
                            {
                                StringBuilder XmlBuilder = new StringBuilder();
                                XmlBuilder.Append("<?xml version='1.0' encoding='utf-8'?>");
                                XmlBuilder.Append("<soapenv:Envelope xmlns:soapenv='http://schemas.xmlsoap.org/soap/envelope/' xmlns:wbs='http://xml.amadeus.com/ws/2009/01/WBS_Session-2.0.xsd' xmlns:pnr='http://xml.amadeus.com/PNRADD_17_1_1A'>");
                                XmlBuilder.Append("<soapenv:Header>");
                                //XmlBuilder.Append(GetSessionHeader(sessionid)); //XmlBuilder.Append("<def:SessionId>" + SessionIdRet + "</def:SessionId>");
                                XmlBuilder.Append(objSessuonHeader.GetSessionHeader(SessionIdRet, username, officeID, pass, SoapAction, svcUrl, includeSessionType));
                                XmlBuilder.Append("</soapenv:Header>");
                                XmlBuilder.Append("<soapenv:Body>");
                                XmlBuilder.Append("<PNR_AddMultiElements>");
                                XmlBuilder.Append("<pnrActions>");
                                XmlBuilder.Append("<optionCode>11</optionCode>");
                                XmlBuilder.Append("</pnrActions>");

                                XmlBuilder.Append("<dataElementsMaster>");
                                XmlBuilder.Append("<marker1/>");
                                XmlBuilder.Append("<dataElementsIndiv>");
                                XmlBuilder.Append("<elementManagementData>");
                                XmlBuilder.Append("<reference>");
                                XmlBuilder.Append("<qualifier>OT</qualifier>");
                                XmlBuilder.Append("<number>1</number>");
                                XmlBuilder.Append("</reference>");
                                XmlBuilder.Append("<segmentName>RF</segmentName>");
                                XmlBuilder.Append("</elementManagementData>");
                                XmlBuilder.Append("<freetextData>");
                                XmlBuilder.Append("<freetextDetail>");
                                XmlBuilder.Append("<subjectQualifier>3</subjectQualifier>");
                                XmlBuilder.Append("<type>P22</type>");
                                XmlBuilder.Append("</freetextDetail>");
                                XmlBuilder.Append("<longFreetext>Repricing for ticketing</longFreetext>");
                                XmlBuilder.Append("</freetextData>");
                                XmlBuilder.Append("</dataElementsIndiv>");
                                XmlBuilder.Append("</dataElementsMaster>");

                                XmlBuilder.Append("</PNR_AddMultiElements>");
                                XmlBuilder.Append("</soapenv:Body>");
                                XmlBuilder.Append("</soapenv:Envelope>");
                                HSREQRES.Add("REPNRRETRIVE_REQ", XmlBuilder.ToString());
                                ResAME11 = PostXml(XmlBuilder.ToString(), SoapAction);
                                HSREQRES.Add("REPNRRETRIVE_RES", ResAME11);
                                if (ResAME11 == "")
                                {
                                    SignInOutArray = Signoutandignore(SessionIdRet);
                                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), HSREQRES["REPNRRETRIVE_REQ"].ToString(), "");
                                    HSREQRES.Add("REPNRRETRIVE_STATUS", "FALSE");
                                }
                                else if (ResAME11.Contains("<errorText>") == true)
                                {
                                    SignInOutArray = Signoutandignore(SessionIdRet);
                                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), HSREQRES["REPNRRETRIVE_REQ"].ToString(), HSREQRES["REPNRRETRIVE_RES"].ToString());
                                    HSREQRES.Add("REPNRRETRIVE_STATUS", "FALSE");
                                }
                                else if (ResAME11.Substring(0, 5) == "Error")
                                {
                                    SignInOutArray = Signoutandignore(SessionIdRet);
                                    BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "", "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), HSREQRES["REPNRRETRIVE_REQ"].ToString(), HSREQRES["REPNRRETRIVE_RES"].ToString());
                                    HSREQRES.Add("REPNRRETRIVE_STATUS", "FALSE");
                                }
                                else
                                {
                                    HSREQRES.Add("REPNRRETRIVE_STATUS", "TRUE");
                                }

                            }
                            catch (Exception ex)
                            {
                                SignInOutArray = Signoutandignore(SessionIdRet);
                                BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), ex.ToString(), "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), HSREQRES["RETST_REQ"].ToString(), HSREQRES["RETST_RES"].ToString(), "", "");
                                HSREQRES.Add("REPNRRETRIVE_STATUS", "FALSE");
                            }
                        }
                        else
                        {
                            Status = "FALSE";
                        }
                        if (HSREQRES["REPNRRETRIVE_STATUS"].ToString() == "TRUE")
                        {
                            Status = "TRUE";
                        }
                        else
                        {
                            Status = "FALSE";
                        }








                        //=========================TST END
                    }
                    else
                    {
                        Status = "FALSE";
                        SignInOutArray = Signoutandignore(SessionIdPrice);
                        BAL_IA.InsertGDSTicketingLogs(HSREQRES["ORDERID"].ToString(), HSREQRES["SIGNIN_REQ"].ToString(), HSREQRES["SIGNIN_RES"].ToString(), HSREQRES["RETRIVEPNR_REQ"].ToString(), HSREQRES["RETRIVEPNR_RES"].ToString(), "", "", SignInOutArray[0].ToString(), SignInOutArray[1].ToString(), "REPRICINGFAREDIFF" + farediff.ToString(), "", "", HSREQRES["REPRICE_REQ"].ToString(), HSREQRES["REPRICE_RES"].ToString(), "", "", "", "");
                    }

                }
                else { Status = "FALSE"; }

            }
            catch (Exception ex)
            {
                Status = "FALSE";
            }
            TSTArray.Add(Status);
            TSTArray.Add(SessionIdRet);
            return TSTArray;
        }
    }

}
public class TICKETDTL
{
    public string PaxType { get; set; }
    public string TKTNo { get; set; }
    public string PTNo { get; set; }

}
