﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace GALWS.AMDNPL
//{
//    public class AmdDataAccess
//    {
//    }
//}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

using System.Data.Common;
using System.Collections;
using GALWS.AMDNPL;

namespace GALWS.AMDNPL
{
    public class AmdDAL
    {
        //SqlConnection con;
        //SqlCommand cmd;
        //SqlDataAdapter adap;
        private SqlDatabase DBHelper;
        public AmdDAL(string connectionString)
        {
            DBHelper = new SqlDatabase(connectionString);
        }
        public int InsertGDSBookingLogs(string ORDERID, string PNR, string SELL_REQ, string SELL_RES, string ADDMULTI_REQ, string ADDMULTI_RES, string PRICE_REQ, string PRICE_RES, string TST_REQ, string TST_RES, string PNR_REQ,
                     string PNR_RES, string OTHER, string PNRRET_REQ, string PNRRET_RES, string SSR_REQ, string SSR_RES, string SSRSAVE_REQ, string SSRSAVE_RES, string PNRRET_AFT_SSR_REQ, string PNRRET_AFT_SSR_RES, string PCC)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_INSERTAMDGDSBOOKING";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@ORDERID", DbType.String, ORDERID);
            DBHelper.AddInParameter(cmd, "@PNR", DbType.String, PNR);
            DBHelper.AddInParameter(cmd, "@SELL_REQ", DbType.String, SELL_REQ);
            DBHelper.AddInParameter(cmd, "@SELL_RES", DbType.String, SELL_RES);
            DBHelper.AddInParameter(cmd, "@ADDMULTI_REQ", DbType.String, ADDMULTI_REQ);
            DBHelper.AddInParameter(cmd, "@ADDMULTI_RES", DbType.String, ADDMULTI_RES);
            DBHelper.AddInParameter(cmd, "@PRICE_REQ", DbType.String, PRICE_REQ);
            DBHelper.AddInParameter(cmd, "@PRICE_RES", DbType.String, PRICE_RES);
            DBHelper.AddInParameter(cmd, "@TST_REQ", DbType.String, TST_REQ);
            DBHelper.AddInParameter(cmd, "@TST_RES", DbType.String, TST_RES);
            DBHelper.AddInParameter(cmd, "@PNR_REQ", DbType.String, PNR_REQ);
            DBHelper.AddInParameter(cmd, "@PNR_RES", DbType.String, PNR_RES);
            DBHelper.AddInParameter(cmd, "@OTHER", DbType.String, OTHER);


            DBHelper.AddInParameter(cmd, "@PNRRET_REQ", DbType.String, PNRRET_REQ);
            DBHelper.AddInParameter(cmd, "@PNRRET_RES", DbType.String, PNRRET_RES);
            DBHelper.AddInParameter(cmd, "@SSR_REQ", DbType.String, SSR_REQ);
            DBHelper.AddInParameter(cmd, "@SSR_RES", DbType.String, SSR_RES);
            DBHelper.AddInParameter(cmd, "@SSRSAVE_REQ", DbType.String, SSRSAVE_REQ);
            DBHelper.AddInParameter(cmd, "@SSRSAVE_RES", DbType.String, SSRSAVE_RES);
            DBHelper.AddInParameter(cmd, "@PNRRET_AFT_SSR_REQ", DbType.String, PNRRET_AFT_SSR_REQ);
            DBHelper.AddInParameter(cmd, "@PNRRET_AFT_SSR_RES", DbType.String, PNRRET_AFT_SSR_RES);
            DBHelper.AddInParameter(cmd, "@PCC", DbType.String, PCC);

            int i = DBHelper.ExecuteNonQuery(cmd);
            return i;
        }
        public List<SoapActionUrl> GetSoapActionUrl(string PROVIDER)
        {

            var SoapList = new List<SoapActionUrl>();
            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "SP_GETSOAPACTIONURL";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@PROVIDER", DbType.String, PROVIDER);
                DataTable dt = new DataTable();
                dt.Load(DBHelper.ExecuteReader(Dbcmd));
                DataRow[] dr;
                dr = dt.Select("SOAPACTIONMETHOD IN ('ADDPAX','AIRLINEPNR','CRYPTIC','DOCISSUANCE','GDSPNR','IGNORE','PRICING','SELL','SIGNIN','SIGNOUT','TST','PNRRET')");//"SOAPACTIONMETHOD IN ('ADDPAX','AIRLINEPNR','GDSPNR','IGNORE','PRICING','SELL','SIGNIN','SIGNOUT','TST')"
                SoapList.Add(new SoapActionUrl
                {
                    ADDPAX = dr[0]["SOAPACTIONURL"].ToString(),
                    AIRLINEPNR = dr[1]["SOAPACTIONURL"].ToString(),
                    CRYPTIC = dr[2]["SOAPACTIONURL"].ToString(),
                    DOCISSUANCE = dr[3]["SOAPACTIONURL"].ToString(),
                    GDSPNR = dr[4]["SOAPACTIONURL"].ToString(),
                    IGNORE = dr[5]["SOAPACTIONURL"].ToString(),
                    PNRRET = dr[6]["SOAPACTIONURL"].ToString(),
                    PRICING = dr[7]["SOAPACTIONURL"].ToString(),
                    SELL = dr[8]["SOAPACTIONURL"].ToString(),
                    SIGNIN = dr[9]["SOAPACTIONURL"].ToString(),
                    SIGNOUT = dr[10]["SOAPACTIONURL"].ToString(),
                    TST = dr[11]["SOAPACTIONURL"].ToString()


                });
            }

            catch (Exception ex)
            {

            }
            return SoapList;

        }
        public DataTable GetTicketingCrd(string VC, string trip)
        {
            DataTable dt = new DataTable();
            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "GetTicketingCrd";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@VC", DbType.String, VC);
                DBHelper.AddInParameter(Dbcmd, "@Trip", DbType.String, trip);
                dt.Load(DBHelper.ExecuteReader(Dbcmd));

            }

            catch (Exception ex)
            {

            }
            return dt;
        }
        public int InsertGDSTicketingLogs(string ORDERID, string SIGNIN_REQ, string SIGNIN_RES, string RETRIVEPNR_REQ, string RETRIVEPNR_RES, string CRIPTIC_REQ, string CRIPTIC_RES, string SIGNOUT_REQ, string SIGNOUT_RES, string OTHER, string RETRIVEPNRTKT_REQ, string RETRIVEPNRTKT_RES, string REPRICE_REQ, string REPRICE_RES, string RETST_REQ, string RETST_RES, string REPNRRETRIVE_REQ, string REPNRRETRIVE_RES)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_INSERTGDSTICKETINGLOGS";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@ORDERID", DbType.String, ORDERID);
            DBHelper.AddInParameter(cmd, "@SIGNIN_REQ", DbType.String, SIGNIN_REQ);
            DBHelper.AddInParameter(cmd, "@SIGNIN_RES", DbType.String, SIGNIN_RES);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNR_REQ", DbType.String, RETRIVEPNR_REQ);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNR_RES", DbType.String, RETRIVEPNR_RES);
            DBHelper.AddInParameter(cmd, "@CRIPTIC_REQ", DbType.String, CRIPTIC_REQ);
            DBHelper.AddInParameter(cmd, "@CRIPTIC_RES", DbType.String, CRIPTIC_RES);
            DBHelper.AddInParameter(cmd, "@SIGNOUT_REQ", DbType.String, SIGNOUT_REQ);
            DBHelper.AddInParameter(cmd, "@SIGNOUT_RES", DbType.String, SIGNOUT_RES);
            DBHelper.AddInParameter(cmd, "@OTHER", DbType.String, OTHER);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNRTKT_REQ", DbType.String, RETRIVEPNRTKT_REQ);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNRTKT_RES", DbType.String, RETRIVEPNRTKT_RES);
            DBHelper.AddInParameter(cmd, "@REPRICE_REQ", DbType.String, REPRICE_REQ);
            DBHelper.AddInParameter(cmd, "@REPRICE_RES", DbType.String, REPRICE_RES);
            DBHelper.AddInParameter(cmd, "@RETST_REQ", DbType.String, RETST_REQ);
            DBHelper.AddInParameter(cmd, "@RETST_RES", DbType.String, RETST_RES);
            DBHelper.AddInParameter(cmd, "@REPNRRETRIVE_REQ", DbType.String, REPNRRETRIVE_REQ);
            DBHelper.AddInParameter(cmd, "@REPNRRETRIVE_RES", DbType.String, REPNRRETRIVE_RES);
            int i = DBHelper.ExecuteNonQuery(cmd);
            return i;
        }

        public int Insert1ATicketingLogs(string ORDERID, string RETRIVEPNR_REQ, string RETRIVEPNR_RES, string DocIssuance_REQ, string DocIssuance_RES, string RETRIVEPNRTKT_REQ, string RETRIVEPNRTKT_RES, string SIGNOUT_REQ,
                      string SIGNOUT_RES, string OTHER, string REPRICE_REQ, string REPRICE_RES, string RETST_REQ, string RETST_RES, string AdEliment11_REQ, string AdEliment11__RES, string AdEliment21_REQ, string AdEliment21__RES, string OfficeID)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_INSERT1ATICKETINGLOGS";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@ORDERID", DbType.String, ORDERID);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNR_REQ", DbType.String, RETRIVEPNR_REQ);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNR_RES", DbType.String, RETRIVEPNR_RES);
            DBHelper.AddInParameter(cmd, "@DocIssuance_REQ", DbType.String, DocIssuance_REQ);
            DBHelper.AddInParameter(cmd, "@DocIssuance_RES", DbType.String, DocIssuance_RES);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNRTKT_REQ", DbType.String, RETRIVEPNRTKT_REQ);
            DBHelper.AddInParameter(cmd, "@RETRIVEPNRTKT_RES", DbType.String, RETRIVEPNRTKT_RES);
            DBHelper.AddInParameter(cmd, "@REPRICE_REQ", DbType.String, REPRICE_REQ);
            DBHelper.AddInParameter(cmd, "@REPRICE_RES", DbType.String, REPRICE_RES);
            DBHelper.AddInParameter(cmd, "@RETST_REQ", DbType.String, RETST_REQ);
            DBHelper.AddInParameter(cmd, "@RETST_RES", DbType.String, RETST_RES);
            DBHelper.AddInParameter(cmd, "@AdEliment11_REQ", DbType.String, AdEliment11_REQ);
            DBHelper.AddInParameter(cmd, "@AdEliment11_RES", DbType.String, AdEliment11__RES);
            DBHelper.AddInParameter(cmd, "@AdEliment21_REQ", DbType.String, AdEliment21_REQ);
            DBHelper.AddInParameter(cmd, "@AdEliment21_RES", DbType.String, AdEliment21__RES);
            DBHelper.AddInParameter(cmd, "@SIGNOUT_REQ", DbType.String, SIGNOUT_REQ);
            DBHelper.AddInParameter(cmd, "@SIGNOUT_RES", DbType.String, SIGNOUT_RES);
            DBHelper.AddInParameter(cmd, "@OTHER", DbType.String, OTHER);
            DBHelper.AddInParameter(cmd, "@OfficeID", DbType.String, OfficeID);

            int i = DBHelper.ExecuteNonQuery(cmd);
            return i;
        }

        public DataSet GetGDSXmlLogsAndFlight(string OrderId)
        {
            DataSet ds = new DataSet();

            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "SP_GETGDSXMLLOGSANDFLIGHT";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@ORDERID", DbType.String, OrderId);
                ds = DBHelper.ExecuteDataSet(Dbcmd);

            }

            catch (Exception ex)
            {

            }
            return ds;
        }
        public DataTable GetSessionPooling(string Type, string SessionId, string SecurityToken, int SqNumber, int PoolCounter)
        {
            DataTable dt = new DataTable();
            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "SP_Session_Pooling";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@Type", DbType.String, Type);
                DBHelper.AddInParameter(Dbcmd, "@SessionId", DbType.String, SessionId);
                DBHelper.AddInParameter(Dbcmd, "@SecurityToken", DbType.String, SecurityToken);
                DBHelper.AddInParameter(Dbcmd, "@SqNumber", DbType.Int32, SqNumber);
                DBHelper.AddInParameter(Dbcmd, "@PoolCounter", DbType.Int32, PoolCounter);
                dt.Load(DBHelper.ExecuteReader(Dbcmd));

            }

            catch (Exception ex)
            {

            }
            return dt;
        }
        public List<BlockAirline> BlockAirline(string Provider, string Trip)
        {

            List<BlockAirline> listblock = new List<BlockAirline>();
            IDataReader reader;
            DbCommand cmd = new SqlCommand();

            try
            {
                cmd.CommandText = "SP_BLOCK_AIRLINE";
                cmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(cmd, "@Provider", DbType.String, Provider);
                DBHelper.AddInParameter(cmd, "@Trip", DbType.String, Trip);
                reader = DBHelper.ExecuteReader(cmd);
                while (reader.Read())
                {
                    listblock.Add(new BlockAirline
                    {
                        Airline = reader["Airline"].ToString(),
                        Provider = float.Parse(reader["Provider"].ToString()),
                        Trip = float.Parse(reader["Trip"].ToString())
                    });
                }
            }
            catch (Exception ex)
            {
            }
            finally
            {
                //reader.Close();
                //cmd.Dispose();
            }
            return listblock;
        }
        public DataTable GetExpiredSession(int PoolCounter)
        {
            DataTable dt = new DataTable();
            try
            {
                DbCommand Dbcmd = new SqlCommand();
                Dbcmd.CommandText = "SP_GET_DELETE_EXPIREDSESSION";
                Dbcmd.CommandType = CommandType.StoredProcedure;
                DBHelper.AddInParameter(Dbcmd, "@Counter", DbType.Int32, PoolCounter);
                dt.Load(DBHelper.ExecuteReader(Dbcmd));

            }

            catch (Exception ex)
            {
                throw ex;

            }
            return dt;
        }
        public int DeleteExpiredSession(int Counter)
        {
            DbCommand cmd = new SqlCommand();
            cmd.CommandText = "SP_GET_DELETE_EXPIREDSESSION";
            cmd.CommandType = CommandType.StoredProcedure;
            DBHelper.AddInParameter(cmd, "@Counter", DbType.Int32, Counter);
            int i = DBHelper.ExecuteNonQuery(cmd);
            return i;
        }
    }
}
