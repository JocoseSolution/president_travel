﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Data;
namespace GALWS.AMDNPL
{
    public class AmdBusiness
    {
        string Conn = "";

        public AmdBusiness(string ConnectionString)
        {
            Conn = ConnectionString;
        }
        public int InsertGDSBookingLogs(string ORDERID, string PNR, string SELL_REQ, string SELL_RES, string ADDMULTI_REQ, string ADDMULTI_RES, string PRICE_REQ, string PRICE_RES, string TST_REQ, string TST_RES, string PNR_REQ,
                      string PNR_RES, string OTHER, string PNRRET_REQ, string PNRRET_RES, string SSR_REQ, string SSR_RES, string SSRSAVE_REQ, string SSRSAVE_RES, string PNRRET_AFT_SSR_REQ, string PNRRET_AFT_SSR_RES, string PCC)
        {
            try
            {
                AmdDAL DAL1A = new AmdDAL(Conn);
                return DAL1A.InsertGDSBookingLogs(ORDERID, PNR, SELL_REQ, SELL_RES, ADDMULTI_REQ, ADDMULTI_RES, PRICE_REQ, PRICE_RES, TST_REQ, TST_RES, PNR_REQ, PNR_RES, OTHER, PNRRET_REQ, PNRRET_RES, SSR_REQ, SSR_RES, SSRSAVE_REQ, SSRSAVE_RES, PNRRET_AFT_SSR_REQ, PNRRET_AFT_SSR_RES, PCC);
            }
            catch { return 0; }
        }
        public List<SoapActionUrl> GetSoapActionUrl(string Provider)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetSoapActionUrl(Provider);
        }
        public DataTable GetTicketingCrd(string VC, string trip)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetTicketingCrd(VC, trip);
        }
        public int InsertGDSTicketingLogs(string ORDERID, string SIGNIN_REQ, string SIGNIN_RES, string RETRIVEPNR_REQ, string RETRIVEPNR_RES, string CRIPTIC_REQ, string CRIPTIC_RES, string SIGNOUT_REQ,
                      string SIGNOUTRES, string OTHER, string RETRIVEPNRTKT_REQ, string RETRIVEPNRTKT_RES, string REPRICE_REQ, string REPRICE_RES, string RETST_REQ, string RETST_RES, string REPNRRETRIVE_REQ, string REPNRRETRIVE_RES)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.InsertGDSTicketingLogs(ORDERID, SIGNIN_REQ, SIGNIN_RES, RETRIVEPNR_REQ, RETRIVEPNR_RES, CRIPTIC_REQ, CRIPTIC_RES, SIGNOUT_REQ,
                      SIGNOUTRES, OTHER, RETRIVEPNRTKT_REQ, RETRIVEPNRTKT_RES, REPRICE_REQ, REPRICE_RES, RETST_REQ, RETST_RES, REPNRRETRIVE_REQ, REPNRRETRIVE_RES);
        }

        public int Insert1ATicketingLogs(string ORDERID, string RETRIVEPNR_REQ, string RETRIVEPNR_RES, string DocIssuance_REQ, string DocIssuance_RES, string RETRIVEPNRTKT_REQ, string RETRIVEPNRTKT_RES, string SIGNOUT_REQ,
                      string SIGNOUT_RES, string OTHER, string REPRICE_REQ, string REPRICE_RES, string RETST_REQ, string RETST_RES, string AdEliment11_REQ, string AdEliment11__RES, string AdEliment21_REQ, string AdEliment21__RES, string OfficeID)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.Insert1ATicketingLogs(ORDERID, RETRIVEPNR_REQ, RETRIVEPNR_RES, DocIssuance_REQ, DocIssuance_RES, RETRIVEPNRTKT_REQ, RETRIVEPNRTKT_RES, SIGNOUT_REQ,
                       SIGNOUT_RES, OTHER, REPRICE_REQ, REPRICE_RES, RETST_REQ, RETST_RES, AdEliment11_REQ, AdEliment11__RES, AdEliment21_REQ, AdEliment21__RES, OfficeID);
        }

        public DataSet GetGDSXmlLogsAndFlight(string OrderId)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetGDSXmlLogsAndFlight(OrderId);
        }
        public DataTable GetSessionPooling(string Type, string SessionId, string SecurityToken, int SqNumber, int PoolCounter)
        {

            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetSessionPooling(Type, SessionId, SecurityToken, SqNumber, PoolCounter);
        }
        public List<BlockAirline> BlockAirline(string Provider, string Trip)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.BlockAirline(Provider, Trip);

        }
        public DataTable GetExpiredSession(int PoolCounter)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetExpiredSession(PoolCounter);
        }
        public int DeleteExpiredSession(int PoolCounter)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.DeleteExpiredSession(PoolCounter);
        }


    }
}
