﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using STD.DAL;
using System.Data;
namespace STD.BAL
{
    public class AmdBAL
    {
        string Conn = "";

        public AmdBAL(string ConnectionString)
        {
            Conn = ConnectionString;
        }
        public int InsertGDSBookingLogs(string ORDERID, string PNR, string SELL_REQ, string SELL_RES, string ADDMULTI_REQ, string ADDMULTI_RES, string PRICE_REQ, string PRICE_RES, string TST_REQ, string TST_RES, string PNR_REQ,
                      string PNR_RES, string OTHER)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.InsertGDSBookingLogs(ORDERID, PNR, SELL_REQ, SELL_RES, ADDMULTI_REQ, ADDMULTI_RES, PRICE_REQ, PRICE_RES, TST_REQ, TST_RES, PNR_REQ, PNR_RES, OTHER);
        }
        public List<Shared.SoapActionUrl> GetSoapActionUrl(string Provider)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetSoapActionUrl(Provider);
        }
        public DataTable GetTicketingCrd(string VC)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetTicketingCrd(VC);
        }
        public int InsertGDSTicketingLogs(string ORDERID, string SIGNIN_REQ, string SIGNIN_RES, string RETRIVEPNR_REQ, string RETRIVEPNR_RES, string CRIPTIC_REQ, string CRIPTIC_RES, string SIGNOUT_REQ,
                      string SIGNOUTRES, string OTHER, string RETRIVEPNRTKT_REQ, string RETRIVEPNRTKT_RES, string REPRICE_REQ, string REPRICE_RES, string RETST_REQ, string RETST_RES, string REPNRRETRIVE_REQ, string REPNRRETRIVE_RES)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.InsertGDSTicketingLogs(ORDERID, SIGNIN_REQ, SIGNIN_RES, RETRIVEPNR_REQ, RETRIVEPNR_RES, CRIPTIC_REQ, CRIPTIC_RES, SIGNOUT_REQ,
                      SIGNOUTRES, OTHER, RETRIVEPNRTKT_REQ, RETRIVEPNRTKT_RES, REPRICE_REQ, REPRICE_RES, RETST_REQ, RETST_RES, REPNRRETRIVE_REQ, REPNRRETRIVE_RES);
        }
        public DataSet GetGDSXmlLogsAndFlight(string OrderId)
        {
            AmdDAL DAL1A = new AmdDAL(Conn);
            return DAL1A.GetGDSXmlLogsAndFlight(OrderId);
        }
    }
}
