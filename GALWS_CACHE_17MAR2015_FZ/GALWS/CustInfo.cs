﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace STD.Shared
{
    public class CustInfo
    {
        public int PAXID { get; set; }
        public string ORDERID { get; set; }
        public string TRANSACTIONID { get; set; }
        public string TITLE { get; set; }
        public string FNAME { get; set; }
        public string MNAME { get; set; }
        public string LNAME { get; set; }
        public string PAXTYPE { get; set; }
        public string TICKETNUMBER { get; set; }
        public string DOB { get; set; }
        public string FFNUMBER { get; set; }
        public string FFAIRLINE { get; set; }
        public string MEALTYPE { get; set; }
        public string SEATTYPE { get; set; }
        public bool ISPRIMARY { get; set; }
        public string INFASSOCIATEPAXNAME { get; set; }
        public string MORDIFYSTATUS { get; set; }

    }
}
