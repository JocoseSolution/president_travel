﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Globalization;
using System.Xml;
using System.Xml.Serialization;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using System.Configuration;

namespace STD.BAL
{
    /// <summary>
    /// Summary description for Utility
    /// </summary>
    /// 
    [Serializable()]
    public class Utility
    {
        #region String Utility Functions
        public static string[] Split(string input, string seperator)
        {
            return input.Split(new string[1] { seperator }, StringSplitOptions.None);
        }
        public static string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable
            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public static string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public static string Mid(string param, int startIndex, int endIndex)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, endIndex);
            //return the result of the operation
            return result;
        }

        public static void Split_Tax_YQ_OT(string[] tax, out double YQ, out double OT)
        {
            string[] tax1 = null;
            YQ = 0;
            OT = 0;

            try
            {
                for (int i = 0; i <= tax.Length - 2; i++)
                {
                    if (tax[i].Contains("YQ"))//Strings.InStr(tax[i], "YQ")
                    {
                        tax1 = tax[i].Split(':');
                        YQ = YQ + Convert.ToInt32(tax1[1]);
                    }
                    else
                    {
                        tax1 = tax[i].Split(':');
                        OT = OT + Convert.ToInt32(tax1[1]);
                    }
                }
            }
            catch (Exception ex)
            {
                //throw ex;
            }


            //totTax = YQ + YR + WO + OT;
            //totFare = basefare + totTax + SrvTax + TF + admrk;
            //netFare = (totFare + tds) - comm;
        }

        public static bool SaveTextToFile(string strData, string FullPath, ref string ErrInfo)
        {
            string Contents = null;
            bool Saved = false;
            StreamWriter objReader = default(StreamWriter);
            try
            {
                objReader = new StreamWriter(FullPath);
                objReader.Write(strData);
                objReader.Close();
                Saved = true;
            }
            catch (Exception Ex)
            {
                ErrInfo = Ex.Message;
            }
            return Saved;
        }

        public static string datecon(string MM)
        {
            string mm_str = "";
            switch (MM)
            {
                case "01":
                    mm_str = "JAN";
                    break;
                case "02":
                    mm_str = "FEB";
                    break;
                case "03":
                    mm_str = "MAR";
                    break;
                case "04":
                    mm_str = "APR";
                    break;
                case "05":
                    mm_str = "MAY";
                    break;
                case "06":
                    mm_str = "JUN";
                    break;
                case "07":
                    mm_str = "JUL";
                    break;
                case "08":
                    mm_str = "AUG";
                    break;
                case "09":
                    mm_str = "SEP";
                    break;
                case "10":
                    mm_str = "OCT";
                    break;
                case "11":
                    mm_str = "NOV";
                    break;
                case "12":
                    mm_str = "DEC";
                    break;
                default:

                    break;
            }

            return mm_str;

        }


        #endregion

        internal static string[] Split(string allowedChars1, char[] sep)
        {
            throw new NotImplementedException();
        }

        public static string GetRandomNumber(string allowedChars1)
        {

            char[] sep = { ',' };

            string[] arr = allowedChars1.Split(sep);

            string rndString = "";

            string temp = "";

            Random rand = new Random();

            for (int i = 0; i <= 4; i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                rndString += temp;
            }

            return rndString;

        }

        public static string GetRndm()
        {

            string allowedChars = "";

            allowedChars = "1,2,3,4,5,6,7,8,9,0";

            string rnmd2 = GetRandomNumber(allowedChars);

            return rnmd2;

        }

        public static string ConvertToAgeFromDOB(string birthday)
        {
            //IFormatProvider culture = new CultureInfo("en-US", true);
            //DateTime dt = DateTime.Parse(birthday, culture);

            DateTime data;
            DateTime.TryParseExact(birthday, "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out data);
           DateTime now = DateTime.Today;
            int age = now.Year - data.Year;
            if (now < data.AddYears(age)) age--;
            if (age.ToString().Length == 1)
                return "0" + age.ToString();
            else
                return age.ToString();
        }

        public static System.Boolean IsNumeric(System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { return false; }

        }

        public string SerializeAnObject(object obj, string R)
        {
            string newFileName = R + DateTime.Now.ToString("hh:mm:ss");
            string xml = "";
            XmlDocument doc = new XmlDocument();
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            System.IO.MemoryStream stream = new System.IO.MemoryStream();
            try
            {
                //string activeDir = @"D:\Req_Res_Abacus\" + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\";
                ////string activeDir = ConfigurationManager.AppSettings["AbacusSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + @"\";

                //DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
                //if (!Directory.Exists(objDirectoryInfo.FullName))
                //{
                //    Directory.CreateDirectory(activeDir);
                //}


                serializer.Serialize(stream, obj);
                stream.Position = 0;
                doc.Load(stream);
                //doc.Save(activeDir + newFileName.Replace(":", "") + ".xml");
                xml = doc.InnerXml.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            finally
            {
                stream.Close();
                stream.Dispose();
            }
            return xml;
        }


       
        public static string RemoveAlpha(string s)
        {
            return Regex.Replace(s, "[A-Za-z]", "");
        }
    
        public static XDocument RemoveNamespace(XDocument xdoc)
        {
            foreach (XElement e in xdoc.Root.DescendantsAndSelf())
            {
                if (e.Name.Namespace != XNamespace.None)
                {
                    e.Name = XNamespace.None.GetName(e.Name.LocalName);
                }
                if (e.Attributes().Where(a => a.IsNamespaceDeclaration || a.Name.Namespace != XNamespace.None).Any())
                {
                    e.ReplaceAttributes(e.Attributes().Select(a => a.IsNamespaceDeclaration ? null : a.Name.Namespace != XNamespace.None ? new XAttribute(XNamespace.None.GetName(a.Name.LocalName), a.Value) : a));
                }
            }

            return xdoc;
        }
        public static void SaveFile(string Res, string module, string Userid, string ailine = "")
        {

            string Hour = DateTime.Now.ToString("HH");
            string Minutes = DateTime.Now.ToString("mm");
            string newFileName = module + DateTime.Now.ToString("hh_mm_ss");
            string activeDir = ConfigurationManager.AppSettings["B2BSaveUrl"] + DateTime.Now.ToString("dd-MMMM-yyyy") + "\\" + Userid + "\\";
            if (newFileName.Contains("SearchReq_") || newFileName.Contains("SearchRes_") || newFileName.Contains("ItineraryPriceReq_") || newFileName.Contains("ItineraryPriceRes_") || newFileName.Contains("FareRulesRequest_") || newFileName.Contains("FareRulesResponse_"))
                activeDir += Hour + "\\" + ailine + "\\";
            else
            {
                string[] orderid = module.Split('_');
                activeDir += @"\" + orderid[1] + @"\";
                newFileName = newFileName.Replace(orderid[1], "");
            }

            DirectoryInfo objDirectoryInfo = new DirectoryInfo(activeDir);
            if (!Directory.Exists(objDirectoryInfo.FullName))
            {
                Directory.CreateDirectory(activeDir);
            }
            try
            {
                string path = activeDir + newFileName + ".txt";
                if (!File.Exists(path))
                {
                    // Create a file to write to.
                    using (StreamWriter sw = File.CreateText(path))
                    {
                        sw.Write(Res);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}